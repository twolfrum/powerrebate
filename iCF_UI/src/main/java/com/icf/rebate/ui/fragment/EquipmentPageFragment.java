package com.icf.rebate.ui.fragment;

import java.util.List;
import java.util.Map;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.icf.rebate.networklayer.model.CustomerInfo;
import com.icf.rebate.networklayer.model.FormResponseBean.Equipment;
import com.icf.rebate.networklayer.model.FormResponseBean.Item;
import com.icf.rebate.networklayer.model.FormResponseBean.Options;
import com.icf.rebate.networklayer.model.FormResponseBean.Part;
import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.rebate.ui.FormFragment;
import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.ui.controller.FormUIManager;
import com.icf.rebate.ui.controller.FormUIManager.FormViewCreationCallback;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.listeners.FragmentCallFlow;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.ICFLogger;
import com.icf.rebate.ui.util.UiUtil;
import com.icf.rebate.ui.util.UiViewUtils;

public class EquipmentPageFragment extends Fragment implements OnCheckedChangeListener, FragmentCallFlow, FormViewCreationCallback
{
    public static final String TAG = EquipmentPageFragment.class.getSimpleName();
    Item item;
    LinearLayout scrollViewLayout;
    Switch switch1;
    FormUIManager manager;
    EditText waterHeaterEfficiency, circulatingHeatPump;
	TextView result;

	private Equipment equipment;
    private LinearLayout view;
    private boolean isDeleted = false;
    private String title;

    public EquipmentPageFragment()
    {
	super();
    }

    public static EquipmentPageFragment newInstance(Item item)
    {
	if (item == null)
	{
	    throw new IllegalArgumentException();
	}
	EquipmentPageFragment frag = new EquipmentPageFragment();
	frag.item = item;
	return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
	View view = inflater.inflate(R.layout.form_equipment_page_layout, null, false);
	return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
	super.onViewCreated(view, savedInstanceState);
	getUiControls(view);
	initialize(view);
	bindCallbacks(view);
	requestData();

	CustomerInfo cInfo = RebateManager.getInstance().getFormBean().getCustomerInfo();
	if (UiUtil.isJobCompleted(cInfo))
	{
	    UiUtil.setEnableView((ViewGroup) view, false, null);
	}
    }

    private void resetFormViews()
    {
	result = null;
    }

    @Override
    public void requestData()
    {

    }

    @Override
    public void getUiControls(View root)
    {

	view = (LinearLayout) root.findViewById(R.id.switch_parent_layout);
	switch1 = (Switch) root.findViewById(R.id.equipment_type);
	switch1.setTextOn("       ");
	switch1.setTextOff("       ");
	scrollViewLayout = (LinearLayout) root.findViewById(R.id.scrol_layout);
    }

    @Override
    public void initialize(View root)
    {
	if (!checkEarlyOrReplace(item))
	{
	    view.setVisibility(View.GONE);
	}
	manager = new FormUIManager(getString(R.string.equipmentId));
	manager.setFormViewCreationCallback(this);
	switch1.setChecked(item.isReplaceOnFailSelected());
	manager.setEquipmentName(equipment.getEquipmentId());
	manager.createViews(getParentFragment(), scrollViewLayout, item);
    }

    @Override
    public void bindCallbacks(View root)
    {
	switch1.setOnCheckedChangeListener(this);

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
    {
	resetFormViews();
	item.setReplaceOnFailSelected(isChecked);
	if (!item.isReplaceOnFailSelected())
	{
	    manager.setPartBackground(R.drawable.diff_part_bg);
	}
	else
	{
	    manager.setPartBackground(R.drawable.part_bg);
	}
	scrollViewLayout.removeAllViews();
	manager.createViews(getParentFragment(), scrollViewLayout, item);
    }

    public Item getItem()
    {
	return item;
    }

    public void setEquipment(Equipment equipment)
    {
	this.equipment = equipment;
    }

    public EditText getLastFocusedEditField()
    {
	return manager.getLastFocusedEditField();
    }

    public void updateCaptureImage(int pageIndex)
    {
	UiUtil.updateCaptureImage(manager, pageIndex + "", getActivity());
    }

    @Override
    public void viewCreated(View view, Part part, final Options option)
    {
	if (result == null)
	{
	    String name = null;
	    if (part != null)
	    {
		name = part.getId();
		if (name != null && name.equalsIgnoreCase(AppConstants.EQUIPMENTS_RESULT_ID))
		{
            result = (TextView) view;
            try {
                item.setStaticRebateValue(
                        ((Map<String, String>) result.getTag()).get("staticDisplayedValue"));
            } catch (Exception e) {
            }
		}
	    }
	}

	if (waterHeaterEfficiency == null)
	{
	    String name = null;

	    if (part != null)
	    {
		name = part.getId();

		if (name != null && name.equalsIgnoreCase(AppConstants.WATER_HEATER_EFFICIENCY_ID))
		{
		    if (view instanceof EditText)
		    {
			waterHeaterEfficiency = (EditText) view;
			waterHeaterEfficiency.setOnFocusChangeListener(new OnFocusChangeListener()
			{

			    @Override
			    public void onFocusChange(View v, boolean hasFocus)
			    {
				if (!hasFocus && option != null && option.getSavedValue() != null && !TextUtils.isEmpty(option.getSavedValue()))
				{
				    if (option.getSavedValue().startsWith("0."))
				    {
					waterHeaterEfficiency.setText(option.getSavedValue());
				    }
				    else
				    {
					int value = 0;
					if (option.getSavedValue().contains("."))
					{
					    value = (int) LibUtils.parseFloat(option.getSavedValue(), 0);
					}
					else
					{
					    value = LibUtils.parseInteger(option.getSavedValue(), 0);
					}
					if (value >= 0)
					{
					    option.setSavedValue("0." + String.valueOf(value));
					    waterHeaterEfficiency.setText(option.getSavedValue());
					}
				    }

				}
				if (UiViewUtils.isValidationMode(option.isItemSaved()) && option.isMandatory() && !option.isFieldFilled())
				{
				    waterHeaterEfficiency.setBackground(getActivity().getResources().getDrawable(R.drawable.bg_dialog_mandatory_box));
				}
				else
				{
				    waterHeaterEfficiency.setBackground(getActivity().getResources().getDrawable(R.drawable.combo_box_bg));
				}
			    }
			});
		    }
		}
	    }

	}

	// if (circulatingHeatPump == null)
	{
	    String name = null;

	    if (part != null)
	    {
		name = part.getId();

		if (name != null && name.equalsIgnoreCase(AppConstants.GEOTHERMAL_CIRCULATING_PUMPSIZE))
		{
		    if (view instanceof EditText)
		    {
			circulatingHeatPump = (EditText) view;
			circulatingHeatPump.setOnFocusChangeListener(new OnFocusChangeListener()
			{

			    @Override
			    public void onFocusChange(View v, boolean hasFocus)
			    {
				if (!hasFocus && option != null && option.getSavedValue() != null && !TextUtils.isEmpty(option.getSavedValue()))
				{
				    if (option.getSavedValue().startsWith("0."))
				    {
					circulatingHeatPump.setText(option.getSavedValue());
				    }
				    else
				    {
					int value = 0;
					if (option.getSavedValue().contains("."))
					{
					    value = (int) LibUtils.parseFloat(option.getSavedValue(), 0);
					    // circulatingHeatPump.setText(LibUtils.parseFloat(option.getSavedValue(), 0) + "");
					}
					else
					{
					    value = LibUtils.parseInteger(option.getSavedValue(), 0);
					}

					if (value >= 0)
					{
					    option.setSavedValue("0." + String.valueOf(value));
					    circulatingHeatPump.setText(option.getSavedValue());
					}
				    }
				}
				if (UiViewUtils.isValidationMode(option.isItemSaved()) && option.isMandatory() && !option.isFieldFilled())
				{
				    circulatingHeatPump.setBackground(getActivity().getResources().getDrawable(R.drawable.bg_dialog_mandatory_box));
				}
				else
				{
				    circulatingHeatPump.setBackground(getActivity().getResources().getDrawable(R.drawable.combo_box_bg));
				}
			    }
			});
		    }
		}
	    }

	}
    }

    public void updateRebate(float rebateValue)
    {
	if (result != null)
	{
        String formatedValue = item.getStaticRebateValue();
        if (TextUtils.isEmpty(formatedValue)) {
            formatedValue = (String.format("%.02f", rebateValue));
        }
	    result.setText(formatedValue + "");
	    ICFLogger.d(TAG, "Result:" + "$" + formatedValue + "");
	}
    }

    private boolean checkEarlyOrReplace(Item item)
    {
	boolean bRet = false;
	if (item != null)
	{
	    if (item.getEarlyRetirement() == null || item.getReplaceOnFail() == null || item.getEarlyRetirement().getParts() == null || item.getReplaceOnFail().getParts() == null || item.getEarlyRetirement().getParts().size() == 0 || item.getReplaceOnFail().getParts().size() == 0)
	    {
		if (item.getReplaceOnFail() == null)
		{
		    item.setReplaceOnFailSelected(false);
		}
		bRet = false;
	    }
	    else
	    {
		bRet = true;
	    }
	}
	return bRet;
    }

    @Override
    public void onDestroyView()
    {
	UiUtil.cleanBitmap(getView());
	saveForm();
	resetFormViews();
	super.onDestroyView();
    }
    
    public void saveForm()
    {
	if (!isDeleted)
	{
	    List<Item> items = ((FormFragment) getParentFragment()).getEquipmentItems();
	    if (items != null)
	    {
		int index = items.indexOf(item);
		if (index > -1)
		{
		    ICFLogger.d(TAG, "onDestroyView " + index);
		    ((FormFragment) getParentFragment()).saveForm(this, index, false);
		}
	    }
	}
    }

    public void setFragmentDeleted()
    {
	isDeleted = true;
    }

    public void setTitleId(String title)
    {
	this.title = title;
    }

    public String getTitleId()
    {
	return this.title;
    }

}
