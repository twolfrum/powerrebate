package com.icf.rebate.ui.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLConfig;

public class MeasurableWebView extends WebView {
    private final int MAX_DIMEN;

    public MeasurableWebView(Context context) {
        super(context);
        MAX_DIMEN = initMaxDimen();
    }

    public MeasurableWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        MAX_DIMEN = initMaxDimen();
    }

    public int getContentWidth() {
        return Math.min(MAX_DIMEN, this.computeHorizontalScrollRange());
    }

    @Override
    public int getContentHeight() {
        return Math.min(MAX_DIMEN, this.computeVerticalScrollRange());
    }

    private int initMaxDimen() {
        // Safe minimum default size
        final int IMAGE_MAX_BITMAP_DIMENSION = 2048;

        // Get EGL Display
        EGL10 egl = (EGL10) EGLContext.getEGL();
        EGLDisplay display = egl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);

        // Initialise
        int[] version = new int[2];
        egl.eglInitialize(display, version);

        // Query total number of configurations
        int[] totalConfigurations = new int[1];
        egl.eglGetConfigs(display, null, 0, totalConfigurations);

        // Query actual list configurations
        EGLConfig[] configurationsList = new EGLConfig[totalConfigurations[0]];
        egl.eglGetConfigs(display, configurationsList, totalConfigurations[0], totalConfigurations);

        int[] textureSize = new int[1];
        int maximumTextureSize = 0;

        // Iterate through all the configurations to located the maximum texture size
        for (int i = 0; i < totalConfigurations[0]; i++) {
            // Only need to check for width since opengl textures are always squared
            egl.eglGetConfigAttrib(display, configurationsList[i], EGL10.EGL_MAX_PBUFFER_WIDTH, textureSize);

            // Keep track of the maximum texture size
            if (maximumTextureSize < textureSize[0])
                maximumTextureSize = textureSize[0];
        }

        // Release
        egl.eglTerminate(display);

        // Return largest texture size found, or default
        return Math.max(maximumTextureSize, IMAGE_MAX_BITMAP_DIMENSION);
    }
}

