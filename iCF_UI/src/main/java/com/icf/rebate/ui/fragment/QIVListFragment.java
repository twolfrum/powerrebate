package com.icf.rebate.ui.fragment;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import io.fabric.sdk.android.Fabric;
import android.widget.ListView;
import android.widget.TextView;

import com.icf.rebate.adapter.QIVAdapter;
import com.icf.rebate.app.model.DashboardItem.ServiceItem;
import com.icf.rebate.networklayer.model.FormResponseBean;
import com.icf.rebate.networklayer.model.QIVItem;
import com.crashlytics.android.Crashlytics;
import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.ui.RootActivity;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.listeners.FragmentCallFlow;
import com.icf.rebate.ui.listeners.RequestCallFlow;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.UiUtil;
import com.icf.rebate.ui.util.rebate.QIVRebateCalculation;

public class QIVListFragment extends Fragment implements FragmentCallFlow, RequestCallFlow, OnItemClickListener
{
    private ListView equipmentListView;
    private String title;
    private TextView emptyTxtView;
    private List<QIVItem> qivData = new ArrayList<QIVItem>();
    private QIVAdapter qivAAdapter;
    private QIVRebateCalculation qivRebateCalculation;
    private boolean shouldComputeRebate = true;
    private ServiceItem serviceItem;

    public QIVListFragment()
    {
	super();
    }

    public void setServiceList(ArrayList<ServiceItem> serviceList)
    {
		int index = serviceList.indexOf(new ServiceItem(AppConstants.QUALITYINSTALLVERIFICATION_ID));
		if (index > -1)
		{
			serviceItem = serviceList.get(index);
		}
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
	View view = inflater.inflate(R.layout.equipment_screen, null, false);

	return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
	super.onViewCreated(view, savedInstanceState);
	getUiControls(view);
	initialize(view);
	bindCallbacks(view);
	requestData();
    }

    @Override
    public void showProgressDialog()
    {
	getActivity().runOnUiThread(new Runnable()
	{
	    public void run()
	    {
		UiUtil.showProgressDialog(getActivity());
	    }
	});
    }

    @Override
    public void stopProgressDialog()
    {
	getActivity().runOnUiThread(new Runnable()
	{
	    public void run()
	    {
		UiUtil.dismissSpinnerDialog();
	    }
	});
    }

    @Override
    public void updateData(final Object obj)
    {
	if (obj != null)
	{

	    this.qivData.clear();

	    List<QIVItem> qivData = ((FormResponseBean) obj).getQivList();

	    if (qivData != null)
	    {
		ListIterator<QIVItem> iterator = qivData.listIterator();
		int index = -1;
		while (iterator.hasNext())
		{
		    QIVItem qivItem = (QIVItem) iterator.next();
		    if (qivItem.isItemSaved())
		    {
			++index;
			qivItem.setIndex(index);
			this.qivData.add(qivItem);
		    }
		}
	    }

	    getActivity().runOnUiThread(new Runnable()
	    {
		public void run()
		{
		    if (QIVListFragment.this.qivData.size() > 0)
		    {
			// qivAAdapter.addAll(qivData);
			qivAAdapter.notifyDataSetChanged();
		    }
		    else
		    {
			equipmentListView.setVisibility(View.GONE);
			emptyTxtView.setVisibility(View.VISIBLE);
		    }
		}
	    });
	}

    }

    @Override
    public void displayError(String title, String message)
    {

    }

    public void setTitle(String title)
    {
	this.title = title;
    }

    @Override
    public void requestData()
    {
	RebateManager.getInstance().checkAndrequestData(this);
    }

    @Override
    public void getUiControls(View root)
    {
	equipmentListView = (ListView) root.findViewById(R.id.equipment_list);
	emptyTxtView = (TextView) root.findViewById(R.id.empty_text);
    }

    @Override
    public void onDestroyView()
    {
	if (shouldComputeRebate)
	{
	    float rebateValue = computeRebate();
	    RebateManager.getInstance().getFormBean().setQIVRebate(rebateValue);
	}
	super.onDestroyView();
    }

    @Override
    public void onResume()
    {
	super.onResume();
	shouldComputeRebate = true;
    }

    private float computeRebate()
    {
	int count = qivAAdapter.getCount();
	float rebateValue = 0;
	for (int i = 0; i < count; i++)
	{
	    QIVItem item = qivData.get(i);
	    rebateValue += item.getRebateValue();
	}
	return rebateValue;
    }

    @Override
    public void initialize(View root)
    {
	qivAAdapter = new QIVAdapter(getActivity(), 0, qivData);
	qivAAdapter.setAdapterListener(qivAAdapter);
	equipmentListView.setAdapter(qivAAdapter);
	qivRebateCalculation = new QIVRebateCalculation();
	RootActivity rootAct = (RootActivity) getActivity();
	if (rootAct != null)
	{
	    rootAct.updateTitle(title);
	}

    }

    @Override
    public void bindCallbacks(View root)
    {
	equipmentListView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
	if (qivData.size() > position)
	{
	    // Add crashylitcs for qiv type
	    if (Fabric.isInitialized())
	    {
		String str = "";
		Crashlytics.setString("selectedEquipmentName", str = (qivData.get(position) == null ? "null" : qivData.get(position).getName()));
		Log.d("QIV", "Setting crashlytics custom tag selectedEquipmentName=" + str);
	    }
	    QIVFragment qivFrag = QIVFragment.newInstance(qivData.get(position), qivRebateCalculation);
	    shouldComputeRebate = false;
	    ((RootActivity) getActivity()).beginTransaction().replace(R.id.mainLayout, qivFrag).addToBackStack(null).commitAllowingStateLoss();
	}
    }

}
