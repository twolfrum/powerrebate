package com.icf.rebate.ui.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.net.Uri;
import android.support.v4.content.FileProvider;
import android.util.Log;

import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.ameren.rebate.ui.R;

public class LoggerUtils
{

    public static String TAG = LoggerUtils.class.getSimpleName();

    public static long APPLOG_FILE_SIZE = 5242880L; // 5MB limit

    // public static String APPLOG_DIR_PATH = Environment.getExternalStorageDirectory() + "/download/";

    public static String APPLOG_DIR_PATH;

    public static String APPLOG_FIRST_FILE_NAME = "ICFLogcat_first.txt";

    public static String APPLOG_SECOND_FILE_NAME = "ICFLogcat_second.txt";

    public static String ZIPPED_LOG_FILENAME = "icflogs.zip";

    public static Timer logMonitorTimer;

    public static LogCleanupTask logMonitorTask;

    public static final long LOG_MONITOR_INTERVAL = 5 * 60000l;

    // @SuppressWarnings("unused")
    private static Process appLoggingProcess;

    private static Process appLogClearTunnelProcess;

    private static LoggerUtils loggerUtils = new LoggerUtils();

    public static LoggerUtils getInstance()
    {
	return loggerUtils;
    }

    private void setAppLogDirectoryPath(Context context)
    {
	// String state = Environment.getExternalStorageState();
	// if (Environment.MEDIA_MOUNTED.equals(state) && !isInKnoxMode)
	// {
	// APPLOG_DIR_PATH = Environment.getExternalStorageDirectory() +
	// "/download/";
	// }
	// else
	// {
	// APPLOG_DIR_PATH = context.getExternalFilesDir(null).getPath();
	// }

	APPLOG_DIR_PATH = context.getFilesDir().getAbsolutePath();
	File file = new File(APPLOG_DIR_PATH + "/logfile/");
	if (!file.exists())
	{
	    file.mkdir();
	}
	APPLOG_DIR_PATH += "/logfile/";
	ICFLogger.d(ICFLogger.TAG, " APPLOG_DIR_PATH: " + APPLOG_DIR_PATH);
    }

    public void setupAppLogging(Context context) throws IOException
    {
	setAppLogDirectoryPath(context);
	scheduleLogFileMonitoring(context);

	String newFileName = APPLOG_FIRST_FILE_NAME;
	String currentFileName = LibUtils.getCurrentLogFile();

	if (currentFileName == null)
	{
	    currentFileName = APPLOG_FIRST_FILE_NAME;
	}

	File file = new File(APPLOG_DIR_PATH, currentFileName);

	if (file != null && !file.exists())
	{
	    file.createNewFile();
	}

	if (!isLogSizeExceeded(file))
	{
	    newFileName = currentFileName;
	}
	else if (currentFileName.equalsIgnoreCase(APPLOG_FIRST_FILE_NAME))
	{
	    newFileName = APPLOG_SECOND_FILE_NAME;
	}

	setLogFile(context, newFileName, currentFileName, file);
    }

    private void setLogFile(Context context, String newFileName, String currentFileName, File file) throws IOException
    {
	if (!newFileName.equalsIgnoreCase(currentFileName))
	{
	    file = new File(APPLOG_DIR_PATH, newFileName);
	    if (file != null && !file.exists())
	    {
		file.createNewFile();
	    }

	    if (isLogSizeExceeded(file) && file.delete())
	    {
		ICFLogger.d(ICFLogger.TAG, "Logfile " + newFileName + " size exeeded 5MB");
		file = new File(APPLOG_DIR_PATH, newFileName);
		if (file != null && !file.exists())
		{
		    file.createNewFile();
		}
	    }

	    currentFileName = newFileName;

	}

	LibUtils.setCurrentLogFile(currentFileName);
	logFiledetails(file, currentFileName);

	cleanupLoggingProcess();
	appLogClearTunnelProcess = Runtime.getRuntime().exec("logcat -c");
	appLoggingProcess = Runtime.getRuntime().exec("logcat -v time -f " + file.getAbsolutePath());
    }

    public void deleteAppLog(Context context)
    {
	File outputFile = new File(APPLOG_DIR_PATH, APPLOG_SECOND_FILE_NAME);
	if (outputFile.exists())
	{
	    outputFile.delete();
	}

	outputFile = new File(APPLOG_DIR_PATH, APPLOG_FIRST_FILE_NAME);
	if (outputFile.exists())
	{
	    outputFile.delete();
	}

	outputFile = new File(APPLOG_DIR_PATH, ZIPPED_LOG_FILENAME);
	if (outputFile.exists())
	{
	    outputFile.delete();
	}

	LibUtils.setCurrentLogFile(null);
    }

    public void cleanupLoggingProcess()
    {
	if (appLoggingProcess != null)
	{
	    appLoggingProcess.destroy();
	    appLoggingProcess = null;
	}

	if (appLogClearTunnelProcess != null)
	{
	    appLogClearTunnelProcess.destroy();
	    appLogClearTunnelProcess = null;
	}
    }

    public void intiateLogDump(final Context context, final String[] emailAddress)
    {
	final String currentFileName = LibUtils.getCurrentLogFile();
	if (currentFileName == null)
	{
	    ICFLogger.d(ICFLogger.TAG, "IntiateLogDump currentFileName is null");
	    return;
	}
	Thread thread = new Thread(new Runnable()
	{
	    public void run()
	    {
		android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
		// cleanupLoggingProcess();
		File appLogs = new File(APPLOG_DIR_PATH, currentFileName);

		if (appLogs.exists())
		{
		    try
		    {
			Resources res = context.getResources();

			String subject = String.format(res.getString(R.string.report_email_subject), res.getString(R.string.app_name)) + ": " + context.getResources().getString(R.string.app_version) + context.getResources().getString(R.string.report_email_build) + ": " + context.getResources().getString(R.string.build_version);
			sendOrUploadEmail(context, emailAddress, subject);

		    }
		    catch (FileNotFoundException e)
		    {
			e.printStackTrace();
		    }
		    catch (IOException e)
		    {
			e.printStackTrace();
		    }
		}
		if (UiUtil.rootActivity != null)
		{
		    Activity activity = UiUtil.rootActivity.get();
		    if (activity != null)
		    {
			activity.runOnUiThread(new Runnable()
			{

			    @Override
			    public void run()
			    {
				UiUtil.dismissSpinnerDialog();
			    }
			});
		    }
		}
	    }
	});

	thread.start();

    }

    private void sendOrUploadEmail(Context context, String[] emailAddress, String subject) throws IOException
    {
	String emailBody = null;
	String zipFilePath = APPLOG_DIR_PATH + File.separator + ZIPPED_LOG_FILENAME;
	FileUtil.doZipOnlyFiles(APPLOG_DIR_PATH, zipFilePath);

	PackageManager pm = context.getPackageManager();
	Intent intent = new Intent(Intent.ACTION_SENDTO);
	// intent.setType("*/*");
	intent.setType("text/plain");
	intent.setData(Uri.parse("mailto:"));

	emailBody = context.getResources().getString(R.string.report_email_content);
	// Uri uri = Uri.fromFile(new File(zipFilePath));
	Uri uri = FileProvider.getUriForFile(context, "com.icf.ameren.rebate.ui.fileprovider", new File(zipFilePath));
	// Uri.parse("content://" + DatabaseManager.AUTHORITY + "/" + ZIPPED_LOG_FILENAME);
	// intent.putExtra(Intent.EXTRA_STREAM, uri);
	// }
	ArrayList<LabeledIntent> intentList = new ArrayList<LabeledIntent>();

	List<ResolveInfo> resInfo = pm.queryIntentActivities(intent, 0);

	for (int i = 0; i < resInfo.size(); i++)
	{
	    // Extract the label, append it, and repackage it in a LabeledIntent
	    ResolveInfo ri = resInfo.get(i);
	    String packageName = ri.activityInfo.packageName;
	    Intent tempIntent = new Intent(Intent.ACTION_SEND);
	    tempIntent.setComponent(new ComponentName(packageName, ri.activityInfo.name));
	    tempIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	    tempIntent.putExtra(Intent.EXTRA_EMAIL, emailAddress);
	    tempIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
	    tempIntent.putExtra(Intent.EXTRA_TEXT, emailBody);
	    tempIntent.putExtra(Intent.EXTRA_STREAM, uri);
	    context.grantUriPermission(packageName, uri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
	    // message/rfc822" is used to inform Intent.ACTION_SEND which intents to be invoked
	    tempIntent.setType("message/rfc822");
	    // tempIntent.setType("*/*");
	    tempIntent.setPackage(packageName);
	    intentList.add(new LabeledIntent(tempIntent, packageName, ri.loadLabel(pm), ri.icon));

	}
	if (!intentList.isEmpty())
	{
	    // convert intentList to array
	    Intent openInChooser = Intent.createChooser(intentList.remove(0), "Send Email Using : ");
	    LabeledIntent[] extraIntents = intentList.toArray(new LabeledIntent[intentList.size()]);
	    openInChooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents);
	    context.startActivity(openInChooser);
	}
	else
	{
	    Log.d(TAG, "No email client found.");
	}
	// String mailParticipants = new String(reportIssueMailID);
	// intent.setData(Uri.parse("mailto: " + mailParticipants));
	// intent.putExtra(Intent.EXTRA_SUBJECT, subject);
	// intent.putExtra(Intent.EXTRA_TEXT, emailBody);
	// intent.putExtra(android.content.Intent.EXTRA_CC, ccAddress);
	// Intent newIntent = Intent.createChooser(intent, context.getResources().getString(R.string.send_email_using) + ": ");
	// newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	// context.startActivity(newIntent);

    }

    private boolean isLogSizeExceeded(File appLogs)
    {
	return appLogs != null && appLogs.exists() && appLogs.length() >= APPLOG_FILE_SIZE;
    }

    private void logFiledetails(File appLogs, String filename)
    {
	if (appLogs != null && appLogs.exists())
	{
	    long length = appLogs.length();
	    ICFLogger.d(ICFLogger.TAG, "File " + filename + " Log Size : " + readableFileSize(length));
	}
    }

    private String readableFileSize(long size)
    {
	if (size <= 0)
	    return "0";
	final String[] units = new String[] { "B", "KB", "MB", "GB", "TB" };
	int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
	return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    public void scheduleLogFileMonitoring(Context context)
    {
	ICFLogger.d(ICFLogger.TAG, "Started Log Monitoring !");
	cancelTimerTask();
	logMonitorTimer = new Timer("logmonitor");
	logMonitorTask = new LogCleanupTask(context);
	logMonitorTimer.scheduleAtFixedRate(logMonitorTask, 0, LOG_MONITOR_INTERVAL);
    }

    private static void cancelTimerTask()
    {

	if (logMonitorTimer != null)
	{
	    logMonitorTimer.purge();
	    logMonitorTimer.cancel();
	    logMonitorTimer = null;
	}

	if (logMonitorTask != null)
	{
	    logMonitorTask.cancel();
	    logMonitorTask = null;
	}
    }

    static class LogCleanupTask extends TimerTask
    {
	private Context context;

	public LogCleanupTask(Context context)
	{
	    this.context = context;
	}

	@Override
	public void run()
	{
	    if (loggerUtils != null)
	    {
		String currentFileName = LibUtils.getCurrentLogFile();
		if (currentFileName != null)
		{
		    File file = new File(APPLOG_DIR_PATH, currentFileName);
		    if (file != null && file.exists())
		    {
			ICFLogger.d(ICFLogger.TAG, "Check log file capacity");
		    }
		    if (loggerUtils.isLogSizeExceeded(file))
		    {
			String newFileName = APPLOG_FIRST_FILE_NAME;

			if (currentFileName.equalsIgnoreCase(APPLOG_FIRST_FILE_NAME))
			{
			    newFileName = APPLOG_SECOND_FILE_NAME;
			}
			try
			{
			    ICFLogger.d(ICFLogger.TAG, "setLogFile");
			    loggerUtils.setLogFile(context, newFileName, currentFileName, file);
			}
			catch (IOException e)
			{
			    e.printStackTrace();
			}
		    }

		}
	    }
	}
    }
}
