package com.icf.rebate.ui.customviews;

/**
 * Created by adminpc on 17/11/14.
 */
public class Line
{
    private float startX;
    private float startY;
    private float stopX;
    private float stopY;

    public Line(float startX, float startY, float stopX, float stopY)
    {
        this.startX = startX;
        this.startY = startY;
        this.stopX = stopX;
        this.stopY = stopY;
    }

    public float getStartY()
    {
        return startY;
    }

    public void setStartY(float startY)
    {
        this.startY = startY;
    }

    public float getStopX()
    {
        return stopX;
    }

    public void setStopX(float stopX)
    {
        this.stopX = stopX;
    }

    public float getStopY()
    {
        return stopY;
    }

    public void setStopY(float stopY)
    {
        this.stopY = stopY;
    }

    public float getStartX()
    {

        return startX;
    }

    public void setStartX(float startX)
    {
        this.startX = startX;
    }
}
