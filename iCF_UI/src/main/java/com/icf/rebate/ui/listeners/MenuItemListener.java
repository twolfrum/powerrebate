package com.icf.rebate.ui.listeners;

import com.icf.rebate.app.model.MenuItem;

import android.view.View;

public interface MenuItemListener
{
    public void onFinishInflate(int layoutId, View view, MenuItem menuItem);
    
}
