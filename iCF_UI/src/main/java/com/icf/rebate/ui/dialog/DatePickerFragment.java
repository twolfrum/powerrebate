package com.icf.rebate.ui.dialog;

import com.icf.rebate.ui.controller.DatePickWatcher;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

public class DatePickerFragment extends DialogFragment {
    
    private DatePickWatcher pickWatcher;

    public DatePickerFragment() {
        super();
    }
    
    public void setPickWatcher(DatePickWatcher pickWatcher)
    {
        this.pickWatcher = pickWatcher;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Create a new instance of DatePickerDialog and return it
        DatePickerDialog dlg = new DatePickerDialog(getActivity(), 
        	pickWatcher, 
        	getArguments().getInt("initYear"), 
        	getArguments().getInt("initMonth"),
        	getArguments().getInt("initDay"));
        
        DatePicker picker = dlg.getDatePicker();
        long rangeMarker = getArguments().getLong("minDate");
        if (rangeMarker > 0) picker.setMinDate(rangeMarker);
        rangeMarker = getArguments().getLong("maxDate");
        if (rangeMarker > 0) picker.setMaxDate(rangeMarker);
        
        return dlg;
    }

}
