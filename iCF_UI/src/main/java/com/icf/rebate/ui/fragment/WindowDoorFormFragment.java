/**
 * 
 */
package com.icf.rebate.ui.fragment;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.adapter.CustomSpinnerAdapter;
import com.icf.rebate.adapter.InsulationPageAdapter;
import com.icf.rebate.adapter.WDPageAdapter;
import com.icf.rebate.networklayer.model.CustomerInfo;
import com.icf.rebate.networklayer.model.FormResponseBean;
import com.icf.rebate.networklayer.model.FormResponseBean.Insulation;
import com.icf.rebate.networklayer.model.FormResponseBean.InsulationItem;
import com.icf.rebate.networklayer.model.FormResponseBean.WDItem;
import com.icf.rebate.networklayer.model.FormResponseBean.WindowAndDoor;
import com.icf.rebate.ui.RootActivity;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.customviews.CustomPageIndicator;
import com.icf.rebate.ui.customviews.CustomViewPager;
import com.icf.rebate.ui.dialog.DialogListener;
import com.icf.rebate.ui.dialog.ICFDialogFragment;
import com.icf.rebate.ui.listeners.FragmentCallFlow;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.FileUtil;
import com.icf.rebate.ui.util.UiUtil;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

/**
 * @author Ken Butler Dec 17, 2015 WindowDoorFormFragment.java Copyright (c) 2015 ICF International
 */
public class WindowDoorFormFragment extends Fragment
	implements FragmentCallFlow, OnPageChangeListener, OnItemSelectedListener, OnClickListener, DialogListener, PersistentForm
{

    private static final String TAG = "WindowDoorFragment";
    private FormResponseBean mBean;
    public CustomViewPager mViewPager; // For multiple replacements
    private Spinner mFormSelectComboBox;
    private RootActivity mHomeScreenActivity;
    private int mWDIndex;
    private int mPageIndex;
    private WindowAndDoor mWindowAndDoor;
    private WDPageAdapter mPageAdapter; // TODO Change this
    private ICFDialogFragment mCommonDialog;

    public WindowDoorFormFragment()
    {
	super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
	View view = inflater.inflate(R.layout.fragment_form_insulation, container, false); // can reuse insulation layout here they are the same
	return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
	super.onViewCreated(view, savedInstanceState);
	mBean = RebateManager.getInstance().getFormBean();
	getUiControls(view);
	initialize(view);
	bindCallbacks(view);
	requestData();

	CustomerInfo cInfo = RebateManager.getInstance().getFormBean().getCustomerInfo();
	if (UiUtil.isJobCompleted(cInfo))
	{
	    ArrayList<View> list = new ArrayList<View>();
	    list.add(mViewPager);
	    UiUtil.setEnableView((ViewGroup) view, false, list);
	}
    }

    @Override
    public void getUiControls(View root)
    {
	mFormSelectComboBox = (Spinner) root.findViewById(R.id.insulation_selector);
	mViewPager = (CustomViewPager) root.findViewById(R.id.pager);
    }

    @Override
    public void requestData()
    {
	updateWDViews();

    }

    private void updateWDViews()
    {
	if (mBean == null || mWindowAndDoor == null)
	{
	    return;
	}
	mPageAdapter.setWindowAndDoor(mWindowAndDoor);
	mPageAdapter.notifyDataSetChanged();

    }

    @Override
    public void onDismiss()
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void onClick(View v)
    {
	if (v != null)
	{
	    WDPageFragment frag = mPageAdapter.getCurrentFragment(mWindowAndDoor.getItems().get(mPageIndex));
	    if (mPageAdapter != null && frag != null)
	    {
		EditText editField = frag.getLastFocusedEditField();
		if (editField != null)
		{
		    UiUtil.hideSoftKeyboard(getActivity(), editField);
		    editField.clearFocus();
		}
	    }

	    int id = v.getId();
	    if (id == R.id.save_btn)
	    {
		mViewPager.clearFocus();
		saveForm(frag, mPageIndex, true);
	    }
	    else if (id == R.id.close_btn)
	    {
		mCommonDialog = ICFDialogFragment.newDialogFrag(R.layout.delete_confirmation);
		mCommonDialog.setDialogListener(this);
		mCommonDialog.show(getActivity().getSupportFragmentManager(), "Delete Confirmation");
		getActivity().getSupportFragmentManager().executePendingTransactions();
	    }
	    else if (id == R.id.add_btn)
	    {
		if (mBean != null && mWindowAndDoor != null && mPageAdapter != null
				&& mWindowAndDoor.getItems() != null
				//&& mWindowAndDoor.getItems().size() < AppConstants.MAX_PAGE_SUPPORT
			    && mWindowAndDoor.getItems().size() < mWindowAndDoor.getMaxMeasures()  )
		{
		    try
		    {
			WDItem newItem = mWindowAndDoor.createNewItem();
			addNewItem(newItem);

			mPageAdapter.notifyDataSetChanged();
			mViewPager.setOffscreenPageLimit(mWindowAndDoor.getItems().size());
			mViewPager.firePageCountChanged();
			mViewPager.setCurrentItem(mWindowAndDoor.getItems().size() - 1);
			updateSpinnerAdapter();
		    }
		    catch (CloneNotSupportedException e)
		    {
			e.printStackTrace();
		    }
		}
		else
		{
			UiUtil.showError(getActivity(), getResources().getString(R.string.alert_title),
					getResources().getString(R.string.maximum_measures_limit,
					mWindowAndDoor.getName(), mWindowAndDoor.getMaxMeasures()));
		}
	    }
	}
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
	mViewPager.setCurrentItem(position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void onPageScrollStateChanged(int arg0)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void onPageSelected(int index)
    {
	mPageIndex = index;
	mFormSelectComboBox.post(new Runnable()
	{

	    @Override
	    public void run()
	    {
		mFormSelectComboBox.setSelection(mViewPager.getCurrentItem(), true);
	    }
	});
    }

    public String getPageName()
    {
	WDPageFragment fragm = (WDPageFragment) mPageAdapter.getItem(mPageIndex);
	return fragm.getItem().getPageNum() + "";
    }

    @Override
    public void initialize(View root)
    {
	mHomeScreenActivity = (RootActivity) getActivity();
	if (mHomeScreenActivity == null)
	{
	    return;
	}
	if (getArguments() != null && getArguments().containsKey("index"))
	{
	    mWDIndex = getArguments().getInt("index");
	}

	if (mBean != null && mBean.getWindowAndDoor() != null && mBean.getWindowAndDoor().size() > mWDIndex)
	{
	    mWindowAndDoor = mBean.getWindowAndDoor().get(mWDIndex);
	}

	if (mBean != null)
	{
	    updateSpinnerAdapter();
	    // qivList = mBean.getQivList();
	}

	mPageAdapter = new WDPageAdapter(getChildFragmentManager(), mViewPager);
	mViewPager.setAdapter(mPageAdapter);
	mViewPager.setOnPageChangeListener(this);
	mViewPager.setCurrentItem(0);
	if (mWindowAndDoor.getItems() != null)
	{
	    mViewPager.setOffscreenPageLimit(mWindowAndDoor.getItems().size());
	}
	CustomPageIndicator indicator = ((CustomPageIndicator) getActivity().findViewById(R.id.page_indicator));
	mViewPager.setPageIndicator(indicator);
	indicator.setPager(mViewPager);

	mHomeScreenActivity.currenFragment = TAG;
    }

    private void updateSpinnerAdapter()
    {
	if (mWindowAndDoor != null)
	{
	    List<WDItem> itemList = mWindowAndDoor.getItems();
	    ArrayList<String> list = new ArrayList<String>();
	    if (itemList != null && itemList.size() > 0)
	    {
		for (int i = 0; i < itemList.size(); i++)
		{
		    list.add(mWindowAndDoor.getName() + " " + (i + 1));
		}
	    }
	    else
	    {
		list.add(mWindowAndDoor.getName() + "1");
	    }

	    CustomSpinnerAdapter spinnerAdapter = new CustomSpinnerAdapter(getActivity(), 0, list);
	    mFormSelectComboBox.setAdapter(spinnerAdapter);
	    mFormSelectComboBox.setSelection(mPageIndex, true);
	}
    }

    @Override
    public void bindCallbacks(View root)
    {
	root.findViewById(R.id.save_btn).setOnClickListener(this);
	root.findViewById(R.id.close_btn).setOnClickListener(this);
	root.findViewById(R.id.add_btn).setOnClickListener(this);
	mFormSelectComboBox.setOnItemSelectedListener(this);
    }

    public List<WDItem> getWDItems()
    {
	if (mWindowAndDoor != null)
	{
	    return mWindowAndDoor.getItems();
	}
	else
	{
	    return null;
	}

    }

    private synchronized void deleteItem()
    {
	if (mBean != null && mWindowAndDoor != null && mPageAdapter != null)
	{
	    WDItem newItem = null;
	    try
	    {
		newItem = mWindowAndDoor.createNewItem();
	    }
	    catch (CloneNotSupportedException e)
	    {
		e.printStackTrace();
	    }
	    WDPageFragment frag = mPageAdapter.getCurrentFragment(mWindowAndDoor.getItems().get(mPageIndex));
	    final int selectedPageNum = mWindowAndDoor.getItems().get(mPageIndex).getPageNum();
	    Log.v(TAG, "deleteItem " + mPageIndex);
	    if (frag != null)
	    {
		frag.setFragmentDeleted();
	    }
	    // deleteQIVItem();
	    // mViewPager.setCurrentItem(equipmentIndex);
	    WDItem deletedItem = mPageAdapter.deleteItem(mPageIndex);

	    if (deletedItem != null)
	    {
		float rebateValue = mBean.getWindowDoorListRebate();
		rebateValue -= deletedItem.getRebateValue();
		mBean.setWindowDoorListRebate(rebateValue);
	    }

	    Thread t = new Thread(new Runnable()
	    {
		public void run()
		{
		    String name = mWindowAndDoor.getId();
		    FileUtil.deleteFolderPartImage(name, selectedPageNum + "");
		}
	    });
	    t.start();

	    if (newItem != null && mWindowAndDoor.getItems().size() == 0)
	    {
		addNewItem(newItem);
	    }
	    mPageAdapter.notifyDataSetChanged();
	    mViewPager.setOffscreenPageLimit(mWindowAndDoor.getItems().size());
	    mViewPager.firePageCountChanged();

	    if (mPageIndex >= mWindowAndDoor.getItems().size())
	    {
		--mPageIndex;
	    }
	    mViewPager.setCurrentItem(mPageIndex);
	    updateSpinnerAdapter();
	}
	mCommonDialog.dismiss();
    }

    @Override
    public void onFinishInflate(int layoutId, View view)
    {
	if (layoutId == R.layout.equipment_image_dialog_layout && view != null)
	{
	    // final Part part = (Part) uiManager.getDialog().getDataObj();
	    // partImageLayout = view.findViewById(R.id.image_layout);
	    // partImageName = (TextView) view.findViewById(R.id.image_name);
	    // partImagePreview = (ImageView) view.findViewById(R.id.image_preview);
	    //
	    // if (part != null && part.getImagePath() != null)
	    // {
	    // updatePartImageView(part);
	    // }
	    // else
	    // {
	    // partImageLayout.setVisibility(View.GONE);
	    // }
	    //
	    // view.findViewById(R.id.upload_image).setOnClickListener(new OnClickListener()
	    // {
	    // @Override
	    // public void onClick(View v)
	    // {
	    // Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
	    // if (part != null)
	    // {
	    // takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(FileUtil.getFilePathForPartImage(part, mPageIndex))));
	    // // takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
	    // }
	    //
	    // startActivityForResult(takePictureIntent, AppConstants.CAPTURE_PHOTO_REQUEST_CODE);
	    // }
	    // });

	}
	else if (layoutId == R.layout.delete_confirmation && view != null)
	{
	    ImageButton cancel = (ImageButton) view.findViewById(R.id.delete_cancel_btn);
	    ImageButton done = (ImageButton) view.findViewById(R.id.delete_done_btn);
	    cancel.setOnClickListener(new OnClickListener()
	    {

		@Override
		public void onClick(View v)
		{
		    mCommonDialog.dismiss();
		}
	    });
	    done.setOnClickListener(new OnClickListener()
	    {

		@Override
		public void onClick(View v)
		{
		    deleteItem();
		}

	    });
	}
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
	super.onActivityResult(requestCode, resultCode, data);

	if (requestCode == AppConstants.CAPTURE_PHOTO_REQUEST_CODE && resultCode == FragmentActivity.RESULT_OK)
	{
	    WDPageFragment frag = mPageAdapter.getCurrentFragment(mWindowAndDoor.getItems().get(mPageIndex));
	    if (mPageAdapter != null && frag != null)
	    {
		int pageNum = mWindowAndDoor.getItems().get(mPageIndex).getPageNum();
		frag.updateCaptureImage(pageNum);
	    }
	}
    }

    private boolean deleteItemAtIndex(int mPageIndex)
    {
	boolean retVal = false;

	if (mWindowAndDoor.getItems() != null && mWindowAndDoor.getItems().size() > mPageIndex && mWindowAndDoor.getItems().size() > 1)
	{
	    final int selectedPageNum = mWindowAndDoor.getItems().get(mPageIndex).getPageNum();
	    retVal = mPageAdapter.deleteItem(mPageIndex) != null;

	    Thread t = new Thread(new Runnable()
	    {
		public void run()
		{
		    String equipmentName = mWindowAndDoor.getId();
		    FileUtil.deleteFolderPartImage(equipmentName, selectedPageNum + "");
		}
	    });
	    t.start();
	}
	return retVal;
    }

    private WDItem getItemAtIndex(int mPageIndex)
    {
	WDItem item = null;

	if (mWindowAndDoor.getItems() != null && mWindowAndDoor.getItems().size() > mPageIndex)
	{
	    item = mWindowAndDoor.getItems().get(mPageIndex);
	}
	return item;
    }

    @Override
    public void onDestroyView()
    {
	super.onDestroyView();

	setValidState();
    }
    
    @Override
    public int getNoScreensToToolList()
    {
	return 2;
    }

    @Override
    public void setValidState()
    {
	if (mWindowAndDoor != null && mWindowAndDoor.getItems() != null)
	{
	    Iterator<WDItem> itemsIterator = mWindowAndDoor.getItems().listIterator();
	    while (itemsIterator.hasNext())
	    {
		WDItem item = (WDItem) itemsIterator.next();
		if (item.isItemSaved())
		{
		    if (item != null && item.getFormFilledState() == AppConstants.FORM_ITEM_FILLED_STATE.ITEM_COMPLETELY_SAVED.getState())
		    {
			mWindowAndDoor.setMandatoryFieldsComplete(true);
		    }
		    else
		    {
			mWindowAndDoor.setMandatoryFieldsComplete(false);
		    }
		}
		else
		{
		    mWindowAndDoor.setMandatoryFieldsComplete(true);
		}
	    }
	}
	
    }

    @Override
    public boolean isValid()
    {
	setValidState();
	return mWindowAndDoor.isMandatoryFieldsComplete();
    }

    @Override
    public void saveForm(boolean isManualSave)
    {
	WDPageFragment frag = mPageAdapter.getCurrentFragment(mWindowAndDoor.getItems().get(mPageIndex));
	saveForm(frag, mPageIndex, isManualSave);
    }

    public synchronized void saveForm(WDPageFragment frag, int mPageIndex, boolean manualSave)
    {
	if (mPageAdapter != null)
	{
	    boolean isItemSaved = mPageAdapter.setItemSaved(mPageIndex, manualSave);

	    if (!manualSave && !isItemSaved)
	    {
		deleteItemAtIndex(mPageIndex);
		return;
	    }

	    WDItem item = getItemAtIndex(mPageIndex);
	    // createQIVItem(item, mPageIndex);
	    double oldRebate = mWindowAndDoor.getRebateValue();
	    double val = 0f;

	    if (mPageAdapter != null && frag != null)
	    {
		val = mWindowAndDoor.computeRebate(item, frag.getInstalledUVal(), frag.getSHGCVal(), frag.getNumInstalledVal());
		frag.updateRebate(val);
	    }

	    Toast.makeText(getActivity(), getString(R.string.saved_string), Toast.LENGTH_LONG).show();
	    float rebate = mBean.getWindowDoorListRebate();
	    rebate -= oldRebate;
	    rebate += mWindowAndDoor.getRebateValue();
	    mBean.setWindowDoorListRebate(rebate);
	    RebateManager.getInstance().updateFormState(null);
	}
    }

    private void addNewItem(WDItem newItem)
    {
	if (mWindowAndDoor.getItems().size() > 0)
	{
	    WDItem lastItem = mWindowAndDoor.getItems().get(mWindowAndDoor.getItems().size() - 1);
	    newItem.setPageNum(lastItem.getPageNum() + 1);
	}
	mWindowAndDoor.addNewItem(newItem);
    }
}
