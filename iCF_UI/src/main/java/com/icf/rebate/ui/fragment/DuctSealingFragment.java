package com.icf.rebate.ui.fragment;

import java.util.List;
import java.util.Map;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.icf.rebate.app.model.DuctSealingItem;
import com.icf.rebate.networklayer.model.CustomerInfo;
import com.icf.rebate.networklayer.model.FormResponseBean.Options;
import com.icf.rebate.networklayer.model.FormResponseBean.Part;
import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.ui.controller.FocusManager;
import com.icf.rebate.ui.controller.FormUIManager;
import com.icf.rebate.ui.controller.FormUIManager.FormViewCreationCallback;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.listeners.RequestCallFlow;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.ICFLogger;
import com.icf.rebate.ui.util.UiUtil;
import com.icf.rebate.ui.util.UiViewUtils;
import com.icf.rebate.ui.util.rebate.DuctSealingRebateCalculation;
import com.icf.rebate.ui.util.rebate.ProjectCostRebateCalculation;

public class DuctSealingFragment extends Fragment implements RequestCallFlow, FormViewCreationCallback {
    // private UIElementManager serverLayoutInflater;
    public static final String TAG = DuctSealingFragment.class.getSimpleName();
    private FormUIManager uiManager;
    private LinearLayout scrollViewLayout;
    private DuctSealingItem ductSealingItem;
    private EditText testIn, testOut, leakageImprovement, totalLeakage, eligibility,
                postMeasuredAirFlow, result, projectCost, quantity;
    private String testInValue, testOutValue, postMeasuredAirFlowId,
                projectCostValue, quantityValue;
    private float tonnageValue = 0;
    private DuctSealingRebateCalculation measurementBasedRebateCalculation;
    private ProjectCostRebateCalculation costBasedRebateCalculation;
    private Spinner tonnage;
    private boolean isDeleted = false;
    private String title;
    private String rebateType;
    private String rebateCalcType;

    public DuctSealingFragment() {
        super();
    }

    public static DuctSealingFragment newInstance(DuctSealingItem ductSealingItem,
                                            String rebateType, String rebateCalcType) {
        DuctSealingFragment frag = new DuctSealingFragment();
        frag.ductSealingItem = ductSealingItem;
        frag.rebateType = rebateType;
        frag.rebateCalcType = rebateCalcType;

        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.duct_sealing_lyt, null, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getUiControls(view);
        initalize();
        createViews();

        CustomerInfo cInfo = RebateManager.getInstance().getFormBean().getCustomerInfo();
        if (UiUtil.isJobCompleted(cInfo)) {
            UiUtil.setEnableView((ViewGroup) view, false, null);
        }
        FocusManager.newFocusManager(view);
    }

    private void getUiControls(View view) {
        scrollViewLayout = (LinearLayout) view.findViewById(R.id.duct_scrol_layout);
    }

    protected void initalize() {
        uiManager = new FormUIManager(getString(R.string.ductsealingId));
        uiManager.setFormViewCreationCallback(this);
        uiManager.setEquipmentName(getActivity().getResources().getString(R.string.ductsealingId));

        if (!TextUtils.isEmpty(rebateCalcType) &&
                rebateCalcType.equals(ProjectCostRebateCalculation.CALC_TYPE))
            costBasedRebateCalculation = new ProjectCostRebateCalculation();
        else {
            //default duct sealing calculation
            measurementBasedRebateCalculation = new DuctSealingRebateCalculation();
        }
    }

    private void createViews() {
        List<Part> partList = null;
        if (ductSealingItem != null && ductSealingItem.getSealingDetail().getParts() != null) {
            partList = ductSealingItem.getSealingDetail().getParts();
        }

        if (partList != null) {
            boolean isFormFilled = ductSealingItem.getFormFilledState() != AppConstants.FORM_ITEM_FILLED_STATE.ITEM_UNEDITED.getState();
            scrollViewLayout.removeAllViews();
            uiManager.createViews(getParentFragment(), scrollViewLayout, partList, isFormFilled);
        }
    }

    @Override
    public void showProgressDialog() {
        // TODO Auto-generated method stub

    }

    @Override
    public void stopProgressDialog() {
        // TODO Auto-generated method stub

    }

    @Override
    public void updateData(Object obj) {

    }

    @Override
    public void displayError(String title, String message) {

    }

    public DuctSealingItem getDuctSealingItem() {
        return ductSealingItem;
    }

    @Override
    public void viewCreated(final View view, Part part, final Options option) {
        if (tonnage == null || testIn == null || testOut == null ||
                postMeasuredAirFlow == null || leakageImprovement == null ||
                totalLeakage == null || eligibility == null || result == null ||
                projectCost == null || quantity == null)
        {
            if (part != null) {
                String name = part.getId();
                if (name == null) name = part.getName();
                if (name != null && name.equalsIgnoreCase(AppConstants.TONNAGE_ID)) {
                    if (view instanceof Spinner) {
                        tonnage = (Spinner) view;
                        tonnage.setOnItemSelectedListener(new OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                String selectedValue = (String) ((TextView) view).getText();
                                if (selectedValue != null && option != null && !selectedValue.equalsIgnoreCase(option.getSavedValue())) {
                                    if (!selectedValue.equalsIgnoreCase(AppConstants.COMBO_SELECT_VALUE)) {
                                        tonnageValue = LibUtils.parseFloat(selectedValue, 0);
                                        if (testOut != null && testOut.getText().toString().trim().length() > 0 && testIn != null && testIn.getText().toString().trim().length() > 0 && postMeasuredAirFlow != null && postMeasuredAirFlow.getText().toString().trim().length() > 0) {
                                            float rebateValue = measurementBasedRebateCalculation.getRebateValue(testIn.getText().toString(), testOut.getText().toString(), postMeasuredAirFlow.getText().toString(), tonnageValue);
                                            updateDuctSealingDetails(rebateValue);
                                        }
                                        option.setSavedValue(selectedValue);
                                    } else {
                                        option.setSavedValue(null);
                                    }
                                }

                                if (UiViewUtils.isValidationMode(option.isItemSaved()) && option.isMandatory() && !option.isFieldFilled()) {
                                    ((View) view.getParent()).setBackground(getActivity().getResources().getDrawable(R.drawable.bg_dialog_mandatory_box));
                                } else {
                                    ((View) view.getParent()).setBackground(getActivity().getResources().getDrawable(R.drawable.combo_box_bg));
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {}
                        });
                    }
                } else if (name != null && name.equalsIgnoreCase(AppConstants.TEST_IN_ID)) {
                    if (view instanceof EditText) {
                        testIn = (EditText) view;
                        testIn.setInputType(InputType.TYPE_CLASS_NUMBER);
                        testIn.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {}
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                            @Override
                            public void afterTextChanged(Editable s) {
                                testInValue = ((EditText) view).getText().toString().trim();
                                float value = calculateRebateValue();
                                if (value > -1f) updateDuctSealingDetails(value);
                            }
                        });
                    }
                } else if (name != null && name.equalsIgnoreCase(AppConstants.TEST_OUT_ID)) {
                    if (view instanceof EditText) {
                        testOut = (EditText) view;
                        testOut.setInputType(InputType.TYPE_CLASS_NUMBER);
                        testOut.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {}
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                            @Override
                            public void afterTextChanged(Editable s) {
                                testOutValue = ((EditText) view).getText().toString().trim();
                                float value = calculateRebateValue();
                                if (value > -1f) updateDuctSealingDetails(value);
                            }
                        });
                    }

                } else if (name != null && name.equalsIgnoreCase(AppConstants.POST_TEST_MEASURED_AIR_FLOW_ID)) {
                    if (view instanceof EditText) {
                        postMeasuredAirFlow = (EditText) view;
                        postMeasuredAirFlow.setInputType(InputType.TYPE_CLASS_NUMBER);
                        postMeasuredAirFlow.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {}
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                            @Override
                            public void afterTextChanged(Editable s) {
                                postMeasuredAirFlowId = ((EditText) view).getText().toString().trim();
                                float value = calculateRebateValue();
                                if (value > -1f) updateDuctSealingDetails(value);
                            }
                        });
                    }

                } else if (name != null && name.equalsIgnoreCase(AppConstants.PROJECT_COST)) {
                    if (view instanceof EditText) {
                        projectCost = (EditText) view;
                        projectCost.setInputType(InputType.TYPE_CLASS_NUMBER);
                        projectCost.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {}
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                            @Override
                            public void afterTextChanged(Editable s) {
                                projectCostValue = ((EditText) view).getText().toString().trim();
                                float value = calculateRebateValue();
                                if (value > -1f) updateDuctSealingDetails(value);
                            }
                        });
                    }

                } else if (name != null && name.equalsIgnoreCase(AppConstants.QUANTITY)) {
                    if (view instanceof EditText) {
                        quantity = (EditText) view;
                        quantity.setInputType(InputType.TYPE_CLASS_NUMBER);
                        quantity.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {}
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                            @Override
                            public void afterTextChanged(Editable s) {
                                quantityValue = ((EditText) view).getText().toString().trim();
                                float value = calculateRebateValue();
                                if (value > -1f) updateDuctSealingDetails(value);
                            }
                        });
                    }
                } else if (name != null && name.equalsIgnoreCase(AppConstants.IMPROVEMENT_DATE)) {

                } else if (name != null && name.equalsIgnoreCase(AppConstants.LEAKAGE_IMPROVEMENT_ID)) {
                    if (view instanceof EditText) {
                        leakageImprovement = (EditText) view;
                    }
                } else if (name != null && name.equalsIgnoreCase(AppConstants.TOTAL_LEAKAGE_ID)) {
                    if (view instanceof EditText) {
                        totalLeakage = (EditText) view;
                    }
                } else if (name != null && name.equalsIgnoreCase(AppConstants.ELIGIBILITY)) {
                    if (view instanceof EditText) {
                        eligibility = (EditText) view;
                    }
                } else if (name != null && name.equalsIgnoreCase(AppConstants.DUCT_SEALING_PASS_FAIL)) {
                    if (view instanceof EditText) {
                        result = (EditText) view;
                        try {
                            ductSealingItem.setStaticRebateValue(
                                    ((Map<String, String>) result.getTag()).get("staticDisplayedValue"));
                        } catch (Exception e) {
                        }
                    }
                }
            }
        }
    }

    public void updateDuctSealingDetails() {
        float rebateValue = calculateRebateValue();
        if (rebateValue < 0) rebateValue = 0;
        updateDuctSealingDetails(rebateValue);
    }

    private float calculateRebateValue() {
        float rebateValue = -1f;

        if (!TextUtils.isEmpty(rebateCalcType) &&
                rebateCalcType.equals(ProjectCostRebateCalculation.CALC_TYPE)) {
            if (!TextUtils.isEmpty(projectCostValue)) {
                float cost;
                int qty = 0;
                try {
                    cost = Float.parseFloat(projectCostValue);
                    if (!TextUtils.isEmpty(quantityValue)) qty = Integer.parseInt(quantityValue);
                    rebateValue = costBasedRebateCalculation.getRebateValue(rebateType, cost, qty);
                } catch (NumberFormatException e) {
                    return rebateValue;
                }
            } else {
                //if there is no project cost then by definition the rebate is 0
                rebateValue = 0f;
            }
        } else {
            //default duct sealing calculation
            if (!TextUtils.isEmpty(testInValue) &&
                    !TextUtils.isEmpty(testOutValue) &&
                    !TextUtils.isEmpty(postMeasuredAirFlowId)) {
                rebateValue = measurementBasedRebateCalculation.getRebateValue
                        (testInValue, testOutValue, postMeasuredAirFlowId, tonnageValue);
            }
        }

        return rebateValue;
    }

    private void updateDuctSealingDetails(float value) {
        if (totalLeakage != null) {
            int rebateValue = (int) measurementBasedRebateCalculation.getTotalImprovement();
            totalLeakage.setText(String.valueOf(rebateValue));
            totalLeakage.setBackground(getActivity().getResources().getDrawable(R.drawable.bg_dialog_box));
        }
        if (leakageImprovement != null) {
            int rebateValue = (int) measurementBasedRebateCalculation.getLeakageImprovement();
            leakageImprovement.setText(String.valueOf(rebateValue));
            leakageImprovement.setBackground(getActivity().getResources().getDrawable(R.drawable.bg_dialog_box));
        }
        if (eligibility != null) {
            if (measurementBasedRebateCalculation.isLevel1Qualified()) {
                eligibility.setText("Level 1 Qualified");
            } else if (measurementBasedRebateCalculation.isLevel2Qualified()) {
                eligibility.setText("Level 2 Qualified");
            } else {
                eligibility.setText("Not Eligible");
            }

            eligibility.setBackground(getActivity().getResources().getDrawable(R.drawable.bg_dialog_box));
        }

        ductSealingItem.setRebateValue(value);
        if (result != null) {
            String formatedValue = ductSealingItem.getStaticRebateValue();
            if (TextUtils.isEmpty(formatedValue)) {
                formatedValue = (String.format("%.02f", value));
            }
            result.setText("" + formatedValue);
            result.setBackground(getActivity().getResources().getDrawable(R.drawable.bg_dialog_box));
            ICFLogger.d(TAG, "Result:" + "$" + formatedValue);
        }
    }

    public void updateCaptureImage(int pageIndex) {
        UiUtil.updateCaptureImage(uiManager, pageIndex + "", getActivity());
    }

    @Override
    public void onDestroyView() {
        UiUtil.cleanBitmap(getView());
        saveForm();
        resetFormViews();
        super.onDestroyView();
    }

    public void saveForm() {
        List<DuctSealingItem> items = ((DuctSealingPagerFragment) getParentFragment()).getDuctList();

        if (items != null) {
            int index = items.indexOf(ductSealingItem);
            if (index > -1) {
                Log.v(TAG, "onDestroyView " + index);
                if (!isDeleted) {
                    if (ductSealingItem.isItemSaved() && result != null) {
                        String formatedValue = (String.format("%.02f", ductSealingItem.getRebateValue()));
                        result.setText("$" + formatedValue);
                    }
                    ((DuctSealingPagerFragment) getParentFragment()).saveForm(index, false);
                }
            }
        }
    }

    public void setFragmentDeleted() {
        isDeleted = true;
    }

    private void resetFormViews() {
        tonnage = null;
        testIn = null;
        testOut = null;
        postMeasuredAirFlow = null;
        leakageImprovement = null;
        totalLeakage = null;
        eligibility = null;
        result = null;
        projectCost = null;
        quantity = null;
    }

    public void setTitleId(String title) {
        this.title = title;
    }

    public String getTitleId() {
        return this.title;
    }
}
