package com.icf.rebate.ui.fragment;

import android.support.v4.app.Fragment;

import com.icf.rebate.app.model.SealingItem;
import com.icf.rebate.ui.controller.FormUIManager;
import com.icf.rebate.ui.listeners.RequestCallFlow;

/**
 * Created by 19713 on 2/8/2018.
 */

public abstract class SealingFragment extends Fragment
        implements FormUIManager.FormViewCreationCallback, RequestCallFlow {

    public abstract SealingFragment newInstance(SealingItem item);
    public abstract void setTitleId(String title);
    public abstract String getTitleId();
}
