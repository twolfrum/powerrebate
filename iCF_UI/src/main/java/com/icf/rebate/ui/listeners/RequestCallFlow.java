package com.icf.rebate.ui.listeners;

public interface RequestCallFlow
{
    void showProgressDialog();

    void stopProgressDialog();

    void updateData(Object obj);
    
    void displayError(String title, String message);
}
