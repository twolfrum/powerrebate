package com.icf.rebate.ui;

import com.icf.ameren.rebate.ui.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

public class SplashActivity extends Activity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
	super.onCreate(savedInstanceState);
	if (!isTaskRoot())
	{
	    Intent intent = getIntent();
	    String intentAction = intent.getAction();
	    if (intent.hasCategory(Intent.CATEGORY_LAUNCHER) && intentAction != null && intentAction.equals(Intent.ACTION_MAIN))
	    {
		finish();
		return;
	    }
	}
	requestWindowFeature(Window.FEATURE_NO_TITLE);
	getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
	getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
	getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
	setContentView(R.layout.splash_screen);
	
	final Handler handler = new Handler();
	handler.postDelayed(new Runnable()
	{
	    @Override
	    public void run()
	    {
		Intent obj = new Intent(getApplicationContext(), LoginActivity.class);
		startActivity(obj);
		finish();
	    }
	}, 3000);
    }

}
