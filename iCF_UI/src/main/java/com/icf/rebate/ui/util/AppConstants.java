package com.icf.rebate.ui.util;

public class AppConstants
{
    public static final String LICENSE_AGREEMENT = "ICF_License_Agreement";
    public static final int CAPTURE_PHOTO_REQUEST_CODE = 1;
    public static final int REQUEST_CODE_GALLERY = 2;
    public static final int REQUEST_CODE_I_MANIFOLD = 3;
    public static final int CAPTURE_IMAGE_MEDIUM_SIZE = 1000;
    public static final String HTTP_AUTHENTICATION_ERROR = "e1001";
    public static final int MAX_SUBMITTED_ENTRIES = 10;

    public static final String LAUNCH_IMANIFOLD = "Launch_iManifold";
    public static final String ACTUAL_CAPACITY = "Actual_Capacity";
    public static final String SYSTEM_EFFICIENCY = "System_Efficiency";
    public static final String SYSTEM_NOMINAL_CAPACITY = "System_Nominal_Capacity";
    public static final String TUNE_UP_PASS_OR_FAIL = "Tune_Up_Pass_OR_Fail";
    public static final String QUALITY_INSTALL_VERIFICATION_PASS_FAIL = "Quality_Install_Verification_Pass_Fail";
    public static final String TONNAGE_ID = "tonnage";
    public static final String WINDSHIELD_ID = "windshielding";
    public static final String STORIES_ID = "stories";
    public static final String QUALITY_INSTALL_SYSTEM_TESTED = "Quality_Install_Verification_System_Tested";
    public static final String TEST_IN_ID = "Duct_Sealing_Test_In";
    public static final String TEST_OUT_ID = "Duct_Sealing_Test_Out";
    public static final String PRE_TEST_MEASURED_AIR_FLOW_ID = "Duct_Sealing_PreTest_Measured_Air_Flow_CFM";
    public static final String POST_TEST_MEASURED_AIR_FLOW_ID = "Duct_Sealing_PostTest_Measured_Air_Flow_CFM";
    public static final String LEAKAGE_IMPROVEMENT_ID = "Duct_Sealing_Leakage_Percent_Improvement";
    public static final String TOTAL_LEAKAGE_ID = "Duct_Sealing_Test_Out_Percent_Leakage";
    public static final String DUCT_SEALING_PASS_FAIL = "Duct_Sealing_Pass_Fail";
    public static final String PROJECT_COST = "projectCost";
    public static final String IMPROVEMENT_DATE = "Improvement Date";
    public static final String QUANTITY = "quantity";
    
    public static final String EQUIP_NOMINAL_BTU = "AC_TuneUp_Equipment_Nominal_BTU";
    public static final String FAN_AIRFLOW_B4 = "Fan_Airflow_Before";
    public static final String FAN_AIRFLOW_AFTER = "Fan_Airflow_After";
    public static final String COIL_ENTERING_WB = "Coil_Entering_WB"; 
    public static final String COIL_ENTERING_WB_B4 = COIL_ENTERING_WB + "_Before";
    public static final String COIL_ENTERING_WB_AFTER = COIL_ENTERING_WB + "_After";
    public static final String COIL_ENTERING_ENTHALPY_B4 = COIL_ENTERING_WB + "_Before_Enthalpy";
    public static final String COIL_ENTERING_ENTHALPY_AFTER = COIL_ENTERING_WB + "_After_Enthalpy";

    public static final String COIL_LEAVING_WB = "Coil_Leaving_WB";
    public static final String COIL_LEAVING_WB_B4 = COIL_LEAVING_WB + "_Before";
    public static final String COIL_LEAVING_WB_AFTER = COIL_LEAVING_WB + "_After";
    public static final String COIL_LEAVING_ENTHALPY_B4 = COIL_LEAVING_WB + "_Before_Enthalpy";
    public static final String COIL_LEAVING_ENTHALPY_AFTER = COIL_LEAVING_WB + "_After_Enthalpy";
    public static final String COIL_CAPACITY = "Coil_Capacity";
    public static final String COIL_CAPACITY_B4 = COIL_CAPACITY + "_Before";
    public static final String COIL_CAPACITY_AFTER = COIL_CAPACITY + "_After";
    public static final String SYS_EFFICIENCY_B4 = "System_Effective_Efficiency_Before";
    public static final String SYS_EFFICIENCY_AFTER = "System_Effective_Efficiency_After";
    
    public static final String FAN_STATIC_PRESS_AFTER = "Fan_Airflow_Static_Pressure_After";
    public static final String SYS_WATTS_AFTER = "System_Watts_After";

    public static final String ELIGIBILITY = "eligibility";
    public static final String TUNE_UP_PAGE_NAME = "Tune_Up_Page_Name";
    public static final String THERMOSTAT_INSTALL_UPGRADE_MEASURE_NAME_ID = "measureName";
    public static final String THERMOSTAT_INSTALL_MANUFACTURER_NAME_ID = "Thermostat_Install_Manufacturer_Name";
    public static final String THERMOSTAT_INSTALL_OTHER_MANUFACTURER_NAME_ID = "Thermostat_Install_Other_Manufacturer_Name";
    public static final String THERMOSTAT_INSTALL_OLD_T_STAT_TYPE_ID = "Thermostat_Install_Old_T_Stat_Type";
    public static final String THERMOSTAT_INSTALL_MODEL_NUMBER_ID = "Thermostat_Install_Model_number";
    public static final String THERMOSTAT_INSTALL_RESULT_ID = "Thermostat_Install_Result";
    public static final String EQUIPMENTS_RESULT_ID = "equipment_result";
    public static final String INSULATION_RESULT_ID = "insulation_result";
    public static final String WD_RESULT_ID = "wd_result";
    public static final String QIV_MANIFOLD = "IManifold Values";

    public static final String COMBO_SELECT_VALUE = "Select Value";
    public static final String OLD_HEATING_SYSTEM_SIZE = "furnace_old_system_efficiency";
    public static final String OLD_HEATING_SYSTEM_TYPE = "furnaceOldHeatingSystemType";
    public static final String SYSTEM_TYPE = "systemType";
    public static final String NEW_AIR_HANDLER_UNIT = "newAirHandlerUnit";

    public static final String SEER_ID = "seerValue";
    public static final String BRUSHLESS_PERMANENT_MAGNET_MOTOR_CHECKBOX_ID = "brushless_permanent_magnet_motor_checkbox";
    public static final String OLD_BLOWER_MOTOR_CHECKBOX_ID = "old_blower_motor_checkbox";
    public static final String BRUSHLESS_PERMANENT_MAGNET_MOTOR_ECM_AHRI_ID = "ECM_is_in_AHRI_Directory";
    public static final String WATER_HEATER_EFFICIENCY_ID = "Water_Heater_Efficiency";
    public static final String NEW_WATER_HEATER_ID = "New_Water_Heater_Id";
    public static final String MINI_SPLITS_OLD_HEATING_SYSTEM_TYPE_ID = "Mini_Splits_Old_Heating_System_Type_ID";
    public static final String FURNACE_EFFICIENCY_ID = "furnace_effciency_id";

    public static final String BOILER_EFFICIENCY_ID = "ROF_Boiler_Efficiency";
    public static final String BOILER_OLD_SYSTEM_EFFICIENCY_ID = "boiler_old_system_efficiency";
    public static final String FURNACE_OLD_SYSTEM_EFFICIENCY_ID = "furnace_old_system_efficiency";
    public static final String BOILER_OLD_HEATING_SYSTEM_TYPE_ID = "boilerOldHeatingSystemType";
    public static final String OLD_HEATING_SYSTEM_TYPE_ID = "oldHeatingSystemType";
    public static final String FURNACE_OLD_HEATING_SYSTEM_TYPE_ID = "furnaceOldHeatingSystemType";
    public static final float BOILER_OLD_SYSTEM_EFFICIENCY_THERSHOLD_VALUE = 80f;
    public static final String AC_FURNACE_SUFFIX = "Furnace";
    public static final String AC_FURNACE = "acfurnace";

    public static final String ADD_SYMBOL = "+Add";
    public static final String DELETE_SYMBOL = "Delete";

    public static final String EQUIPMENT_ID = "equipments";
    public static final String DUCTIMPROVEMENT_ID = "ductImprovement";
    public static final String DUCTSEALING_ID = "ductsealing";
    public static final String QUALITYINSTALLVERIFICATION_ID = "qualityInstallVerification";
    public static final String THERMOSTATREFERRAL_ID = "thermostatReferral";
    public static final String THERMOSTATREFERRAL_XML_NAME = "Thermostat_Referral_email";
    public static final String THERMOSTATINSTAL_ID = "thermostatInstall";
    public static final String TUNEUP_ID = "tuneUp";

    public static final String manifold_xmlName = "iManifold_File_Name";

    public static final int MAX_PAGE_SUPPORT = 5;

    // public static final String SYSTEM_TONNAGE_ID = "System Tonnage";
    // public static final String RESULT = "Result";
    // public static final String SYSTEM_NOMINAL_TONNAGE = "System Nominal (Tonnage)";

    public static final int ONE_TON_TO_BTUH = 12000;

    public static final String SIGNATURE_FILE_NAME = "signature.png";

    public static final String VOICE_FILE_NAME = "voice.3gp";

    public static final String INVOICE_FILE_NAME = "invoice.jpg";

    public static final String JOB_DOCUMENT = "form_data.json";

    public static final String JOB_DOCUMENT_UI = "form_data_ui.json";

    public static final String TERMS_FILE = "terms.txt";
    
    public static final String LICENSE_FILE = "licenseAgreement.txt";

    public static final String FAQ_FILE = "FAQ.htm";

    public static final int ID_PENDING_JOBS = 1;

    public static final int ID_REBATE_TOOL = 2;

    // Equipment Ids

    public static final String ACFURNACE_ID = "acfurnace";
    public static final String HEATPUMP_ID = "heatpump";
    public static final String DUALFUELHEATPUMP_ID = "dualfuelheatpump";
    public static final String MINISPLITS_ID = "minisplits";
    public static final String GEOTHERMALHEATPUMPS_ID = "geothermalheatpumps";
    public static final String ACWITHELECTRICHEAT_ID = "acwithelectricheat";
    public static final String PACKAGEUNIT_ID = "packageunit";
    public static final String BRUSHLESSPERMANENTMAGNETMOTOR_ID = "brushlesspermanentmagnetmotor";
    public static final String RETROFIT_BRUSHLESSPERMANENTMAGNETMOTOR_ID = "retrofitbrushlesspermanentmagnetmotor";
    public static final String WATERHEATER_ID = "waterheater";
    public static final String BOILER_ID = "boiler";
    public static final String FURNACE_ID = "furnace";
    
    // IDs for submission screens
    public static final String SURVEY_ID = "survey";
    public static final String REBATE_RECIPIENT_ID = "rebateRecipient";
    public static final String TERMS_AND_CONDITIONS_ID = "termsAndConditions";
    
    public static final String SURVEY_DETAIL_ID = "surveyDetail";

    public static final String GEOTHERMAL_CIRCULATING_PUMPSIZE = "geothermal_heat_pumps_circulating_pump_size";

    // launch URI

    public static final String LAUNCH_MANIFOLD_URI_WITH_DATA = "imanifold://startnewsession_with_data?key=";

    public static final String LAUNCH_MANIFOLD_URI_WITH_ONLY_KEY = "imanifold://startnewsession?key=";

    public static final String QUALITY_INSTALL_VERIFICATION = "Quality Install Verification";

    public static final String EQUIPMENT_CONDENSER_ID = "New_Outside_Condenser";

    public static final String HEAT_PUMP_CONDENSER_ID = "New_Heat_Pump_Condenser";

    public static final String HEAT_PUMP_NEW_AIR_HANDLER_ID = "New_Air_Handler_Unit";

    public static final String DUAL_HEAT_PUMP_CONDENSER_ID = "New_Heat_Pump_Condenser";

    public static final String MINI_SPLITS_CONDENSER_ID = "New_Heat_Pump_Condenser";

    public static final String TUNE_UP_CONDENSER_ID = "Condenser";

    public static final String MINI_SPLITS_NEW_AIR_HANDLER_ID = "newAirHandlerUnit";

    public static final String GEO_THERMAL_CONDENSER_ID = "New_Heat_Pump_Condenser";

    public static final String GEO_THERMAL_NEW_AIR_HANDLER_ID = "New_Air_Handler_Unit";

    public static final String AC_WITH_ELECTRIC_HEAT_CONDENSER_ID = "New_Outside_Condenser";

    public static final String AC_WITH_ELECTRIC_HEAT_AIR_HANDLER_UNIT = "New_Air_Handler_Unit";

    public static final String TUNE_UP_AIR_HANDLER = "Evaporator/Air_Handler";

    public static final String TUNE_UP_PACKAGE_UNIT = "Tune_Up_Package_Unit";

    public static final String[] MANIFOLD_CONDENSER_IDS = { EQUIPMENT_CONDENSER_ID, HEAT_PUMP_CONDENSER_ID, TUNE_UP_CONDENSER_ID, TUNE_UP_PACKAGE_UNIT };

    public static final String[] MANIFOLD_AIRHANDLER_IDS = { HEAT_PUMP_NEW_AIR_HANDLER_ID, MINI_SPLITS_NEW_AIR_HANDLER_ID, TUNE_UP_AIR_HANDLER };

    public static final String MANUFACTURE_NAME = "Manufacturer Name";

    public static final String MODEL_NUMBER = "Model Number";

    public static final String SERIAL_NUMBER = "Serial Number";

    public static final String URI_CONDENSER_MAKE = "condenser_make=";

    public static final String URI_CONDENSER_SERIAL = "condenser_serial=";

    public static final String URI_CONDENSER_MODEL = "condenser_model=";

    public static final String URI_AIRHANDLER_MAKE = "air_handler_make=";

    public static final String URI_AIRHANDLER_SERIAL = "air_handler_serial=";

    public static final String URI_AIRHANDLER_MODEL = "air_handler_model=";

    public static final String[] URI_PARAMTERS = { URI_CONDENSER_MAKE, URI_CONDENSER_MODEL, URI_CONDENSER_SERIAL, URI_AIRHANDLER_MAKE, URI_AIRHANDLER_MODEL, URI_AIRHANDLER_SERIAL };
    
    public static final String SUBTYPE_SELECT_POSITION_KEY = "subtype_select_position_key";
    
    public static final String AGREE_BUTTON_FILE_NAME = "iAgreeButton.png";

    public static final String DISAGREE_BUTTON_FILE_NAME = "iDisagreeButton.png";
    
    public static final String REBATE_CONFIM_DETAIL_TYPE_PICTURE = "picture";
    
    public static final String REBATE_CONFIRMATION_DETAIL_TYPE_MULTILINE = "multiLineInput";
    
    public static final String REBATE_CONFIRMATION_DOCUMENT_LATER = "documentLater";

    public static final String REBATE_CONFIRMATION_ORDER_NUMBER = "orderNumber";

    public static enum FORM_EDIT_MODE
    {
	EDIT_MODE(0), VALIDATION_MODE(1);

	final int mode;

	FORM_EDIT_MODE(int mode)
	{
	    this.mode = mode;
	}

	public int getMode()
	{
	    return mode;
	}
    }

    public static enum FORM_ITEM_FILLED_STATE
    {
	ITEM_UNEDITED(0), ITEM_COMPLETELY_SAVED(1), ITEM_PARTIALLY_SAVED(2);

	final int state;

	FORM_ITEM_FILLED_STATE(int state)
	{
	    this.state = state;
	}

	public int getState()
	{
	    return state;
	}
    }

    public static enum FORM_EDITED_STATE
    {
	FORM_UNEDITED(3), FORM_EDITED(4), FORM_EDITED_BUT_NOT_COMPLETELY_SAVED(5);

	final int state;

	FORM_EDITED_STATE(int state)
	{
	    this.state = state;
	}

	public int getState()
	{
	    return state;
	}
    }

    
}
