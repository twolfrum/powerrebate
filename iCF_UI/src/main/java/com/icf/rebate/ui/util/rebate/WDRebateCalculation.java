/**
 * 
 */
package com.icf.rebate.ui.util.rebate;

import java.util.ArrayList;
import java.util.List;

import com.icf.rebate.networklayer.model.CustomerInfo;
import com.icf.rebate.networklayer.model.FormResponseBean;
import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.util.ICFLogger;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * @author Ken Butler Dec 18, 2015
 * WDRebateCalculation.java
 * Copyright (c) 2015 ICF International
 */
public class WDRebateCalculation {

    public final String TAG = "WDRebateCalculation";
    private ArrayList<String> selectionArgs;
    private ICFSQLiteOpenHelper helper;
    private String selectionString;
    private String[] columns;
    private String orderBy;
    private double mMaxRebate;
    private int mNumInstalled;
    
    public WDRebateCalculation(String wdType, double uVal, double shgcVal, int numInstalled) {
	
	int utilityCompanyId = LibUtils.getSelectedUtilityCompany().getId();
	
	FormResponseBean bean = RebateManager.getInstance().getFormBean();
	CustomerInfo customerInfo = bean.getCustomerInfo();
	boolean singleHouse = customerInfo.getHouseType() == CustomerInfo.SINGLE_FAMILY_TYPE;
	helper = new ICFSQLiteOpenHelper(LibUtils.getApplicationContext());
	mNumInstalled = numInstalled;
	
	selectionArgs = new ArrayList<>();
	
	// Windows and glass doors.
	if (wdType.equals("window") || wdType.equals("glassDoor")) {
	    	selectionArgs.add(String.valueOf(utilityCompanyId));
		selectionArgs.add(wdType);
		selectionArgs.add(singleHouse ? "Single Family" : "Multifamily");
		selectionArgs.add(String.valueOf(uVal));
		selectionArgs.add(String.valueOf(uVal));
		selectionArgs.add(String.valueOf(shgcVal));
		selectionArgs.add(String.valueOf(shgcVal));
	    
		selectionString = WindowDoorTable.COLUMN_UTILITY_COMPANY + " = ? AND " 
		    + WindowDoorTable.COLUMN_WD_TYPE + " = ? AND " 
		    + WindowDoorTable.COLUMN_HOUSE_TYPE + " = ? AND "
		    + WindowDoorTable.COLUMN_INSTALLED_U_MIN + " <= ? AND " 
		    + WindowDoorTable.COLUMN_INSTALLED_U_MAX + " >= ? AND " 
		    + WindowDoorTable.COLUMN_SHGC_MIN + " <= ? AND "
		    + WindowDoorTable.COLUMN_SHGC_MAX + " >= ?;";
	} 
	// Solar screens
	else {
	    	selectionArgs.add(String.valueOf(utilityCompanyId));
		selectionArgs.add(wdType);
		selectionArgs.add(singleHouse ? "Single Family" : "Multifamily");
		
		selectionString = WindowDoorTable.COLUMN_UTILITY_COMPANY + " = ? AND " 
			    + WindowDoorTable.COLUMN_WD_TYPE + " = ? AND " 
			    + WindowDoorTable.COLUMN_HOUSE_TYPE + " = ?;";
	}
	Log.d(TAG, "selectionString= " + selectionString);
    }
    
    /**
     * Calculates the rebate amount based on form details. 
     * @return - rebate, the rebate amount for this application
     */
    public double computeRebate() {
	double rebateVal = 0f;
	
	SQLiteDatabase database = helper.getReadableDatabase();
	Cursor dbCursor = database.query(WindowDoorTable.TABLE_NAME, null, null, null, null, null, null);
	String[] columnNames = dbCursor.getColumnNames();
	
	// Gets list of all columns that apply to todays date
	List<String> matchingDateRangeCols = LibUtils.getYearColumnName(columnNames, null);
	
	for (String col : matchingDateRangeCols)
	{

	    columns = new String[] { col, WindowDoorTable.COLUMN_REBATE_MAX };
	    
	    Cursor cursor = database.query(WindowDoorTable.TABLE_NAME, columns, selectionString, selectionArgs.toArray(new String[selectionArgs.size()]), null, null, orderBy);
	    String data = LibUtils.getSqlQueryString(WindowDoorTable.TABLE_NAME, columns, selectionString, selectionArgs.toArray(new String[selectionArgs.size()]), null, null, orderBy);
	    ICFLogger.d(TAG, data);
	    
	    while (cursor.moveToNext())
	    {
		
		if (!cursor.isNull(0) && !cursor.isNull(1) && rebateVal < cursor.getDouble(0)) {
		    rebateVal = cursor.getDouble(0);
		    mMaxRebate = cursor.getDouble(1);
		}
	    }
	}
	
	database.close();
	rebateVal *= mNumInstalled;
	return rebateVal > mMaxRebate ? mMaxRebate : rebateVal;
    }
    
}
