/**
 * 
 */
package com.icf.rebate.ui.fragment;

import com.icf.rebate.ui.util.AppConstants;

/**
 * @author Ken Butler Nov 28, 2016 PersistentForm.java Copyright (c) 2016 ICF International
 */
public interface PersistentForm
{
    
    /**
     * Saves form information
     */
    void saveForm(boolean isManualSave);

    /**
     * Sets validation status @see {@link AppConstants.FORM_ITEM_FILLED_STATE}
     */
    void setValidState();

    /**
     * Checks the form validity
     * 
     * @return true or false
     */
    boolean isValid();

    /**
     * Returns the number of fragments to pop to get back to the Rebate Tool List screen
     * 
     * @return the number of screens back to the toollist
     */
    int getNoScreensToToolList();
}
