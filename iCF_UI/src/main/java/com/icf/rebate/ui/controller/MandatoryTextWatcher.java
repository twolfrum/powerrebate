package com.icf.rebate.ui.controller;

import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.ui.util.UiViewUtils;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;

public class MandatoryTextWatcher implements TextWatcher //, OnFocusChangeListener
{

//    @Override
//    public void onFocusChange(View v, boolean hasFocus)
//    {
//	// TODO Auto-generated method stub
//
//    }
    private EditText watched;
    
    public MandatoryTextWatcher (EditText watched) {
	this.watched = watched;
	afterTextChanged(watched.getEditableText());
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after)
    {}

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count)
    {}

    @Override
    public void afterTextChanged(Editable s)
    {
	if (TextUtils.isEmpty(s.toString())) 
	{
	    watched.setBackground(watched.getContext().getResources().getDrawable(R.drawable.bg_dialog_mandatory_box));
	}
	else
	{
	    watched.setBackground(watched.getContext().getResources().getDrawable(R.drawable.text_field_white));
	}
    }

}
