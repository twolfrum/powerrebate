package com.icf.rebate.ui.util.rebate;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.rebate.ui.util.ICFLogger;

public class QIVTuneUpRebateTable {
    public static final String TABLE_NAME = "qiv_tuneup_rebate";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_UTILITY_COMPANY = "utility_company";
    public static final String COLUMN_REBATE_TYPE = "rebate_type";
    public static final String COLUMN_CUSTOMER_TYPE = "customer_type";
    public static final String COLUMN_HOUSE_TYPE = "house_type";
    public static final String COLUMN_PASS_PERCENTAGE = "pass_percentage";
    public static final String COLUMN_MEASURE_UNIT = "measure_unit";

    // Database creation SQL statement
    private static final String DATABASE_CREATE = "create table "
            + TABLE_NAME
            + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_UTILITY_COMPANY + " INTEGER not null,"
            + COLUMN_REBATE_TYPE + " TEXT not null,"
            + COLUMN_CUSTOMER_TYPE + " TEXT not null,"
            + COLUMN_HOUSE_TYPE + " TEXT not null,"
            + COLUMN_PASS_PERCENTAGE + " INTEGER not null,"
            + COLUMN_MEASURE_UNIT + " TEXT not null"
            + ");";

    private static final List<String> mRebateColumns = new ArrayList<String>();
    

 /*   + ICFSQLiteOpenHelper.COLUMN_INCENTIVE_2015 + " REAL,"
    + ICFSQLiteOpenHelper.COLUMN_INCENTIVE_2016 + " REAL,"
    + ICFSQLiteOpenHelper.COLUMN_INCENTIVE_2017 + " REAL,"
    + ICFSQLiteOpenHelper.COLUMN_INCENTIVE_2018 + " REAL,"
    + ICFSQLiteOpenHelper.COLUMN_INCENTIVE_2019 + " REAL,"
    + ICFSQLiteOpenHelper.COLUMN_INCENTIVE_2020 + " REAL,"
    + ICFSQLiteOpenHelper.COLUMN_INCENTIVE_2021 + " REAL,"
    + ICFSQLiteOpenHelper.COLUMN_INCENTIVE_2022 + " REAL,"
    + ICFSQLiteOpenHelper.COLUMN_INCENTIVE_2023 + " REAL,"
    + ICFSQLiteOpenHelper.COLUMN_INCENTIVE_2024 + " REAL"*/

    private static final String TAG = EquipmentListRebateTable.class.getName();

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
    }

    public static void addRebateIntervalFields(int id, SQLiteDatabase database, String csvLine) {
        String[] csvLineItems = csvLine.split(",");
        mRebateColumns.clear();
        for (String s : csvLineItems) {
            // Matches 4 digits between 1900 - 2099
            if (s.matches("19\\d\\d|20\\d\\d")) {
                String dateRange = "col" + id + "_" + "01_01_" + s + "__12_31_" + s;
                final String sqlStatement = "ALTER TABLE "
                        + TABLE_NAME
                        + " ADD COLUMN "
                        + dateRange
                        + " REAL"
                        + ";";
                mRebateColumns.add(dateRange);
                Cursor cursor = database.rawQuery("SELECT * FROM " + TABLE_NAME, null);
                if (cursor.getColumnIndex(dateRange) == -1) {
                    database.execSQL(sqlStatement);
                }
            } else if (s.matches("\\d{2}/\\d{2}/\\d{4}-\\d{2}/\\d{2}/\\d{4}")) {
                final String[] date = s.split("-|/");
                final String colName = "col" + id + "_" + date[0] + "_" + date[1] + "_" + date[2] + "__" + date[3] + "_" + date[4] + "_" + date[5];
                final String sqlStatement = "ALTER TABLE "
                        + TABLE_NAME
                        + " ADD COLUMN "
                        + colName
                        + " REAL"
                        + ";";
                mRebateColumns.add(colName);
                Cursor cursor = database.rawQuery("SELECT * FROM " + TABLE_NAME, null);
                if (cursor.getColumnIndex(colName) == -1) {
                    database.execSQL(sqlStatement);
                }
            }
        }
    }

    public static void insert(int id, SQLiteDatabase database, String strData) {
        try {
            if (strData == null || strData.length() == 0) {
                return;
            }
            String[] columnData = strData.split(",");
            ContentValues values = new ContentValues();
            int i = 1;
            values.put(COLUMN_UTILITY_COMPANY, id);
            for (String value : columnData) {
                switch (i) {
                    case 0:
                        values.put(COLUMN_UTILITY_COMPANY, Integer.parseInt(value));
                        break;
                    case 1:
                        values.put(COLUMN_REBATE_TYPE, value);
                        break;
                    case 2:
                        values.put(COLUMN_CUSTOMER_TYPE, value);
                        break;
                    case 3:
                        values.put(COLUMN_HOUSE_TYPE, value);
                        break;
                    case 4:
                        values.put(COLUMN_PASS_PERCENTAGE, Integer.parseInt(value));
                        break;
                    case 5:
                        values.put(COLUMN_MEASURE_UNIT, value);
                        break;
                    default:
                        if (i - 6 >= 0 && i - 6 < mRebateColumns.size()) {
                            values.put(mRebateColumns.get(i - 6), LibUtils.parseDouble(value, 0f));
                        }
                        break;
        /*case 6:
		    values.put(ICFSQLiteOpenHelper.COLUMN_INCENTIVE_2015, LibUtils.parseFloat(value, 0.00f));
		    break;
		case 7:
		    values.put(ICFSQLiteOpenHelper.COLUMN_INCENTIVE_2016, LibUtils.parseFloat(value, 0.00f));
		    break;
		case 8:
		    values.put(ICFSQLiteOpenHelper.COLUMN_INCENTIVE_2017, LibUtils.parseFloat(value, 0.00f));
		    break;
		case 9:
		    values.put(ICFSQLiteOpenHelper.COLUMN_INCENTIVE_2018, LibUtils.parseFloat(value, 0.00f));
		    break;
		case 10:
		    values.put(ICFSQLiteOpenHelper.COLUMN_INCENTIVE_2019, LibUtils.parseFloat(value, 0.00f));
		    break;
		case 11:
		    values.put(ICFSQLiteOpenHelper.COLUMN_INCENTIVE_2020, LibUtils.parseFloat(value, 0.00f));
		    break;
		case 12:
		    values.put(ICFSQLiteOpenHelper.COLUMN_INCENTIVE_2021, LibUtils.parseFloat(value, 0.00f));
		    break;
		case 13:
		    values.put(ICFSQLiteOpenHelper.COLUMN_INCENTIVE_2022, LibUtils.parseFloat(value, 0.00f));
		    break;
		case 14:
		    values.put(ICFSQLiteOpenHelper.COLUMN_INCENTIVE_2023, LibUtils.parseFloat(value, 0.00f));
		    break;
		case 15:
		    values.put(ICFSQLiteOpenHelper.COLUMN_INCENTIVE_2024, LibUtils.parseFloat(value, 0.00f));
		    break;*/
                }
                i++;
            }
            database.insert(TABLE_NAME, null, values);
            ICFLogger.d(TAG, "Insert : " + TABLE_NAME + " " + values);
        } catch (Exception e) {
            ICFLogger.e(TAG, e);
        }
    }
}
