package com.icf.rebate.ui.customviews;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

/**
 * TODO: document your custom view class.
 */
public class SignatureView extends View
{
    private float currentXPos;
    private float currentYPos;

    private ArrayList<Line> lines = new ArrayList<Line>();
    private float downXPos;
    private float downYPos;

    private Paint linePaint;

    private Bitmap signatureBitmap;

    private boolean userSigned;

    public SignatureView(Context context)
    {
	super(context);
	init(null, 0);
    }

    public SignatureView(Context context, AttributeSet attrs)
    {
	super(context, attrs);
	init(attrs, 0);
    }

    public SignatureView(Context context, AttributeSet attrs, int defStyle)
    {
	super(context, attrs, defStyle);
	init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle)
    {
	// Set up a default TextPaint object
	linePaint = new Paint();
	linePaint.setAntiAlias(true);
	linePaint.setStrokeWidth(4);
	linePaint.setColor(Color.BLACK);
	linePaint.setStyle(Paint.Style.STROKE);
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
	super.onDraw(canvas);

	if (signatureBitmap != null)
	{
	    canvas.drawBitmap(signatureBitmap, 0, 0, linePaint);
	}
	else
	{
	    for (Line l : lines)
	    {
		canvas.drawLine(l.getStartX(), l.getStartY(), l.getStopX(), l.getStopY(), linePaint);
	    }
	}
    }

    public void clearSignature()
    {
	userSigned = false;
	lines.clear();
	invalidate();
    }

    public void setSignatureBitmap(Bitmap bmp)
    {
	signatureBitmap = bmp;
    }

    public boolean saveSignature(String path)
    {
	Bitmap bitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
	Canvas c = new Canvas(bitmap);
	c.drawColor(Color.WHITE);
	draw(c);
	try
	{
	    File f = new File(path);
	    f.createNewFile();

	    FileOutputStream out = new FileOutputStream(f);
	    bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
	    out.close();
	    Log.d("Sig", "Signature saved");
	    return true;
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	    return false;
	}

    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
	int action = event.getAction();

	if (action == MotionEvent.ACTION_DOWN)
	{
	    currentXPos = downXPos = event.getX();
	    currentYPos = downYPos = event.getY();
	    lines.add(new Line(currentXPos, currentYPos, currentXPos, currentYPos));
	    invalidate();
	}
	else if (action == MotionEvent.ACTION_UP)
	{
	    if (downXPos == event.getX() && downYPos == event.getY())
	    {
		lines.add(new Line(currentXPos, currentYPos, event.getX() + 2, event.getY() + 2));
	    }
	    else
	    {
		lines.add(new Line(currentXPos, currentYPos, event.getX(), event.getY()));
		currentXPos = event.getX();
		currentYPos = event.getY();
	    }
	    userSigned = true;
	    invalidate();
	}
	else if (action == MotionEvent.ACTION_MOVE)
	{
	    lines.add(new Line(currentXPos, currentYPos, event.getX(), event.getY()));
	    currentXPos = event.getX();
	    currentYPos = event.getY();
	    invalidate();
	}
	return true;
    }
    
    public boolean isUserSigned()
    {
	return userSigned;
    }
}
