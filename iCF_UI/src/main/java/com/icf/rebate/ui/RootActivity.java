package com.icf.rebate.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Random;

import android.Manifest;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings.Secure;
import android.support.annotation.NonNull;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager.OnBackStackChangedListener;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.text.format.DateUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.TouchDelegate;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import io.fabric.sdk.android.Fabric;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.adapter.SettingsExpandableListAdapter;
import com.icf.rebate.app.BackgroundService;
import com.icf.rebate.app.model.MenuOption;
import com.icf.rebate.networklayer.ConnectionManager;
import com.icf.rebate.networklayer.HttpManagerListener;
import com.icf.rebate.networklayer.ICFHttpManager;
import com.icf.rebate.networklayer.PendingJobRunnable;
import com.icf.rebate.networklayer.model.AppDetails;
import com.icf.rebate.networklayer.model.ConfigurationDetail.UtilityCompany;
import com.icf.rebate.networklayer.model.LoginResponseBean;
import com.icf.rebate.networklayer.model.PendingJobItem;
import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.dialog.DialogListener;
import com.icf.rebate.ui.dialog.ICFDialogFragment;
import com.icf.rebate.ui.dialog.ICFPINDialog;
import com.icf.rebate.ui.fragment.CustomerInformationFragment;
import com.icf.rebate.ui.fragment.DashboardFragment;
import com.icf.rebate.ui.listeners.PendingJobListener;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.FileUtil;
import com.icf.rebate.ui.util.LoggerUtils;
import com.icf.rebate.ui.util.UiUtil;

//import com.nostra13.universalimageloader.core.ImageLoader;

public class RootActivity extends ActionBarActivity implements OnClickListener, OnGroupClickListener, HttpManagerListener, OnGroupExpandListener {

    public String currenFragment = null;

    private DrawerLayout mDrawerLayout;

    private ActionBarDrawerToggle mDrawerToggle;

    private ExpandableListView settingListView;

    private ArrayList<MenuOption> settingListData = new ArrayList<MenuOption>();

    private SettingsExpandableListAdapter settingAdapter;

    public String passwordChange;

    private long lastStopTime = -1;

    private GoogleApiClient mGoogleApiClient;

    private Location currentLocation;

    private View utilityCompanyHeaderRoot;

    private TextView utilityCompanyHeader;

    private ImageView utilityCompanyImg;

    private AlertDialog mAppUpdateDialog;

    private MenuOption optionFaq;

    // private View subHeaderRoot;
    //
    // private TextView subheader;
    //
    // private ImageView subHeaderImg;
    //
    // private ImageLoader imageLoader;

    private PendingJobRunnable pendingRunnable;

    private int previousGroup = -1;

    private static final LocationRequest REQUEST = LocationRequest.create().setInterval(5000) // 5 seconds
            .setFastestInterval(16) // 16ms = 60fps
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

    private static final int MY_PERMISSION_REQUEST_READ_FINE_LOCATION = 100;
    private static final int MY_PERMISSION_REQUEST_CAMERA = 101;
    private static final String TAG = RootActivity.class.getSimpleName();

    // Was running into some crashes when taking pictures because the FormResponseBean in RebateManager was getting
    // freed by the system, need to marshal data relating to the current bean when this activity is stopping and unmarshal on start
    @Override
    protected void onSaveInstanceState(Bundle state) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.landingpage);
        animation = new Random().nextInt() % 2 == 0;
        UiUtil.rootActivity = new WeakReference<RootActivity>(this);

        // Crashlytics custom tag for user id
        if (Fabric.isInitialized()) {
            String userId;
            Crashlytics.setUserIdentifier(userId = (LibUtils.getLoggedInUserBean() == null ? "null" : LibUtils.getLoggedInUserBean().getUserId()));
            Log.d(TAG, "Setting crashlytics custom tag userId=" + userId);
        }
        // if(!(fragment instanceof DashboardFragment) )
        // {
        utilityCompanyHeaderRoot = findViewById(R.id.company_header);
        // }

        createNavigationDrawer();
        initialize();
        // TODO why do we need this?
        Handler h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                updateTitle(getResources().getString(R.string.dashboard_txt));
            }
        }, 100);
        updateCompanyDetails();
        updateManifoldValue(getIntent());
        displaySubmissionError(getIntent());
        startBackgroundService();

    }

    /**
     * Checks current version number against the version number sent in the form_data.json file. Prompts user to update if applicable.
     *
     * @return needsUpdate - whether or not the app needs an update
     */
    public boolean checkForUpdate(boolean dismissableDialog) {
        boolean needsUpdate = false;
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }

        AppDetails appDetails = RebateManager.getInstance().getAppDetail();

        if (pInfo == null || appDetails == null) {
            return false;
        }
        // Compare the version codes
        int thisVerCode = pInfo.versionCode;
        int latestVerCode = LibUtils.parseInteger(appDetails.getAndroidBuildVersion(), -1);

        if (thisVerCode < latestVerCode && latestVerCode != -1) {
            mAppUpdateDialog = UiUtil.showError(this, getResources().getString(R.string.alert_title), appDetails.getAndroidBuildUpgradeMessage(), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("market://details?id=com.icf.ameren.rebate.ui"));
                    startActivity(intent);
                }

            }, dismissableDialog);
            needsUpdate = true;
        }
        return needsUpdate;
    }

    public void dismissUpdateDialog() {
        if (mAppUpdateDialog != null) {
            mAppUpdateDialog.dismiss();
        }
    }

    private void startBackgroundService() {
        Intent t = new Intent(getApplicationContext(), BackgroundService.class);
        t.setAction(BackgroundService.START_SERVICE);
        startService(t);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        updateManifoldValue(intent);
        displaySubmissionError(intent);
    }

    private void updateManifoldValue(Intent intent) {
        if (intent != null) {
            Uri uri = intent.getData();
            if (uri != null) {
                String path = uri.getPath();
                if (UiUtil.currentFormManager != null && UiUtil.manifoldKey != null) {
                    UiUtil.currentFormManager.retrieveManifoldValue(path);
                    UiUtil.currentFormManager = null;
                    UiUtil.manifoldKey = null;
                }
            }
        }
    }

    private void displaySubmissionError(Intent intent) {
        if (intent != null) {
            if (intent.getBooleanExtra("notification_submission_failed", false)) {
                UiUtil.showError(RootActivity.this, getString(R.string.error_title), getString(R.string.submission_fails));
                NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                manager.cancel(1000);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // checkForUpdate();
    }

    public void initializeGoogleService() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this).addApi(LocationServices.API).addConnectionCallbacks(connectionCallBack).addOnConnectionFailedListener(connectionFail).build();
        }
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean gpsProviderEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean networkProviderEnabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (!gpsProviderEnabled && !networkProviderEnabled) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setMessage("GPS is disabled in your device. Enable it?").setCancelable(false).setPositiveButton("Enable GPS", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivityForResult(callGPSSettingIntent, 5);
                }
            });
            alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            AlertDialog alert = alertDialogBuilder.create();
            alert.show();
        } else {
            mGoogleApiClient.connect();
        }
    }

    public void cleanupGoogleService() {
        if (mGoogleApiClient != null) {
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, locationListener);
            }
            mGoogleApiClient.disconnect();
        }
        mGoogleApiClient = null;
    }

    ConnectionCallbacks connectionCallBack = new ConnectionCallbacks() {
        @Override
        public void onConnectionSuspended(int arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onConnected(Bundle arg0) {
            if (ContextCompat.checkSelfPermission(RootActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getApplicationContext(),
                    android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(RootActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION)) {
                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                } else {
                    // No explanation needed, we can request the permission.
                    ActivityCompat.requestPermissions(RootActivity.this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            MY_PERMISSION_REQUEST_READ_FINE_LOCATION);
                }
            } else LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, REQUEST, locationListener);

            if (ContextCompat.checkSelfPermission(RootActivity.this,
                    Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(getApplicationContext(),
                            android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(RootActivity.this,
                        Manifest.permission.CAMERA)) {
                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                    boolean brkpt = true;
                } else {
                    // No explanation needed, we can request the permission.
                    ActivityCompat.requestPermissions(RootActivity.this,
                            new String[]{Manifest.permission.CAMERA},
                            MY_PERMISSION_REQUEST_CAMERA);
                }
            }
        }
    };

    LocationListener locationListener = new LocationListener() {

        @Override
        public void onLocationChanged(Location arg0) {
            currentLocation = arg0;
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mainLayout);
            if (fragment instanceof CustomerInformationFragment) {
                ((CustomerInformationFragment) fragment).updateLocation(currentLocation);
            }
        }
    };

    OnConnectionFailedListener connectionFail = new OnConnectionFailedListener() {

        @Override
        public void onConnectionFailed(ConnectionResult arg0) {

        }
    };

    private boolean animation;

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void initialize() {

        DashboardFragment dashboardFragment = new DashboardFragment();
        FragmentTransaction transaction = beginTransaction();
        transaction.replace(R.id.mainLayout, dashboardFragment).addToBackStack("dashboard");
        transaction.commit();
        updateCompanyDetails();

        getSupportFragmentManager().addOnBackStackChangedListener(new OnBackStackChangedListener() {

            @Override
            public void onBackStackChanged() {
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mainLayout);
                doPostActionOnAttachDetachFragment(fragment);
                // Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mainLayout);
                // setCompanyHeaderVisibility(fragment);
            }
        });
        retreivePendingJobs();
    }

    public void clearPendingJobs() {
        if (pendingRunnable != null) {
            pendingRunnable = null;
        }
    }

    public boolean isInitializedFully(PendingJobListener listener) {
        if (pendingRunnable != null) {
            return pendingRunnable.isInitializedFully(listener);
        }
        return true;
    }

    public void retreivePendingJobs() {
        if (pendingRunnable == null) {
            pendingRunnable = new PendingJobRunnable();
        }
        if (!pendingRunnable.isRunning()) {
            Thread t = new Thread(pendingRunnable);
            t.start();
        }
    }

    public void addOrUpdatePendingJob(PendingJobItem jobItem) {
        if (pendingRunnable == null) {
            pendingRunnable = new PendingJobRunnable();
        }
        pendingRunnable.addOrUpdatePendingJob(jobItem);
    }

    private void createNavigationDrawer() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        settingListView = (ExpandableListView) findViewById(R.id.list_slidermenu);
        settingListView.setItemsCanFocus(true);
        TypedArray settingString = getResources().obtainTypedArray(R.array.settings_menu);
        for (int i = 0; i < settingString.length(); i++) {
            MenuOption obj = new MenuOption();
            int titleId = settingString.getResourceId(i, 0);
            obj.setTitleId(titleId);
            obj.setItem(new Object());
            obj.setSelected(false);
            if (titleId == R.string.reportIssue && LibUtils.getConfigurationDetail() != null && !LibUtils.getConfigurationDetail().isReportIssue()) {
                continue;
            }
            settingListData.add(obj);
        }
        settingString.recycle();
        settingString = null;
        settingAdapter = new SettingsExpandableListAdapter(this, settingListData, settingListView, getApplicationContext());
        settingListView.setAdapter(settingAdapter);
        settingListView.setOnGroupClickListener(this);
        settingListView.setOnGroupExpandListener(this);

        mDrawerToggle = new ActionBarDrawerToggle(this, /* host Activity */
                mDrawerLayout, /* DrawerLayout object */
                R.drawable.logo_icf, /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open, /* "open drawer" description for accessibility */
                R.string.drawer_close /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                hideKeyboard();
                collapseAllExpandableList();
                supportInvalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                hideKeyboard();
                supportInvalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        // subHeaderRoot = findViewById(R.id.subheaderRoot);
        // subheader = (TextView) findViewById(R.id.subheader);
        // subHeaderImg = (ImageView) findViewById(R.id.subHeaderImg);
        updateCompanyDetails();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSION_REQUEST_READ_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, REQUEST, locationListener);
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    boolean brkpt = true;
                }
                return;
            }
            case MY_PERMISSION_REQUEST_CAMERA: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    boolean brkpt = true;
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    boolean brkpt = true;
                }
                return;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            int count = getSupportFragmentManager().getBackStackEntryCount();
            if (count != 1) {
                onBackPressed();
            }
        } else if (itemId == R.id.action_settings) {
            if (mDrawerLayout.isDrawerOpen(Gravity.END)) {
                mDrawerLayout.closeDrawer(Gravity.END);
                settingListData.remove(optionFaq);
                optionFaq = null;
            } else {
                //determine if FAQ option is available and add as necessary
                UtilityCompany company = LibUtils.getSelectedUtilityCompany();
                StringBuilder faqPath = new StringBuilder(Integer.toString(company.getId()))
                        .append("_").append(AppConstants.FAQ_FILE);
                File faqFile = FileUtil.getResourceBundleFile(faqPath.toString());
                if (faqFile.exists()) {
                    optionFaq = new MenuOption();
                    optionFaq.setTitleId(R.string.faq);
                    optionFaq.setItem(new Object());
                    optionFaq.setSelected(false);
                    settingListData.add(4, optionFaq);
                    settingAdapter.notifyDataSetChanged();
                }
                mDrawerLayout.openDrawer(Gravity.END);
            }
        } else {
            return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void dologout() {
        UiUtil.dismissSpinnerDialog();
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
        finish();
    }

    public void startLoadingProgress() {
        UiUtil.StartLoadingBarActivity(this, R.string.logout_loading_title, R.string.logout_loading_message, true, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
            }
        });

    }

    public void stopLoadingProgress() {
        UiUtil.stopLoadingBar();
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        if (requestCode == 5 && resultCode == 0) {
            LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            boolean gpsProviderEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            boolean networkProviderEnabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!gpsProviderEnabled && !networkProviderEnabled) {
                // GPS still not enabled..
            } else {
                if (mGoogleApiClient == null) {
                    mGoogleApiClient = new GoogleApiClient.Builder(this).addApi(LocationServices.API).addConnectionCallbacks(connectionCallBack).addOnConnectionFailedListener(connectionFail).build();
                }
                mGoogleApiClient.connect();
            }
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout != null && mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
            mDrawerLayout.closeDrawer(Gravity.RIGHT);
            return;
        }
        int count = getSupportFragmentManager().getBackStackEntryCount();
        if (count == 1) {
            RootActivity.this.moveTaskToBack(true);
        } else {
            hideKeyboard();
            if (count == 2) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                updateTitle(getResources().getString(R.string.dashboard_txt));
            }
            super.onBackPressed();
        }

    }

    private boolean logout() {
        LoginResponseBean bean = LibUtils.getLoggedInUserBean();
        String username = bean.getUserName();
        String password = bean.getPassword();
        String deviceId = Secure.getString(getContentResolver(), Secure.ANDROID_ID);
        try {
            UiUtil.showProgressDialog(this);
            if (!ConnectionManager.isOnline()) {
                UiUtil.dismissSpinnerDialog();
                dologout();
                return true;
            }
            ICFHttpManager.getInstance().doLogout(this, username, password, deviceId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public boolean onGroupClick(ExpandableListView parent, View view, int groupPosition, long id) {
        if (settingAdapter != null) {
            MenuOption menu = (MenuOption) settingAdapter.getGroup(groupPosition);
            if ((getResources().getString(menu.getTitleId())).equals(getResources().getString(R.string.home)) ||
                    (getResources().getString(menu.getTitleId())).equals(getResources().getString(R.string.logout))) {
                if ((getResources().getString(menu.getTitleId())).equals(getResources().getString(R.string.logout))) {
                    return logout();
                }
                return true;
            } else if ((getResources().getString(menu.getTitleId())).equals(getResources().getString(R.string.about))) {
                ICFDialogFragment aboutDialog = ICFDialogFragment.newDialogFrag(R.layout.about_screen_popup);
                aboutDialog.setDialogListener(new DialogListener() {
                    @Override
                    public void onFinishInflate(int layoutId, View view) {
                        if (layoutId == R.layout.about_screen_popup) {
                            TextView txtView = (TextView) view.findViewById(R.id.about_detail);
                            String version = getString(R.string.about_app) + getString(R.string.app_version);
                            txtView.setText(version);
                        }
                    }

                    @Override
                    public void onDismiss() {
                    }
                });
                aboutDialog.show(getSupportFragmentManager(), "about_popup");
                getSupportFragmentManager().executePendingTransactions();
                return true;
            } else if ((getResources().getString(menu.getTitleId())).equals(getResources().getString(R.string.reportIssue))) {
                UiUtil.showProgressDialog(this);
                String emailAddress = LibUtils.getConfigurationDetail().getEmailTo();
                LoggerUtils.getInstance().intiateLogDump(this, new String[]{emailAddress});
                return true;
            } else if ((getResources().getString(menu.getTitleId())).equals(getResources().getString(R.string.my_company_txt))) {
                Fragment frag = getSupportFragmentManager().findFragmentById(R.id.mainLayout);
                if (!(frag instanceof DashboardFragment)) {
                    return true;
                }
            } else if ((getResources().getString(menu.getTitleId())).equals(getResources().getString(R.string.faq))) {
                final ICFDialogFragment faqDialog = ICFDialogFragment.newDialogFrag(R.layout.faq_popup);
                faqDialog.setDialogListener(new DialogListener() {
                    @Override
                    public void onFinishInflate(int layoutId, View view) {
                        try {
                            byte[] data = FileUtil.getResourceBundleFileContent
                                    (LibUtils.getSelectedUtilityCompany().getId() + "_" + AppConstants.FAQ_FILE);
                            String contentHtml = new String(data);
                            WebView faq = (WebView) view.findViewById(R.id.faq);
                            faq.loadDataWithBaseURL("", contentHtml, "text/html", "UTF-8", "");
                        } catch (Exception e) {
                            //e.printStackTrace();
                            faqDialog.dismiss();
                        }
                    }

                    @Override
                    public void onDismiss() {
                    }
                });

                faqDialog.show(getSupportFragmentManager(), "faq_popup");
                getSupportFragmentManager().executePendingTransactions();
                return true;
            }
        }
        return false;
    }

    @Override
    public void connectionError(Message msg) {
        if (msg != null) {
            if (msg.arg1 == ICFHttpManager.REQ_ID_LOGOUT) {
                UiUtil.dismissSpinnerDialog();
                dologout();
                return;
            }
        }
        UiUtil.showNetworkError(msg, this);
    }

    @Override
    public void networkConnectivityError(Message msg) {
        if (msg != null && msg.arg1 == ICFHttpManager.REQ_ID_LOGOUT) {
            UiUtil.dismissSpinnerDialog();
            dologout();
            return;
        }
        UiUtil.showNetworkError(msg, this);
    }

    @Override
    public void dataReceived(final Message msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (msg.arg1) {
                    case ICFHttpManager.REQ_ID_CHANGE_PASSWORD: {
                        LoginResponseBean bean = (LoginResponseBean) msg.obj;
                        if (bean != null) {
                            if (bean.isSuccessResponse()) {
                                if (passwordChange != null && passwordChange.length() > 0) {
                                    bean.setPassword(passwordChange);
                                }
                                LibUtils.setLoggedInUserBean(bean);
                                UiUtil.dismissSpinnerDialog();
                                UiUtil.showError(RootActivity.this, getString(R.string.successful_title), bean.getStatusMessage());
                            } else {
                                // TODO: show error message and take the user back to the change
                                // password dialog.
                                UiUtil.dismissSpinnerDialog();
                                UiUtil.showError(RootActivity.this, getString(R.string.error_title), bean.getStatusMessage());
                            }
                        }

                    }
                    break;
                    case ICFHttpManager.REQ_ID_LOGOUT: {
                        UiUtil.dismissSpinnerDialog();
                        dologout();
                    }

                }
            }
        });
    }

    public void updateTitle(String title) {
        int titleId = 0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            titleId = getResources().getIdentifier("action_bar_title", "id", "android");
        } else
//	{
//	    titleId = R.id.action_bar_title;
//	}
            // Final check for non-zero invalid id
            if (titleId > 0) {
                TextView titleTextView = (TextView) findViewById(titleId);
                titleTextView.setText(title);
                DisplayMetrics metrics = getResources().getDisplayMetrics();
                LinearLayout.LayoutParams txvPars = (android.widget.LinearLayout.LayoutParams) titleTextView.getLayoutParams();
                txvPars.gravity = Gravity.CENTER_HORIZONTAL;
                txvPars.width = metrics.widthPixels;
                titleTextView.setLayoutParams(txvPars);
                titleTextView.setGravity(Gravity.CENTER);

                ViewGroup vg = (ViewGroup) titleTextView.getParent();
                final View view = (View) vg.getParent();
                if (view instanceof LinearLayout) {
                    ((LinearLayout) view).setClickable(true);
                    ((LinearLayout) view).setOnClickListener(null);
                    final int upId = getResources().getIdentifier("up", "id", "android");
                    if (upId > 0) {
                        final View upView = ((LinearLayout) view).findViewById(upId);
                        View parentView = (View) upView.getParent();
                        parentView.post(new Runnable() {

                            @Override
                            public void run() {
                                Rect outRect = new Rect();
                                upView.getHitRect(outRect);
                                outRect.right += 60;
                                TouchDelegate delegate = new TouchDelegate(outRect, upView);
                                // ((View) upView.getParent()).setTouchDelegate(delegate);
                                ((LinearLayout) view).setTouchDelegate(delegate);
                                upView.setOnClickListener(new OnClickListener() {

                                    @Override
                                    public void onClick(View v) {
                                        onBackPressed();
                                    }
                                });

                            }
                        });
                    }
                }
            }
        android.support.v4.app.Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mainLayout);
        if (fragment != null && fragment.isVisible() && fragment instanceof DashboardFragment) {
            if (fragment instanceof DashboardFragment) {
                getSupportActionBar().setDisplayShowHomeEnabled(false);
            } else {
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }
        } else {
            getSupportActionBar().setDisplayShowHomeEnabled(false);
        }
    }

    private void collapseAllExpandableList() {
        if (settingAdapter != null) {
            int size = settingAdapter.getGroupCount();
            for (int i = 0; i < size; i++) {
                settingListView.collapseGroup(i);
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (lastStopTime != -1) {
            long diff = System.currentTimeMillis() - lastStopTime;
            long maxIdleTimeForPin = LibUtils.getConfigurationDetail().getSessionExpiryTime();
            long maxIdleTimeForLogout = LibUtils.getConfigurationDetail().getSessionExpTimeLogout() * DateUtils.MINUTE_IN_MILLIS;
            maxIdleTimeForPin = maxIdleTimeForPin * DateUtils.MINUTE_IN_MILLIS;
            // If time properties are 0 default to 10000 minutes
            if (maxIdleTimeForPin <= 0) {
                maxIdleTimeForPin = 10000 * DateUtils.MINUTE_IN_MILLIS;
            }
            if (maxIdleTimeForLogout <= 0) {
                maxIdleTimeForLogout = 10000 * DateUtils.MINUTE_IN_MILLIS;
            }
            if (diff > maxIdleTimeForLogout) {
                logout();
            } else if (diff > maxIdleTimeForPin) {
                displayValidatePinScreen();
            }
        }
    }

    private void displayValidatePinScreen() {
        ICFPINDialog.validatePINAfterIdleTime(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        lastStopTime = System.currentTimeMillis();
        cleanupGoogleService();
    }

//    public Location getCurrentLocation() {
//        if (currentLocation == null) {
//            return LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
//        }
//        return currentLocation;
//    }

    public void updateCompanyDetails() {
        utilityCompanyHeader = (TextView) findViewById(R.id.companyTitle);
        utilityCompanyImg = (ImageView) findViewById(R.id.companyImg);
        UtilityCompany company = LibUtils.getSelectedUtilityCompany(); // TODO THIS IS CAUSEING A BUG - Returning null after a picture intent or other times
        // resuming application from stopped state
        // TODO remove this work around when the root of the problem is found
        if (company == null) {
            return;
        }
        utilityCompanyHeader.setText(company.getName());

        Bitmap bitmap = null;
        int newHeight = 0, newWidth = 0;
        try {
            InputStream istr = new FileInputStream(FileUtil.getResourceBundleFile(company.getIcon()));
            // bitmap = BitmapDrawable.createFromStream(istr, null);
            BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
            bitmap = BitmapFactory.decodeStream(istr, null, bitmapOptions);
            // Need to scale the bitmap
            DisplayMetrics dm = getResources().getDisplayMetrics();
            newHeight = (int) (dm.density * 32);
            newWidth = newHeight * bitmapOptions.outWidth / bitmapOptions.outHeight;
            bitmap = UiUtil.scaleBitmap(bitmap, newWidth, newHeight);
            istr.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (bitmap != null) {
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(newWidth, newHeight);
            utilityCompanyImg.setLayoutParams(lp);
            utilityCompanyImg.setImageBitmap(bitmap);
        } else {
            utilityCompanyImg.setImageResource(R.drawable.default_utility_company_logo);
        }

        // if (UiUtil.getResource(UiUtil.selectedId.getIcon()) == 0)
        // {
        // utilityCompanyHeader.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icf_logo, 0, 0, 0);
        // }
        // else
        // {
        // utilityCompanyHeader.setCompoundDrawablesWithIntrinsicBounds(UiUtil.getResource(UiUtil.selectedId.getIcon()), 0, 0, 0);
        // }
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
        doPostActionOnAttachDetachFragment(fragment);
    }

    private void doPostActionOnAttachDetachFragment(Fragment fragment) {
        if (!(fragment instanceof DialogFragment)) {
            if (settingAdapter != null) {
                if ((fragment instanceof DashboardFragment)) {
                    settingAdapter.update(true);
                } else {
                    settingAdapter.update(false);
                }
            }

            if (utilityCompanyHeaderRoot != null) {
                if ((fragment instanceof DashboardFragment) || (fragment instanceof PendingJobsFragment) || (fragment instanceof CustomerInformationFragment)) {
                    utilityCompanyHeaderRoot.setVisibility(View.VISIBLE);
                } else {
                    utilityCompanyHeaderRoot.setVisibility(View.GONE);
                }
            }
        }
    }

    public ArrayList<PendingJobItem> getPendingJobItems() {
        return pendingRunnable.getPendingJobItems();
    }

    /*
     * public UploadProgressListener progressListener = new UploadProgressListener() {
     * 
     * @Override public void updateProgress(int progress, Object obj) {
     * 
     * if (obj instanceof PendingJobItem) { UiUtil.updateSubmissionProgress(((PendingJobItem) obj).getCustomerName(), progress); Fragment frag =
     * RootActivity.this.getSupportFragmentManager().findFragmentById(R.id.mainLayout); if (frag instanceof PendingJobsFragment) { ((PendingJobsFragment)
     * frag).refreshAdapter(); } } }
     * 
     * @Override public void onSuccess(Object obj) { if (obj instanceof PendingJobItem) { UiUtil.removeSubmissionProgress(((PendingJobItem)
     * obj).getCustomerName()); ((PendingJobItem) obj).setJobState(CustomerInfo.SUBMITTED); RebateManager.getInstance().updateFormState(((PendingJobItem)
     * obj).getBean(), CustomerInfo.SUBMITTED); } Fragment frag = RootActivity.this.getSupportFragmentManager().findFragmentById(R.id.mainLayout); if (frag
     * instanceof PendingJobsFragment) { ((PendingJobsFragment) frag).refreshAdapter(); } }
     * 
     * public void onFailure(Object obj) { if (obj instanceof PendingJobItem) { UiUtil.removeSubmissionProgress(((PendingJobItem) obj).getCustomerName());
     * ((PendingJobItem) obj).setJobState(CustomerInfo.SUBMISSION_ERROR); RebateManager.getInstance().updateFormState(((PendingJobItem) obj).getBean(),
     * CustomerInfo.SUBMISSION_ERROR); } Fragment frag = RootActivity.this.getSupportFragmentManager().findFragmentById(R.id.mainLayout); if (frag instanceof
     * PendingJobsFragment) { ((PendingJobsFragment) frag).refreshAdapter(); } } };
     */
    @Override
    public void onGroupExpand(int groupPosition) {
        if (settingAdapter != null && groupPosition != previousGroup && previousGroup != -1) {
            MenuOption option = (MenuOption) settingAdapter.getGroup(previousGroup);
            option.setSelected(false);
            settingListView.collapseGroup(previousGroup);
            hideKeyboard();
        }
        MenuOption option1 = (MenuOption) settingAdapter.getGroup(groupPosition);
        option1.setSelected(true);
        previousGroup = groupPosition;

    }

    public void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            view.clearFocus();

            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public FragmentTransaction beginTransaction() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (animation) {
            // transaction.setCustomAnimations(R.anim.slide_left, R.anim.slide_right, R.anim.pop_enter, R.anim.pop_exit);
            transaction.setCustomAnimations(R.anim.slide_left, R.anim.slide_right, R.anim.slide_left, R.anim.slide_right);
        } else {
            // transaction.setCustomAnimations(R.anim.slide_up, R.anim.slide_down);
            transaction.setCustomAnimations(R.anim.slide_up, R.anim.slide_down, R.anim.slide_up, R.anim.slide_down);
        }
        return transaction;
    }

}
