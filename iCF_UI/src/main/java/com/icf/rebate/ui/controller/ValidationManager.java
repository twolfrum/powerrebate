package com.icf.rebate.ui.controller;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.regex.Pattern;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.ui.util.UiUtil;
import com.icf.rebate.networklayer.model.CustomerScreenItem;

public class ValidationManager {
    private static ValidationManager mInstance;

    private Hashtable<String, ArrayList<ValidationView>> validationHashtable;

    public static EmptyValidationRule emptyRule = new EmptyValidationRule();

    public static EmailValidationRule emailRule = new EmailValidationRule();

    public static RegexValidationRule phoneRule = new RegexValidationRule("\\+?(-|\\s)?1?\\s?\\(?\\d{3}\\)?(-|\\s)?\\d{3}(\\s|-)?\\d{4}");

    public static RegexValidationRule nameRule = new RegexValidationRule("[A-Za-z0-9\\.\\,\\s']+");

    static Pattern EMAIL = Pattern.compile("[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}");

    private ValidationManager() {
        validationHashtable = new Hashtable<String, ArrayList<ValidationView>>();
    }

    public static ValidationManager getInstance() {
        if (mInstance == null) {
            mInstance = new ValidationManager();
        }
        return mInstance;
    }

    public void clearViewList(String name) {
        ArrayList<ValidationView> list = validationHashtable.get(name);
        if (list != null) {
            validationHashtable.remove(name);
        }
    }

    public void addValidationView(View view, String name, ValidationRule rule, int messageId, boolean optional) {
        ArrayList<ValidationView> list = validationHashtable.get(name);
        if (list == null) {
            list = new ArrayList<ValidationView>();
            validationHashtable.put(name, list);
        }
        ValidationView vv = new ValidationView(view, rule, messageId);
        vv.setOptional(optional);
        list.add(vv);
    }

    public void addValidationView(View view, String name, ValidationRule rule, int messageId) {
        ArrayList<ValidationView> list = validationHashtable.get(name);
        if (list == null) {
            list = new ArrayList<ValidationView>();
            validationHashtable.put(name, list);
        }
        list.add(new ValidationView(view, rule, messageId));
        if (rule instanceof EmptyValidationRule && view instanceof EditText) {
            ((EditText) view).addTextChangedListener(new MandatoryTextWatcher((EditText) view));
        }
    }

    public void addValidationView(View view, String name, ValidationRule rule,
                                  int messageId, String[] messageParms) {
        ArrayList<ValidationView> list = validationHashtable.get(name);
        if (list == null) {
            list = new ArrayList<ValidationView>();
            validationHashtable.put(name, list);
        }
        list.add(new ValidationView(view, rule, messageId, messageParms));
        if (rule instanceof EmptyValidationRule && view instanceof EditText) {
            ((EditText) view).addTextChangedListener(new MandatoryTextWatcher((EditText) view));
        }
    }

    public static LengthValidationRule lengthValidationRule(CustomerScreenItem csi) {
        String minLen = csi.getMinLength();
        String maxLen = csi.getMaxLength();
        LengthValidationRule rule = null;
        if (minLen != null || maxLen != null) {
            rule = new LengthValidationRule(minLen, maxLen, csi.getTitle());
        }
        return rule;
    }

    public static RangeValidationRule rangeValidationRule(CustomerScreenItem csi) {
        String minVal = csi.getMinValue();
        String maxVal = csi.getMaxValue();

        RangeValidationRule rule = null;
        if (minVal != null && maxVal != null) {
            rule = new RangeValidationRule(minVal, maxVal);
        }
        return rule;
    }

    public boolean doValidation(String name) {
        ArrayList<ValidationView> list = validationHashtable.get(name);
        for (ValidationView tempView : list) {
            if (tempView.validate()) {
                return false;
            }
        }
        return true;
    }

    public static class ValidationView {
        private View view;
        private ValidationRule rule;
        private int messageId;
        private String[] messageParms;
        private boolean optional;

        public ValidationView(View view) {
            this.view = view;
        }

        public void setOptional(boolean optional) {
            this.optional = optional;
        }

        public boolean isOptional() {
            return this.optional;
        }

        public ValidationView(View view, ValidationRule rule, int messageId) {
            this.view = view;
            this.rule = rule;
            this.messageId = messageId;
        }

        public ValidationView(View view, ValidationRule rule, int messageId, String[] messageParms) {
            this.view = view;
            this.rule = rule;
            this.messageId = messageId;
            this.messageParms = messageParms;
        }

        public View getView() {
            return view;
        }

        public ValidationRule getRule() {
            return rule;
        }

        public boolean validate() {
            if (rule == null || view == null) {
                return true;
            }
            return rule.doValidation(view, messageId, optional, messageParms);
        }

    }

    public static abstract class ValidationRule {
        public abstract boolean doValidation(View view, int errorId,
                                             boolean optional, String[] errorParms);
    }

    public static void showError(int errorId) {
        Activity activity = UiUtil.rootActivity.get();
        if (activity != null) {
            UiUtil.showError(activity, activity.getString(R.string.alert_title), activity.getString(errorId));
        }
    }

    public static void showError(int errorId, Object[] msgParms) {
        if (msgParms == null) {
            showError(errorId);
            return;
        }
        Activity activity = UiUtil.rootActivity.get();
        if (activity != null) {
            UiUtil.showError(activity, activity.getString(R.string.alert_title), activity.getString(errorId, msgParms));
        }
    }

    public static void showError(String err) {
        Activity activity = UiUtil.rootActivity.get();
        if (activity != null) {
            UiUtil.showError(activity, activity.getString(R.string.alert_title), err);
        }
    }

    public static class EmptyValidationRule extends ValidationRule {

        @Override
        public boolean doValidation(View view, int resId, boolean optional, String[] errorParms) {
            String data = ((TextView) view).getText().toString();
            data = data.trim();
            if (TextUtils.isEmpty(data)) {
                if (!optional) {
                    showError(resId, errorParms);
                    return true;
                }
            }
            return false;
        }
    }

    public static class EmailValidationRule extends ValidationRule {

        @Override
        public boolean doValidation(View view, int errorId, boolean optional, String[] errorParms) {
            String data = ((EditText) view).getText().toString();
            data = data.trim();
            if (!TextUtils.isEmpty(data)) {
                if (!EMAIL.matcher(data).matches()) {
                    showError(errorId, errorParms);
                    return true;
                }
            } else {
                if (!optional) {
                    showError(errorId, errorParms);
                    return true;
                }
            }

            return false;
        }
    }

    public static class RegexValidationRule extends ValidationRule {
        String mPatternStr;

        public RegexValidationRule(String regexPattern) {
            mPatternStr = regexPattern;
        }

        @Override
        public boolean doValidation(View view, int errorId, boolean optional, String[] errorParms) {
            Pattern pattern = Pattern.compile(mPatternStr);
            String data = ((EditText) view).getText().toString();
            data = data.trim();
            if (!TextUtils.isEmpty(data)) {
                if (!pattern.matcher(data).matches()) {
                    showError(errorId, errorParms);
                    return true;
                }
            } else {
                if (!optional) {
                    showError(errorId, errorParms);
                    return true;
                }
            }

            return false;
        }
    }

    public static class LengthValidationRule extends ValidationRule {
        private int minLength;
        private int maxLength;
        private String label;

        public LengthValidationRule(String minLength, String maxLength, String label) {
            if (!TextUtils.isEmpty(minLength)) {
                this.minLength = Integer.parseInt(minLength);
            }
            if (!TextUtils.isEmpty(maxLength)) {
                this.maxLength = Integer.parseInt(maxLength);
            }
            this.label = label;
        }

        @Override
        public boolean doValidation(View view, int errorId, boolean optional, String[] errorParms) {
            if (minLength + maxLength == 0) return false;

            String data = ((EditText) view).getText().toString();
            data = data.trim();
            //don't enforce if nothing has been entered
            if (TextUtils.isEmpty(data)) return false;

            if (minLength == maxLength && data.length() != minLength) {
                showError(R.string.validation_value_length, new Object[]{label, minLength});
                return true;
            } else if (minLength > 0 && maxLength > 0) {
                if (data.length() < minLength || data.length() > maxLength) {
                    showError(R.string.validation_value_length_range, new Object[]{label, minLength, maxLength});
                    return true;
                }
            } else if (minLength > 0 && data.length() < minLength) {
                showError(R.string.validation_value_length_min, new Object[]{label, minLength});
                return true;
            } else if (maxLength > 0 && data.length() > maxLength) {
                showError(R.string.validation_value_length_max, new Object[]{label, maxLength});
                return true;
            }
            return false;
        }
    }

    public static class RangeValidationRule extends ValidationRule {
        private String minValue, maxValue;
        private float fMinValue, fMaxValue;

        public RangeValidationRule(String minValue, String maxValue) {
            this.minValue = minValue;
            this.maxValue = maxValue;

            try {
                fMinValue = Float.parseFloat(minValue.trim().replace(",", ""));
            } catch (NumberFormatException e) {
                fMinValue = 0;
            }

            try {
                fMaxValue = Float.parseFloat(maxValue.trim().replace(",", ""));
            } catch (NumberFormatException e) {
                fMaxValue = 0;
            }
        }

        @Override
        public boolean doValidation(View view, int resId, boolean optional, String[] errorParms) {
            String data = ((EditText) view).getText().toString().trim().replace(",", "");
            if (TextUtils.isEmpty(data) && optional) {
                return false;
            }

            float fValue;
            try {
                fValue = Float.parseFloat(data);
            } catch (NumberFormatException e) {
                fValue = 0;
            }

            if (fValue < fMinValue || fValue > fMaxValue) {
                showError(resId, new String[]{minValue, maxValue});
                return true;
            }
            return false;
        }
    }

}
