package com.icf.rebate.ui.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.net.ParseException;
import android.text.format.DateUtils;

public class DateFormatter
{

    private static final String DAY = "EEEE"; // eg : <TUESDAY>
    private static final String YESTERDAY = "Yesterday";
    private static final String TODAY = "Today";
    private static final String DATE_MONTH_YEAR_SHORT = "MMM d";// eg : <sep 05>
    private static final String DATE_MONTH_YEAR = "d MMM yyyy";// eg : <sep 05, 2014>
    private static final String DATE_MONTH_YEAR_TIME = "d MMM yyyy hh:mm a";// eg : <sep 05, 2014 11:24 AM>
    private static final String TIME = "hh:mm a";// eg : <11:24 AM>
    private static final String SIMPLE_FORMAT = "yyyy-MM-dd";
    private static final String N_A = "n/a";

    public static String getFormattedDate(long timestamp)
    {
	if(DateUtils.isToday(timestamp)) {
	    return "Today " + new SimpleDateFormat(TIME).format(new Date(timestamp));
	} else {
	    return new SimpleDateFormat(DATE_MONTH_YEAR_TIME).format(new Date(timestamp));
	}
//
//	SimpleDateFormat tempFormat = new SimpleDateFormat(SIMPLE_FORMAT);
//
//	String date = tempFormat.format(compareTimeDate);
//	String currentDate = tempFormat.format(Calendar.getInstance().getTime());


	/*
	 * if (timestamp <= 0 || currentDate.equalsIgnoreCase(date) == true) {
	 * 
	 * // Time is not in correct format, so show the date as "Today". return TODAY; } else if (timestamp > last7days.getTimeInMillis()) { SimpleDateFormat
	 * df = new SimpleDateFormat(DAY); String reportDate = df.format(timestamp); return reportDate; } else {
	 */
	// It's not today/yesterday/Within 7 days so show the date.
//
//	try
//	{
////	    Date parseDate = tempFormat.parse(date);
//	    SimpleDateFormat displayDateFormat = new SimpleDateFormat(DATE_MONTH_YEAR);
//	    return displayDateFormat.format(compareTimeDate);
//		    
////	    String displayDate = displayDateFormat.format(parseDate);
////	    displayDateFormat = null;
////	    parseDate = null;
////	    return displayDate;
//	}
//	catch (ParseException e)
//	{
//	    e.printStackTrace();
//	    return N_A;
//	}
//	catch (java.text.ParseException e)
//	{
//	    e.printStackTrace();
//	    return N_A;
//	}
	// }
    }

    private static Calendar clearTimes(Calendar c)
    {
	c.set(Calendar.HOUR_OF_DAY, 0);
	c.set(Calendar.MINUTE, 0);
	c.set(Calendar.SECOND, 0);
	c.set(Calendar.MILLISECOND, 0);
	return c;
    }

}
