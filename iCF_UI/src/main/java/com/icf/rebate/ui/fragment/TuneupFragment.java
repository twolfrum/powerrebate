package com.icf.rebate.ui.fragment;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.icf.rebate.networklayer.model.CustomerInfo;
import com.icf.rebate.networklayer.model.FormResponseBean.Options;
import com.icf.rebate.networklayer.model.FormResponseBean.Part;
import com.icf.rebate.networklayer.model.FormResponseBean.TuneUp;
import com.icf.rebate.networklayer.model.FormResponseBean.TuneUpItem;
import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.ui.controller.CustomTextWatcher;
import com.icf.rebate.ui.controller.FocusManager;
import com.icf.rebate.ui.controller.FormUIManager;
import com.icf.rebate.ui.controller.FormUIManager.FormViewCreationCallback;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.listeners.FragmentCallFlow;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.ICFLogger;
import com.icf.rebate.ui.util.UiUtil;
import com.icf.rebate.ui.util.UiViewUtils;
import com.icf.rebate.ui.util.rebate.EnthalpyTable;
import com.icf.rebate.ui.util.rebate.ICFSQLiteOpenHelper;
import com.icf.rebate.ui.util.rebate.TuneUpRebateCalculation;

public class TuneupFragment extends Fragment implements FragmentCallFlow, OnCheckedChangeListener, FormViewCreationCallback {
    public static final String TAG = TuneupFragment.class.getSimpleName();
    private float SYSTEM_EFFICIENCY_THERSHOLD = 0;
    private TuneUpItem tuneUpItemBean;
    private TuneUp tuneUpBean;
    private FormUIManager uiManager;
    private SQLiteDatabase database;
    private View switchContainer;
    LinearLayout scrollViewLayout;
    Switch switch1;
    TextView switcherLeftTxtView, switcherRightTxtView;
    EditText actualCapacity;
    EditText systemEfficiency;
    Spinner tonnage;
    EditText systemNominalCapacity;
    EditText result;
    EditText eligible;
    EditText pageName;
    int index = -1;
    boolean switchState = false;
    private TuneUpRebateCalculation rebateTable = null;
    private boolean isDeleted = false;

    FocusManager focusManager;
    private String title;
    private EffectiveEfficiencyManager effEffMgr;

    public TuneupFragment() {
        super();
    }

    public static TuneupFragment newInstance(TuneUpItem tuneUpItemBean, TuneUp tuneUpBean, int index) {
        TuneupFragment frag = new TuneupFragment();
        frag.rebateTable = new TuneUpRebateCalculation(tuneUpBean == null ? "tuneUp" : tuneUpBean.getJsonId());
        frag.SYSTEM_EFFICIENCY_THERSHOLD = TuneUpRebateCalculation.getSystemEfficiencyThreshold();
        frag.tuneUpItemBean = tuneUpItemBean;
        frag.index = index;
        frag.tuneUpBean = tuneUpBean;
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.form_equipment_page_layout, null, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getUiControls(view);
        initialize(view);
        bindCallbacks(view);
        requestData();

        try {
            ICFSQLiteOpenHelper helper = new ICFSQLiteOpenHelper(LibUtils.getApplicationContext());
            database = helper.getReadableDatabase();
        } catch (Exception e) {
            ICFLogger.e(TAG, e);
        }

        createViews();

        CustomerInfo cInfo = RebateManager.getInstance().getFormBean().getCustomerInfo();
        if (UiUtil.isJobCompleted(cInfo)) {
            UiUtil.setEnableView((ViewGroup) view, false, null);
        }
        focusManager = FocusManager.newFocusManager(view);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //
        // if (requestCode == IManifoldDummyActivity.IManifoldResultCode && data != null)
        // {
        // if (data.hasExtra("actual_capacity"))
        // {
        // String actualCapacity = data.getStringExtra("actual_capacity");
        // if (this.actualCapacity != null && actualCapacity != null)
        // {
        // this.actualCapacity.setText(actualCapacity);
        // computeEfficiency();
        // }
        // }
        // }
    }

    public void computeEfficiency() {
        if (this.systemNominalCapacity != null && !TextUtils.isEmpty(this.systemNominalCapacity.getText()) && actualCapacity != null && !TextUtils.isEmpty(this.actualCapacity.getText())) {
            // subtract previous rebate amount on start of compute method then add new rebate at the end
            float rebateValue = tuneUpItemBean.getRebateValue();
            if (RebateManager.getInstance().getFormBean() != null) {
                float prevAmmount = RebateManager.getInstance().getFormBean().getTuneUpRebate();
                RebateManager.getInstance().getFormBean().setTuneUpRebate(prevAmmount - rebateValue);
            }
            try {
                int systemEfficiency = (int) ((LibUtils.parseFloat(actualCapacity.getText().toString(), 0) / LibUtils.parseFloat(systemNominalCapacity.getText().toString(), 0)) * 100);
                if (this.systemEfficiency != null) {
                    this.systemEfficiency.setText(systemEfficiency + "");
                    this.systemEfficiency.setBackground(getActivity().getResources().getDrawable(R.drawable.bg_dialog_box));
                }

                boolean isEligible = systemEfficiency >= SYSTEM_EFFICIENCY_THERSHOLD;

                if (!isEligible) {
                    ArrayList<Part> partList = null;
                    if (tuneUpItemBean.getPackages() != null) {
                        partList = tuneUpItemBean.getPackages().getParts();
                    } else {
                        partList = tuneUpItemBean.getSplit().getParts();
                    }
                    if (partList != null) {
                        for (Part part : partList) {
                            if (part.getId().equalsIgnoreCase(AppConstants.ELIGIBILITY)) {
                                List<Options> optionList = part.getOptions();
                                if (optionList != null && optionList.size() > 0) {
                                    optionList.get(0).setSavedValue("Not Eligible");
                                }
                            }
                        }
                    }
                    if (eligible != null) {
                        eligible.setText("Not Eligible");
                        eligible.setTextColor(Color.RED);
                    }
                    tuneUpItemBean.setRebateValue(0);
                } else {

                    // KVB ... Keep getting errors here not sure why there was no null checking in place for package not existing?
                    ArrayList<Part> partList = null;
                    if (tuneUpItemBean.getPackages() != null) {
                        partList = tuneUpItemBean.getPackages().getParts();
                    } else {
                        partList = tuneUpItemBean.getSplit().getParts();
                    }

                    if (partList != null) {
                        for (Part part : partList) {
                            if (part.getId() == null) continue;
                            if (part.getId().equalsIgnoreCase(AppConstants.ELIGIBILITY)) {
                                List<Options> optionList = part.getOptions();
                                if (optionList != null && optionList.size() > 0) {
                                    optionList.get(0).setSavedValue("Eligible");
                                }
                            }
                        }
                    }
                    if (eligible != null) {
                        eligible.setText("Eligible");
                        eligible.setTextColor(Color.parseColor("#458B00"));
                    }

                    float tonnageValue = 0;
                    if (tonnage != null) {
                        tonnageValue = LibUtils.parseFloat((String) tonnage.getSelectedItem(), 0);
                    }
                    tuneUpItemBean.setRebateValue(rebateTable.getIncentive(tonnageValue));
                }
                //$$$TBD SHIT
                String formatedValue = tuneUpItemBean.getStaticRebateValue();
                if (TextUtils.isEmpty(formatedValue)) {
                    formatedValue = (String.format("%.02f", tuneUpItemBean.getRebateValue()));
                }
                this.result.setText("" + formatedValue);
                this.result.setBackground(getActivity().getResources().getDrawable(R.drawable.bg_dialog_box));
                ICFLogger.d(TAG, "Result:" + "$" + formatedValue + "");
                // tuneUpItemBean.setEligibility(isEligible);
            } catch (Exception e) {
                Log.e(TAG, "Error occured during rebate calculation", e);
                e.printStackTrace();
            }
            rebateValue = tuneUpItemBean.getRebateValue();
            if (RebateManager.getInstance().getFormBean() != null) {
                float prevAmmount = RebateManager.getInstance().getFormBean().getTuneUpRebate();
                RebateManager.getInstance().getFormBean().setTuneUpRebate(prevAmmount + rebateValue);
            }
        }
        // If threshold is 0 then run the calculation regardless of systemEfficiency
        else if (SYSTEM_EFFICIENCY_THERSHOLD == 0) {
            if (isValidForm())
                getRebateIncentive();
        } else {
            tuneUpItemBean.setRebateValue(0);
        }
    }

    /**
     * Check mandatory fields are filled
     */
    private boolean isValidForm() {
        if (tuneUpItemBean != null) {
            List<Part> parts = null;
            if (tuneUpItemBean.isPackageSelected() && tuneUpItemBean.getPackages() != null) {
                parts = tuneUpItemBean.getPackages().getParts();
            } else if (tuneUpItemBean.getSplit() != null) {
                parts = tuneUpItemBean.getSplit().getParts();
            }

            int formState = UiUtil.validateForm(parts);
            return formState == AppConstants.FORM_ITEM_FILLED_STATE.ITEM_COMPLETELY_SAVED.getState();
        }
        return false;
    }

    /**
     * Gets the rebate incentive found by the lookup in {@link TuneUpRebateCalculation}
     */
    private void getRebateIncentive() {
        // Subtract previous rebate amount on start of compute method then add new rebate at the end
        float rebateValue = tuneUpItemBean.getRebateValue();
        if (RebateManager.getInstance().getFormBean() != null) {
            float prevAmmount = RebateManager.getInstance().getFormBean().getTuneUpRebate();
            RebateManager.getInstance().getFormBean().setTuneUpRebate(prevAmmount - rebateValue);
        }
        ArrayList<Part> partList = null;
        if (tuneUpItemBean.getPackages() != null) {
            partList = tuneUpItemBean.getPackages().getParts();
        } else {
            partList = tuneUpItemBean.getSplit().getParts();
        }

        if (partList != null) {
            for (Part part : partList) {
                if (part.getId() == null) continue;
                if (part.getId().equalsIgnoreCase(AppConstants.ELIGIBILITY)) {
                    List<Options> optionList = part.getOptions();
                    if (optionList != null && optionList.size() > 0) {
                        optionList.get(0).setSavedValue("Eligible");
                    }
                }
            }
        }
        if (eligible != null) {
            eligible.setText("Eligible");
            eligible.setTextColor(Color.parseColor("#458B00"));
        }

        float tonnageValue = 0;
        if (tonnage != null) {
            tonnageValue = LibUtils.parseFloat((String) tonnage.getSelectedItem(), 0);
        }
        tuneUpItemBean.setRebateValue(rebateTable.getIncentive(tonnageValue));

        //$$$TBD SHIT
        String formatedValue = tuneUpItemBean.getStaticRebateValue();
        if (TextUtils.isEmpty(formatedValue)) {
            formatedValue = (String.format("%.02f", tuneUpItemBean.getRebateValue()));
        }
        this.result.setText("" + formatedValue);
        this.result.setBackground(getActivity().getResources().getDrawable(R.drawable.bg_dialog_box));
        ICFLogger.d(TAG, "Result:" + "$" + formatedValue + "");
        updateRebate();
    }

    private void updateRebate() {
        float rebateValue = tuneUpItemBean.getRebateValue();
        if (RebateManager.getInstance().getFormBean() != null) {
            float prevAmmount = RebateManager.getInstance().getFormBean().getTuneUpRebate();
            RebateManager.getInstance().getFormBean().setTuneUpRebate(prevAmmount + rebateValue);
        }
    }

    private void createViews() {
        List<Part> partList = null;
        if (tuneUpItemBean.isPackageSelected() && tuneUpItemBean.getPackages() != null) {
            partList = tuneUpItemBean.getPackages().getParts();
        } else if (!tuneUpItemBean.isPackageSelected() && tuneUpItemBean.getSplit() != null) {
            partList = tuneUpItemBean.getSplit().getParts();
        }

        if (partList != null) {
            scrollViewLayout.removeAllViews();
            boolean isFormFilled = tuneUpItemBean.getFormFilledState() != AppConstants.FORM_ITEM_FILLED_STATE.ITEM_UNEDITED.getState();
            uiManager.createViews(getParentFragment(), scrollViewLayout, partList, isFormFilled);
        }

        // Set initial visibility of "After" fields as necessary
        if (effEffMgr != null) {
            effEffMgr.evaluateEfficiency();
        }
    }

    @Override
    public void requestData() {

    }

    @Override
    public void getUiControls(View root) {
        switch1 = (Switch) root.findViewById(R.id.equipment_type);
        switch1.setTextOn("       ");
        switch1.setTextOff("       ");
        switcherLeftTxtView = (TextView) root.findViewById(R.id.switcherLeftTxtView);
        switcherRightTxtView = (TextView) root.findViewById(R.id.switcherRightTxtView);
        scrollViewLayout = (LinearLayout) root.findViewById(R.id.scrol_layout);
        switchContainer = root.findViewById(R.id.switch_parent_layout);
    }

    @Override
    public void initialize(View root) {
        if (!checkForSplitPackage(tuneUpItemBean)) {
            switchContainer.setVisibility(View.GONE);
        }
        uiManager = new FormUIManager(getString(R.string.tuneUpId));
        uiManager.setFormViewCreationCallback(this);
        uiManager.setEquipmentName(getActivity().getResources().getString(R.string.tuneUpId));
        switcherLeftTxtView.setText(getString(R.string.split));
        switcherRightTxtView.setText(getString(R.string.packages));

        if (tuneUpItemBean != null) {
            switchState = tuneUpItemBean.isPackageSelected();
            switch1.setChecked(switchState);
        }
    }

    private boolean checkForSplitPackage(TuneUpItem item) {
        boolean splitAndPackageExist = true;

        if (item != null) {
            if (item.getPackages() == null || item.getPackages().getParts() == null || item.getPackages().getParts().size() == 0) {
                item.setPackageSelected(false);
                splitAndPackageExist = false;
            } else if (item.getSplit() == null || item.getSplit().getParts() == null || item.getSplit().getParts().size() == 0) {
                item.setPackageSelected(true);
                splitAndPackageExist = false;
            } else
                ; // nothing
        }

        return splitAndPackageExist;
    }

    @Override
    public void bindCallbacks(View root) {
        switch1.setOnCheckedChangeListener(this);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (switchState != isChecked) {
            resetFormViews();
            tuneUpItemBean.setPackageSelected(isChecked);
        /*
	     * if (!tuneUpItemBean.isPackageSelected()) { uiManager.setPartBackground(R.drawable.diff_part_bg); } else {
	     * uiManager.setPartBackground(R.drawable.part_bg); }
	     */
            createViews();
            switchState = isChecked;
            if (focusManager != null) {
                focusManager.repopulateFocus(scrollViewLayout);
            }
        }
    }

    private void resetFormViews() {
        tonnage = null;
        actualCapacity = null;
        systemEfficiency = null;
        systemNominalCapacity = null;
        result = null;
    }

    @Override
    public void viewCreated(View view, final Part part, final Options option) {
        if (tonnage == null || actualCapacity == null || systemEfficiency == null || systemNominalCapacity == null || result == null) {
            String name = null;
            String optionName = null;

            if (part != null) {
                name = part.getId();
                optionName = option.getId();

                if (name != null && name.equalsIgnoreCase(AppConstants.TONNAGE_ID)) {
                    if (view instanceof Spinner) {
                        tonnage = (Spinner) view;
                        tonnage.setOnItemSelectedListener(new OnItemSelectedListener() {

                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                String selectedValue = (String) ((TextView) view).getText();
                                if (selectedValue != null && option != null && !selectedValue.equalsIgnoreCase(option.getSavedValue()) && selectedValue.length() > 0) {
                                    if (!selectedValue.equalsIgnoreCase(AppConstants.COMBO_SELECT_VALUE)) {
                                        float tonnageValue = LibUtils.parseFloat(selectedValue, 0);
                                        if (systemNominalCapacity != null) {
                                            systemNominalCapacity.setText((int) (tonnageValue * AppConstants.ONE_TON_TO_BTUH) + "");
                                        }
                                        if (effEffMgr != null) {
                                            effEffMgr.notifyTonnageChanged(selectedValue);
                                        }
                                        option.setSavedValue(selectedValue);
                                    } else {
                                        if (systemNominalCapacity != null) {
                                            systemNominalCapacity.setText("");
                                        }
                                        if (effEffMgr != null) {
                                            effEffMgr.notifyTonnageChanged("");
                                        }
                                        option.setSavedValue(null);
                                    }

                                    computeEfficiency();
                                }

                                if (UiViewUtils.isValidationMode(option.isItemSaved()) && option.isMandatory() && !option.isFieldFilled()) {
                                    ((View) view.getParent()).setBackground(getActivity().getResources().getDrawable(R.drawable.bg_dialog_mandatory_box));
                                } else {
                                    ((View) view.getParent()).setBackground(getActivity().getResources().getDrawable(R.drawable.combo_box_bg));
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                    }
                }

                //=====================================================================================
                // DTE AC TUNE-UP ADDITIONAL MEASURES
                //
		/*
		 * This section is specific to DTE AC tune-up. All "before" (B4) measures are shown by default.
		 * The "after" measures are shown conditionally 
		 * 
		 */

                if (name != null && (
                        name.equalsIgnoreCase(AppConstants.FAN_AIRFLOW_B4) ||
                                //Equipment Nominal BTU and System Nominal Capacity refer to the
                                //same data element. In 10_form_data.json (DTE Energy),
                                //Equipment Nominal BTU is referenced with id: "id": "System_Nominal_Capacity"
                                //to maintain compatibility with the iOS app.
                                name.equalsIgnoreCase(AppConstants.EQUIP_NOMINAL_BTU) ||
                                name.equalsIgnoreCase(AppConstants.SYSTEM_NOMINAL_CAPACITY) ||
                                name.equalsIgnoreCase(AppConstants.COIL_ENTERING_WB_B4) ||
                                name.equalsIgnoreCase(AppConstants.COIL_ENTERING_ENTHALPY_B4) ||
                                name.equalsIgnoreCase(AppConstants.COIL_LEAVING_WB_B4) ||
                                name.equalsIgnoreCase(AppConstants.COIL_LEAVING_ENTHALPY_B4) ||
                                name.equalsIgnoreCase(AppConstants.COIL_CAPACITY_B4) ||
                                name.equalsIgnoreCase(AppConstants.SYS_EFFICIENCY_B4) ||
                                name.equalsIgnoreCase(AppConstants.FAN_AIRFLOW_AFTER) ||
                                name.equalsIgnoreCase(AppConstants.COIL_ENTERING_WB_AFTER) ||
                                name.equalsIgnoreCase(AppConstants.COIL_ENTERING_ENTHALPY_AFTER) ||
                                name.equalsIgnoreCase(AppConstants.COIL_LEAVING_WB_AFTER) ||
                                name.equalsIgnoreCase(AppConstants.COIL_LEAVING_ENTHALPY_AFTER) ||
                                name.equalsIgnoreCase(AppConstants.COIL_CAPACITY_AFTER) ||
                                name.equalsIgnoreCase(AppConstants.SYS_EFFICIENCY_AFTER) ||
                                name.equalsIgnoreCase(AppConstants.FAN_STATIC_PRESS_AFTER) ||
                                name.equalsIgnoreCase(AppConstants.SYS_WATTS_AFTER))) {

                    if (effEffMgr == null) {
                        effEffMgr = new EffectiveEfficiencyManager();
                    }
                    effEffMgr.registerDataField(name, (EditText) view, part);
                }

                if (name != null && name.equalsIgnoreCase(AppConstants.ELIGIBILITY)) {
                    if (view instanceof EditText) {
                        eligible = (EditText) view;
                        if (eligible.getText() != null) {
                            String text = eligible.getText().toString();

                            if (text.equalsIgnoreCase("Not Eligible")) {
                                eligible.setTextColor(Color.RED);
                            } else {
                                eligible.setTextColor(Color.parseColor("#458B00"));
                            }
                        }
                    }
                } else if (name != null && name.equalsIgnoreCase(AppConstants.SYSTEM_NOMINAL_CAPACITY)) {
                    if (view instanceof EditText) {
                        systemNominalCapacity = (EditText) view;

                        if (tonnage != null && tonnage.getSelectedView() != null) {
                            float tonnageValue = LibUtils.parseFloat((String) ((TextView) tonnage.getSelectedView()).getText(), 0);
                            systemNominalCapacity.setText((int) (tonnageValue * AppConstants.ONE_TON_TO_BTUH) + "");
                        }
                    }
                } else if (name != null && name.equalsIgnoreCase(AppConstants.TUNE_UP_PASS_OR_FAIL)) {
                    if (view instanceof EditText) {
                        result = (EditText) view;
                        try {
                            tuneUpItemBean.setStaticRebateValue(
                                    ((Map<String, String>) result.getTag()).get("staticDisplayedValue"));
                        } catch (Exception e) {
                        }

                    }
                } else if (name != null && name.equalsIgnoreCase(AppConstants.TUNE_UP_PAGE_NAME)) {
                    if (view instanceof EditText) {
                        final EditText pageName = (EditText) view;
                        if (option != null) {
                            List<String> savedName = option.getValues();
                            TuneupPagerFragment tuneFragment = (TuneupPagerFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.mainLayout);
                            if (savedName != null && savedName.size() >= (index + 1)) {
                                pageName.setText(savedName.get(index));
                                if (tuneFragment != null) {
                                    tuneFragment.updateSpinneritem(option);
                                }
                            } else {
                                if (index != -1) {
                                    pageName.setText("Equipment " + (index + 1));
                                }
                            }
                        }
                        pageName.addTextChangedListener(new TextWatcher() {

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                            }

                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                            }

                            @Override
                            public void afterTextChanged(Editable s) {
                                String editedString = pageName.getText().toString();
                                TuneupPagerFragment fragment = (TuneupPagerFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.mainLayout);
                                if (fragment != null) {
                                    if (option != null) {
                                        option.setSavedValue(editedString);
                                        fragment.updateSpinnerName(editedString, option);
                                    }

                                }

                            }
                        });
                    }

                }
                // KVB - Adding this for no iManifold support
                else if ((name != null && name.equalsIgnoreCase(AppConstants.ACTUAL_CAPACITY)
                        && view instanceof EditText)
                        || (optionName != null && optionName.equalsIgnoreCase(AppConstants.ACTUAL_CAPACITY)
                        && view instanceof EditText)) {
                    actualCapacity = ((EditText) view);
                    actualCapacity.addTextChangedListener(new TextWatcher() {

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            // TODO Auto-generated method stub

                        }

                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                            // TODO Auto-generated method stub

                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            if (s.toString().trim().equals("")) {
                                actualCapacity.setBackgroundResource(R.drawable.bg_dialog_mandatory_box);
                            } else {
                                actualCapacity.setBackgroundResource(R.drawable.combo_box_bg);
                            }

                            computeEfficiency();
                        }
                    });
                } else if (name != null && name.equalsIgnoreCase(AppConstants.SYSTEM_EFFICIENCY))

                {
                    if (view instanceof EditText) {
                        systemEfficiency = (EditText) view;
                    }
                }
                // ----------------------------------------------------------------------------------------------

                else if (option != null) {
                    name = option.getId();

                    if (name != null && name.equalsIgnoreCase(AppConstants.ACTUAL_CAPACITY)) {
                        if (view instanceof EditText) {
                            actualCapacity = (EditText) view;
                            actualCapacity.setImeOptions(EditorInfo.IME_ACTION_DONE);
                            actualCapacity.addTextChangedListener(new TextWatcher() {

                                @Override
                                public void onTextChanged(CharSequence s, int start, int before, int count) {

                                }

                                @Override
                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                }

                                @Override
                                public void afterTextChanged(Editable s) {
                                    computeEfficiency();
                                }
                            });
                            actualCapacity.setOnEditorActionListener(new OnEditorActionListener() {
                                @Override
                                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                                        option.setSavedValue(actualCapacity.getText().toString());
                                        computeEfficiency();
                                    }
                                    return false;
                                }
                            });
                        }
                    } else if (name != null && name.equalsIgnoreCase(AppConstants.SYSTEM_EFFICIENCY)) {
                        if (view instanceof EditText) {
                            systemEfficiency = (EditText) view;
                        }
                    }

                }
            }
        }
    }

    public TuneUpItem getTuneUpItemBean() {
        return tuneUpItemBean;
    }

    public void setTuneUpItemBean(TuneUpItem tuneUpItemBean) {
        this.tuneUpItemBean = tuneUpItemBean;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setFragmentDeleted() {
        isDeleted = true;
    }

    @Override
    public void onDestroyView() {
        UiUtil.cleanBitmap(getView());
        saveForm();
        resetFormViews();
        if (database != null) {
            database.close();
        }
        super.onDestroyView();
    }

    public boolean isValid() {

        return tuneUpBean.isMandatoryFieldsComplete();
    }

    public void saveForm() {
        computeEfficiency();

        List<TuneUpItem> items = ((TuneupPagerFragment) getParentFragment()).getTuneUpList();

        if (items != null) {
            int index = items.indexOf(tuneUpItemBean);
            if (index > -1) {
                Log.v(TAG, "saveForm() " + index);
                if (!isDeleted) {
                    if (tuneUpItemBean.isItemSaved() && result != null) {
                        String formatedValue = tuneUpItemBean.getStaticRebateValue();
                        if (TextUtils.isEmpty(formatedValue)) {
                            formatedValue = (String.format("%.02f", tuneUpItemBean.getRebateValue()));
                            result.setText("$" + formatedValue);
                        } else result.setText(formatedValue);
                    }
                    ((TuneupPagerFragment) getParentFragment()).saveForm(index, false);
                }
            }
            Iterator<TuneUpItem> itemsIterator = items.listIterator();
            while (itemsIterator.hasNext()) {
                if (tuneUpBean == null) {
                    break;
                }
                TuneUpItem item = (TuneUpItem) itemsIterator.next();
                if (item.isItemSaved()) {
                    if (item.getFormFilledState() == AppConstants.FORM_ITEM_FILLED_STATE.ITEM_COMPLETELY_SAVED.getState()) {
                        tuneUpBean.setMandatoryFieldsComplete(true);
                    } else {
                        tuneUpBean.setMandatoryFieldsComplete(false);
                    }
                } else {
                    tuneUpBean.setMandatoryFieldsComplete(true);
                }
            }
        }
    }

    public void setTitleId(String title) {
        this.title = title;
    }

    public String getTitleId() {
        return this.title;
    }

    public void updateCaptureImage(int pageIndex) {
        UiUtil.updateCaptureImage(uiManager, pageIndex + "", getActivity());
    }

    public String getPageName() {
        TuneupPagerFragment tuneFragment = (TuneupPagerFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.mainLayout);
        return tuneFragment.getPageName();
    }

    private interface ValueWatchClient {
        void notifyValueChanged(String valueName, Object newValue);
    }

    private class EffectiveEfficiencyManager implements ValueWatchClient {
        EnthalpyHandler enthalpyEnterB4Handler;
        EnthalpyHandler enthalpyEnterAfterHandler;
        EnthalpyHandler enthalpyLeaveB4Handler;
        EnthalpyHandler enthalpyLeaveAfterHandler;

        CoilCapacityHandler coilCapacityB4Handler;
        CoilCapacityHandler coilCapacityAfterHandler;

        EffectiveEfficiencyHandler effectEffB4Handler;
        EffectiveEfficiencyHandler effectEffAfterHandler;

        //watchable
//	EditText equipNominalBTU; //Integer
//	EditText fanAirflowB4; //float
//	EditText fanAirflowAfter; //float
//	EditText coilEnteringWBB4; //float
//	EditText coilEnteringWBAfter; //float
//	EditText coilLeavingWBB4; //float
//	EditText coilLeavingWBAfter; //float

        //calculated
//	EditText coilEnteringEnthalpyB4;
//	EditText coilEnteringEnthalpyAfter;
//	EditText coilLeavingEnthalpyB4;
//	EditText coilLeavingEnthalpyAfter;
//	EditText coilCapacityB4;
//	EditText coilCapacityAfter;
//	EditText systemEfficiencyB4;
//	EditText systemEfficiencyAfter;

        private List<View> afterFields;
        private int afterVisible;
        private Float effEffB4;

        public EffectiveEfficiencyManager() {
            effectEffB4Handler = new EffectiveEfficiencyHandler(this, AppConstants.SYS_EFFICIENCY_B4);
            effectEffAfterHandler = new EffectiveEfficiencyHandler(this, AppConstants.SYS_EFFICIENCY_AFTER);

            coilCapacityB4Handler = new CoilCapacityHandler(effectEffB4Handler, AppConstants.COIL_CAPACITY_B4);
            coilCapacityAfterHandler = new CoilCapacityHandler(effectEffAfterHandler, AppConstants.COIL_CAPACITY_AFTER);

            enthalpyEnterB4Handler = new EnthalpyHandler(coilCapacityB4Handler, AppConstants.COIL_ENTERING_ENTHALPY_B4);
            enthalpyLeaveB4Handler = new EnthalpyHandler(coilCapacityB4Handler, AppConstants.COIL_LEAVING_ENTHALPY_B4);
            enthalpyEnterAfterHandler = new EnthalpyHandler(coilCapacityAfterHandler, AppConstants.COIL_ENTERING_ENTHALPY_AFTER);
            enthalpyLeaveAfterHandler = new EnthalpyHandler(coilCapacityAfterHandler, AppConstants.COIL_LEAVING_ENTHALPY_AFTER);

            afterFields = new ArrayList<View>();
            afterVisible = View.GONE;
        }

        public void registerDataField(String name, EditText field, Part part) {

            if (name.equalsIgnoreCase(AppConstants.FAN_AIRFLOW_B4)) {
                coilCapacityB4Handler.setFanAirflow(field);
            } else if (name.equalsIgnoreCase(AppConstants.EQUIP_NOMINAL_BTU) ||
                    name.equalsIgnoreCase(AppConstants.SYSTEM_NOMINAL_CAPACITY)) {
                //Equipment Nominal BTU and System Nominal Capacity refer to the
                //same data element. In 10_form_data.json (DTE Energy),
                //Equipment Nominal BTU is referenced with id: "id": "System_Nominal_Capacity"
                //to maintain compatibility with the iOS app.
                effectEffB4Handler.setEquipNominalBTU(field);
                effectEffAfterHandler.setEquipNominalBTU(field);
            } else if (name.equalsIgnoreCase(AppConstants.COIL_ENTERING_WB_B4)) {
                enthalpyEnterB4Handler.setWbTemp(field);
            } else if (name.equalsIgnoreCase(AppConstants.COIL_ENTERING_ENTHALPY_B4)) {
                enthalpyEnterB4Handler.setWbEnthalpy(field);
                coilCapacityB4Handler.setEnthalpyEnter(field.getText().toString());
            } else if (name.equalsIgnoreCase(AppConstants.COIL_LEAVING_WB_B4)) {
                enthalpyLeaveB4Handler.setWbTemp(field);
            } else if (name.equalsIgnoreCase(AppConstants.COIL_LEAVING_ENTHALPY_B4)) {
                enthalpyLeaveB4Handler.setWbEnthalpy(field);
                coilCapacityB4Handler.setEnthalpyLeave(field.getText().toString());
            } else if (name.equalsIgnoreCase(AppConstants.COIL_CAPACITY_B4)) {
                coilCapacityB4Handler.setCoilCapacity(field);
                effectEffB4Handler.setCoilCapacity(field.getText().toString());
            } else if (name.equalsIgnoreCase(AppConstants.SYS_EFFICIENCY_B4)) {
                effectEffB4Handler.setSysEffectEfficiency(field);
                String _effEffB4 = field.getText().toString().replace("%", "");
                //cache initial effective efficiency before to set proper field visibilities
                effEffB4 = TextUtils.isEmpty(_effEffB4) ? null : Float.parseFloat(_effEffB4) / 100f;
            } else if (name.equalsIgnoreCase(AppConstants.FAN_AIRFLOW_AFTER)) {
                coilCapacityAfterHandler.setFanAirflow(field);
                field.setTag(R.id.field_part, part);
                afterFields.add(field);
            } else if (name.equalsIgnoreCase(AppConstants.COIL_ENTERING_WB_AFTER)) {
                enthalpyEnterAfterHandler.setWbTemp(field);
                field.setTag(R.id.field_part, part);
                afterFields.add(field);
            } else if (name.equalsIgnoreCase(AppConstants.COIL_ENTERING_ENTHALPY_AFTER)) {
                enthalpyEnterAfterHandler.setWbEnthalpy(field);
                coilCapacityAfterHandler.setEnthalpyEnter(field.getText().toString());
                field.setTag(R.id.field_part, part);
                afterFields.add(field);
            } else if (name.equalsIgnoreCase(AppConstants.COIL_LEAVING_WB_AFTER)) {
                enthalpyLeaveAfterHandler.setWbTemp(field);
                field.setTag(R.id.field_part, part);
                afterFields.add(field);
            } else if (name.equalsIgnoreCase(AppConstants.COIL_LEAVING_ENTHALPY_AFTER)) {
                enthalpyLeaveAfterHandler.setWbEnthalpy(field);
                coilCapacityAfterHandler.setEnthalpyLeave(field.getText().toString());
                field.setTag(R.id.field_part, part);
                afterFields.add(field);
            } else if (name.equalsIgnoreCase(AppConstants.COIL_CAPACITY_AFTER)) {
                coilCapacityAfterHandler.setCoilCapacity(field);
                effectEffAfterHandler.setCoilCapacity(field.getText().toString());
                field.setTag(R.id.field_part, part);
                afterFields.add(field);
            } else if (name.equalsIgnoreCase(AppConstants.SYS_EFFICIENCY_AFTER)) {
                effectEffAfterHandler.setSysEffectEfficiency(field);
                field.setTag(R.id.field_part, part);
                afterFields.add(field);
            } else if (name.equalsIgnoreCase(AppConstants.FAN_STATIC_PRESS_AFTER)) {
                field.setTag(R.id.field_part, part);
                afterFields.add(field);
            } else if (name.equalsIgnoreCase(AppConstants.SYS_WATTS_AFTER)) {
                field.setTag(R.id.field_part, part);
                afterFields.add(field);
            }
        }

        public void notifyTonnageChanged(String tonnage) {
	    /*
	     * Tonnage is used to auto-calculate EquipmentNominalBTU which is a 
	     * value watched by both the Before and After System Effective Efficiency 
	     * Handlers  
	     */
            effectEffB4Handler.notifyValueChanged(AppConstants.TONNAGE_ID, tonnage);
            effectEffAfterHandler.notifyValueChanged(AppConstants.TONNAGE_ID, tonnage);
        }

        @Override
        public void notifyValueChanged(String valueName, Object newValue) {
            if (valueName.equals(AppConstants.SYS_EFFICIENCY_B4)) {
                evaluateEfficiency((Float) newValue);
            }
        }

        private void evaluateEfficiency() {
            //called during initialization to set visibility of "After" fields
            //based on previously saved effective efficiency B4
            evaluateEfficiency(effEffB4);
        }

        private void evaluateEfficiency(Float effB4) {
            if (effB4 != null) {
                afterVisible = (Float) effB4 < 0.85f ? View.VISIBLE : View.GONE;
            }

            for (View v : afterFields) {
                showField(v, afterVisible);
            }
        }

        private void showField(View field, int visibility) {
            ((View) field.getParent()).setVisibility(visibility);
            field.setVisibility(visibility);
            ((Part) field.getTag(R.id.field_part))
                    .setHidden(visibility == View.GONE ? "true" : "false");
        }
    }

    private class EnthalpyHandler implements TextWatcher, View.OnFocusChangeListener {
        EditText wbTemp;
        EditText wbEnthalpy;
        ValueWatchClient client;
        String valueName;

        public EnthalpyHandler(ValueWatchClient client, String valueName) {
            super();
            this.client = client;
            this.valueName = valueName;
        }

        public void setWbTemp(EditText wbTemp) {
            this.wbTemp = wbTemp;
            //override default behavior of CustomTextWatcher that gets added by default
            CustomTextWatcher ctw = (CustomTextWatcher) this.wbTemp.getTag(R.id.custom_text_watcher_id);
            ctw.setEmptySaveValue("");
            this.wbTemp.addTextChangedListener(this);
        }

        public void setWbEnthalpy(EditText wbEnthalpy) {
            this.wbEnthalpy = wbEnthalpy;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            String temp = s.toString();

            if (TextUtils.isEmpty(temp)) {
                wbEnthalpy.setText(null);
                client.notifyValueChanged(valueName, null);
            } else {
                String[] tempTokens = temp.split("\\.");
                try {
                    Float enthalpy = EnthalpyTable.query(database, tempTokens[0],
                            tempTokens.length > 1 ? tempTokens[1] : "0");
                    wbEnthalpy.setText(enthalpy.toString());
                    client.notifyValueChanged(valueName, enthalpy);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (TextUtils.isEmpty(s.toString())) {
                wbTemp.setBackground(wbTemp.getContext().getResources().getDrawable(R.drawable.bg_dialog_mandatory_box));
            } else {
                wbTemp.setBackground(wbTemp.getContext().getResources().getDrawable(R.drawable.text_field_white));
            }
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) {
                //wbTemp is the only UI widget that can fire this event
                String temp = wbTemp.getText().toString();
                double value = 0;
                try {
                    value = Double.parseDouble(temp);
                } catch (NumberFormatException e) {
                    return;
                }
                String msg = null;
                if (value > 85.9) msg = "The maximum value is 85.9";
                if (value < 35.0) msg = "The minimum value is 35.0";

                if (!TextUtils.isEmpty(msg)) {
                    wbTemp.setText(null);
                    UiUtil.showError(getActivity(), getActivity().getResources().getString(R.string.alert_title), msg,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    wbTemp.requestFocus();
                                }
                            });
                }
            }
        }
    }

    private class CoilCapacityHandler implements ValueWatchClient, TextWatcher { //OnFocusChangeListener  
        ValueWatchClient client;
        String valueName;
        Float enthalpyEnter;
        Float enthalpyLeave;
        EditText fanAirflow;
        EditText coilCapacity;

        public CoilCapacityHandler(ValueWatchClient client, String valueName) {
            super();
            this.client = client;
            this.valueName = valueName;
        }

        public void setEnthalpyEnter(String enthalpyEnter) {
            try {
                this.enthalpyEnter = Float.parseFloat(enthalpyEnter);
            } catch (NumberFormatException e) {
                this.enthalpyEnter = null;
            }
        }

        public void setEnthalpyLeave(String enthalpyLeave) {
            try {
                this.enthalpyLeave = Float.parseFloat(enthalpyLeave);
            } catch (NumberFormatException e) {
                this.enthalpyLeave = null;
            }
        }

        public void setFanAirflow(EditText fanAirflow) {
            this.fanAirflow = fanAirflow;
            this.fanAirflow.addTextChangedListener(this);
        }

        public void setCoilCapacity(EditText coilCapacity) {
            this.coilCapacity = coilCapacity;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            calculate();
        }

        @Override
        public void notifyValueChanged(String valueName, Object newValue) {
            if (valueName.contains(AppConstants.COIL_ENTERING_WB)) {
                enthalpyEnter = (Float) newValue;
            }
            if (valueName.contains(AppConstants.COIL_LEAVING_WB)) {
                enthalpyLeave = (Float) newValue;
            }
            calculate();
        }

        private void calculate() {
            Float airFlow = null;
            String _airFlow = fanAirflow.getText().toString();
            if (!TextUtils.isEmpty(_airFlow)) {
                airFlow = Float.parseFloat(_airFlow);
            }

            Float _coilCapacity = null;
            if (airFlow != null && enthalpyEnter != null && enthalpyLeave != null) {
                _coilCapacity = airFlow * 4.5f * (enthalpyEnter - enthalpyLeave);
            }

            coilCapacity.setText(_coilCapacity == null ? null :
                    String.format("%.02f", _coilCapacity));
            client.notifyValueChanged(valueName, _coilCapacity);
        }
    }

    private class EffectiveEfficiencyHandler implements ValueWatchClient, TextWatcher { //OnFocusChangeListener
        ValueWatchClient client;
        String valueName;
        Float coilCapacity;
        EditText equipNominalBTU;
        EditText sysEffectEfficiency;

        public EffectiveEfficiencyHandler(ValueWatchClient client, String valueName) {
            super();
            this.client = client;
            this.valueName = valueName;
        }

        public void setEquipNominalBTU(EditText equipNominalBTU) {
            this.equipNominalBTU = equipNominalBTU;
            this.equipNominalBTU.addTextChangedListener(this);
        }

        public void setSysEffectEfficiency(EditText sysEffectEfficiency) {
            this.sysEffectEfficiency = sysEffectEfficiency;
        }

        public void setCoilCapacity(String coilCapacity) {
            try {
                this.coilCapacity = Float.parseFloat(coilCapacity);
            } catch (NumberFormatException e) {
                this.coilCapacity = null;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            calculate();
        }

        @Override
        public void notifyValueChanged(String valueName, Object newValue) {
            if (valueName.contains(AppConstants.COIL_CAPACITY)) {
                coilCapacity = (Float) newValue;
                calculate();
            }

            if (valueName.equals(AppConstants.TONNAGE_ID)) {
                if (TextUtils.isEmpty((String) newValue)) {
                    equipNominalBTU.setText("");
                } else {
                    float tonnageValue = LibUtils.parseFloat((String) newValue, 0);
                    equipNominalBTU.setText((int) (tonnageValue * AppConstants.ONE_TON_TO_BTUH) + "");
                }
            }
        }

        private void calculate() {
            Float nominalBTU = null;
            String _nominalBTU = equipNominalBTU.getText().toString();
            _nominalBTU = _nominalBTU.replace(",", "");
            if (!TextUtils.isEmpty(_nominalBTU)) {
                nominalBTU = Float.parseFloat(_nominalBTU);
            }

            Float _effectEfficiency = null;
            if (nominalBTU != null && coilCapacity != null) {
                _effectEfficiency = coilCapacity / nominalBTU;
            }
            String effectEfficiency = _effectEfficiency == null ? null :
                    ((Integer) Math.round((_effectEfficiency * 100f))).toString();
            try {
                sysEffectEfficiency.setText(effectEfficiency == null ? "" : effectEfficiency + "%");
            }
            //When the form is being initialized, EditText widgets that contribute to the calculation
            //of system effective efficiency can fire value change events that result in this method
            //being called. This can happen before sysEffectEfficiency has been instantiated.
            catch (Exception e) {
            }

            client.notifyValueChanged(valueName, _effectEfficiency);
        }
    }
}
