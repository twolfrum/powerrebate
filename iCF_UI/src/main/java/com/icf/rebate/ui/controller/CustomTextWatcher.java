package com.icf.rebate.ui.controller;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.icf.rebate.networklayer.model.FormResponseBean.Options;
import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.UiUtil;
import com.icf.rebate.ui.util.UiViewUtils;

public abstract class CustomTextWatcher implements TextWatcher, OnFocusChangeListener
{
    public static final String TAG = CustomTextWatcher.class.toString();
    
    EditText editText;
    Options options;
    Context context;
    String emptySaveValue = null;

    public void setEmptySaveValue(String emptySaveValue) {
        this.emptySaveValue = emptySaveValue;
    }

    public CustomTextWatcher(EditText editText, Options options, Context context)
    {
	editText.setOnFocusChangeListener(this);

	this.editText = editText;
	this.options = options;
	this.context = context;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after)
    {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count)
    {
        if (canValidate)
        {
            validateField(s.toString());
        }
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (TextUtils.isEmpty(s.toString()) && emptySaveValue != null) {
            options.setSavedValue(emptySaveValue);
        } else if (options.getDataType() != null && options.getDataType().equalsIgnoreCase("ND")) {
            try {
                float floatValue = LibUtils.parseFloat(s.toString(), 0);
                options.setSavedValue(floatValue + "");
            } catch (Exception e) {
                options.setSavedValue("");
            }
        }
    }

    private void validateField(String str)
    {

	if (options.getFormat() != null && !TextUtils.isEmpty(options.getFormat()) && !TextUtils.isEmpty(str))
	{
	    Pattern pattern = Pattern.compile(options.getFormat());
	    Matcher m = pattern.matcher(str.toString());
	    if (!m.matches())
	    {
		str = str.substring(0, str.length() - 1);
		canValidate = false;
		editText.setText(str);
		canValidate = true;
		editText.setSelection(editText.getText().toString().length());
	    }
	}
	options.setSavedValue(str.toString());
    }

    abstract void getEditTextView(EditText edit);

    boolean canValidate = true;

    @Override
    public void onFocusChange(View v, boolean hasFocus)
    {
	if (hasFocus)
	{
	    getEditTextView(editText);
	    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
	    imm.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT);
	    if (options != null && options.getSavedValue() != null)
	    {
		canValidate = false;
		String newText = options.getSavedValue().replaceAll("[^\\w\\s!.]", "");
		editText.setText(newText);
		editText.setSelection(newText.length());
		canValidate = true;
	    }
	}
	else
	{
	    getEditTextView(null);
	    // String editFieldValue = editText.getText().toString();

	    if (options != null && options.getSavedValue() != null)
	    {
            if (options.getMinLength() != null && !TextUtils.isEmpty(options.getMinLength()))
            {
                int minLength = LibUtils.parseInteger(options.getMinLength(), 0);
                if (options.getSavedValue().length() < minLength)
                {
                options.setSavedValue(null);
                editText.setText(options.getSavedValue());
                String message = "Please enter minimum " + minLength + " digits.";
                UiUtil.showError((Activity) context, context.getResources().getString(R.string.alert_title), message);
                }
            }

            // Min and max value validation
            if (options != null
                && options.getSavedValue() != null
                && options.getDataType() != null
                && (options.getDataType().equalsIgnoreCase("ND")
                    || options.getDataType().equalsIgnoreCase("I")))
            {
                double value;
                try
                {
//$$$TJW chokes here if comma in saved value
                value = LibUtils.parseDouble(options.getSavedValue().replaceAll("[^\\w\\s!.]", ""), 0);
                if (options.getMinValue() != null)
                {
                    double minValue = Double.parseDouble(options.getMinValue());
                    if (value < minValue)
                    {
                    options.setSavedValue(null);
                    editText.setText(options.getSavedValue());
                    String message = "The minimum value is " + minValue +".";
                    UiUtil.showError((Activity) context, context.getResources().getString(R.string.alert_title), message,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    editText.requestFocus();
                                }
                            });
                    }
                }
                if (options.getMaxValue() != null)
                {
                    double maxValue = Double.parseDouble(options.getMaxValue());
                    if (value > maxValue)
                    {
                    options.setSavedValue(null);
                    editText.setText(options.getSavedValue());
                    String message = "The maximum value is " + maxValue +".";
                    UiUtil.showError((Activity) context, context.getResources().getString(R.string.alert_title), message,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    editText.requestFocus();
                                }
                            });
                    }
                }
                }
                catch (NumberFormatException e)
                {
                options.setSavedValue(null);
                editText.setText(options.getSavedValue());
                Log.i(TAG, "Error parsing double.");
                }
            }

            if (options.getSavedValue() != null && options.getDigitGrouping() != null && !TextUtils.isEmpty(options.getDigitGrouping()))
            {
                double doubleValue = LibUtils.parseDouble(options.getSavedValue().replaceAll("[^\\w\\s!.]", ""), 0);
                if (doubleValue > 0)
                {
                options.setSavedValue(LibUtils.formatNumber(options.getDigitGrouping(), doubleValue));
                canValidate = false;
                editText.setText(options.getSavedValue());
                canValidate = true;
                }
            }

            if (options != null && options.isMandatory() && !options.isFieldFilled() && UiViewUtils.isValidationMode(options.isItemSaved()))
            {
                v.setBackground(v.getContext().getResources().getDrawable(R.drawable.bg_dialog_mandatory_box));
            }
            else
            {
                v.setBackground(v.getContext().getResources().getDrawable(R.drawable.bg_dialog_box));
            }
	    }
	}
    }
}
