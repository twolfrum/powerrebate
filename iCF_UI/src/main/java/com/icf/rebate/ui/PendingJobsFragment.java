package com.icf.rebate.ui;

import java.io.FileInputStream;
import java.util.ArrayList;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.adapter.PendingJobAdapter;
import com.icf.rebate.networklayer.DataParserImpl;
import com.icf.rebate.networklayer.ICFHttpManager;
import com.icf.rebate.networklayer.model.CustomerInfo;
import com.icf.rebate.networklayer.model.FormResponseBean;
import com.icf.rebate.networklayer.model.PendingJobItem;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.dialog.DialogListener;
import com.icf.rebate.ui.dialog.ICFDialogFragment;
import com.icf.rebate.ui.fragment.CustomerInformationFragment;
import com.icf.rebate.ui.listeners.PendingJobListener;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.FileUtil;
import com.icf.rebate.ui.util.ICFLogger;
import com.icf.rebate.ui.util.UiUtil;

public class PendingJobsFragment extends Fragment implements OnItemClickListener, DialogListener, PendingJobListener
{
    private ListView lstPendingJob;

    private PendingJobAdapter adapter;

    private OnClickListener adapterClickListener;

    private ArrayList<PendingJobItem> jobList;

    private ICFDialogFragment deleteConfirmDialog;

    private Handler mHandler = new Handler();

    private ProgressBar mPendingProgressBar;

    private TextView noPendingJobs;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
	View view = inflater.inflate(R.layout.pending_jobs_screen, container, false);
	return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
	super.onViewCreated(view, savedInstanceState);
	((ActionBarActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
	initialize(view);
	initializeAdapter();

	((RootActivity) getActivity()).updateTitle(getResources().getString(R.string.pending_jobs_txt));
    }

    private void initialize(View view)
    {
	lstPendingJob = (ListView) view.findViewById(R.id.pendingjobList);
	noPendingJobs = (TextView) view.findViewById(R.id.txt_no_pending_jobs);
	mPendingProgressBar = (ProgressBar) view.findViewById(R.id.pendingJobProgress);

	lstPendingJob.setOnItemClickListener(this);

    }

    private OnClickListener getAdapterOnClickListener()
    {
	if (adapterClickListener == null)
	{
	    adapterClickListener = new OnClickListener()
	    {

		@Override
		public void onClick(final View v)
		{
		    int id = v.getId();
		    if (id == R.id.submitBtn)
		    {
			// UiUtil.showNotImplemented(getActivity());
			final Object obj = v.getTag();
			if (obj instanceof PendingJobItem)
			{
			    ((PendingJobItem) obj).setJobState(CustomerInfo.SUBMISSION_IN_PROGRESS);
			}

			ICFLogger.d("PendingJobFragment", "Job State :" + CustomerInfo.SUBMISSION_IN_PROGRESS);
			Thread t = new Thread(new Runnable()
			{

			    @Override
			    public void run()
			    {
				boolean submitFailed = false;
				PendingJobItem pItem = (PendingJobItem) obj;
				byte[] data = null;
				try
				{
				    FileInputStream fis = new FileInputStream(pItem.getFolderPath() + "/" + AppConstants.JOB_DOCUMENT_UI);
				    data = new byte[fis.available()];
				    fis.read(data);
				    fis.close();
				}
				catch (Exception e)
				{

				}
				if (data != null && data.length > 0)
				{
				    FormResponseBean bean = (FormResponseBean) DataParserImpl.getInstance().parse(ICFHttpManager.REQ_ID_FORM, data);
				    pItem.setBean(bean);
				    doSubmission(pItem);
				}
				else
				{
				    submitFailed = true;
				}

				if (submitFailed)
				{
				    pItem.setJobState(CustomerInfo.SUBMISSION_ERROR);
				    RebateManager.getInstance().updateFormState(pItem.getBean(), CustomerInfo.SUBMISSION_ERROR);
				    refreshAdapter();
				}
			    }
			});
			t.start();

		    }
		    else if (id == R.id.trash_icon)
		    {
			Object obj = v.getTag();
			deleteConfirmDialog = ICFDialogFragment.newDialogFrag(R.layout.delete_confirmation);
			deleteConfirmDialog.setDialogListener(PendingJobsFragment.this);
			deleteConfirmDialog.setDataObj(obj);
			deleteConfirmDialog.show(getActivity().getSupportFragmentManager(), "Delete Confirmation");
			getActivity().getSupportFragmentManager().executePendingTransactions();
		    }
		}
	    };
	}
	return adapterClickListener;
    }

    private void doSubmission(PendingJobItem p)
    {
	RebateManager.getInstance().doSubmission(p, R.string.pending_job_screen_network_mess);
    }

    private void initializeAdapter()
    {
	ArrayList<PendingJobItem> list = null;
	Activity activity = (RootActivity) getActivity();
	if (activity != null)
	{
	    list = ((RootActivity) activity).getPendingJobItems();
	}
	if ((list == null || list.size() == 0) && !((RootActivity) getActivity()).isInitializedFully(this))
	{
	    noPendingJobs.setText(R.string.initializing);
	    noPendingJobs.setVisibility(View.VISIBLE);
	    mPendingProgressBar.setVisibility(View.VISIBLE);
	}
	else
	{
	    if (list == null)
	    {
		list = new ArrayList<PendingJobItem>();
	    }
	    this.jobList = new ArrayList<PendingJobItem>(list);
	    adapter = new PendingJobAdapter(getActivity(), 0, jobList);
	    adapter.setAdapterListener(adapter);
	    adapter.setOnClickListener(getAdapterOnClickListener());
	    lstPendingJob.setAdapter(adapter);
	    noPendingJobs.setText(R.string.no_pending_jobs);
	    if (jobList.size() <= 0)
	    {
		noPendingJobs.setVisibility(View.VISIBLE);
	    }
	    else
	    {
		noPendingJobs.setVisibility(View.GONE);
	    }
	}
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
	final PendingJobItem item = jobList.get(position);
	if (item.isSubmitted())
	{
	    return;
	}
	if (item.isSubmissionInProgress())
	{
	    UiUtil.showError(getActivity(), getResources().getString(R.string.alert_title), getString(R.string.uploadprogress_item_click));
	    return;
	}
	AsyncTask<PendingJobItem, Void, FormResponseBean> retrieveTask = new AsyncTask<PendingJobItem, Void, FormResponseBean>()
	{

	    @Override
	    protected void onPreExecute()
	    {
		UiUtil.showProgressDialog(getActivity());
	    }

	    @Override
	    protected FormResponseBean doInBackground(PendingJobItem... params)
	    {
		byte[] data = null;
		try
		{
		    FileInputStream fis = new FileInputStream(params[0].getFolderPath() + "/" + AppConstants.JOB_DOCUMENT_UI);
		    data = new byte[fis.available()];
		    fis.read(data);
		    fis.close();
		}
		catch (Exception e)
		{

		}
		if (data != null && data.length > 0)
		{
		    FormResponseBean bean = (FormResponseBean) DataParserImpl.getInstance().parse(ICFHttpManager.REQ_ID_FORM, data);
		    return bean;
		}
		return null;
	    }

	    @Override
	    protected void onPostExecute(FormResponseBean result)
	    {
		UiUtil.dismissSpinnerDialog();
		RebateManager.getInstance().setCurrentBean(result);
		CustomerInformationFragment rebateTool = new CustomerInformationFragment();
		((RootActivity) getActivity()).beginTransaction().replace(R.id.mainLayout, rebateTool).addToBackStack(null).commitAllowingStateLoss();
	    }

	};
	retrieveTask.execute(item);

    }

    public void refreshAdapter()
    {
	getActivity().runOnUiThread(new Runnable()
	{

	    @Override
	    public void run()
	    {
		initializeAdapter();
		// if (adapter != null)
		// {
		// adapter.notifyDataSetChanged();
		// }
	    }
	});
    }

    @Override
    public void onFinishInflate(int layoutId, View view)
    {
	if (layoutId == R.layout.delete_confirmation && view != null)
	{
	    ImageButton cancel = (ImageButton) view.findViewById(R.id.delete_cancel_btn);
	    ImageButton done = (ImageButton) view.findViewById(R.id.delete_done_btn);
	    cancel.setOnClickListener(new OnClickListener()
	    {

		@Override
		public void onClick(View v)
		{
		    deleteConfirmDialog.dismiss();
		}
	    });
	    done.setOnClickListener(new OnClickListener()
	    {

		@Override
		public void onClick(View v)
		{
		    Thread t = new Thread(new Runnable()
		    {

			@Override
			public void run()
			{
			    Object obj = deleteConfirmDialog.getDataObj();
			    if (obj instanceof PendingJobItem)
			    {
				FileUtil.deleteEntireFolder(((PendingJobItem) obj).getFolderPath(), true);
				ArrayList<PendingJobItem> list = ((RootActivity) getActivity()).getPendingJobItems();
				if (list != null)
				{
				    list.remove(((PendingJobItem) obj));
				}
				mHandler.post(new Runnable()
				{

				    @Override
				    public void run()
				    {
					initializeAdapter();
					deleteConfirmDialog.dismiss();
				    }
				});
			    }
			}
		    });
		    t.start();

		}
	    });
	}
    }

    @Override
    public void onDismiss()
    {

    }

    @Override
    public void intializedCompleted()
    {
	Activity activity = getActivity();
	if (activity != null)
	{
	    activity.runOnUiThread(new Runnable()
	    {

		@Override
		public void run()
		{
		    noPendingJobs.setVisibility(View.GONE);
		    mPendingProgressBar.setVisibility(View.GONE);
		    refreshAdapter();

		}
	    });
	}
    }

}
