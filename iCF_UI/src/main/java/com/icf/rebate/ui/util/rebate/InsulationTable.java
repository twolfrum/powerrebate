/**
 * 
 */
package com.icf.rebate.ui.util.rebate;

import java.util.ArrayList;
import java.util.List;

import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.rebate.ui.util.ICFLogger;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

/**
 * @author Ken Butler Dec 4, 2015
 * InsulationTable.java
 * Copyright (c) 2015 ICF International
 */
public class InsulationTable {
    

    private static final String TAG = InsulationTable.class.getSimpleName();

    public static final String TABLE_NAME = "insulation_rebate";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_REBATE_TYPE = "rebate_type";
    public static final String COLUMN_UTILITY_COMPANY = "utility_company";
    public static final String COLUMN_HOUSE_TYPE = "house_type";
    public static final String COLUMN_INSULATION_TYPE = "insulation_type";
    public static final String COLUMN_EXISTING_RVAL_MAX = "existing_r_value_max";
    public static final String COLUMN_FINAL_RVAL_MIN = "final_r_value_min";
    public static final String COLUMN_SQ_FOOTAGE_MIN = "square_footage_min";
    
    // Create table sql
    public static final String CREATE_INSULATION_TABLE = "CREATE TABLE " 
	   + TABLE_NAME + "("
	   + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
	   + COLUMN_REBATE_TYPE + " TEXT,"
	   + COLUMN_UTILITY_COMPANY + " INTEGER NOT NULL,"
	   + COLUMN_HOUSE_TYPE + " TEXT,"
	   + COLUMN_INSULATION_TYPE + " TEXT,"
	   + COLUMN_EXISTING_RVAL_MAX + " INTEGER,"
	   + COLUMN_FINAL_RVAL_MIN + " INTEGER,"
	   + COLUMN_SQ_FOOTAGE_MIN + " INTEGER"
	   + ");";   
    
    private static final List<String> mRebateColumns = new ArrayList<String>();

    public static void onCreate(SQLiteDatabase database) {
	database.execSQL(CREATE_INSULATION_TABLE);
	Log.v(TAG, "Created table: "+ CREATE_INSULATION_TABLE);
    }
    
    public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion){}
    
    public static void addRebateIntervalFields(int id, SQLiteDatabase database, String csvLine){
	String[] csvLineItems = csvLine.split(",");
	mRebateColumns.clear();
	for (String s : csvLineItems) {
	    // Matches 4 digits between 1900 - 2099
	    if (s.matches("19\\d\\d|20\\d\\d")) {
		String dateRange = "col" + id + "_" +"01_01_"+s+"__12_31_"+s;
		final String sqlStatement = "ALTER TABLE "
			+ TABLE_NAME 
			+ " ADD COLUMN "
			+ dateRange
			+ " REAL"
			+ ";";
		mRebateColumns.add(dateRange);
		Cursor cursor = database.rawQuery("SELECT * FROM "+TABLE_NAME, null);
		if (cursor.getColumnIndex(dateRange) == -1) {
		    database.execSQL(sqlStatement);
		}
	    } else if (s.matches("\\d{2}/\\d{2}/\\d{4}-\\d{2}/\\d{2}/\\d{4}")) {
		final String[] date = s.split("-|/");
		final String colName = "col" + id + "_" + date[0] + "_" + date[1] + "_" + date[2] + "__" + date[3] + "_" + date[4] + "_" + date[5];
		final String sqlStatement = "ALTER TABLE "
			+ TABLE_NAME 
			+ " ADD COLUMN "
			+ colName
			+ " REAL"
			+ ";";
		mRebateColumns.add(colName);
		Cursor cursor = database.rawQuery("SELECT * FROM "+TABLE_NAME, null);
		if (cursor.getColumnIndex(colName) == -1) {
		    database.execSQL(sqlStatement);
		}
	    }
	}
    }
    
    public static void insert(int id, SQLiteDatabase database, String[] columnData) {
	
	// Constants based on columns in csv
	final int REBATE_TYPE = 0;
	final int HOUSE_TYPE = 1;
	final int INSULATION_TYPE = 2;
	final int EXISTING_R_MAX = 3;
	final int FINAL_R_MIN = 4;
	final int SQ_FOOTAGE_MIN = 5;
	
	// Last column that's not a date range
	final int LAST_DEFINED_COLUMN = SQ_FOOTAGE_MIN;
	
	try {
	    ContentValues values = new ContentValues();
	    
	    // Insert utility company
	    values.put(COLUMN_UTILITY_COMPANY, id);
	    values.put(COLUMN_REBATE_TYPE, columnData[REBATE_TYPE]);
	    values.put(COLUMN_HOUSE_TYPE, columnData[HOUSE_TYPE]);
	    values.put(COLUMN_INSULATION_TYPE, columnData[INSULATION_TYPE]);
	    values.put(COLUMN_EXISTING_RVAL_MAX, Integer.parseInt(columnData[EXISTING_R_MAX]));
	    values.put(COLUMN_FINAL_RVAL_MIN, Integer.parseInt(columnData[FINAL_R_MIN]));
	    values.put(COLUMN_SQ_FOOTAGE_MIN, Integer.parseInt(columnData[SQ_FOOTAGE_MIN]));
	    int j = 0;
	    for (int i = LAST_DEFINED_COLUMN + 1; i <columnData.length; i++) {
		if (j < mRebateColumns.size()) {
		    values.put(mRebateColumns.get(j++), LibUtils.parseDouble(columnData[i], 0f));
		}
	    }
	    
	    database.insert(TABLE_NAME, null, values);
	    ICFLogger.d(TAG, "Insert : " +TABLE_NAME+ " "+values);
	} catch (SQLiteException e) {
	    Log.e(TAG, "Error inserting into " + TABLE_NAME);
	    e.printStackTrace();
	}
    }
}
