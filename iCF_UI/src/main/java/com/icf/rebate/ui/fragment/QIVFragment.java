package com.icf.rebate.ui.fragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.OnEditorActionListener;

import com.icf.rebate.networklayer.model.CustomerInfo;
import com.icf.rebate.networklayer.model.FormResponseBean;
import com.icf.rebate.networklayer.model.FormResponseBean.Options;
import com.icf.rebate.networklayer.model.FormResponseBean.Part;
import com.icf.rebate.networklayer.model.QIVItem;
import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.ui.controller.FormUIManager;
import com.icf.rebate.ui.controller.FormUIManager.FormViewCreationCallback;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.listeners.FragmentCallFlow;
import com.icf.rebate.ui.listeners.RequestCallFlow;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.FileUtil;
import com.icf.rebate.ui.util.ICFLogger;
import com.icf.rebate.ui.util.UiUtil;
import com.icf.rebate.ui.util.rebate.QIVRebateCalculation;

public class QIVFragment extends Fragment implements FragmentCallFlow, RequestCallFlow, FormViewCreationCallback
{
    public static final String TAG = QIVFragment.class.getSimpleName();
    int index = -1;
    QIVItem qivItem;
    FormUIManager uiManager;
    LinearLayout scrollViewLayout;
    EditText systemTested;
    EditText actualCapacity;
    EditText systemEfficiency;
    EditText tonnage;
    EditText systemNominalCapacity;
    EditText result;
    EditText eligible;
    String selectedTonnageValue;
    QIVRebateCalculation qivRebateCalculation;

    private float SYSTEM_CAPACITY_THERSHOLD;

    private TextView txtClear;

    public QIVFragment()
    {
	super();
    }

    public static QIVFragment newInstance(QIVItem qivItem, QIVRebateCalculation qivRebateCalculation)
    {
	QIVFragment fragment = new QIVFragment();
	qivRebateCalculation = new QIVRebateCalculation();
	qivRebateCalculation.fetchValueFromDB();
	fragment.SYSTEM_CAPACITY_THERSHOLD = qivRebateCalculation.getSystemCapcityThreshold();
	if (qivItem != null)
	{
	    fragment.qivItem = qivItem;
	    fragment.index = qivItem.getIndex() + 1;
	    fragment.qivRebateCalculation = qivRebateCalculation;

	    if (qivItem.getPartList() != null)
	    {
		List<Part> parts = qivItem.getPartList();

		Part part = UiUtil.getPartById(parts, AppConstants.TONNAGE_ID);
		if (part != null && part.getOptions() != null && part.getOptions().size() > 0 && part.getOptions().get(0) != null)
		{
		    fragment.selectedTonnageValue = part.getOptions().get(0).getSavedValue();
		}
	    }
	}

	return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
	View view = inflater.inflate(R.layout.qiv_screen, null, false);
	return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
	super.onViewCreated(view, savedInstanceState);

	getUiControls(view);
	initialize(view);
	bindCallbacks(view);
	requestData();

	CustomerInfo cInfo = RebateManager.getInstance().getFormBean().getCustomerInfo();
	if (UiUtil.isJobCompleted(cInfo))
	{
	    UiUtil.setEnableView((ViewGroup) view, false, null);
	}
    }

    @Override
    public void showProgressDialog()
    {
	getActivity().runOnUiThread(new Runnable()
	{
	    public void run()
	    {
		UiUtil.showProgressDialog(getActivity());
	    }
	});
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
	super.onActivityResult(requestCode, resultCode, data);
//	Log.d(TAG, "request code= " + requestCode + " resultCode= " + resultCode);
//	Log.d(TAG, "extras= " + data.getExtras().toString());
	/*
	 if (requestCode == IManifoldDummyActivity.IManifoldResultCode && data != null)
	 {
	 if (data.hasExtra("actual_capacity"))
	 {
	 String actualCapacity = data.getStringExtra("actual_capacity");
	 if (this.actualCapacity != null && actualCapacity != null)
	 {
	 this.actualCapacity.setText(actualCapacity);
	 computeEfficiency();
	 }
	 }
	 }*/
    }

    private void computeEfficiency()
    {
	if (this.systemNominalCapacity != null && !TextUtils.isEmpty(this.systemNominalCapacity.getText()) && this.systemEfficiency != null && actualCapacity != null && !TextUtils.isEmpty(actualCapacity.getText()))
	{
	    try
	    {
		int systemEfficiency = (int) ((LibUtils.parseFloat(actualCapacity.getText().toString(), 0) / LibUtils.parseFloat(systemNominalCapacity.getText().toString(), 0)) * 100);
		this.systemEfficiency.setText(systemEfficiency + "");
		this.systemEfficiency.setBackground(getActivity().getResources().getDrawable(R.drawable.bg_dialog_box));
		this.actualCapacity.setBackground(getActivity().getResources().getDrawable(R.drawable.bg_dialog_box));

		boolean isEligible = systemEfficiency >= SYSTEM_CAPACITY_THERSHOLD;
		if (isEligible)
		{
		    float ton = 1;

		    if (selectedTonnageValue != null)
		    {
			try
			{
			    ton = LibUtils.parseFloat(selectedTonnageValue, 0);
			}
			catch (Exception e)
			{

			}
		    }

		    List<Part> partList = qivItem.getPartList();

		    for (Part part : partList)
		    {
			if (part.getId().equalsIgnoreCase(AppConstants.ELIGIBILITY))
			{
			    List<Options> optionList = part.getOptions();
			    if (optionList != null && optionList.size() > 0)
			    {
				optionList.get(0).setSavedValue("Eligible");
			    }
			}
		    }
		    eligible.setText("Eligible");
		    eligible.setTextColor(Color.parseColor("#458B00"));

		    qivItem.setRebateValue(qivRebateCalculation.getIncentive(ton));
		}
		else
		{
		    List<Part> partList = qivItem.getPartList();
		    for (Part part : partList)
		    {
			if (part.getId().equalsIgnoreCase(AppConstants.ELIGIBILITY))
			{
			    List<Options> optionList = part.getOptions();
			    if (optionList != null && optionList.size() > 0)
			    {
				optionList.get(0).setSavedValue("Not Eligible");
			    }
			}
		    }
		    eligible.setText("Not Eligible");
		    eligible.setTextColor(Color.RED);
		    qivItem.setRebateValue(0);
		}
		String formatedValue = qivItem.getStaticRebateValue();
		if (TextUtils.isEmpty(formatedValue)) {
			formatedValue = (String.format("%.02f", qivItem.getRebateValue()));
		}
		this.result.setText("" + formatedValue);
		this.result.setBackground(getActivity().getResources().getDrawable(R.drawable.bg_dialog_box));
		ICFLogger.d(TAG, "Result:" + "$" + formatedValue);
	    }
	    catch (Exception e)
	    {

	    }
	}
	else
	{
	    qivItem.setRebateValue(0);
	}
    }

    @Override
    public void stopProgressDialog()
    {
	getActivity().runOnUiThread(new Runnable()
	{
	    public void run()
	    {
		UiUtil.dismissSpinnerDialog();
	    }
	});
    }

    @Override
    public void updateData(final Object obj)
    {
	if (obj != null && obj instanceof FormResponseBean)
	{
	    List<QIVItem> qivList = ((FormResponseBean) obj).getQivList();
	    if (qivList != null && qivList.size() > index)
	    {
		final QIVItem item = qivList.get(index);
		final List<Part> parts = item.getPartList();
		if (parts != null)
		{
		    getActivity().runOnUiThread(new Runnable()
		    {
			public void run()
			{
			    scrollViewLayout.removeAllViews();
			    // boolean isFormFilled = item.getFormFilledState() != AppConstants.FORM_ITEM_FILLED_STATE.ITEM_UNEDITED.getState();
			    uiManager.createViews(QIVFragment.this, scrollViewLayout, parts, item.isItemSaved(), item);
			}

		    });
		}
	    }
	}
    }

    @Override
    public void displayError(String title, String message)
    {

    }

    @Override
    public void requestData()
    {
	RebateManager.getInstance().checkAndrequestData(this);
    }

    @Override
    public void getUiControls(View root)
    {
	scrollViewLayout = (LinearLayout) root.findViewById(R.id.scrol_layout);
	txtClear = (TextView) root.findViewById(R.id.clear);
	txtClear.setOnClickListener(new OnClickListener()
	{

	    @Override
	    public void onClick(View v)
	    {
		actualCapacity.setText("");
		systemEfficiency.setText("");
		result.setText("");
		eligible.setText("");
		List<Part> partList = qivItem.getPartList();
		for (Part tempPart : partList)
		{
		    if (AppConstants.QIV_MANIFOLD.equalsIgnoreCase(tempPart.getName()))
		    {
			List<Options> optionList = tempPart.getOptions();
			for (Options tempOption : optionList)
			{
			    if (AppConstants.ACTUAL_CAPACITY.equalsIgnoreCase(tempOption.getId()))
			    {
				tempOption.setSavedValue("");
			    }
			    else if (AppConstants.SYSTEM_EFFICIENCY.equalsIgnoreCase(tempOption.getId()))
			    {
				tempOption.setSavedValue("");
			    }

			    if (AppConstants.manifold_xmlName.equalsIgnoreCase(tempOption.getXmlName()))
			    {
				String value = tempOption.getSavedValue();
				if (!TextUtils.isEmpty(value))
				{
				    String fileName = FileUtil.getAbsolutePath(value);
				    FileUtil.deleteFile(fileName);
				    tempOption.setSavedValue("");
				}
			    }
			}
		    }
		    else if (AppConstants.QUALITY_INSTALL_VERIFICATION_PASS_FAIL.equalsIgnoreCase(tempPart.getId()) || AppConstants.ELIGIBILITY.equalsIgnoreCase(tempPart.getId()))
		    {
			List<Options> optionList = tempPart.getOptions();
			for (Options tempOption : optionList)
			{
			    if (tempOption.getInputType() == null)
			    {
				tempOption.setSavedValue("");
			    }
			}
		    }
		}
	    }
	});
    }

    @Override
    public void initialize(View root)
    {
	uiManager = new FormUIManager(getString(R.string.qualityInstallVerificationId));
	uiManager.setFormViewCreationCallback(this);
	uiManager.setEquipmentName(getActivity().getResources().getString(R.string.qualityInstallVerificationId));
    }

    @Override
    public void bindCallbacks(View root)
    {

    }

    @Override
    public void viewCreated(View view, final Part part, final Options option)
    {
	if (tonnage == null || actualCapacity == null || systemEfficiency == null || systemNominalCapacity == null || result == null)
	{
	    String name = null;

	    if (part != null)
	    {
		name = part.getId();

		if (name != null && name.equalsIgnoreCase(AppConstants.TONNAGE_ID))
		{
		    if (view instanceof EditText)
		    {
			tonnage = (EditText) view;

			if (selectedTonnageValue != null)
			{
			    tonnage.setText(selectedTonnageValue);
			    this.tonnage.setBackground(getActivity().getResources().getDrawable(R.drawable.bg_dialog_box));
			    if (systemNominalCapacity != null)
			    {
				float tonnageValue = LibUtils.parseFloat(selectedTonnageValue, 0);
				systemNominalCapacity.setText((int) (tonnageValue * AppConstants.ONE_TON_TO_BTUH) + "");
				systemNominalCapacity.setBackground(getActivity().getResources().getDrawable(R.drawable.bg_dialog_box));
			    }
			}
		    }
		}
		if (name != null && name.equalsIgnoreCase(AppConstants.ELIGIBILITY))
		{
		    if (view instanceof EditText)
		    {
			eligible = (EditText) view;
			if (eligible.getText() != null)
			{
			    String text = eligible.getText().toString();

			    if (text.equalsIgnoreCase("Not Eligible"))
			    {
				eligible.setTextColor(Color.RED);
			    }
			    else if (text.equalsIgnoreCase("Eligible"))
			    {
				eligible.setTextColor(Color.parseColor("#458B00"));
			    }
			}
		    }
		}
		else if (name != null && name.equalsIgnoreCase(AppConstants.QUALITY_INSTALL_SYSTEM_TESTED))
		{
		    if (view instanceof EditText)
		    {
			systemTested = (EditText) view;

			if (qivItem != null)
			{
			    systemTested.setText(qivItem.getName());
			    systemTested.setBackground(getActivity().getResources().getDrawable(R.drawable.bg_dialog_box));
			}
		    }
		}
		else if (name != null && name.equalsIgnoreCase(AppConstants.SYSTEM_NOMINAL_CAPACITY))
		{
		    if (view instanceof EditText)
		    {
			systemNominalCapacity = (EditText) view;

			if (selectedTonnageValue != null)
			{
			    float tonnageValue = LibUtils.parseFloat(selectedTonnageValue, 0);
			    systemNominalCapacity.setText((int) (tonnageValue * AppConstants.ONE_TON_TO_BTUH) + "");
			    systemNominalCapacity.setBackground(getActivity().getResources().getDrawable(R.drawable.bg_dialog_box));
			}
		    }
		}
		else if (name != null && name.equalsIgnoreCase(AppConstants.QUALITY_INSTALL_VERIFICATION_PASS_FAIL))
		{
		    if (view instanceof EditText)
		    {
				result = (EditText) view;
				result.setEnabled(false);
				try {
					qivItem.setStaticRebateValue(
							((Map<String, String>) result.getTag()).get("staticDisplayedValue"));
				} catch (Exception e) {
				}

			}
		}
		else if (option != null)
		{
		    name = option.getId();

		    if (name != null && name.equalsIgnoreCase(AppConstants.ACTUAL_CAPACITY))
		    {
			if (view instanceof EditText)
			{
			    actualCapacity = (EditText) view;
			    actualCapacity.setImeOptions(EditorInfo.IME_ACTION_DONE);
			    actualCapacity.setOnEditorActionListener(new OnEditorActionListener()
			    {
				@Override
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
				{
				    if (actionId == EditorInfo.IME_ACTION_DONE)
				    {
					option.setSavedValue(actualCapacity.getText().toString());
					computeEfficiency();
				    }
				    return false;
				}
			    });

			    actualCapacity.addTextChangedListener(new TextWatcher()
			    {
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count)
				{

				}

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after)
				{

				}

				@Override
				public void afterTextChanged(Editable s)
				{
				    computeEfficiency();
				}
			    });

			}
		    }
		    else if (name != null && name.equalsIgnoreCase(AppConstants.SYSTEM_EFFICIENCY))
		    {
			if (view instanceof EditText)
			{
			    systemEfficiency = (EditText) view;
			}
		    }
		}
	    }
	}
    }

    @Override
    public void onDestroyView()
    {
	super.onDestroyView();
	UiUtil.cleanBitmap(getView());
	saveForm();
	RebateManager.getInstance().updateFormState(null);
    }

    public void saveForm()
    {
	Toast.makeText(getActivity(), getString(R.string.saved_string), Toast.LENGTH_LONG).show();
	qivItem.setSavedItem(true);
	qivItem.setMandatoryFiledComplete(UiUtil.validateForm(qivItem.getPartList()));
    }
    
    
}
