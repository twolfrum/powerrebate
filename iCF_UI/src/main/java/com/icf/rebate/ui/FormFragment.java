package com.icf.rebate.ui;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.adapter.CustomSpinnerAdapter;
import com.icf.rebate.adapter.EquipementPageAdapter;
import com.icf.rebate.app.model.DashboardItem.ServiceItem;
import com.icf.rebate.networklayer.model.CustomerInfo;
import com.icf.rebate.networklayer.model.FormResponseBean;
import com.icf.rebate.networklayer.model.FormResponseBean.Equipment;
import com.icf.rebate.networklayer.model.FormResponseBean.Item;
import com.icf.rebate.networklayer.model.FormResponseBean.Part;
import com.icf.rebate.networklayer.model.QIVItem;
import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.rebate.ui.controller.FormUIManager;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.customviews.CustomPageIndicator;
import com.icf.rebate.ui.customviews.CustomViewPager;
import com.icf.rebate.ui.dialog.DialogListener;
import com.icf.rebate.ui.dialog.ICFDialogFragment;
import com.icf.rebate.ui.fragment.EquipmentPageFragment;
import com.icf.rebate.ui.fragment.PersistentForm;
import com.icf.rebate.ui.listeners.FragmentCallFlow;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.FileUtil;
import com.icf.rebate.ui.util.UiUtil;

public class FormFragment extends Fragment implements FragmentCallFlow, OnPageChangeListener, OnItemSelectedListener, OnClickListener, DialogListener, PersistentForm {
    public static final String TAG = FormFragment.class.getSimpleName();
    private RootActivity homeScreenactivity;
    private EquipementPageAdapter pageAdapter;
    private Spinner equipmentComboBox;
    // private ViewPager pageViewer;
    int equipmentIndex = 0;
    int pageIndex = 0;
    public CustomViewPager viewPager;
    private ICFDialogFragment commonDialog;
    private FormResponseBean bean;
    private List<QIVItem> qivList;
    private Equipment equipment;

    public FormFragment() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.form_screen_layout, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bean = RebateManager.getInstance().getFormBean();
        getUiControls(view);
        initialize(view);
        bindCallbacks(view);
        requestData();

        CustomerInfo cInfo = RebateManager.getInstance().getFormBean().getCustomerInfo();
        if (UiUtil.isJobCompleted(cInfo)) {
            ArrayList<View> list = new ArrayList<View>();
            list.add(viewPager);
            UiUtil.setEnableView((ViewGroup) view, false, list);
        }
    }

    public void updateEquipmentViews() {
        if (bean == null || equipment == null) {
            return;
        }
        // viewPager.setCurrentItem(equipmentIndex);
        pageAdapter.setEquipment(equipment);
        pageAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        if (v != null) {
            EquipmentPageFragment frag = pageAdapter.getCurrentFragment(equipment.getItems().get(pageIndex));
            if (pageAdapter != null && frag != null) {
                EditText editField = frag.getLastFocusedEditField();
                if (editField != null) {
                    UiUtil.hideSoftKeyboard(getActivity(), editField);
                    editField.clearFocus();
                }
            }

            int id = v.getId();
            if (id == R.id.save_btn) {
                viewPager.clearFocus();
                saveForm(frag, pageIndex, true);
            } else if (id == R.id.close_btn) {
                commonDialog = ICFDialogFragment.newDialogFrag(R.layout.delete_confirmation);
                commonDialog.setDialogListener(this);
                commonDialog.show(getActivity().getSupportFragmentManager(), "Delete Confirmation");
                getActivity().getSupportFragmentManager().executePendingTransactions();
            } else if (id == R.id.add_btn) {
                if (bean != null && equipment != null && pageAdapter != null && equipment.getItems() != null
                        //&& equipment.getItems().size() < AppConstants.MAX_PAGE_SUPPORT
                        && equipment.getItems().size() < equipment.getMaxMeasures()) {
                    try {
                        Item newItem = equipment.createNewItem();
                        addNewItem(newItem);

                        pageAdapter.notifyDataSetChanged();
                        viewPager.setOffscreenPageLimit(equipment.getItems().size());
                        viewPager.firePageCountChanged();
                        viewPager.setCurrentItem(equipment.getItems().size() - 1);
                        updateSpinnerAdapter();
                    } catch (CloneNotSupportedException e) {
                        e.printStackTrace();
                    }

                } else if (equipment.getItems().size() == equipment.getMaxMeasures()) {
                    UiUtil.showError(getActivity(), getResources().getString(R.string.alert_title),
                            getResources().getString(R.string.maximum_measures_limit,
                                equipment.getName(), equipment.getMaxMeasures()));
                }
            }
        }
    }

    private void addNewItem(Item newItem) {
        if (equipment.getItems().size() > 0) {
            Item lastItem = equipment.getItems().get(equipment.getItems().size() - 1);
            newItem.setPageNum(lastItem.getPageNum() + 1);
        }
        equipment.addNewItem(newItem);
    }


    @Override
    public int getNoScreensToToolList() {
        return 2;
    }

    @Override
    public boolean isValid() {
        setValidState();
        return equipment.isMandatoryFieldsComplete();
    }


    @Override
    public void setValidState() {
        if (equipment != null && equipment.getItems() != null) {
            Iterator<Item> itemsIterator = equipment.getItems().listIterator();
            while (itemsIterator.hasNext()) {
                FormResponseBean.Item item = (FormResponseBean.Item) itemsIterator.next();
                if (item.isItemSaved()) {
                    if (item != null && item.getFormFilledState() == AppConstants.FORM_ITEM_FILLED_STATE.ITEM_COMPLETELY_SAVED.getState()) {
                        equipment.setMandatoryFieldsComplete(true);
                    } else {
                        equipment.setMandatoryFieldsComplete(false);
                    }
                } else {
                    equipment.setMandatoryFieldsComplete(true);
                }
            }
        }
    }

    @Override
    public void saveForm(boolean isManualSave) {
        EquipmentPageFragment frag = pageAdapter.getCurrentFragment(equipment.getItems().get(pageIndex));
        saveForm(frag, pageIndex, isManualSave);

    }

    public synchronized void saveForm(EquipmentPageFragment frag, int pageIndex, boolean manualSave) {
        if (pageAdapter != null) {
            boolean isItemSaved = pageAdapter.setItemSaved(pageIndex, manualSave);


            if (!manualSave && !isItemSaved) {
                deleteItemAtIndex(pageIndex);
                return;
            }

            Item item = getItemAtIndex(pageIndex);
            if (!equipment.isQivHidden()) {
                createQIVItem(item, pageIndex);
            }

            float oldRebate = equipment.getRebateValue();
            float val = equipment.computeRebate(item);

            if (pageAdapter != null && frag != null) {
                frag.updateRebate(val);
            }

            Toast.makeText(getActivity(), getString(R.string.saved_string), Toast.LENGTH_LONG).show();
            float equipmentRebate = bean.getEquipmentListRebate();
            equipmentRebate -= oldRebate;
            equipmentRebate += equipment.getRebateValue();
            bean.setEquipmentListRebate(equipmentRebate);
            RebateManager.getInstance().updateFormState(null);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        setValidState();
    }

    private Item getItemAtIndex(int pageIndex) {
        Item item = null;

        if (equipment.getItems() != null && equipment.getItems().size() > pageIndex) {
            item = equipment.getItems().get(pageIndex);
        }
        return item;
    }

    private boolean deleteItemAtIndex(int pageIndex) {
        boolean retVal = false;

        if (equipment.getItems() != null && equipment.getItems().size() > pageIndex && equipment.getItems().size() > 1) {
            final int selectedPageNum = equipment.getItems().get(pageIndex).getPageNum();
            retVal = pageAdapter.deleteItem(pageIndex) != null;

            Thread t = new Thread(new Runnable() {
                public void run() {
                    String equipmentName = equipment.getEquipmentId();
                    FileUtil.deleteFolderPartImage(equipmentName, selectedPageNum + "");
                }
            });
            t.start();
        }
        return retVal;
    }

    private String getQIVItemName(Item item, int pageIndex) {
        String name = null;

        if (item != null && item.getReplaceOnFail() != null) {
            name = equipment.getName() + " " + (pageIndex + 1);
        }

        return name;
    }

    private QIVItem findQIVItem(Item item) {
        QIVItem qivItem = null;

        if (qivList != null) {
            ListIterator<QIVItem> iterator = qivList.listIterator();
            while (iterator.hasNext()) {
                qivItem = (QIVItem) iterator.next();

                if (item.getId() == qivItem.getId()) {
                    break;
                } else {
                    qivItem = null;
                }
            }
        }

        return qivItem;
    }

    private int getQIVItemIndex(QIVItem item) {
        int index = -1;

        if (qivList != null) {
            ListIterator<QIVItem> iterator = qivList.listIterator();
            while (iterator.hasNext()) {
                ++index;
                QIVItem qivItem = (QIVItem) iterator.next();

                if (item.getId() == qivItem.getId()) {
                    break;
                }
            }
        }

        return index;
    }

    private void createQIVItem(Item item, int pageIndex) {
        if (equipment.canCreateQIVFor() && item != null) {
            String name = getQIVItemName(item, pageIndex);
            if (name != null) {
                QIVItem qivItem = findQIVItem(item);
                List<Part> parts = null;
                if (equipment != null && equipment.getEquipmentId() != null && equipment.getEquipmentId().equalsIgnoreCase(AppConstants.GEOTHERMALHEATPUMPS_ID)) {
                    parts = item.getEarlyRetirement().getParts();
                } else {
                    parts = item.getReplaceOnFail().getParts();
                }
                if (qivItem != null) {
                    qivItem.setName(name);
                    updateItem(parts, qivItem);
                } else {
                    QIVItem model = (QIVItem) qivList.get(0).createObject();
                    model.setName(name);
                    model.setId(item.getId());
                    model.setEquipmentId(equipment.getEquipmentId());
                    model.setPageIndex(pageIndex);
                    model.setQivRequired(equipment.isQivRequired());
                    updateItem(parts, model);

                    qivList.add(model);
                }

            }
        }
    }

    private void deleteQIVItem() {
        if (qivList != null) {
            Item item = getItemAtIndex(pageIndex);

            if (item != null) {
                QIVItem qivItem = findQIVItem(item);

                if (qivItem != null) {
                    int qivItemIndex = getQIVItemIndex(qivItem);
                    qivList.remove(qivItemIndex);
                }
            }
        }
    }

    public void updateItem(List<Part> parts, QIVItem qivItem) {
        Part part = UiUtil.getPartById(parts, AppConstants.TONNAGE_ID);
        if (part != null && part.getOptions() != null && part.getOptions().size() > 0) {
            Part part2 = UiUtil.getPartById(qivItem.getPartList(), AppConstants.TONNAGE_ID);
            if (part2 != null && part2.getOptions() != null && part2.getOptions().size() > 0) {
                float tonnage = LibUtils.parseFloat(part.getOptions().get(0).getSavedValue(), 0);
                if (equipment != null && equipment.getEquipmentId() != null && equipment.getEquipmentId().equalsIgnoreCase(AppConstants.MINISPLITS_ID)) {
                    tonnage = tonnage / AppConstants.ONE_TON_TO_BTUH;
                }
                part2.getOptions().get(0).setSavedValue(tonnage + "");
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AppConstants.CAPTURE_PHOTO_REQUEST_CODE && resultCode == FragmentActivity.RESULT_OK) {
            EquipmentPageFragment frag = pageAdapter.getCurrentFragment(equipment.getItems().get(pageIndex));
            if (pageAdapter != null && frag != null) {
                int pageNum = equipment.getItems().get(pageIndex).getPageNum();
                frag.updateCaptureImage(pageNum);
            }
        }
    }

    View partImageLayout;
    TextView partImageName;
    ImageView partImagePreview;

    @Override
    public void onFinishInflate(int layoutId, View view) {
        if (layoutId == R.layout.equipment_image_dialog_layout && view != null) {
            // final Part part = (Part) uiManager.getDialog().getDataObj();
            // partImageLayout = view.findViewById(R.id.image_layout);
            // partImageName = (TextView) view.findViewById(R.id.image_name);
            // partImagePreview = (ImageView) view.findViewById(R.id.image_preview);
            //
            // if (part != null && part.getImagePath() != null)
            // {
            // updatePartImageView(part);
            // }
            // else
            // {
            // partImageLayout.setVisibility(View.GONE);
            // }
            //
            // view.findViewById(R.id.upload_image).setOnClickListener(new OnClickListener()
            // {
            // @Override
            // public void onClick(View v)
            // {
            // Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            // if (part != null)
            // {
            // takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(FileUtil.getFilePathForPartImage(part, pageIndex))));
            // // takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            // }
            //
            // startActivityForResult(takePictureIntent, AppConstants.CAPTURE_PHOTO_REQUEST_CODE);
            // }
            // });

        } else if (layoutId == R.layout.delete_confirmation && view != null) {
            ImageButton cancel = (ImageButton) view.findViewById(R.id.delete_cancel_btn);
            ImageButton done = (ImageButton) view.findViewById(R.id.delete_done_btn);
            cancel.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    commonDialog.dismiss();
                }
            });
            done.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    deleteItem();
                }

            });
        }
    }

    @Override
    public void onDismiss() {
        partImagePreview = null;
        partImageName = null;
        partImageLayout = null;
    }

    @Override
    public void requestData() {
        updateEquipmentViews();
    }

    @Override
    public void getUiControls(View root) {
        equipmentComboBox = (Spinner) root.findViewById(R.id.equipment_selector);
        viewPager = (CustomViewPager) root.findViewById(R.id.pager);
    }

    @Override
    public void initialize(View view) {
        homeScreenactivity = (RootActivity) getActivity();
        if (homeScreenactivity == null) {
            return;
        }
        if (getArguments() != null && getArguments().containsKey("index")) {
            equipmentIndex = getArguments().getInt("index");
        }

        if (bean != null && bean.getEquipments() != null && bean.getEquipments().size() > equipmentIndex) {
            equipment = bean.getEquipments().get(equipmentIndex);
        }

        if (bean != null) {
            // ArrayList<String> list = new ArrayList<>();
            //
            // if (bean.getEquipments() != null)
            // {
            // ListIterator<Equipment> iterator = bean.getEquipments().listIterator();
            // while (iterator.hasNext())
            // {
            // FormResponseBean.Equipment equipment = (FormResponseBean.Equipment) iterator.next();
            // list.add(equipment.getName());
            // }
            //
            // CustomSpinnerAdapter equipmentAdapter = new CustomSpinnerAdapter(getActivity(), 0, list);
            // equipmentComboBox.setAdapter(equipmentAdapter);
            // equipmentComboBox.setSelection(equipmentIndex, true);
            // }
            updateSpinnerAdapter();
            qivList = bean.getQivList();
        }

        pageAdapter = new EquipementPageAdapter(getChildFragmentManager(), viewPager);
        // commWindowViewPagerAdapter = new CommWindowPagerAdapter(activity.getSupportFragmentManager(), commWindowViewPager,
        // activity.getString(R.string.window_conference_call_count));
        // commWindowViewPagerAdapter.add(communicationWindow);
        // viewPager.setPageMargin(5);
        viewPager.setAdapter(pageAdapter);
        viewPager.setOnPageChangeListener(this);
        viewPager.setCurrentItem(0);
        // TODO causing bugs quickfix... look at this again
        if (equipment == null) {
            if (bean == null) {
                bean = RebateManager.getInstance().getFormBean();
                if (bean == null) {
                    bean = RebateManager.getInstance().getDefaultBean();
                }
                if (bean == null) {
                    bean = RebateManager.getInstance().createCachedFormBean();
                }
            }
            equipment = bean.getEquipments().get(equipmentIndex);
        }
        viewPager.setOffscreenPageLimit(equipment.getItems().size());
        CustomPageIndicator indicator = ((CustomPageIndicator) getActivity().findViewById(R.id.page_indicator));
        viewPager.setPageIndicator(indicator);
        indicator.setPager(viewPager);

        // pageAdapter = new EquipementPageAdapter(getChildFragmentManager(), uiManager);
        // viewPager.setAdapter(pageAdapter);
        homeScreenactivity.currenFragment = TAG;

    }

    private synchronized void deleteItem() {
        if (bean != null && equipment != null && pageAdapter != null) {
            Item newItem = null;
            try {
                newItem = equipment.createNewItem();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
            EquipmentPageFragment frag = pageAdapter.getCurrentFragment(equipment.getItems().get(pageIndex));
            final int selectedPageNum = equipment.getItems().get(pageIndex).getPageNum();
            Log.v(TAG, "deleteItem " + pageIndex);
            if (frag != null) {
                frag.setFragmentDeleted();
            }
            deleteQIVItem();
            // viewPager.setCurrentItem(equipmentIndex);
            Item deletedItem = pageAdapter.deleteItem(pageIndex);

            if (deletedItem != null) {
                float rebateValue = bean.getEquipmentListRebate();
                rebateValue -= deletedItem.getRebateValue();
                bean.setEquipmentListRebate(rebateValue);
            }

            Thread t = new Thread(new Runnable() {
                public void run() {
                    String equipmentName = equipment.getEquipmentId();
                    FileUtil.deleteFolderPartImage(equipmentName, selectedPageNum + "");
                }
            });
            t.start();

            if (newItem != null && equipment.getItems().size() == 0) {
                addNewItem(newItem);
            }
            pageAdapter.notifyDataSetChanged();
            viewPager.setOffscreenPageLimit(equipment.getItems().size());
            viewPager.firePageCountChanged();

            if (pageIndex >= equipment.getItems().size()) {
                --pageIndex;
            }
            viewPager.setCurrentItem(pageIndex);
            updateSpinnerAdapter();
        }
        commonDialog.dismiss();
    }

    private void updateSpinnerAdapter() {
        if (equipment != null) {
            List<Item> itemList = equipment.getItems();
            ArrayList<String> list = new ArrayList<String>();
            if (itemList != null && itemList.size() > 0) {
                for (int i = 0; i < itemList.size(); i++) {
                    list.add(equipment.getName() + " " + (i + 1));
                }
            } else {
                list.add(equipment.getName() + "1");
            }

            CustomSpinnerAdapter equipmentAdapter = new CustomSpinnerAdapter(getActivity(), 0, list);
            equipmentComboBox.setAdapter(equipmentAdapter);
            equipmentComboBox.setSelection(pageIndex, true);
        }

    }

    @Override
    public void bindCallbacks(View root) {
        root.findViewById(R.id.save_btn).setOnClickListener(this);
        root.findViewById(R.id.close_btn).setOnClickListener(this);
        root.findViewById(R.id.add_btn).setOnClickListener(this);
        equipmentComboBox.setOnItemSelectedListener(this);
        // viewPager.setOnPageChangeListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        viewPager.setCurrentItem(position);
        // if (equipmentIndex != position)
        // {
        // equipmentIndex = position;
        // equipment = bean.getEquipments().get(equipmentIndex);
        // updateEquipmentViews();
        // }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onPageScrollStateChanged(int arg0) {

    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {

    }

    @Override
    public void onPageSelected(int index) {
        pageIndex = index;
        equipmentComboBox.post(new Runnable() {

            @Override
            public void run() {
                equipmentComboBox.setSelection(viewPager.getCurrentItem(), true);
            }
        });
    }

    public String getPageName() {
        EquipmentPageFragment fragm = (EquipmentPageFragment) pageAdapter.getItem(pageIndex);
        return fragm.getItem().getPageNum() + "";
    }

    // public int getPageIndex()
    // {
    // return pageIndex;
    // }

    public List<Item> getEquipmentItems() {
        if (equipment != null) {
            return equipment.getItems();
        } else {
            return null;
        }

    }
}
