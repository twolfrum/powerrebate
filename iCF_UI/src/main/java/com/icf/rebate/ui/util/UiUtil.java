package com.icf.rebate.ui.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import com.icf.ameren.rebate.ui.BuildConfig;
import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.networklayer.HttpManagerListener;
import com.icf.rebate.networklayer.ICFHttpManager;
import com.icf.rebate.networklayer.model.CustomerInfo;
import com.icf.rebate.networklayer.model.FormResponseBean.Options;
import com.icf.rebate.networklayer.model.FormResponseBean.Part;
import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.rebate.ui.RootActivity;
import com.icf.rebate.ui.controller.DatePickWatcher;
import com.icf.rebate.ui.controller.FormUIManager;
import com.icf.rebate.ui.customviews.ICFProgressDialog;
import com.icf.rebate.ui.dialog.DatePickerFragment;
import com.icf.rebate.ui.dialog.DialogListener;
import com.icf.rebate.ui.dialog.ICFDialogFragment;
import com.icf.rebate.ui.util.rebate.ICFSQLiteOpenHelper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class UiUtil
{

    private static ICFProgressDialog proDialog;

    private static ICFDialogFragment commonDialog;

    private static Dialog spinnerDialog;

    public static int MAX_IMAGE_DOWNLOAD_RETRY_COUNT = 3;

    public static boolean isTabletDevice;

    public static final int DASHBOARD_IMAGE_OPTIONS = 1;

    public static final boolean DEBUG_BUILD = BuildConfig.DEBUG;

    private static EditText barEditText;

    private static Options barEditOptions;

    public static boolean SCREEN_INCH_7 = false;

    public static FormUIManager currentFormManager;

    public static String manifoldKey;

    public static Options manifoldOptions;
    
    // temp code : need to remove it
    public static WeakReference<RootActivity> rootActivity;
    
    private static UiUtil singleton; 

    private static UiUtil getInstance() {
	if (singleton == null) singleton = new UiUtil();
	return singleton;
    }

    // private static Hashtable<String, Integer> uploadProgress;

    // private static Hashtable<Integer, DisplayImageOptions> displayImageOptionList;

    public static void StartLoadingBarActivity(final Activity activity, int titleId, int id, boolean isCancellable)
    {
	StartLoadingBarActivity(activity, titleId, id, isCancellable, true);
    }

    public static void StartLoadingBarActivity(final Activity activity, int titleId, int id, boolean isCancellable, boolean background)
    {
	StartLoadingBarActivity(activity, titleId, id, isCancellable, true, null);
    }

    public static void StartLoadingBarActivity(final Activity activity, int titleId, int id, boolean isCancellable, boolean background, DialogInterface.OnCancelListener cancelListener)
    {
	if (proDialog != null)
	{
	    if (proDialog.isShowing() && proDialog.getId() == id)
	    {
		return;
	    }
	}
	stopLoadingBar();

	String msg = activity.getString(id);
	proDialog = new ICFProgressDialog(activity);
	proDialog.setId(id);
	if (titleId > -1)
	{

	    proDialog.setTitle(titleId);
	}

	if (id > -1)
	{
	    proDialog.setMessage(activity.getString(id));
	}

	proDialog.setIndeterminate(false);
	proDialog.setCancelable(isCancellable);
	proDialog.setCanceledOnTouchOutside(false);
	if (isCancellable && cancelListener != null)
	{
	    proDialog.setOnCancelListener(cancelListener);
	}

	proDialog.setOnDismissListener(new DialogInterface.OnDismissListener()
	{

	    @Override
	    public void onDismiss(DialogInterface dialog)
	    {
		// Log.d("", "On Dismiss gg");
	    }
	});
	proDialog.show();

    }

    public static void stopLoadingBar()
    {
	if (proDialog != null && proDialog.isShowing())
	{
	    proDialog.dismiss();
	    proDialog = null;
	}
    }

    public static void showProgressDialog(Activity activity)
    {
	if (activity != null && !activity.isFinishing())
	{
	    try
	    {
		if (spinnerDialog != null)
		{
		    dismissSpinnerDialog();
		}
		spinnerDialog  = new ProgressDialog(activity, R.style.MyTheme_Progress);
            spinnerDialog.setCancelable(false);
            ((ProgressDialog)spinnerDialog).setIndeterminate(true);
            spinnerDialog.show();
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}
    }

    public static void dismissSpinnerDialog()
    {
	try
	{
	    if (spinnerDialog != null && spinnerDialog.isShowing())
	    {
		spinnerDialog.setOnCancelListener(null);
		spinnerDialog.dismiss();
	    }
	    spinnerDialog = null;
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
    }

    public static void showDialog(final FragmentManager fm, final String header, final String content, final DialogListener listener)
    {
	commonDialog = ICFDialogFragment.newDialogFrag(header, content);
	commonDialog.setDialogListener(listener);
	commonDialog.showCommitAllowingLoss(fm, "commonDialog");
	fm.executePendingTransactions();
    }

    public static void showDialog(final FragmentManager fm, final String header, final String content)
    {
	commonDialog = ICFDialogFragment.newDialogFrag(header, content);
	commonDialog.showCommitAllowingLoss(fm, "commonDialog");
	fm.executePendingTransactions();
    }

    public static void showDialog(final FragmentManager fm, int layoutId)
    {
	commonDialog = ICFDialogFragment.newDialogFrag(layoutId);
	commonDialog.showCommitAllowingLoss(fm, "commonDialog");
	fm.executePendingTransactions();
    }
    
    public static void showPasswordResetDialog(final Activity ctx, final HttpManagerListener httpListener)
    {
	// null check
	if (ctx == null || httpListener == null) {
	    return;
	}
	AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
	builder.setTitle(ctx.getString(R.string.forgot_password));
	
	LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	View lyt = inflater.inflate(R.layout.lyt_diag_forgot_password, null, false);
	builder.setView(lyt);
	
	final EditText input = (EditText) lyt.findViewById(R.id.edt_forgot_password);
	
	// Set up the buttons
	builder.setPositiveButton("OK", new DialogInterface.OnClickListener() { 
	    @Override
	    public void onClick(DialogInterface dialog, int which) {
		ICFHttpManager httpManager = ICFHttpManager.getInstance();
		if (httpManager != null) {
		    httpManager.resetPassword(httpListener, input.getText().toString());
		}
		View view = ctx.getCurrentFocus();
		if (view != null) {  
		    InputMethodManager imm = (InputMethodManager)ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
		    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
		}
		dialog.cancel();
	    }
	});
	builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
	    @Override
	    public void onClick(DialogInterface dialog, int which) {
	        dialog.cancel();
	    }
	});

	builder.show();
    }
    
    public static void showDatePickDialog (final FragmentManager fm, Context context,
	    final Calendar minDate, final Calendar maxDate, final DatePickWatcher pickWatcher) {
	
	Bundle bundle = new Bundle();
	//set acceptable date range
	bundle.putLong("minDate", minDate != null ? minDate.getTimeInMillis() : 0);
	bundle.putLong("maxDate", maxDate != null ? maxDate.getTimeInMillis() : 0);
	
	//parse the existing string date value to initialize the picker
        int year;
        int month;
        int day;

	String currentDate = pickWatcher.getCurrentDate();
	if (!TextUtils.isEmpty(currentDate)) {
	    String[] dateTokens = currentDate.split("/");
	    month = Integer.parseInt(dateTokens[0]) - 1;
	    day = Integer.parseInt(dateTokens[1]);
	    year = Integer.parseInt(dateTokens[2]);
	} else {
	    Calendar cc = Calendar.getInstance();
	    year = cc.get(Calendar.YEAR);
	    month = cc.get(Calendar.MONTH);
	    day = cc.get(Calendar.DAY_OF_MONTH);
	}
	bundle.putInt("initMonth", month);
	bundle.putInt("initDay", day);
	bundle.putInt("initYear", year);

	DialogFragment dlg = new DatePickerFragment();
	((DatePickerFragment)dlg).setPickWatcher(pickWatcher);
	dlg.setArguments(bundle);
	dlg.show(fm, "datePicker");
    }

    public static void dismissDialog()
    {
	if (commonDialog != null && commonDialog.isVisible())
	{
	    commonDialog.dismiss();
	    commonDialog = null;
	}
    }

    public static void hideSoftKeyboard(Activity activity, View view)
    {
	if (view != null && view.getApplicationWindowToken() != null)
	{
	    InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
	    imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
	}

    }

    public static void showNetworkError(Message msg, final Activity activity)
    {
	if (activity != null)
	{
	    activity.runOnUiThread(new Runnable()
	    {

		@Override
		public void run()
		{
		    UiUtil.dismissSpinnerDialog();
		    showError(activity, activity.getString(R.string.error), activity.getString(R.string.network_error));
		}
	    });
	}

    }

    public static void showError(Activity activity, String title, String message)
    {

	AlertDialog.Builder adb = new AlertDialog.Builder(activity).setTitle(title).setMessage(message).setPositiveButton(R.string.alert_ok, new DialogInterface.OnClickListener()
	{
	    public void onClick(DialogInterface dialog, int which)
	    {
		dialog.dismiss();
	    }
	}).setIcon(R.drawable.ic_dialog_alert_holo_light);
	AlertDialog ad = adb.create();
	ad.setCanceledOnTouchOutside(false);
	ad.show();

	LayoutParams params = ad.getWindow().getAttributes();
	if (params != null)
	{
	    params.width = (int) (UiUtil.getDisplayWidth(activity.getApplicationContext()) * 0.80f);
	}
	ad.getWindow().setAttributes(params);

	((TextView) ad.findViewById(android.R.id.message)).setLinksClickable(true);
	((TextView) ad.findViewById(android.R.id.message)).setText(Html.fromHtml(message));
	((TextView) ad.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
    }

    public static void showError(Activity activity, String title, String message, DialogInterface.OnClickListener listener)
    {
	new AlertDialog.Builder(activity).setTitle(title).setMessage(message).setPositiveButton(R.string.alert_ok, listener).setIcon(R.drawable.ic_dialog_alert_holo_light).show();
    }
    // Don't need to show() the returned alertDialog here (this method shows the dialog for you)
    public static AlertDialog showError(Activity activity, String title, String message, DialogInterface.OnClickListener listener, boolean cancelable)
    {
	AlertDialog alertDialog = new AlertDialog.Builder(activity).setTitle(title).setMessage(message).setPositiveButton(R.string.alert_ok, listener).setIcon(R.drawable.ic_dialog_alert_holo_light).create();
	alertDialog.setCancelable(cancelable);
	alertDialog.show();
	return alertDialog;
    }
    
    /**
     * Dismiss all DialogFragments added to given FragmentManager and child fragments
     */
    public static void dismissAllDialogs(FragmentManager manager) {
        List<Fragment> fragments = manager.getFragments();

        if (fragments == null)
            return;

        for (Fragment fragment : fragments) {
            if (fragment instanceof DialogFragment) {
                DialogFragment dialogFragment = (DialogFragment) fragment;
                dialogFragment.dismissAllowingStateLoss();
            }

            FragmentManager childFragmentManager = fragment.getChildFragmentManager();
            if (childFragmentManager != null)
                dismissAllDialogs(childFragmentManager);
        }
    }

    public static void showConfirmation(Activity activity, String title, String message, DialogInterface.OnClickListener listener)
    {
	new AlertDialog.Builder(activity).setTitle(title).setMessage(message).setPositiveButton(R.string.alert_ok, listener).setNegativeButton(R.string.alert_cancel, listener).setIcon(R.drawable.ic_dialog_alert_holo_light).show();
    }

    public static void showConfirmation(Activity activity, String title, String message, int positiveText, int negativeText, DialogInterface.OnClickListener listener)
    {
	new AlertDialog.Builder(activity).setTitle(title).setMessage(message).setPositiveButton(positiveText, listener).setNegativeButton(negativeText, listener).setIcon(R.drawable.ic_dialog_alert_holo_light).show();
    }

    public static void showError(Activity activity, String title, String message, int positiveText, int negativeText, DialogInterface.OnClickListener listener)
    {
	new AlertDialog.Builder(activity).setTitle(title).setMessage(message).setPositiveButton(positiveText, listener).setNegativeButton(negativeText, listener).setIcon(R.drawable.ic_dialog_alert_holo_light).show();
    }

    public static boolean isLandscape(Context context)
    {
	Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
	Point size = new Point();
	display.getSize(size);
	int width = size.x;
	int height = size.y;
	if (width > height)
	{
	    return true;
	}
	else
	{
	    return false;
	}

    }

    //
    // public static DisplayImageOptions getImageOptions(int id)
    // {
    // if (displayImageOptionList == null)
    // {
    // displayImageOptionList = new Hashtable<Integer, DisplayImageOptions>();
    // }
    //
    // switch (id)
    // {
    // case DASHBOARD_IMAGE_OPTIONS:
    // if (!displayImageOptionList.containsKey(id))
    // {
    // DisplayImageOptions.Builder builder = new DisplayImageOptions.Builder();
    // // builder.showStubImage(R.drawable.default_pic);
    // builder.resetViewBeforeLoading(false);
    // builder.cacheInMemory(true);
    // builder.cacheOnDisc(true);
    // builder.imageScaleType(ImageScaleType.EXACTLY_STRETCHED);
    // builder.bitmapConfig(Config.RGB_565);
    // // builder.showImageForEmptyUri(R.drawable.default_pic_big);
    // // builder.showImageOnFail(R.drawable.default_pic_big);
    // DisplayImageOptions option = builder.build();
    // displayImageOptionList.put(id, option);
    // }
    // break;
    // }
    // return null;
    // }

    public static int getResource(String text)
    {

	if (text == null)
	{
	    return 0;
	}
	try
	{
	    int lstIndex = text.lastIndexOf('/');
	    text = text.substring(lstIndex + 1);
	    if (text.startsWith("pending"))
	    {
		return R.drawable.resource_pendingjobs;
	    }
	    else if (text.startsWith("dashboard"))
	    {
		return R.drawable.resource_dashboard_wg_logo;
	    }
	    else if (text.startsWith("rebat"))
	    {
		return R.drawable.resource_rebate;
	    }
	    else if (text.startsWith("duc"))
	    {
		return R.drawable.resource_duct;
	    }
	    else if (text.startsWith("confirmation_dp"))
	    {
		return R.drawable.resource_confirmation_dp_logo;
	    }
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	return 0;
    }

    public static void showNotImplemented(Context context)
    {
	Toast.makeText(context, "Not Implemented", Toast.LENGTH_SHORT).show();
    }
    
    public static void showToast(Context ctx, String value) {
	Toast.makeText(ctx, value, Toast.LENGTH_SHORT).show();
    }

    public static Bitmap scaleBitmap(Bitmap bitmap, int pixelSize)
    {
	if (bitmap.getWidth() > bitmap.getHeight() && bitmap.getWidth() > pixelSize)
	{
	    bitmap = Bitmap.createScaledBitmap(bitmap, pixelSize, (pixelSize * bitmap.getHeight()) / bitmap.getWidth(), false);
	}
	else if (bitmap.getHeight() > bitmap.getWidth() && bitmap.getHeight() > pixelSize)
	{
	    bitmap = Bitmap.createScaledBitmap(bitmap, (pixelSize * bitmap.getWidth()) / bitmap.getHeight(), pixelSize, false);
	}
	return bitmap;
    }

    public static int getDisplayHeight(Context context)
    {
	WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
	Point p = new Point();
	wm.getDefaultDisplay().getSize(p);
	return p.y;
    }

    public static int getDisplayWidth(Context context)
    {
	WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
	Point p = new Point();
	wm.getDefaultDisplay().getSize(p);
	return p.x;
    }

    public static void setCurrentBarCodeView(EditText edit)
    {
	barEditText = edit;
    }
    
    public static void setCurrentBarCodeOptions(Options options) {
	barEditOptions = options;
    }

    public static void updateBarCodeText(String value)
    {
	if (barEditText != null)
	{
	    barEditText.setText(value);
	    if (!barEditText.hasFocus())
	    {
		Context context = barEditText.getContext();
		if (context != null)
		{
		    if (value != null && value.length() > 0)
		    {
			barEditText.setBackground(context.getResources().getDrawable(R.drawable.bg_dialog_box));
			barEditOptions.setBarcodeScanned(true);
		    }
		    else
		    {
			barEditText.setBackground(context.getResources().getDrawable(R.drawable.bg_dialog_mandatory_box));
		    }
		}
	    }
	    else
	    {
		barEditText.clearFocus();
	    }
	}
    }

    public static Part getPartByName(List<Part> parts, String name)
    {
	Part retPart = null;

	if (parts != null)
	{
	    ListIterator<Part> iterator = parts.listIterator();
	    while (iterator.hasNext())
	    {
		Part part = (Part) iterator.next();
		if (part.getName() != null && part.getName().equalsIgnoreCase(name))
		{
		    retPart = part;
		    break;
		}
	    }
	}
	return retPart;
    }

    public static Part getPartById(List<Part> parts, String id)
    {
	Part retPart = null;

	if (parts != null)
	{
	    ListIterator<Part> iterator = parts.listIterator();
	    while (iterator.hasNext())
	    {
		Part part = (Part) iterator.next();
		if (part.getId() != null && part.getId().equalsIgnoreCase(id))
		{
		    retPart = part;
		    break;
		}
	    }
	}
	return retPart;
    }

    public static Options getOptionByName(List<Part> parts, String name)
    {
	Options option = null;

	if (parts != null)
	{
	    ListIterator<Part> iterator = parts.listIterator();
	    while (iterator.hasNext())
	    {
		Part part = (Part) iterator.next();
		option = getOptionByName(part, name);
	    }
	}

	return option;
    }

    public static Options getOptionByName(Part part, String name)
    {
	Options option = null;
	if (part != null && part.getOptions() != null)
	{
	    ListIterator<Options> iterator = part.getOptions().listIterator();
	    while (iterator.hasNext())
	    {
		Options options = (Options) iterator.next();
		if (options.getName() != null && options.getName().equalsIgnoreCase(name))
		{
		    option = options;
		    break;
		}
	    }
	}

	return option;
    }

    public static ICFDialogFragment photoDialog;

    private static DialogListener listener = new DialogListener()
    {

	private Bitmap mBitmap;

	@Override
	public void onFinishInflate(int layoutId, View view)
	{
	    if (layoutId == R.layout.image_preview_popup && view != null && photoDialog != null && photoDialog.getDataObj() != null)
	    {
		final Part part = (Part) photoDialog.getDataObj();
		final ImageView previewImage = (ImageView) view.findViewById(R.id.preview_image);
		ImageButton done = (ImageButton) view.findViewById(R.id.done_image);
		Handler h = new Handler();
		h.post(new Runnable()
		{
		    @Override
		    public void run()
		    {
			String url = Uri.fromFile(new File(FileUtil.getAbsolutePath(part.getImagePath()))).toString();
			try
			{
			    URL newurl = new URL(url);
			    mBitmap = BitmapFactory.decodeStream(newurl.openConnection().getInputStream());
			    previewImage.setImageBitmap(mBitmap);
			}
			catch (Exception e)
			{
			    Log.e("Error", e.getMessage());
			}
		    }
		});

		done.setOnClickListener(new OnClickListener()
		{

		    @Override
		    public void onClick(View v)
		    {
			UiUtil.closePhotoDialog();
		    }
		});
	    }
	}

	@Override
	public void onDismiss()
	{
	    if (mBitmap != null)
	    {
		mBitmap.recycle();
		mBitmap = null;
	    }
	}
    };

    public static boolean serializationMode;

    public static void closePhotoDialog()
    {
	if (photoDialog != null)
	{
	    photoDialog.dismiss();
	    photoDialog = null;
	}
    }

    private static Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth)
    {

	int width = bm.getWidth();

	int height = bm.getHeight();

	float scaleWidth = ((float) newWidth) / width;

	float scaleHeight = ((float) newHeight) / height;

	// create a matrix for the manipulation

	Matrix matrix = new Matrix();

	// resize the bit map

	matrix.postScale(scaleWidth, scaleHeight);

	// recreate the new Bitmap

	Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);

	return resizedBitmap;

    }

    private static ArrayList<String> cleanUpBitmapList = new ArrayList<String>();

    public static void cleanBitmap(View view)
    {
	if (view == null)
	{
	    cleanUpBitmapList.clear();
	    return;
	}
	for (String tempString : cleanUpBitmapList)
	{
	    View oldView = view.findViewWithTag(tempString);
	    if (oldView instanceof ImageView)
	    {
		Drawable d = ((ImageView) oldView).getDrawable();
		if (d instanceof BitmapDrawable)
		{
		    Bitmap b = ((BitmapDrawable) d).getBitmap();
		    if (b != null)
		    {
			b.recycle();
			b = null;
		    }
		}
	    }
	}
	cleanUpBitmapList.clear();
    }

    public static void addCleanUpBitmapTask(View view)
    {
	if (view == null)
	{
	    cleanUpBitmapList.clear();
	    return;
	}
	String oldTag = (String) view.getTag();
	if (oldTag != null)
	{
	    if (cleanUpBitmapList.indexOf(oldTag) > -1)
	    {
		View rootView = view.getRootView();
		View oldView = rootView.findViewWithTag(oldTag);
		if (oldView instanceof ImageView)
		{
		    Drawable d = ((ImageView) oldView).getDrawable();
		    if (d instanceof BitmapDrawable)
		    {
			Bitmap b = ((BitmapDrawable) d).getBitmap();
			if (b != null)
			{
			    b.recycle();
			    b = null;
			}
		    }
		}
		cleanUpBitmapList.remove(oldTag);
	    }
	}
	String tag = "cleanup_" + System.nanoTime();
	view.setTag(tag);
	cleanUpBitmapList.add(tag);
    }

    public static void showPreviewDialog(View view, String path, final Part part)
    {
	if (view != null)
	{
	    ViewGroup group = (ViewGroup) view.getParent();
	    if (group.getChildAt(1) instanceof ImageView)
	    {
		((ImageView) group.getChildAt(1)).setImageURI(null);
		addCleanUpBitmapTask(((ImageView) group.getChildAt(1)));

		Bitmap bitmap = BitmapFactory.decodeFile(path);
		if (bitmap != null)
		{
		    Bitmap scaleBitmap = getResizedBitmap(bitmap, 50, 100);
		    if (scaleBitmap != null && !scaleBitmap.equals(bitmap))
		    {
			bitmap.recycle();
			bitmap = null;
			bitmap = scaleBitmap;
		    }
		}
		// ((ImageView) group.getChildAt(1)).setImageURI(Uri.parse(path));
		((ImageView) group.getChildAt(1)).setImageBitmap(bitmap);
	    }
	    group.getChildAt(1).setVisibility(View.VISIBLE);
	    group.getChildAt(1).setOnClickListener(new OnClickListener()
	    {
		@Override
		public void onClick(View v)
		{
		    photoDialog = ICFDialogFragment.newDialogFrag(R.layout.image_preview_popup);
		    photoDialog.setDataObj(part);
		    photoDialog.setDialogListener(listener);
		    photoDialog.show(rootActivity.get().getSupportFragmentManager(), "PreviewDialog");

		}
	    });

	}
    }

    public static void dumpDB(Context context)
    {
	try
	{
	    File sd = Environment.getExternalStorageDirectory();

	    if (sd.canWrite())
	    {
		ICFSQLiteOpenHelper helper = new ICFSQLiteOpenHelper(context.getApplicationContext());
		SQLiteDatabase database = helper.getReadableDatabase();

		String backupDBPath = ICFSQLiteOpenHelper.DB_NAME;
		File currentDB = new File(database.getPath());
		File backupDB = new File(sd, backupDBPath);

		if (currentDB.exists())
		{
		    FileChannel src = new FileInputStream(currentDB).getChannel();
		    FileChannel dst = new FileOutputStream(backupDB).getChannel();
		    dst.transferFrom(src, 0, src.size());
		    src.close();
		    dst.close();
		}
	    }
	}
	catch (Exception e)
	{

	}
    }

    public static int resolveBitmapOrientation(File bitmapFile) throws IOException
    {
	ExifInterface exif = null;
	exif = new ExifInterface(bitmapFile.getAbsolutePath());

	return exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
    }

    public static Bitmap applyOrientation(Bitmap bitmap, int orientation)
    {
	{
	    int rotate = 0;
	    switch (orientation)
	    {
	    case ExifInterface.ORIENTATION_ROTATE_270:
		rotate = 270;
		break;
	    case ExifInterface.ORIENTATION_ROTATE_180:
		rotate = 180;
		break;
	    case ExifInterface.ORIENTATION_ROTATE_90:
		rotate = 90;
		break;
	    default:
		return bitmap;
	    }

	    int w = bitmap.getWidth();
	    int h = bitmap.getHeight();
	    Matrix mtx = new Matrix();
	    mtx.postRotate(rotate);
	    return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);
	}
    }

    public static void updateCaptureImage(FormUIManager uiManager, String pageIndex, FragmentActivity activity)
    {
	if (uiManager != null && uiManager.getCaptuerImageView() != null)
	{
	    Part part = (Part) uiManager.getCaptuerImageView().getTag();

	    if (part != null)
	    {
		String filePath;
		filePath = FileUtil.getFilePathForPartImage(uiManager.getEquipmentName(), part, pageIndex);
		// code to resize the image to reduce the size.
		File file = new File(filePath);
		if (file != null && file.exists())
		{
		    int orientation = -1;
		    try
		    {
			orientation = resolveBitmapOrientation(file);
		    }
		    catch (IOException e1)
		    {
			e1.printStackTrace();
		    }
		    Bitmap bitmap = BitmapFactory.decodeFile(filePath);
		    if (bitmap != null)
		    {
			Bitmap scaleBitmap = UiUtil.scaleBitmap(bitmap, AppConstants.CAPTURE_IMAGE_MEDIUM_SIZE);
			if (scaleBitmap != null && !scaleBitmap.equals(bitmap))
			{
			    bitmap.recycle();
			    bitmap = scaleBitmap;
			}

			if (orientation != -1)
			{
			    Bitmap orientBitmap = applyOrientation(bitmap, orientation);
			    if (orientBitmap != null && !orientBitmap.equals(bitmap))
			    {
				bitmap.recycle();
				bitmap = orientBitmap;
			    }
			}
			OutputStream os = null;
			try
			{
			    if (file.delete())
			    {
				file = new File(filePath);
				os = new FileOutputStream(file);
				bitmap.compress(CompressFormat.JPEG, 100, os);
				part.setImagePath(FileUtil.getRelativePath(filePath));

				View view = uiManager.getCaptuerImageView();

				if (part != null && part.isMandatory() && (part.getImagePath() == null || TextUtils.isEmpty(part.getImagePath())) && UiViewUtils.isValidationMode(part.isItemSaved()))
				{
				    view.setBackground(uiManager.getCaptuerImageView().getResources().getDrawable(R.drawable.picture_mandatory_bg_selector));
				}
				else
				{
				    view.setBackground(uiManager.getCaptuerImageView().getResources().getDrawable(R.drawable.btn_bg_selector));

				}
				int margin = activity.getResources().getDimensionPixelOffset(R.dimen.form_screen_part_detail_top_margin);
				view.setPadding(margin, margin, margin, margin);
				UiUtil.showPreviewDialog(view, FileUtil.getAbsolutePath(part.getImagePath()), part);
			    }

			}
			catch (FileNotFoundException e)
			{
			    e.printStackTrace();
			}
			finally
			{
			    if (os != null)
			    {
				try
				{
				    os.flush();
				    os.close();
				}
				catch (Exception e)
				{

				}
			    }

			    if (bitmap != null)
			    {
				bitmap.recycle();
				bitmap = null;
			    }
			}
		    }
		}
		LibUtils.updateExifData(filePath);
	    }
	}

    }

    public static boolean isJobCompleted(CustomerInfo info)
    {
	if (info != null && (info.getState() == CustomerInfo.SUBMISSION_IN_PROGRESS || info.getState() == CustomerInfo.COMPLETED_JOB || info.getState() == CustomerInfo.SUBMISSION_ERROR))
	{
	    return true;
	}
	return false;
    }

    public static boolean isJobSubmitted(CustomerInfo info)
    {
	if (info != null && (info.getState() == CustomerInfo.SUBMITTED))
	{
	    return true;
	}
	return false;
    }

    public static void setEnableView(ViewGroup viewGroup, boolean enable, ArrayList<View> excludeView)
    {
	for (int i = 0; i < viewGroup.getChildCount(); i++)
	{
	    View child = viewGroup.getChildAt(i);
	    if (child instanceof ViewGroup)
	    {
		setEnableView((ViewGroup) child, enable, excludeView);
	    }
	    if (excludeView != null)
	    {
		if (excludeView.indexOf(child) > -1)
		{
		    continue;
		}
	    }
	    child.setEnabled(enable);
	}
    }

    public static float convertToPostTwoDigtDecimal(float val)
    {
	// val = Math.round(val);
	String s = String.format("%.2f", val);
	val = Float.parseFloat(s);
	return val;
    }

    //
    // public static void removeSubmissionProgress(String id)
    // {
    // if (uploadProgress == null)
    // {
    // uploadProgress = new Hashtable<String, Integer>();
    // }
    // uploadProgress.remove(id);
    // }
    //
    // public static void updateSubmissionProgress(String id, int progress)
    // {
    // if (uploadProgress == null)
    // {
    // uploadProgress = new Hashtable<String, Integer>();
    // }
    // uploadProgress.put(id, progress);
    // }
    //
    // public static int getUpdateSubmissionProgress(String id)
    // {
    // if (uploadProgress != null)
    // {
    // Integer temp = uploadProgress.get(id);
    // if (temp != null)
    // {
    // return temp;
    // }
    // }
    // return -1;
    // }

    public static int validateForm(List<Part> partList)
    {
	int formFilled = -1;

	boolean isMandtoryFieldsCompelte = true;
	boolean isFormFilled = false;

	int partCount = 0;

	if (partList != null)
	{
	    Iterator<Part> parts = partList.listIterator();
	    String defaultSystemType = null;

	    while (parts.hasNext())
	    {
		Part part = (Part) parts.next();
		if (part != null && part.getOptions() != null && !part.isHidden() && (part.getId() == null || !part.getId().equalsIgnoreCase(AppConstants.TUNE_UP_PAGE_NAME)) && (defaultSystemType == null || (part.getSystemType() != null && defaultSystemType.equalsIgnoreCase(part.getSystemType()))))
		{
		    ++partCount;
		    if (part.getId() != null && part.getId().equalsIgnoreCase(AppConstants.SYSTEM_TYPE) && part.getOptions() != null && part.getOptions().get(0).getSavedValue() != null)
		    {
			defaultSystemType = part.getOptions().get(0).getSavedValue();
		    }

		    if (part.hasPicture()){
                if (part.getImagePath() != null && !TextUtils.isEmpty(part.getImagePath()))
                {
                    isFormFilled = true;
                }
                else if (part.isMandatory())
                {
                    isMandtoryFieldsCompelte = false;
                }
		    }

		    Iterator<Options> option = part.getOptions().listIterator();

		    while (option.hasNext())
		    {
                Options options = (Options) option.next();
                if (options != null)
                {
                    if (options.getXmlName() != null && options.getXmlName().equalsIgnoreCase("iManifold_File_Name"))
                    {
                    continue;
                    }
                    if (options.getDefaultSystemType() != null && options.getSavedValue() != null && options.getSavedValue().equalsIgnoreCase(options.getDefaultSystemType()))
                    {
                    continue;
                    }
                    if (!isFormFilled)
                    {
                    isFormFilled = options.isFieldFilled();
                    }

                    if (options.isMandatory() && !options.isFieldFilled())
                    {
                    isMandtoryFieldsCompelte = false;
                    }
                }

                if (isFormFilled && !isMandtoryFieldsCompelte)
                {
                    break;
                }
		    }

		    if (isFormFilled && !isMandtoryFieldsCompelte)
		    {
			break;
		    }
		}
		else if (part != null && part.getOptions() == null && !part.isHidden())
		{
		    if (part.hasPicture())
		    {
			if (part.getImagePath() != null && !TextUtils.isEmpty(part.getImagePath()))
			{
			    isFormFilled = true;
			}
			else if (part.isMandatory())
			{
			    isMandtoryFieldsCompelte = false;
			}
		    }
		}
	    }
	}

	if (partCount == 0)
	{
	    isFormFilled = true;
	}

	if (isFormFilled && isMandtoryFieldsCompelte)
	{
	    formFilled = AppConstants.FORM_ITEM_FILLED_STATE.ITEM_COMPLETELY_SAVED.getState();
	}
	else if (isFormFilled && !isMandtoryFieldsCompelte)
	{
	    formFilled = AppConstants.FORM_ITEM_FILLED_STATE.ITEM_PARTIALLY_SAVED.getState();
	}
	else if (!isFormFilled)
	{
	    formFilled = AppConstants.FORM_ITEM_FILLED_STATE.ITEM_UNEDITED.getState();
	}
	else
	{
	    formFilled = AppConstants.FORM_ITEM_FILLED_STATE.ITEM_PARTIALLY_SAVED.getState();
	}

	return formFilled;
    }
    
    public static Bitmap bitmapResizer(Bitmap bitmap, int newWidth, int newHeight) {
        // Get the source image's dimensions
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inBitmap = bitmap;
        options.inJustDecodeBounds = true;

        int srcWidth = bitmap.getWidth();
        int srcHeight = bitmap.getHeight();

        // Only scale if the source is big enough. This code is just trying to fit a image into a certain width.
        if (newWidth > srcWidth)
            newWidth = srcWidth;


        // Calculate the correct inSampleSize/scale value. This helps reduce memory use. It should be a power of 2
        // from: http://stackoverflow.com/questions/477572/android-strange-out-of-memory-issue/823966#823966
        int inSampleSize = 1;
        while (srcWidth / 2 > newWidth) {
            srcWidth /= 2;
            srcHeight /= 2;
            inSampleSize *= 2;
        }

        float desiredScale = (float) newWidth / srcWidth;

        // Decode with inSampleSize
        options.inJustDecodeBounds = false;
        options.inDither = false;
        options.inSampleSize = inSampleSize;
        options.inScaled = false;
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;

        // Resize
        Matrix matrix = new Matrix();
        matrix.postScale(desiredScale, desiredScale);
        Bitmap scaledBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        bitmap = null;

        // Save
        return scaledBitmap;

    }
    

/**
 * Scales the provided bitmap to have the height and width provided.
 * (Alternative method for scaling bitmaps
 * since Bitmap.createScaledBitmap(...) produces bad (blocky) quality bitmaps.)
 * 
 * @param bitmap is the bitmap to scale.
 * @param newWidth is the desired width of the scaled bitmap.
 * @param newHeight is the desired height of the scaled bitmap.
 * @return the scaled bitmap.
 */
 public static Bitmap scaleBitmap(Bitmap bitmap, int newWidth, int newHeight) {
  if (bitmap == null) {
      return null;
  }
     
  Bitmap scaledBitmap = Bitmap.createBitmap(newWidth, newHeight, Config.ARGB_8888);

  float scaleX = newWidth / (float) bitmap.getWidth();
  float scaleY = newHeight / (float) bitmap.getHeight();
  float pivotX = 0;
  float pivotY = 0;

  Matrix scaleMatrix = new Matrix();
  scaleMatrix.setScale(scaleX, scaleY, pivotX, pivotY);

  Canvas canvas = new Canvas(scaledBitmap);
  canvas.setMatrix(scaleMatrix);
  canvas.drawBitmap(bitmap, 0, 0, new Paint(Paint.FILTER_BITMAP_FLAG));

  return scaledBitmap;
}
    
    /**
     * Returns the path of a gallery intent uri
     *
     * @param uri - uri from intent
     * @return path - images actual path
     */
    public static String getPathFromUri(Context ctx, Uri uri) {
        String path = null;
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = ctx.getContentResolver().query(uri, proj, null, null, null);
        if(cursor.moveToFirst()){;
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            path = cursor.getString(column_index);
        }
        cursor.close();
        return path;
    }
    
    // From apache common lib
    public static String replaceEach(
            final String text, final String[] searchList, final String[] replacementList, final boolean repeat, final int timeToLive) {

        // mchyzer Performance note: This creates very few new objects (one major goal)
        // let me know if there are performance requests, we can create a harness to measure

        if (text == null || text.isEmpty() || searchList == null ||
                searchList.length == 0 || replacementList == null || replacementList.length == 0) {
            return text;
        }

        // if recursing, this shouldn't be less than 0
        if (timeToLive < 0) {
            throw new IllegalStateException("Aborting to protect against StackOverflowError - " +
                                            "output of one loop is the input of another");
        }

        final int searchLength = searchList.length;
        final int replacementLength = replacementList.length;

        // make sure lengths are ok, these need to be equal
        if (searchLength != replacementLength) {
            throw new IllegalArgumentException("Search and Replace array lengths don't match: "
                + searchLength
                + " vs "
                + replacementLength);
        }

        // keep track of which still have matches
        final boolean[] noMoreMatchesForReplIndex = new boolean[searchLength];

        // index on index that the match was found
        int textIndex = -1;
        int replaceIndex = -1;
        int tempIndex = -1;

        // index of replace array that will replace the search string found
        // NOTE: logic duplicated below START
        for (int i = 0; i < searchLength; i++) {
            if (noMoreMatchesForReplIndex[i] || searchList[i] == null ||
                    searchList[i].isEmpty() || replacementList[i] == null) {
                continue;
            }
            tempIndex = text.indexOf(searchList[i]);

            // see if we need to keep searching for this
            if (tempIndex == -1) {
                noMoreMatchesForReplIndex[i] = true;
            } else {
                if (textIndex == -1 || tempIndex < textIndex) {
                    textIndex = tempIndex;
                    replaceIndex = i;
                }
            }
        }
        // NOTE: logic mostly below END

        // no search strings found, we are done
        if (textIndex == -1) {
            return text;
        }

        int start = 0;

        // get a good guess on the size of the result buffer so it doesn't have to double if it goes over a bit
        int increase = 0;

        // count the replacement text elements that are larger than their corresponding text being replaced
        for (int i = 0; i < searchList.length; i++) {
            if (searchList[i] == null || replacementList[i] == null) {
                continue;
            }
            final int greater = replacementList[i].length() - searchList[i].length();
            if (greater > 0) {
                increase += 3 * greater; // assume 3 matches
            }
        }
        // have upper-bound at 20% increase, then let Java take over
        increase = Math.min(increase, text.length() / 5);

        final StringBuilder buf = new StringBuilder(text.length() + increase);

        while (textIndex != -1) {

            for (int i = start; i < textIndex; i++) {
                buf.append(text.charAt(i));
            }
            buf.append(replacementList[replaceIndex]);

            start = textIndex + searchList[replaceIndex].length();

            textIndex = -1;
            replaceIndex = -1;
            tempIndex = -1;
            // find the next earliest match
            // NOTE: logic mostly duplicated above START
            for (int i = 0; i < searchLength; i++) {
                if (noMoreMatchesForReplIndex[i] || searchList[i] == null ||
                        searchList[i].isEmpty() || replacementList[i] == null) {
                    continue;
                }
                tempIndex = text.indexOf(searchList[i], start);

                // see if we need to keep searching for this
                if (tempIndex == -1) {
                    noMoreMatchesForReplIndex[i] = true;
                } else {
                    if (textIndex == -1 || tempIndex < textIndex) {
                        textIndex = tempIndex;
                        replaceIndex = i;
                    }
                }
            }
            // NOTE: logic duplicated above END

        }
        final int textLength = text.length();
        for (int i = start; i < textLength; i++) {
            buf.append(text.charAt(i));
        }
        final String result = buf.toString();
        if (!repeat) {
            return result;
        }

        return replaceEach(result, searchList, replacementList, repeat, timeToLive - 1);
    }
}
