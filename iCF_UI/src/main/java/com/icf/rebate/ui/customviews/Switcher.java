package com.icf.rebate.ui.customviews;

import java.util.Locale;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.icf.ameren.rebate.ui.R;


public class Switcher extends ImageView implements View.OnTouchListener, View.OnClickListener
{
    private Drawable knobActive;
    private Drawable knobInactive;
    private int slideOn;
    private int slideOff;
    private boolean inactive = true;

    @SuppressWarnings("unused")
    private static final String TAG = "Switcher";

    /** A callback to be called when the user wants to switch activity. */
    public interface OnSwitchListener
    {
	// Returns true if the listener agrees that the switch can be changed.
	public boolean onSwitchChanged(Switcher source, boolean onOff);
    }

    private static final int ANIMATION_SPEED = 400;
    private static final long NO_ANIMATION = -1;
    private OnCheckedChangeListener listener = null;
    private boolean mSwitch = false;
    private int mPosition = 0;
    private long mAnimationStartTime = NO_ANIMATION;
    private int mAnimationStartPosition;
    private OnSwitchListener mListener;
    private boolean enabled = true;
    private boolean pressed = false;
    private String onText = "ON";
    private String offText = "OFF";
    private Paint paint;
    
    public Switcher(Context context, AttributeSet attrs)
    {
	super(context, attrs);
	paint = new Paint();
	knobActive = context.getResources().getDrawable(R.drawable.btn_bg_transparent);
	knobInactive = context.getResources().getDrawable(R.drawable.btn_bg_transparent);
	slideOn = R.color.header_bg;
	slideOff = R.color.header_bg;
	//onText = context.getResources().getString(R.string.on);
	//offText = context.getResources().getString(R.string.off);
    }

    public void setSwitch(boolean onOff)
    {
	mSwitch = onOff;
	invalidate();
    }

    // Try to change the switch position. (The client can veto it.)
    private void tryToSetSwitch(boolean onOff)
    {
	try
	{

	    if (listener != null && mSwitch != onOff)
	    {
		listener.onCheckedChanged(this, onOff);
		mSwitch = onOff;
	    }
	    startParkingAnimation();

	}
	finally
	{
	}
    }

    public void setOnSwitchListener(OnSwitchListener listener)
    {
	mListener = listener;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
	if (!enabled)
	    return false;

	final int available = getWidth() - getPaddingRight() - getPaddingLeft() - getDrawable().getIntrinsicWidth();

	switch (event.getAction())
	{
	case MotionEvent.ACTION_DOWN:
	    mAnimationStartTime = NO_ANIMATION;
	    setPressed(true);
	    pressed = true;
	    invalidate();
	    break;

	case MotionEvent.ACTION_MOVE:
	    trackTouchEvent(event);
	    break;

	case MotionEvent.ACTION_UP:
	    // Log.d("TAG", "" + mPosition + ":" + available);
	    tryToSetSwitch(mPosition >= (available * 0.5));
	    setPressed(false);
	    pressed = false;
	    break;

	case MotionEvent.ACTION_CANCEL:
	    // Log.d(TAG, "Motion event is being cancelled");
	    tryToSetSwitch(mPosition >= (available * 0.5));
	    // tryToSetSwitch(mSwitch);
	    setPressed(false);
	    pressed = false;
	    break;
	}
	return true;
    }

    private void startParkingAnimation()
    {
	mAnimationStartTime = AnimationUtils.currentAnimationTimeMillis();
	mAnimationStartPosition = mPosition;
    }

    private void trackTouchEvent(MotionEvent event)
    {
	Drawable drawable = getDrawable();
	int drawableWidth = drawable.getIntrinsicWidth();
	final int width = getWidth();
	final int available = width - getPaddingRight() - getPaddingLeft() - drawableWidth;
	int x = (int) event.getX();
	mPosition = x - getPaddingRight() - drawableWidth / 2;
	if (mPosition < available * 0.5)
	    mPosition = 0;
	if (mPosition >= available * 0.5)
	    mPosition = available;
	invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
	// Log.d(TAG, "-------onDraw being called-------");
	Drawable drawable = getDrawable();
	int drawableHeight = drawable.getIntrinsicHeight();
	int drawableWidth = drawable.getIntrinsicWidth();

	if (drawableWidth == 0 || drawableHeight == 0)
	{
	    return; // nothing to draw (empty bounds)
	}

	final int available = getWidth() - getPaddingRight() - getPaddingLeft() - drawableWidth;
	if (mAnimationStartTime != NO_ANIMATION)
	{
	    long time = AnimationUtils.currentAnimationTimeMillis();
	    int deltaTime = (int) (time - mAnimationStartTime);
	    mPosition = mAnimationStartPosition + ANIMATION_SPEED * (!mSwitch ? -deltaTime : deltaTime) / 1000;
	    // Log.d(TAG, "mPosition now is:" + mPosition);
	    int treshold = (int) (available * 0.5);
	    if (mPosition < treshold)
	    {
		mPosition = 0;
	    }
	    else
	    {
		mPosition = available;
	    }
	    boolean done = (mPosition == (!mSwitch ? 0 : available));
	    // Log.d(TAG, "Now::" + mPosition + done);
	    if (!done)
	    {
		invalidate();
	    }
	    else
	    {
		mAnimationStartTime = NO_ANIMATION;
	    }
	}
	else if (!pressed)
	{
	    mPosition = !mSwitch ? 0 : available;
	    // Log.d(TAG, "mPosition is:" + mPosition + mSwitch);
	}
	int offsetTop = (getHeight() - drawableHeight - getPaddingTop() - getPaddingBottom()) / 2;
	int offsetLeft = getPaddingLeft() + mPosition;
	// offsetLeft=0;
	// offsetTop = 0;
	int saveCount = canvas.getSaveCount();
	canvas.save();

	Resources res = getContext().getResources();
	// if (inactive) {
	// paint.setColor(res.getColor(R.color.gray));
	// } else {
	// paint.setColor(res.getColor(R.color.oscar_white));
	// }

	paint.setColor(res.getColor(android.R.color.white));
	paint.setTextSize(res.getDimension(R.dimen.form_screen_switch_text_size));
	paint.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
	if (mPosition >= available * 0.5)
	{
	    this.setBackgroundResource(slideOn);
	    canvas.drawText("", res.getDimension(R.dimen.form_screen_switch_text_left_margin), (int) (drawableHeight * 0.65), paint);
	}
	else
	{
	    this.setBackgroundResource(slideOff);
	    canvas.drawText("", available - res.getDimension(R.dimen.form_screen_switch_text_left_margin), (int) (drawableHeight * 0.65), paint);
	}

	canvas.translate(offsetLeft, offsetTop);
	drawable.draw(canvas);
	canvas.restoreToCount(saveCount);
    }

    // Consume the touch events for the specified view.
    public void addTouchView(View v)
    {
	v.setOnTouchListener(this);
    }

    public void disableComponent()
    {
	inactive = true;
	setImageDrawable(knobInactive);
	invalidate();

    }

    public void enableComponent()
    {

	inactive = false;
	setImageDrawable(knobActive);
	invalidate();
    }

    // This implements View.OnTouchListener so we intercept the touch events
    // and pass them to ourselves.
    public boolean onTouch(View v, MotionEvent event)
    {
	onTouchEvent(event);
	return true;
    }

    public void setEnabled(boolean enable)
    {
	enabled = enable;
    }

    public void setOnCheckedChangeListener(OnCheckedChangeListener listener1)
    {
	listener = listener1;
    }

    public interface OnCheckedChangeListener
    {
	public void onCheckedChanged(Switcher switcher, boolean isChecked);
    }

    public void clear()
    {

	knobActive.setCallback(null);
	knobActive = null;

	knobInactive.setCallback(null);
	knobInactive = null;
    }

    @Override
    public void onClick(View v)
    {
	// setSwitch(!this.isChecked());

    }

    public void setOnText(String onText)
    {
	if (onText != null)
	{
	    this.onText = onText.toUpperCase(Locale.getDefault());
	}
    }

    public void setOffText(String offText)
    {
	if (offText != null)
	{
	    this.offText = offText.toUpperCase(Locale.getDefault());
	}

    }

    public void setDrawable(Drawable knob, int slideon, int slideoff)
    {
	knobActive = knob;
	knobInactive = knob;
	slideOn = slideon;
	slideOff = slideoff;
    }

  

}
