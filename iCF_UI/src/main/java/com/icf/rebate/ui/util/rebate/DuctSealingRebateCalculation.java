package com.icf.rebate.ui.util.rebate;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.List;

import com.icf.rebate.networklayer.model.CustomerInfo;
import com.icf.rebate.networklayer.model.FormResponseBean;
import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.util.ICFLogger;
import com.icf.rebate.ui.util.UiUtil;

public class DuctSealingRebateCalculation
{

    private static final String TAG = DuctSealingRebateCalculation.class.getName();

    private float incentives[] = null;

    private boolean level1Qualified;

    private boolean level2Qualified;

    private String[] measureUnit;

    private float leakageImprove;

    private float totalImprove;

    public DuctSealingRebateCalculation()
    {
    }

    private void fetchRebateData()
    {
	SQLiteDatabase database = null;
	try
	{
	    ICFSQLiteOpenHelper helper = new ICFSQLiteOpenHelper(LibUtils.getApplicationContext());
	    database = helper.getReadableDatabase();

	    String selectionString = DuctSealingRebateTable.COLUMN_UTILITY_COMPANY + "= ? AND (" + DuctSealingRebateTable.COLUMN_CUSTOMER_TYPE + " = ? OR " + DuctSealingRebateTable.COLUMN_CUSTOMER_TYPE + "=  ?) AND (" + DuctSealingRebateTable.COLUMN_HOUSE_TYPE + " = ? OR " + DuctSealingRebateTable.COLUMN_HOUSE_TYPE + " = ?)";

	    String[] selectionArgs = null;

	    FormResponseBean bean = RebateManager.getInstance().getFormBean();

	    String customerType = null;
	    if (bean.getAppDetails() != null)
	    {
		customerType = bean.getAppDetails().getCustomerType();
	    }

	    if (customerType == null || customerType.length() == 0)
	    {
		customerType = "NA";
	    }

	    String houseType = bean.getCustomerInfo().getHouseType() == CustomerInfo.SINGLE_FAMILY_TYPE ? "Single Family" : "Multifamily";

	    selectionArgs = new String[] { String.valueOf(LibUtils.getSelectedUtilityCompany().getId()), customerType, "NA", houseType, "NA" };

	 // Need to figure out which column the date range is referring to
	    Cursor dbCursor = database.query(DuctSealingRebateTable.TABLE_NAME, null, null, null, null, null, null);
	    String[] columnNames = dbCursor.getColumnNames();
	    
	    // Gets list of all columns that apply to todays date
	    List<String> matchingDateRangeCols = LibUtils.getYearColumnName(columnNames, null);
	    
	    for (String col : matchingDateRangeCols) {
	    
	    String[] columns = new String[] { DuctSealingRebateTable.COLUMN_LEVEL, DuctSealingRebateTable.COLUMN_MEASURE_UNIT, col };

	    Cursor cursor = database.query(DuctSealingRebateTable.TABLE_NAME, columns, selectionString, selectionArgs, null, null, null);
	    String data = LibUtils.getSqlQueryString(DuctSealingRebateTable.TABLE_NAME, columns, selectionString, selectionArgs, null, null, null);
	    ICFLogger.d(TAG, data);
	    incentives = new float[2];
	    measureUnit = new String[2];

	    while (cursor.moveToNext())
	    {
		int level = cursor.getInt(0);
		if (level == 1)
		{
		    measureUnit[0] = cursor.getString(1);
		    // TODO:
		   // incentives[0] = cursor.getFloat(2);
		    if (!cursor.isNull(2)) {
			incentives[0] = incentives[0] < cursor.getFloat(2) ? cursor.getFloat(1) : incentives[0];
		    }
		}
		else
		{
		    measureUnit[1] = cursor.getString(1);
		    // incentives[1] = cursor.getFloat(2);
		    if(!cursor.isNull(2))
		    incentives[1] = incentives[1] < cursor.getFloat(2) ? cursor.getFloat(1) : incentives[1];
		}
	    }
	}
	}
	catch (Exception e)
	{
	    ICFLogger.e(TAG, e);
	}
	finally
	{
	    if (database != null)
	    {
		database.close();
	    }
	}
    }

	public float getRebateValue(String testInCFMStr, String testOutCFMStr, String postTestMeasuredAirflowStr, float tonnage) {
		try {
			if (measureUnit == null || incentives == null) {
				fetchRebateData();
			}
			int testInCFM = LibUtils.parseInteger(testInCFMStr, 0);
			int testOutCFM = LibUtils.parseInteger(testOutCFMStr, 0);
			int postTestMeasuredAirflow = LibUtils.parseInteger(postTestMeasuredAirflowStr, 0);
			float leakageImprovement = 0.0f;
			if (testInCFM > 0) {
				leakageImprovement = (((float) (testInCFM - testOutCFM)) / testInCFM) * 100;
			}
			setLeakageImprovement(leakageImprovement);
			float totalLeakage = 0.0f;
			if (postTestMeasuredAirflow > 0) {
				totalLeakage = ((float) testOutCFM / (float) postTestMeasuredAirflow) * 100;
			}
			setTotalImprovement(totalLeakage);

			int index = 0;
			if (totalLeakage < 12) {
				level2Qualified = true;
				level1Qualified = false;
				index = 1;
			} else if ((totalLeakage >= 12 && totalLeakage <= 20) || leakageImprovement >= 50) {
				level1Qualified = true;
				level2Qualified = false;
			} else {
				level1Qualified = false;
				level2Qualified = false;
			}

			if (measureUnit != null && measureUnit[index].equalsIgnoreCase(ICFSQLiteOpenHelper.KEY_SYSTEM)) {
				tonnage = 1;
			}
			if (level1Qualified || level2Qualified) {
				float ret = incentives[index] * tonnage;
				ret = UiUtil.convertToPostTwoDigtDecimal(ret);
				return ret;
			}
		} catch (Exception e) {
			ICFLogger.e(TAG, "Error in getRebateValue : testInCFMStr: " + testInCFMStr + " testOutCFMStr: " + testOutCFMStr, e);
		}
		return 0f;
	}

	private void setLeakageImprovement(float leakageImprovement) {
		this.leakageImprove = leakageImprovement;
	}

	public float getLeakageImprovement() {
		return leakageImprove;
	}

	private void setTotalImprovement(float totalImprovement) {
		this.totalImprove = totalImprovement;
	}

	public float getTotalImprovement() {
		return totalImprove;
	}

	public boolean isLevel1Qualified() {
		return level1Qualified;
	}

	public boolean isLevel2Qualified() {
		return level2Qualified;
	}
}
