package com.icf.rebate.ui.customviews;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;

//TODO: why are we subclassing? why cant we directly use ProgressDialog?
public class ICFProgressDialog extends ProgressDialog
{
    private String message;

    private int id;

    public ICFProgressDialog(Activity activity, int theme)
    {
	super(activity, theme);
    }

    public ICFProgressDialog(Activity activity)
    {
	super(activity);
    }

    public void setId(int id)
    {
	this.id = id;
    }

    public int getId()
    {
	return this.id;
    }

    @Override
    public void setMessage(CharSequence message)
    {
	this.message = message.toString();
	super.setMessage(this.message);

    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
	super.onCreate(savedInstanceState);
    }

    public void dismiss()
    {
	super.dismiss();
    }

}
