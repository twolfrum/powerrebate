package com.icf.rebate.ui.fragment;

import java.util.List;
import java.util.Map;

import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.app.model.AirSealingItem;
import com.icf.rebate.networklayer.model.CustomerInfo;
import com.icf.rebate.networklayer.model.FormResponseBean.Options;
import com.icf.rebate.networklayer.model.FormResponseBean.Part;
import com.icf.rebate.ui.controller.FocusManager;
import com.icf.rebate.ui.controller.FormUIManager;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.controller.FormUIManager.FormViewCreationCallback;
import com.icf.rebate.ui.listeners.RequestCallFlow;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.ICFLogger;
import com.icf.rebate.ui.util.UiUtil;
import com.icf.rebate.ui.util.UiViewUtils;
import com.icf.rebate.ui.util.rebate.ProjectCostRebateCalculation;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

public class AirSealingFragment extends Fragment implements FormViewCreationCallback, RequestCallFlow
{
    public static final String TAG = AirSealingFragment.class.getSimpleName();
    private FormUIManager uiManager;
    private LinearLayout scrollViewLayout;
    private AirSealingItem airSealingItem;
    private EditText preMeasuredAirFlow, postMeasuredAirFlow, floorArea,
            result, projectCost, quantity;
    private String projectCostValue, quantityValue;
    private Spinner windshielding;
    private Spinner numStories;
    private boolean isDeleted = false;
    private String title;
    private ProjectCostRebateCalculation costBasedRebateCalculation;
    private String rebateType;
    private String rebateCalcType;
    
    public AirSealingFragment()
    {
	super();
    }

    public static AirSealingFragment newInstance(AirSealingItem airSealingItem,
                                         String rebateType, String rebateCalcType) {
        AirSealingFragment frag = new AirSealingFragment();
        frag.airSealingItem = airSealingItem;
        frag.rebateType = rebateType;
        frag.rebateCalcType = rebateCalcType;

        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.duct_sealing_lyt, null, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getUiControls(view);
        initalize();
        createViews();

        CustomerInfo cInfo = RebateManager.getInstance().getFormBean().getCustomerInfo();
        if (UiUtil.isJobCompleted(cInfo)) {
            UiUtil.setEnableView((ViewGroup) view, false, null);
        }
        FocusManager.newFocusManager(view);
    }

    private void getUiControls(View view) {
        scrollViewLayout = (LinearLayout) view.findViewById(R.id.duct_scrol_layout);

    }

    protected void initalize() {
        uiManager = new FormUIManager(getString(R.string.airsealingId));
        uiManager.setFormViewCreationCallback(this);
        uiManager.setEquipmentName(getActivity().getResources().getString(R.string.ductsealingId));

        if (!TextUtils.isEmpty(rebateCalcType) &&
                rebateCalcType.equals(ProjectCostRebateCalculation.CALC_TYPE))
            costBasedRebateCalculation = new ProjectCostRebateCalculation();
    }

    private void createViews() {
        List<Part> partList = null;
        if (airSealingItem != null && airSealingItem.getSealingDetail().getParts() != null) {
            partList = airSealingItem.getSealingDetail().getParts();
        }

        if (partList != null) {
            boolean isFormFilled = airSealingItem.getFormFilledState() != AppConstants.FORM_ITEM_FILLED_STATE.ITEM_UNEDITED.getState();
            scrollViewLayout.removeAllViews();
            uiManager.createViews(getParentFragment(), scrollViewLayout, partList, isFormFilled);
        }

    }

    @Override
    public void showProgressDialog()
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void stopProgressDialog()
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void updateData(Object obj)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void displayError(String title, String message)
    {
	// TODO Auto-generated method stub
    }

    public AirSealingItem getAirSealingItem()
    {
	return airSealingItem;
    }

    @Override
    public void viewCreated(final View view, Part part, final Options option) {
        if (windshielding == null || numStories == null || floorArea == null ||
                preMeasuredAirFlow == null || postMeasuredAirFlow == null ||
                result == null || projectCost == null || quantity == null) {
            if (part != null) {
                String name = part.getId();
                if (name != null && name.equalsIgnoreCase(AppConstants.WINDSHIELD_ID)) {
                    if (view instanceof Spinner) {
                        windshielding = (Spinner) view;
                        windshielding.setOnItemSelectedListener(new OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view,
                                                       int position, long id) {
                                String selectedValue = (String) ((TextView) view).getText();
                                if (selectedValue != null && option != null &&
                                        !selectedValue.equalsIgnoreCase(option.getSavedValue())) {
                                    if (!selectedValue.equalsIgnoreCase(AppConstants.COMBO_SELECT_VALUE)) {
                                        option.setSavedValue(selectedValue);
                                    } else {
                                        option.setSavedValue(null);
                                    }
                                }

                                if (UiViewUtils.isValidationMode(option.isItemSaved()) &&
                                        option.isMandatory() && !option.isFieldFilled()) {
                                    ((View) view.getParent()).setBackground(getActivity().getResources()
                                            .getDrawable(R.drawable.bg_dialog_mandatory_box));
                                } else {
                                    ((View) view.getParent()).setBackground(getActivity().getResources()
                                            .getDrawable(R.drawable.combo_box_bg));
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {}
                        });
                    }
                } else if (name != null && name.equalsIgnoreCase(AppConstants.STORIES_ID)) {
                    if (view instanceof Spinner) {
                        numStories = (Spinner) view;
                        numStories.setOnItemSelectedListener(new OnItemSelectedListener() {

                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                String selectedValue = (String) ((TextView) view).getText();
                                if (selectedValue != null && option != null && !selectedValue.equalsIgnoreCase(option.getSavedValue())) {
                                    if (!selectedValue.equalsIgnoreCase(AppConstants.COMBO_SELECT_VALUE)) {
                                        option.setSavedValue(selectedValue);
                                    } else {
                                        option.setSavedValue(null);
                                    }
                                }

                                if (UiViewUtils.isValidationMode(option.isItemSaved()) && option.isMandatory() && !option.isFieldFilled()) {
                                    ((View) view.getParent()).setBackground(getActivity().getResources().getDrawable(R.drawable.bg_dialog_mandatory_box));
                                } else {
                                    ((View) view.getParent()).setBackground(getActivity().getResources().getDrawable(R.drawable.combo_box_bg));
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {}
                        });
                    }
                } else if (name != null && name.equalsIgnoreCase(AppConstants.TEST_IN_ID)) {
                    if (view instanceof EditText) {
                        preMeasuredAirFlow = (EditText) view;
                        preMeasuredAirFlow.setInputType(InputType.TYPE_CLASS_NUMBER);
                        preMeasuredAirFlow.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {}
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                            @Override
                            public void afterTextChanged(Editable s) {
                                option.setSavedValue(s.toString());
                            }
                        });
                    }

                } else if (name != null && name.equalsIgnoreCase(AppConstants.TEST_OUT_ID)) {
                    if (view instanceof EditText) {
                        postMeasuredAirFlow = (EditText) view;
                        postMeasuredAirFlow.setInputType(InputType.TYPE_CLASS_NUMBER);
                        postMeasuredAirFlow.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {}
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                            @Override
                            public void afterTextChanged(Editable s) {
                                option.setSavedValue(s.toString());
                            }
                        });
                    }

                } else if (name != null && name.equalsIgnoreCase(AppConstants.PROJECT_COST)) {
                    if (view instanceof EditText) {
                        projectCost = (EditText) view;
                        projectCost.setInputType(InputType.TYPE_CLASS_NUMBER);
                        projectCost.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {}
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                            @Override
                            public void afterTextChanged(Editable s) {
                                projectCostValue = ((EditText) view).getText().toString().trim();
                                float value = calculateRebateValue();
                                if (value > -1f) updateAirSealingDetails(value);
                            }
                        });
                    }

                } else if (name != null && name.equalsIgnoreCase(AppConstants.QUANTITY)) {
                    if (view instanceof EditText) {
                        quantity = (EditText) view;
                        quantity.setInputType(InputType.TYPE_CLASS_NUMBER);
                        quantity.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {}
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                            @Override
                            public void afterTextChanged(Editable s) {
                                quantityValue = ((EditText) view).getText().toString().trim();
                                float value = calculateRebateValue();
                                if (value > -1f) updateAirSealingDetails(value);
                            }
                        });
                    }
                } else if (name != null && name.equalsIgnoreCase(AppConstants.IMPROVEMENT_DATE)) {

                } else if (name != null && name.equalsIgnoreCase(AppConstants.DUCT_SEALING_PASS_FAIL)) {
                    if (view instanceof EditText) {
                        result = (EditText) view;
                        try {
                            airSealingItem.setStaticRebateValue(
                                    ((Map<String, String>) result.getTag()).get("staticDisplayedValue"));
                        } catch (Exception e) {
                        }
                    }
                }
            }
        }
    }
    
    public void updateAirSealingDetails(float value) {
        airSealingItem.setRebateValue(value);
        if (result != null) {
            String formatedValue = airSealingItem.getStaticRebateValue();
            if (TextUtils.isEmpty(formatedValue)) {
                formatedValue = (String.format("%.02f", value));
            }
            result.setText("" + formatedValue);
            result.setBackground(getActivity().getResources().getDrawable(R.drawable.bg_dialog_box));
            ICFLogger.d(TAG, "Result:" + "$" + formatedValue);
        }
    }

    public void updateAirSealingDetails() {
        float rebateValue = calculateRebateValue();
        if (rebateValue < 0) rebateValue = 0;
        updateAirSealingDetails(rebateValue);
    }

    private float calculateRebateValue() {
        float rebateValue = -1f;

        if (!TextUtils.isEmpty(rebateCalcType) &&
                rebateCalcType.equals(ProjectCostRebateCalculation.CALC_TYPE)) {
            if (!TextUtils.isEmpty(projectCostValue)) {
                float cost;
                int qty = 0;
                try {
                    cost = Float.parseFloat(projectCostValue);
                    if (!TextUtils.isEmpty(quantityValue)) qty = Integer.parseInt(quantityValue);
                    rebateValue = costBasedRebateCalculation.getRebateValue(rebateType, cost, qty);
                } catch (NumberFormatException e) {
                    return rebateValue;
                }
            } else {
                //if there is no project cost then by definition the rebate is 0
                rebateValue = 0f;
            }
        }

        return rebateValue;
    }

    @Override
    public void onDestroyView() {
        UiUtil.cleanBitmap(getView());
        saveForm();
        resetFormViews();
        super.onDestroyView();
    }

    public void saveForm() {
        List<AirSealingItem> items = ((AirSealingPagerFragment) getParentFragment()).getAirList();

        if (items != null) {
            int index = items.indexOf(airSealingItem);
            if (index > -1) {
                Log.v(TAG, "onDestroyView " + index);
                if (!isDeleted) {
                    if (airSealingItem.isItemSaved() && result != null) {
                        String formatedValue = (String.format("%.02f", airSealingItem.getRebateValue()));
                        result.setText("$" + formatedValue);
                    }
                    ((AirSealingPagerFragment) getParentFragment()).saveForm(index, false);
                }
            }
        }
    }

    public void setFragmentDeleted() {
        isDeleted = true;
    }

    private void resetFormViews() {
        windshielding = null;
        numStories = null;
        floorArea = null;
        preMeasuredAirFlow = null;
        postMeasuredAirFlow = null;
        result = null;
        projectCost = null;
        quantity = null;
    }

    public void setTitleId(String title) {
        this.title = title;
    }

    public String getTitleId() {
        return this.title;
    }

}
