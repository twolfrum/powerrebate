package com.icf.rebate.ui.customviews;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;

import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.ui.util.UiUtil;

@SuppressLint("DrawAllocation")
public class CustomPageIndicator extends View
{
    private CustomViewPager pager;
    private Paint dotPaint;
    private int cellSize;
    private Bitmap red_img = BitmapFactory.decodeResource(getResources(), R.drawable.dot_active);
    private Bitmap white_img = BitmapFactory.decodeResource(getResources(), R.drawable.dots);

    public CustomPageIndicator(Context context, AttributeSet attrs)
    {
	super(context, attrs);

	initPaints();
    }

    public CustomPageIndicator(Context context)
    {
	super(context);
	initPaints();
    }

    private final void initPaints()
    {
	red_img = drawableToBitmap(getResources().getDrawable(R.drawable.page_indicator_highlighted));
	white_img = drawableToBitmap(getResources().getDrawable(R.drawable.page_indicator_normal));
	
	white_img = Bitmap.createScaledBitmap(white_img, 16, 16, true);
	red_img = Bitmap.createScaledBitmap(red_img, 16, 16, true);
	dotPaint = new Paint();
	dotPaint.setColor(Color.TRANSPARENT);
	cellSize = white_img.getHeight() + 7;
    }

    public void setPager(CustomViewPager pager)
    {
	this.pager = pager;
    }

    public void update()
    {
	this.postDelayed(new Runnable()
	{

	    @Override
	    public void run()
	    {
		invalidate();
	    }
	}, 100);
    }

    public void updatePageCount()
    {
	this.postDelayed(new Runnable()
	{
	    @Override
	    public void run()
	    {
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT)
		{
		    requestLayout();
		}
		else
		{
		    invalidate();
		}
	    }
	}, 100);
    }

    private int getNumPages()
    {
	return pager == null ? 1 : pager.getPageCount();
    }

    private int getActivePage()
    {
	return pager == null ? 0 : pager.getCurrentPage();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
	setMeasuredDimension(measureWidth(widthMeasureSpec), measureHeight(heightMeasureSpec));
    }

    private int measureWidth(int measureSpec)
    {

	int result = 0;
	int specMode = MeasureSpec.getMode(measureSpec);
	int specSize = MeasureSpec.getSize(measureSpec);

	if (specMode == MeasureSpec.EXACTLY)
	{
	    result = specSize;
	}
	else
	{
	    result = getNumPages() * cellSize;
	    if (specMode == MeasureSpec.AT_MOST)
	    {
		result = Math.min(result, specSize);
	    }
	}
	return result;
    }

    private int measureHeight(int measureSpec)
    {
	int result = 0;
	int specMode = MeasureSpec.getMode(measureSpec);
	int specSize = MeasureSpec.getSize(measureSpec);

	if (specMode == MeasureSpec.EXACTLY)
	{
	    result = specSize;
	}
	else
	{
	    result = cellSize;
	    if (specMode == MeasureSpec.AT_MOST)
	    {
		result = Math.min(result, specSize);
	    }
	}
	return result;
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
	super.onDraw(canvas);

	int count = getNumPages();
	int current = getActivePage();
	if (count <= 1)
	    return;
	int x = 0;
	for (int i = 0; i < count; i++, x += cellSize)
	{
	    int dotSize = white_img.getWidth();
	    int dotOffset = 1;
	    RectF oval = new RectF(x + dotOffset, dotOffset + 1, x + dotOffset + dotSize, white_img.getHeight() + 3);
	    canvas.drawRect(oval, dotPaint);
	    if (i == current)
	    {
		int centreX = (int) (x + dotOffset + (oval.width() - red_img.getWidth()) / 2);
		int centreY = (int) (dotOffset + 1 + (oval.height() - red_img.getHeight()) / 2);
		canvas.drawBitmap(red_img, centreX, centreY, null);
	    }
	    else
	    {
		canvas.drawRect(oval, dotPaint);
		int centreX = (int) (x + dotOffset + (oval.width() - white_img.getWidth()) / 2);
		int centreY = (int) (dotOffset + 1 + (oval.height() - white_img.getHeight()) / 2);
		canvas.drawBitmap(white_img, centreX, centreY, null);
	    }
	}
    }
    
    public static Bitmap drawableToBitmap(Drawable drawable) {
	    if (drawable instanceof BitmapDrawable) {
	        return ((BitmapDrawable)drawable).getBitmap();
	    }

	    int width = drawable.getIntrinsicWidth();
	    width = width > 0 ? width : 1;
	    int height = drawable.getIntrinsicHeight();
	    height = height > 0 ? height : 1;

	    Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
	    Canvas canvas = new Canvas(bitmap); 
	    drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
	    drawable.draw(canvas);

	    return bitmap;
	}

}
