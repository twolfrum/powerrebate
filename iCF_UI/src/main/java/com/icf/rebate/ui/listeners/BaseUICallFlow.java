package com.icf.rebate.ui.listeners;


/**
 * To be implemented by all the UI modules.
 * 
 * @author varun.t
 * 
 */
public interface BaseUICallFlow
{
    void requestData();

}
