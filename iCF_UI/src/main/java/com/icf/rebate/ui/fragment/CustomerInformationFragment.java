package com.icf.rebate.ui.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.adapter.CustomSpinnerAdapter;
import com.icf.rebate.networklayer.DataParserImpl;
import com.icf.rebate.networklayer.model.AppDetails;
import com.icf.rebate.networklayer.model.CustomerInfo;
import com.icf.rebate.networklayer.model.CustomerInfo.RebateDifferentPerson;
import com.icf.rebate.networklayer.model.CustomerScreenItem;
import com.icf.rebate.networklayer.model.CustomerScreenLayout;
import com.icf.rebate.networklayer.model.FormResponseBean;
import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.rebate.ui.RootActivity;
import com.icf.rebate.ui.controller.DatePickWatcher;
import com.icf.rebate.ui.controller.MandatoryTextWatcher;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.controller.ValidationManager;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.FileUtil;
import com.icf.rebate.ui.util.ICFLogger;
import com.icf.rebate.ui.util.UiUtil;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class CustomerInformationFragment extends Fragment implements Callback,
        OnClickListener, OnItemSelectedListener, OnMapReadyCallback
{
    public static final String NAME = CustomerInformationFragment.class.getSimpleName();
    private RootActivity rootAct;
    private ImageView info;
    private Handler resHandler;
    private View mapRoot;
    private String profileImageUrl;
    private SharedPreferences pref;
	private TextView txtCustomerName, txtDateInstalled;

    private EditText edtCustomerAddress, edtCustomerAccNo, edtCustomerAccNo2,
            edtEmailAddress, edtCustomerFName, edtCustomerLName, edtCityName,
            edtStateName, edtZipCode, edtPhoneNumber, edtPermitNumber, edtAudit,
			edtSizeOfHouse, edtHouseBuiltYear, edtTechName, edtTechId,
			edtTechPhone, edtProjectIncentive;

    private CheckBox chkBasementFoundation, chkNewConstruction;

    private Dialog rebate_dialog;
    private GoogleMap googleMap;

    private CheckBox send_rebate_other, rebate_current_bill_checkbox,
            send_rebate_to_uitility_checkbox, single_family_checkbox,
            multiple_family_checkbox;

    private View accountDetailRoot;
    private ImageView locationPin;
    private Button custom_info_continue;
    private Location currentLocation;
    private CustomerInfo customerInfo;
    private AppDetails appDetails;
    private CustomerScreenLayout customerScreenLayout;
	private Runnable mAppDetailsInitializer;
    private boolean mAppDetailsInitialized;
    private String geoAddress;

    private Spinner customerType, mHeatingCoolingType, mBuildingType,
            mSpinWaterHeatingFuelType, mSpinHeatingAndCoolingFuelType,
            mSpinCentralAC, mSpinAuditor, mSpinYearBuilt, mSpinDistributor,
			mSpinProjectType;

    private ViewGroup customerTypeLyt, mHeatingCoolingLayout,
            mBuildingTypeLayout, mLytWaterHeatingFuelType,
            mLytHeatingAndCoolingFuelType, mLytCentralAC,
			mLytAuditor, send_rebate_lyt, mYearHouseBuiltEdtLyt,
            mYearBuiltSpinLyt, mDistributorLyt, mDateInstalledLyt,
			mTechInfoLyt, mProjectTypeLyt;

    ArrayList<String> customerTypeList = new ArrayList<>();
    private String jobId;

	// Values for spinner selections
    private String customType, mHeatingCoolingSelection, mBuildingTypeSelection,
            mWaterHeatingFuelTypeSelection, mHeatingAndCoolingFuelTypeSelection,
            mCentralACSelection, mYearBuiltSelection, mDistributorSelection,
            mProjectTypeSelection; //,mAuditorSelection;

    private boolean customerAddressEdited = false;
    private boolean canEditHouseOrCustomerType = true;

	private TextWatcher sizeOfHouseWatcher = new TextWatcher() {
		private DecimalFormat dfnd = new DecimalFormat("###,###");

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
		}

		@Override
		public void afterTextChanged(Editable s) {
			edtSizeOfHouse.removeTextChangedListener(this);
			try {
				int inilen, endlen;
				inilen = edtSizeOfHouse.getText().length();

				String v = s.toString().trim().replace(",", "");
				Number n = dfnd.parse(v);
				int cp = edtSizeOfHouse.getSelectionStart();
				edtSizeOfHouse.setText(dfnd.format(n));
				endlen = edtSizeOfHouse.getText().length();
				int sel = (cp + (endlen - inilen));
				if (sel > 0 && sel <= edtSizeOfHouse.getText().length()) {
					edtSizeOfHouse.setSelection(sel);
				} else {
					edtSizeOfHouse.setSelection(edtSizeOfHouse.getText().length() - 1);
				}
			} catch (NumberFormatException nfe) {
				// do nothing?
			} catch (ParseException e) {
				// do nothing?
			}

			edtSizeOfHouse.addTextChangedListener(this);
		}
	};

    private View rootView;

    public CustomerInformationFragment()
    {
	super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
	View view = inflater.inflate(R.layout.custom_info_screen, null);
	return view;
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim)
    {
	Animation animation = AnimationUtils.loadAnimation(LibUtils.getApplicationContext(), nextAnim);
	animation.setDuration(700);
	animation.setAnimationListener(new AnimationListener()
	{
	    @Override
	    public void onAnimationStart(Animation animation)
	    {

	    }

	    @Override
	    public void onAnimationRepeat(Animation animation)
	    {

	    }

	    @Override
	    public void onAnimationEnd(Animation animation)
	    {
		initializeAfterAnimationEnd(rootView);
	    }
	});
	return animation;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
	super.onViewCreated(view, savedInstanceState);
	rootView = view;
    }

    @Override
    public void onResume()
    {
	super.onResume();
	((RootActivity) getActivity()).checkForUpdate(true);
    }

	private void initializeAfterAnimationEnd(View view) {
		FormResponseBean bean = RebateManager.getInstance().getFormBean();
		if (bean != null) {
			ICFLogger.d("INIT_TRACE", "initializeAfterAnimationEnd(); bean != null");
            //pending job
			customerInfo = bean.getCustomerInfo();
			appDetails = bean.getAppDetails();
			customerScreenLayout = bean.getCustomerScreenLayout();
		} else {
			ICFLogger.d("INIT_TRACE", "initializeAfterAnimationEnd(); bean == null");
            //new job
            appDetails = RebateManager.getInstance().getAppDetail();
            customerScreenLayout = RebateManager.getInstance().getCustomerScreenLayout();
		}

		getUiControls(view); //instantiate declared UI widget variables;
        initialize();
		updateAppDetails();
		bindCallbacks();

		view.post(new Runnable() {
			@Override
			public void run() {
				populateCustomerInfo(customerInfo);
			}
		});

		ArrayList<View> excludeView = new ArrayList<View>();
		excludeView.add(custom_info_continue);
		if (customerInfo != null) {
			if (UiUtil.isJobCompleted(customerInfo)) {
				UiUtil.setEnableView((ViewGroup) view, false, excludeView);
			}
		}
	}

    private void populateMapLocation()
    {
	if (customerInfo == null)
	{
	    ((RootActivity) getActivity()).initializeGoogleService();
	    return;
	}
	if (currentLocation == null)
	{
	    ((RootActivity) getActivity()).initializeGoogleService();
	}
	if (googleMap != null && currentLocation != null)
	{
	    CameraUpdate point = CameraUpdateFactory.newLatLngZoom(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), 17.0f);
	    ICFLogger.d(NAME, "Longitude And Latitude: " + currentLocation.getLatitude() + "Latitude:" + currentLocation.getLongitude());
	    // moves camera to coordinates
	    googleMap.moveCamera(point);
	    // animates camera to coordinates
	    plotLocation();
	}
    }

    private void plotLocation()
    {
	CircleOptions op = new CircleOptions();
	op.radius(10.0);
	op.strokeWidth(2);
	op.strokeColor(getResources().getColor(R.color.white));
	op.fillColor(getResources().getColor(R.color.header_bg));
	op.center(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()));
	googleMap.clear();
	googleMap.addCircle(op);
    }

	public void getUiControls(View view) {
		// rebate_header = (TextView) view.findViewById(R.id.rebate_header);
		txtCustomerName = (TextView) view.findViewById(R.id.lblcustomerName);
		edtCustomerFName = (EditText) view.findViewById(R.id.rebate_edtFirstName);
		edtCustomerLName = (EditText) view.findViewById(R.id.rebate_edtLastName);
		edtCustomerAddress = (EditText) view.findViewById(R.id.rebate_customer_address);
		edtCustomerAccNo = (EditText) view.findViewById(R.id.rebate_customer_accno);
		edtCustomerAccNo2 = (EditText) view.findViewById(R.id.rebate_customer_accno2);
		edtEmailAddress = (EditText) view.findViewById(R.id.rebate_customer_email);
		edtPhoneNumber = (EditText) view.findViewById(R.id.rebate_customer_phone);
		edtCityName = (EditText) view.findViewById(R.id.rebate_customer_city);
		edtStateName = (EditText) view.findViewById(R.id.rebate_customer_state);
		edtZipCode = (EditText) view.findViewById(R.id.rebate_customer_zipcode);
		edtPermitNumber = (EditText) view.findViewById(R.id.edt_permit_number);
		edtAudit = (EditText) view.findViewById(R.id.edt_audit);
		edtTechName = (EditText) view.findViewById(R.id.edtTechnicianName);
		edtTechId = (EditText) view.findViewById(R.id.edtTechnicianId);
		edtTechPhone = (EditText) view.findViewById(R.id.edtTechnicianPhone);
		edtProjectIncentive = (EditText) view.findViewById(R.id.edtProjectIncentive);

		mapRoot = view.findViewById(R.id.map_view_root);
		info = (ImageView) view.findViewById(R.id.info_image);
		send_rebate_lyt = (ViewGroup) view.findViewById(R.id.send_rebate_lyt);
		send_rebate_other = (CheckBox) view.findViewById(R.id.send_rebate_checkbox);
		rebate_current_bill_checkbox = (CheckBox) view.findViewById(R.id.rebate_current_bill_checkbox);
		send_rebate_to_uitility_checkbox = (CheckBox) view.findViewById(R.id.send_rebate_to_uitility_checkbox);
		single_family_checkbox = (CheckBox) view.findViewById(R.id.single_family_checkbox);
		multiple_family_checkbox = (CheckBox) view.findViewById(R.id.multiple_family_checkbox);
		accountDetailRoot = view.findViewById(R.id.accountDetail);
		locationPin = (ImageView) view.findViewById(R.id.location_pin);
		custom_info_continue = (Button) view.findViewById(R.id.custom_info_continue);
		customerType = (Spinner) view.findViewById(R.id.customer_type);
		customerTypeLyt = (ViewGroup) view.findViewById(R.id.customer_type_drop_down_lyt);
		mHeatingCoolingType = (Spinner) view.findViewById(R.id.spin_heating_cooling_type);
		mBuildingType = (Spinner) view.findViewById(R.id.spin_building_type);
		mSpinWaterHeatingFuelType = (Spinner) view.findViewById(R.id.spin_water_heater_fuel_type);
		mHeatingCoolingLayout = (ViewGroup) view.findViewById(R.id.drop_down_lyt_heating_cooling_type);
		mBuildingTypeLayout = (ViewGroup) view.findViewById(R.id.lyt_building_type);
		mLytHeatingAndCoolingFuelType = (ViewGroup) view.findViewById(R.id.lyt_heating_cooling_fuel_type);
		mLytCentralAC = (ViewGroup) view.findViewById(R.id.lyt_ac);
		mLytAuditor = (ViewGroup) view.findViewById(R.id.lyt_audit);
		mSpinHeatingAndCoolingFuelType = (Spinner) view.findViewById(R.id.spin_heating_cooling_fuel_type);
		mSpinCentralAC = (Spinner) view.findViewById(R.id.spin_ac);
		mSpinProjectType = (Spinner) view.findViewById(R.id.spinProjectType);
		//mSpinAuditor = (Spinner) view.findViewById(R.id.spin_audit);
		mLytWaterHeatingFuelType = (ViewGroup) view.findViewById(R.id.lyt_water_heating_fuel_type);
		mYearHouseBuiltEdtLyt = (ViewGroup) view.findViewById(R.id.yearHouseBuiltEdtRoot);
		mYearBuiltSpinLyt = (ViewGroup) view.findViewById(R.id.yearBuiltRootSpin);
		mDistributorLyt = (ViewGroup) view.findViewById(R.id.distributor);
		mDateInstalledLyt = (ViewGroup) view.findViewById(R.id.dateInstalledLyt);
		mTechInfoLyt = (ViewGroup) view.findViewById(R.id.technicianInfoLyt);
		mProjectTypeLyt = (ViewGroup) view.findViewById(R.id.projectTypeLyt);
		txtDateInstalled = (TextView) view.findViewById(R.id.date_installed);
		edtSizeOfHouse = (EditText) view.findViewById(R.id.edtSizeofHouse);
		edtHouseBuiltYear = (EditText) view.findViewById(R.id.edtYearHousebuilt);
		mSpinYearBuilt = (Spinner) view.findViewById(R.id.spin_year_built);
		mSpinDistributor = (Spinner) view.findViewById(R.id.spin_distributor);
		chkBasementFoundation = (CheckBox) view.findViewById(R.id.chkBasementFoundation);
		chkNewConstruction = (CheckBox) view.findViewById(R.id.chk_new_construction);

		mapRoot.setVisibility(View.VISIBLE);
		((MapFragment) getActivity().getFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
		ValidationManager.getInstance().clearViewList("customerInfo");
		ValidationManager.getInstance().addValidationView(edtCustomerFName, "customerInfo", ValidationManager.emptyRule, R.string.validation_fname);
		ValidationManager.getInstance().addValidationView(edtCustomerFName, "customerInfo", ValidationManager.nameRule, R.string.validation_fname);
		ValidationManager.getInstance().addValidationView(edtCustomerLName, "customerInfo", ValidationManager.emptyRule, R.string.validation_lname);
		ValidationManager.getInstance().addValidationView(edtCustomerLName, "customerInfo", ValidationManager.nameRule, R.string.validation_lname);
		ValidationManager.getInstance().addValidationView(edtCustomerAddress, "customerInfo", ValidationManager.emptyRule, R.string.validation_address);
		ValidationManager.getInstance().addValidationView(edtEmailAddress, "customerInfo", ValidationManager.emailRule, R.string.validation_email, true);
		ValidationManager.getInstance().addValidationView(edtPhoneNumber, "customerInfo", ValidationManager.phoneRule, R.string.validation_phone);
		ValidationManager.getInstance().addValidationView(edtCityName, "customerInfo", ValidationManager.emptyRule, R.string.validation_city);
		ValidationManager.getInstance().addValidationView(edtStateName, "customerInfo", ValidationManager.emptyRule, R.string.validation_state);
		ValidationManager.getInstance().addValidationView(edtZipCode, "customerInfo", ValidationManager.emptyRule, R.string.validation_zip);

		edtCustomerFName.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				txtCustomerName.setText(edtCustomerFName.getText().toString() + " " + edtCustomerLName.getText().toString());
			}
		});

		edtCustomerLName.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				txtCustomerName.setText(edtCustomerFName.getText().toString() + " " + edtCustomerLName.getText().toString());
			}
		});

		// Format phone number if it contains 9 or 10 numbers
		//edtPhoneNumber.setOnFocusChangeListener(new FormatPhoneOnFocusChange());
		//edtPhoneNumber.addTextChangedListener(new PhoneNumberFormattingTextWatcher());

		if (UiUtil.DEBUG_BUILD) {
			edtCustomerFName.setText("Anne");
			edtCustomerLName.setText("Droid");
			edtEmailAddress.setText("dev@mportal.com");
			edtPhoneNumber.setText("9999999999");
			edtCustomerAddress.setText("address");
			edtCityName.setText("city");
			edtStateName.setText("state");
			edtZipCode.setText("zip");
		}
	}

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        populateMapLocation();
    }

    private void getAddress(final Location location)
    {
	Thread t = new Thread(new Runnable()
	{

	    @Override
	    public void run()
	    {
		List<Address> addresses = null;
		try
		{
		    Geocoder coder = new Geocoder(getActivity().getApplicationContext(), Locale.US);
		    addresses = coder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
		    ICFLogger.d(NAME, "Adresses : " + addresses);
		    if (addresses != null)
		    {
			if (addresses.size() > 0)
			{
			    final Address address = addresses.get(0);
			    final StringBuffer buf = new StringBuffer();
			    buf.append(address.getFeatureName())
			       .append(" ")
			       .append(address.getThoroughfare());
			    
			    edtCustomerAddress.post(new Runnable()
			    {

				@Override
				public void run()
				{
				    geoAddress = buf.toString();
				    autoLocation = true;
				    edtCustomerAddress.setText(buf.toString());
				    edtCityName.setText(address.getLocality());
				    edtStateName.setText(address.getAdminArea());
				    edtZipCode.setText(address.getPostalCode());
				    customerAddressEdited = false;
				    edtCustomerAddress.setBackground(getResources().getDrawable(R.drawable.text_field_white));
				    edtCustomerAddress.setTextColor(getResources().getColor(R.color.header_bg));

				    ICFLogger.d(NAME, "GeoAddress:" + geoAddress);
				    ICFLogger.d(NAME, "postalcode:" + address.getPostalCode());
				}
			    });
			}
		    }
		}
		catch (Exception e)
		{
		    e.printStackTrace();
		}
		if (addresses == null || addresses.size() == 0)
		{
		    final Activity activity = getActivity();
		    if (activity != null)
		    {
			edtCustomerAddress.post(new Runnable()
			{
			    public void run()
			    {
				UiUtil.showError(activity, activity.getResources().getString(R.string.alert_title), activity.getResources().getString(R.string.geocoder_fails));
			    }
			});
		    }
		}

	    }
	});
	t.start();
    }

    public void getLocationFromAddress(final String address)
    {
	Runnable r = new Runnable()
	{

	    @Override
	    public void run()
	    {
		String uri = "http://maps.google.com/maps/api/geocode/json?address=" + address + "&sensor=false";
		HttpGet httpGet = new HttpGet(uri);
		HttpClient client = new DefaultHttpClient();
		HttpResponse response;
		StringBuilder stringBuilder = new StringBuilder();

		try
		{
		    response = client.execute(httpGet);
		    HttpEntity entity = response.getEntity();
		    InputStream stream = entity.getContent();
		    int b;
		    while ((b = stream.read()) != -1)
		    {
			stringBuilder.append((char) b);
		    }
		}
		catch (ClientProtocolException e)
		{
		    e.printStackTrace();
		}
		catch (IOException e)
		{
		    e.printStackTrace();
		}

		JSONObject jsonObject = new JSONObject();
		try
		{
		    jsonObject = new JSONObject(stringBuilder.toString());

		    double lng = ((JSONArray) jsonObject.get("results")).getJSONObject(0).getJSONObject("geometry").getJSONObject("location").getDouble("lng");

		    double lat = ((JSONArray) jsonObject.get("results")).getJSONObject(0).getJSONObject("geometry").getJSONObject("location").getDouble("lat");

		}
		catch (Exception e)
		{

		}
	    }
	};
	Thread t = new Thread(r);
	t.start();

    }

    public void initialize()
    {
	rootAct = (RootActivity) getActivity();
	if (rootAct == null)
	{
	    return;
	}
	rootAct.currenFragment = NAME;
	resHandler = new Handler(this);
	pref = rootAct.getSharedPreferences("USERDETAILS", rootAct.MODE_PRIVATE);
	profileImageUrl = pref.getString("profileimageurl", null);
	rootAct.updateTitle(getResources().getString(R.string.customer_info_txt));
	info.setSelected(false);
    }

	public void bindCallbacks() {
        ICFLogger.d("INIT_TRACE", "bindCallbacks() entered");
		txtCustomerName.setOnClickListener(this);
		info.setOnClickListener(this);
		send_rebate_other.setOnClickListener(this);
		rebate_current_bill_checkbox.setOnClickListener(this);
		send_rebate_to_uitility_checkbox.setOnClickListener(this);
		locationPin.setOnClickListener(this);
		custom_info_continue.setOnClickListener(this);

		single_family_checkbox.setOnClickListener(this);
		multiple_family_checkbox.setOnClickListener(this);
		customerType.setOnItemSelectedListener(this);

		chkBasementFoundation.setOnClickListener(this);
		edtCustomerAddress.addTextChangedListener(addressTextWatcher);

		if (customerScreenLayout.getEmail() != null && customerScreenLayout.getEmail().isMandatory()) {
			edtEmailAddress.addTextChangedListener(new MandatoryTextWatcher(edtEmailAddress));
		}

		if (customerScreenLayout.getEmail() != null && customerScreenLayout.getPhone().isMandatory()) {
			edtPhoneNumber.addTextChangedListener(new MandatoryTextWatcher(edtPhoneNumber));
		}

		mHeatingCoolingType.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				mHeatingCoolingSelection = parent.getItemAtPosition(position).toString();
				CustomerScreenItem csi = customerScreenLayout.getHeatingCoolingSystem();
				if (mHeatingCoolingSelection.equalsIgnoreCase(AppConstants.COMBO_SELECT_VALUE)) {
					mHeatingCoolingSelection = null;
					if (csi != null && csi.isMandatory()) {
						mHeatingCoolingType.setBackgroundResource(R.drawable.bg_dialog_mandatory_box);
					}
				} else mHeatingCoolingType.setBackgroundResource(R.drawable.text_field_white);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});

		mBuildingType.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				mBuildingTypeSelection = parent.getItemAtPosition(position).toString();
				CustomerScreenItem csi = customerScreenLayout.getBuildingType();
				if (mBuildingTypeSelection.equalsIgnoreCase(getString(R.string.combo_select_value))) {
					mBuildingTypeSelection = null;
					if (csi != null && csi.isMandatory()) {
						mBuildingType.setBackgroundResource(R.drawable.bg_dialog_mandatory_box);
					}
				} else mBuildingType.setBackgroundResource(R.drawable.text_field_white);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});

		mSpinYearBuilt.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				mYearBuiltSelection = parent.getItemAtPosition(position).toString();
				CustomerScreenItem csi = customerScreenLayout.getYearBuilt();
				if (mYearBuiltSelection.equalsIgnoreCase(getString(R.string.combo_select_value))) {
					mYearBuiltSelection = null;
					if (csi != null && csi.isMandatory()) {
						mSpinYearBuilt.setBackgroundResource(R.drawable.bg_dialog_mandatory_box);
					}
				} else mSpinYearBuilt.setBackgroundResource(R.drawable.text_field_white);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});

		mSpinDistributor.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				mDistributorSelection = parent.getItemAtPosition(position).toString();
				CustomerScreenItem csi = customerScreenLayout.getDistributor();
				if (mDistributorSelection.equalsIgnoreCase(getString(R.string.combo_select_value))) {
					mDistributorSelection = null;
					if (csi != null && csi.isMandatory()) {
						mSpinDistributor.setBackgroundResource(R.drawable.bg_dialog_mandatory_box);
					}
				} else mSpinDistributor.setBackgroundResource(R.drawable.text_field_white);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});

		// Heating and cooling fuel type
		mSpinHeatingAndCoolingFuelType.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				mHeatingAndCoolingFuelTypeSelection = parent.getItemAtPosition(position).toString();
				CustomerScreenItem csi = customerScreenLayout.getHeatingCoolingFuelType();
				if (getString(R.string.combo_select_value).equals(mHeatingAndCoolingFuelTypeSelection)) {
					mHeatingAndCoolingFuelTypeSelection = null;
					if (csi != null && csi.isMandatory()) {
						mSpinHeatingAndCoolingFuelType.setBackgroundResource(R.drawable.bg_dialog_mandatory_box);
					}
				} else
					mSpinHeatingAndCoolingFuelType.setBackgroundResource(R.drawable.text_field_white);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});

		// Central A/C
		mSpinCentralAC.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				mCentralACSelection = parent.getItemAtPosition(position).toString();
				CustomerScreenItem csi = customerScreenLayout.getCentralAC();
				if (getString(R.string.combo_select_value).equals(mCentralACSelection)) {
					mCentralACSelection = null;
					if (csi != null && csi.isMandatory()) {
						mSpinCentralAC.setBackgroundResource(R.drawable.bg_dialog_mandatory_box);
					}
				} else mSpinCentralAC.setBackgroundResource(R.drawable.text_field_white);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});

		// Auditor Company
//    mSpinAuditor.setOnItemSelectedListener(new OnItemSelectedListener()
//    {
//
//        @Override
//        public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
//        {
//            mAuditorSelection = parent.getItemAtPosition(position).toString();
//            CustomerScreenItem csi = customerScreenLayout.getAuditorCompanyName();
//            if (getString(R.string.combo_select_value).equals(mAuditorSelection))
//            {
//                mAuditorSelection = null;
//                if (csi != null && csi.isMandatory()) {
//                    mSpinAuditor.setBackgroundResource(R.drawable.bg_dialog_mandatory_box);
//                }
//            } else mSpinAuditor.setBackgroundResource(R.drawable.text_field_white);
//        }
//
//        @Override
//        public void onNothingSelected(AdapterView<?> parent)
//        {/* unimpl */}
//    });

		// Water heating fuel type
		mSpinWaterHeatingFuelType.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				mWaterHeatingFuelTypeSelection = parent.getItemAtPosition(position).toString();
				CustomerScreenItem csi = customerScreenLayout.getWaterHeatingFuelType();
				if (getString(R.string.combo_select_value).equals(mWaterHeatingFuelTypeSelection)) {
					mWaterHeatingFuelTypeSelection = null;
					if (csi != null && csi.isMandatory()) {
						mSpinWaterHeatingFuelType.setBackgroundResource(R.drawable.bg_dialog_mandatory_box);
					}
				} else mSpinWaterHeatingFuelType.setBackgroundResource(R.drawable.text_field_white);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});

		// project type
		mSpinProjectType.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				mProjectTypeSelection = parent.getItemAtPosition(position).toString();
				CustomerScreenItem csi = customerScreenLayout.getProjectTypes();
				if (getString(R.string.combo_select_value).equals(mProjectTypeSelection)) {
                    mProjectTypeSelection = null;
					if (csi != null && csi.isMandatory()) {
                        mSpinProjectType.setBackgroundResource(R.drawable.bg_dialog_mandatory_box);
					}
				} else mSpinProjectType.setBackgroundResource(R.drawable.text_field_white);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});

		txtDateInstalled.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Calendar endDate = Calendar.getInstance();
				Calendar beginDate = Calendar.getInstance();
				beginDate.set(Calendar.YEAR, endDate.get(Calendar.YEAR) - 1);
				UiUtil.showDatePickDialog(getChildFragmentManager(),
						getActivity(), beginDate, endDate,
						new DatePickWatcher(txtDateInstalled, getActivity()));
			}
		});
	}

    private TextWatcher addressTextWatcher = new TextWatcher()
    {

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count)
	{

	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after)
	{

	}

	@Override
	public void afterTextChanged(Editable s) {
		if (!autoLocation) {
			customerAddressEdited = true;
			edtCustomerAddress.setBackground(getResources().getDrawable(R.drawable.text_field_lightblue));
		}
		autoLocation = false;
	}
	};

    boolean autoLocation = false;

    private String customerCountry;

    @Override
    public boolean handleMessage(Message msg)
    {
	int what = msg.what;
	// if (what == ICFHttpHandler.HTTP_UPLOAD_IMAGE_REQUEST)
	// {
	// if (homeScreenactivity != null)
	// {
	// homeScreenactivity.removeTempfile();
	// displayUserProfile();
	// UiUtil.dismissSpinnerDialog();
	// }
	// }
	return false;
    }

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.single_family_checkbox) {
			single_family_checkbox.setChecked(true);
			if (multiple_family_checkbox.isChecked()) {
				multiple_family_checkbox.setChecked(false);
			}
		} else if (id == R.id.multiple_family_checkbox) {
			multiple_family_checkbox.setChecked(true);
			if (single_family_checkbox.isChecked()) {
				single_family_checkbox.setChecked(false);
			}
		} else if (id == R.id.info_image) {
			if (info.isSelected()) {
				info.setSelected(false);
				accountDetailRoot.setVisibility(View.GONE);
			} else {
				info.setSelected(true);
				accountDetailRoot.setVisibility(View.VISIBLE);
			}
		} else if (id == R.id.send_rebate_checkbox) {
			if (!rebate_current_bill_checkbox.isChecked() && !send_rebate_to_uitility_checkbox.isChecked()) {
				send_rebate_other.setChecked(true);
			}
			if (send_rebate_other.isChecked()) {
				send_rebate_lyt.setVisibility(View.VISIBLE);

				addValidationRebateToOtherPerson();

				if (!customerScreenLayout.getSendRebateToMultiple()) {
					rebate_current_bill_checkbox.setChecked(false);
					send_rebate_to_uitility_checkbox.setChecked(false);
				}
			} else {
				send_rebate_lyt.setVisibility(View.GONE);
				ValidationManager.getInstance().clearViewList("sendRebate");
			}
		} else if (id == R.id.rebate_current_bill_checkbox) {
			if (!send_rebate_other.isChecked() && !send_rebate_to_uitility_checkbox.isChecked()) {
				rebate_current_bill_checkbox.setChecked(true);
			}
			if (rebate_current_bill_checkbox.isChecked()) {
				if (!customerScreenLayout.getSendRebateToMultiple()) {
					send_rebate_other.setChecked(false);
					send_rebate_to_uitility_checkbox.setChecked(false);
				}
				send_rebate_lyt.setVisibility(View.GONE);
			}
		} else if (id == R.id.send_rebate_to_uitility_checkbox) {
			if (!send_rebate_other.isChecked() && !rebate_current_bill_checkbox.isChecked()) {
				send_rebate_to_uitility_checkbox.setChecked(true);
			}
			if (send_rebate_to_uitility_checkbox.isChecked()) {
				if (!customerScreenLayout.getSendRebateToMultiple()) {
					rebate_current_bill_checkbox.setChecked(false);
					send_rebate_other.setChecked(false);
				}
				send_rebate_lyt.setVisibility(View.GONE);
			}
		} else if (id == R.id.custom_info_continue) {
			// Check if update is required
			if (((RootActivity) getActivity()).checkForUpdate(false)) {
				return;
			}
			if (!ValidationManager.getInstance().doValidation("customerInfo")) {
				return;
			}
			if (send_rebate_other.isChecked()) {
				if (!ValidationManager.getInstance().doValidation("sendRebate")) {
					return;
				}
			}

			CustomerScreenItem csi;
			// Validation for account numbers (edtCustomerAccNo, edtCustomerAccNo2)
			csi = customerScreenLayout.getAccountNumber();
			if (csi != null && csi.isMandatory()) {
				if (TextUtils.isEmpty(edtCustomerAccNo.getText())) {
					UiUtil.showError(getActivity(), getResources().getString(R.string.alert_title),
							getResources().getString(R.string.validation_generic_empty, csi.getTitle()));
					return;
				}
			}

			csi = customerScreenLayout.getAccountNumber2();
			if (csi != null && csi.isMandatory()) {
				if (TextUtils.isEmpty(edtCustomerAccNo2.getText())) {
					UiUtil.showError(getActivity(), getResources().getString(R.string.alert_title),
							getResources().getString(R.string.validation_generic_empty, csi.getTitle()));
					return;
				}
			}

			csi = customerScreenLayout.getPermitNumber();
			if (csi != null && csi.isMandatory()) {
				if (TextUtils.isEmpty(edtPermitNumber.getText())) {
					UiUtil.showError(getActivity(), getResources().getString(R.string.alert_title),
							getResources().getString(R.string.validation_generic_empty, csi.getTitle()));
					return;
				}
			}

			csi = customerScreenLayout.getAuditorCompanyNameText();
			if (csi != null && csi.isMandatory()) {
				if (TextUtils.isEmpty(edtAudit.getText())) {
					UiUtil.showError(getActivity(), getResources().getString(R.string.alert_title),
							getResources().getString(R.string.validation_generic_empty, csi.getTitle()));
					return;
				}
			}

			if (appDetails != null) {
				// Validation for customer type
				if (appDetails.getCustomerType() != null
						&& customerType != null
						&& customerType.getSelectedItem() != null
						&& customerType.getSelectedItem().toString().equalsIgnoreCase(AppConstants.COMBO_SELECT_VALUE)) {
					//customerScreenLayout settings can override required status
					csi = customerScreenLayout.getCustomerType();
					if ((csi != null && csi.isMandatory()) || csi == null) {
						UiUtil.showError(getActivity(), getResources().getString(R.string.alert_title), getResources().getString(R.string.validation_customer_type));
						return;
					}
				}
				// Validation for auditor company
//            if (appDetails.getAuditorCompanyName() != null && mAuditorSelection == null)
//            {
//                //customerScreenLayout settings can override required status
//                csi = customerScreenLayout.getAuditorCompanyName();
//                if ((csi != null && csi.isMandatory()) || csi == null) {
//                    UiUtil.showError(getActivity(), getResources().getString(R.string.alert_title), getResources().getString(R.string.validation_auditor_company));
//                    return;
//                }
//            }
				// Validation for building type
				if (appDetails.getBuildingTypes() != null && mBuildingTypeSelection == null) {
					//customerScreenLayout settings can override required status
					csi = customerScreenLayout.getBuildingType();
					if (csi != null && csi.isMandatory()) {
						UiUtil.showError(getActivity(), getResources().getString(R.string.alert_title),
								getResources().getString(R.string.validation_building_type));
						return;
					}
				}
				// Validation for heating/cooling type
				if (appDetails.getHeatingCoolingSystem() != null && mHeatingCoolingSelection == null) {
					//customerScreenLayout settings can override required status
					csi = customerScreenLayout.getHeatingCoolingSystem();
					if ((csi != null && csi.isMandatory()) || csi == null) {
						UiUtil.showError(getActivity(), getResources().getString(R.string.alert_title), getResources().getString(R.string.validation_heating_cooling_type));
						return;
					}
				}
				// Validation for heating and cooling fuel type
				if (appDetails.getHeatingCoolingFuelType() != null && mHeatingAndCoolingFuelTypeSelection == null) {
					//customerScreenLayout settings can override required status
					csi = customerScreenLayout.getHeatingCoolingFuelType();
					if ((csi != null && csi.isMandatory()) || csi == null) {
						UiUtil.showError(getActivity(), getResources().getString(R.string.alert_title), getResources().getString(R.string.validation_heating_cooling_fuel_type));
						return;
					}
				}
				// Validation for central a/c
				if (appDetails.getCentralAC() != null && mCentralACSelection == null) {
					//customerScreenLayout settings can override required status
					csi = customerScreenLayout.getCentralAC();
					if ((csi != null && csi.isMandatory()) || csi == null) {
						UiUtil.showError(getActivity(), getResources().getString(R.string.alert_title), getResources().getString(R.string.validation_central_ac));
						return;
					}
				}
				// Validation for water heating fuel type
				if (appDetails.getWaterHeatingFuelType() != null && mWaterHeatingFuelTypeSelection == null) {
					//customerScreenLayout settings can override required status
					csi = customerScreenLayout.getWaterHeatingFuelType();
					if ((csi != null && csi.isMandatory()) || csi == null) {
						UiUtil.showError(getActivity(), getResources().getString(R.string.alert_title), getResources().getString(R.string.validation_water_heating_fuel_type));
						return;
					}
				}
				// Validation for combo year built
				if (appDetails.getYearBuilt() != null && mYearBuiltSelection == null) {
					//customerScreenLayout settings can override required status
					csi = customerScreenLayout.getYearBuilt();
					if ((csi != null && csi.isMandatory())) {
						UiUtil.showError(getActivity(), getResources().getString(R.string.alert_title),
								getResources().getString(R.string.validation_year_built_range));
						return;
					}
				}
			}
			customerInfo = retrieveCustomerInfo();
			if (customerInfo.canEditHouseOrCustomerType()) {
				int strId = R.string.continue_rebate_screen_mess_houseType;
				if (appDetails != null && appDetails.getCustomerType() != null) {
					strId = R.string.continue_rebate_screen_mess;
				}
				UiUtil.showConfirmation(getActivity(), getResources().getString(R.string.alert_title), getResources().getString(strId),
						R.string.alert_continue, R.string.alert_cancel, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								if (which == DialogInterface.BUTTON_POSITIVE) {
									customerInfo.setCanEditHouseOrCustomerType(false);
									RebateToolListFragment toolListFragment = new RebateToolListFragment();
									((RootActivity) getActivity()).beginTransaction().replace(R.id.mainLayout, toolListFragment).addToBackStack(null).commit();
								} else if (which == DialogInterface.BUTTON_NEGATIVE) {
									dialog.dismiss();
								}
							}
						}
				);
			} else {
				customerInfo.setCanEditHouseOrCustomerType(false);
				RebateToolListFragment toolListFragment = new RebateToolListFragment();
				((RootActivity) getActivity()).beginTransaction().replace(R.id.mainLayout, toolListFragment).addToBackStack(null).commit();
			}
		} else if (id == R.id.location_pin) {
			((RootActivity) getActivity()).initializeGoogleService();
		} else {
		}
	}

    private void addValidationRebateToOtherPerson()
    {
	EditText firstName = (EditText) send_rebate_lyt.findViewById(R.id.first_name_edit);
	EditText lastName = (EditText) send_rebate_lyt.findViewById(R.id.last_name_edit);
	EditText address1 = (EditText) send_rebate_lyt.findViewById(R.id.address_edit1);
	EditText city = (EditText) send_rebate_lyt.findViewById(R.id.city_edit);
	EditText state = (EditText) send_rebate_lyt.findViewById(R.id.state_edit);
	EditText zip = (EditText) send_rebate_lyt.findViewById(R.id.zip_edit);

	ValidationManager.getInstance().clearViewList("sendRebate");
	// KVB 11/24/2015
	EditText phone = (EditText) send_rebate_lyt.findViewById(R.id.phone_edit);
	phone.setOnFocusChangeListener(new FormatPhoneOnFocusChange());
	EditText email = (EditText) send_rebate_lyt.findViewById(R.id.email_edit);

	ValidationManager.getInstance().addValidationView(phone, "sendRebate", ValidationManager.phoneRule, R.string.validation_rebate_phone, true);
	ValidationManager.getInstance().addValidationView(email, "sendRebate", ValidationManager.emailRule, R.string.validation_rebate_email, true);
	// --------------------------------------------------------------------------------------------------------------------------------------
	ValidationManager.getInstance().addValidationView(firstName, "sendRebate", ValidationManager.emptyRule, R.string.validation_rebatefname);
	ValidationManager.getInstance().addValidationView(lastName, "sendRebate", ValidationManager.emptyRule, R.string.validation_rebatelname);
	ValidationManager.getInstance().addValidationView(firstName, "sendRebate", ValidationManager.nameRule, R.string.validation_rebatefname);
	ValidationManager.getInstance().addValidationView(lastName, "sendRebate", ValidationManager.nameRule, R.string.validation_rebatelname);

	ValidationManager.getInstance().addValidationView(address1, "sendRebate", ValidationManager.emptyRule, R.string.validation_rebate_address1);
	ValidationManager.getInstance().addValidationView(city, "sendRebate", ValidationManager.emptyRule, R.string.validation_rebate_city);
	ValidationManager.getInstance().addValidationView(state, "sendRebate", ValidationManager.emptyRule, R.string.validation_rebate_state);
	ValidationManager.getInstance().addValidationView(zip, "sendRebate", ValidationManager.emptyRule, R.string.validation_rebate_zip);
    }

    private CustomerInfo retrieveCustomerInfo()
    {
	CustomerInfo info = new CustomerInfo();
	if (TextUtils.isEmpty(jobId))
	{
	    jobId = System.currentTimeMillis() + "";
	}
	info.setJobId(jobId);
	info.setCanEditHouseOrCustomerType(canEditHouseOrCustomerType);
	info.setAddressEdited(customerAddressEdited);
	info.setCustomerAccNo(edtCustomerAccNo.getText().toString());
	info.setCustomerAccNo2(edtCustomerAccNo2.getText().toString());
	info.setFirstName(edtCustomerFName.getText().toString());
	info.setLastName(edtCustomerLName.getText().toString());
	info.setCustomerAddress(edtCustomerAddress.getText().toString());
	info.setEmailAddress(edtEmailAddress.getText().toString().trim());
	info.setPhone(edtPhoneNumber.getText().toString());
	info.setCustomercity(edtCityName.getText().toString());
	info.setCustomerstate(edtStateName.getText().toString());
	info.setCustomerzipCode(edtZipCode.getText().toString());
	info.setCustomerCountry(customerCountry);
	info.setGeoAddress(geoAddress);
    info.setTechnicianName(edtTechName.getText().toString());
    info.setTechnicianId(edtTechId.getText().toString());
    info.setTechnicianPhone(edtTechPhone.getText().toString());
    info.setCustomerIncentive(edtProjectIncentive.getText().toString());

	if (currentLocation != null)
	{
	    info.setGeoLat(currentLocation.getLatitude());
	    info.setGeoLong(currentLocation.getLongitude());
	}

	if (send_rebate_to_uitility_checkbox.isChecked())
	{
	    info.setRebateOption(CustomerInfo.REBATE_UTILITY_COMPANY);
	    info.updateAppDetail(appDetails);

	    info.setDiffPerson(null);
	}
	else if (send_rebate_other.isChecked())
	{
	    info.setRebateOption(CustomerInfo.REBATE_DIFFERENT_PERSON);
	    info.updateAppDetail(appDetails);

	    EditText firstName = (EditText) send_rebate_lyt.findViewById(R.id.first_name_edit);
	    EditText lastName = (EditText) send_rebate_lyt.findViewById(R.id.last_name_edit);
	    EditText address1 = (EditText) send_rebate_lyt.findViewById(R.id.address_edit1);
	    EditText city = (EditText) send_rebate_lyt.findViewById(R.id.city_edit);
	    EditText state = (EditText) send_rebate_lyt.findViewById(R.id.state_edit);
	    EditText zip = (EditText) send_rebate_lyt.findViewById(R.id.zip_edit);
	    EditText phone = (EditText) send_rebate_lyt.findViewById(R.id.phone_edit);
	    EditText email = (EditText) send_rebate_lyt.findViewById(R.id.email_edit);
	    RebateDifferentPerson diffPerson = new RebateDifferentPerson();
	    // KVB 11/24/2015
	    diffPerson.setPhone(phone.getText().toString());
	    diffPerson.setEmail(email.getText().toString());
	    // -----------------------------------------------------------------
	    diffPerson.setFirstName(firstName.getText().toString());
	    diffPerson.setLastName(lastName.getText().toString());
	    diffPerson.setAddress(address1.getText().toString());
	    diffPerson.setCity(city.getText().toString());
	    diffPerson.setState(state.getText().toString());
	    diffPerson.setZipCode(zip.getText().toString());

	    info.setDiffPerson(diffPerson);
	}
	else if (rebate_current_bill_checkbox.isChecked())
	{
	    info.setRebateOption(CustomerInfo.REBATE_CURRENT_BILL);
	    info.updateAppDetail(appDetails);
	    info.setDiffPerson(null);
	}

	if (single_family_checkbox.isChecked())
	{
	    info.setHouseType(CustomerInfo.SINGLE_FAMILY_TYPE);
	}
	else if (multiple_family_checkbox.isChecked())
	{
	    info.setHouseType(CustomerInfo.MULTIPLE_FAMILY_TYPE);
	}

	if (appDetails != null)
	{
		if (TextUtils.isEmpty(appDetails.getDeviceType())) {
			StringBuilder deviceTyp = new StringBuilder(android.os.Build.MODEL)
					.append(" SDK Ver ")
					.append(android.os.Build.VERSION.SDK_INT)
					.append(" (")
					.append(android.os.Build.VERSION.RELEASE)
					.append(")");
			appDetails.setDeviceType(deviceTyp.toString());
		}

		String currAppVersion = getString(R.string.app_version);
		if (TextUtils.isEmpty(appDetails.getMobileAppVersion())) {
			appDetails.setMobileAppVersion(currAppVersion);
		} else if (!appDetails.getMobileAppVersion().contains(currAppVersion)) {
			StringBuilder appVer = new StringBuilder(appDetails.getMobileAppVersion())
					.append(", ")
					.append(currAppVersion);
			appDetails.setMobileAppVersion(appVer.toString());
		}

		DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);
		if (TextUtils.isEmpty(appDetails.getApplicationCreateDate())) {
			appDetails.setApplicationCreateDate(df.format(new Date(System.currentTimeMillis())));
		}
	    appDetails.setApproximateSizeofHouse(edtSizeOfHouse.getText().toString());
	    appDetails.setApproximateYearHouseBuilt(edtHouseBuiltYear.getText().toString());
	    appDetails.setBasementFoundation(chkBasementFoundation.isChecked());
	    appDetails.setNewConstruction(chkNewConstruction.isChecked());
	    appDetails.setCustomerType(customType);
	    appDetails.setHeatingCoolingSelection(mHeatingCoolingSelection);
	    appDetails.setBuildingTypeSelection(mBuildingTypeSelection);
        appDetails.setYearBuiltSelection(mYearBuiltSelection);
        appDetails.setDistributorSelection(mDistributorSelection);
	    appDetails.setWaterHeatingFuelTypeSelection(mWaterHeatingFuelTypeSelection);
	    appDetails.setHeatingCoolingFuelTypeSelection(mHeatingAndCoolingFuelTypeSelection);
	    appDetails.setCentralACSelection(mCentralACSelection);
	    appDetails.setPermitNumber(edtPermitNumber.getText().toString());
		appDetails.setAuditorCompanyNameSelection(edtAudit.getText().toString());
		appDetails.setProjectTypeSelection(mProjectTypeSelection);
		appDetails.setDateInstalled(txtDateInstalled.getText().toString());

	    if (single_family_checkbox.isChecked())
	    {
		appDetails.setHouseType(getResources().getString(R.string.single_txt));
	    }
	    else if (multiple_family_checkbox.isChecked())
	    {
		appDetails.setHouseType(getResources().getString(R.string.multi_txt));
	    }
	}
	return info;
    }

    private OnTouchListener customerHouseShowToast = new OnTouchListener()
    {
	@Override
	public boolean onTouch(View v, MotionEvent event)
	{
	    if (event.getAction() == MotionEvent.ACTION_UP)
	    {
		int id = v.getId();

		int strId = R.string.hTypecTypeCantchange;
		if (id == R.id.single_family_checkbox || id == R.id.multiple_family_checkbox)
		{
		    if (appDetails != null && appDetails.getCustomerType() != null)
		    {
			strId = R.string.hTypecTypeCantchange;
		    }
		    else
		    {
			strId = R.string.houseCantchange;
		    }
		}

		Toast.makeText(v.getContext(), strId, Toast.LENGTH_LONG).show();
	    }
	    return true;
	}
    };

	private void populateCustomerInfo(CustomerInfo info) {
        if (info == null) {
            return;
        }

        synchronized (mAppDetailsInitializer) {
            if (!mAppDetailsInitialized) {
                try {
                    ICFLogger.d("INIT_TRACE", "populateCustomerInfo() waiting on AppDetails initialization");
                    mAppDetailsInitializer.wait();
                } catch (InterruptedException e) {
                    ICFLogger.d("INIT_TRACE", "populateCustomerInfo() AppDetails initialization wait interrupted");
                }
            }

            ICFLogger.d("INIT_TRACE", "populateCustomerInfo() Go Go Go");
            jobId = info.getJobId();
            customerCountry = info.getCustomerCountry();
            edtCustomerAccNo.setText(info.getCustomerAccNo());
            edtCustomerAccNo2.setText(info.getCustomerAccNo2());
            customerAddressEdited = info.isAddressEdited();
            canEditHouseOrCustomerType = info.canEditHouseOrCustomerType();
            edtCustomerFName.setText(info.getFirstName());
            edtCustomerLName.setText(info.getLastName());
            txtCustomerName.setText(info.getFirstName() + " " + info.getLastName());
            edtTechName.setText(info.getTechnicianName());
            edtTechId.setText(info.getTechnicianId());
            edtTechPhone.setText(info.getTechnicianPhone());
            edtProjectIncentive.setText(info.getCustomerIncentive());

            if (info.isAddressEdited()) {
                edtCustomerAddress.setBackground(getResources().getDrawable(R.drawable.text_field_lightblue));
            } else {
                autoLocation = true;
                edtCustomerAddress.setBackground(getResources().getDrawable(R.drawable.text_field_white));
                edtCustomerAddress.setTextColor(getResources().getColor(R.color.header_bg));
            }
            edtCustomerAddress.setText(info.getCustomerAddress());
            edtCityName.setText(info.getCustomerCity());
            edtStateName.setText(info.getCustomerState());
            edtZipCode.setText(info.getCustomerZipCode());
            edtEmailAddress.setText(info.getEmailAddress());
            edtPhoneNumber.setText(info.getPhone());

            if (customerTypeList != null && customerTypeList.size() > 0 && customerType != null) {
                // String type = info.getCustomerType();
                String type = appDetails.getCustomerType();
                for (int i = 0; i < customerTypeList.size(); i++) {
                    if (customerTypeList.get(i).equalsIgnoreCase(type)) {
                        customerType.setSelection(i);
                    }
                }
            }
            ICFLogger.d("INIT_TRACE", "populateCustomerInfo() step 1 appDetails is null:" + (appDetails == null));
            if (appDetails != null) {
                // Sets saved heating/cooling system selection if applicable
                ICFLogger.d("INIT_TRACE", "populateCustomerInfo() HeatingCoolingSelection is empty:" + (TextUtils.isEmpty(appDetails.getHeatingCoolingSelection())));
                if (appDetails.getHeatingCoolingSelection() != null) {
                    String selection = appDetails.getHeatingCoolingSelection();
                    List<String> heatingCoolingOptions = appDetails.getHeatingCoolingSystem();
                    for (int i = 0; i < heatingCoolingOptions.size(); i++) {
                        if (heatingCoolingOptions.get(i).equalsIgnoreCase(selection)) {
                            mHeatingCoolingType.setSelection(i + 1 < mHeatingCoolingType.getCount() ? i + 1 : 0);
                        }
                    }
                }

                // Sets saved building type if applicable
                ICFLogger.d("INIT_TRACE", "populateCustomerInfo() BuildingTypeSelection is empty:" + (TextUtils.isEmpty(appDetails.getBuildingTypeSelection())));
                if (appDetails.getBuildingTypeSelection() != null) {
                    String selection = appDetails.getBuildingTypeSelection();
                    List<String> buildingTypeOptions = appDetails.getBuildingTypes();
                    for (int i = 0; i < buildingTypeOptions.size(); i++) {
                        if (buildingTypeOptions.get(i).equalsIgnoreCase(selection)) {
                            // Because of "Select a Value" being at index 0
                            mBuildingType.setSelection(i + 1 < mBuildingType.getCount() ? i + 1 : 0);
                        }
                    }
                }

                // Sets saved year built if applicable
                ICFLogger.d("INIT_TRACE", "populateCustomerInfo() YearBuiltSelection is empty:" + (TextUtils.isEmpty(appDetails.getYearBuiltSelection())));
                if (appDetails.getYearBuiltSelection() != null) {
                    String selection = appDetails.getYearBuiltSelection();
                    List<String> yearBuiltOptions = appDetails.getYearBuilt();
                    for (int i = 0; i < yearBuiltOptions.size(); i++) {
                        if (yearBuiltOptions.get(i).equalsIgnoreCase(selection)) {
                            // Because of "Select a Value" being at index 0
                            mSpinYearBuilt.setSelection(i + 1 < mSpinYearBuilt.getCount() ? i + 1 : 0);
                        }
                    }
                }

                // Sets saved distributor if applicable
                ICFLogger.d("INIT_TRACE", "populateCustomerInfo() DistributorSelection is empty:" + (TextUtils.isEmpty(appDetails.getDistributorSelection())));
                if (appDetails.getDistributor() != null) {
                    String selection = appDetails.getDistributorSelection();
                    List<String> distribOptions = appDetails.getDistributor();
                    for (int i = 0; i < distribOptions.size(); i++) {
                        if (distribOptions.get(i).equalsIgnoreCase(selection)) {
                            // Because of "Select a Value" being at index 0
                            mSpinDistributor.setSelection(i + 1 < mSpinDistributor.getCount() ? i + 1 : 0);
                        }
                    }
                }

                // Sets saved heating and cooling fuel type if applicable
                ICFLogger.d("INIT_TRACE", "populateCustomerInfo() HeatingCoolingFuelTypeSelection is empty:" + (TextUtils.isEmpty(appDetails.getHeatingCoolingFuelTypeSelection())));
                if (appDetails.getHeatingCoolingFuelType() != null) {
                    String selection = appDetails.getHeatingCoolingFuelTypeSelection();
                    List<String> heatingAndCoolingFuelTypeOptions = appDetails.getHeatingCoolingFuelType();
                    for (int i = 0; i < heatingAndCoolingFuelTypeOptions.size(); i++) {
                        if (heatingAndCoolingFuelTypeOptions.get(i).equals(selection)) {
                            mSpinHeatingAndCoolingFuelType.setSelection(i + 1 < mSpinHeatingAndCoolingFuelType.getCount() ? i + 1 : 0);
                        }
                    }
                }

                // Sets central a/c of applicable
                List<String> centralACOptions;
                ICFLogger.d("INIT_TRACE", "populateCustomerInfo() CentralACSelection is empty:" + (TextUtils.isEmpty(appDetails.getCentralACSelection())));
                if ((centralACOptions = appDetails.getCentralAC()) != null) {
                    String selection = appDetails.getCentralACSelection();
                    for (int i = 0; i < centralACOptions.size(); i++) {
                        if (centralACOptions.get(i).equals(selection)) {
                            mSpinCentralAC.setSelection(i + 1 < mSpinCentralAC.getCount() ? i + 1 : 0);
                        }
                    }
                }

                // Sets saved water heater fuel type if applicable
                ICFLogger.d("INIT_TRACE", "populateCustomerInfo() WaterHeatingFuelTypeSelection is empty:" + (TextUtils.isEmpty(appDetails.getWaterHeatingFuelTypeSelection())));
                if (appDetails.getWaterHeatingFuelType() != null) {
                    String selection = appDetails.getWaterHeatingFuelTypeSelection();
                    List<String> waterHeatingFuelOptions = appDetails.getWaterHeatingFuelType();
                    for (int i = 0; i < waterHeatingFuelOptions.size(); i++) {
                        if (waterHeatingFuelOptions.get(i).equals(selection)) {
                            // Because of "Select a Value" being at // index 0
                            mSpinWaterHeatingFuelType.setSelection(i + 1 < mSpinWaterHeatingFuelType.getCount() ? i + 1 : 0);
                        }
                    }
                }

                // Sets project type selection if applicable
                List<String> projectTypes;
                ICFLogger.d("INIT_TRACE", "populateCustomerInfo() ProjectTypeSelection is empty:" +
                        (TextUtils.isEmpty(appDetails.getProjectTypeSelection())));
                if ((projectTypes = appDetails.getProjectTypes()) != null) {
                    String selection = appDetails.getProjectTypeSelection();
                    for (int i = 0; i < projectTypes.size(); i++) {
                        if (projectTypes.get(i).equals(selection)) {
                            mSpinProjectType.setSelection(i + 1 < mSpinProjectType.getCount() ? i + 1 : 0);
                        }
                    }
                }

                //Sets optional date installed
                txtDateInstalled.setText(appDetails.getDateInstalled());
            }
            currentLocation = new Location("current");
            currentLocation.setLatitude(info.getGeoLat());
            currentLocation.setLongitude(info.getGeoLong());
            currentLocation.setTime(new Date().getTime());

            rebate_current_bill_checkbox.setChecked(false);
            send_rebate_to_uitility_checkbox.setChecked(false);
            send_rebate_other.setChecked(false);

            if (info.getRebateOption() == CustomerInfo.REBATE_UTILITY_COMPANY) {
                send_rebate_to_uitility_checkbox.setChecked(true);
            } else if (info.getRebateOption() == CustomerInfo.REBATE_DIFFERENT_PERSON) {
                send_rebate_other.setChecked(true);

                addValidationRebateToOtherPerson();

                send_rebate_lyt.setVisibility(View.VISIBLE);

                EditText firstName = (EditText) send_rebate_lyt.findViewById(R.id.first_name_edit);
                EditText lastName = (EditText) send_rebate_lyt.findViewById(R.id.last_name_edit);
                EditText address1 = (EditText) send_rebate_lyt.findViewById(R.id.address_edit1);
                EditText city = (EditText) send_rebate_lyt.findViewById(R.id.city_edit);
                EditText state = (EditText) send_rebate_lyt.findViewById(R.id.state_edit);
                EditText zip = (EditText) send_rebate_lyt.findViewById(R.id.zip_edit);
                // KVB 11/25/2015
                EditText phone = (EditText) send_rebate_lyt.findViewById(R.id.phone_edit);
                EditText email = (EditText) send_rebate_lyt.findViewById(R.id.email_edit);
                // -------------------------------------------------------------------------

                RebateDifferentPerson diffPerson = info.getDiffPerson();
                if (diffPerson != null) {
                    firstName.setText(diffPerson.getFirstName());
                    lastName.setText(diffPerson.getLastName());
                    address1.setText(diffPerson.getAddress());
                    city.setText(diffPerson.getCity());
                    state.setText(diffPerson.getState());
                    zip.setText(diffPerson.getZipCode());
                    phone.setText(diffPerson.getPhone());
                    email.setText(diffPerson.getEmail());
                }

            } else if (info.getRebateOption() == CustomerInfo.REBATE_CURRENT_BILL) {
                rebate_current_bill_checkbox.setChecked(true);
            }

            if (!canEditHouseOrCustomerType) {
                single_family_checkbox.setClickable(false);
                multiple_family_checkbox.setClickable(false);
                customerType.setClickable(false);

                customerType.setOnTouchListener(customerHouseShowToast);
                single_family_checkbox.setOnTouchListener(customerHouseShowToast);
                multiple_family_checkbox.setOnTouchListener(customerHouseShowToast);
            }

            single_family_checkbox.setChecked(false);
            multiple_family_checkbox.setChecked(false);

            if (CustomerInfo.SINGLE_FAMILY_TYPE == info.getHouseType()) {
                single_family_checkbox.setChecked(true);
            } else if (CustomerInfo.MULTIPLE_FAMILY_TYPE == info.getHouseType()) {
                multiple_family_checkbox.setChecked(true);
            }

            ICFLogger.d("INIT_TRACE", "populateCustomerInfo() step 2 appDetails is null:" + (appDetails == null));
            if (appDetails != null) {
                edtSizeOfHouse.setText(appDetails.getApproximateSizeofHouse());
                edtHouseBuiltYear.setText(appDetails.getApproximateYearHouseBuilt());
                chkBasementFoundation.setChecked(appDetails.isBasementFoundation());
                chkNewConstruction.setChecked(appDetails.isNewConstruction());
                edtPermitNumber.setText(appDetails.getPermitNumber());
                edtAudit.setText(appDetails.getAuditorCompanyNameSelection());
            }
        }
    }

    @Override
    public void onDestroyView()
    {
	super.onDestroyView();
	if (getActivity() != null)
	{
	    android.app.FragmentManager fm = getActivity().getFragmentManager();
	    if (fm != null)
	    {
		android.app.Fragment f = (android.app.Fragment) fm.findFragmentById(R.id.map);
		if (f != null)
		{
		    android.app.FragmentTransaction ft = fm.beginTransaction();
		    ft.remove(f);
		    ft.commitAllowingStateLoss();
		}
	    }
	}
    }

    @Override
    public void onStop()
    {
	super.onStop();
	((RootActivity) getActivity()).cleanupGoogleService();
	((RootActivity) getActivity()).dismissUpdateDialog();
	FormResponseBean bean = RebateManager.getInstance().getFormBean();
	if (bean != null)
	{
	    if (customerInfo == null)
	    {
		customerInfo = retrieveCustomerInfo();
		if (TextUtils.isEmpty(customerInfo.getFirstName()) || TextUtils.isEmpty(customerInfo.getLastName()))
		{
		    return;
		}
	    }
	    customerInfo.setJobRootPath(bean.getCustomerInfo().getJobRootPath());
	    customerInfo.setState(bean.getCustomerInfo().getState());
	    bean.setCustomerInfo(customerInfo);
	    bean.setAppDetails(appDetails);
	    bean.filterEquipmentList();
	    DataParserImpl.getInstance().writeJson(bean, null);
	}
	else
	{
	    if (customerInfo != null)
	    {
		if (!TextUtils.isEmpty(customerInfo.getFirstName()))
		{
		    RebateManager.getInstance().resetJob();
		    // TODO this bean is returned null sometimes creating trap..
		    bean = RebateManager.getInstance().createCachedFormBean();
		    customerInfo.setState(CustomerInfo.NEW_JOB);
		    customerInfo.setJobRootPath(FileUtil.getJobDirectory(jobId));
		    bean.setCustomerInfo(customerInfo);
		    bean.setContractorDetail(null);
		    bean.setContractorDetail(RebateManager.getInstance().getContractorDetail());
		    bean.setAppDetails(appDetails);
		    RebateManager.getInstance().setCurrentBean(bean);
		    DataParserImpl.getInstance().writeJson(bean, null);
		}
	    }
	}

    }

    public void updateLocation(Location location)
    {
	if (location != null)
	{
	    currentLocation = location;
	    CameraUpdate point = CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 17.0f);
	    // moves camera to coordinates
	    // googleMap.moveCamera(point);
	    // animates camera to coordinates
	    googleMap.animateCamera(point, 1000, null);

	    getAddress(location);

	    ((RootActivity) getActivity()).cleanupGoogleService();
	    plotLocation();
	}

    }

	private void updateAppDetails() {
        mAppDetailsInitialized = false;
		mAppDetailsInitializer = new Runnable() {
            @Override
            public void run() {
                ICFLogger.d("INIT_TRACE", "updateAppDetails(); inner UI thread starting");
                if (appDetails != null) {
                    customerTypeList.clear();
                    if (appDetails.getCustomerType() != null) {
                        customerTypeLyt.setVisibility(View.VISIBLE);
                        customerTypeList.add(AppConstants.COMBO_SELECT_VALUE);

                        List<String> customerTypes = appDetails.getSelectableCustomerTypes();
                        for (String type : customerTypes) {
                            customerTypeList.add(type);
                        }

                        CustomSpinnerAdapter customTypeAdapter = new CustomSpinnerAdapter(getActivity(), 0, customerTypeList);
                        customerType.setAdapter(customTypeAdapter);
                        //customerType.setSelection(0, true);
                        customType = customerType.getSelectedItem().toString();
                        try {
                            String customerTypeLbl = customerScreenLayout.getCustomerType().getTitle();
                            ((TextView) customerTypeLyt.findViewById(R.id.customer_type_txt)).setText(customerTypeLbl);
                        } catch (Exception e) {
                        } //label defaults to "Customer Type"
                    } else {
                        customerTypeLyt.setVisibility(View.GONE);
                    }

                    if (appDetails.getHeatingCoolingSystem() != null) {
                        mHeatingCoolingLayout.setVisibility(View.VISIBLE);
                        List<String> spinnerOptions = new ArrayList<String>(appDetails.getHeatingCoolingSystem());
                        spinnerOptions.add(0, AppConstants.COMBO_SELECT_VALUE);
                        CustomSpinnerAdapter customTypeAdapter = new CustomSpinnerAdapter(getActivity(), 0, spinnerOptions);
                        mHeatingCoolingType.setAdapter(customTypeAdapter);
                    } else {
                        mHeatingCoolingLayout.setVisibility(View.GONE);
                    }

                    if (appDetails.getBuildingTypes() != null) {
                        mBuildingTypeLayout.setVisibility(View.VISIBLE);
                        List<String> btSpinnerOptions = new ArrayList<String>(appDetails.getBuildingTypes());
                        btSpinnerOptions.add(0, getString(R.string.combo_select_value));
                        CustomSpinnerAdapter btCustomTypeAdapter = new CustomSpinnerAdapter(getActivity(), 0, btSpinnerOptions);
                        mBuildingType.setAdapter(btCustomTypeAdapter);
                    } else {
                        mBuildingTypeLayout.setVisibility(View.GONE);
                    }

                    // Set options to water heating fuel type if in json
                    if (appDetails.getWaterHeatingFuelType() != null) {
                        mLytWaterHeatingFuelType.setVisibility(View.VISIBLE);
                        List<String> spinnerOptions = new ArrayList<>(appDetails.getWaterHeatingFuelType());
                        spinnerOptions.add(0, getString(R.string.combo_select_value));
                        CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(getActivity(), 0, spinnerOptions);
                        mSpinWaterHeatingFuelType.setAdapter(adapter);
                    } else {
                        mLytWaterHeatingFuelType.setVisibility(View.GONE);
                    }

                    // Set options to heating cooling fuel type if applicable
                    if (appDetails.getHeatingCoolingFuelType() != null) {
                        mLytHeatingAndCoolingFuelType.setVisibility(View.VISIBLE);
                        List<String> spinnerOptions = new ArrayList<>(appDetails.getHeatingCoolingFuelType());
                        spinnerOptions.add(0, getString(R.string.combo_select_value));
                        CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(getActivity(), 0, spinnerOptions);
                        mSpinHeatingAndCoolingFuelType.setAdapter(adapter);
                        // TODO set validation
                    } else {
                        mLytHeatingAndCoolingFuelType.setVisibility(View.GONE);
                    }

                    // Set options to central ac if applicable
                    if (appDetails.getCentralAC() != null) {
                        mLytCentralAC.setVisibility(View.VISIBLE);
                        List<String> spinnerOptions = new ArrayList<>(appDetails.getCentralAC());
                        spinnerOptions.add(0, getString(R.string.combo_select_value));
                        CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(getActivity(), 0, spinnerOptions);
                        mSpinCentralAC.setAdapter(adapter);
                    } else {
                        mLytCentralAC.setVisibility(View.GONE);
                    }

                    // Set options to year built if applicable
                    if (appDetails.getYearBuilt() != null) {
                        mYearBuiltSpinLyt.setVisibility(View.VISIBLE);
                        List<String> spinnerOptions = new ArrayList<>(appDetails.getYearBuilt());
                        spinnerOptions.add(0, getString(R.string.combo_select_value));
                        CustomSpinnerAdapter adapter = new CustomSpinnerAdapter
                                (getActivity(), 0, spinnerOptions);
                        mSpinYearBuilt.setAdapter(adapter);
                    } else {
                        mYearBuiltSpinLyt.setVisibility(View.GONE);
                    }

                    // Set options to distributor if applicable
                    if (appDetails.getDistributor() != null) {
                        mDistributorLyt.setVisibility(View.VISIBLE);
                        List<String> spinnerOptions = new ArrayList<>(appDetails.getDistributor());
                        spinnerOptions.add(0, getString(R.string.combo_select_value));
                        CustomSpinnerAdapter adapter = new CustomSpinnerAdapter
                                (getActivity(), 0, spinnerOptions);
                        mSpinDistributor.setAdapter(adapter);
                    } else {
                        mDistributorLyt.setVisibility(View.GONE);
                    }

                    if (appDetails.getProjectTypes() != null) {
                        mProjectTypeLyt.setVisibility(View.VISIBLE);
                        List<String> ptSpinnerOptions = new ArrayList<String>(appDetails.getProjectTypes());
                        ptSpinnerOptions.add(0, getString(R.string.combo_select_value));
                        CustomSpinnerAdapter ptCustomTypeAdapter = new CustomSpinnerAdapter(getActivity(), 0, ptSpinnerOptions);
                        mSpinProjectType.setAdapter(ptCustomTypeAdapter);
                    } else {
                        mProjectTypeLyt.setVisibility(View.GONE);
                    }
                    // Set options to auditor company if applicable
//				if (appDetails.getAuditorCompanyName() != null)
//				{
//                  mLytAuditor.setVisibility(View.VISIBLE);
//					List<String> spinnerOptions = new ArrayList<>(appDetails.getAuditorCompanyName());
//					spinnerOptions.add(0, getString(R.string.combo_select_value));
//					CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(getActivity(), 0, spinnerOptions);
//					mSpinAuditor.setAdapter(adapter);
//				}
//				else {
//					mLytAuditor.setVisibility(View.GONE);
//				}
                } else {
                    customerTypeLyt.setVisibility(View.GONE);
                }

			/*
			 * This is where labels will be changed or fields will be hidden based on the contents of form_data.json#customerScreen, TODO incomplete
			 * right now
			 */
                if (rootView != null) {
                    TextView tvAccount1 = (TextView) rootView.findViewById(R.id.rebate_customer_label);
                    TextView tvAccount2 = (TextView) rootView.findViewById(R.id.rebate_customer_label2);
                    TextView tvPermitNumber = (TextView) rootView.findViewById(R.id.lbl_permit_number);
                    TextView tvWaterHeatingFuelType = (TextView) rootView.findViewById(R.id.header_water_heating_fuel_type);
                    TextView tvCentralAC = (TextView) rootView.findViewById(R.id.lbl_ac);
                    TextView tvAuditor = (TextView) rootView.findViewById(R.id.lbl_audit);
                    TextView tvHeatingCoolingFuelType = (TextView) rootView.findViewById(R.id.header_heating_cooling_fuel_type);
                    TextView tvHeatingCoolingSystemType = (TextView) rootView.findViewById(R.id.txt_heating_cooling);
                    TextView tvFirstName = (TextView) rootView.findViewById(R.id.rebate_lblFirstName);
                    TextView tvLastName = (TextView) rootView.findViewById(R.id.rebate_lblLastName);
                    TextView tvYearBuilt = (TextView) rootView.findViewById(R.id.lbl_year_built);
                    TextView tvDateInstalledLbl = (TextView) rootView.findViewById(R.id.lbl_date_installed);
                    TextView tvTechNameLbl = (TextView) rootView.findViewById(R.id.technicianNameLbl);
                    TextView tvTechIdLbl = (TextView) rootView.findViewById(R.id.technicianIdLbl);
                    TextView tvTechPhoneLbl = (TextView) rootView.findViewById(R.id.technicianPhoneLbl);
                    TextView tvProjectIncentiveLbl = (TextView) rootView.findViewById(R.id.projectIncentiveLbl);
                    TextView tvProjectTypeLbl = (TextView) rootView.findViewById(R.id.projectTypeLbl);


                    if (customerScreenLayout != null) {

                        // Label values
                        String newConstruction, basementFoundation, singleFamily, multiFamily,
                                firstName, lastName;
                        CustomerScreenItem account1, account2, permitNumber, approxHouseSize,
                                approxYearBuilt, yearBuilt, auditorCompanyName, dateInstalled;

                        firstName = customerScreenLayout.getFirstName();
                        lastName = customerScreenLayout.getLastName();
                        account1 = customerScreenLayout.getAccountNumber();
                        account2 = customerScreenLayout.getAccountNumber2();
                        permitNumber = customerScreenLayout.getPermitNumber();
                        approxHouseSize = customerScreenLayout.getApproximateSizeofHouse();
                        approxYearBuilt = customerScreenLayout.getApproximateYearHouseBuilt();
                        yearBuilt = customerScreenLayout.getYearBuilt();
                        newConstruction = customerScreenLayout.getNewConstruction();
                        basementFoundation = customerScreenLayout.getBasementFoundation();
                        multiFamily = customerScreenLayout.getMultiFamily();
                        singleFamily = customerScreenLayout.getSingleFamily();
                        auditorCompanyName = customerScreenLayout.getAuditorCompanyNameText();
                        dateInstalled = customerScreenLayout.getDateInstalled();

                        // First & Last Name
                        if (!TextUtils.isEmpty(firstName)) tvFirstName.setText(firstName);
                        if (!TextUtils.isEmpty(lastName)) tvLastName.setText(lastName);

                        // Account 1
                        if (account1 == null || account1.getTitle().equals("")) {
                            tvAccount1.setVisibility(View.GONE);
                            if (edtCustomerAccNo != null)
                                edtCustomerAccNo.setVisibility(View.GONE);
                        } else {
                            // Add acct# length validation as needed
                            ValidationManager.ValidationRule rule = ValidationManager.lengthValidationRule(account1);
                            if (rule != null) {
                                ValidationManager.getInstance().addValidationView(edtCustomerAccNo, "customerInfo", rule, 0, true);
                            }
                            //add red box as needed
                            if (account1.isMandatory()) {
                                edtCustomerAccNo.addTextChangedListener(new MandatoryTextWatcher(edtCustomerAccNo));
                            }
                            tvAccount1.setText(account1.getTitle());
                        }
                        // Account 2
                        if (account2 == null || account2.getTitle().equals("")) {
                            tvAccount2.setVisibility(View.GONE);
                            if (edtCustomerAccNo2 != null)
                                edtCustomerAccNo2.setVisibility(View.GONE);
                        } else {
                            ValidationManager.ValidationRule rule = ValidationManager.lengthValidationRule(account2);
                            if (rule != null) {
                                ValidationManager.getInstance()
                                        .addValidationView(edtCustomerAccNo2, "customerInfo", rule, R.string.validation_house_size, !approxHouseSize.isMandatory());
                            }
                            //add red box as needed
                            if (account2.isMandatory()) {
                                edtCustomerAccNo2.addTextChangedListener(new MandatoryTextWatcher(edtCustomerAccNo2));
                            }
                            tvAccount2.setText(account2.getTitle());
                        }

                        // Permit number
                        if (permitNumber == null || permitNumber.getTitle().equals("")) {
                            hideParentView(tvPermitNumber);
                        } else {
                            ValidationManager.ValidationRule rule = ValidationManager.lengthValidationRule(permitNumber);
                            if (rule != null) {
                                ValidationManager.getInstance()
                                        .addValidationView(edtPermitNumber, "customerInfo", rule,
                                                0, !permitNumber.isMandatory());
                            }
                            //add red box as needed
                            if (permitNumber.isMandatory()) {
                                edtPermitNumber.addTextChangedListener(new MandatoryTextWatcher(edtPermitNumber));
                            }
                            tvPermitNumber.setText(permitNumber.getTitle());
                        }

                        // Basement Foundation
                        if (basementFoundation != null && !basementFoundation.trim().equals("")) {
                            chkBasementFoundation.setText(basementFoundation);
                        } else {
                            hideView((View) chkBasementFoundation.getParent());
                        }
                        // New construction
                        if (newConstruction != null && !newConstruction.trim().equals("")) {
                            chkNewConstruction.setText(newConstruction);
                        } else {
                            hideView((View) chkNewConstruction.getParent());
                        }
                        // Heating and cooling fuel type
                        if (tvHeatingCoolingFuelType != null && customerScreenLayout.getHeatingCoolingFuelType() != null) {
                            tvHeatingCoolingFuelType.setText(customerScreenLayout.getHeatingCoolingFuelType().getTitle());
                        }
                        // Central a/c
                        if (tvCentralAC != null && customerScreenLayout.getCentralAC() != null) {
                            tvCentralAC.setText(customerScreenLayout.getCentralAC().getTitle());
                        }

                        // Auditor Company
                        if (auditorCompanyName == null || auditorCompanyName.getTitle().equals("")) {
                            hideParentView(tvAuditor);
                        } else {
                            ValidationManager.ValidationRule rule = ValidationManager.lengthValidationRule(auditorCompanyName);
                            if (rule != null) {
                                ValidationManager.getInstance()
                                        .addValidationView(edtAudit, "customerInfo", rule,
                                                0, !auditorCompanyName.isMandatory());
                            }
                            //add red box as needed
                            if (auditorCompanyName.isMandatory()) {
                                edtAudit.addTextChangedListener(new MandatoryTextWatcher(edtAudit));
                            }
                            tvAuditor.setText(auditorCompanyName.getTitle());
                        }

                        // Optional Date Installed
                        if (dateInstalled != null) {
                            mDateInstalledLyt.setVisibility(View.VISIBLE);
                            if (!TextUtils.isEmpty(dateInstalled.getTitle())) {
                                tvDateInstalledLbl.setText(dateInstalled.getTitle());
                            }
                            if (dateInstalled.isMandatory()) {
                                txtDateInstalled.setBackground(
                                        TextUtils.isEmpty(appDetails.getDateInstalled()) ?
                                                getResources().getDrawable(R.drawable.bg_dialog_mandatory_box) :
                                                getResources().getDrawable(R.drawable.text_field_white)
                                );
                                ValidationManager.getInstance().addValidationView(txtDateInstalled, "customerInfo",
                                        ValidationManager.emptyRule, R.string.validation_generic_empty,
                                        new String[]{tvDateInstalledLbl.getText().toString()});
                            } else {
                                txtDateInstalled.setBackground(getResources().getDrawable(R.drawable.text_field_white));
                            }
                        }

                        // Water heating fuel type
                        if (tvWaterHeatingFuelType != null && customerScreenLayout.getWaterHeatingFuelType() != null) {
                            tvWaterHeatingFuelType.setText(customerScreenLayout.getWaterHeatingFuelType().getTitle());
                        }
                        // Heating cooling system
                        if (tvHeatingCoolingSystemType != null && customerScreenLayout.getHeatingCoolingSystem() != null) {
                            tvHeatingCoolingSystemType.setText(customerScreenLayout.getHeatingCoolingSystem().getTitle());
                        }

                        // Single family checkbox
                        if (TextUtils.isEmpty(singleFamily)) {
                            hideView(single_family_checkbox);
                            hideView(getActivity().findViewById(R.id.custom_info_divider5)); // Divider
                        }

                        // Multiple family checkbox
                        if (TextUtils.isEmpty(multiFamily)) {
                            hideView(multiple_family_checkbox);
                            hideView(getActivity().findViewById(R.id.custom_info_divider6)); // Divider
                        } else {
                            multiple_family_checkbox.setText(multiFamily);
                        }

                        // Hide House Type header as needed
                        if (TextUtils.isEmpty(singleFamily) && TextUtils.isEmpty(multiFamily)) {
                            hideView(getActivity().findViewById(R.id.family_type));
                            hideView(getActivity().findViewById(R.id.custom_info_divider4));
                        }

                        // Approximate House Size can be optional
                        if (approxHouseSize == null || approxHouseSize.getTitle().equals("")) {
                            hideView(rootView.findViewById(R.id.sizeOfHouseRoot));
//                                if (appDetails.getYearBuilt() == null)
//                                    hideView(rootView.findViewById(R.id.custom_info_divider7));
                        } else {
                            // Add validation to House Size as needed
                            String minVal = approxHouseSize.getMinValue();
                            String maxVal = approxHouseSize.getMaxValue();
                            //only add empty rule if mandatory and range rule is undefined; range rule supercedes
                            //empty rule
                            if (minVal != null && maxVal != null) {
                                ValidationManager.RangeValidationRule rule =
                                        new ValidationManager.RangeValidationRule(minVal, maxVal);
                                ValidationManager.getInstance()
                                        .addValidationView(edtSizeOfHouse, "customerInfo", rule, R.string.validation_house_size, !approxHouseSize.isMandatory());
                            } else if (approxHouseSize.isMandatory()) {
                                ValidationManager.EmptyValidationRule rule = new ValidationManager.EmptyValidationRule();
                                ValidationManager.getInstance()
                                        .addValidationView(edtSizeOfHouse, "customerInfo", rule, R.string.validation_house_size_empty, false);
                            }

                            //add red box as needed
                            if (approxHouseSize.isMandatory()) {
                                edtSizeOfHouse.addTextChangedListener(new MandatoryTextWatcher(edtSizeOfHouse));
                            }
                            edtSizeOfHouse.addTextChangedListener(sizeOfHouseWatcher);
                        }

                        // Approximate Year Built can be optional
                        if (approxYearBuilt == null || approxYearBuilt.getTitle().equals("")) {
                            hideView(rootView.findViewById(R.id.yearHouseBuiltEdtRoot));
//                                if (appDetails.getYearBuilt() != null)
//                                    hideView(rootView.findViewById(R.id.custom_info_divider7));
                        } else {
                            // Add validation to Year Built as needed
                            String minVal = approxYearBuilt.getMinValue();
                            String maxVal = approxYearBuilt.getMaxValue();
                            //only add empty rule if mandatory and range rule is undefined; range rule supercedes
                            //empty rule
                            if (minVal != null && maxVal != null) {
                                ValidationManager.RangeValidationRule rule = new ValidationManager.RangeValidationRule(minVal, maxVal);
                                ValidationManager.getInstance()
                                        .addValidationView(edtHouseBuiltYear, "customerInfo", rule, R.string.validation_year_built, !approxYearBuilt.isMandatory());
                            } else if (approxYearBuilt.isMandatory()) {
                                ValidationManager.EmptyValidationRule rule = new ValidationManager.EmptyValidationRule();
                                ValidationManager.getInstance()
                                        .addValidationView(edtHouseBuiltYear, "customerInfo", rule, R.string.validation_year_built_empty, false);
                            }

                            //add red box as needed
                            if (approxYearBuilt.isMandatory()) {
                                edtHouseBuiltYear.addTextChangedListener(new MandatoryTextWatcher(edtHouseBuiltYear));
                            }
                        }

                        // Spin selector for yearBuilt date ranges
                        if (tvYearBuilt != null && customerScreenLayout.getYearBuilt() != null) {
                            tvYearBuilt.setText(customerScreenLayout.getYearBuilt().getTitle());
                        }

                        //customize "send rebate to" labelling or hide "send rebate to" options as needed
                        int send_to_opts = 0;
                        if (!TextUtils.isEmpty(customerScreenLayout.getSendRebateToCustomer())) {
                            if (rebate_current_bill_checkbox != null) {
                                rebate_current_bill_checkbox.setText(customerScreenLayout.getSendRebateToCustomer());
                                send_to_opts++;
                            }
                        } else {
                            hideView(rebate_current_bill_checkbox);
                            hideView(rootView.findViewById(R.id.custom_info_divider1));
                        }

                        if (!TextUtils.isEmpty(customerScreenLayout.getSendRebateToHVACContractor())) {
                            if (send_rebate_to_uitility_checkbox != null) {
                                send_rebate_to_uitility_checkbox.setText(customerScreenLayout.getSendRebateToHVACContractor());
                                send_to_opts++;
                            }
                        } else {
                            hideView(send_rebate_to_uitility_checkbox);
                            hideView(rootView.findViewById(R.id.custom_info_divider2));
                        }

                        if (!TextUtils.isEmpty(customerScreenLayout.getSendRebateToDifferentPerson())) {
                            if (send_rebate_other != null) {
                                send_rebate_other.setText(customerScreenLayout.getSendRebateToDifferentPerson());
                                send_to_opts++;
                            }
                        } else {
                            hideView(send_rebate_other);
                            hideView(rootView.findViewById(R.id.custom_info_divider3));
                        }
                        if (send_to_opts == 0) {
                            hideView(rootView.findViewById(R.id.send_rebate_to));
                        }

                        //treat Technician Info field group as an all or nothing
                        if (customerScreenLayout.getTechnicianName() != null) {
                            mTechInfoLyt.setVisibility(View.VISIBLE);
                            tvTechNameLbl.setText(customerScreenLayout.getTechnicianName().getTitle());
                        }
                        if (customerScreenLayout.getTechnicianId() != null) {
                            mTechInfoLyt.setVisibility(View.VISIBLE);
                            tvTechIdLbl.setText(customerScreenLayout.getTechnicianId().getTitle());
                        }
                        if (customerScreenLayout.getTechnicianPhone() != null) {
                            mTechInfoLyt.setVisibility(View.VISIBLE);
                            tvTechPhoneLbl.setText(customerScreenLayout.getTechnicianPhone().getTitle());
                        }
                        if (customerScreenLayout.getProjectIncentive() != null) {
                            mTechInfoLyt.setVisibility(View.VISIBLE);
                            tvProjectIncentiveLbl.setText(customerScreenLayout.getProjectIncentive().getTitle());
                        }
                        if (customerScreenLayout.getProjectTypes() != null) {
                            mTechInfoLyt.setVisibility(View.VISIBLE);
                            tvProjectTypeLbl.setText(customerScreenLayout.getProjectTypes().getTitle());
                        }
                    } //END if (customerScreenLayout != null)
                    else {
                        // Remove
                        if (tvAccount2 != null && edtCustomerAccNo2 != null) {
                            tvAccount2.setVisibility(View.GONE);
                            edtCustomerAccNo2.setVisibility(View.GONE);
                            tvPermitNumber.setVisibility(View.GONE);
                        }

                        hideParentView(tvPermitNumber);
                        hideView(chkNewConstruction);
                        hideView(chkBasementFoundation);
                    }
                }
                synchronized (this) {
                    mAppDetailsInitialized = true;
                    notifyAll();
                    ICFLogger.d("INIT_TRACE", "updateAppDetails(); inner UI thread exiting");
                }
            }
        };

//        Thread t = new Thread(new Runnable() {
//		//mFormInitializer = new Runnable() {
//			@Override
//			public void run() {
//                ICFLogger.d("INIT_TRACE", "updateAppDetails() thread started");
                getActivity().runOnUiThread(mAppDetailsInitializer);
//                ICFLogger.d("INIT_TRACE", "updateAppDetails(); thread exiting");
//            }
//	});
//    t.start();
}

    private void hideView(View v)
    {
	if (v != null)
	{
	    v.setVisibility(View.GONE);
	}
    }

    private void hideParentView(View v)
    {
	if (v != null && v.getParent() != null)
	{
	    ((ViewGroup) v.getParent()).setVisibility(View.GONE);
	}
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        CustomerScreenItem csi = customerScreenLayout.getCustomerType();
        if (customerTypeList != null && customerTypeList.size() >= position)
        {
            customType = customerTypeList.get(position);
            if (customType.equalsIgnoreCase(AppConstants.COMBO_SELECT_VALUE))
            {
                customType = null;
                if (csi != null && csi.isMandatory()) {
                    customerType.setBackgroundResource(R.drawable.bg_dialog_mandatory_box);
                }
            } else customerType.setBackgroundResource(R.drawable.text_field_white);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {

    }

    /**
     * This listens for focus changes then formats a phone number input, only set on {@link:EditText}.
     * 
     * @author 35527
     *
     */
    public static class FormatPhoneOnFocusChange implements OnFocusChangeListener
    {

	@Override
	public void onFocusChange(View v, boolean hasFocus)
	{

	    if (!(v instanceof EditText))
	    {
		throw new IllegalArgumentException("FormatPhoneOnFocusChange can only be bound to the EditText class or subclasses");
	    }

	    EditText edtPhone = (EditText) v;

	    if (!hasFocus)
	    {
		String phoneText = edtPhone.getText().toString();
		int i = 0, j = 0;
		char[] digits = new char[11];
		for (; i < phoneText.length(); i++)
		{
		    char c = phoneText.charAt(i);
		    if (c >= '0' && c <= '9')
		    {
			// Can't have more than 11 digits in phone number
			if (j >= 11)
			{
			    j = -1;
			    break;
			}
			digits[j++] = c;
		    }
		}
		if (j == 10)
		{
		    edtPhone.setText("(" + digits[0] + digits[1] + digits[2] + ")" + " " + digits[3] + digits[4] + digits[5] + "-" + digits[6] + digits[7] + digits[8] + digits[9]);
		}
		else if (j == 11)
		{
		    edtPhone.setText(digits[0] + "-" + digits[1] + digits[2] + digits[3] + "-" + digits[4] + digits[5] + digits[6] + "-" + digits[7] + digits[8] + digits[9] + digits[10]);
		}
		else
		    ;
	    }
	}
    }

}
