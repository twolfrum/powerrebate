package com.icf.rebate.ui.listeners;

import android.view.View;

/**
 * To be implemented by all the fragment modules.
 * 
 * @author varun.t
 * 
 */
public interface FragmentCallFlow extends BaseUICallFlow
{

    void getUiControls(View root);

    void initialize(View root);

    void bindCallbacks(View root);

}
