package com.icf.rebate.ui.util;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.StateListDrawable;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.icf.rebate.adapter.CustomSpinnerAdapter;
import com.icf.rebate.networklayer.model.FormResponseBean.Options;
import com.icf.rebate.networklayer.model.FormResponseBean.Part;
import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.customviews.CustomAutoCompleteTextView;

public class UiViewUtils
{
    public static TextView createTextView(Context context, String text, float size)
    {
	TextView view = new TextView(context);
	view.setText(text);
	view.setTextSize(size);
	LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	params.bottomMargin = 3;
	params.topMargin = 2;
	view.setLayoutParams(params);
	return view;
    }

    public static EditText createEditTextView(Context context, Options options)
    {
	EditText view = new EditText(context);
	view.setPadding(10, 10, 10, 10);
	view.setTag(options.getName());

	if (options != null && options.isMandatory() && !options.isFieldFilled() && isValidationMode(options.isItemSaved()))
	{
	    view.setBackground(context.getResources().getDrawable(R.drawable.bg_dialog_mandatory_box));
	}
	else
	{
	    view.setBackground(context.getResources().getDrawable(R.drawable.bg_dialog_box));
	}

	view.setMinimumWidth((int) context.getResources().getDimension(R.dimen.form_screen_max_width));
	
	LayoutParams params = null;
	setDataTypes(view, options.getDataType());
	// KVB multiline support
	if (options != null && options.getNumberOfLines() != null || "TV".equals(options.getInputType())) {
	    int numOfLines = LibUtils.parseInteger(options.getNumberOfLines(), 1);
	    int height = view.getLineHeight()*numOfLines;
	    int extraSpace = (int) context.getResources().getDimension(R.dimen.standard_padding);
	    height += extraSpace;
	    view.setMinHeight(height);
	    view.setMaxHeight(height);
	    view.setMinLines(numOfLines);
	    view.setGravity(Gravity.TOP|Gravity.START);
	    // TODO Hardcoded for width to be match parent for now
	    params = new LayoutParams(LayoutParams.MATCH_PARENT, height);
	    view.setImeOptions(view.getImeOptions());
	    view.setInputType(EditorInfo.TYPE_TEXT_FLAG_MULTI_LINE|EditorInfo.TYPE_TEXT_FLAG_CAP_SENTENCES|EditorInfo.TYPE_TEXT_FLAG_AUTO_COMPLETE);
	    // Weird bug seems to be always setting input type to single line after set input type
	    view.setSingleLine(false);
	    view.setImeOptions(EditorInfo.IME_ACTION_NEXT);
	} else {
	    view.setImeOptions(EditorInfo.IME_ACTION_NEXT);
	    params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	}
	
	params.bottomMargin = 10;
	view.setLayoutParams(params);
	view.setEnabled(options.isEditable());
	view.setFocusable(options.isEditable());
	view.setTextColor(context.getResources().getColor(android.R.color.black));
	

	return view;
    }
    
    public static TextView createDatePickResultView(Context context, Options options)
    {
	TextView view = new TextView(context);
	view.setPadding(10, 10, 10, 10);
	view.setTag(options.getName());
	view.setMinimumWidth((int) context.getResources().getDimension(R.dimen.form_screen_max_width));
	view.setTextColor(context.getResources().getColor(android.R.color.black));
	view.setClickable(true);

	LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	params.bottomMargin = 10;
	view.setLayoutParams(params);

	if (options != null && options.isMandatory() && !options.isFieldFilled() && isValidationMode(options.isItemSaved()))
	{
	    view.setBackground(context.getResources().getDrawable(R.drawable.bg_dialog_mandatory_box));
	}
	else
	{
	    view.setBackground(context.getResources().getDrawable(R.drawable.bg_dialog_box));
	}

	return view;
    }

    public static void setDataTypes(EditText view, String dataType)
    {
	if (dataType != null)
	{
	    if (dataType.equalsIgnoreCase("S"))
	    {
		view.setInputType(EditorInfo.TYPE_CLASS_TEXT);
	    }
	    else if (dataType.equalsIgnoreCase("I"))
	    {
		view.setInputType(EditorInfo.TYPE_CLASS_NUMBER);
	    }
	    else if (dataType.equalsIgnoreCase("D"))
	    {
		view.setInputType(EditorInfo.TYPE_CLASS_DATETIME);
	    }
	    else if (dataType.equalsIgnoreCase("ND"))
	    {
		view.setInputType(EditorInfo.TYPE_CLASS_NUMBER | EditorInfo.TYPE_NUMBER_FLAG_DECIMAL);
	    }
	}
    }

    public static Button createButtonView(Context context, String text, OnClickListener listener)
    {
	android.view.ViewGroup.LayoutParams params;
	Button view = new Button(context);
	view.setText(text);
	view.setTextColor(context.getResources().getColor(android.R.color.white));
	view.setTextSize(16);
	view.setPadding(20, 10, 20, 10);
	if (text.equalsIgnoreCase("Launch iManifold") || text.equalsIgnoreCase("LaunchiManifold"))
	{
	    params = new LayoutParams((int) context.getResources().getDimension(R.dimen.imanifold_btn_width), android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
	}
	else
	{
	    params = new LayoutParams(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
	}
	view.setLayoutParams(params);
	view.setOnClickListener(listener);
	view.setBackground(context.getResources().getDrawable(R.drawable.btn_bg_selector));
	return view;
    }

    public static TextView createButtonLikeTextView(Context context, String text, OnClickListener listener)
    {
	TextView view = new TextView(context);
	view.setText(text);
	view.setTextColor(context.getResources().getColor(android.R.color.white));
	view.setTextSize(20);
	view.setGravity(Gravity.CENTER);
	view.setPadding(10, 10, 10, 10);
	android.view.ViewGroup.LayoutParams params = new LayoutParams(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
	view.setLayoutParams(params);
	view.setOnClickListener(listener);
	view.setBackground(context.getResources().getDrawable(R.drawable.btn_bg_selector));
	return view;
    }

    public static ImageView createImageButtonView(Context context, int id)
    {
	ImageView view = new ImageView(context);
	view.setBackground(context.getResources().getDrawable(R.drawable.btn_bg_selector));
	view.setImageDrawable(context.getResources().getDrawable(id));
	int margin = context.getResources().getDimensionPixelOffset(R.dimen.form_screen_part_detail_top_margin);
	view.setPadding(margin, margin, margin, margin);
	return view;
    }

    public static ImageView createPictureButtonView(Context context, Part part, int id)
    {
	ImageView view = new ImageView(context);
	view.setMinimumWidth((int)context.getResources().getDimension(R.dimen.width_small));
	view.setMinimumHeight((int)context.getResources().getDimension(R.dimen.height_small));
	if (part != null && part.isMandatory() && (part.getImagePath() == null || TextUtils.isEmpty(part.getImagePath())) && isValidationMode(part.isItemSaved()))
	{
	    view.setBackground(context.getResources().getDrawable(R.drawable.picture_mandatory_bg_selector));
	}
	else
	{
	    view.setBackground(context.getResources().getDrawable(R.drawable.btn_bg_selector));
	}

	view.setImageDrawable(context.getResources().getDrawable(id));
	int margin = context.getResources().getDimensionPixelOffset(R.dimen.form_screen_part_detail_top_margin);
	return view;
    }

    public static ImageView createImagePreviewButtonView(Context context, int id)
    {
	ImageView view = new ImageView(context);
	view.setImageDrawable(context.getResources().getDrawable(id));
	int margin = context.getResources().getDimensionPixelOffset(R.dimen.form_screen_part_detail_top_margin);
	view.setPadding(margin, margin, margin, margin);
	return view;
    }

    public static CheckBox createCheckBoxView(Context context, String text, OnCheckedChangeListener listener)
    {
	CheckBox view = new CheckBox(context);
	view.setCompoundDrawablePadding(context.getResources().getDimensionPixelSize(R.dimen.image_padding));
	view.setCompoundDrawablesRelativeWithIntrinsicBounds(context.getResources().getDrawable(R.drawable.rebate_checkbox), null, null, null);
	view.setButtonDrawable(new StateListDrawable());
	view.setOnCheckedChangeListener(listener);
	view.setPadding(5, 10, 0, 0);
	view.setText(text);
	return view;
    }

    public static Switch createSwticherView(Context context, String text, String[] values, OnCheckedChangeListener listener)
    {
	Switch view = new Switch(context);
	view.setOnCheckedChangeListener(listener);
	view.setText(text);
	ColorDrawable cd = new ColorDrawable(context.getResources().getColor(R.color.header_bg));
	view.setThumbDrawable(cd);
	if (values != null && values.length > 1)
	{
	    view.setTextOn(values[0]);
	    view.setTextOff(values[1]);
	}

	return view;
    }

    public static Spinner createComboBoxView(Context context, String title, List<String> values, String selectedValue, OnItemSelectedListener listener)
    {
	Spinner view = new Spinner(context, Spinner.MODE_DROPDOWN);
	view.setPadding(0, 0, 0, 0);

	view.setLayoutParams(new LayoutParams(android.view.ViewGroup.LayoutParams.MATCH_PARENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT));
	CustomSpinnerAdapter equipmentAdapter = new CustomSpinnerAdapter(context, 0, values);
	view.setAdapter(equipmentAdapter);
	// view.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, values));
	view.setPrompt(title);

	view.setPopupBackgroundDrawable(context.getResources().getDrawable(R.drawable.combo_box_bg));
	view.setOnItemSelectedListener(listener);
	view.setDropDownWidth(android.view.ViewGroup.LayoutParams.MATCH_PARENT);
	view.setDropDownVerticalOffset(context.getResources().getDimensionPixelOffset(R.dimen.form_screen_equipment_combo_box_dropdown_vertical_offset));

	if (selectedValue != null && !TextUtils.isEmpty(selectedValue))
	{
	    int selectedPos = values.indexOf(selectedValue);
	    if (selectedPos > -1)
	    {
		view.setSelection(selectedPos);
	    }
	}
	return view;
    }

    public static boolean isValidationMode(boolean itemSaved)
    {
	// itemSaved current not used
	return (RebateManager.getInstance().getFormBean().getEditMode() == AppConstants.FORM_EDIT_MODE.VALIDATION_MODE.getMode() || RebateManager.getInstance().getFormBean().getEditMode() == AppConstants.FORM_EDIT_MODE.EDIT_MODE.getMode());
    }

    public static AutoCompleteTextView createAutoCompletion(Context context, Options options, List<String> autosuggestList, OnFocusChangeListener listener)
    {
	CustomAutoCompleteTextView view = new CustomAutoCompleteTextView(context);
	view.setPadding(10, 10, 10, 10);
	view.setLayoutParams(new LayoutParams(android.view.ViewGroup.LayoutParams.MATCH_PARENT, context.getResources().getDimensionPixelOffset(R.dimen.form_drobdown_height)));
	ArrayAdapter<String> equipmentAdapter = new CustomSpinnerAdapter(context, 0, autosuggestList);
	view.setAdapter(equipmentAdapter);
	// view.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, values));
	view.setDropDownBackgroundDrawable(context.getResources().getDrawable(R.drawable.combo_box_bg));
	view.setOnFocusChangeListener(listener);
	view.setDropDownWidth(android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
	view.setDropDownVerticalOffset(context.getResources().getDimensionPixelOffset(R.dimen.form_screen_equipment_auto_comnplete_dropdown_vertical_offset));
	view.setSingleLine();
	view.setImeOptions(EditorInfo.IME_ACTION_NEXT);
	view.dismissDropDown();
	if (isValidationMode(options.isItemSaved()) && options.isMandatory() && !options.isFieldFilled())
	{
	    view.setBackground(context.getResources().getDrawable(R.drawable.bg_dialog_mandatory_box));
	}
	else
	{
	    view.setBackground(context.getResources().getDrawable(R.drawable.combo_box_bg));
	}

	return view;
    }

    public static ViewGroup createPartCategory(Context context)
    {
	LinearLayout layout = new LinearLayout(context);

	return layout;
    }

    public static ViewGroup createSpinnerBg(Context context, View view, Options options)
    {
	RelativeLayout layout = new RelativeLayout(context);
	android.view.ViewGroup.LayoutParams params = new LayoutParams(android.view.ViewGroup.LayoutParams.MATCH_PARENT, context.getResources().getDimensionPixelOffset(R.dimen.form_drobdown_height));
	layout.setLayoutParams(params);

	ImageView imageView = new ImageView(context);
	imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.down_arrow));
	RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
	param.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
	param.addRule(RelativeLayout.CENTER_VERTICAL);
	// imageView.setScaleType(ScaleType.MATRIX);
	int padding = context.getResources().getDimensionPixelOffset(R.dimen.form_screen_part_detail_top_margin);
	imageView.setPadding(padding, padding, padding, padding);

	imageView.setLayoutParams(param);

	layout.addView(view);
	layout.addView(imageView);

	return layout;
    }

    public static void addSubView(ViewGroup group, View child)
    {
	if (group != null && child != null)
	{
	    group.addView(child);
	}
    }

    public static ViewGroup createHorizontalView(Context context, View child1, View child2)
    {
	LinearLayout group = new LinearLayout(context);
	group.setWeightSum(100);
	LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
	if (context.getResources().getBoolean(R.bool.isPhoneLayout) || context.getResources().getBoolean(R.bool.is7inchtablet))
	{
	    params.weight = 20;
	}
	else
	{
	    params.weight = 10;
	}
	// params.leftMargin = 10;
	child1.setLayoutParams(params);
	group.addView(child1);
	params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
	params.leftMargin = 10;
	if (context.getResources().getBoolean(R.bool.isPhoneLayout) || context.getResources().getBoolean(R.bool.is7inchtablet))
	{
	    params.weight = 80;
	}
	else
	{
	    params.weight = 90;
	}
	params.gravity = Gravity.CENTER_VERTICAL;
	int margin = context.getResources().getDimensionPixelOffset(R.dimen.form_screen_part_detail_top_margin);
	child2.setPadding(0, margin, 0, margin);
	child2.setLayoutParams(params);
	group.addView(child2);
	return group;
    }

    public static ViewGroup createHorizontalViewForAddDelete(Context context, View child1, View child2)
    {
	LinearLayout group = new LinearLayout(context);
	group.setWeightSum(100);
	LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
	if (context.getResources().getBoolean(R.bool.isPhoneLayout) || context.getResources().getBoolean(R.bool.is7inchtablet))
	{
	    params.weight = 30;
	}
	else
	{
	    params.weight = 15;
	}
	params.leftMargin = 10;
	child1.setLayoutParams(params);
	group.addView(child1);
	params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
	params.leftMargin = 10;
	if (context.getResources().getBoolean(R.bool.isPhoneLayout) || context.getResources().getBoolean(R.bool.is7inchtablet))
	{
	    params.weight = 70;
	}
	else
	{
	    params.weight = 85;
	}
	params.gravity = Gravity.CENTER_VERTICAL;
	int margin = context.getResources().getDimensionPixelOffset(R.dimen.form_screen_part_detail_top_margin);
	child2.setPadding(0, margin, 0, margin);
	child2.setLayoutParams(params);
	group.addView(child2);
	return group;
    }

    public static ViewGroup createHorizontalViewgroup(Context context)
    {
	LinearLayout group = new LinearLayout(context);
	LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
	group.setLayoutParams(params);

	return group;
    }

    public static ViewGroup addChildrenToViewGroup(Context context, ViewGroup group, View child)
    {
	LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	int childCount = group.getChildCount();

	if (childCount > 0)
	{
	    int margin = context.getResources().getDimensionPixelOffset(R.dimen.form_screen_part_detail_top_margin);
	    child.setPadding(margin, margin, margin, margin);
	    params.leftMargin = margin * 2;

	}

	params.gravity = Gravity.RIGHT;
	child.setLayoutParams(params);

	group.addView(child);

	return group;
    }

    public static ViewGroup createHorizontalViewFitContent(Context context, View child1, View child2)
    {
	LinearLayout group = new LinearLayout(context);
	LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	params.topMargin = context.getResources().getDimensionPixelOffset(R.dimen.form_screen_part_detail_top_margin);
	params.gravity = Gravity.CENTER_HORIZONTAL;
	group.setLayoutParams(params);

	group.addView(child1);

	group.addView(child2);
	return group;
    }

    public static LinearLayout createPartLayout(Context context, int background)
    {
	LinearLayout group = new LinearLayout(context);
	group.setBackground(context.getResources().getDrawable(background));
	group.setPadding(20, 20, 20, 20);
	LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
	params.bottomMargin = 20;
	params.gravity = Gravity.CENTER_HORIZONTAL;
	group.setLayoutParams(params);
	group.setOrientation(LinearLayout.VERTICAL);
	return group;
    }

    public static LinearLayout createLinearLayout(Context context)
    {
	LinearLayout group = new LinearLayout(context);
	LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	params.gravity = Gravity.CENTER_HORIZONTAL;
	params.topMargin = context.getResources().getDimensionPixelOffset(R.dimen.form_screen_part_detail_top_margin);
	group.setLayoutParams(params);
	return group;
    }

    public static View getViewByTag(ViewGroup group, String tag)
    {
	View view = null;

	if (group != null && tag != null)
	{
	    for (int i = 0; i < group.getChildCount(); i++)
	    {
		View child = group.getChildAt(i);
		if (child != null)
		{
		    if (child instanceof ViewGroup)
		    {
			ViewGroup childGroup = (ViewGroup) child;
			for (int j = 0; j < childGroup.getChildCount(); j++)
			{
			    View child2 = childGroup.getChildAt(j);
			    if (child2 != null && child.getTag() instanceof Part)
			    {
				Part partTag = (Part) child.getTag();
				if (partTag.getId() != null && partTag.getId().equalsIgnoreCase(tag))
				{
				    view = child;
				    break;
				}
			    }
			}

			/*
			 * if (view != null) { break; }
			 */
		    }
		    else if (child.getTag() != null && child.getTag() instanceof String && ((String) child.getTag()).equalsIgnoreCase(tag))
		    {
			view = child;
			break;
		    }
		}
	    }
	}
	return view;
    }

    public static ArrayList<View> getViewsByTag(ViewGroup group, String tag)
    {

	ArrayList<View> views = new ArrayList<View>();
	if (group != null && tag != null)
	{
	    for (int i = 0; i < group.getChildCount(); i++)
	    {
		View child = group.getChildAt(i);
		if (child != null)
		{
		    if (child instanceof ViewGroup)
		    {
			ViewGroup childGroup = (ViewGroup) child;
			for (int j = 0; j < childGroup.getChildCount(); j++)
			{
			    View child2 = childGroup.getChildAt(j);
			    if (child2 != null && child.getTag() instanceof Part)
			    {
				Part partTag = (Part) child.getTag();
				if (partTag.getId() != null && partTag.getId().equalsIgnoreCase(tag))
				{

				    views.add(child);
				    // break;
				}
			    }
			}

			/*
			 * if (view != null) { break; }
			 */
		    }
		    else if (child.getTag() != null && child.getTag() instanceof String && ((String) child.getTag()).equalsIgnoreCase(tag))
		    {
			views.add(child);
			// break;
		    }
		}
	    }
	}
	return views;
    }

    public static void setupViewsBySystemType(ViewGroup group, String systemTypeId, String selectedValue)
    {

	if (group != null && systemTypeId != null && selectedValue != null)
	{
	    for (int i = 0; i < group.getChildCount(); i++)
	    {
		View child = group.getChildAt(i);
		if (child != null)
		{
		    if (child instanceof ViewGroup)
		    {
			ViewGroup childGroup = (ViewGroup) child;
			for (int j = 0; j < childGroup.getChildCount(); j++)
			{
			    View child2 = childGroup.getChildAt(j);
			    if (child2 != null && child.getTag() instanceof String)
			    {
				String partTag = (String) child.getTag();
				if (partTag.startsWith(systemTypeId) && partTag.contains("_"))
				{
				    if (partTag.split("_")[1].equalsIgnoreCase(selectedValue))
				    {
					child.setVisibility(View.VISIBLE);
				    }
				    else
				    {
					child.setVisibility(View.GONE);
				    }
				    break;
				}
			    }
			}
		    }
		}
	    }
	}
    }

}
