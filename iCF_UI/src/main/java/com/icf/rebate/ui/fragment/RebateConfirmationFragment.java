package com.icf.rebate.ui.fragment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.Chronometer;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.icf.ameren.rebate.ui.BuildConfig;
import com.icf.rebate.adapter.RebateConfirmationAdapter;
import com.icf.rebate.app.model.ConfirmationMode;
import com.icf.rebate.networklayer.model.AppDetails;
import com.icf.rebate.networklayer.model.CustomerInfo;
import com.icf.rebate.networklayer.model.FormResponseBean;
import com.icf.rebate.networklayer.model.PendingJobItem;
import com.icf.rebate.networklayer.model.RebateConfirmationDetails;
import com.icf.rebate.networklayer.model.RebateConfirmationItem;
import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.ui.RootActivity;
import com.icf.rebate.ui.SignatureFragment;
import com.icf.rebate.ui.controller.MandatoryTextWatcher;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.dialog.DialogListener;
import com.icf.rebate.ui.dialog.ICFDialogFragment;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.DateFormatter;
import com.icf.rebate.ui.util.FileUtil;
import com.icf.rebate.ui.util.ICFLogger;
import com.icf.rebate.ui.util.UiUtil;

public class RebateConfirmationFragment extends Fragment implements OnItemClickListener, DialogListener {

    public static final String TAG = RebateConfirmationFragment.class.getSimpleName();

    private final String ITEM_INSTANCE_KEY = "itemInstanceKey";
    // private TextView rebateConfirmationHeader;

    // private ListView rebateConfirmationList;

    // private ArrayList<ConfirmationMode> modeList;

    // private RebateConfirmationAdapter adapter;

    private View checkBoxView;

    private RootActivity rootAct;

    private MediaRecorder recorder;

    private MediaPlayer player;

    private Chronometer mChronometer;

    private int mTotalDuration;

    private String mTotalDurationStr;

    private int mCurrentDuration;

    private SeekBar mSeekbar;

    private TextView mCurrentPositionTxt;

    private TextView mTotalDurationTxt;

    private ImageView btnPlay;

    private ImageView btnVoice;

    private View playProgressVG;

    private ConfirmationMode signatureMode;

    private ConfirmationMode voiceMode;

    private OnClickListener adapterClickListener;

    private ImageButton btnClear;

    private ImageButton btnDone;

    private ICFDialogFragment voiceDialog;

    private View signatureFooter;

    private int jobState;

    private String jobId;

    protected int selectedDeletePosition;

    private TextView btnRebateSubmit;

    private ImageView imgPreviewInvoice;

    private ImageView imgCameraInvoice;

    private ICFDialogFragment photoDialog;

    private AppDetails appDetails;

    private EditText userComments;

    private View confirmationPictureItems;

    private RebateConfirmationItem mCurrentItem;

    private CheckBox mDocumentLater;

    private EditText mOrderNumber;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            mCurrentItem = (RebateConfirmationItem) savedInstanceState.getSerializable(ITEM_INSTANCE_KEY);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(ITEM_INSTANCE_KEY, mCurrentItem);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.rebate_confirmation_screen, null);

        btnRebateSubmit = (TextView) view.findViewById(R.id.rebate_confirmation_submit_btn);
        // imgPreviewInvoice = (ImageView) view.findViewById(R.id.previewInvoice);

        jobId = RebateManager.getInstance().getFormBean().getCustomerInfo().getJobId();
        jobState = RebateManager.getInstance().getFormBean().getCustomerInfo().getState();

        // imgCameraInvoice = (ImageView) view.findViewById(R.id.cameraInvoice);
        userComments = (EditText) view.findViewById(R.id.rebate_confirmation_comments);
        confirmationPictureItems = view.findViewById(R.id.container_picture_items);

        // rebateConfirmationHeader = (TextView) view.findViewById(R.id.rebate_confirmation_header);
        // rebateConfirmationList = (ListView) view.findViewById(R.id.rebate_confirmation_list);
        // rebateConfirmationList.setOnItemClickListener(this);
        // rebateConfirmationList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        // modeList = getConfirmationList();

        // adapter = new RebateConfirmationAdapter(getActivity(), 0, modeList);
        // adapter.setAdapterListener(adapter);
        // adapter.setOnClickListener(getAdapterOnClickListener());
        // rebateConfirmationList.setAdapter(adapter);

        // setListViewHeight();

        appDetails = RebateManager.getInstance().getFormBean().getAppDetails();
        // getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        if (appDetails != null && appDetails.getComments() != null && appDetails.getComments().length() > 0) {
            userComments.setText(appDetails.getComments().toString());
        }

	/*
     * String fileName = FileUtil.getJobDirectory(jobId) + "/" + AppConstants.INVOICE_FILE_NAME; if (!FileUtil.exists(fileName)) {
	 * imgPreviewInvoice.setVisibility(View.INVISIBLE); } else { setInvoiceDrawable(fileName); }
	 */
        btnRebateSubmit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
		/* ------------------------------ Comment this whole block for no validation ---------------------------- */

                boolean showPopup = false;
                updateAppDetails();

                if (mDocumentLater != null && mDocumentLater.isChecked()) {
                    showPopup = false;
                } else {
                    // Check for required invoice image
                    RebateConfirmationDetails rebateConfirmationDetails = RebateManager.getInstance().getFormBean().getRebateConfirmationDetails();
                    if (rebateConfirmationDetails != null) {
                        List<RebateConfirmationItem> items = rebateConfirmationDetails.getItems();
                        if (items != null && hasInvoiceItem(items) && items.size() > 0) {
                            for (RebateConfirmationItem item : items) {
                                if ("rebateConfirmationInvoice".equals(item.getId())) {
                                    if (item.getNumberOfImages() == 0) showPopup = true;
                                }

                                if (AppConstants.REBATE_CONFIRMATION_ORDER_NUMBER.equals(item.getId())) {
                                    if (item.isMandatory() && TextUtils.isEmpty(mOrderNumber.getText())) {
                                        showPopup = true;
                                    }
                                }
                            }
                        }
                    }
                }
//		if (FileUtil.exists(FileUtil.getJobDirectory(jobId) + "/" + AppConstants.SIGNATURE_FILE_NAME))
//		{
//		    showPopup = false;
//		}
//
//		if (FileUtil.exists(FileUtil.getJobDirectory(jobId) + "/" + AppConstants.VOICE_FILE_NAME))
//		{
//		    showPopup = false;
//		}

                if (showPopup) {
                    UiUtil.showError(getActivity(), getString(R.string.error_title), getString(R.string.no_consent_message));
                } else {
                    // submission
                    doSubmission();
                }
		/* ------------------------------------------------------------------------------------------------------- */
                // Uncomment this for no validation
                // doSubmission();
            }
        });

	/*
	 * imgCameraInvoice.setOnClickListener(new OnClickListener() {
	 * 
	 * @Override public void onClick(View v) {
	 * 
	 * // Tell the host activity to swap out this frag for the camera frag AlertDialog dialog = new
	 * AlertDialog.Builder(getView().getContext()).setPositiveButton("Take Picture", new DialogInterface.OnClickListener() { public void
	 * onClick(DialogInterface dialog, int which) { dialog.cancel(); String fileName = FileUtil.getJobDirectory(jobId) + "/" +
	 * AppConstants.INVOICE_FILE_NAME; Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
	 * takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(fileName))); startActivityForResult(takePictureIntent,
	 * AppConstants.CAPTURE_PHOTO_REQUEST_CODE); } }).setNegativeButton("Choose existing", new DialogInterface.OnClickListener() { public void
	 * onClick(DialogInterface dialog, int which) { dialog.cancel(); Intent i = new Intent(Intent.ACTION_PICK,
	 * android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI); startActivityForResult(i, AppConstants.REQUEST_CODE_GALLERY); }
	 * }).setIcon(null).show(); } });
	 */

        // rebateConfirmationHeader - current utility company name
        // updateCompanyDetails();
        rootAct = (RootActivity) getActivity();
        if (rootAct != null) {
            rootAct.updateTitle(getResources().getString(R.string.rebate_confirmation_txt));
        }

        addInvoiceView((ViewGroup) view.findViewById(R.id.container_picture_items)); /* Special case invoice view should always be added even if non-existent in the form_data.json */
        addConfirmationItems((ViewGroup) view.findViewById(R.id.container_picture_items)); /* Add rebate confirmation items from form_data.json */

        if (appDetails != null && !TextUtils.isEmpty((appDetails.getOrderNumber()))) {
            mOrderNumber.setText(appDetails.getOrderNumber());
        }

        if (mDocumentLater != null && appDetails != null) {
            mDocumentLater.setChecked(appDetails.isDocumentLater());
            handleDocumentLaterRequest(appDetails.isDocumentLater());
        }

        return view;
    }

    /*
     * public void setListViewHeight() { int pixel = getResources().getDimensionPixelOffset(R.dimen.rebate_confirmation_screen_list_height); int listHeight =
     * adapter.getListObjects().size() * pixel; ViewGroup.LayoutParams lp = rebateConfirmationList.getLayoutParams(); lp.height = listHeight +
     * rebateConfirmationList.getDividerHeight(); rebateConfirmationList.setLayoutParams(lp); }
     */

    private List<RebateConfirmationItem> getRebateConfirmationItems() {

        if (RebateManager.getInstance().getFormBean() != null) {
            RebateConfirmationDetails rebateConfirmationDetails = RebateManager.getInstance().getFormBean().getRebateConfirmationDetails();
            if (rebateConfirmationDetails != null) {
                return rebateConfirmationDetails.getItems();
            }
        }
        return null;
    }

    private boolean hasInvoiceItem(List<RebateConfirmationItem> items) {
        if (items != null) {
            for (RebateConfirmationItem item : items) {
                if ("rebateConfirmationInvoice".equals(item.getId())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Add the view for invoice collection, always displayed on this screen
     *
     * @param vg container
     */
    private void addInvoiceView(ViewGroup vg) {
        // Check to see if invoice view is in the items list, if not add the default
        if (hasInvoiceItem(getRebateConfirmationItems())) {
            return;
        } else {
            // Make a default invoice item
            RebateConfirmationItem item = new RebateConfirmationItem("rebateConfirmationInvoice",
                    "Rebate Confirmation Invoice", "invoice", "picture");
            RebateConfirmationDetails rebateConfirmationDetails =
                    RebateManager.getInstance().getFormBean().getRebateConfirmationDetails();
            if (rebateConfirmationDetails != null) {
                List<RebateConfirmationItem> items = rebateConfirmationDetails.getItems();
                if (items != null) {
                    items.add(0, item);
                }
            }
        }
    }

    /**
     * This method iterates through a List of {@link:RebateConfirmationItem} and adds the appropriate ui components for each item.
     *
     * @param vg - container to add picture items to
     */
    private void addConfirmationItems(ViewGroup vg) {
        if (vg != null && RebateManager.getInstance().getFormBean() != null) {
            RebateConfirmationDetails rebateConfirmationDetails = RebateManager.getInstance().getFormBean().getRebateConfirmationDetails();
            if (rebateConfirmationDetails != null) {
                List<RebateConfirmationItem> items = rebateConfirmationDetails.getItems();
                if (items != null && items.size() > 0) {
                    LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                    for (RebateConfirmationItem item : items) {
                        if (AppConstants.REBATE_CONFIM_DETAIL_TYPE_PICTURE.equals(item.getType())) {
                            View v = inflater.inflate(R.layout.tmpl_capture_image, vg, false);
                            // Set label
                            TextView label = (TextView) v.findViewById(R.id.lbl_capture);
                            label.setText(item.getLabel() != null ? item.getLabel() : "");
                            // Show first thumbnail if applicable
                            String fileName = FileUtil.getJobDirectory(jobId) + "/" + item.getFilename() + "_" + item.getNumberOfImages() + "_.jpg";
                            imgPreviewInvoice = (ImageView) v.findViewById(R.id.iv_capture_thumbnail);
                            imgCameraInvoice = (ImageView) v.findViewById(R.id.iv_capture_camera);
                            imgPreviewInvoice.setTag(item); // Needed for the preview dialog to find file
                            if (!FileUtil.exists(fileName)) {
                                imgPreviewInvoice.setVisibility(View.INVISIBLE);
                                if ("rebateConfirmationInvoice".equals(item.getId())) {
                                    imgCameraInvoice.setBackground(getResources().getDrawable(R.drawable.picture_mandatory_bg_selector));
                                }
                            } else {
                                setInvoiceDrawable(fileName);
                            }
                            addThumbnailListener(imgPreviewInvoice);
                            addPictureListener(v, item);
                            vg.addView(v);
                        } else if (AppConstants.REBATE_CONFIRMATION_DETAIL_TYPE_MULTILINE.equals(item.getType())) {
                            // TODO for now this is only changing the label of the comments field later this should make these dynamically to
                            View parentView = (View) vg.getParent();
                            TextView tvHeaderComments = (TextView) parentView.findViewById(R.id.comments_txt);
                            if (tvHeaderComments != null) {
                                tvHeaderComments.setText(item.getLabel());
                            }
                        } else if (AppConstants.REBATE_CONFIRMATION_DOCUMENT_LATER.equals(item.getId())) {
                            View parentView = (View) vg.getParent();
                            mDocumentLater = (CheckBox) parentView.findViewById(R.id.document_later);
                            if (mDocumentLater != null) {
                                mDocumentLater.setVisibility(View.VISIBLE);
                                mDocumentLater.setText(item.getLabel() != null ? item.getLabel() : "");
                                mDocumentLater.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                                    @Override
                                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                        handleDocumentLaterRequest(isChecked);
                                    }
                                });
                            }
                        } else if (AppConstants.REBATE_CONFIRMATION_ORDER_NUMBER.equals(item.getId())) {
                            View parentView = (View) vg.getParent();
                            mOrderNumber = (EditText) parentView.findViewById(R.id.order_number);
                            if (mOrderNumber != null) {
                                ((ViewGroup)mOrderNumber.getParent()).setVisibility(View.VISIBLE);
                                if (!TextUtils.isEmpty(item.getLabel())) {
                                    ((TextView)parentView.findViewById(R.id.order_number_txt))
                                            .setText(item.getLabel());
                                }
                                //add red box as needed
                                if (item.isMandatory()) {
                                    mOrderNumber.addTextChangedListener(new MandatoryTextWatcher(mOrderNumber));
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Adds listener to thumbnail image view
     */
    private void addThumbnailListener(View v) {
        v.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                mCurrentItem = (RebateConfirmationItem) v.getTag(); // Get the item tagged on this view so we know which image file path to get
                photoDialog = ICFDialogFragment.newDialogFrag(R.layout.image_preview_popup);
                photoDialog.setDialogListener(RebateConfirmationFragment.this);
                photoDialog.show(getActivity().getSupportFragmentManager(), "PreviewDialog");
            }
        });
    }

    /**
     * Adds listener to picture item
     *
     * @param v    - view to put listener on
     * @param item - holding details relating to this view
     */
    private void addPictureListener(final View v, final RebateConfirmationItem item) {
        v.findViewById(R.id.iv_capture_camera).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View imageView) {

                // Tell the host activity to swap out this frag for the camera frag
                AlertDialog dialog = new AlertDialog.Builder(getView().getContext()).setPositiveButton("Take Picture", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        mCurrentItem = item;
                        imgCameraInvoice = (ImageView) v.findViewById(R.id.iv_capture_camera);
                        imgPreviewInvoice = (ImageView) v.findViewById(R.id.iv_capture_thumbnail);
                        String fileName = FileUtil.getJobDirectory(jobId) + "/" + item.getFilename() + "_" + (item.getNumberOfImages() + 1) + "_.jpg";
                        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                FileProvider.getUriForFile(RebateConfirmationFragment.this.getActivity(),
                                        BuildConfig.APPLICATION_ID + ".fileprovider",
                                        new File(fileName)));
                        startActivityForResult(takePictureIntent, AppConstants.CAPTURE_PHOTO_REQUEST_CODE);
                    }
                }).setNegativeButton("Choose existing", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        mCurrentItem = item;
                        imgCameraInvoice = (ImageView) v.findViewById(R.id.iv_capture_camera);
                        imgPreviewInvoice = (ImageView) v.findViewById(R.id.iv_capture_thumbnail);
                        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, AppConstants.REQUEST_CODE_GALLERY);
                    }
                }).setIcon(null).show();
            }
        });
    }

    private void handleDocumentLaterRequest(boolean later) {
        if (!later) {
            confirmationPictureItems.setVisibility(View.VISIBLE);
            return;
        }

        //confirm user's intentions if picture documents have already been saved
        List<RebateConfirmationItem> items = getRebateConfirmationItems();
        if (items != null && items.size() > 0) {
            for (RebateConfirmationItem item : items) {
                if (item.getNumberOfImages() > 0) {
                    UiUtil.showConfirmation(getActivity(), getString(R.string.dialog_title_document_later),
                            getString(R.string.dialog_body_document_later),
                            R.string.alert_continue, R.string.alert_cancel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (which == DialogInterface.BUTTON_POSITIVE) {
                                        deleteConfirmationDocs();
                                        confirmationPictureItems.setVisibility(View.GONE);
                                    }
                                }
                            });
                    return;
                }
            }
        }
        confirmationPictureItems.setVisibility(View.GONE);
        //update checkBox value in JSON????
        //RebateManager.getInstance().updateFormState(null);
    }

    private void deleteConfirmationDocs() {
        List<RebateConfirmationItem> items = getRebateConfirmationItems();
        for (RebateConfirmationItem item : items) {
            for (int i = 1; i <= item.getNumberOfImages(); i++) {
                File fileToDelete = new File(getImageFileName(item.getFilename(), i));
                fileToDelete.delete();
            }
            item.setNumberOfImages(0);
        }
        RebateManager.getInstance().updateFormState(null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        CustomerInfo cInfo = RebateManager.getInstance().getFormBean().getCustomerInfo();
        if (UiUtil.isJobCompleted(cInfo)) {
            ArrayList<View> list = new ArrayList<View>();
            list.add(btnRebateSubmit);
            UiUtil.setEnableView((ViewGroup) view, false, list);
        }
    }

    boolean isformValidationSuccess = false;

    private void doSubmission() {
        final PendingJobItem item = new PendingJobItem();
        FormResponseBean bean = RebateManager.getInstance().getFormBean();
        item.setBean(bean);
        item.setCustomerName(bean.getCustomerInfo().getFirstName() + " " + bean.getCustomerInfo().getLastName());
        item.setCustomerAddress(bean.getCustomerInfo().getCustomerAddress());
        item.setDate(DateFormatter.getFormattedDate(System.currentTimeMillis()));
        item.setLastModified(System.currentTimeMillis());
        item.setFolderPath(FileUtil.getJobDirectory(bean.getCustomerInfo().getJobId()));
        ICFLogger.d(TAG, "PendingJobItem Name : " + bean.getCustomerInfo().getFirstName() + " " + bean.getCustomerInfo().getLastName() + "date:" + DateFormatter.getFormattedDate(System.currentTimeMillis()) + " JOB ID :" + bean.getCustomerInfo().getJobId());
        isformValidationSuccess = RebateManager.getInstance().doFormValidation(item);
        // Uncomment this for validation
        if (isformValidationSuccess)
        // Uncomment this for no validation
        // if (true)
        {
            RebateManager.getInstance().updateFormState(bean, CustomerInfo.COMPLETED_JOB, new Runnable() {

                @Override
                public void run() {
                    RebateManager.getInstance().doSubmission(item, R.string.user_consent_screen_network_mess);
                }
            });
            rootAct.updateTitle(getResources().getString(R.string.dashboard_txt));
            getActivity().getSupportFragmentManager().popBackStackImmediate("dashboard", 0);
        }
        ICFLogger.d(TAG, "isformValidationSuccess" + isformValidationSuccess);
    }

    private void displayDeleteConfirmation() {
        UiUtil.showConfirmation(getActivity(), getString(R.string.hdr_confirmation), getString(R.string.delete_confirmation), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_POSITIVE) {
                    String fileName = null;
                    if (selectedDeletePosition == 0) {
                        fileName = FileUtil.getJobDirectory(jobId) + "/" + AppConstants.SIGNATURE_FILE_NAME;
                        ICFLogger.d(TAG, "DeleteSignature :" + fileName);
                    } else if (selectedDeletePosition == 1) {
                        fileName = FileUtil.getJobDirectory(jobId) + "/" + AppConstants.VOICE_FILE_NAME;
                        ICFLogger.d(TAG, "DeleteVoiceFile :" + fileName);
                    }
                    FileUtil.deleteFile(fileName);
                    // refreshList(false);
                }
            }
        });
    }

    private OnClickListener getAdapterOnClickListener() {
        if (adapterClickListener == null) {
            adapterClickListener = new OnClickListener() {

                @Override
                public void onClick(View v) {
                    int id = v.getId();
                    CustomerInfo cInfo = RebateManager.getInstance().getFormBean().getCustomerInfo();
                    if (UiUtil.isJobCompleted(cInfo)) {
                        return;
                    }
                    if (id == R.id.trash_icon) {
                        int position = (Integer) v.getTag();
                        selectedDeletePosition = position;
                        displayDeleteConfirmation();
                    }
                }
            };
        }
        return adapterClickListener;
    }

    /*
     * public void refreshList(boolean updateJobState) {
     * 
     * modeList = getConfirmationList();
     * 
     * adapter = new RebateConfirmationAdapter(getActivity(), 0, modeList); adapter.setAdapterListener(adapter); rebateConfirmationList.setAdapter(adapter);
     * adapter.setOnClickListener(getAdapterOnClickListener()); if (updateJobState) { updateComments(); RebateManager.getInstance().updateFormState(null,
     * jobState); } }
     */

    public ArrayList<ConfirmationMode> getConfirmationList() {
        ArrayList<ConfirmationMode> rebateConfirmationList = new ArrayList<ConfirmationMode>();
        signatureMode = new ConfirmationMode();
        signatureMode.setImageResId(R.drawable.confirmation_signature_icon);
        signatureMode.setModeName(getActivity().getString(R.string.signature_txt));
        if (FileUtil.exists(FileUtil.getJobDirectory(jobId) + "/" + AppConstants.SIGNATURE_FILE_NAME)) {
            signatureMode.setSelected(true);
        }
        rebateConfirmationList.add(signatureMode);

        voiceMode = new ConfirmationMode();
        voiceMode.setImageResId(R.drawable.confirmation_mic_icon);
        voiceMode.setModeName(getActivity().getString(R.string.voice_agreement));
        if (FileUtil.exists(FileUtil.getJobDirectory(jobId) + "/" + AppConstants.VOICE_FILE_NAME)) {
            voiceMode.setSelected(true);
        }
        rebateConfirmationList.add(voiceMode);

        if ((signatureMode.isSelected() || voiceMode.isSelected()) && isformValidationSuccess) {
            jobState = CustomerInfo.COMPLETED_JOB;
        }
        return rebateConfirmationList;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (position == 0 && (signatureMode.isSelected() || (!signatureMode.isSelected() && !voiceMode.isSelected()))) {
            checkBoxView = view;
            SignatureFragment signatureFrag = new SignatureFragment();
            signatureFrag.show(getActivity().getSupportFragmentManager(), "signatureFragment");
            getActivity().getSupportFragmentManager().executePendingTransactions();
            // getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout, signatureFrag).addToBackStack(null).commit();
        } else if (position == 1 && (voiceMode.isSelected() || (!signatureMode.isSelected() && !voiceMode.isSelected()))) {
            displayVoiceAgreement();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void displayVoiceAgreement() {
        voiceDialog = ICFDialogFragment.newDialogFrag(R.layout.voiceagreement);
        voiceDialog.setDialogListener(this);
        voiceDialog.show(getActivity().getSupportFragmentManager(), "voiceAgreement");
        getActivity().getSupportFragmentManager().executePendingTransactions();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1000) {
            if (checkBoxView != null) {
                CheckBox cb = (CheckBox) checkBoxView.findViewById(R.id.list_check_box);
                if (!cb.isChecked()) {
                    cb.setChecked(true);
                }

            }
        }

        if (requestCode == AppConstants.CAPTURE_PHOTO_REQUEST_CODE && resultCode == FragmentActivity.RESULT_OK) {
            if (mCurrentItem != null) {
                if (mCurrentItem.getNumberOfImages() == 0) { // add image if first
                    loadNewPicture();
                } else {
                    UiUtil.showConfirmation(getActivity(), getString(R.string.dialog_title_add_or_replace), getString(R.string.dialog_body_add_or_replace), R.string.dialog_add, R.string.dialog_replace, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (which == DialogInterface.BUTTON_POSITIVE) {
                                loadNewPicture();
                            } else {
                                int numOfImgJustCaptured = mCurrentItem.getNumberOfImages() + 1; // number of the just captured image
                                int numOfPreviouslySaveImg = numOfImgJustCaptured - 1; // all the previously saved images
                                while (numOfPreviouslySaveImg > 0) {
                                    File fileToDelete = new File(getImageFileName(mCurrentItem.getFilename(), mCurrentItem.getNumberOfImages()));
                                    fileToDelete.delete();
                                    mCurrentItem.setNumberOfImages(--numOfPreviouslySaveImg);
                                }
                                new File(getImageFileName(mCurrentItem.getFilename(), numOfImgJustCaptured)).renameTo(new File(getImageFileName(mCurrentItem.getFilename(), 1))); // make																				  // 1

                                loadNewPicture();
                            }
                            RebateManager.getInstance().updateFormState(null);
                        }

                    });
                }
                imgCameraInvoice.setBackground(getResources().getDrawable(R.drawable.btn_bg_selector));
            }

        }
        if (requestCode == AppConstants.REQUEST_CODE_GALLERY && resultCode == FragmentActivity.RESULT_OK) {
            // Need to move the selected image to job directory
            if (mCurrentItem != null) {
                if (mCurrentItem.getNumberOfImages() == 0) { // add if first
                    copyAndLoadNewPicture(data);
                } else {
                    UiUtil.showConfirmation(getActivity(), getString(R.string.dialog_title_add_or_replace), getString(R.string.dialog_body_add_or_replace), R.string.dialog_add, R.string.dialog_replace, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (which == DialogInterface.BUTTON_POSITIVE) {
                                copyAndLoadNewPicture(data);
                            } else {
                                int numOfPreviouslySaveImg = mCurrentItem.getNumberOfImages();
                                while (numOfPreviouslySaveImg > 0) {
                                    File fileToDelete = new File(getImageFileName(mCurrentItem.getFilename(), mCurrentItem.getNumberOfImages()));
                                    fileToDelete.delete();
                                    mCurrentItem.setNumberOfImages(--numOfPreviouslySaveImg);
                                }
                                copyAndLoadNewPicture(data);
                            }
                            RebateManager.getInstance().updateFormState(null);
                        }

                    });
                }
                imgCameraInvoice.setBackground(getResources().getDrawable(R.drawable.btn_bg_selector));
            }
        }
    }

    private void loadNewPicture() {
        int picNumber = mCurrentItem.getNumberOfImages() + 1;
        String filePath = getImageFileName(mCurrentItem.getFilename(), picNumber);
        mCurrentItem.setNumberOfImages(picNumber);
        updateInvoiceImg(filePath);
        setInvoiceDrawable(filePath);
    }

    private void copyAndLoadNewPicture(Intent data) {
        String from = UiUtil.getPathFromUri(getActivity(), data.getData());
        int picNumber = mCurrentItem.getNumberOfImages() + 1;
        String to = FileUtil.getJobDirectory(jobId) + "/" + mCurrentItem.getFilename() + "_" + picNumber + "_.jpg";
        if (FileUtil.cp(from, to)) {
            updateInvoiceImg(to);
            setInvoiceDrawable(to);
            mCurrentItem.setNumberOfImages(picNumber);
        } else {
            UiUtil.showError(getActivity(), getString(R.string.error_title), getString(R.string.error_saving_image));
        }
    }

    private String getImageFileName(String filename, int imageNumber) {
        return FileUtil.getJobDirectory(jobId) + "/" + filename + "_" + imageNumber + "_.jpg";
    }

    private void updateInvoiceImg(String filePath) {
        // String filePath = FileUtil.getJobDirectory(jobId) + "/" + AppConstants.INVOICE_FILE_NAME;
        File file = new File(filePath);
        if (file != null && file.exists()) {
            int orientation = -1;
            try {
                orientation = UiUtil.resolveBitmapOrientation(file);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            Bitmap bitmap = BitmapFactory.decodeFile(filePath);
            if (bitmap != null) {
                bitmap = UiUtil.scaleBitmap(bitmap, AppConstants.CAPTURE_IMAGE_MEDIUM_SIZE);
                if (orientation != -1) {
                    Bitmap orientBitmap = UiUtil.applyOrientation(bitmap, orientation);
                    if (orientBitmap != null) {
                        bitmap = orientBitmap;
                    }
                }
                OutputStream os;
                try {
                    if (file.delete()) {
                        file = new File(filePath);
                        os = new FileOutputStream(file);
                        bitmap.compress(CompressFormat.JPEG, 100, os);
                        os.flush();
                        os.close();
                    }
                    LibUtils.updateExifData(filePath);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void setInvoiceDrawable(String fileName) {
        imgPreviewInvoice.setVisibility(View.VISIBLE);
        Drawable drawable = imgCameraInvoice.getDrawable();
        int margin = getActivity().getResources().getDimensionPixelOffset(R.dimen.form_screen_part_detail_top_margin);
        int width = drawable.getIntrinsicWidth() + (margin << 2);
        int height = drawable.getIntrinsicHeight() + (margin << 1);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(width, height);
        params.addRule(RelativeLayout.RIGHT_OF, R.id.iv_capture_camera);
        params.setMargins(margin, 0, 0, 0);
        imgPreviewInvoice.setLayoutParams(params);

        imgPreviewInvoice.setPadding(10, 2, 10, 2);
        imgPreviewInvoice.setBackground(null);
        imgPreviewInvoice.setImageDrawable(null);
        imgPreviewInvoice.setImageURI(Uri.parse(fileName));
    }

    // public void updateCompanyDetails()
    // {
    // rebateConfirmationHeader.setText(UiUtil.selectedId.getName());
    // if (UiUtil.getResource(UiUtil.selectedId.getIcon()) == 0)
    // {
    // rebateConfirmationHeader.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icf_logo, 0, 0, 0);
    // }
    // else
    // {
    // rebateConfirmationHeader.setCompoundDrawablesWithIntrinsicBounds(UiUtil.getResource(UiUtil.selectedId.getIcon()), 0, 0, 0);
    // }
    // }

    public void startRecording(String path) {
        if (recorder != null) {
            mChronometer.stop();
            recorder.stop();
            recorder.release();
            recorder = null;
        }
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        recorder.setOutputFile(path);

        mChronometer.setVisibility(View.VISIBLE);
        playProgressVG.setVisibility(View.INVISIBLE);
        try {
            recorder.prepare();
            recorder.start();
            mChronometer.start();
            btnVoice.setImageResource(R.drawable.stop_selector);
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stopRecording(boolean initializePlayer) {
        if (recorder != null) {
            mChronometer.stop();
            recorder.stop();
            recorder.release();
            recorder = null;
            btnVoice.setImageResource(R.drawable.voice_recording_btn);
            btnPlay.setEnabled(true);
            if (initializePlayer) {
                initializePlayer();
            }
        }
    }

    public void prepareMediaPlayer(String path) {
        player = new MediaPlayer();
        try {
            player.setDataSource(path);
            player.setOnPreparedListener(prepareListener);
            player.setOnCompletionListener(completionListener);
            player.prepare();
            // player.start();

        } catch (IllegalArgumentException | SecurityException | IllegalStateException | IOException e) {
            e.printStackTrace();
        }
    }

    OnCompletionListener completionListener = new OnCompletionListener() {

        @Override
        public void onCompletion(MediaPlayer mp) {
            stopPlaying();
        }
    };

    OnPreparedListener prepareListener = new OnPreparedListener() {

        @Override
        public void onPrepared(MediaPlayer mp) {
            if (player != null) {
                mChronometer.setVisibility(View.INVISIBLE);
                playProgressVG.setVisibility(View.VISIBLE);

                btnPlay.setEnabled(true);

                mTotalDuration = player.getDuration();
                mSeekbar.setMax(mTotalDuration);
                mSeekbar.postDelayed(progressRunnable, 1000);
            }
        }
    };

    Runnable progressRunnable = new Runnable() {

        @Override
        public void run() {
            if (player != null) {
                if (mSeekbar != null) {
                    mSeekbar.setProgress(player.getCurrentPosition());
                }
                updateTime();
                if (player.isPlaying()) {
                    mSeekbar.postDelayed(progressRunnable, 1000);
                }
            } else {
                btnPlay.setImageResource(R.drawable.play_selector);
                mSeekbar.setProgress(0);
                mCurrentPositionTxt.setText("00:00");
            }
        }
    };

    OnSeekBarChangeListener seekListener = new OnSeekBarChangeListener() {

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (fromUser) {
                if (player != null) {
                    if (progress == mTotalDuration) {
                        stopPlaying();
                    } else {
                        player.seekTo(progress);
                        updateTime();
                    }
                } else {
                    mSeekbar.setProgress(0);
                }
            }

        }
    };

    public void pausePlaying() {
        if (player != null) {
            player.pause();
            btnPlay.setImageResource(R.drawable.play_selector);
        }
    }

    public void resumePlaying() {
        if (player != null) {
            player.start();
            btnPlay.setImageResource(R.drawable.voice_recording_pause_btn);
            mSeekbar.postDelayed(progressRunnable, 0);
        }
    }

    public void stopPlaying() {
        if (player != null) {
            player.stop();
            player.release();
            player = null;
            mTotalDurationStr = null;
            btnPlay.setImageResource(R.drawable.play_selector);
            mSeekbar.setProgress(0);
            mCurrentPositionTxt.setText("00:00");
        }
    }

    private void updateTime() {
        mCurrentDuration = player.getCurrentPosition();
        int dSeconds = 0;
        int dMinutes = 0;
        int dHours = 0;
        if (mTotalDurationStr == null) {
            dSeconds = (int) (mTotalDuration / 1000) % 60;
            dMinutes = (int) ((mTotalDuration / (1000 * 60)) % 60);
            dHours = (int) ((mTotalDuration / (1000 * 60 * 60)) % 24);
        }

        int cSeconds = (int) (mCurrentDuration / 1000) % 60;
        int cMinutes = (int) ((mCurrentDuration / (1000 * 60)) % 60);
        int cHours = (int) ((mCurrentDuration / (1000 * 60 * 60)) % 24);

        String currentDurationStr = null;
        if (dHours == 0) {
            currentDurationStr = String.format("%02d:%02d", cMinutes, cSeconds);
            if (mTotalDurationStr == null) {
                mTotalDurationStr = String.format("%02d:%02d", dMinutes, dSeconds);
            }
        } else {
            currentDurationStr = String.format("%02d:%02d:%02d", cHours, cMinutes, cSeconds);
            if (mTotalDurationStr == null) {
                mTotalDurationStr = String.format("%02d:%02d:%02d", dHours, dMinutes, dSeconds);
            }
        }

        mTotalDurationTxt.setText(mTotalDurationStr);
        mCurrentPositionTxt.setText(currentDurationStr);
    }

    private void initializePlayer() {
        mChronometer.setVisibility(View.INVISIBLE);
        btnPlay.setImageResource(R.drawable.play_selector);
        btnVoice.setImageResource(R.drawable.stop_selector);
        btnVoice.setEnabled(false);
        prepareMediaPlayer(getVoiceFilePath());
    }

    private void initializeRecorder() {
        mChronometer.setBase(SystemClock.elapsedRealtime());
        btnVoice.setEnabled(true);
        mChronometer.setVisibility(View.VISIBLE);
        playProgressVG.setVisibility(View.INVISIBLE);
        btnVoice.setImageResource(R.drawable.voice_recording_btn);
        btnPlay.setImageResource(R.drawable.play_selector);
        btnPlay.setEnabled(false);
    }

    @Override
    public void onFinishInflate(int layoutId, View view) {
        if (layoutId == R.layout.voiceagreement) {

            btnPlay = (ImageView) view.findViewById(R.id.playVoice);
            btnVoice = (ImageView) view.findViewById(R.id.recordVoice);
            mChronometer = (Chronometer) view.findViewById(R.id.chrono);
            mCurrentPositionTxt = (TextView) view.findViewById(R.id.currentPosition);
            mTotalDurationTxt = (TextView) view.findViewById(R.id.totalDuration);
            playProgressVG = view.findViewById(R.id.playProgress);
            mSeekbar = (SeekBar) view.findViewById(R.id.seekbar);
            mSeekbar.setOnSeekBarChangeListener(seekListener);
            mChronometer.setFormat("%s:%s:%s");
            signatureFooter = view.findViewById(R.id.signatureFooter);

            btnClear = (ImageButton) view.findViewById(R.id.btnClear);
            btnDone = (ImageButton) view.findViewById(R.id.btnDone);

            if (FileUtil.exists(getVoiceFilePath())) {
                signatureFooter.setVisibility(View.GONE);
                initializePlayer();
            } else {
                initializeRecorder();
            }

            if (btnPlay != null) {
                btnPlay.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (player == null) {
                            prepareMediaPlayer(getVoiceFilePath());
                            resumePlaying();
                        } else {
                            if (player.isPlaying()) {
                                pausePlaying();
                            } else {
                                resumePlaying();
                            }
                        }
                    }
                });
            }

            if (btnVoice != null) {
                btnVoice.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (recorder == null) {
                            initializeRecorder();
                            startRecording(getVoiceFilePath());
                        } else {
                            stopRecording(true);
                        }
                    }
                });
            }
            if (btnClear != null) {
                btnClear.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        String fileName = FileUtil.getJobDirectory(jobId) + "/" + AppConstants.VOICE_FILE_NAME;
                        stopRecording(false);
                        FileUtil.deleteFile(fileName);
                        if (FileUtil.exists(getVoiceFilePath())) {
                            initializePlayer();
                        } else {
                            initializeRecorder();
                        }
                    }
                });
            }
            if (btnDone != null) {
                btnDone.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        voiceDialog.dismissAllowingStateLoss();
                    }
                });
            }
        } else if (layoutId == R.layout.image_preview_popup && view != null && photoDialog != null) {

            final ImageView previewImage = (ImageView) view.findViewById(R.id.preview_image);
            ImageButton done = (ImageButton) view.findViewById(R.id.done_image);
            final TextView tvNext = (TextView) view.findViewById(R.id.tv_next);
            Handler h = new Handler();
            h.post(new Runnable() {
                @Override
                public void run() {
                    if (mCurrentItem != null) {
                        String fileName = FileUtil.getJobDirectory(jobId) + "/" + mCurrentItem.getFilename() + "_" + mCurrentItem.getNumberOfImages() + "_.jpg";
                        // next button
                        tvNext.setVisibility(View.VISIBLE);
                        tvNext.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                int imgNumber = mCurrentItem.getCurrentThumbPreviewPosition() + 1;
                                File nextImage = new File(FileUtil.getJobDirectory(jobId) + "/" + mCurrentItem.getFilename() + "_" + imgNumber + "_.jpg");
                                String fileName = null;
                                if (nextImage.exists()) {
                                    fileName = nextImage.getPath();
                                    mCurrentItem.setCurrentThumbPreviewPosition(imgNumber);
                                } else {
                                    imgNumber = 1;
                                    nextImage = new File(FileUtil.getJobDirectory(jobId) + "/" + mCurrentItem.getFilename() + "_" + imgNumber + "_.jpg");
                                    if (nextImage.exists()) {
                                        fileName = nextImage.getPath();
                                        mCurrentItem.setCurrentThumbPreviewPosition(imgNumber);
                                    }
                                }
                                if (fileName != null) {
                                    try {
                                        Bitmap image = BitmapFactory.decodeFile(fileName);
                                        previewImage.setImageBitmap(image);
                                    } catch (Exception e) {
                                        Log.e("Error", e.getMessage());
                                    }
                                }
                            }

                        });
                        try {
                            // URL newurl = new URL(photoUri);
                            Bitmap image = BitmapFactory.decodeFile(fileName);
                            previewImage.setImageBitmap(image);
                        } catch (Exception e) {
                            Log.e("Error", e.getMessage());
                        }
                    }
                }
            });

            done.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    photoDialog.dismiss();
                }
            });
        }
    }

    @Override
    public void onDismiss() {
        stopRecording(false);
        stopPlaying();
        // refreshList(false);
    }

    @Override
    public void onStop() {
        super.onStop();
        // refreshList(true);
    }

    private String getVoiceFilePath() {
        return FileUtil.getJobDirectory(jobId) + File.separator + AppConstants.VOICE_FILE_NAME;
    }

    private void updateAppDetails() {
        if (appDetails != null) {
            String comments = userComments.getText().toString();
            if (!TextUtils.isEmpty(comments)) {
                appDetails.setComments(comments);
            }

            if (mDocumentLater != null) {
                appDetails.setDocumentLater(mDocumentLater.isChecked());
            }

            if (mOrderNumber != null) {
                appDetails.setOrderNumber(mOrderNumber.getText().toString());
            }
        }
    }
}
