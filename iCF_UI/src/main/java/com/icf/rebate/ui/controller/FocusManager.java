package com.icf.rebate.ui.controller;

import java.util.ArrayList;

import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;

/**
 * Used for setting the ACTION_NEXT and also can be used to customize the focus in future.
 * 
 * @author adminpc
 * 
 */
public class FocusManager
{
    private ArrayList<View> inputViewList;

    private FocusManager()
    {

    }

    public static FocusManager newFocusManager(View view)
    {
	FocusManager manager = new FocusManager();
	manager.populateInputViews(view);
	manager.addKeyFocusListener();
	return manager;
    }

    public void repopulateFocus(View view)
    {
	if (inputViewList == null)
	{
	    inputViewList = new ArrayList<View>();
	}
	if (inputViewList != null)
	{
	    inputViewList.clear();
	}
	populateInputViews(view);
	addKeyFocusListener();
    }

    private void addKeyFocusListener()
    {
	if (inputViewList == null)
	{
	    return;
	}
	int i = 0;
	for (View view : inputViewList)
	{
	    if (view instanceof EditText)
	    {
		if (i == inputViewList.size() - 1)
		{
		    ((EditText) view).setImeOptions(EditorInfo.IME_ACTION_DONE);
		}
		else
		{
		    if (((EditText) view).getImeOptions() != EditorInfo.IME_ACTION_DONE)
		    {
			((EditText) view).setImeOptions(EditorInfo.IME_ACTION_NEXT);
		    }
		}
	    }
	    i++;
	}
    }

    private void populateInputViews(View view)
    {
	if (view == null)
	{
	    return;
	}
	if (inputViewList == null)
	{
	    inputViewList = new ArrayList<View>();
	}
	View tempView = null;
	if (view instanceof ViewGroup)
	{
	    int count = ((ViewGroup) view).getChildCount();
	    for (int i = 0; i < count; i++)
	    {
		tempView = ((ViewGroup) view).getChildAt(i);
		if (tempView != null && tempView.getVisibility() == View.VISIBLE)
		{
		    if (tempView instanceof ViewGroup)
		    {
			populateInputViews(tempView);
		    }
		    else if (isInputView(tempView))
		    {
			inputViewList.add(tempView);
		    }
		}
	    }
	}
	else if (isInputView(tempView))
	{
	    inputViewList.add(tempView);
	}
    }

    private boolean isInputView(View view)
    {
	if (view instanceof EditText)
	{
	    if (((EditText) view).isEnabled())
	    {
		return true;
	    }
	}
	return false;
    }
}
