package com.icf.rebate.ui.util.rebate;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.icf.rebate.networklayer.model.FormResponseBean;
import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.util.ICFLogger;

import java.util.List;

/**
 * Created by 19713 on 2/27/2018.
 */

public class ProjectCostRebateCalculation {
    private static final String TAG = ProjectCostRebateCalculation.class.getName();
    private static final String HOUSE_TYP_SINGLE = "Single Family"; //"single";
    private static final String HOUSE_TYP_MULTI = "Multifamily"; //"Mullti";

    public static final String CALC_TYPE = "projectCost";

    public float getRebateValue(String rebateType, float projectCost, int quantity) {
        float rebate = 0f;
        FormResponseBean bean = RebateManager.getInstance().getFormBean();

        String houseType = null;
        if (bean.getAppDetails() != null) {
            houseType = bean.getAppDetails().getHouseType();
            if (houseType == null || houseType.length() == 0) {
                houseType = "NA";
            } else {
                if (houseType.toLowerCase().contains("single")) houseType = HOUSE_TYP_SINGLE;
                if (houseType.toLowerCase().contains("multi")) houseType = HOUSE_TYP_MULTI;
            }
        }

        String selectionString = ProjectCostRebateTable.COLUMN_UTILITY_COMPANY + "= ?"
                + " AND " + ProjectCostRebateTable.COLUMN_REBATE_TYPE + " = ?"
                + " AND " + ProjectCostRebateTable.COLUMN_HOUSE_TYPE + " = ?";

        String[] selectionArgs = new String[]
                {String.valueOf(LibUtils.getSelectedUtilityCompany().getId()), rebateType, houseType};

        try {
            ICFSQLiteOpenHelper helper = new ICFSQLiteOpenHelper(LibUtils.getApplicationContext());
            SQLiteDatabase database = helper.getReadableDatabase();

            // Need to figure out which column the date range is referring to
            Cursor dbCursor = database.query(ProjectCostRebateTable.TABLE_NAME,
                    null, selectionString, selectionArgs, null, null, null);
            String[] columnNames = dbCursor.getColumnNames();

            // Gets list of all columns that apply to todays date
            List<String> matchingDateRangeCols = LibUtils.getYearColumnName(columnNames, null);
            if (!matchingDateRangeCols.isEmpty() && dbCursor.moveToNext()) {
                float multiplier = dbCursor.getFloat
                   (dbCursor.getColumnIndex(ProjectCostRebateTable.COLUMN_PROJECT_COST_MULTIPLIER));
                float maxRebate =  dbCursor.getFloat
                   (dbCursor.getColumnIndex(matchingDateRangeCols.get(0)));

                if (houseType.equalsIgnoreCase(HOUSE_TYP_MULTI)) {
                    if (multiplier * projectCost < maxRebate) {
                        rebate = quantity * (multiplier * projectCost);
                    } else {
                        rebate = maxRebate * quantity;
                    }
                } else {
                    //houseType = HOUSE_TYP_SINGLE or NA
                    rebate = Math.min((projectCost * multiplier), maxRebate);
                }
            }
        } catch (Exception e) {
            ICFLogger.e(TAG, "Error in getRebateValue; houseType: " + houseType, e);
        }
        return rebate;
    }
}
