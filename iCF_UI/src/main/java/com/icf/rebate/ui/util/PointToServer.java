package com.icf.rebate.ui.util;
 

import android.content.Context;

import com.icf.ameren.rebate.ui.R;

//TODO: Let us remove this later/ i dont think we will need this for ICF since not much customization is needed

public class PointToServer
{

    PointingServers obj;
    Context context;
    String displayText;
    private boolean isInKnoxMode = false;

    public enum PointingServers
    {
	DEV_LOCAL
    }

    public PointToServer(Context context, PointingServers obj, boolean isInKnoxMode)
    {
	this.obj = obj;
	this.context = context;
    }

    public String getServerUrl()
    {
	String url = null;
	  
	switch (obj)
	{
	case DEV_LOCAL:
        url = context.getString(R.string.production_build);//.production_build//.rw_staging_build);//.dev_box_local_url);
	    //netstat -ao to find listening port; verify with SOAP UI 
	    //url = "http://localhost:9090";
	    displayText = "devLocal";
	    break;
	}
	return url;
    }


    public String getDisplayText()
    {
	return displayText == null ? "" : displayText;
    }
//
//    public boolean isQARelease()
//    {
//	return obj == PointingServers.QA_LOCAL || obj == PointingServers.QA_PUBLIC || obj == PointingServers.USBOX;
//    }
//
//    public boolean isDevRelease()
//    {
//	return obj == PointingServers.DEV_LOCAL || obj == PointingServers.DEV_PUBLIC;
//    }
//
//    public boolean isProd()
//    {
//	boolean retVal = false;
//	switch (obj)
//	{
//	case PRODUCTION:
//	    retVal = true;
//	    break;
//	}
//	return retVal;
//    }
}