package com.icf.rebate.ui.util.rebate;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.rebate.ui.util.ICFLogger;

public class ICFSQLiteOpenHelper extends SQLiteOpenHelper
{
    public static final String DB_NAME = "icf.db";
    private static final int DATABASE_VERSION = 2;
    private static final String TAG = ICFSQLiteOpenHelper.class.getName();

    public static final String COLUMN_INCENTIVE_2015 = "incentive_2015";
    public static final String COLUMN_INCENTIVE_2016 = "incentive_2016";
    public static final String COLUMN_INCENTIVE_2017 = "incentive_2017";
    public static final String COLUMN_INCENTIVE_2018 = "incentive_2018";
    public static final String COLUMN_INCENTIVE_2019 = "incentive_2019";
    public static final String COLUMN_INCENTIVE_2020 = "incentive_2020";
    public static final String COLUMN_INCENTIVE_2021 = "incentive_2021";
    public static final String COLUMN_INCENTIVE_2022 = "incentive_2022";
    public static final String COLUMN_INCENTIVE_2023 = "incentive_2023";
    public static final String COLUMN_INCENTIVE_2024 = "incentive_2024";
    public static final String KEY_SYSTEM = "system";
    public static final String KEY_TONNAGE = "ton";

    public static final int FLOAT_MULTIPLIER = 10000;

    public ICFSQLiteOpenHelper(Context context)
    {
	super(context, DB_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
	EquipmentListRebateTable.onCreate(db);
	QIVTuneUpRebateTable.onCreate(db);
	DuctSealingRebateTable.onCreate(db);
	ThermostatRebateTable.onCreate(db);
	InsulationTable.onCreate(db);
	WindowDoorTable.onCreate(db);
	ProjectCostRebateTable.onCreate(db);
	EnthalpyTable.onCreate(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
	EquipmentListRebateTable.onUpgrade(db, oldVersion, newVersion);
	QIVTuneUpRebateTable.onUpgrade(db, oldVersion, newVersion);
	DuctSealingRebateTable.onUpgrade(db, oldVersion, newVersion);
	ThermostatRebateTable.onUpgrade(db, oldVersion, newVersion);
	InsulationTable.onUpgrade(db, oldVersion, newVersion);
	WindowDoorTable.onUpgrade(db, oldVersion, newVersion);
	ProjectCostRebateTable.onUpgrade(db, oldVersion, newVersion);
	EnthalpyTable.onUpgrade(db, oldVersion, newVersion);
	
	ICFLogger.d(TAG, "Upgrade : " + "db :" + db + "oldVersion :" + oldVersion + "newVersion :" + newVersion);
    }

    public boolean truncateRebateTable()
    {
	SQLiteDatabase database = getWritableDatabase();
	String[] tableNames = new String[] { EquipmentListRebateTable.TABLE_NAME,
										 QIVTuneUpRebateTable.TABLE_NAME,
										 DuctSealingRebateTable.TABLE_NAME,
										 ThermostatRebateTable.TABLE_NAME,
			                             InsulationTable.TABLE_NAME,
			                             WindowDoorTable.TABLE_NAME,
										 ProjectCostRebateTable.TABLE_NAME};
	for (int i = 0; i < tableNames.length; i++)
	{
	    try
	    {
		database.execSQL("DROP TABLE " + tableNames[i]);
	    }
	    catch (Exception e)
	    {
		ICFLogger.e(TAG, e);
	    }
	}
	onCreate(database);
	database.close();
	return true;
    }
    
}
