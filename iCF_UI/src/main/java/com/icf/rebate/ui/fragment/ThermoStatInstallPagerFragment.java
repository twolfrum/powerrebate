package com.icf.rebate.ui.fragment;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.icf.rebate.adapter.CustomSpinnerAdapter;
import com.icf.rebate.adapter.ThermoStatInstallAdapter;
import com.icf.rebate.app.model.DashboardItem.ServiceItem;
import com.icf.rebate.networklayer.model.CustomerInfo;
import com.icf.rebate.networklayer.model.FormResponseBean;
import com.icf.rebate.networklayer.model.QIVItem;
import com.icf.rebate.networklayer.model.ThermostatInstallItem;
import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.ui.RootActivity;
import com.icf.rebate.ui.controller.FormUIManager;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.customviews.CustomPageIndicator;
import com.icf.rebate.ui.customviews.CustomViewPager;
import com.icf.rebate.ui.dialog.DialogListener;
import com.icf.rebate.ui.dialog.ICFDialogFragment;
import com.icf.rebate.ui.listeners.FragmentCallFlow;
import com.icf.rebate.ui.listeners.RequestCallFlow;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.FileUtil;
import com.icf.rebate.ui.util.UiUtil;

public class ThermoStatInstallPagerFragment extends Fragment
	implements FragmentCallFlow, RequestCallFlow, OnItemSelectedListener, OnClickListener, DialogListener, OnPageChangeListener, PersistentForm
{
    public CustomViewPager viewPager;

    private FormUIManager uiManager;

    private String title;

    private Spinner statsComboBox;

    private ThermoStatInstallAdapter statsAdapter;

    private List<ThermostatInstallItem> statsList;

    private CustomSpinnerAdapter adapter;

    private List<String> spinnerList = new ArrayList<>();

    private int pageIndex = 0;

    private ICFDialogFragment commonDialog;

    private FormResponseBean bean;

    public ThermoStatInstallPagerFragment()
    {
	super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
	View view = inflater.inflate(R.layout.form_screen_layout, container, false);
	return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
	super.onViewCreated(view, savedInstanceState);

	getUiControls(view);
	initialize(view);
	bindCallbacks(view);
	requestData();

	CustomerInfo cInfo = RebateManager.getInstance().getFormBean().getCustomerInfo();
	if (UiUtil.isJobCompleted(cInfo))
	{
	    ArrayList<View> list = new ArrayList<View>();
	    list.add(viewPager);
	    UiUtil.setEnableView((ViewGroup) view, false, list);
	}
    }

    @Override
    public void requestData()
    {
	RebateManager.getInstance().checkAndrequestData(this);
    }

    @Override
    public void getUiControls(View root)
    {
	statsComboBox = (Spinner) root.findViewById(R.id.equipment_selector);
	viewPager = (CustomViewPager) root.findViewById(R.id.pager);

    }

    @Override
    public void initialize(View root)
    {
	uiManager = new FormUIManager("ThermostatInstallPager");
	RootActivity rootAct = (RootActivity) getActivity();
	if (rootAct != null)
	{
	    rootAct.updateTitle(title);
	}

	statsAdapter = new ThermoStatInstallAdapter(getChildFragmentManager(), viewPager);
	viewPager.setAdapter(statsAdapter);
	viewPager.setCurrentItem(0);
    }

    private void updateUI()
    {
	if (statsList != null)
	{
	    statsAdapter.setInstallList(statsList);
	    statsAdapter.notifyDataSetChanged();
	    viewPager.setOffscreenPageLimit(statsList.size());
	    CustomPageIndicator indicator = ((CustomPageIndicator) getActivity().findViewById(R.id.page_indicator));
	    viewPager.setPageIndicator(indicator);
	    indicator.setPager(viewPager);

	    updateSpinnerAdapter();
	}

    }

    @Override
    public void bindCallbacks(View root)
    {
	statsComboBox.setOnItemSelectedListener(this);
	root.findViewById(R.id.save_btn).setOnClickListener(this);
	root.findViewById(R.id.close_btn).setOnClickListener(this);
	root.findViewById(R.id.add_btn).setOnClickListener(this);
	viewPager.setOnPageChangeListener(this);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
	if (viewPager.getCurrentPage() != position)
	{
	    viewPager.setCurrentItem(position, true);
	}
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {

    }

    @Override
    public void onDestroyView()
    {
	super.onDestroyView();
	saveForm(false);
    }

    private float computeRebate()
    {
	int count = statsAdapter.getCount();
	float rebateValue = 0;
	for (int i = 0; i < count; i++)
	{
	    ThermostatInstallItem item = statsList.get(i);
	    rebateValue += item.getRebateValue();
	}

	return rebateValue;

    }

    @Override
    public void onClick(View v)
    {
	if (v != null)
	{
	    UiUtil.hideSoftKeyboard(getActivity(), uiManager.getLastFocusedEditField());

	    int id = v.getId();
	    if (id == R.id.save_btn)
	    {
		saveForm(pageIndex, true);
	    }
	    else if (id == R.id.close_btn)
	    {
		commonDialog = ICFDialogFragment.newDialogFrag(R.layout.delete_confirmation);
		commonDialog.setDialogListener(this);
		commonDialog.show(getActivity().getSupportFragmentManager(), "Delete Confirmation");
		getActivity().getSupportFragmentManager().executePendingTransactions();
	    }
	    else if (id == R.id.add_btn)
	    {
			addItem();
	    }
	}

    }

    private synchronized void addItem()
    {
	ThermostatInstallItem item = statsList.get(pageIndex);
	if (statsList != null && statsList.size() < item.getMaxMeasures()) //AppConstants.MAX_PAGE_SUPPORT)
	{
	    item = (ThermostatInstallItem) statsList.get(0).createObject();
	    item.setPageNum(statsList.get(statsList.size() - 1).getPageNum() + 1);
	    statsList.add(item);
	    statsAdapter.setInstallList(statsList);
	    statsAdapter.notifyDataSetChanged();
	    viewPager.setOffscreenPageLimit(statsList.size());
	    viewPager.firePageCountChanged();
	    viewPager.setCurrentItem(statsList.size() - 1);
	    updateSpinnerAdapter();
	}
	else if (statsList.size() == item.getMaxMeasures())
	{
		UiUtil.showError(getActivity(), getResources().getString(R.string.alert_title),
				getResources().getString(R.string.maximum_measures_limit,
						item.getName(), item.getMaxMeasures()));
	}
    }

    @Override
    public int getNoScreensToToolList()
    {
	return 1;
    }
    
    @Override
    public void setValidState()
    {
	// unimpl

    }

    @Override
    public boolean isValid()
    {
	return RebateManager.getInstance().checkAnyFormFilled(getString(R.string.thermostatInstallId));
    }

    @Override
    public void saveForm(boolean isManualSave)
    {
	// Update page fragment... so result gets updated
	if (statsAdapter != null && statsList != null)
	{
	    ThermoStatInstallFragment pageFragment = statsAdapter.getThermoStatFragment(statsList.get(pageIndex));
	    if (pageFragment != null)
	    {
		pageFragment.updateThermoStatResult();
	    }
	}
	saveForm(pageIndex, isManualSave);
	if (statsList != null)
	{
	    for (int i = 0; i < statsList.size(); i++)
	    {
		if (statsList.get(i) != null && spinnerList != null && statsList.size() >= spinnerList.size())
		{
		    // statsList.get(i).setName(spinnerList.get(i));
		    statsList.get(i).setName("Thermostat");
		}
	    }
	}
	//$$$ called when Save button clicked
	RebateManager.getInstance().getFormBean().setThermostatInstallRebate(computeRebate());
    }

    public synchronized void saveForm(int pageIndex, boolean manualSave)
    {
	if (statsAdapter != null)
	{
	    boolean isItemSaved = statsAdapter.setItemSaved(pageIndex, manualSave);

	    RebateManager.getInstance().updateFormState(null);
	    if (!manualSave && !isItemSaved)
	    {
		deleteItemAtIndex(pageIndex);
		return;
	    }

	    Toast.makeText(getActivity(), getString(R.string.saved_string), Toast.LENGTH_LONG).show();
	}
    }

    private synchronized boolean deleteItemAtIndex(int pageIndex)
    {
	boolean retVal = false;

	if (statsList != null && statsList.size() > pageIndex && statsList.size() > 1)
	{
	    final int selectedPageNum = statsList.get(pageIndex).getPageNum();
	    retVal = statsAdapter.deleteItem(pageIndex) != null;

	    Thread t = new Thread(new Runnable()
	    {
		public void run()
		{
		    String equipmentName = getActivity().getResources().getString(R.string.thermostatInstallId);
		    FileUtil.deleteFolderPartImage(equipmentName, selectedPageNum + "");
		}
	    });
	    t.start();
	}
	return retVal;
    }

    @Override
    public void onFinishInflate(int layoutId, View view)
    {
	if (layoutId == R.layout.delete_confirmation && view != null)
	{
	    ImageButton cancel = (ImageButton) view.findViewById(R.id.delete_cancel_btn);
	    ImageButton done = (ImageButton) view.findViewById(R.id.delete_done_btn);
	    cancel.setOnClickListener(new OnClickListener()
	    {

		@Override
		public void onClick(View v)
		{
		    commonDialog.dismiss();
		}
	    });
	    done.setOnClickListener(new OnClickListener()
	    {

		@Override
		public void onClick(View v)
		{
		    deleteItem();
		}

	    });
	}
    }

    @Override
    public void onDismiss()
    {

    }

    private synchronized void deleteItem()
    {
	ThermostatInstallItem newItem = (ThermostatInstallItem) statsList.get(0).createObject();
	newItem.setPageNum(statsList.get(statsList.size() - 1).getPageNum() + 1);
	ThermoStatInstallFragment frag = statsAdapter.getThermoStatFragment(statsList.get(pageIndex));
	final int selectedPageNum = statsList.get(pageIndex).getPageNum();
	if (frag != null)
	{
	    frag.setFragmentDetroyed(pageIndex);
	}

	statsAdapter.deleteItem(pageIndex);
	Log.v(ThermoStatInstallFragment.TAG, "deleteItem " + pageIndex);

	Thread t = new Thread(new Runnable()
	{
	    public void run()
	    {
		String equipmentName = getActivity().getResources().getString(R.string.thermostatInstallId);
		FileUtil.deleteFolderPartImage(equipmentName, selectedPageNum + "");
	    }
	});
	t.start();
	spinnerList.remove(pageIndex);
	if (newItem != null && statsList.size() == 0)
	{
	    newItem.setPageNum(0);
	    statsList.add(newItem);
	}
	statsAdapter.notifyDataSetChanged();
	viewPager.setOffscreenPageLimit(statsList.size());
	viewPager.firePageCountChanged();
	if (pageIndex >= statsList.size())
	{
	    --pageIndex;
	}
	viewPager.setCurrentItem(pageIndex);
	updateSpinnerAdapter();
	if (commonDialog != null)
	{
	    commonDialog.dismiss();
	}
    }

    public void setTitle(String title)
    {
	this.title = title;
    }

    @Override
    public void stopProgressDialog()
    {
	getActivity().runOnUiThread(new Runnable()
	{
	    public void run()
	    {
		UiUtil.dismissSpinnerDialog();
	    }
	});
    }

    @Override
    public void updateData(final Object obj)
    {
	if (obj != null && obj instanceof FormResponseBean)
	{
	    bean = (FormResponseBean) obj;
	    statsList = ((FormResponseBean) obj).getThermostatInstall();

	    getActivity().runOnUiThread(new Runnable()
	    {
		public void run()
		{
		    updateUI();
		}
	    });
	}

    }

    @Override
    public void displayError(String title, String message)
    {

    }

    @Override
    public void showProgressDialog()
    {
	getActivity().runOnUiThread(new Runnable()
	{
	    public void run()
	    {
		UiUtil.showProgressDialog(getActivity());
	    }
	});
    }

    @Override
    public void onPageScrollStateChanged(int arg0)
    {

    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2)
    {

    }

    @Override
    public void onPageSelected(int index)
    {
	pageIndex = index;
	statsComboBox.setSelection(pageIndex);
    }

    public void serviceItem(ServiceItem serviceItem)
    {
	setTitle(serviceItem.getServiceItemName());
    }

    public void updateRebateValue(float value)
    {
	if (bean != null)
	{
	    bean.setThermostatInstallRebate(value);
	}
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
	super.onActivityResult(requestCode, resultCode, data);

	if (requestCode == AppConstants.CAPTURE_PHOTO_REQUEST_CODE && resultCode == FragmentActivity.RESULT_OK)
	{
	    ThermoStatInstallFragment frag = statsAdapter.getThermoStatFragment(statsList.get(pageIndex));
	    if (statsAdapter != null && frag != null)
	    {
		int pageNum = statsList.get(pageIndex).getPageNum();
		frag.updateCaptureImage(pageNum);
	    }

	    /*
	     * if (uiManager != null && uiManager.getCaptuerImageView() != null) { Part part = (Part) uiManager.getCaptuerImageView().getTag();
	     * 
	     * if (part != null) { String filePath; filePath = FileUtil.getFilePathForPartImage(part, pageIndex); // code to resize the image to reduce the
	     * size. File file = new File(filePath); if (file != null && file.exists()) { Bitmap bitmap = BitmapFactory.decodeFile(filePath); if (bitmap !=
	     * null) { bitmap = UiUtil.scaleBitmap(bitmap, AppConstants.CAPTURE_IMAGE_MEDIUM_SIZE); OutputStream os; try { if (file.delete()) { file = new
	     * File(filePath); os = new FileOutputStream(file); bitmap.compress(CompressFormat.JPEG, 100, os); os.flush(); os.close();
	     * part.setImagePath(filePath); View view = uiManager.getCaptuerImageView(); UiUtil.showPreviewDialog(view, part.getImagePath(), part); }
	     * 
	     * } catch (FileNotFoundException e) { e.printStackTrace(); } catch (IOException e) { e.printStackTrace(); } } } LibUtils.updateExifData(filePath);
	     * } }
	     */
	}
    }

    public String getPageName()
    {
	ThermoStatInstallFragment fragm = (ThermoStatInstallFragment) statsAdapter.getItem(pageIndex);
	return fragm.getstatsItemBean().getPageNum() + "";
    }

    // public int getPageIndex()
    // {
    // return pageIndex;
    // }

    private void updateSpinnerAdapter()
    {
	ListIterator<ThermostatInstallItem> iterator = statsList.listIterator();
	int index = 1;
	spinnerList = new ArrayList<String>();
	while (iterator.hasNext())
	{
	    iterator.next();
	    spinnerList.add("Thermostat " + index++);
	}

	if (spinnerList.size() == 0)
	{
	    spinnerList.add("Thermostat " + index);
	}

	adapter = new CustomSpinnerAdapter(getActivity(), 0, spinnerList);
	statsComboBox.setAdapter(adapter);
	statsComboBox.setSelection(viewPager.getCurrentItem(), true);
    }

    public List<ThermostatInstallItem> getStatsList()
    {
	return statsList;
    }

    public void setStatsList(List<ThermostatInstallItem> statsList)
    {
	this.statsList = statsList;
    }
}
