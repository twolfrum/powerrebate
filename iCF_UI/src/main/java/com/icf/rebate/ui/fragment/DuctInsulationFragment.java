package com.icf.rebate.ui.fragment;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.icf.rebate.networklayer.model.CustomerInfo;
import com.icf.rebate.networklayer.model.DuctItem;
import com.icf.rebate.networklayer.model.UIElement;
import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.ui.RootActivity;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.controller.UIElementManager;
import com.icf.rebate.ui.util.UiUtil;

public class DuctInsulationFragment extends Fragment
{
    private UIElementManager serverLayoutInflater;

    private View rootView;

    private ArrayList<UIElement> uiList;

    private String title;

    public DuctInsulationFragment()
    {
	super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
	View view = inflater.inflate(R.layout.qiv_screen, null, false);
	rootView = view;
	return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
	super.onViewCreated(view, savedInstanceState);
	serverLayoutInflater = new UIElementManager(getActivity());
	RootActivity rootAct = (RootActivity) getActivity();
	if (rootAct != null)
	{
	    rootAct.updateTitle(title);
	}
	initalize();

	CustomerInfo cInfo = RebateManager.getInstance().getFormBean().getCustomerInfo();
	if (UiUtil.isJobCompleted(cInfo))
	{
	    UiUtil.setEnableView((ViewGroup) view, false, null);
	}
    }

    protected void initalize()
    {
	if (uiList != null && uiList.size() > 0)
	{

	    View inflateView = serverLayoutInflater.inflate(uiList);

	    ((ViewGroup) rootView).addView(inflateView);

	    ((ViewGroup) rootView).invalidate();
	}
    }

    public void setData(DuctItem item)
    {
	this.uiList = item.getUiList();
	this.title = item.getName();
    }

}
