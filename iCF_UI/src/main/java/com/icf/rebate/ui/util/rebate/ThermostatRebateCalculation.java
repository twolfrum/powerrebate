package com.icf.rebate.ui.util.rebate;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import java.util.List;

import com.icf.rebate.networklayer.model.FormResponseBean;
import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.util.ICFLogger;
import com.icf.rebate.ui.util.UiUtil;

public class ThermostatRebateCalculation {
    private static final String TAG = ThermostatRebateCalculation.class.getName();

    public float getRebateValue(String upgradeMeasureName, boolean isRebate, String oldTStatType, int measure) {
        if (TextUtils.isEmpty(upgradeMeasureName) || TextUtils.isEmpty(oldTStatType)) {
            return 0.0f;
        }
        SQLiteDatabase database = null;
        float retValue = 0.0f;
        try {

            ICFSQLiteOpenHelper helper = new ICFSQLiteOpenHelper(LibUtils.getApplicationContext());
            database = helper.getReadableDatabase();
            String selectionString = ThermostatRebateTable.COLUMN_UTILITY_COMPANY + "= ? AND (" + ThermostatRebateTable.COLUMN_CUSTOMER_TYPE + " = ? OR " + ThermostatRebateTable.COLUMN_CUSTOMER_TYPE + " = ?) AND " + ThermostatRebateTable.COLUMN_UPGRADE_MEASURE_NAME + " = ? AND " + ThermostatRebateTable.COLUMN_TYPE + " = ? AND " + ThermostatRebateTable.COLUMN_OLD_STAT_TYPE + " = ?";

            String[] selectionArgs = null;

            FormResponseBean bean = RebateManager.getInstance().getFormBean();

            String customerType = null;
            if (bean.getAppDetails() != null) {
                customerType = bean.getAppDetails().getCustomerType();
            }

            if (customerType == null || customerType.length() == 0) {
                customerType = "NA";
            }

            String type = isRebate ? "Rebate" : "Referral";

            selectionArgs = new String[]{String.valueOf(LibUtils.getSelectedUtilityCompany().getId()), "NA", customerType, upgradeMeasureName, type, oldTStatType};

            // Need to figure out which column the date range is referring to
            Cursor dbCursor = database.query(ThermostatRebateTable.TABLE_NAME, null, null, null, null, null, null);
            String[] columnNames = dbCursor.getColumnNames();

            // Gets list of all columns that apply to todays date
            List<String> matchingDateRangeCols = LibUtils.getYearColumnName(columnNames, null);

            float incentive = 0;

            for (String col : matchingDateRangeCols) {

                String[] columns = new String[]{ThermostatRebateTable.COLUMN_MEASURE_UNIT, col};

                Cursor cursor = database.query(ThermostatRebateTable.TABLE_NAME, columns, selectionString, selectionArgs, null, null, null);
                String data = LibUtils.getSqlQueryString(ThermostatRebateTable.TABLE_NAME, columns, selectionString, selectionArgs, null, null, null);
                ICFLogger.d(TAG, data);
                while (cursor.moveToNext()) {
                    String measureType = cursor.getString(0);
                    if (!"ton".equalsIgnoreCase(measureType)) {
                        measure = 1;
                    }
                    // float incentive = cursor.getFloat(1);
                    if (!cursor.isNull(1)) {
                        incentive = incentive < cursor.getFloat(1) ? cursor.getFloat(1) : incentive;
                    }
                    retValue = measure * incentive;
                    retValue = UiUtil.convertToPostTwoDigtDecimal(retValue);
                    // break;
                }
            }
        } catch (Exception e) {
            ICFLogger.e(TAG, e);
        } finally {
            if (database != null) {
                database.close();
            }
        }
        return retValue;
    }
}
