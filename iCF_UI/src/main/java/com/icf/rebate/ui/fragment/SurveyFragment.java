
package com.icf.rebate.ui.fragment;

import java.util.Iterator;
import java.util.List;

import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.networklayer.model.FormResponseBean.Options;
import com.icf.rebate.networklayer.model.FormResponseBean.Part;
import com.icf.rebate.networklayer.model.PartListItem;
import com.icf.rebate.networklayer.model.Submission;
import com.icf.rebate.networklayer.model.SubmissionItem;
import com.icf.rebate.ui.RootActivity;
import com.icf.rebate.ui.controller.FormUIManager;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.controller.FormUIManager.FormViewCreationCallback;
import com.icf.rebate.ui.listeners.FragmentCallFlow;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.UiUtil;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

/**
 * @author Ken Butler Jan 12, 2016
 * SurveyFragment.java
 * Copyright (c) 2016 ICF International
 */
public class SurveyFragment extends Fragment implements FragmentCallFlow, FormViewCreationCallback, OnClickListener {

    public static final String TAG = SurveyFragment.class.getSimpleName();
    
    private Submission mSubmission;
    private View rootView;
    private LinearLayout mScrollViewCLayout;
    private FormUIManager manager;
    private String mTitle;
    
    public static SurveyFragment newInstance(Submission item) {
	if (item == null) {
	    throw new IllegalArgumentException("item cannot be null.");
	}
	
	SurveyFragment frag = new SurveyFragment();
	frag.setItem(item);
	frag.setTitle("Survey");
	return frag;
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
	rootView = inflater.inflate(R.layout.form_survey_layout, container, false);
	return rootView;
    }
    
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
	super.onViewCreated(view, savedInstanceState);
	getUiControls(view);
	initialize(view);
	bindCallbacks(view);
	requestData();

	// Set title in root activity
	if (getActivity() instanceof RootActivity && mTitle != null) {
	    ((RootActivity) getActivity()).setTitle(mTitle);
	}
    }

    @Override
    public void requestData() {
	// Unimpl
    }

    @Override
    public void viewCreated(View view, Part part, Options option) {
	if (part != null) {
	    Log.d(TAG, view.getClass().getSimpleName() + " created for part " + part.getName());
	}
    }

    @Override
    public void getUiControls(View root) {
	mScrollViewCLayout = (LinearLayout) root.findViewById(R.id.scrol_layout);
	rootView.findViewById(R.id.btn_continue).setOnClickListener(this);
    }

    @Override
    public void initialize(View root) {
	manager = new FormUIManager("Survey");
	manager.setFormViewCreationCallback(this);
	// Only one thing in the list right now
	// TODO add id to each item object in form_data.json
	SubmissionItem item = mSubmission.getItems().get(0);
	if (item != null && item.getSurveyDetail() != null) {
	    manager.createViews(this, mScrollViewCLayout, item.getSurveyDetail().getParts(), item.isItemSaved());
	}
    }

    
    @Override
    public void onClick(View v) {
	Submission submission = null;
	Fragment frag = null;
	// Screen sequence options
	if((submission = RebateManager.getInstance().getFormBean().getSubmissionObjectById(AppConstants.REBATE_RECIPIENT_ID)) != null) {
		frag = new ConfirmRebateFragment();
	    }
	    else if((submission = RebateManager.getInstance().getFormBean().getSubmissionObjectById(AppConstants.TERMS_AND_CONDITIONS_ID)) != null) {
		frag = new TermAndConditionFragment();
	    } 
	    else {
		frag = new RebateConfirmationFragment();
	    }
	
	
	int id = v.getId();
	if (id == (R.id.btn_continue))
	{
	    PartListItem item = null;
	    boolean mandatoryFieldsFilled = true;
	    if (mSubmission != null && mSubmission.getItems() != null 
		    && mSubmission.getItems().get(0) != null 
		    && (item = mSubmission.getItems().get(0).getSurveyDetail()) != null) 
	    {
		List<Part> parts = item.getParts();
		if (parts != null && parts.size() > 0) {
		    mandatoryFieldsFilled = (UiUtil.validateForm(parts) == AppConstants.FORM_ITEM_FILLED_STATE.ITEM_COMPLETELY_SAVED.getState());
		}
	    }
	    if (mandatoryFieldsFilled) {
		((RootActivity) getActivity()).beginTransaction().replace(R.id.mainLayout, frag).addToBackStack(null).commit();
	    } else {
		UiUtil.showError(getActivity(), getString(R.string.alert_title), getString(R.string.alert_survey_desc));
	    }
	}
    }

    @Override
    public void bindCallbacks(View root) {
	// Unimpl
    }
    
    public Submission getItem()
    {
        return mSubmission;
    }

    public void setItem(Submission mItem)
    {
        this.mSubmission = mItem;
    }

    public String getTitle()
    {
        return mTitle;
    }

    public void setTitle(String mTitle)
    {
        this.mTitle = mTitle;
    }
}
