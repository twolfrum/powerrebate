package com.icf.rebate.ui.fragment;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.icf.rebate.adapter.DashboardAdapter;
import com.icf.rebate.app.model.DashboardItem;
import com.icf.rebate.networklayer.model.ConfigurationDetail;
import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.ui.RootActivity;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.dialog.ICFPINDialog;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.UiUtil;

public class DashboardFragment extends Fragment implements OnItemClickListener
{
    private ListView dashboardListView;

    private ConfigurationDetail configDetail;

    private ArrayList<DashboardItem> dashboardList;

    private DashboardAdapter dashboardAdapter;

    // private TextView dashboardHeader;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
	View view = inflater.inflate(R.layout.dashboard_screen, null);
	getUiControls(view);
	initialize();
	bindCallbacks();
	return view;
    }

    @Override
    public void onDestroyView()
    {
	super.onDestroyView();
	dashboardListView = null;
	configDetail = null;
	dashboardList = null;
	if (dashboardAdapter != null)
	{
	    dashboardAdapter.onDestroy();
	    dashboardAdapter = null;
	}
    }

    public void getUiControls(View root)
    {
	dashboardListView = (ListView) root.findViewById(R.id.dashboardlist);
    }

    public void initialize()
    {
	configDetail = LibUtils.getConfigurationDetail();
	dashboardList = LibUtils.getSelectedUtilityCompany().getDashboardItemList();
	dashboardAdapter = new DashboardAdapter(getActivity(), 0, dashboardList);
	dashboardAdapter.setAdapterListener(dashboardAdapter);
	dashboardListView.setAdapter(dashboardAdapter);
	((RootActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(false);
    }

    public void bindCallbacks()
    {
	dashboardListView.setOnItemClickListener(this);
    }

    // @Override
    // public String getFragmentTag()
    // {
    // return "dashboardfragment";
    // }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
	DashboardItem item = dashboardList.get(position);
	int dashId = LibUtils.parseInteger(item.getId(), -1);
	switch (dashId)
	{
	case AppConstants.ID_PENDING_JOBS:
	    // RootActivity ractivity = (RootActivity) getActivity();
	    // if (ractivity.getPendingJobItems() == null || ractivity.getPendingJobItems().size() == 0)
	    // {
	    // UiUtil.showError(getActivity(), getString(R.string.error_title), getString(R.string.no_pending_jobs));
	    // return;
	    // }
	    ICFPINDialog.validatePINForPendingJobs(getActivity());
	    // PendingJobsFragment pendingJobsFragment = new PendingJobsFragment();
	    // getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout,
	    // pendingJobsFragment).addToBackStack(null).commitAllowingStateLoss();
	    break;
	case AppConstants.ID_REBATE_TOOL:

	    // UiUtil.dumpDB(getActivity());
	    ((ActionBarActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
	    RebateManager.getInstance().setCurrentBean(null);
	    CustomerInformationFragment rebateTool = new CustomerInformationFragment();
	    ((RootActivity) getActivity()).beginTransaction().replace(R.id.mainLayout, rebateTool).addToBackStack(null).commitAllowingStateLoss();
	    break;
	default:
	    UiUtil.showNotImplemented(getActivity());
	    break;
	}
    }

    // @Override
    // public int getHeaderText()
    // {
    // return R.string.hdr_dashboard;
    // }
    //
    // @Override
    // public int getHeaderImg()
    // {
    // return R.drawable.icf_logo;
    // }
    //
    // public void updateCompanyDetails()
    // {
    // dashboardHeader.setText(UiUtil.selectedId.getName());
    // if (UiUtil.getResource(UiUtil.selectedId.getIcon()) == 0)
    // {
    // dashboardHeader.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icf_logo, 0, 0, 0);
    // }
    // else
    // {
    // dashboardHeader.setCompoundDrawablesWithIntrinsicBounds(UiUtil.getResource(UiUtil.selectedId.getIcon()), 0, 0, 0);
    // }
    // }
}
