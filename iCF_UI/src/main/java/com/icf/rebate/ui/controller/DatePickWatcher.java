package com.icf.rebate.ui.controller;

import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.networklayer.model.FormResponseBean.Options;
import com.icf.rebate.ui.util.UiViewUtils;

import android.app.DatePickerDialog;
import android.content.Context;
import android.widget.DatePicker;
import android.widget.TextView;

public class DatePickWatcher implements DatePickerDialog.OnDateSetListener {
    private TextView pickResult;
    private Options options;
    private Context context;

    public DatePickWatcher(TextView pickResult, Options options, Context context) {
        this.pickResult = pickResult;
        this.options = options;
        this.context = context;
    }

    public DatePickWatcher (TextView pickResult, Context context) {
        this.pickResult = pickResult;
        this.context = context;
    }

    public String getCurrentDate() {
        if (options == null) {
            return pickResult.getText().toString();
        } else {
            return options.getSavedValue();
        }
    }

    public TextView getPickResultView() {
        return pickResult;
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        String theDate = String.format("%1$02d/%2$02d/%3$d", month + 1, day, year);
        boolean flagMandatory = false;

        if (pickResult != null) {
            pickResult.setText(theDate);
        }

        if (options != null) {
            options.setSavedValue(theDate);
            if (options.isMandatory() && !options.isFieldFilled() && UiViewUtils.isValidationMode(options.isItemSaved())) {
                flagMandatory = true;
            } else {
                flagMandatory = false;
            }
        }

        if (flagMandatory) {
            pickResult.setBackground(context.getResources().getDrawable(R.drawable.bg_dialog_mandatory_box));
        } else {
            pickResult.setBackground(context.getResources().getDrawable(R.drawable.text_field_white));
        }
    }
}
