package com.icf.rebate.ui.fragment;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.icf.rebate.adapter.DuctImprovementAdapter;
import com.icf.rebate.app.model.DashboardItem.ServiceItem;
import com.icf.rebate.networklayer.model.CustomerInfo;
import com.icf.rebate.networklayer.model.DuctItem;
import com.icf.rebate.networklayer.model.FormResponseBean;
import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.ui.RootActivity;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.listeners.RequestCallFlow;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.UiUtil;

public class DuctImprovementFragment extends Fragment implements RequestCallFlow, OnItemClickListener
{

    private ListView lstView;

    private ArrayList<DuctItem> ductList;

    private DuctImprovementAdapter ductImprovementAdapter;

    private FormResponseBean bean;

    private String title;

    public DuctImprovementFragment()
    {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
	View view = inflater.inflate(R.layout.ductimprovement_screen, null, false);
	return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
	super.onViewCreated(view, savedInstanceState);
	lstView = (ListView) view.findViewById(R.id.ductlist);
	RootActivity rootAct = (RootActivity) getActivity();
	if (rootAct != null)
	{
	    rootAct.updateTitle(title);
	}
	RebateManager.getInstance().checkAndrequestData(this);

	CustomerInfo cInfo = RebateManager.getInstance().getFormBean().getCustomerInfo();
	if (UiUtil.isJobCompleted(cInfo))
	{
	    UiUtil.setEnableView((ViewGroup) view, false, null);
	}
    }

    @Override
    public void showProgressDialog()
    {
	getActivity().runOnUiThread(new Runnable()
	{
	    public void run()
	    {
		UiUtil.showProgressDialog(getActivity());
	    }
	});
    }

    @Override
    public void stopProgressDialog()
    {
	getActivity().runOnUiThread(new Runnable()
	{
	    public void run()
	    {
		UiUtil.dismissSpinnerDialog();
	    }
	});
    }

    @Override
    public void updateData(Object obj)
    {
	bean = (FormResponseBean) obj;

	getActivity().runOnUiThread(new Runnable()
	{
	    public void run()
	    {
		updateDuctList();
	    }
	});
    }

    private void updateDuctList()
    {
	if (bean != null)
	{
	    ductList = new ArrayList<DuctItem>(bean.getDuctImprovements());
	    ductImprovementAdapter = new DuctImprovementAdapter(getActivity(), 0, ductList);
	    ductImprovementAdapter.setAdapterListener(ductImprovementAdapter);
	    lstView.setAdapter(ductImprovementAdapter);
	    lstView.setOnItemClickListener(this);
	}
    }

    @Override
    public void displayError(String title, String message)
    {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
	if (position < ductList.size())
	{
	    DuctItem item = ductList.get(position);
	    if (item.getName().equalsIgnoreCase("Duct Sealing"))
	    {
		DuctSealingFragment fragment = new DuctSealingFragment();
		// fragment.setData(item);
		((RootActivity) getActivity()).beginTransaction().replace(R.id.mainLayout, fragment).addToBackStack(null).commit();
	    }
	    else if (item.getName().equalsIgnoreCase("Duct Insulation"))
	    {
		DuctInsulationFragment fragment = new DuctInsulationFragment();
		fragment.setData(item);
		((RootActivity) getActivity()).beginTransaction().replace(R.id.mainLayout, fragment).addToBackStack(null).commit();
	    }
	}
    }

    public void setTitle(String title)
    {
	this.title = title;
    }
}
