package com.icf.rebate.ui;

import com.crashlytics.android.Crashlytics;
import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.FileUtil;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import io.fabric.sdk.android.Fabric;

public class LicenseActivity extends FragmentActivity implements OnClickListener
{
    private WebView txtLicenseAgreement;    
    private Button btnAgree;
    private Button btnDisagree;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
	super.onCreate(savedInstanceState);
	// Uncomment to enable crashlytics
	Fabric.with(this, new Crashlytics());
	if(!hasLicenseAccepted()){
	    
        	setContentView(R.layout.activity_license);
        	
        	txtLicenseAgreement = (WebView) findViewById(R.id.licenseText);
        	btnAgree = (Button) findViewById(R.id.licenseagree);
        	btnDisagree = (Button) findViewById(R.id.licensedisagree);
        	
        	txtLicenseAgreement.loadUrl("file:///android_asset/www/license.html");
        	btnAgree.setOnClickListener(this);
        	btnDisagree.setOnClickListener(this);
	}
	else{
	    final Handler handler = new Handler();
		handler.postDelayed(new Runnable()
		{

		    @Override
		    public void run()
		    {
			Intent obj = new Intent(getApplicationContext(), SplashActivity.class);
			startActivity(obj);
			finish();
		    }
		}, 3000);
	}
    }

    @Override
    public void onClick(View v)
    {
	int id = v.getId();
	if (id == R.id.licenseagree)
	{
	    setSharedPreferences();
	    
	    final Handler handler = new Handler();
		handler.postDelayed(new Runnable()
		{
		    @Override
		    public void run()
		    {
			Intent obj = new Intent(getApplicationContext(), SplashActivity.class);
			startActivity(obj);
			finish();
		    }
		}, 3000);
	}
	else if (id == R.id.licensedisagree)
	{	   
            System.exit(0);
	}
    }
    
    private boolean hasLicenseAccepted()
    {
	SharedPreferences setting = getSharedPreferences(AppConstants.LICENSE_AGREEMENT, 0);
	
	return setting.getBoolean(AppConstants.LICENSE_AGREEMENT, false);
    }
    
    private void setSharedPreferences()
    {
	SharedPreferences setting = getSharedPreferences(AppConstants.LICENSE_AGREEMENT, 0);
	SharedPreferences.Editor editor = setting.edit();
        editor.putBoolean(AppConstants.LICENSE_AGREEMENT, true);
        editor.commit();
    }
}
