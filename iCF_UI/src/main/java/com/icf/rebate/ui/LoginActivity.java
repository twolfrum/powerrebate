package com.icf.rebate.ui;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.UnderlineSpan;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.View.OnLayoutChangeListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crittercism.app.Crittercism;
import com.google.android.gms.location.LocationServices;
import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.adapter.UtilityCompanyAdapter;
import com.icf.rebate.app.BackgroundService;
import com.icf.rebate.app.model.RBResponseBean;
import com.icf.rebate.networklayer.HttpData;
import com.icf.rebate.networklayer.HttpManagerListener;
import com.icf.rebate.networklayer.ICFHttpManager;
import com.icf.rebate.networklayer.model.ConfigurationDetail;
import com.icf.rebate.networklayer.model.ConfigurationDetail.UtilityCompany;
import com.icf.rebate.networklayer.model.LoginResponseBean;
import com.icf.rebate.networklayer.model.ResponseBean;
import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.dialog.DialogListener;
import com.icf.rebate.ui.dialog.ICFDialogFragment;
import com.icf.rebate.ui.dialog.ICFPINDialog;
import com.icf.rebate.ui.util.Analytics;
import com.icf.rebate.ui.util.FileUtil;
import com.icf.rebate.ui.util.ICFLogger;
import com.icf.rebate.ui.util.LoggerUtils;
import com.icf.rebate.ui.util.UiUtil;

public class LoginActivity extends FragmentActivity implements OnClickListener, DialogListener, HttpManagerListener, OnKeyListener
{

    private static final String TAG = LoginActivity.class.getName();

    // private LoginResponseBean resbean;

    // TODO: remove this later
    private ImageView imgSplash;

    private View welcome_header;

    private String strUserName;

    private String strPassword;

    private String strChangePassword;

    // private String loginError;

    private ICFDialogFragment commonDialog;

    // TODO: avoid keeping UI components as instance variables
    private EditText oldPasswordField, newPasswordField, confirmPasswordField;
    private EditText pin_1, pin_2, pin_3, pin_4;

    private int mTop;

    private boolean offlineMode;

	private static final int MY_PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE = 100;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.login_screen);
		TextView login = (TextView) findViewById(R.id.login_btn);
		login.setOnClickListener(this);
		// Forgot password textview
		TextView forgotPassword = (TextView) findViewById(R.id.tv_password_reset);
		forgotPassword.setOnClickListener(this);

		imgSplash = (ImageView) findViewById(R.id.splashImg);
		welcome_header = findViewById(R.id.welcome_header);
		((EditText) findViewById(R.id.login_username)).addOnLayoutChangeListener(new OnLayoutChangeListener() {

			@Override
			public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
				View parentView = (View) v.getParent();
				Rect r = new Rect();
				parentView.getGlobalVisibleRect(r);
				if (mTop == 0) {
					mTop = r.top;
				}
				final int diff = mTop - r.top;
				if (diff > 0) {
					welcome_header.setVisibility(View.GONE);
				} else {
					mTop = 0;
					welcome_header.setVisibility(View.VISIBLE);
				}
			}
		});
		String previousUsername = LibUtils.getAppInfo(LibUtils.KEY_APP_INFO_USERNAME, null);
		if (previousUsername != null) {
			((EditText) findViewById(R.id.login_username)).setText(previousUsername);
			((CheckBox) findViewById(R.id.account_remember_me)).setChecked(true);
		}

		// TODO: For testing will remove this later
		if (UiUtil.DEBUG_BUILD) {
			imgSplash.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
//		    if (!TextUtils.isEmpty(strUserName) && !TextUtils.isEmpty(strPassword))
//		    {
//			EditText password = (EditText) findViewById(R.id.login_password);
//			ICFHttpManager.getInstance().doReset(strUserName, password.getText().toString().trim());
//			LibUtils.setDataForUser(strUserName, LibUtils.KEY_USER_INFO_PIN, null);
//			// LibUtils.setPINForUser(LoginActivity.this, strUserName, null);
//		    }
					AlertDialog.Builder alertDialog = new AlertDialog.Builder(LoginActivity.this);
					alertDialog.setTitle("Connect Local Host");

					final EditText input = new EditText(LoginActivity.this);
					input.setText("http://localhost:9090");
					LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.MATCH_PARENT,
							LinearLayout.LayoutParams.MATCH_PARENT);
					input.setLayoutParams(lp);
					alertDialog.setView(input);

					alertDialog.setPositiveButton("Ok",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int which) {
									String localUrl = input.getText().toString();
									ICFHttpManager.getInstance().setServerUrl(localUrl);
									LibUtils.setServerUrl(localUrl);
								}
							});
					alertDialog.setNegativeButton("Cancel",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int which) {
									dialog.cancel();
								}
							});

					alertDialog.show();
				}
			});
			EditText userName = (EditText) findViewById(R.id.login_username);
			EditText password = (EditText) findViewById(R.id.login_password);

			userName.setText("icftest1");//praveen1");
			password.setText("abcde123");//mportal1234");

			strUserName = userName.getText().toString().trim();
			strPassword = password.getText().toString().trim();
			stopBackgroundService();
		}

		if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
					!= PackageManager.PERMISSION_GRANTED &&
				ContextCompat.checkSelfPermission(getApplicationContext(),
						android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

			// Should we show an explanation?
			if (ActivityCompat.shouldShowRequestPermissionRationale(this,
					Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
				// Show an explanation to the user *asynchronously* -- don't block
				// this thread waiting for the user's response! After the user
				// sees the explanation, try again to request the permission.
				boolean brkpt = true;
			} else {
				// No explanation needed, we can request the permission.
				ActivityCompat.requestPermissions(this,
						new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
						MY_PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE);
			}
		}
	}

    private void stopBackgroundService()
    {
	Intent t = new Intent(getApplicationContext(), BackgroundService.class);
	t.setAction(BackgroundService.STOP_SERVICE);
	startService(t);
    }

    @Override
    public void onDestroy()
    {
	UiUtil.dismissSpinnerDialog();
	super.onDestroy();
    }

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
										   @NonNull int[] grantResults) {
		switch (requestCode) {
			case MY_PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE: {
				// If request is cancelled, the result arrays are empty.
				if (grantResults.length > 0
						&& grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					boolean brkpt = true;
				} else {
					// permission denied, boo! Disable the
					// functionality that depends on this permission.
					boolean brkpt = true;
				}
				return;
			}
		}
	}

	@Override
    public void onClick(View v)
    {

	if (v.getId() == R.id.login_btn)
	{
	    doLogin();
	}
	else if ((v.getId() == R.id.login_done_btn) || (v.getId() == R.id.login_done_image))
	{
	    String oldPassword = oldPasswordField.getText().toString().trim();
	    String newPassword = newPasswordField.getText().toString().trim();
	    String confirmPassword = confirmPasswordField.getText().toString().trim();
	    if (newPassword != null && confirmPassword != null && newPassword.length() > 0 && confirmPassword.length() > 0)
	    {
		if (newPassword.equals(confirmPassword))
		{
		    changePasswordRequest(strUserName, oldPassword, newPassword);
		}
		else
		{

		    UiUtil.showError(LoginActivity.this, getResources().getString(R.string.alert_title), getResources().getString(R.string.password_mismatch));
		    oldPasswordField.setText("");
		    newPasswordField.setText("");
		    confirmPasswordField.setText("");
		    oldPasswordField.requestFocus();
		}

	    }
	    else
	    {
		UiUtil.showError(LoginActivity.this, getResources().getString(R.string.alert_title), getResources().getString(R.string.all_fields_mandatory));
	    }
	    hideKeyboard();
	}
	else if ((v.getId() == R.id.login_cancel_btn) || (v.getId() == R.id.login_cancel_image))
	{
	    newPasswordField.setText("");
	    confirmPasswordField.setText("");
	    if (commonDialog != null)
	    {
		commonDialog.dismiss();
	    }
	}
	// Reset password functionality
	else if (v.getId() == R.id.tv_password_reset) 
	{
	    final TextView tvForgotPassword = (TextView) v;
	    
	    // Give it an underline when clicked for a short moment
	    final String tvText = tvForgotPassword.getText().toString();
	    SpannableString spannable = new SpannableString(tvText);
	    spannable.setSpan(new UnderlineSpan(), 0, tvText.length(), 0);
	    tvForgotPassword.setText(spannable);
	    Handler handler = new Handler();
	    handler.postDelayed(new Runnable()
	    {
	        @Override
	        public void run()
	        {
	    		tvForgotPassword.setText(tvText); // Remove underline after 250 ms passed
	        }
	    }, 250);
	    
	    showPasswordResetDialog();
	}
	// else if ((v.getId() == R.id.change_pin_cancel_btn) || (v.getId() ==
	// R.id.change_pin_cancel_image))
	// {
	// if (commonDialog != null)
	// {
	// commonDialog.dismiss();
	// }
	// }
	// else if ((v.getId() == R.id.change_pin_done_btn) || (v.getId() ==
	// R.id.change_pin_done_image))
	// {
	// // Unnecessary condition, just append the text from all 4 text and
	// validate it
	// //if (pin_1 != null && pin_2 != null && pin_3 != null && pin_4 !=
	// null && pin_1.getText().toString().length() == 1 &&
	// pin_2.getText().toString().length() == 1 &&
	// pin_3.getText().toString().length() == 1 &&
	// pin_4.getText().toString().length() == 1)
	// // {
	// String strPin = pin_1.getText().toString() +
	// pin_2.getText().toString() + pin_3.getText().toString() +
	// pin_4.getText().toString();
	// if (validatePin(strPin))
	// {
	// LibUtils.setPINForUser(LoginActivity.this, strUserName, strPin);
	// doConfigRequest();
	// if (commonDialog != null)
	// {
	// commonDialog.dismiss();
	// }
	// }
	// // }
	// else
	// {
	// UiUtil.showDialog(getSupportFragmentManager(),
	// getResources().getString(R.string.alert_title),
	// getResources().getString(R.string.all_fields_mandatory));
	// }
	//
	// }

    }

    private void showPasswordResetDialog() {
	UiUtil.showPasswordResetDialog(this, this);
    }
    
    private void doConfigRequest()
    {
	ICFHttpManager.getInstance().doConfigRequest(this);
    }

    private void doRBRequest()
    {
	ICFHttpManager.getInstance().doResourceBundleRequest(this);
    }

    private void displaySetPINDialog()
    {
	UiUtil.dismissSpinnerDialog();
	ICFPINDialog.showSetPINDialog(this);
    }

    protected boolean validatePin(String strPin)
    {
	return strPin.length() == 4;
    }

    private void doLogin()
    {
		EditText userName = (EditText) findViewById(R.id.login_username);
		EditText password = (EditText) findViewById(R.id.login_password);

		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(password.getApplicationWindowToken(), 0);
		strUserName = userName.getText().toString().trim();
		strPassword = password.getText().toString().trim();
		// pref.edit().putString("userName", strUserName).commit();
		// pref.edit().putString("password", strPassword).commit();
		if (!TextUtils.isEmpty(strUserName) && !TextUtils.isEmpty(strPassword))
		{
			CheckBox rememberMe = (CheckBox) findViewById(R.id.account_remember_me);
			if (rememberMe.isChecked())
			{
			LibUtils.setAppInfo(LibUtils.KEY_APP_INFO_USERNAME, strUserName);
			}
			else
			{
			LibUtils.removeAppInfo(LibUtils.KEY_APP_INFO_USERNAME);
			}

			if (!ICFHttpManager.getInstance().checkLoginConnectivity(strUserName))
			{
				String pin = LibUtils.getDataForUser(strUserName, LibUtils.KEY_USER_INFO_PIN);
				if (!TextUtils.isEmpty(pin))
				{
					Analytics.reportLoginError(getString(R.string.no_network_connection), strUserName);
					ICFPINDialog.validatePINOfflineUse(strUserName, this, new Runnable()
					{
					public void run()
					{
						UiUtil.showProgressDialog(LoginActivity.this);
						ICFHttpManager.getInstance().login(LoginActivity.this, strUserName, strPassword);
					}
					});
					return;
				}
				else
				{
					Message msg = new Message();
					msg.arg1 = ICFHttpManager.REQ_ID_LOGIN;
					msg.obj = LibUtils.getApplicationContext().getResources().getString(R.string.no_network_connection);
					networkConnectivityError(msg);
					return;
				}
			}
			UiUtil.showProgressDialog(this);
			ICFHttpManager.getInstance().login(this, strUserName, strPassword);
		}
		else
		{
			UiUtil.showError(LoginActivity.this, getResources().getString(R.string.error_title), getResources().getString(R.string.userCredentialError));
		}
    }

    private void displayChangePasswordScreen()
    {
	UiUtil.dismissSpinnerDialog();
	commonDialog = ICFDialogFragment.newDialogFrag(R.layout.login_change_password);
	commonDialog.setDialogListener(this);
	commonDialog.showCommitAllowingLoss(getSupportFragmentManager(), "changepassword");
	getSupportFragmentManager().executePendingTransactions();
    }

    protected void changePasswordRequest(String userName, String oldPassword, String newPassword)
    {
	UiUtil.showProgressDialog(this);
	strChangePassword = newPassword;
	ICFHttpManager.getInstance().changePassword(this, userName, oldPassword, newPassword);
    }

    private void refreshUtilitySelectionScreen()
    {
	try
	{
	    if (commonDialog != null)
	    {
		ListView listView = (ListView) commonDialog.getDialog().findViewById(R.id.utilityList);
		((BaseAdapter) listView.getAdapter()).notifyDataSetChanged();
	    }
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
    }

    public void displayUtilitySelectionScreen()
    {
	List<UtilityCompany> list = LibUtils.getConfigurationDetail().getUtilityList();
	if (list == null || list.size() == 0)
	{
	    // TODO: show error message
	}
	else if (list.size() == 1)
	{
	    UtilityCompany selectedCompany = list.get(0);
	    LibUtils.setSelectedUtilityCompany(selectedCompany);
	    FileUtil.createCurrentUserDirectory();
	    launchMainScreen();
	}
	else
	{
	    String previousId = LibUtils.getDataForUser(LibUtils.getLoggedInUserBean().getUserName(), LibUtils.KEY_USER_INFO_UTILITY_COMPANY);
	    int id = LibUtils.parseInteger(previousId, -1);
	    if (id != -1)
	    {
		for (UtilityCompany utilityCompany : list)
		{
		    if (id == utilityCompany.getId())
		    {
			LibUtils.setSelectedUtilityCompany(utilityCompany);
			FileUtil.createCurrentUserDirectory();
			launchMainScreen();
			return;
		    }
		}
	    }
	    commonDialog = ICFDialogFragment.newDialogFrag(R.layout.dialog_utility_selection);
	    commonDialog.setDialogListener(this);
	    commonDialog.showCommitAllowingLoss(getSupportFragmentManager(), "utilityselection");
	    getSupportFragmentManager().executePendingTransactions();
	}
    }

    private void launchMainScreen()
    {
	Analytics.reportLoginSuccess(!offlineMode);
	RebateManager.getInstance().clearData();
	LibUtils.RESET_PENDING_JOBS = true;

	Intent intent = new Intent(getApplicationContext(), RootActivity.class);
	intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
	startActivityForResult(intent, 100);
	finish();
    }

    @Override
    public void onBackPressed()
    {
	super.onBackPressed();
	finish();
    }

    @Override
    public void onFinishInflate(int layoutId, View view)
    {
	if (layoutId == R.layout.dialog_utility_selection)
	{
	    handleUtilitySelection(view);
	}
	else if (layoutId == R.layout.login_pin_change)
	{
	    // handlePinChange(view);
	}
	else if (layoutId == R.layout.login_change_password)
	{
	    handlePasswordChange(view);
	}
    }

    private void handleUtilitySelection(View view)
    {
	if (view == null)
	{
	    return;
	}
	ListView listView = (ListView) view.findViewById(R.id.utilityList);
	if (listView != null)
	{
	    listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

	    final ConfigurationDetail configResponse = LibUtils.getConfigurationDetail();
	    UtilityCompanyAdapter adapter = new UtilityCompanyAdapter(this, 0, configResponse.getUtilityList());
	    adapter.setAdapterListener(adapter);
	    listView.setAdapter(adapter);
	    listView.setOnItemClickListener(new OnItemClickListener()
	    {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id)
		{
		    ArrayList<UtilityCompany> uti = configResponse.getUtilityList();
		    if (uti != null)
		    {
			UtilityCompany company = uti.get(position);
			LibUtils.setSelectedUtilityCompany(company);
			FileUtil.createCurrentUserDirectory();
		    }
		    UtilityCompanyAdapter adapter = (UtilityCompanyAdapter) parent.getAdapter();
		    adapter.notifyDataSetChanged();
		    launchMainScreen();
		}
	    });
	}
    }

    @Override
    public void onDismiss()
    {

    }

    @Override
    public void connectionError(Message msg)
    {
	if (msg.arg1 == ICFHttpManager.REQ_ID_LOGIN)
	{
	    String message = null;
	    if (msg.obj instanceof HttpData)
	    {
		message = ((HttpData) msg.obj).getStatus().getReasonPhrase();
	    }
	    else if (msg.obj instanceof String)
	    {
		message = (String) msg.obj;
	    }
	    // loginError = message;
	    Analytics.reportLoginError(message, strUserName);
	}
	UiUtil.showNetworkError(msg, this);
    }

    @Override
    public void networkConnectivityError(Message msg)
    {
	connectionError(msg);
    }

    // TODO: find a better way to run this on UI thread
    @Override
    public void dataReceived(final Message msg)
    {
	switch (msg.arg1)
	{
	case ICFHttpManager.REQ_ID_RESOURCE_BUNDLE:
	{
	    RBResponseBean bean = (RBResponseBean) msg.obj;
	    if (bean != null)
	    {
//$$$TJW suppress loading of new resource files
//to test with existing resources
		unzipResourceBundle(bean);
		doConfigRequest();
		// runOnUiThread(new Runnable()
		// {
		// @Override
		// public void run()
		// {
		// refreshUtilitySelectionScreen();
		// }
		// });
	    }
	}
	    break;
	case ICFHttpManager.REQ_ID_LOGIN:
	{
	    offlineMode = false;
	    if (msg.getData() != null)
	    {
		offlineMode = msg.getData().getBoolean("offlineData");
	    }
	    final LoginResponseBean bean = (LoginResponseBean) msg.obj;
	    runOnUiThread(new Runnable()
	    {
		@Override
		public void run()
		{
		    if (bean != null && bean.isSuccessResponse())
		    {
			Crittercism.setUsername(bean.getUserName());
			// server is not sending password in response to setting
			// it manually
			bean.setPassword(strPassword);
			LibUtils.setLoggedInUserBean(bean);
			if (bean.shouldChangePassword())
			{
			    UiUtil.dismissSpinnerDialog();
			    displayChangePasswordScreen();
			}
			else
			{
			    // doConfigRequest();
			    doRBRequest();
			}
		    }
		    else
		    {
			Analytics.reportLoginError(bean.getStatusMessage(), strUserName);
			UiUtil.dismissSpinnerDialog();
			UiUtil.showError(LoginActivity.this, getString(R.string.error_title), bean.getStatusMessage());
		    }
		}
	    });
	}
	    break;
	case ICFHttpManager.REQ_ID_CONFIG:
	{
	    final ConfigurationDetail bean = (ConfigurationDetail) msg.obj;
	    LibUtils.setConfigurationDetail(bean);
	    LibUtils.processRebateFiles();

	    if (bean != null)
	    {
		String analyticsKey = bean.getAnalyticsKey();
		if (analyticsKey != null && !TextUtils.isEmpty(analyticsKey))
		{
		    String storedKey = LibUtils.getAppInfo(LibUtils.KEY_ANALYTICS, null);
		    if ((storedKey != null && !analyticsKey.equalsIgnoreCase(storedKey)) || storedKey == null)
		    {
			LibUtils.setAppInfo(LibUtils.KEY_ANALYTICS, analyticsKey);
			Analytics.init(getApplication(), analyticsKey);
		    }
		}
		if (bean.isReportIssue())
		{
		    try
		    {
			LoggerUtils.getInstance().setupAppLogging(getApplicationContext());
		    }
		    catch (IOException e)
		    {
			e.printStackTrace();
		    }
		}
		else
		{
		    LoggerUtils.getInstance().deleteAppLog(getApplicationContext());
		    LoggerUtils.getInstance().cleanupLoggingProcess();
		}
	    }
	    runOnUiThread(new Runnable()
	    {
		@Override
		public void run()
		{
		    if (bean != null && bean.isSuccessResponse())
		    {
			if (commonDialog != null)
			{
			    commonDialog.dismiss();
			}
			String pin = LibUtils.getDataForUser(strUserName, LibUtils.KEY_USER_INFO_PIN);
			// String pin = LibUtils.getPINForUser(LoginActivity.this, strUserName);
			if (TextUtils.isEmpty(pin))
			{
			    UiUtil.dismissSpinnerDialog();
			    displaySetPINDialog();
			}
			else
			{
			    displayUtilitySelectionScreen();
			}
		    }
		    else
		    {
			UiUtil.dismissSpinnerDialog();
			if (bean != null)
			{
			    UiUtil.showError(LoginActivity.this, getString(R.string.error_title), bean.getStatusMessage());
			}
		    }
		}
	    });
	}
	    break;
	case ICFHttpManager.REQ_ID_CHANGE_PASSWORD:
	{
	    runOnUiThread(new Runnable()
	    {

		@Override
		public void run()
		{
		    LoginResponseBean bean = (LoginResponseBean) msg.obj;
		    // server is not sending password in the response hence setting the
		    // password manually
		    bean.setPassword(strChangePassword);
		    strChangePassword = null;
		    LibUtils.setLoggedInUserBean(bean);
		    // pref.edit().putString("password", strChangePassword).commit();
		    if (bean != null)
		    {
			if (bean.isSuccessResponse())
			{
			    LibUtils.setLoggedInUserBean(bean);
			    doConfigRequest();
			    if (commonDialog != null)
			    {
				commonDialog.dismiss();
			    }
			}
			else
			{
			    UiUtil.dismissSpinnerDialog();
			    UiUtil.showError(LoginActivity.this, getString(R.string.error_title), bean.getStatusMessage());
			}
		    }

		}
	    });

	}
	    break;
	case ICFHttpManager.REQ_ID_RESET_PASSWORD: {
	    final Activity activity = this;
	    runOnUiThread(new Runnable()
	    {
	        @Override
	        public void run()
	        {
	            ResponseBean bean = (ResponseBean) msg.obj;
	            if (bean != null) {
	        	UiUtil.showError(activity, getString(R.string.forgot_password), bean.getStatusMessage());
	            }
	            else {
	        	UiUtil.showError(activity, getString(R.string.forgot_password), getString(R.string.error_reset_password));
	            }
	        }
	    });
	    
	}
	break;
	}
    }

    private void unzipResourceBundle(RBResponseBean bean)
    {
	ZipInputStream zis = null;
	try
	{
	    // delete old RB folder
	    File rbdir = getExternalFilesDir(LibUtils.FILES_RB);
	    if (rbdir.exists())
	    {
		FileUtil.deleteFolder(rbdir, false);
	    }

	    // Varun : For development purpose.
	    // zis = new ZipInputStream(new ByteArrayInputStream(FileUtil.readInputStream(new FileInputStream(new File(Environment.getExternalStorageDirectory()
	    // + "/icf/", "response.zip")))));
	    zis = new ZipInputStream(new ByteArrayInputStream(bean.getRBData()));
	    ZipEntry ze;
	    while ((ze = zis.getNextEntry()) != null)
	    {
		try
		{
		    ByteArrayOutputStream baos = new ByteArrayOutputStream();
		    byte[] buffer = new byte[1024];
		    int count;
		    while ((count = zis.read(buffer)) != -1)
		    {
			baos.write(buffer, 0, count);
		    }
		    String filename = ze.getName();
		    byte[] bytes = baos.toByteArray();
		    File file = new File(getExternalFilesDir(LibUtils.FILES_RB), filename);
		    FileOutputStream fout = new FileOutputStream(file);
		    fout.write(bytes);
		    fout.close();
		    ICFLogger.d(TAG, "Writing " + filename + " data size : " + bytes.length);
		}
		catch (Exception e)
		{
		    ICFLogger.e(TAG, e);
		}
	    }
	}
	catch (Exception e)
	{
	    ICFLogger.e(TAG, e);
	}
	finally
	{
	    try
	    {
		if (zis != null)
		{
		    zis.close();
		}
	    }
	    catch (Exception e)
	    {
		ICFLogger.e(TAG, e);
	    }
	}
    }

    private void handlePasswordChange(View view)
    {
	newPasswordField = (EditText) view.findViewById(R.id.login_new_password_field);
	confirmPasswordField = (EditText) view.findViewById(R.id.login_confirm_password_field);
	oldPasswordField = (EditText) view.findViewById(R.id.login_old_password_field);
	RelativeLayout done = (RelativeLayout) view.findViewById(R.id.login_done_btn);
	RelativeLayout cancel = (RelativeLayout) view.findViewById(R.id.login_cancel_btn);
	Button cancel_image = (Button) view.findViewById(R.id.login_cancel_image);
	Button done_image = (Button) view.findViewById(R.id.login_done_image);
	done.setOnClickListener(this);
	cancel.setOnClickListener(this);
	cancel_image.setOnClickListener(this);
	done_image.setOnClickListener(this);

    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event)
    {
	if (keyCode == KeyEvent.KEYCODE_DEL)
	{
	    int id = v.getId();
	    if (id == R.id.pin_2)
	    {
		if (pin_2.getText().toString().length() == 0)
		{
		    pin_1.post(new Runnable()
		    {
			public void run()
			{
			    pin_1.requestFocus();
			}
		    });
		}
	    }
	    else if (id == R.id.pin_3)
	    {
		if (pin_3.getText().toString().length() == 0)
		{
		    pin_2.post(new Runnable()
		    {
			public void run()
			{
			    pin_2.requestFocus();
			}
		    });
		}
	    }
	    else if (id == R.id.pin_4)
	    {
		if (pin_4.getText().toString().length() == 0)
		{
		    pin_3.post(new Runnable()
		    {
			public void run()
			{
			    pin_3.requestFocus();
			}
		    });
		}
	    }
	    else
	    {
	    }
	}
	return false;
    }

    public void hideKeyboard()
    {
	InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
	imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }

}
