package com.icf.rebate.ui.util;

import java.util.HashMap;
import java.util.Map;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.CalendarContract.Attendees;
import android.telephony.TelephonyManager;

import com.icf.rebate.networklayer.ConnectionManager;
import com.icf.rebate.networklayer.utils.LibUtils;
import com.localytics.android.DatapointHelper;
import com.localytics.android.LocalyticsActivityLifecycleCallbacks;
import com.localytics.android.LocalyticsSession;

public class Analytics
{

    private static final String TAG = Analytics.class.getName();

    private static final String EVENT_SUBMISSION_FAILURE = "SUBMISSION_FAILURE";
    private static final String EVENT_SUBMISSION_SUCCESS = "SUBMISSION_SUCCESS";
    private static final String EVENT_LOGIN_SUCCESS = "LOGIN_SUCCESS";
    private static final String EVENT_LOGIN_ERROR = "LOGIN_ERROR";

    private static final String KEY_USERNAME = "User Name";
    private static final String KEY_UTILITY = "Utility";
    private static final String KEY_ONLINE_MODE = "onlineMode";
    private static final String KEY_REASON = "Reason";
    private static final String KEY_NETWORK_TYPE = "Network Type";
    private static final String KEY_DEVICE_ID = "Device ID";

    private static LocalyticsSession localyticsSession;

    public static void init(Application app, String key)
    {
	// Instantiate the object
	Analytics.localyticsSession = new LocalyticsSession(app.getApplicationContext(), key);
	// Register LocalyticsActivityLifecycleCallbacks
	app.registerActivityLifecycleCallbacks(new LocalyticsActivityLifecycleCallbacks(Analytics.localyticsSession));
    }

    public static void tagEvent(String event)
    {
	localyticsSession.tagEvent(event);
    }

    public static void tagEvent(String event, Map<String, String> attributes)
    {
	ICFLogger.d(TAG, "Logging Analytics Event : " + event + (attributes != null ? ", Attributes : " + attributes.toString() : ""));
	localyticsSession.tagEvent(event, attributes);
    }

    public static void reportLoginError(String reason, String username)
    {
	try
	{
	    HashMap<String, String> map = new HashMap<>();
	    if (reason != null)
	    {
		map.put(KEY_REASON, reason);
	    }
	    String networkType = getNetworkType(LibUtils.getApplicationContext());
	    if (networkType != null)
	    {
		map.put(KEY_NETWORK_TYPE, networkType);
	    }
	    map.put(KEY_USERNAME, username);
	    tagEvent(EVENT_LOGIN_ERROR, map);
	}
	catch (Exception e)
	{
	    ICFLogger.e(TAG, e);
	}
    }

    public static void reportLoginSuccess(boolean onlineMode)
    {
	try
	{
	    HashMap<String, String> map = new HashMap<>();
	    map.put(KEY_USERNAME, LibUtils.getLoggedInUserBean().getUserName());
	    map.put(KEY_UTILITY, LibUtils.getSelectedUtilityCompany().getName());
	    map.put(KEY_ONLINE_MODE, onlineMode + "");
	    String networkType = getNetworkType(LibUtils.getApplicationContext());
	    if (networkType != null)
	    {
		map.put(KEY_NETWORK_TYPE, networkType);
	    }
	    String deviceId = DatapointHelper.getAndroidIdOrNull(LibUtils.getApplicationContext());
	    if (deviceId != null)
	    {
		map.put(KEY_DEVICE_ID, deviceId);
	    }
	    tagEvent(EVENT_LOGIN_SUCCESS, map);
	}
	catch (Exception e)
	{
	    ICFLogger.e(TAG, e);
	}
    }

    private static String getNetworkType(Context context)
    {
	try
	{
	    if (!ConnectionManager.isOnline())
	    {
		return "None";
	    }

	    ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
	    if (mWifi.isConnected())
	    {
		return "WiFi";
	    }

	    TelephonyManager mTelephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
	    int networkType = mTelephonyManager.getNetworkType();
	    switch (networkType)
	    {
	    case TelephonyManager.NETWORK_TYPE_GPRS:
	    case TelephonyManager.NETWORK_TYPE_EDGE:
	    case TelephonyManager.NETWORK_TYPE_CDMA:
	    case TelephonyManager.NETWORK_TYPE_1xRTT:
	    case TelephonyManager.NETWORK_TYPE_IDEN:
		return "2G";
	    case TelephonyManager.NETWORK_TYPE_UMTS:
	    case TelephonyManager.NETWORK_TYPE_EVDO_0:
	    case TelephonyManager.NETWORK_TYPE_EVDO_A:
	    case TelephonyManager.NETWORK_TYPE_HSDPA:
	    case TelephonyManager.NETWORK_TYPE_HSUPA:
	    case TelephonyManager.NETWORK_TYPE_HSPA:
	    case TelephonyManager.NETWORK_TYPE_EVDO_B:
	    case TelephonyManager.NETWORK_TYPE_EHRPD:
	    case TelephonyManager.NETWORK_TYPE_HSPAP:
		return "3G";
	    case TelephonyManager.NETWORK_TYPE_LTE:
		return "LTE";
	    default:
		return null;
	    }
	}
	catch (Exception e)
	{
	    ICFLogger.e(TAG, e);
	}
	return null;
    }

    public static void reportSubmissionFailure(String reason)
    {
	try
	{
	    HashMap<String, String> map = new HashMap<>();
	    if (reason != null)
	    {
		map.put(KEY_REASON, reason);
	    }
	    map.put(KEY_USERNAME, LibUtils.getLoggedInUserBean().getUserName());
	    map.put(KEY_UTILITY, LibUtils.getSelectedUtilityCompany().getName());
	    tagEvent(EVENT_SUBMISSION_FAILURE, map);
	}
	catch (Exception e)
	{
	    ICFLogger.e(TAG, e);
	}
    }

    public static void reportSubmissionSuccess()
    {
	try
	{
	    Map<String, String> attr = new HashMap<String, String>();
	    attr.put(KEY_USERNAME, LibUtils.getLoggedInUserBean().getUserName());
	    attr.put(KEY_UTILITY, LibUtils.getSelectedUtilityCompany().getName());
	    tagEvent(EVENT_SUBMISSION_SUCCESS, attr);
	}
	catch (Exception e)
	{
	    ICFLogger.e(TAG, e);
	}
    }
}
