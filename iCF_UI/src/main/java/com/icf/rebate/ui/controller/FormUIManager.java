package com.icf.rebate.ui.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.Map;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.google.zxing.client.android.CaptureActivity;
import com.google.zxing.client.android.Intents;
import com.icf.ameren.rebate.ui.BuildConfig;
import com.icf.rebate.adapter.CustomSpinnerAdapter;
import com.icf.rebate.networklayer.model.CustomerInfo;
import com.icf.rebate.networklayer.model.FormResponseBean;
import com.icf.rebate.networklayer.model.FormResponseBean.Equipment;
import com.icf.rebate.networklayer.model.QIVItem;
import com.icf.rebate.networklayer.model.FormResponseBean.Item;
import com.icf.rebate.networklayer.model.FormResponseBean.Options;
import com.icf.rebate.networklayer.model.FormResponseBean.Options.Reference;
import com.icf.rebate.networklayer.model.FormResponseBean.Part;
import com.icf.rebate.networklayer.model.PartListItem;
import com.icf.rebate.ui.FormFragment;
import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.ui.dialog.ICFDialogFragment;
import com.icf.rebate.ui.fragment.DuctSealingPagerFragment;
import com.icf.rebate.ui.fragment.PersistentForm;
import com.icf.rebate.ui.fragment.QIVFragment;
import com.icf.rebate.ui.fragment.ThermoStatInstallPagerFragment;
import com.icf.rebate.ui.fragment.TuneupFragment;
import com.icf.rebate.ui.fragment.TuneupPagerFragment;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.FileUtil;
import com.icf.rebate.ui.util.ICFLogger;
import com.icf.rebate.ui.util.UiUtil;
import com.icf.rebate.ui.util.UiViewUtils;

public class FormUIManager
{
    public static final String TAG = FormUIManager.class.getSimpleName();
    private EditText edit = null;
    private ICFDialogFragment commonDialog;
    private FormViewCreationCallback callBackListener;
    private Item item = null;
    private QIVItem qivItem = null;
    private String managerId;
    private Hashtable<String, View> exViewHashTable;
    private HashMap<EditText, ArrayList<EditText>> referenceMap;
    private int partitionSize = -1;

    private List<Part> mPartList = null;

    public FormUIManager(String id)
    {
	managerId = id;
    }

    public void createViews(Fragment fragment, ViewGroup parent, Item item) {
        exViewHashTable = new Hashtable<String, View>();
        referenceMap = new HashMap<EditText, ArrayList<EditText>>();
        partitionSize = -1;
        if (parent != null && item != null) {
            this.item = item;
            List<Part> partList = new ArrayList<Part>();

            if (item.getReplaceOnFail() != null && item.getReplaceOnFail().getParts() != null) {
                partList.addAll(item.getReplaceOnFail().getParts());
            }

            if (!item.isReplaceOnFailSelected() && item.getEarlyRetirement() != null && item.getEarlyRetirement().getParts() != null) {
                partitionSize = partList.size();

                partList.addAll(item.getEarlyRetirement().getParts());
                partList.remove(new Part(AppConstants.EQUIPMENTS_RESULT_ID));
            }

            ListIterator<Part> iterator = partList.listIterator();
            int count = 0;
            while (iterator.hasNext()) {
                Part part = (Part) iterator.next();

                boolean isFormSaved = item.getFormFilledState() != AppConstants.FORM_ITEM_FILLED_STATE.ITEM_UNEDITED.getState();
                if (partitionSize != -1) {
                    if (count < partitionSize - 1) {
                        partBackground = R.drawable.part_bg;
                    } else {
                        partBackground = R.drawable.early_retirement_bg;
                    }
                } else {
                    partBackground = R.drawable.part_bg;
                }
                parent.addView(createPartView(fragment, part, partList, isFormSaved));
                count++;
            }
        }
    }

    public void createViews(Fragment fragment, ViewGroup parent, List<Part> partList, boolean isItemSaved, QIVItem item) {
        this.qivItem = item;
        createViews(fragment, parent, partList, isItemSaved);
    }

    public void createViews(Fragment fragment, ViewGroup parent, List<Part> partList, boolean isItemSaved) {
        mPartList = partList;
        exViewHashTable = new Hashtable<String, View>();
        if (parent != null && partList != null) {
            ListIterator<Part> iterator = partList.listIterator();
            while (iterator.hasNext()) {
                Part part = (Part) iterator.next();

                View view = createPartView(fragment, part, partList, isItemSaved);
                if (view != null) {
                    parent.addView(view);
                }
            }
        }
    }

    private ViewGroup createPartView(final Fragment fragment, final Part part, final List<Part> partList, boolean isItemSaved)
    {
	final Context context = fragment.getActivity();
	final ViewGroup partLayout = UiViewUtils.createPartLayout(context, partBackground);

	if (part != null)
	{
	    part.setItemSaved(isItemSaved);
	    boolean isDuplicatePart = part.getDuplicatePartId() > -1;
	    ViewGroup group = null;

	    if (part.getName() != null)
	    {
		TextView addButton = null;
		TextView deleteButton = null;

		if (part.getId() != null)
		{
		    String id = part.getId();
		    if (id.equalsIgnoreCase(AppConstants.NEW_AIR_HANDLER_UNIT))
		    {
			if (isDuplicatePart)
			{
			    deleteButton = createDeleteButtonView(fragment, part, context, partLayout, partList);
			}
			else
			{
			    addButton = createAddButtonView(fragment, part, context, partLayout, partList, isItemSaved);
			}
		    }
		}

		TextView partLabel = UiViewUtils.createTextView(context, part.getName(), 20);
		partLabel.setPadding(0, 0, 0, 10);
		partLabel.setTextColor(context.getResources().getColor(R.color.header_bg));

		if (addButton != null || deleteButton != null)
		{
		    group = UiViewUtils.createHorizontalViewForAddDelete(context, partLabel, addButton != null ? addButton : deleteButton);

		    // // UiViewUtils.addChildrenToViewGroup(context, group, partLabel);
		    //
		    // if (addButton != null)
		    // {
		    // UiViewUtils.addChildrenToViewGroup(context, group, addButton);
		    // }
		    //
		    // if (deleteButton != null)
		    // {
		    // UiViewUtils.addChildrenToViewGroup(context, group, deleteButton);
		    // }

		}
		partLayout.addView(group != null ? group : partLabel);
	    }
	    if (part.getOptions() != null)
	    {
		ListIterator<Options> iterator = part.getOptions().listIterator();
		while (iterator.hasNext())
		{
		    final Options options = (Options) iterator.next();

		    if (!options.isHidden())
		    {
			options.setItemSaved(isItemSaved);
			if (options.getInputType() == null && options.getName() != null || options.getInputType() != null && options.getInputType().equals("TV"))
			{
			    TextView optionLabel = UiViewUtils.createTextView(context, options.getName(), 17);
			    optionLabel.setTextColor(context.getResources().getColor(R.color.header_bg));
			    partLayout.addView(optionLabel);

                final EditText editView = createOptionsEditView(fragment, part, options);
                group = null;
                group = createBarcodeView(fragment, options, editView, group);
                partLayout.addView(group != null ? group : editView);
                if (editView != null && !TextUtils.isEmpty(options.getExternalReferId())) {
                    exViewHashTable.put(options.getExternalReferId(), editView);
                }

			    if (callBackListener != null)
			    {
				    callBackListener.viewCreated(editView, part, options);
			    }
			}
			else if (options.getInputType() != null)
			{
			    if (options.getInputType().equalsIgnoreCase("CO"))
			    {
				if (options.getName() != null)
				{
				    partLayout.addView(UiViewUtils.createTextView(context, options.getName(), 17));
				}
				createComboBox(fragment, partLayout, part, options);
			    }
			    else if (options.getInputType().equalsIgnoreCase("C"))
			    {
				createCheckbox(fragment, partLayout, part, options);
			    }
			    else if (options.getInputType().equalsIgnoreCase("SW"))
			    {
				createSwitcher(fragment, partLayout, options);
			    }
			    else if (options.getInputType().equalsIgnoreCase("AS"))
			    {
				partLayout.addView(UiViewUtils.createTextView(context, options.getName(), 17));
				List<String> autosuggestList = null;

				// if (equipmentName != null)
				{
				    autosuggestList = RebateManager.getInstance().getAutoSuggestList(options.getSuggestion());
				}

				if (autosuggestList == null)
				{
				    autosuggestList = new ArrayList<String>();
				}
				AutoCompleteTextView view = (AutoCompleteTextView) createAutoCompleteTextView(fragment, partLayout, options, autosuggestList, part);
				// createReferenceCheckbox(fragment, partList, partLayout, options, view);
			    }
			    else if (options.getInputType().equalsIgnoreCase("B"))
			    {
				partLayout.addView(UiViewUtils.createButtonView(context, options.getName(), new OnClickListener()
				{
				    private void showPackageError(final String packageName, String errorMessage)
				    {
					UiUtil.showError(fragment.getActivity(), fragment.getString(R.string.alert_title), errorMessage, R.string.alert_install, R.string.alert_cancel, new DialogInterface.OnClickListener()
					{

					    @Override
					    public void onClick(DialogInterface dialog, int which)
					    {
						if (which == DialogInterface.BUTTON_POSITIVE)
						{
						    Intent t = new Intent(Intent.ACTION_VIEW);
						    String uri = "market://details?id=" + packageName;
						    t.setData(Uri.parse(uri));
						    fragment.startActivity(t);
						}
					    }
					});
				    }

				    @Override
				    public void onClick(View v)
				    {
					if (v != null && v.getRootView() != null)
					{
					    v.getRootView().clearFocus();
					}
					if (options.getId() != null && options.getId().equalsIgnoreCase(AppConstants.LAUNCH_IMANIFOLD))
					{
					    if (options.getPackageName() != null)
					    {
						PackageInfo pInfo = null;
						try
						{
						    pInfo = fragment.getActivity().getPackageManager().getPackageInfo(options.getPackageName(), PackageManager.GET_META_DATA);
						}
						catch (NameNotFoundException e)
						{
						    e.printStackTrace();
						}
						if (pInfo == null)
						{
						    showPackageError(options.getPackageName(), fragment.getString(R.string.imanifold_app_error));
						}
						else
						{
						    if (!TextUtils.isEmpty(options.getLaunchUri()))
						    {
							String temp = managerId;
							if (managerId.length() > 20)
							{
							    // temp = managerId.substring(0, 20);
							    temp = managerId.substring(0, 18);
							}
							String key = temp + System.currentTimeMillis();
							ICFLogger.d(TAG, "iManiFold Launch Uri : " + options.getLaunchUri() + key);
							String index = "0";
							if (fragment instanceof ThermoStatInstallPagerFragment)
							{
							    index = ((ThermoStatInstallPagerFragment) fragment).getPageName();
							}
							else if (fragment instanceof TuneupFragment)
							{
							    index = ((TuneupFragment) fragment).getPageName();
							}
							else if (fragment instanceof QIVFragment && qivItem != null)
							{
							    equipmentName = qivItem.getEquipmentId();
							    index = "" + qivItem.getPageIndex();
							}
							String uri = createLaunchURI(options.getLaunchUri() + key);
							Intent t = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
							// Intent t = new Intent(Intent.ACTION_VIEW, Uri.parse("imanifold://startnewsession?key=uniquekey"));
							ResolveInfo rInfo = fragment.getActivity().getPackageManager().resolveActivity(t, PackageManager.MATCH_DEFAULT_ONLY);
							if (rInfo == null)
							{
							    showPackageError(options.getPackageName(), fragment.getString(R.string.imanifold_app_update_error));
							}
							else
							{
							    fragment.startActivity(t);
							    UiUtil.currentFormManager = FormUIManager.this;
							    UiUtil.manifoldKey = key;

							    Options manifoldOption = new Options();
							    manifoldOption.setMandatory("false");
							    // manifoldOption.setId(part.getId());
							    manifoldOption.setXmlName(AppConstants.manifold_xmlName);
							    manifoldOption.setId(index);
							    if (part.getOptions() != null)
							    {
								for (Options option : part.getOptions())
								{
								    if (option.getXmlName() != null && option.getXmlName().equalsIgnoreCase(manifoldOption.getXmlName()))
								    {
									part.removeOptions(option);
									break;
								    }
								}
							    }
							    UiUtil.manifoldOptions = manifoldOption;
							    part.addOptions(manifoldOption);
							}

						    }
						    // fragment.startActivityForResult(new Intent(fragment.getActivity(), IManifoldDummyActivity.class),
						    // IManifoldDummyActivity.IManifoldResultCode);
						}
					    }
					}
				    }
				}));
			    } //if (options.getInputType().equalsIgnoreCase("B"))
			    else if (options.getInputType().equalsIgnoreCase("DF"))
			    {
				//create clickable text view to activate date-picker widget
				final TextView dateView = createDatePickView(fragment, options);
				partLayout.addView(group != null ? group : dateView);
			    }
			} // END (options.getInputType() != null)
            else if (options.getDataType() != null) {
                final EditText editView = createPartEditView(fragment, partLayout, part, options);
                String staticValue = options.getStaticDisplayedValue();
                if (!TextUtils.isEmpty(staticValue)) {
                    Map tag = new HashMap<String, String>();
                    tag.put("staticDisplayedValue", options.getStaticDisplayedValue());
                    editView.setTag(tag);
                }
                group = null;
                group = createBarcodeView(fragment, options, editView, group);
                group = createSaveView(fragment, options, editView, group, part);
                partLayout.addView(group != null ? group : editView);
                if (editView != null && !TextUtils.isEmpty(options.getExternalReferId())) {
                    exViewHashTable.put(options.getExternalReferId(), editView);
                }
                if (callBackListener != null) {
                    callBackListener.viewCreated(editView, part, options);
                }
            }
            }
        }
        }
        // Special case for id of "*_result and other special cases" always has a right aligned save button
//	    if (isResultField(part))
//	    {
//		partLayout.addView(UiViewUtils.createButtonView(context, context.getString(R.string.save), new OnClickListener()
//		{
//
//		    @Override
//		    public void onClick(View v)
//		    {
//			// Save any updates to form bean
//			PersistentForm formFragment = null;
//			boolean validForm = false;
//			int screensToPop = 0;
//			try
//			{
//			    formFragment = (PersistentForm) fragment;
//			    formFragment.saveForm(true);
//			    validForm = formFragment.isValid();
//			    screensToPop = formFragment.getNoScreensToToolList();
//			}
//			catch (ClassCastException e)
//			{
//			    Log.e(TAG, "Fragment tagged: " + fragment.getTag() + " must implement PersistentForm");
//			    e.printStackTrace();
//			}
//			// Check if all mandatory fields are filled before popping fragment
//			if (validForm)
//			{
//			    // Go back to Rebate Tool List Screen
//			    while (screensToPop-- > 0)
//				fragment.getFragmentManager().popBackStack();
//			}
//			else
//			{
//			    UiUtil.showDialog(fragment.getFragmentManager(), context.getString(R.string.header_form_invalid), context.getString(R.string.form_invalid));
//			}
//		    }
//
//		}));
//	    }
	    if (part.hasPicture())
	    {
		createPictureView(fragment, part, partLayout);
	    }

	    if (part.getId() != null && (part.getId().equalsIgnoreCase(AppConstants.OLD_HEATING_SYSTEM_SIZE) || part.getId().equalsIgnoreCase(AppConstants.BOILER_OLD_SYSTEM_EFFICIENCY_ID)))
	    {
		partLayout.setTag(part);
	    }

	    if (part.getSystemType() != null && !TextUtils.isEmpty(part.getSystemType()))
	    {
		partLayout.setTag(AppConstants.SYSTEM_TYPE + "_" + part.getSystemType());
	    }
	}
	if (part.isHidden() && partLayout != null)
	{
	    partLayout.setVisibility(View.GONE);
	}
	return partLayout;
    }

    // TODO refactor data structure to add options.result structure...
    private boolean isResultField(Part part)
    {
	boolean isResult = false;
	if (part != null && part.getId() != null)
	{
	    isResult = part.getId().toLowerCase(Locale.US).contains("_result") || part.getId().equalsIgnoreCase("Tune_Up_Pass_OR_Fail") || part.getId().equalsIgnoreCase("Duct_Sealing_Pass_Fail");
	}
	return isResult;
    }

    private TextView createAddButtonView(final Fragment fragment, final Part part, Context context, final ViewGroup partLayout, final List<Part> partList, final boolean isItemSaved)
    {

	TextView view = UiViewUtils.createButtonLikeTextView(context, AppConstants.ADD_SYMBOL, new OnClickListener()
	{
	    @Override
	    public void onClick(View v)
	    {
		if (v != null && v.getRootView() != null)
		{
		    v.getRootView().clearFocus();
		}
		Part newPart = (Part) part.createDuplicatePart();

		if (item != null && item.getReplaceOnFail() != null && item.getReplaceOnFail().getParts() != null)
		{
		    int index = -1;

		    partBackground = R.drawable.part_bg;

		    index = item.getReplaceOnFail().getParts().lastIndexOf(part) + 1;
		    item.getReplaceOnFail().getParts().add(index, newPart);
		    ((ViewGroup) partLayout.getParent()).addView(createPartView(fragment, newPart, partList, isItemSaved), index);
		}

	    }
	});
	return view;
    }

    private TextView createDeleteButtonView(final Fragment fragment, final Part part, Context context, final ViewGroup partLayout, final List<Part> partList)
    {
	return UiViewUtils.createButtonLikeTextView(context, AppConstants.DELETE_SYMBOL, new OnClickListener()
	{
	    @Override
	    public void onClick(View v)
	    {
		if (v != null && v.getRootView() != null)
		{
		    v.getRootView().clearFocus();
		}
		int index = -1;
		Part tempPart = part;
		if (item != null && item.getReplaceOnFail() != null && item.getReplaceOnFail().getParts() != null)
		{
		    index = item.getReplaceOnFail().getParts().indexOf(tempPart);
		    item.getReplaceOnFail().getParts().remove(index);
		    ((ViewGroup) partLayout.getParent()).removeViewAt(index);
		}
	    }
	});
    }

    private EditText createOptionsEditView(final Fragment fragment, final Part part, final Options options)
    {
	final EditText editView = UiViewUtils.createEditTextView(fragment.getActivity(), options);

	((EditText) editView).setText(options.getSavedValue());

	if (options.hasBarcode()) {
	    editView.addTextChangedListener(new BarcodeTextWatcher(editView, options, fragment.getActivity())
	    {
	        @Override
	        void getEditTextView(EditText editText)
	        {
	    	edit = editText;
	        }
	    });
	} else {
	    editView.addTextChangedListener(new CustomTextWatcher(editView, options, fragment.getActivity())
	    {
	        @Override
	        void getEditTextView(EditText editText)
	        {
	    	edit = editText;
	        }
	    });
	}

	if (callBackListener != null)
	{
	    callBackListener.viewCreated(editView, part, options);
	}

	return editView;
    }

    private ViewGroup createSaveView(final Fragment fragment, final Options options, final TextView editView, ViewGroup group, Part part)
    {
	final Context context = fragment.getActivity();
	if (isResultField(part))
	{
	    final Button saveButton = UiViewUtils.createButtonView(context, context.getString(R.string.save), new OnClickListener() {
			@Override
			public void onClick(View v)
			{
				// Save any updates to form bean
				PersistentForm formFragment = null;
				boolean validForm = false;
				int screensToPop = 0;
				try
				{
				formFragment = (PersistentForm) fragment;
				formFragment.saveForm(true);
				validForm = formFragment.isValid();
				screensToPop = formFragment.getNoScreensToToolList();
				}
				catch (ClassCastException e)
				{
				Log.e(TAG, "Fragment tagged: " + fragment.getTag() + " must implement PersistentForm");
				e.printStackTrace();
				}
				// Check if all mandatory fields are filled before popping fragment
				if (validForm)
				{
				// Go back to Rebate Tool List Screen
				while (screensToPop-- > 0)
					fragment.getFragmentManager().popBackStack();
				}
				else
				{
				UiUtil.showDialog(fragment.getFragmentManager(), context.getString(R.string.header_form_invalid), context.getString(R.string.form_invalid));
				}
			}
	    });

		saveButton.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus) {
					saveButton.performClick();
				}
			}
		});


	    group = UiViewUtils.createHorizontalView(fragment.getActivity(), editView, saveButton);
	    // Make these two the same height preserve previous params
	    android.view.ViewGroup.LayoutParams params = editView.getLayoutParams();
	    params.height = (int) context.getResources().getDimension(R.dimen.height_small);
	    editView.setLayoutParams(params);
	    params = saveButton.getLayoutParams();
	    params.height = (int) context.getResources().getDimension(R.dimen.height_small);
	    saveButton.setPadding(20, 0, 20, 0);
	    saveButton.setTextSize(14);
	    saveButton.setLayoutParams(params);
		saveButton.setFocusableInTouchMode(true);
	}
	return group;
    }

    private ViewGroup createBarcodeView(final Fragment fragment, final Options options, final EditText editView, ViewGroup group)
    {
	if (options.hasBarcode())
	{
	    View barcodeView = UiViewUtils.createImageButtonView(fragment.getActivity(), R.drawable.barcode_img);
	    barcodeView.setOnClickListener(new OnClickListener()
	    {
		@Override
		public void onClick(View v)
		{
		    if (v != null && v.getRootView() != null)
		    {
			v.getRootView().clearFocus();
		    }
		    edit = (EditText) editView;
		    UiUtil.setCurrentBarCodeView(edit);
		    UiUtil.setCurrentBarCodeOptions(options);

		    Intent intent = new Intent(fragment.getActivity(), CaptureActivity.class);
		    // intent.setAction("com.google.zxing.client.android.SCAN");
		    intent.putExtra(Intents.Scan.MODE, Intents.Scan.ONE_D_MODE);
		    fragment.getActivity().startActivity(intent);
		}
	    });
	    group = UiViewUtils.createHorizontalView(fragment.getActivity(), editView, barcodeView);
	}
	return group;
    }

    private void createReferenceCheckbox(final Fragment fragment, final List<Part> partList, ViewGroup partLayout, final Options options, final EditText refereeEditField)
    {
	final Reference reference = options.getReference();
	if (reference != null && partList != null && reference.getReferId() != null)
	{

	    ArrayList<EditText> refereeEditFieldList = null;
	    EditText referenceView = (EditText) UiViewUtils.getViewByTag(partLayout, reference.getReferId());

	    if (referenceView != null)
	    {
		if (!referenceMap.containsKey(referenceView))
		{
		    refereeEditFieldList = new ArrayList<>();
		    referenceMap.put(referenceView, refereeEditFieldList);
		}

		if (refereeEditFieldList == null)
		{
		    refereeEditFieldList = referenceMap.get(referenceView);
		}

		if (!refereeEditFieldList.contains(refereeEditField))
		{
		    refereeEditFieldList.add(refereeEditField);
		}

	    }

	    CheckBox cB = UiViewUtils.createCheckBoxView(fragment.getActivity(), reference.getName(), new OnCheckedChangeListener()
	    {

		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
		{
		    if (reference.getReferId() != null)
		    {
			String[] refereceId = reference.getReferId().split("_");
			if (refereceId.length > 1)
			{
			    ListIterator<Part> iterator = partList.listIterator();

			    while (iterator.hasNext())
			    {
				Part par = (Part) iterator.next();
				if (par.getId() != null && par.getId().equalsIgnoreCase(refereceId[0]))
				{
				    if (par.getOptions() != null)
				    {
					ListIterator<Options> optIterator = par.getOptions().listIterator();

					while (optIterator.hasNext())
					{
					    Options opt = (Options) optIterator.next();
					    if (opt.getId().equalsIgnoreCase(refereceId[1]))
					    {
						if (isChecked)
						{
						    refereeEditField.setText(opt.getSavedValue() != null ? opt.getSavedValue() : "");
						}

						refereeEditField.setEnabled(!isChecked);
						refereeEditField.setFocusable(!isChecked);
						refereeEditField.setTextColor(fragment.getActivity().getResources().getColor(android.R.color.black));
						reference.setChecked(isChecked);
						options.setSavedValue(refereeEditField.getText().toString());
						break;
					    }
					}
				    }
				    break;
				}
			    }
			}
		    }
		}
	    });

	    cB.setChecked(reference.isChecked());
	    partLayout.addView(cB);

	}

    }

    View captureImageView = null;

    private String equipmentName;

    private int partBackground = R.drawable.part_bg;
    private CheckBox brushlessCheckBoxView;
    private CheckBox oldBowlerView;
    private CheckBox ecmAHRIView;

    private void createPictureView(final Fragment fragment, final Part part, ViewGroup partLayout)
    {
	ViewGroup group = UiViewUtils.createLinearLayout(fragment.getActivity());
	ImageView cameraImageView = UiViewUtils.createPictureButtonView(fragment.getActivity(), part, R.drawable.camera_img);
	cameraImageView.setTag(part);

	LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	params.gravity = Gravity.CENTER_VERTICAL;
	int margin = fragment.getActivity().getResources().getDimensionPixelOffset(R.dimen.form_screen_part_detail_top_margin);
	cameraImageView.setPadding(margin, margin, margin, margin);
	cameraImageView.setLayoutParams(params);
	cameraImageView.setOnClickListener(new OnClickListener()
	{

	    @Override
	    public void onClick(View v)
	    {
		if (v != null && v.getRootView() != null)
		{
		    v.getRootView().clearFocus();
		}
		captureImageView = v;
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		if (part != null)
		{
		    String index = 0 + "";
		    if (fragment instanceof FormFragment)
		    {
			index = ((FormFragment) fragment).getPageName();
		    }
		    else if (fragment instanceof DuctSealingPagerFragment)
		    {
			index = ((DuctSealingPagerFragment) fragment).getPageName();
		    }
		    else if (fragment instanceof ThermoStatInstallPagerFragment)
		    {
			index = ((ThermoStatInstallPagerFragment) fragment).getPageName();
		    }
		    else if (fragment instanceof TuneupFragment)
		    {
			index = ((TuneupPagerFragment) fragment).getPageName();
		    }
		    
		    File equipPic = new File(FileUtil.getFilePathForPartImage(equipmentName, part, index));
		    try {
    		    	equipPic.getParentFile().mkdirs();
    		    	equipPic.createNewFile();
		    } catch (Exception e) {
			
		    }
		    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
					FileProvider.getUriForFile(fragment.getActivity(),
					BuildConfig.APPLICATION_ID + ".fileprovider",
					equipPic));
		}
		fragment.startActivityForResult(takePictureIntent, AppConstants.CAPTURE_PHOTO_REQUEST_CODE);
		// showImageUploadDialog(part, (FormFragment) fragment);
	    }
	});

	// if (part.getImagePath() != null)
	// {
	//
	//
	// }
	// else
	// {
	// group = UiViewUtils.createLinearLayout(fragment.getActivity());
	// group.addView(cameraImageView);
	// }

	ImageView previewImageView = UiViewUtils.createImagePreviewButtonView(fragment.getActivity(), R.drawable.gallery_icon);
	Drawable drawable = previewImageView.getDrawable();
	margin = fragment.getActivity().getResources().getDimensionPixelOffset(R.dimen.form_screen_part_detail_top_margin);
	int width = drawable.getIntrinsicWidth() + (margin << 2);
	int height = drawable.getIntrinsicHeight() + (margin << 1);
	previewImageView.setScaleType(ScaleType.FIT_XY);
	previewImageView.setPadding(10, 2, 10, 2);
	params = new LayoutParams(width, height);
	params.leftMargin = 5;
	previewImageView.setLayoutParams(params);

	group = UiViewUtils.createHorizontalViewFitContent(fragment.getActivity(), cameraImageView, previewImageView);

	if (part.getImagePath() != null)
	{
	    UiUtil.showPreviewDialog(previewImageView, FileUtil.getAbsolutePath(part.getImagePath()), part);
	}
	else
	{
	    previewImageView.setVisibility(View.INVISIBLE);
	}
	partLayout.addView(group);
    }

    private EditText createPartEditView(final Fragment fragment, ViewGroup partLayout, final Part part, final Options options)
    {

	EditText editView = UiViewUtils.createEditTextView(fragment.getActivity(), options);
	if (options.getSavedValue() != null)
	{
	    editView.setText(options.getSavedValue());
	}

	TextWatcher tw = new CustomTextWatcher(editView, options, fragment.getActivity())
    {
        @Override
        void getEditTextView(EditText editText)
        {
            edit = editText;
        }
    };

	editView.addTextChangedListener(tw);
    editView.setTag(R.id.custom_text_watcher_id, tw);

	return editView;
    }

    private TextView createDatePickView(final Fragment fragment, final Options options)
    {
	final TextView dateView = UiViewUtils.createDatePickResultView(fragment.getActivity(), options);
	if (options.getSavedValue() != null)
	{
	    dateView.setText(options.getSavedValue());
	}
	final DatePickWatcher dpw = new DatePickWatcher(dateView, options, fragment.getActivity());
	
	dateView.setOnClickListener(new OnClickListener()
	{
	    @Override
	    public void onClick(View v)
	    {
		Calendar endDate = Calendar.getInstance();
		Calendar beginDate = Calendar.getInstance();
		beginDate.set(Calendar.YEAR, endDate.get(Calendar.YEAR) - 1);
		UiUtil.showDatePickDialog(fragment.getChildFragmentManager(), fragment.getActivity(), 
			beginDate, endDate, dpw);
	    }
	});

	return dateView;
    }

    private void createSwitcher(final Fragment fragment, final ViewGroup partLayout, final Options options)
    {
	Switch sw = UiViewUtils.createSwticherView(fragment.getActivity(), options.getName(), options.getValues().toArray(new String[options.getValues().size()]), new OnCheckedChangeListener()
	{
	    @Override
	    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
	    {
		options.setSavedValue(isChecked ? "true" : "false");
	    }
	});

	if (options.getSavedValue() != null)
	{
	    sw.setChecked(Boolean.parseBoolean(options.getSavedValue()));
	}

	partLayout.addView(sw);
    }

    private void createCheckbox(final Fragment fragment, ViewGroup partLayout, final Part part, final Options options)
    {

	final CheckBox cB = UiViewUtils.createCheckBoxView(fragment.getActivity(), options.getName(), new OnCheckedChangeListener()
	{

	    @Override
	    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
	    {
		options.setSavedValue(isChecked ? "true" : "false");
		if (ecmAHRIView != null && oldBowlerView != null && brushlessCheckBoxView != null)
		{
		    if (buttonView == brushlessCheckBoxView)
		    {
			if (isChecked)
			{
			    oldBowlerView.setChecked(false);
			    brushlessCheckBoxView.setChecked(true);
			    ecmAHRIView.setChecked(false);
			    ecmAHRIView.setAlpha(1.0f);
			    ecmAHRIView.setClickable(true);
			}
			else
			{
			    ecmAHRIView.setAlpha(0.5f);
			    ecmAHRIView.setClickable(false);
			    ecmAHRIView.setChecked(false);
			}
		    }
		    else if (buttonView == oldBowlerView)
		    {
			if (isChecked)
			{
			    brushlessCheckBoxView.setChecked(false);
			    oldBowlerView.setChecked(true);
			    ecmAHRIView.setAlpha(0.5f);
			    ecmAHRIView.setClickable(false);
			    ecmAHRIView.setChecked(false);
			}
		    }
		}

	    }
	});
	if (part != null && part.getId() != null && part.getId().equalsIgnoreCase(AppConstants.BRUSHLESS_PERMANENT_MAGNET_MOTOR_CHECKBOX_ID))
	{
	    brushlessCheckBoxView = cB;
	}
	else if (part != null && part.getId() != null && part.getId().equalsIgnoreCase(AppConstants.OLD_BLOWER_MOTOR_CHECKBOX_ID))
	{
	    oldBowlerView = cB;
	}
	else if (part != null && part.getId() != null && part.getId().equalsIgnoreCase(AppConstants.BRUSHLESS_PERMANENT_MAGNET_MOTOR_ECM_AHRI_ID))
	{
	    ecmAHRIView = cB;
	    if (ecmAHRIView != null)
	    {
		if (options.getSavedValue() == null || (brushlessCheckBoxView != null && !brushlessCheckBoxView.isChecked()))
		{
		    ecmAHRIView.setAlpha(0.5f);
		    ecmAHRIView.setClickable(false);
		    ecmAHRIView.setChecked(false);
		}
	    }
	}
	if (options.getSavedValue() != null)
	{
	    cB.setChecked(Boolean.parseBoolean(options.getSavedValue()));
	}
	partLayout.addView(cB);
    }

    private void createComboBox(final Fragment fragment, final ViewGroup partLayout, final Part part, final Options options)
    {
	String previousSeletedValue = options.getDefaultSystemType();

	if (part.getId() != null && part.getId().equalsIgnoreCase(AppConstants.SYSTEM_TYPE))
	{
	    if (options.getSavedValue() != null && options.getDefaultSystemType() == null)
	    {
		previousSeletedValue = options.getSavedValue();
		options.setDefaultSystemType(previousSeletedValue);
	    }
	    else if (options.getDefaultSystemType() != null && options.getSavedValue() == null)
	    {
		options.setSavedValue(options.getDefaultSystemType());
	    }
	}

	View view = UiViewUtils.createComboBoxView(fragment.getActivity(), part.getId(), options.getValues(), previousSeletedValue, new OnItemSelectedListener()
	{
	    @Override
	    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
	    {
		if (parent != null && parent.getRootView() != null)
		{
		    parent.getRootView().clearFocus();
		}
		String seletedValue = null;

		if (view instanceof TextView)
		{
		    seletedValue = ((TextView) view).getText().toString().trim();
		    if (!seletedValue.equalsIgnoreCase(AppConstants.COMBO_SELECT_VALUE))
		    {
			options.setSavedValue(seletedValue);
		    }
		    else
		    {
			options.setSavedValue(null);
		    }

		    if (UiViewUtils.isValidationMode(options.isItemSaved()) && options.isMandatory() && !options.isFieldFilled())
		    {
			((View) view.getParent()).setBackground(fragment.getActivity().getResources().getDrawable(R.drawable.bg_dialog_mandatory_box));
		    }
		    else
		    {
			((View) view.getParent()).setBackground(fragment.getActivity().getResources().getDrawable(R.drawable.combo_box_bg));
		    }
		}

		if (options != null && parent != null && parent.getTag() != null && parent.getTag() instanceof String)
		{
		    if (AppConstants.OLD_HEATING_SYSTEM_TYPE.equalsIgnoreCase((String) parent.getTag()))
		    {
			ArrayList<View> viewToManipulate = UiViewUtils.getViewsByTag((ViewGroup) partLayout.getParent(), AppConstants.OLD_HEATING_SYSTEM_SIZE);
			if (viewToManipulate != null)
			{
			    for (int i = 0; i < viewToManipulate.size(); i++)
			    {
				seletedValue = seletedValue.toLowerCase();

				boolean hidden = true;
				if (seletedValue.indexOf("furnace") != -1 || seletedValue.indexOf("dual fuel") != -1)
				{
				    hidden = false;
				    viewToManipulate.get(i).setVisibility(View.VISIBLE);
				}
				else
				{
				    viewToManipulate.get(i).setVisibility(View.GONE);
				}

				Part partTag = (Part) viewToManipulate.get(i).getTag();
				if (partTag != null)
				{
				    partTag.setMandatory(hidden ? "false" : "true");
				    partTag.setHidden(hidden ? "true" : "false");
				}
			    }
			}
		    }
		    else if (AppConstants.SYSTEM_TYPE.equalsIgnoreCase((String) parent.getTag()))
		    {
			UiViewUtils.setupViewsBySystemType((ViewGroup) partLayout.getParent(), AppConstants.SYSTEM_TYPE, seletedValue);
		    }
		    else if (AppConstants.BOILER_OLD_HEATING_SYSTEM_TYPE_ID.equalsIgnoreCase((String) parent.getTag()))
		    {
			ArrayList<View> viewToManipulate = UiViewUtils.getViewsByTag((ViewGroup) partLayout.getParent(), AppConstants.BOILER_OLD_SYSTEM_EFFICIENCY_ID);
			if (viewToManipulate != null)
			{
			    for (int i = 0; i < viewToManipulate.size(); i++)
			    {
				seletedValue = seletedValue.toLowerCase();

				boolean hidden = true;
				if (seletedValue.indexOf("furnace") != -1)
				{
				    hidden = false;
				    viewToManipulate.get(i).setVisibility(View.VISIBLE);
				}
				else
				{
				    viewToManipulate.get(i).setVisibility(View.GONE);
				}

				Part partTag = (Part) viewToManipulate.get(i).getTag();
				if (partTag != null)
				{
				    partTag.setMandatory(hidden ? "false" : "true");
				    partTag.setHidden(hidden ? "true" : "false");
				}
			    }
			}
		    }
		}
	    }

	    @Override
	    public void onNothingSelected(AdapterView<?> parent)
	    {
	    }
	});

	if (part.getId() != null && (part.getId().equalsIgnoreCase(AppConstants.OLD_HEATING_SYSTEM_TYPE) || part.getId().equalsIgnoreCase(AppConstants.SYSTEM_TYPE) || part.getId().equalsIgnoreCase(AppConstants.BOILER_OLD_HEATING_SYSTEM_TYPE_ID)))
	{
	    view.setTag(part.getId());
	}

	if (options.getValues() != null)
	{
	    int index = options.getValues().indexOf(options.getSavedValue());
	    // ((Spinner) view).setSelection(index);
	    String selected = options.getSavedValue();
	    List<String> values = options.getValues();
	    String[] newValues = new String[(options.getValues().size() + 1)];
	    newValues[0] = AppConstants.COMBO_SELECT_VALUE;
	    for (int i = 1; i <= options.getValues().size(); i++)
	    {
		newValues[i] = values.get(i - 1);
	    }
	    List<String> newList = Arrays.asList(newValues);
	    CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(fragment.getActivity(), view.getId(), newList);
	    ((Spinner) view).setAdapter(adapter);
	    if (selected != null && selected.length() > 0)
	    {
		for (int i = 0; i < options.getValues().size(); i++)
		{
		    if (options.getValues().get(i).equalsIgnoreCase(selected))
		    {
			((Spinner) view).setSelection(index + 1);
			break;
		    }
		}
	    }
	}

	partLayout.addView(UiViewUtils.createSpinnerBg(fragment.getActivity(), view, options));

	if (callBackListener != null)
	{
	    callBackListener.viewCreated(view, part, options);
	}
    }

    private View createAutoCompleteTextView(final Fragment fragment, ViewGroup partLayout, final Options options, List<String> values, Part part)
    {

	View view = UiViewUtils.createAutoCompletion(fragment.getActivity(), options, values, new OnFocusChangeListener()
	{
	    @Override
	    public void onFocusChange(View view, boolean hasFocus)
	    {
		if (!hasFocus)
		{
		    if (view instanceof TextView)
		    {
			String value = String.valueOf(((TextView) view).getText());
			options.setSavedValue(value);
		    }
		    else
		    {
			options.setSavedValue(null);
		    }

		    if (UiViewUtils.isValidationMode(options.isItemSaved()) && options.isMandatory() && !options.isFieldFilled())
		    {
			((View) view).setBackground(fragment.getActivity().getResources().getDrawable(R.drawable.bg_dialog_mandatory_box));
		    }
		    else
		    {
			((View) view).setBackground(fragment.getActivity().getResources().getDrawable(R.drawable.combo_box_bg));
		    }
		}

	    }
	});

	if (part.getId() != null && options.getId() != null)
	{
	    // view.setTag(part.getId() + "_" + options.getId());
	}

	if (options.getSavedValue() != null)
	{
	    ((AutoCompleteTextView) view).setText(options.getSavedValue());
	}

	partLayout.addView(UiViewUtils.createSpinnerBg(fragment.getActivity(), view, options));
	if (callBackListener != null)
	{
	    callBackListener.viewCreated(view, part, options);
	}
	return view;
    }

    public EditText getLastFocusedEditField()
    {
	return edit;
    }

    private void showImageUploadDialog(Part part, FormFragment fragment)
    {
	if (part != null && fragment != null)
	{
	    commonDialog = ICFDialogFragment.newDialogFrag(R.layout.equipment_image_dialog_layout);
	    commonDialog.setDialogListener(fragment);
	    commonDialog.setDataObj(part);
	    commonDialog.show(fragment.getActivity().getSupportFragmentManager(), "captureImageDialog");
	}
    }

    public ICFDialogFragment getDialog()
    {
	return commonDialog;
    }

    public void setCommonDialog(ICFDialogFragment obj)
    {
	commonDialog = obj;
    }

    public View getCaptuerImageView()
    {
	return captureImageView;
    }

    public void setEquipmentName(String name)
    {
	this.equipmentName = name;
    }

    public String getEquipmentName()
    {
	return this.equipmentName;
    }

    public void setFormViewCreationCallback(FormViewCreationCallback listener)
    {
	this.callBackListener = listener;
    }

    public static interface FormViewCreationCallback
    {
	void viewCreated(View view, Part part, Options option);
    }

    public void retrieveManifoldValue(String pathName)
    {
	Hashtable<String, String> hashManifold = importManifoldValue(pathName);
	if (hashManifold != null && hashManifold.size() > 0)
	{
	    if (UiUtil.manifoldKey.equalsIgnoreCase(hashManifold.get("key")))
	    {
		if (UiUtil.manifoldOptions != null)
		{
		    String fileName = UiUtil.manifoldOptions.getSavedValue();
		    if (TextUtils.isEmpty(fileName))
		    {
			fileName = "manifold_" + System.currentTimeMillis() + ".csv";
		    }
		    if (RebateManager.getInstance().getFormBean() != null)
		    {
			// String jobId = RebateManager.getInstance().getFormBean().getCustomerInfo().getJobId();
			String index = UiUtil.manifoldOptions.getId();
			String pathToCopy = FileUtil.getFilePathForEquipment(equipmentName, index) + fileName;
			try
			{
			    FileUtil.copyFile(pathName, pathToCopy);
			    fileName = FileUtil.getRelativePath(pathToCopy);
			    UiUtil.manifoldOptions.setSavedValue(FileUtil.flattenFile(fileName, "_").replaceAll("^_*", ""));
			}
			catch (FileNotFoundException e)
			{
			    e.printStackTrace();
			}
			catch (IOException e)
			{
			    e.printStackTrace();
			}
		    }
		}
		if (exViewHashTable != null)
		{
		    Enumeration<String> keys = exViewHashTable.keys();
		    while (keys.hasMoreElements())
		    {
			String referId = keys.nextElement();
			if (referId != null)
			{
			    String value = hashManifold.get(referId);
			    View tempView = exViewHashTable.get(referId);
			    if (tempView instanceof EditText)
			    {
				((EditText) tempView).setText(TextUtils.isEmpty(value) ? "0" : value);
				ICFLogger.d(TAG, "iManiFold Result : " + value);
			    }
			}
		    }
		}
	    }
	}
    }

    private Hashtable<String, String> importManifoldValue(String pathName)
    {
	if (TextUtils.isEmpty(pathName))
	{
	    return null;
	}
	byte[] data = FileUtil.getFileContent(pathName);

	if (data == null) {
		String fileName = pathName.substring(pathName.lastIndexOf("/")+1);
		data = FileUtil.getFileContent("/sdcard/" + fileName);
	}

	if (data != null)
	{
	    String[] temp = new String(data).split("\\n");
	    if (temp != null)
	    {
		ArrayList<String> lineList = new ArrayList<String>(Arrays.asList(temp));
		if (lineList != null && lineList.size() >= 2)
		{
		    String splitCellsRegex = ",(?=([^\"]*\"[^\"]*\")*[^\"]*$)";

		    String[] header = lineList.get(0).split(splitCellsRegex);
		    String[] value = null;
		    if (lineList.size() > 2)
		    {
			value = lineList.get(2).split(splitCellsRegex);
		    }
		    else
		    {
			value = lineList.get(1).split(splitCellsRegex);
		    }
		    Log.d(TAG, String.format("Header size: %d; Value size: %d", header.length, value.length));
		    if (header != null && value != null)
		    {
			int i = 0;
			Hashtable<String, String> manifoldHashTable = new Hashtable<String, String>();
			for (String tempStr : header)
			{
			    manifoldHashTable.put(tempStr.replaceAll("\"", ""), value[i].replaceAll("\"", ""));
			    i++;
			}
			return manifoldHashTable;
		    }
		}
	    }
	}
	return null;
    }

    private String createLaunchURI(String launchUri)
    {
	StringBuffer buf = new StringBuffer();
	if (launchUri.startsWith(AppConstants.LAUNCH_MANIFOLD_URI_WITH_ONLY_KEY))
	{
	    buf.append(launchUri);

	}
	else if (launchUri.startsWith(AppConstants.LAUNCH_MANIFOLD_URI_WITH_DATA))
	{
	    FormResponseBean bean = RebateManager.getInstance().getFormBean();
	    CustomerInfo info = bean.getCustomerInfo();

	    buf.append(launchUri); // with key
	    buf.append("&");
	    buf.append("customer_name=");
	    if (!TextUtils.isEmpty(info.getFirstName()))
	    {
		buf.append(info.getFirstName());
		buf.append(" ");
	    }
	    if (!TextUtils.isEmpty(info.getLastName()))
	    {
		buf.append(info.getLastName());
	    }
	    buf.append("&");
	    buf.append("customer_email=");
	    if (!TextUtils.isEmpty(info.getEmailAddress()))
	    {
		buf.append(info.getEmailAddress());
	    }
	    buf.append("&");
	    buf.append("customer_address_1=");
	    if (!TextUtils.isEmpty(info.getCustomerAddress()))
	    {
		buf.append(info.getCustomerAddress().replaceAll(",", " "));
	    }
	    buf.append("&");
	    buf.append("customer_address_2=");
	    buf.append("&");
	    buf.append("customer_city=");
	    if (!TextUtils.isEmpty(info.getCustomerCity()))
	    {
		buf.append(info.getCustomerCity().replaceAll(",", " "));
	    }
	    buf.append("&");

	    buf.append("customer_state_province=");
	    if (!TextUtils.isEmpty(info.getCustomerState()))
	    {
		buf.append(info.getCustomerState().replaceAll(",", " "));
	    }
	    buf.append("&");

	    buf.append("customer_postal_code=");
	    if (!TextUtils.isEmpty(info.getCustomerZipCode()))
	    {
		buf.append(info.getCustomerZipCode());
	    }
	    buf.append("&");

	    buf.append("customer_country=");
	    if (!TextUtils.isEmpty(info.getCustomerCountry()))
	    {
		buf.append(info.getCustomerCountry());
	    }
	    buf.append("&");
	    buf.append(getCondenserAirHandlerData());
	}
	ICFLogger.d("sssssss", "GGGG Launch URI " + buf);
	return buf.toString();
    }

    private String getCondenserAirHandlerData()
    {
	StringBuffer buf = new StringBuffer();
	ArrayList<Part> partList = new ArrayList<Part>();
	if (item != null)
	{
	    if (item.isReplaceOnFailSelected())
	    {
		partList = item.getReplaceOnFail().getParts();
	    }
	    else
	    {
		partList = item.getReplaceOnFail().getParts();
		partList.addAll(item.getEarlyRetirement().getParts());
	    }
	}
	else if (qivItem != null)
	{
	    FormResponseBean bean = RebateManager.getInstance().getFormBean();
	    List<Equipment> list = bean.getEquipments();
	    if (list != null)
	    {
		for (Equipment equip : list)
		{
		    List<Item> itemList = equip.getItems();
		    for (Item tempItem : itemList)
		    {
			if (tempItem.getId() == qivItem.getId())
			{
			    if (tempItem.isReplaceOnFailSelected())
			    {
				partList = tempItem.getReplaceOnFail().getParts();
			    }
			    else
			    {
				partList = tempItem.getReplaceOnFail().getParts();
				partList.addAll(tempItem.getEarlyRetirement().getParts());
			    }
			    break;
			}
		    }
		}
	    }
	}
	else if (mPartList != null)
	{
	    partList.addAll(mPartList);
	}

	boolean populateCondenserData = false;
	boolean populateAirHandlerData = false;
	for (Part part : partList)
	{
	    for (String id : AppConstants.MANIFOLD_CONDENSER_IDS)
	    {
		if (id.equalsIgnoreCase(part.getId()))
		{
		    List<Options> options = part.getOptions();
		    for (Options option : options)
		    {
			if (option.getName().equalsIgnoreCase(AppConstants.MANUFACTURE_NAME))
			{
			    buf.append(AppConstants.URI_CONDENSER_MAKE);
			    buf.append(TextUtils.isEmpty(option.getSavedValue()) ? "" : option.getSavedValue());
			    buf.append("&");
			}
			else if (option.getName().equalsIgnoreCase(AppConstants.SERIAL_NUMBER))
			{
			    buf.append(AppConstants.URI_CONDENSER_SERIAL);
			    buf.append(TextUtils.isEmpty(option.getSavedValue()) ? "" : option.getSavedValue());
			    buf.append("&");
			}
			else if (option.getName().equalsIgnoreCase(AppConstants.MODEL_NUMBER))
			{
			    buf.append(AppConstants.URI_CONDENSER_MODEL);
			    buf.append(TextUtils.isEmpty(option.getSavedValue()) ? "" : option.getSavedValue());
			    buf.append("&");
			}
		    }
		    populateCondenserData = true;
		    break;
		}
		if (populateCondenserData)
		{
		    break;
		}
	    }

	    for (String id : AppConstants.MANIFOLD_AIRHANDLER_IDS)
	    {
		if (id.equalsIgnoreCase(part.getId()))
		{
		    List<Options> options = part.getOptions();
		    for (Options option : options)
		    {
			if (option.getName().equalsIgnoreCase(AppConstants.MANUFACTURE_NAME))
			{
			    buf.append(AppConstants.URI_AIRHANDLER_MAKE);
			    buf.append(TextUtils.isEmpty(option.getSavedValue()) ? "" : option.getSavedValue());
			    buf.append("&");
			}
			else if (option.getName().equalsIgnoreCase(AppConstants.SERIAL_NUMBER))
			{
			    buf.append(AppConstants.URI_AIRHANDLER_SERIAL);
			    buf.append(TextUtils.isEmpty(option.getSavedValue()) ? "" : option.getSavedValue());
			    buf.append("&");
			}
			else if (option.getName().equalsIgnoreCase(AppConstants.MODEL_NUMBER))
			{
			    buf.append(AppConstants.URI_AIRHANDLER_MODEL);
			    buf.append(TextUtils.isEmpty(option.getSavedValue()) ? "" : option.getSavedValue());
			    buf.append("&");
			}
		    }
		    populateAirHandlerData = true;
		    break;
		}
		if (populateAirHandlerData)
		{
		    break;
		}
	    }

	    if (populateAirHandlerData && populateCondenserData)
	    {
		break;
	    }
	}
	String tempString = buf.toString();
	for (String tempParamter : AppConstants.URI_PARAMTERS)
	{
	    if (!tempString.contains(tempParamter))
	    {
		buf.append(tempParamter);
		buf.append("&");
	    }
	}
	return buf.toString();
    }

    public void setPartBackground(int partBack)
    {
	this.partBackground = partBack;
    }
}
