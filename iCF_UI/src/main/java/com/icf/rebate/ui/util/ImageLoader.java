package com.icf.rebate.ui.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.util.LruCache;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.networklayer.utils.LibUtils;

public class ImageLoader {
	
    	public static final String TAG = ImageLoader.class.getSimpleName();
    
    	private LruCache<String, Bitmap> mMemoryCache;
    	List<String> bitmapsBeingProcessed = Collections.synchronizedList(new ArrayList<String>());
	private Context context;

	public ImageLoader(Context context) {
		this.context = context;
		int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
		int cacheSize = maxMemory / 8;
		mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
			@Override
			protected int sizeOf(String key, Bitmap bitmap) {
				return bitmap.getByteCount() / 1024;
			}
		};
	}
	
	private void addBitmapToMemoryCache(String key, Bitmap bitmap) {
		if (getBitmapFromMemCache(key) == null) {
			mMemoryCache.put(key, bitmap);
		}
	}

	public void loadBitmap(final String imageName, final ImageView imageView,
			Drawable placeHolder, final BaseAdapter adapter) {

		final Bitmap bitmap = getBitmapFromMemCache(imageName);
		if (bitmap != null) {
			imageView.setImageBitmap(bitmap);
			adapter.notifyDataSetChanged();
		} else {
			imageView.setImageDrawable(placeHolder);
			// Can't use this because it doesn't scale properly
			/*BitmapWorkerTask task = new BitmapWorkerTask(imageName, imageView, adapter);
			task.execute(imageName);*/
			// If a worker thread is still processing bitmap then return 
			if (bitmapsBeingProcessed.contains(imageName)) {
			    return;
			}
			// Offload this from main thread
			Log.i(TAG, "Loading bitmap " + imageName);
			bitmapsBeingProcessed.add(imageName);
			new Thread(new Runnable() {

			    Bitmap bitmap = null;
			    @Override
			    public void run()
			    {
				int newHeight = 0, newWidth = 0;
				InputStream istr = null;
				try
				{
				    istr = new FileInputStream(FileUtil.getResourceBundleFile(imageName));
				    BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
				    boolean imageToBig = true;
				    // If we get out of memory exceptions keep sub sampling image
				    while (imageToBig) {
					try {
					    bitmap = BitmapFactory.decodeStream(istr, null, bitmapOptions);
					    imageToBig = false;
					} catch (OutOfMemoryError e) {
					    bitmapOptions.inSampleSize = bitmapOptions.inSampleSize < 1 
						    ? 2 : bitmapOptions.inSampleSize * 2;
					    Log.w(TAG, "WARNING: Caught OutOfMemoryError while trying to decode bitmap " + imageName +". Attempting to sub sample...");
					}
				    }
				    // bitmap = BitmapFactory.decodeStream(istr, null, bitmapOptions);
				    // Need to scale the bitmap
				    newWidth = (int)(context.getResources().getDimension(R.dimen.setting_company_image_height));
				    newHeight = newWidth*bitmapOptions.outHeight/bitmapOptions.outWidth;
				    bitmap = UiUtil.scaleBitmap(bitmap, newWidth, newHeight);
				    addBitmapToMemoryCache(imageName, bitmap);
				    istr.close();
				}
				catch (IOException e)
				{
				    e.printStackTrace();
				} 
				if (bitmap != null) {
				    new Handler(Looper.getMainLooper()).post(new Runnable()
				    {
					@Override
				        public void run()
				        {
				    		imageView.setImageBitmap(bitmap);
				    		adapter.notifyDataSetChanged();
				        }
				    });
				}
					try {
						bitmapsBeingProcessed.remove(bitmapsBeingProcessed.indexOf(imageName));
					} catch (Exception e) {
						//e.printStackTrace();
						//ArrayIndexOutOfBoundsException is known to happen here when the
						//image is missing from the downloaded resources
					}
				}
			    
			}).start();
		}
	}
	
	public void loadBitmap(String imageName, ImageView imageView,
		int resId) {
	    if(TextUtils.isEmpty(imageName) && resId != -1)
	    {
		imageView.setImageResource(resId);
	    }
	final Bitmap bitmap = getBitmapFromMemCache(imageName);
	if (bitmap != null) {
		imageView.setImageBitmap(bitmap);
	} else {
	    if(resId != -1) {
		imageView.setImageResource(resId);
	    }
		BitmapWorkerTask task = new BitmapWorkerTask(imageName, imageView, null);
		task.execute(imageName);
	}
}

	private Bitmap getBitmapFromMemCache(String key) {
	    // Added 11/19/2015
	    if (key == null)
		return null;
	    return mMemoryCache.get(key);
	}

	class BitmapWorkerTask extends AsyncTask<String, Void, Bitmap> {
		private String imageName;
		private WeakReference<ImageView> imageView;
		private WeakReference<BaseAdapter> adapter;

		BitmapWorkerTask(String imageName, ImageView imageView, BaseAdapter adapter) {
			this.imageName = imageName;
			this.imageView = new WeakReference<ImageView>(imageView);
			this.adapter = new WeakReference<>(adapter);
		}
		@Override
	    protected Bitmap doInBackground(String... params) {
			try {
	    		//TODO for now load it from assets later load it from RB folder
//	    		InputStream istr = context.getAssets().open(params[0]);
			InputStream istr = new FileInputStream(new File(context.getExternalFilesDir(LibUtils.FILES_RB), params[0]));     
	    		Bitmap bitmap = BitmapFactory.decodeStream(istr);
	    		istr.close();
	    		return bitmap;
			} catch(Exception e) {
				
			}
			return null;
	    }

		@Override
		protected void onPostExecute(Bitmap bitmap) {
			if(bitmap != null) {
				addBitmapToMemoryCache(imageName, bitmap);
				ImageView view = imageView.get();
				if(view != null) {
					view.setImageBitmap(bitmap);
				}
				BaseAdapter baseAdapter = this.adapter.get();
				if(baseAdapter != null) {
				    baseAdapter.notifyDataSetChanged();
				}
			}
		}
	}
}
