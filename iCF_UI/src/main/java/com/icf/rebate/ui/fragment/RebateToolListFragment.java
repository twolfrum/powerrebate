package com.icf.rebate.ui.fragment;

import java.util.ArrayList;

import android.os.Bundle;
import android.os.Handler.Callback;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.icf.rebate.adapter.RebateToolAdapter;
import com.icf.rebate.app.model.DashboardItem;
import com.icf.rebate.app.model.DashboardItem.ServiceItem;
import com.icf.rebate.networklayer.model.ConfigurationDetail;
import com.icf.rebate.networklayer.model.ConfigurationDetail.UtilityCompany;
import com.icf.rebate.networklayer.model.CustomerInfo;
import com.icf.rebate.networklayer.model.FormResponseBean;
import com.icf.rebate.networklayer.model.PendingJobItem;
import com.icf.rebate.networklayer.model.Submission;
import com.icf.rebate.networklayer.model.SubmissionItem;
import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.ui.RootActivity;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.controller.ValidationManager;
import com.icf.rebate.ui.dialog.DialogListener;
import com.icf.rebate.ui.dialog.ICFDialogFragment;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.DateFormatter;
import com.icf.rebate.ui.util.FileUtil;
import com.icf.rebate.ui.util.ICFLogger;
import com.icf.rebate.ui.util.UiUtil;

public class RebateToolListFragment extends Fragment implements Callback, OnClickListener, OnItemClickListener, DialogListener
{
    public static final String TAG = RebateToolListFragment.class.getSimpleName();

    private GridView toolList;

    private RootActivity rootAct;

    private ArrayList<DashboardItem> dashboardList;

    private ArrayList<ServiceItem> serviceList;

    private ConfigurationDetail configDetail;

    private RebateToolAdapter rebateToolAdapter;

    private Button rebate_submit;

    private ICFDialogFragment commonDialog;

    private View itemView;

    private int itemPosition = -1;

    private int rebateValue;

    private CheckBox thermoStatCheckbox;

    public RebateToolListFragment()
    {
	super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
	View view = inflater.inflate(R.layout.rebate_list_screen, null);
	return view;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
	super.onViewCreated(view, savedInstanceState);
	getUiControls(view);
	initialize();
	bindCallbacks();
	updateTotalRebate();
    }

    public void getUiControls(View view)
    {
	toolList = (GridView) view.findViewById(R.id.rebate_tool_list);
	rebate_submit = (Button) view.findViewById(R.id.rebate_submit);
	/*
	 * if (UiUtil.SCREEN_INCH_7) { toolList.setNumColumns(1); } else { if (getResources().getBoolean(R.bool.isPhoneLayout)) { toolList.setNumColumns(1); }
	 * else { toolList.setNumColumns(2); } }
	 */

    }

    private void updateTotalRebate()
    {
	TextView textView = (TextView) getView().findViewById(R.id.total_rebate_txt);
	String formatedValue = (String.format("%.02f", RebateManager.getInstance().getFormBean().getTotalRebate()));
	//$$$TBD SHIT
	textView.setText(getResources().getString(R.string.total_estimate_rebate) + formatedValue);
	ICFLogger.d(TAG, "Total Rebate : $" + formatedValue);
    }

    public void initialize()
    {
	rootAct = (RootActivity) getActivity();
	if (rootAct == null)
	{
	    return;
	}
	configDetail = LibUtils.getConfigurationDetail();
	rootAct.updateTitle(getResources().getString(R.string.rebate_tool_txt));
	UtilityCompany companyId = LibUtils.getSelectedUtilityCompany();
	if (companyId != null)
	{
	    dashboardList = companyId.getDashboardItemList();
	}

	for (int i = 0; i < dashboardList.size(); i++)
	{
	    int dashId = LibUtils.parseInteger(dashboardList.get(i).getId(), -1);
	    if (dashId == AppConstants.ID_REBATE_TOOL)
	    {
		serviceList = dashboardList.get(i).getServiceList();
	    }
	}
	if (serviceList != null && serviceList.size() > 0)
	{
	    FormResponseBean bean = RebateManager.getInstance().getFormBean();
	    bean.setServiceList(serviceList);

	    rebateToolAdapter = new RebateToolAdapter(getActivity(), 0, serviceList);
	    rebateToolAdapter.setAdapterListener(rebateToolAdapter);
	    toolList.setAdapter(rebateToolAdapter);
	}
    }

    public void bindCallbacks()
    {
	rebate_submit.setOnClickListener(this);
	toolList.setOnItemClickListener(this);
    }

    @Override
    public boolean handleMessage(Message msg)
    {
	return false;

    }

    @Override
    public void onClick(View v)
    {
	int id = v.getId();
	if (id == R.id.rebate_submit)
	{
	    if (checkValidationStatus()){
	    Submission submission = null;
	    Fragment frag = null;
	    // There can be several different screens sequences for submission based on submission object in form_data.json
	    if ((submission = RebateManager.getInstance().getFormBean().getSubmissionObjectById(AppConstants.SURVEY_ID)) != null) {
		frag = SurveyFragment.newInstance(submission);
	    } 
	    else if((submission = RebateManager.getInstance().getFormBean().getSubmissionObjectById(AppConstants.REBATE_RECIPIENT_ID)) != null) {
		frag = new ConfirmRebateFragment();
	    }
	    else if((submission = RebateManager.getInstance().getFormBean().getSubmissionObjectById(AppConstants.TERMS_AND_CONDITIONS_ID)) != null) {
		frag = new TermAndConditionFragment();
	    } 
	    else {
		frag = new RebateConfirmationFragment();
	    }
	    ((RootActivity) getActivity()).beginTransaction().replace(R.id.mainLayout, frag).addToBackStack(null).commit();
	    }
	}
	else
	{
	}

    }

    private boolean checkValidationStatus()
    {
	final PendingJobItem item = new PendingJobItem();
	FormResponseBean bean = RebateManager.getInstance().getFormBean();
	item.setBean(bean);
	item.setCustomerName(bean.getCustomerInfo().getFirstName() + " " + bean.getCustomerInfo().getLastName());
	item.setCustomerAddress(bean.getCustomerInfo().getCustomerAddress());
	item.setDate(DateFormatter.getFormattedDate(System.currentTimeMillis()));
	item.setLastModified(System.currentTimeMillis());
	item.setFolderPath(FileUtil.getJobDirectory(bean.getCustomerInfo().getJobId()));
	ICFLogger.d(TAG, "PendingJobItem Name : " + bean.getCustomerInfo().getFirstName() + " " + bean.getCustomerInfo().getLastName() + "date:" + DateFormatter.getFormattedDate(System.currentTimeMillis()) + " JOB ID :" + bean.getCustomerInfo().getJobId());
	boolean isformValidationSuccess = RebateManager.getInstance().doFormValidation(item);
	return isformValidationSuccess;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		itemView = view;
		itemPosition = position;
		if (serviceList != null && serviceList.size() >= position) {
			String serviceId = serviceList.get(position).getServiceId();
			String serviceItemName = serviceList.get(position).getServiceItemName();
			if (serviceId.equalsIgnoreCase(getResources().getString(R.string.equipmentId))) {
				EquipmentFragment equipmentFragment = new EquipmentFragment();
				equipmentFragment.setTitle(serviceItemName);
				((RootActivity) getActivity()).beginTransaction().replace(R.id.mainLayout, equipmentFragment).addToBackStack(null).commit();
			} else if (serviceId.equalsIgnoreCase(getResources().getString(R.string.qualityInstallVerificationId))) {
				QIVListFragment fragment = new QIVListFragment();
				fragment.setTitle(serviceItemName);
				fragment.setServiceList(serviceList);
				((RootActivity) getActivity()).beginTransaction().replace(R.id.mainLayout, fragment).addToBackStack(null).commit();
			} else if (serviceId.equalsIgnoreCase(getResources().getString(R.string.tuneUpId))) {
				TuneupTypeListFragment fragment = new TuneupTypeListFragment();
				fragment.setTitle(serviceItemName);
				((RootActivity) getActivity()).beginTransaction().replace(R.id.mainLayout, fragment).addToBackStack(null).commit();
			}
			// KVB 12/1/2015 - Adding support for insulation
			else if (serviceId.equalsIgnoreCase(getResources().getString(R.string.insulationId))) {
				InsulationFragment fragment = new InsulationFragment();
				fragment.setTitle(serviceItemName);
				((RootActivity) getActivity()).beginTransaction().replace(R.id.mainLayout, fragment).addToBackStack(null).commit();
			}
			//---------------------------------------------------------------------------------------------------------------------
			else if (serviceId.equalsIgnoreCase(getResources().getString(R.string.windowDoorId))) {
				WindowDoorFragment fragment = new WindowDoorFragment();
				fragment.setTitle(serviceItemName);
				((RootActivity) getActivity()).beginTransaction().replace(R.id.mainLayout, fragment).addToBackStack(null).commit();
			} else if (serviceId.equalsIgnoreCase(getResources().getString(R.string.ductImprovementId))) {
				DuctImprovementFragment fragment = new DuctImprovementFragment();
				fragment.setTitle(serviceItemName);
				((RootActivity) getActivity()).beginTransaction().replace(R.id.mainLayout, fragment).addToBackStack(null).commit();
			} else if (serviceId.equalsIgnoreCase(getResources().getString(R.string.ductsealingId))) {
				SealingTypeListFragment fragment = new SealingTypeListFragment();
				fragment.setTitle(serviceItemName);
				fragment.setSealingType(getResources().getString(R.string.ductsealingId));
				((RootActivity) getActivity()).beginTransaction().replace(R.id.mainLayout, fragment).addToBackStack(null).commit();
			} else if (serviceId.equalsIgnoreCase(getResources().getString(R.string.airsealingId))) {
				SealingTypeListFragment fragment = new SealingTypeListFragment();
				fragment.setTitle(serviceItemName);
				fragment.setSealingType(getResources().getString(R.string.airsealingId));
				((RootActivity) getActivity()).beginTransaction().replace(R.id.mainLayout, fragment).addToBackStack(null).commit();
			} else if (serviceId.equalsIgnoreCase(getResources().getString(R.string.thermostatReferralId))) {
				FormResponseBean bean = RebateManager.getInstance().getFormBean();
				thermoStatCheckbox = (CheckBox) itemView.findViewById(R.id.thermo_stat_checkbox);
				CustomerInfo info = bean.getCustomerInfo();
				if (bean != null) {
					if (info != null) {
						if (info.getEmailAddress() == null || (info.getEmailAddress() != null && TextUtils.isEmpty(info.getEmailAddress().toString()))) {
							commonDialog = ICFDialogFragment.newDialogFrag(R.layout.email_popup);
							commonDialog.setDialogListener(this);
							commonDialog.showCommitAllowingLoss(getActivity().getSupportFragmentManager(), "emailpopup");
							getActivity().getSupportFragmentManager().executePendingTransactions();
						} else {
							thermoStatCheckbox.setChecked(!thermoStatCheckbox.isChecked());
						}
					}

					bean.setThermostatReferralChecked(thermoStatCheckbox.isChecked());
				}
			} else if (serviceId.equalsIgnoreCase(getResources().getString(R.string.thermostatInstallId))) {
				/*
				 * commonDialog = ICFDialogFragment.newDialogFrag(R.layout.thermostat_install_popup); commonDialog.setDialogListener(this);
				 * commonDialog.showCommitAllowingLoss(getActivity().getSupportFragmentManager(), "thermostatinstall");
				 * ((RootActivity)getActivity()).executePendingTransactions();
				 */
				ThermoStatInstallPagerFragment fragment = new ThermoStatInstallPagerFragment();
				fragment.setTitle(serviceItemName);
				((RootActivity) getActivity()).beginTransaction().replace(R.id.mainLayout, fragment).addToBackStack(null).commit();
			}
		}
	}

    @Override
    public void onFinishInflate(int layoutId, View view)
    {
	if (layoutId == R.layout.email_popup && view != null)
	{
	    handleEmailPopUp(view);
	}
	else if (layoutId == R.layout.thermostat_install_popup && view != null)
	{
	    handlethermostatPopUp(view);
	}

    }

    private void handlethermostatPopUp(View view)
    {
	EditText noOfItems = (EditText) view.findViewById(R.id.thermostat_no_of_items);
	final EditText rebateCal = (EditText) view.findViewById(R.id.thermostat_rebate_calculation);
	ImageButton cancel = (ImageButton) view.findViewById(R.id.thermostat_cancel_btn);
	ImageButton done = (ImageButton) view.findViewById(R.id.thermostat_done_btn);
	noOfItems.addTextChangedListener(new TextWatcher()
	{

	    @Override
	    public void onTextChanged(CharSequence s, int start, int before, int count)
	    {
		if (s.toString().length() >= 1)
		{
		    rebateValue = (Integer.valueOf(s.toString()) * 100);
		    rebateCal.setText(String.valueOf(rebateValue));
		}
		else
		{
		    rebateCal.setText("");
		}
	    }

	    @Override
	    public void beforeTextChanged(CharSequence s, int start, int count, int after)
	    {

	    }

	    @Override
	    public void afterTextChanged(Editable s)
	    {

	    }
	});
	cancel.setOnClickListener(new OnClickListener()
	{

	    @Override
	    public void onClick(View v)
	    {
		commonDialog.dismiss();
	    }
	});
	done.setOnClickListener(new OnClickListener()
	{

	    @Override
	    public void onClick(View v)
	    {
		if (serviceList != null && itemPosition != -1 && itemView != null && serviceList.size() >= itemPosition)
		{
		    serviceList.get(itemPosition).setServiceItemValue(String.valueOf(rebateValue));
		    rebateToolAdapter.notifyDataSetChanged();
		}

		commonDialog.dismiss();
	    }
	});
    }

    private void handleEmailPopUp(View view)
    {
	final EditText email = (EditText) view.findViewById(R.id.customer_mailid);
	ImageButton cancel = (ImageButton) view.findViewById(R.id.email_cancel_btn);
	ImageButton done = (ImageButton) view.findViewById(R.id.email_done_btn);
	cancel.setOnClickListener(new OnClickListener()
	{
	    @Override
	    public void onClick(View v)
	    {
		commonDialog.dismiss();
	    }
	});
	done.setOnClickListener(new OnClickListener()
	{
	    @Override
	    public void onClick(View v)
	    {
		FormResponseBean bean = RebateManager.getInstance().getFormBean();
		if (bean != null)
		{
		    String mailId = email.getText().toString().trim();
		    if (ValidationManager.emailRule.doValidation(email, R.string.validation_email, false, null))
		    {
			return;
		    }
		    CustomerInfo info = bean.getCustomerInfo();
		    if (info != null)
		    {
			info.setEmailAddress(mailId);
		    }
		    if (thermoStatCheckbox != null)
		    {
			thermoStatCheckbox.setChecked(true);
		    }

		    bean.setThermostatReferralChecked(thermoStatCheckbox.isChecked());
		}
		commonDialog.dismiss();
	    }
	});

    }

    @Override
    public void onDismiss()
    {
	// TODO Auto-generated method stub

    }

}
