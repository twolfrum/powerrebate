/**
 * 
 */
package com.icf.rebate.ui.fragment;

import java.util.List;
import java.util.Map;

import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.networklayer.model.CustomerInfo;
import com.icf.rebate.networklayer.model.FormResponseBean.Insulation;
import com.icf.rebate.networklayer.model.FormResponseBean.InsulationItem;
import com.icf.rebate.networklayer.model.FormResponseBean.Options;
import com.icf.rebate.networklayer.model.FormResponseBean.Part;
import com.icf.rebate.networklayer.model.FormResponseBean.WDItem;
import com.icf.rebate.networklayer.model.FormResponseBean.WindowAndDoor;
import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.rebate.ui.controller.FormUIManager;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.controller.FormUIManager.FormViewCreationCallback;
import com.icf.rebate.ui.listeners.FragmentCallFlow;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.ICFLogger;
import com.icf.rebate.ui.util.UiUtil;
import com.icf.rebate.ui.util.UiViewUtils;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnFocusChangeListener;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.CompoundButton.OnCheckedChangeListener;

/**
 * @author Ken Butler Dec 18, 2015
 * WDPageFragment.java
 * Copyright (c) 2015 ICF International
 */
public class WDPageFragment extends Fragment implements OnCheckedChangeListener, FragmentCallFlow, FormViewCreationCallback {
   
    public final String TAG = "WDPageFragment";
    public final String NUMBER_INSTALLED_ID = "numberInstalled";
    public final String INSTALLED_U_VAL_ID = "installedUValue";
    public final String SHGC_VAL_ID = "installedSHGC";
    
    private WDItem mItem;
    private LinearLayout mScrollViewLayout;
    private FormUIManager mManager;
    private WindowAndDoor mWindowAndDoor;
    private boolean isDeleted;
    private String title;
    private EditText mEdtResult, mEdtNumberInstalled, mEdtInstalledUVal, mEdtSHGCVal;
    private double mInstalledUVal = -1; 
    private double mSHGCVal = -1;
    private int mNumInstalledVal = -1;
    
    public WDPageFragment(){
	super();
    }
    
    public static WDPageFragment newInstance(WDItem item)
    {
	if (item == null)
	{
	    throw new IllegalArgumentException("item cannot be null");
	}

	WDPageFragment fragment = new WDPageFragment();
	fragment.setItem(item);
	return fragment;
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
	View view = inflater.inflate(R.layout.fragment_page_insulation, null, false); // Can use insulation layout here
	return view;
    }
    
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
	super.onViewCreated(view, savedInstanceState);
	getUiControls(view);
	initialize(view);
	bindCallbacks(view);
	requestData();

	CustomerInfo cInfo = RebateManager.getInstance().getFormBean().getCustomerInfo();
	if (UiUtil.isJobCompleted(cInfo))
	{
	    UiUtil.setEnableView((ViewGroup) view, false, null);
	}
    }
    
    @Override
    public void requestData()
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void viewCreated(View view, Part part, final Options option) {
		if (mEdtResult == null) {
			String name = null;
			if (part != null) {
				name = part.getId();
				if (name != null && name.equalsIgnoreCase(AppConstants.WD_RESULT_ID)) {
					if (view instanceof EditText) {
						mEdtResult = (EditText) view;
						try {
							mItem.setStaticRebateValue(
									((Map<String, String>) mEdtResult.getTag()).get("staticDisplayedValue"));
						} catch (Exception e) {
						}
					}
				}
			}
		}
		if (mEdtNumberInstalled == null) {
			String name = null;
			if (part != null) {
				name = part.getId();
				if (name != null && name.equalsIgnoreCase(NUMBER_INSTALLED_ID)) {
					if (view instanceof EditText) {
						mEdtNumberInstalled = (EditText) view;
						mNumInstalledVal = LibUtils.parseInteger(mEdtNumberInstalled.getText().toString(), 0);
						mEdtNumberInstalled.addTextChangedListener(new FieldWatcher(FieldWatcher.NUM_INSTALLED_ID));
					}
				}
			}
		}
		if (mEdtInstalledUVal == null) {
			String name = null;
			if (part != null) {
				name = part.getId();
				if (name != null && name.equalsIgnoreCase(INSTALLED_U_VAL_ID)) {
					if (view instanceof EditText) {
						mEdtInstalledUVal = (EditText) view;
						mInstalledUVal = LibUtils.parseInteger(mEdtInstalledUVal.getText().toString(), 0);
						mEdtInstalledUVal.addTextChangedListener(new FieldWatcher(FieldWatcher.U_VAL_ID));
					}
				}
			}
		}
		if (mEdtSHGCVal == null) {
			String name = null;
			if (part != null) {
				name = part.getId();
				if (name != null && name.equalsIgnoreCase(SHGC_VAL_ID)) {
					if (view instanceof EditText) {
						mEdtSHGCVal = (EditText) view;
						mSHGCVal = LibUtils.parseInteger(mEdtSHGCVal.getText().toString(), 0);
						mEdtSHGCVal.addTextChangedListener(new FieldWatcher(FieldWatcher.SHGC_ID));
					}
				}
			}
		}
	}

	@Override
    public void onDestroyView()
    {
	UiUtil.cleanBitmap(getView());
	saveForm();
	resetFormViews();
	
	super.onDestroyView();
    }
    
    public void saveForm()
    {
	if (!isDeleted)
	{
	    List<WDItem> items = ((WindowDoorFormFragment) getParentFragment()).getWDItems();
	    if (items != null)
	    {
		int index = items.indexOf(mItem);
		if (index > -1)
		{
		    ICFLogger.d(TAG, "onDestroyView " + index);
		    ((WindowDoorFormFragment) getParentFragment()).saveForm(this, index, false);
		}
	    }
	}
    }

    private void resetFormViews()
    {
	mEdtResult = null;
    }

    @Override
    public void getUiControls(View root)
    {
	mScrollViewLayout = (LinearLayout) root.findViewById(R.id.scrol_layout);

    }
    
    @Override
    public void initialize(View root)
    {
	mManager = new FormUIManager(getString(R.string.windowDoorId));
	mManager.setFormViewCreationCallback(this);
	mManager.createViews(getParentFragment(), mScrollViewLayout, mItem.getwDDetail().getParts(), mItem.isItemSaved());
	mManager.setEquipmentName(mWindowAndDoor.getId());
    }
    
    @Override
    public void bindCallbacks(View root)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void onCheckedChanged(CompoundButton arg0, boolean arg1)
    {
	// TODO Auto-generated method stub

    }
    
    public EditText getLastFocusedEditField()
    {
	return mManager.getLastFocusedEditField();
    }

    public void updateCaptureImage(int pageIndex)
    {
	UiUtil.updateCaptureImage(mManager, pageIndex + "", getActivity());
    }
    
    public void updateRebate(double rebateValue)
    {
	if (mEdtResult != null)
	{
	    String formatedValue = mItem.getStaticRebateValue();
		if (TextUtils.isEmpty(formatedValue)) {
			formatedValue = (String.format("%.02f", rebateValue));
		}
		//$$$TBD SHIT
	    mEdtResult.setText(formatedValue + "");
	    ICFLogger.d(TAG, "Result:" + "$" + formatedValue + "");
	}
    }

    public WDItem getItem()
    {
	return mItem;
    }

    public void setItem(WDItem item)
    {
	this.mItem = item;
    }

    public WindowAndDoor getWindowAndDoor()
    {
	return mWindowAndDoor;
    }

    public void setWindowAndDoor(WindowAndDoor windowAndDoor)
    {
	this.mWindowAndDoor = windowAndDoor;
    }

    public String getTitleId()
    {
	return title;
    }

    public void setTitleId(String title)
    {
	this.title = title;
    }

    public boolean isDeleted()
    {
	return isDeleted;
    }

    public void setFragmentDeleted()
    {
	this.isDeleted = true;
    }
    
    public double getInstalledUVal()
    {
        return mInstalledUVal;
    }

    public void setInstalledUVal(double mInstalledUVal)
    {
        this.mInstalledUVal = mInstalledUVal;
    }

    public double getSHGCVal()
    {
        return mSHGCVal;
    }

    public void setSHGCVal(double mSHGCVal)
    {
        this.mSHGCVal = mSHGCVal;
    }

    public int getNumInstalledVal()
    {
        return mNumInstalledVal;
    }

    public void setNumInstalledVal(int mNumInstalledVal)
    {
        this.mNumInstalledVal = mNumInstalledVal;
    }




    private class FieldWatcher implements TextWatcher {

	private String id;
	static final String U_VAL_ID = "u_val_id";
	static final String SHGC_ID = "shgc_id";
	static final String NUM_INSTALLED_ID = "num_installed";
	
	
	public FieldWatcher(String id) {
	    this.id = id;
	}
	
	@Override
	public void afterTextChanged(Editable s){}
	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after){}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count)
	{
	    if (id.equals(U_VAL_ID)) {
		try {
		    mInstalledUVal = Double.parseDouble(s.toString());
		} catch (NumberFormatException e) {
		    mInstalledUVal = -1; // Not a number disregard
		}
	    } else if (id.equals(SHGC_ID)) {
		try {
		    mSHGCVal = Double.parseDouble(s.toString());
		} catch (NumberFormatException e) {
		    mSHGCVal = -1; // Not a number disregard
		}
	    } if (id.equals(NUM_INSTALLED_ID)) {
		try {
		    mNumInstalledVal = Integer.parseInt(s.toString());
		} catch (NumberFormatException e) {
		    mNumInstalledVal = -1; // Not a number disregard
		}
	    }
	    
	}
	
    }
  
}
