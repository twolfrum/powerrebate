package com.icf.rebate.ui.util.rebate;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.rebate.ui.util.ICFLogger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 19713 on 2/27/2018.
 */

public class ProjectCostRebateTable {
    private static final String TAG = ProjectCostRebateTable.class.getSimpleName();

    public static final String TABLE_NAME = "project_cost_rebate";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_REBATE_TYPE = "rebate_type";
    public static final String COLUMN_UTILITY_COMPANY = "utility_company";
    public static final String COLUMN_HOUSE_TYPE = "house_type";
    public static final String COLUMN_PROJECT_COST_MULTIPLIER = "project_cost_multiplier";

    public static final String CREATE_TABLE = "CREATE TABLE "
            + TABLE_NAME + "("
            + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMN_REBATE_TYPE + " TEXT,"
            + COLUMN_UTILITY_COMPANY + " INTEGER NOT NULL,"
            + COLUMN_HOUSE_TYPE + " TEXT,"
            + COLUMN_PROJECT_COST_MULTIPLIER + " REAL"
            + ");";

    private static final List<String> mRebateColumns = new ArrayList<String>();

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(CREATE_TABLE);
        Log.v(TAG, "Created table: " + CREATE_TABLE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion){}

    public static void addRebateIntervalFields(int id, SQLiteDatabase database, String csvLine){
        String[] csvLineItems = csvLine.split(",");
        mRebateColumns.clear();
        for (String s : csvLineItems) {
            // Matches 4 digits between 1900 - 2099
            if (s.matches("19\\d\\d|20\\d\\d")) {
                String dateRange = "col" + id+"_"+"01_01_"+s+"__12_31_"+s;
                final String sqlStatement = "ALTER TABLE "
                        + TABLE_NAME
                        + " ADD COLUMN "
                        + dateRange
                        + " REAL"
                        + ";";
                Cursor cursor = database.rawQuery("SELECT * FROM "+TABLE_NAME, null);
                mRebateColumns.add(dateRange);
                if (cursor.getColumnIndex(dateRange) == -1) {
                    database.execSQL(sqlStatement);
                }
            } else if (s.matches("\\d{2}/\\d{2}/\\d{4}-\\d{2}/\\d{2}/\\d{4}")) {
                final String[] date = s.split("-|/");
                final String colName = "col" + id + "_" + date[0] + "_" + date[1] + "_" + date[2] + "__" + date[3] + "_" + date[4] + "_" + date[5];
                final String sqlStatement = "ALTER TABLE "
                        + TABLE_NAME
                        + " ADD COLUMN "
                        + colName
                        + " REAL"
                        + ";";

                mRebateColumns.add(colName);
                Cursor cursor = database.rawQuery("SELECT * FROM "+TABLE_NAME, null);
                if (cursor.getColumnIndex(colName) == -1) {
                    database.execSQL(sqlStatement);
                }
            }
        }
    }

    public static void insert(int id, SQLiteDatabase database, String rowData) {

        // Constants based on columns in csv
        final int REBATE_TYPE = 0;
        final int HOUSE_TYPE = 1;
        final int PROJECT_COST_MULTIPLIER = 2;

        // Last column that's not a date range
        final int LAST_DEFINED_COLUMN = PROJECT_COST_MULTIPLIER;

        if (rowData == null || rowData.length() == 0) {
            return;
        }
        String[] columnData = rowData.split(",");

        try {
            ContentValues values = new ContentValues();

            // Insert utility company
            values.put(COLUMN_UTILITY_COMPANY, id);
            values.put(COLUMN_REBATE_TYPE, columnData[REBATE_TYPE]);
            values.put(COLUMN_HOUSE_TYPE, columnData[HOUSE_TYPE]);
            values.put(COLUMN_PROJECT_COST_MULTIPLIER, columnData[PROJECT_COST_MULTIPLIER]);
            int j = 0;
            for (int i = LAST_DEFINED_COLUMN + 1; i < columnData.length; i++) {
                if (j < mRebateColumns.size()) {
                    values.put(mRebateColumns.get(j++), LibUtils.parseDouble(columnData[i], 0f));
                }
            }

            database.insert(TABLE_NAME, null, values);
            ICFLogger.d(TAG, "Insert : " +TABLE_NAME+ " "+values);
        } catch (SQLiteException e) {
            Log.e(TAG, "Error inserting into " + TABLE_NAME);
            e.printStackTrace();
        }
    }
}
