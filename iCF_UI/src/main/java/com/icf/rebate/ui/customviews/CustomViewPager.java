package com.icf.rebate.ui.customviews;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class CustomViewPager extends ViewPager
{
    private CustomPageIndicator indicator;

    private OnPageChangeListener pageChangeListener;

    private Context context;

    public CustomViewPager(Context context, AttributeSet attrs)
    {
	super(context, attrs);
	this.context = context;
    }

    @Override
    protected void onMeasure(int arg0, int arg1)
    {
	super.onMeasure(arg0, arg1);
    }

    public void onPageChanged()
    {
	if (indicator != null)
	{
	    indicator.update();
	}
    }

    public int getCurrentPage()
    {
	int current = this.getCurrentItem();
	return current;
    }

    public int getPageCount()
    {
	return this.getChildCount();
    }

    public void firePageCountChanged()
    {
	if (indicator != null)
	{
	    indicator.updatePageCount();
	}
    }

    @Override
    public void setOnPageChangeListener(OnPageChangeListener listener)
    {
	if (listener instanceof PagerAdapter)
	{
	    super.setOnPageChangeListener(listener);
	}
	else
	{
	    this.pageChangeListener = listener;
	}
    }

    public OnPageChangeListener getPageChangeListener()
    {
	return this.pageChangeListener;
    }

    public void setPageIndicator(CustomPageIndicator ind)
    {
	this.indicator = ind;
	if (indicator != null)
	{
	    indicator.setPager(this);
	}
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent arg0)
    {
	return super.onInterceptTouchEvent(arg0);
    }

    // end
}
