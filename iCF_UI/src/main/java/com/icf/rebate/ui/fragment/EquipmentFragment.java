package com.icf.rebate.ui.fragment;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import io.fabric.sdk.android.Fabric;
import android.widget.ListView;

import com.icf.rebate.adapter.EquipmentAdapter;
import com.icf.rebate.app.model.DashboardItem.ServiceItem;
import com.icf.rebate.networklayer.model.FormResponseBean;
import com.icf.rebate.networklayer.model.FormResponseBean.Equipment;
import com.icf.rebate.ui.FormFragment;
import com.crashlytics.android.Crashlytics;
import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.ui.RootActivity;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.listeners.FragmentCallFlow;
import com.icf.rebate.ui.listeners.RequestCallFlow;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.UiUtil;

public class EquipmentFragment extends Fragment implements OnItemClickListener, FragmentCallFlow, RequestCallFlow
{
    private ListView equipmentListView;
    private EquipmentAdapter equipmentAdapter;
    // private TextView equipmentHeader;
    private String title;

    public EquipmentFragment()
    {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
	View view = inflater.inflate(R.layout.equipment_screen, null);
	return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
	super.onViewCreated(view, savedInstanceState);
	getUiControls(view);
	initialize(view);
	bindCallbacks(view);
	requestData();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
	// Add crashylitcs for equipment type
	if (equipmentAdapter != null && equipmentAdapter.getListObjects() != null && equipmentAdapter.getListObjects().size() > position && Fabric.isInitialized()) {
	    String str = "";
	    Crashlytics.setString("selectedEquipmentName", str = (equipmentAdapter.getItem(position) == null ? "null" : equipmentAdapter.getItem(position).getName()));
	    Log.d("Equipment", "Setting crashlytics custom tag selectedEquipmentName=" + str);
	}
	FormFragment formFragment = new FormFragment();
	Bundle bundle = new Bundle();
	bundle.putInt("index", position);
	formFragment.setArguments(bundle);
	((RootActivity) getActivity()).beginTransaction().replace(R.id.mainLayout, formFragment).addToBackStack(null).commitAllowingStateLoss();
    }

    // public void updateCompanyDetails()
    // {
    // equipmentHeader.setText(UiUtil.selectedId.getName());
    // if (UiUtil.getResource(UiUtil.selectedId.getIcon()) == 0)
    // {
    // equipmentHeader.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icf_logo, 0, 0, 0);
    // }
    // else
    // {
    // equipmentHeader.setCompoundDrawablesWithIntrinsicBounds(UiUtil.getResource(UiUtil.selectedId.getIcon()), 0, 0, 0);
    // }
    // }

    // AsyncTask<Void, Void, FormResponseBean> syncTask = new AsyncTask<Void, Void, FormResponseBean>()
    // {
    // @Override
    // protected FormResponseBean doInBackground(Void... params)
    // {
    // FormResponseBean bean = null;
    // try
    // {
    // InputStream stream = getActivity().getAssets().open("form_data.json");
    // byte[] data = FileUtil.readInputStream(stream);
    //
    // bean = (FormResponseBean) DataParserImpl.getInstance().parse(ICFHttpManager.REQ_ID_FORM, data);
    // }
    // catch (IOException e)
    // {
    // e.printStackTrace();
    // }
    // catch (Exception e)
    // {
    // e.printStackTrace();
    // }
    //
    // return bean;
    // }
    //
    // protected void onPostExecute(FormResponseBean result)
    // {
    // EquipmentFragment.bean = result;
    // updateEquipmentList();
    // };
    // };

    private void updateEquipmentList(FormResponseBean bean)
    {
	List<Equipment> equipmentList = bean.getEquipments();
	if (equipmentList == null)
	{
	    equipmentList = new ArrayList<Equipment>();
	}
	equipmentAdapter = new EquipmentAdapter(getActivity(), 0, equipmentList);
	equipmentAdapter.setAdapterListener(equipmentAdapter);
	equipmentListView.setAdapter(equipmentAdapter);
    }

    @Override
    public void getUiControls(View root)
    {
	// equipmentHeader = (TextView) root.findViewById(R.id.equipment_header);
	equipmentListView = (ListView) root.findViewById(R.id.equipment_list);
    }

    @Override
    public void initialize(View root)
    {
	RootActivity rootAct = (RootActivity) getActivity();
	if (rootAct != null)
	{
	    rootAct.updateTitle(title);
	}
    }

    @Override
    public void bindCallbacks(View root)
    {
	equipmentListView.setOnItemClickListener(this);
    }

    @Override
    public void showProgressDialog()
    {
	getActivity().runOnUiThread(new Runnable()
	{
	    public void run()
	    {
		UiUtil.showProgressDialog(getActivity());
	    }
	});
    }

    @Override
    public void stopProgressDialog()
    {
	getActivity().runOnUiThread(new Runnable()
	{
	    public void run()
	    {
		UiUtil.dismissSpinnerDialog();
	    }
	});
    }

    @Override
    public void updateData(final Object obj)
    {
	getActivity().runOnUiThread(new Runnable()
	{
	    public void run()
	    {
		updateEquipmentList((FormResponseBean) obj);
	    }
	});
    }

    @Override
    public void requestData()
    {
	RebateManager.getInstance().checkAndrequestData(this);
    }

    @Override
    public void displayError(String title, String message)
    {

    }

    public void setTitle(String title)
    {
	this.title = title;
    }
}
