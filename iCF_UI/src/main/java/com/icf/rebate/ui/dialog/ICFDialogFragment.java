package com.icf.rebate.ui.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.widget.TextView;

import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.ui.util.UiUtil;

public class ICFDialogFragment extends DialogFragment implements OnKeyListener
{
    private Object dataObj;

    private DialogListener listener;

    private Handler mHandler;

    public ICFDialogFragment()
    {
	mHandler = new Handler();
    }

    public static ICFDialogFragment newDialogFrag(String title, String message)
    {
	if (message == null)
	{
	    throw new IllegalArgumentException("message param can not be null !");
	}
	ICFDialogFragment dialog = new ICFDialogFragment();
	Bundle budle = new Bundle();
	budle.putString("title", title);
	budle.putString("message", message);
	dialog.setArguments(budle);
	return dialog;
    }

    public static ICFDialogFragment newDialogFrag(int layoutId)
    {
	if (layoutId < 1)
	{
	    throw new IllegalArgumentException("invalid layoutId !");
	}
	ICFDialogFragment dialog = new ICFDialogFragment();
	Bundle budle = new Bundle();
	budle.putInt("layoutId", layoutId);
	dialog.setArguments(budle);
	return dialog;
    }

    ICFDialogFragment newDialogFrag()
    {
	ICFDialogFragment dialog = new ICFDialogFragment();
	return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
	if (getArguments() != null)
	{
	    int layoutId = getArguments().getInt("layoutId");
	    String title = getArguments().getString("title");
	    String message = getArguments().getString("message");

	    Dialog dialog = new Dialog(getActivity(), R.style.MyTheme_Dialog);

	    if (layoutId == 0)
	    {
		layoutId = R.layout.alert_message_dialog;
	    }

	    dialog.setContentView(layoutId);

	    if (layoutId == R.layout.alert_message_dialog)
	    {
		View close = dialog.findViewById(R.id.message_close);
		close.setOnClickListener(new OnClickListener()
		{
		    @Override
		    public void onClick(View v)
		    {
			dismiss();
		    }
		});

	    }

	    if (layoutId == R.layout.alert_message_dialog || layoutId == R.layout.voiceagreement || layoutId == R.layout.about_screen_popup)
	    {
		final int finalLayoutId = layoutId;
		mHandler.post(new Runnable()
		{
		    public void run()
		    {
			Dialog d = getDialog();
			if (d != null)
			{
			    Window w = d.getWindow();
			    if (w != null)
			    {
				LayoutParams params = w.getAttributes();
				if (params != null)
				{
				    params.width = (int) (UiUtil.getDisplayWidth(getActivity().getApplicationContext()) * 0.75f);
				    if (finalLayoutId == R.layout.voiceagreement)
				    {
					params.height = (int) (UiUtil.getDisplayHeight(getActivity().getApplicationContext()) * 0.75f);
				    }

				}
				w.setAttributes(params);
			    }
			}
		    }
		});
	    }

	    if (title != null)
	    {
		TextView titleView = (TextView) dialog.findViewById(R.id.message_header);
		if (titleView != null)
		{
		    titleView.setText(message);
		}
	    }

	    if (message != null)
	    {
		TextView messg = (TextView) dialog.findViewById(R.id.message_content);
		if (messg != null)
		{
		    messg.setText(message);
		}
	    }

	    dialog.setCanceledOnTouchOutside(false);
	    if (listener != null)
	    {
		listener.onFinishInflate(layoutId, dialog.getWindow().getDecorView());
	    }

	    return dialog;
	}

	return super.onCreateDialog(savedInstanceState);
    }

    public void setDialogListener(DialogListener listener)
    {
	this.listener = listener;
    }

    @Override
    public void onDismiss(DialogInterface dialog)
    {
	super.onDismiss(dialog);
	if (listener != null)
	{
	    listener.onDismiss();
	}
    }

    public Object getDataObj()
    {
	return dataObj;
    }

    public void setDataObj(Object dataObj)
    {
	this.dataObj = dataObj;
    }

    @Override
    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event)
    {
	if (event.getKeyCode() == KeyEvent.KEYCODE_BACK)
	{
	    dismiss();
	}
	return false;
    }

    public void showCommitAllowingLoss(FragmentManager manager, String tag)
    {
	FragmentTransaction transaction = manager.beginTransaction();
	transaction.add(this, tag);
	transaction.commitAllowingStateLoss();
    }
}
