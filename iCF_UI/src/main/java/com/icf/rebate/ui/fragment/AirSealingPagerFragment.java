package com.icf.rebate.ui.fragment;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.adapter.AirSealingAdapter;
import com.icf.rebate.adapter.CustomSpinnerAdapter;
import com.icf.rebate.app.model.AirSealing;
import com.icf.rebate.app.model.AirSealingItem;
import com.icf.rebate.app.model.DashboardItem.ServiceItem;
import com.icf.rebate.networklayer.model.CustomerInfo;
import com.icf.rebate.networklayer.model.FormResponseBean;
import com.icf.rebate.ui.RootActivity;
import com.icf.rebate.ui.controller.FormUIManager;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.customviews.CustomPageIndicator;
import com.icf.rebate.ui.customviews.CustomViewPager;
import com.icf.rebate.ui.dialog.DialogListener;
import com.icf.rebate.ui.dialog.ICFDialogFragment;
import com.icf.rebate.ui.listeners.FragmentCallFlow;
import com.icf.rebate.ui.listeners.RequestCallFlow;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.FileUtil;
import com.icf.rebate.ui.util.UiUtil;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class AirSealingPagerFragment extends Fragment implements RequestCallFlow, FragmentCallFlow,
        OnItemSelectedListener, OnClickListener, DialogListener, OnPageChangeListener, PersistentForm {
    public CustomViewPager viewPager;
    private FormUIManager uiManager;
    private String title;
    private Spinner airComboBox;
    private AirSealingAdapter airSealingAdapter;
    private AirSealing airSealing;
    private List<AirSealingItem> airList;
    private CustomSpinnerAdapter adapter;
    private List<String> spinnerList = new ArrayList<>();
    private int pageIndex = 0;
    private ICFDialogFragment commonDialog;
    private int mSubtypeSelection; /* The index of the air sealing "type" from the previous fragments list view */
    private FormResponseBean bean;

    public AirSealingPagerFragment() {
        super();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mSubtypeSelection = getArguments().getInt(AppConstants.SUBTYPE_SELECT_POSITION_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.form_screen_layout, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getUiControls(view);
        initialize(view);
        bindCallbacks(view);
        requestData();

        CustomerInfo cInfo = RebateManager.getInstance().getFormBean().getCustomerInfo();
        if (UiUtil.isJobCompleted(cInfo)) {
            ArrayList<View> list = new ArrayList<View>();
            list.add(viewPager);
            UiUtil.setEnableView((ViewGroup) view, false, list);
        }
    }

    @Override
    public void requestData() {
        RebateManager.getInstance().checkAndrequestData(this);
    }

    @Override
    public void getUiControls(View root) {
        airComboBox = (Spinner) root.findViewById(R.id.equipment_selector);
        viewPager = (CustomViewPager) root.findViewById(R.id.pager);
    }

    @Override
    public void initialize(View root) {
        uiManager = new FormUIManager("AirSealingPager");
        RootActivity rootAct = (RootActivity) getActivity();
        if (rootAct != null) {
            rootAct.updateTitle(title);
        }

        airSealingAdapter = new AirSealingAdapter(getChildFragmentManager(), viewPager, uiManager);
        viewPager.setAdapter(airSealingAdapter);
        viewPager.setCurrentItem(0);
    }

    private void updateUI() {
        if (airList != null) {
            airSealingAdapter.setAirList(airList);
            airSealingAdapter.notifyDataSetChanged();
            viewPager.setOffscreenPageLimit(airList.size());
            CustomPageIndicator indicator = ((CustomPageIndicator) getActivity().findViewById(R.id.page_indicator));
            viewPager.setPageIndicator(indicator);
            indicator.setPager(viewPager);

            updateSpinnerAdapter();
        }
    }

    @Override
    public void bindCallbacks(View root) {
        airComboBox.setOnItemSelectedListener(this);
        root.findViewById(R.id.save_btn).setOnClickListener(this);
        root.findViewById(R.id.close_btn).setOnClickListener(this);
        root.findViewById(R.id.add_btn).setOnClickListener(this);
        viewPager.setOnPageChangeListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (viewPager.getCurrentPage() != position) {
            viewPager.setCurrentItem(position, true);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onDestroyView() {
        saveForm(false);
        super.onDestroyView();
    }

    private float computeRebate() {
        int count = airSealingAdapter.getCount();
        float rebateValue = 0;

        for (int i = 0; i < count; i++) {
            AirSealingItem item = airList.get(i);
            rebateValue += item.getRebateValue();
        }
        return rebateValue;
    }

    @Override
    public void onClick(View v) {
        if (v != null) {
            UiUtil.hideSoftKeyboard(getActivity(), uiManager.getLastFocusedEditField());

            int id = v.getId();
            if (id == R.id.save_btn) {
                saveForm(pageIndex, true);
            } else if (id == R.id.close_btn) {
                if (viewPager.getPageCount() > 0) {
                    commonDialog = ICFDialogFragment.newDialogFrag(R.layout.delete_confirmation);
                    commonDialog.setDialogListener(this);
                    commonDialog.show(getActivity().getSupportFragmentManager(), "Delete Confirmation");
                    getActivity().getSupportFragmentManager().executePendingTransactions();
                }
            } else if (id == R.id.add_btn) {
                if (airList != null && airList.size() < airSealing.getMaxMeasures()) { //AppConstants.MAX_PAGE_SUPPORT) {
                    AirSealingItem item = null;
                    item = (AirSealingItem) airList.get(0).clone();
                    item.setPageNum(airList.get(airList.size() - 1).getPageNum() + 1);

                    airList.add(item);
                    airSealingAdapter.setAirList(airList);
                    airSealingAdapter.notifyDataSetChanged();
                    viewPager.setOffscreenPageLimit(airList.size());
                    viewPager.firePageCountChanged();
                    viewPager.setCurrentItem(airList.size() - 1);
                    updateSpinnerAdapter();
                } else if (airList.size() == airSealing.getMaxMeasures()) {
                    UiUtil.showError(getActivity(), getResources().getString(R.string.alert_title),
                            getResources().getString(R.string.maximum_measures_limit,
                                    airSealing.getName(), airSealing.getMaxMeasures()));
                }
            }
        }
    }

    @Override
    public int getNoScreensToToolList() {
        return 1;
    }

    @Override
    public void setValidState() {
        // unimpl
    }

    @Override
    public boolean isValid() {
        return RebateManager.getInstance().checkAnyFormFilled(getString(R.string.airsealingId));
    }

    @Override
    public void saveForm(boolean isManualSave) {

        // Update page fragment... so result gets updated
        if (airSealingAdapter != null && airList != null && pageIndex < airList.size()) {
            AirSealingFragment pageFragment = airSealingAdapter.getAirSealingFragment(airList.get(pageIndex));
            if (pageFragment != null) {
                pageFragment.updateAirSealingDetails();
            }
        }
        saveForm(pageIndex, isManualSave);
        RebateManager.getInstance().getFormBean().setAirSealingRebate(computeRebate());
    }

    public synchronized void saveForm(int pageIndex, boolean manualSave) {
        if (airSealingAdapter != null) {
            boolean isItemSaved = airSealingAdapter.setItemSaved(pageIndex, manualSave);

            RebateManager.getInstance().updateFormState(null);
            if (!manualSave && !isItemSaved) {
                deleteItemAtIndex(pageIndex);
                return;
            }

            Toast.makeText(getActivity(), getString(R.string.saved_string), Toast.LENGTH_LONG).show();
        }
    }

    private boolean deleteItemAtIndex(int pageIndex) {
        boolean retVal = false;

        if (airList != null && airList.size() > pageIndex && airList.size() > 1) {
            final int selectedPageNum = airList.get(pageIndex).getPageNum();
            retVal = airSealingAdapter.deleteItem(pageIndex) != null;

            Thread t = new Thread(new Runnable() {
                public void run() {
                    String equipmentName = getActivity().getResources().getString(R.string.airsealingId);
                    FileUtil.deleteFolderPartImage(equipmentName, selectedPageNum + "");
                }
            });
            t.start();
        }
        return retVal;
    }

    @Override
    public void onFinishInflate(int layoutId, View view) {
        if (layoutId == R.layout.delete_confirmation && view != null) {
            ImageButton cancel = (ImageButton) view.findViewById(R.id.delete_cancel_btn);
            ImageButton done = (ImageButton) view.findViewById(R.id.delete_done_btn);
            cancel.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    commonDialog.dismiss();
                }
            });
            done.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    deleteItem();
                }

            });
        }
    }

    @Override
    public void onDismiss() {

    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public void stopProgressDialog() {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                UiUtil.dismissSpinnerDialog();
            }
        });
    }

    @Override
    public void updateData(final Object obj) {
        if (obj != null && obj instanceof FormResponseBean) {
            bean = (FormResponseBean) obj;
            airSealing = ((FormResponseBean) obj).getAirSealing().get(mSubtypeSelection);
            airList = (List<AirSealingItem>) airSealing.getItems();
            airSealingAdapter.setAirSealing(airSealing);

            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    updateUI();
                }
            });
        }

    }

    private synchronized void deleteItem() {
        AirSealingItem newItem = (AirSealingItem) airList.get(0).clone();
        newItem.setPageNum(airList.get(airList.size() - 1).getPageNum() + 1);
        AirSealingFragment fragment = airSealingAdapter.getAirSealingFragment(airList.get(pageIndex));
        final int selectedPageNum = airList.get(pageIndex).getPageNum();
        if (fragment != null) {
            fragment.setFragmentDeleted();
        }
        airSealingAdapter.deleteItem(pageIndex);

        Thread t = new Thread(new Runnable() {
            public void run() {
                String equipmentName = getActivity().getResources().getString(R.string.airsealingId);
                FileUtil.deleteFolderPartImage(equipmentName, selectedPageNum + "");
            }
        });
        t.start();
        spinnerList.remove(pageIndex);
        if (newItem != null && airList.size() == 0) {
            newItem.setPageNum(0);
            airList.add(newItem);
        }
        airSealingAdapter.notifyDataSetChanged();
        viewPager.setOffscreenPageLimit(airList.size());
        viewPager.firePageCountChanged();
        if (pageIndex >= airList.size()) {
            --pageIndex;
        }

        viewPager.setCurrentItem(pageIndex);
        updateSpinnerAdapter();
        commonDialog.dismiss();
    }

    @Override
    public void displayError(String title, String message) {

    }

    @Override
    public void showProgressDialog() {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                UiUtil.showProgressDialog(getActivity());
            }
        });
    }

    @Override
    public void onPageScrollStateChanged(int arg0) {

    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {

    }

    @Override
    public void onPageSelected(int index) {
        pageIndex = index;
        airComboBox.setSelection(pageIndex);
    }

    public void serviceItem(ServiceItem serviceItem) {
        setTitle(serviceItem.getServiceItemName());
    }

    public void updateRebateValue(float value) {
        if (bean != null) {
            bean.setAirSealingRebate(value);
        }
    }

    public String getPageName() {
        AirSealingFragment fragm = (AirSealingFragment) airSealingAdapter.getItem(pageIndex);
        return fragm.getAirSealingItem().getPageNum() + "";
    }

    private void updateSpinnerAdapter() {
        ListIterator<AirSealingItem> iterator = airList.listIterator();
        int index = 1;
        spinnerList = new ArrayList<String>();
        while (iterator.hasNext()) {
            iterator.next();
            spinnerList.add(airSealing.getName() + " " + index++);
        }
        if (spinnerList.size() == 0) {
            spinnerList.add(airSealing.getName() + " " + index);
        }

        adapter = new CustomSpinnerAdapter(getActivity(), 0, spinnerList);
        airComboBox.setAdapter(adapter);
        airComboBox.setSelection(pageIndex, true);
    }

    public List<AirSealingItem> getAirList() {
        return airList;
    }

    public void setAirList(List<AirSealingItem> airList) {
        this.airList = airList;
    }
}
