/**
 * 
 */
package com.icf.rebate.ui.util.rebate;

import java.util.ArrayList;
import java.util.List;

import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.rebate.ui.util.ICFLogger;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

/**
 * @author Ken Butler Dec 18, 2015
 * WindowDoorTable.java
 * Copyright (c) 2015 ICF International
 */
public class WindowDoorTable {
    
    public static final String TAG = "WindowDoorTable";
    
    public static final String TABLE_NAME = "window_door_rebate";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_REBATE_TYPE = "rebate_type";
    public static final String COLUMN_HOUSE_TYPE = "house_type";
    public static final String COLUMN_UTILITY_COMPANY = "utility_company";
    public static final String COLUMN_WD_TYPE = "wd_type";
    public static final String COLUMN_INSTALLED_U_MIN = "installed_u_min";
    public static final String COLUMN_INSTALLED_U_MAX = "installed_u_max";
    public static final String COLUMN_SHGC_MIN = "shgc_min";
    public static final String COLUMN_SHGC_MAX = "shgc_max";
    public static final String COLUMN_REBATE_MAX = "rebate_max_ammount";

    // Create table statement
    public static final String CREATE_TABLE = "CREATE TABLE "
	    + TABLE_NAME + "("
	    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
	    + COLUMN_REBATE_TYPE + " TEXT,"
	    + COLUMN_UTILITY_COMPANY + " INTEGER NOT NULL,"
	    + COLUMN_HOUSE_TYPE + " TEXT,"
	    + COLUMN_WD_TYPE + " TEXT,"
	    + COLUMN_INSTALLED_U_MIN + " REAL,"
	    + COLUMN_INSTALLED_U_MAX + " REAL,"
	    + COLUMN_SHGC_MIN + " REAL,"
	    + COLUMN_SHGC_MAX + " REAL,"
	    + COLUMN_REBATE_MAX + " REAL"
	    + ");";
    
    private static final List<String> mRebateColumns = new ArrayList<String>();
    
    public static void onCreate(SQLiteDatabase database) {
	database.execSQL(CREATE_TABLE);
	Log.v(TAG, "Created table: "+ CREATE_TABLE);
    }
    
    public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion){}

	public static void addRebateIntervalFields(int id, SQLiteDatabase database, String csvLine) {
		String[] csvLineItems = csvLine.split(",");
		mRebateColumns.clear();
		for (String s : csvLineItems) {
			// Matches 4 digits between 1900 - 2099
			if (s.matches("19\\d\\d|20\\d\\d")) {
				String dateRange = "col" + id + "_" + "01_01_" + s + "__12_31_" + s;
				final String sqlStatement = "ALTER TABLE "
						+ TABLE_NAME
						+ " ADD COLUMN "
						+ dateRange
						+ " REAL"
						+ ";";
				mRebateColumns.add(dateRange);
				Cursor cursor = database.rawQuery("SELECT * FROM " + TABLE_NAME, null);
				if (cursor.getColumnIndex(dateRange) == -1) {
 					database.execSQL(sqlStatement);
				}
			} else if (s.matches("\\d{2}/\\d{2}/\\d{4}-\\d{2}/\\d{2}/\\d{4}")) {
				final String[] date = s.split("-|/");
				final String colName = "col" + id + "_" + date[0] + "_" + date[1] + "_" + date[2] + "__" + date[3] + "_" + date[4] + "_" + date[5];
				final String sqlStatement = "ALTER TABLE "
						+ TABLE_NAME
						+ " ADD COLUMN "
						+ colName
						+ " REAL"
						+ ";";
				mRebateColumns.add(colName);
				Cursor cursor = database.rawQuery("SELECT * FROM " + TABLE_NAME, null);
				if (cursor.getColumnIndex(colName) == -1) {
					database.execSQL(sqlStatement);
				}
			}
		}
	}

	public static void insert(int id, SQLiteDatabase database, String[] columnData) {
	
   	// Constants based on columns in csv
   	final int REBATE_TYPE = 0;
   	final int HOUSE_TYPE = 1;
   	final int WD_TYPE = 2;
   	final int U_MIN = 3;
   	final int U_MAX = 4;
   	final int SHGC_MIN = 5;
   	final int SHGC_MAX = 6;
   	final int MAX_REBATE = 7;
   	
   	// Last column that's not a date range
   	final int LAST_DEFINED_COLUMN = MAX_REBATE;
   	
   	try {
   	    ContentValues values = new ContentValues();
   	    
   	    // Insert utility company
   	    values.put(COLUMN_UTILITY_COMPANY, id);
   	    values.put(COLUMN_REBATE_TYPE, columnData[REBATE_TYPE]);
   	    values.put(COLUMN_HOUSE_TYPE, columnData[HOUSE_TYPE]);
   	    values.put(COLUMN_WD_TYPE, columnData[WD_TYPE]);
   	    values.put(COLUMN_INSTALLED_U_MIN, Double.parseDouble(columnData[U_MIN]));
   	    values.put(COLUMN_INSTALLED_U_MAX, Double.parseDouble(columnData[U_MAX]));
   	    values.put(COLUMN_SHGC_MIN, Double.parseDouble(columnData[SHGC_MIN]));
   	    values.put(COLUMN_SHGC_MAX, Double.parseDouble(columnData[SHGC_MAX]));
   	    values.put(COLUMN_REBATE_MAX, Double.parseDouble(columnData[MAX_REBATE]));
   	    int j = 0;
   	    for (int i = LAST_DEFINED_COLUMN + 1; i <columnData.length; i++) {
   		if (j < mRebateColumns.size()) {
   		    values.put(mRebateColumns.get(j++), LibUtils.parseDouble(columnData[i], 0f));
   		}
   	    }
   	    
   	    database.insert(TABLE_NAME, null, values);
   	    ICFLogger.d(TAG, "Insert : " +TABLE_NAME+ " "+values);
   	} catch (SQLiteException e) {
   	    Log.e(TAG, "Error inserting into " + TABLE_NAME);
   	    e.printStackTrace();
   	}
       }
}
