package com.icf.rebate.ui.util;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;

import com.icf.rebate.networklayer.model.FormResponseBean.Part;
import com.icf.rebate.networklayer.model.LoginResponseBean;
import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.rebate.ui.controller.RebateManager;

//TODO: Clean up unused methods

public class FileUtil
{

    private static final String TAG = "FileUtil";

    public static byte[] readInputStream(InputStream is) throws Exception
    {

	byte[] data = null;
	if (is == null)
	{
	    return data;
	}
	ByteArrayOutputStream baos = new ByteArrayOutputStream();
	int lengthRead = -1;
	byte[] bufRead = new byte[10240];
	try
	{
	    while ((lengthRead = is.read(bufRead)) != -1)
	    {
		baos.write(bufRead, 0, lengthRead);
	    }
	    data = baos.toByteArray();
	}
	catch (Exception e)
	{
	    throw e;
	}
	finally
	{
	    if (baos != null)
	    {
		baos.close();
	    }
	    is.close();
	}

	return data;
    }

    public static boolean copyDirectory(File srcDir, File destDir) throws IOException
    {

	ICFLogger.d(TAG, "copyDirectory started from = " + srcDir + " Dest = destDir" + destDir);
	if (srcDir == null || destDir == null)
	{
	    return false;
	}
	destDir.mkdirs();
	File[] srcFiles = srcDir.listFiles();
	if (srcFiles != null && srcFiles.length > 0)
	{
	    boolean nRet = false;
	    for (int i = 0; i < srcFiles.length; i++)
	    {
		if (srcFiles[i].isDirectory())
		{
		    String dirName = srcFiles[i].getName();
		    String destDirName = destDir.getAbsolutePath() + "/" + dirName;
		    File destFolder = new File(destDirName);
		    nRet = copyDirectory(srcFiles[i], destFolder);
		}
		else
		{
		    String destFileName = destDir.getAbsolutePath() + "/" + srcFiles[i].getName();
		    File destFile = new File(destFileName);
		    if (destFile.exists())
		    {
			destFile.delete();
		    }
		    if (!destFile.exists())
		    {
			destFile.createNewFile();
		    }
		    nRet = copyFile(srcFiles[i], destFile);
		}
	    }
	    return nRet;
	}
	return false;
    }

    public static boolean writeFile(byte[] data, File dstFile) throws FileNotFoundException, IOException
    {
	if (data == null)
	{
	    return false;
	}
	ICFLogger.d(TAG, "WriteFile to Dest = destDir" + dstFile);
	if (!dstFile.exists())
	{
	    dstFile.createNewFile();
	}
	if (dstFile.exists())
	{
	    FileOutputStream out = new FileOutputStream(dstFile);
	    try
	    {
		if (out != null)
		{
		    out.write(data);
		    out.flush();
		    return true;
		}
	    }
	    finally
	    {
		if (out != null)
		{
		    out.close();
		}
	    }
	}
	return false;
    }

    public static boolean writeFile(byte[] data, String path, String filename) throws FileNotFoundException, IOException
    {
	if (data == null)
	{
	    return false;
	}
	File dstFile = new File(path, filename);
	return writeFile(data, dstFile);
    }

    public static boolean copyFile(String src, String dst) throws FileNotFoundException, IOException
    {

	ICFLogger.d(TAG, "copyFile Start from = " + src + " Dest = destDir" + dst);
	if (src == null || dst == null)
	{
	    return false;
	}
	File dstFile = new File(dst);
	if (!dstFile.exists())
	{
	    dstFile.createNewFile();
	}
	FileInputStream in = new FileInputStream(src);
	FileOutputStream out = new FileOutputStream(dstFile);
	try
	{
	    if (in != null && out != null)
	    {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] bRead = new byte[1024];
		int l = -1;
		if (baos != null)
		{
		    while ((l = in.read(bRead)) != -1)
		    {
			baos.write(bRead, 0, l);
		    }
		    out.write(baos.toByteArray());
		    out.flush();
		    baos.close();
		    ICFLogger.d(TAG, "copyFile End from = " + src + " Dest = destDir" + dst);
		    return true;
		}
	    }
	}
	finally
	{
	    if (out != null)
	    {
		out.close();
	    }
	    if (in != null)
	    {
		in.close();
	    }
	}
	ICFLogger.d(TAG, "copyFile Fail End from = " + src + " Dest = destDir" + dstFile);
	return false;
    }

    public static boolean copyFile(File srcFile, File dstFile) throws FileNotFoundException, IOException
    {

	ICFLogger.d(TAG, "copyFile Start from = " + srcFile + " Dest = destDir" + dstFile);
	if (srcFile == null || dstFile == null)
	{
	    return false;
	}
	FileInputStream in = new FileInputStream(srcFile);
	FileOutputStream out = new FileOutputStream(dstFile);
	try
	{
	    if (in != null && out != null)
	    {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] bRead = new byte[1024];
		int l = -1;
		if (baos != null)
		{
		    while ((l = in.read(bRead)) != -1)
		    {
			baos.write(bRead, 0, l);
		    }
		    out.write(baos.toByteArray());
		    out.flush();
		    baos.close();
		    ICFLogger.d(TAG, "copyFile End from = " + srcFile + " Dest = destDir" + dstFile);
		    return true;
		}
	    }
	}
	finally
	{
	    if (out != null)
	    {
		out.close();
	    }
	    if (in != null)
	    {
		in.close();
	    }
	}
	ICFLogger.d(TAG, "copyFile Fail End from = " + srcFile + " Dest = destDir" + dstFile);
	return false;
    }

    /*
     * folder - File object to be deleted recursive - true if subdirectories should also be deleted, if false only the files under the folder will be deleted
     */
    public static void deleteFolder(File folder, boolean recursive)
    {
	if (folder.isDirectory())
	{
	    for (File child : folder.listFiles())
	    {
		if (recursive)
		{
		    deleteFolder(child, recursive);
		}
		else
		{
		    child.delete();
		}
	    }
	    folder.delete();
	}
    }

    public static void createSubmitSuccessJson(String path, String jobId, String address)
    {
	File jsonFile = new File(path + File.separator + AppConstants.JOB_DOCUMENT_UI);
	if (jsonFile.exists())
	{
	    jsonFile.delete();
	}
	byte[] stringBytes = ("{\"customerDetails\":{\"jobId\":\"" + jobId + "\",\"jobState\":5,\"address\":\"" + address + "\"}}").getBytes();
	try
	{
	    jsonFile.createNewFile();
	    FileOutputStream fos = new FileOutputStream(jsonFile);
	    fos.write(stringBytes);
	    fos.close();
	    Log.v(TAG, "Post submit JSON file created.");
	}
	catch (FileNotFoundException e)
	{
	    Log.e(TAG, "File not found", e);
	}
	catch (IOException e)
	{
	    Log.e(TAG, "IO Exception", e);
	}
    }

    public static boolean deleteEntireFolder(String path, boolean deleteEntireFolder)
    {

	ICFLogger.d(TAG, "deleteFolder start = " + path + "  DeleteEntireFolder = " + deleteEntireFolder);
	File file = new File(path);
	if (file != null)
	{
	    int i = 0;
	    File[] myFiles = file.listFiles();
	    int length = 0;
	    File temp = null;
	    if (myFiles != null)
	    {
		length = myFiles.length;
	    }
	    while (i < length)
	    {
		do
		{
		    temp = myFiles[i];
		    if (temp == null)
		    {
			break;
		    }
		    if (temp.isDirectory())
		    {
			ICFLogger.d(TAG, "DeleteFolder start " + temp);
			String dirPath = temp.getAbsolutePath();
			deleteEntireFolder(dirPath, true);
			ICFLogger.d(TAG, "DeleteFolder Ends " + temp);
		    }
		    ICFLogger.d(TAG, "Delete File start " + temp);
		    boolean isDeleted = temp.delete();
		    ICFLogger.d(TAG, "Deletion " + (isDeleted ? "success" : "failed"));
		    ICFLogger.d(TAG, "Delete File Ends " + temp);
		}
		while (false);
		i++;
	    }
	    if (deleteEntireFolder)
	    {
		ICFLogger.d(TAG, "Delete Entire Folder started " + path);
		file.delete();
		ICFLogger.d(TAG, "Delete Entire Folder Endeded " + path);
	    }
	    return true;
	}
	return false;
    }

    public static boolean deleteFile(String path)
    {

	ICFLogger.d(TAG, "started deleteFile = " + path);
	File file = new File(path);
	if (file != null)
	{
	    if (file.exists())
	    {
		ICFLogger.d(TAG, "started deleteFile = " + path);
		return file.delete();
	    }
	}
	ICFLogger.d(TAG, "Ends with no delete for  deleteFile = " + path);
	return false;
    }

    public static boolean copyDirectory(String srcPath, String destPath)
    {

	File srcDir = new File(srcPath);
	File destDir = new File(destPath);
	try
	{
	    return copyDirectory(srcDir, destDir);
	}
	catch (IOException e)
	{
	    e.printStackTrace();
	}
	return false;
    }

    public static boolean exists(String clPath)
    {

	File file = new File(clPath);
	return file.exists();
    }

    /*
     * This function is used to get the size of folder
     */
    public static long getFileSize(File Path)
    {

	long foldersize = 0;
	try
	{

	    File[] filelist = Path.listFiles();
	    for (int i = 0; i < filelist.length; i++)
	    {
		if (filelist[i].isDirectory())
		{
		    foldersize += getFileSize(filelist[i]);
		}
		else
		{

		    foldersize += filelist[i].length();
		}
	    }

	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	return foldersize;
    }

    public static void copyFiles(File[] listFiles, File clDirPath, boolean srcDelete)
    {

	if (listFiles == null || clDirPath == null)
	{
	    return;
	}
	for (int i = 0; i < listFiles.length; i++)
	{
	    String destFileName = clDirPath.getAbsolutePath() + "/" + listFiles[i].getName();
	    File destFile = new File(destFileName);
	    if (destFile.exists())
	    {
		destFile.delete();
	    }
	    try
	    {
		if (!destFile.exists())
		{
		    destFile.createNewFile();
		}
		copyFile(listFiles[i], destFile);
	    }
	    catch (Exception e)
	    {

	    }
	    finally
	    {
		if (srcDelete)
		{
		    listFiles[i].delete();
		}
	    }
	}
    }

    public static boolean deleteTable(SQLiteDatabase _dbInstance, String table, String whereClause, String[] whereArgs)
    {

	boolean isUpdated = false;
	try
	{
	    if (_dbInstance != null)
	    {
		int deletedCount = _dbInstance.delete(table, whereClause, whereArgs);
		isUpdated = (deletedCount > 0) ? true : false;
		ICFLogger.d(ICFLogger.TAG, "Deleted count :::: " + deletedCount);
	    }
	}
	catch (Exception e)
	{
	    // TODO: handle exception
	    // System.out.println("Exception in deleteTable() : " +
	    // e.getMessage());
	    // e.printStackTrace();
	    isUpdated = false;
	}
	return isUpdated;
    }

    public boolean checkTable(SQLiteDatabase _dbInstance, String tableName)
    {

	boolean result = false;
	String query = "SELECT * FROM sqlite_master where tbl_name='" + tableName + "'";
	Cursor cur = _dbInstance.rawQuery(query, null);
	if (cur != null)
	{
	    int rowCount = cur.getCount();
	    if (rowCount > 0)
		result = true;
	    cur.close();
	}
	return result;
    }

    public static String getFileNamePath(String path, String fileName)
    {

	if (path == null)
	{
	    return null;
	}
	File file = new File(path + "/" + fileName);
	if (file.exists())
	{
	    return file.getAbsolutePath();
	}
	return null;
    }

    // public static String getFilePathForPartImage(String equipmentName, Part part, String index)
    // {
    // String jobId = RebateManager.getInstance().getFormBean().getCustomerInfo().getJobId();
    // String directoryName = null;
    // try
    // {
    // directoryName = getJobDirectory(jobId) + File.separator + URLEncoder.encode(equipmentName, "UTF-8") + File.separator + index + File.separator;
    // }
    // catch (Exception e)
    // {
    // directoryName = getJobDirectory(jobId) + File.separator + equipmentName + File.separator + index + File.separator;
    // }
    // File directoryFile = new File(directoryName);
    // if (!directoryFile.exists())
    // {
    // directoryFile.mkdirs();
    // }
    // return directoryName + part.getName() + ".jpg";
    // }
    public static String getFilePathForEquipment(String equipmentName, String index)
    {
	String jobId = RebateManager.getInstance().getFormBean().getCustomerInfo().getJobId();
	String directoryName = null;
	try
	{
	    directoryName = getJobDirectory(jobId) + File.separator + URLEncoder.encode(equipmentName, "UTF-8") + File.separator + index + File.separator;
	}
	catch (Exception e)
	{
	    directoryName = getJobDirectory(jobId) + File.separator + equipmentName + File.separator + index + File.separator;
	}
	File directoryFile = new File(directoryName);
	if (!directoryFile.exists())
	{
	    directoryFile.mkdirs();
	}
	return directoryName;
    }

    public static String getFilePathForPartImage(String equipmentName, Part part, String index)
    {
	return getFilePathForEquipment(equipmentName, index) + part.getName() + ".jpg";
    }

    public static void deleteFolderPartImage(String equipmentName, String pageNum)
    {
	String jobId = RebateManager.getInstance().getFormBean().getCustomerInfo().getJobId();
	String directoryName = null;
	try
	{
	    directoryName = getJobDirectory(jobId) + File.separator + URLEncoder.encode(equipmentName, "UTF-8") + File.separator + pageNum + File.separator;
	}
	catch (Exception e)
	{
	    directoryName = getJobDirectory(jobId) + File.separator + equipmentName + File.separator + pageNum + File.separator;
	}
	File directoryFile = new File(directoryName);
	if (directoryFile.exists())
	{
	    deleteEntireFolder(directoryName, true);
	}
    }

    public static void createCurrentUserDirectory()
    {
	File file = new File(getUserUtilityCompanyDirectory());
	if (file != null && !file.exists())
	{
	    file.mkdirs();
	}
    }

    public static String getRootDirectory()
    {
	if (UiUtil.DEBUG_BUILD)
	{
	    return Environment.getExternalStorageDirectory() + File.separator + "icf";
	}
	else
	{
	    return LibUtils.getApplicationContext().getExternalFilesDir("icf").toString();
	}
    }

    public static String getUserUtilityCompanyDirectory()
    {
	String path = getRootDirectory();
	LoginResponseBean userDetails = LibUtils.getLoggedInUserBean();
	if (userDetails != null)
	{
	    path += File.separator + userDetails.getUserName() + File.separator + LibUtils.getSelectedUtilityCompany().getName();
	}

	return path;
    }

    public static String getHttpResponseDirectory(String username, String responseFileName)
    {
	String path = null;
	if (UiUtil.DEBUG_BUILD)
	{
	    path = Environment.getExternalStorageDirectory() + File.separator + "response";
	}
	else
	{
	    path = LibUtils.getApplicationContext().getExternalFilesDir("response").toString();
	}

	path = path + File.separator + username + File.separator;
	File file = new File(path);
	if (!file.exists())
	{
	    file.mkdirs();
	}
	path += responseFileName;
	return path;
    }

    public static String getCurrentUserDirectory()
    {
	String path = getRootDirectory();
	LoginResponseBean userDetails = LibUtils.getLoggedInUserBean();
	if (userDetails != null)
	{
	    path += File.separator + userDetails.getUserName();
	}
	return path;
    }

    private static void createCustomerUserDirectory(String path)
    {
	File file = new File(path);
	if (file != null && !file.exists())
	{
	    file.mkdirs();
	}
    }

    public static String getJobDirectory(String jobId)
    {
	String path = getUserUtilityCompanyDirectory() + File.separator + jobId;
	createCustomerUserDirectory(path);
	return path;
    }

    public static File getResourceBundleFile(String fileName)
    {
	return new File(LibUtils.getApplicationContext().getExternalFilesDir(LibUtils.FILES_RB), fileName);
    }

    public static byte[] getResourceBundleFileContent(String fileName)
    {
	try
	{
	    FileInputStream fin = new FileInputStream(getResourceBundleFile(fileName));
	    byte[] data = readInputStream(fin);
	    return data;
	}
	catch (Exception e)
	{
	    ICFLogger.e(TAG, e);
	}
	return null;
    }

    public static void doZipOnlyFiles(String filePath, String destFileWithPath) throws IOException
    {
	if (filePath != null)
	{
	    File outFile = new File(destFileWithPath);
	    if (!outFile.exists())
	    {
		outFile.createNewFile();
	    }
	    if (!outFile.exists())
	    {
		return;
	    }
	    File inputFile = new File(filePath);
	    String[] tempFilePath = inputFile.list(new FilenameFilter()
	    {

		@Override
		public boolean accept(File dir, String filename)
		{
		    if (filename.startsWith("ICFLogcat"))
		    {
			return true;
		    }
		    return false;
		}
	    });
	    if (tempFilePath != null)
	    {
		ZipOutputStream out = new ZipOutputStream(new FileOutputStream(outFile));
		try
		{
		    out.setMethod(ZipOutputStream.DEFLATED);
		    for (String tempPath : tempFilePath)
		    {
			ZipEntry fileEntry = new ZipEntry(tempPath);
			FileInputStream in = new FileInputStream(filePath + tempPath);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			out.putNextEntry(fileEntry);
			byte[] bRead = new byte[1024];
			int l = -1;
			if (baos != null)
			{
			    try
			    {
				while ((l = in.read(bRead)) != -1)
				{
				    baos.write(bRead, 0, l);
				}
				out.write(baos.toByteArray());
			    }
			    finally
			    {
				in.close();
				baos.close();
			    }

			}
			out.closeEntry();
		    }
		}
		finally
		{
		    out.finish();
		    out.close();
		}
	    }
	}
    }

    public static void doZip(String[] folderPath, String excludePath, String destFileWithPath, FilenameFilter fileNameFilter) throws IOException
    {
	ICFLogger.d(TAG, "doZip started");

	if (folderPath != null)
	{
	    File outFile = new File(destFileWithPath);
	    if (!outFile.exists())
	    {
		outFile.createNewFile();
	    }
	    if (!outFile.exists())
	    {
		return;
	    }
	    ZipOutputStream out = new ZipOutputStream(new FileOutputStream(outFile));
	    try
	    {
		for (int folIdx = 0; folIdx < folderPath.length; folIdx++)
		{
		    File file = new File(folderPath[folIdx]);
		    if (file.exists())
		    {
			if (excludePath == null)
			{
			    excludePath = file.getParent();
			}
			out.setMethod(ZipOutputStream.DEFLATED);
			File[] subfiles = null;
			if (fileNameFilter == null)
			{
			    subfiles = file.listFiles();
			}
			else
			{
			    subfiles = file.listFiles(fileNameFilter);
			}

			if (subfiles != null)
			{
			    Queue<File> fileQ = new LinkedList<File>(Arrays.asList(subfiles));
			    // Zip all nested files
			    while (!fileQ.isEmpty())
			    {
				File subfile = fileQ.remove();
				// If it is a directory add its children to the queue
				if (subfile.isDirectory())
				{
				    if (fileNameFilter == null)
				    {
					fileQ.addAll(Arrays.asList(subfile.listFiles()));
				    }
				    else
				    {
					fileQ.addAll(Arrays.asList(subfile.listFiles(fileNameFilter)));
				    }
				}
				// Otherwise add the file to the zip archive
				else
				{
				    // Flatten path
				    String path = getExcludedPath(subfile.getPath() + "/", excludePath);
				    path = removeLeadingSeperator(path);
				    path = removeTrailingSeperator(path);
				    path = flattenFile(path, "_");
				    // Add zip entry
				    ZipEntry fileEntry = new ZipEntry(path);
				    FileInputStream in = new FileInputStream(subfile);
				    ByteArrayOutputStream baos = new ByteArrayOutputStream();
				    out.putNextEntry(fileEntry);
				    byte[] bRead = new byte[1024];
				    int l = -1;
				    if (baos != null)
				    {
					try
					{
					    while ((l = in.read(bRead)) != -1)
					    {
						baos.write(bRead, 0, l);
					    }
					    out.write(baos.toByteArray());
					}
					finally
					{
					    in.close();
					    baos.close();
					}

				    }
				    out.closeEntry();
				}
			    }
			    // Old code that was zipping the files
			    
			    // for (int i = 0; i < subfiles.length; i++)
			    // {
			    // if (subfiles[i].isDirectory())
			    // {
			    // String path = getExcludedPath(subfiles[i].getPath() + "/", excludePath);
			    // path = removeLeadingSeperator(path);
			    //
			    // // ZipEntry entry = new ZipEntry(path);
			    // // out.putNextEntry(entry);
			    // // doInternalZip(subfiles[i].getPath(), excludePath, out, fileNameFilter);
			    // continue;
			    // }
			    // String path = getExcludedPath(subfiles[i].getPath(), excludePath);
			    // path = removeLeadingSeperator(path);
			    // ZipEntry fileEntry = new ZipEntry(path);
			    // FileInputStream in = new FileInputStream(subfiles[i]);
			    // ByteArrayOutputStream baos = new ByteArrayOutputStream();
			    // out.putNextEntry(fileEntry);
			    // byte[] bRead = new byte[1024];
			    // int l = -1;
			    // if (baos != null)
			    // {
			    // try
			    // {
			    // while ((l = in.read(bRead)) != -1)
			    // {
			    // baos.write(bRead, 0, l);
			    // }
			    // out.write(baos.toByteArray());
			    // }
			    // finally
			    // {
			    // in.close();
			    // baos.close();
			    // }
			    //
			    // }
			    // out.closeEntry();
			    // }
			}
		    }
		}
	    }
	    finally
	    {
		out.finish();
		out.close();
	    }
	}
	ICFLogger.d(TAG, "doZip Ends");

    }

    private static void doInternalZip(String folderPath, String excludePath, ZipOutputStream out, FilenameFilter fileNameFilter) throws IOException
    {
	ICFLogger.d(TAG, "doInternalZip started");
	if (folderPath == null || out == null)
	{
	    return;
	}
	File file = new File(folderPath);
	if (file.exists())
	{
	    File[] subfiles = null;
	    if (fileNameFilter == null)
	    {
		subfiles = file.listFiles();
	    }
	    else
	    {
		subfiles = file.listFiles(fileNameFilter);
	    }

	    if (subfiles != null)
	    {
		for (int i = 0; i < subfiles.length; i++)
		{
		    if (subfiles[i].isDirectory())
		    {
			String path = getExcludedPath(subfiles[i].getPath() + "/", excludePath);
			path = removeLeadingSeperator(path);
			ZipEntry entry = new ZipEntry(path);
			out.putNextEntry(entry);
			doInternalZip(subfiles[i].getPath(), excludePath, out, fileNameFilter);
			continue;
		    }
		    String path = getExcludedPath(subfiles[i].getPath(), excludePath);
		    path = removeLeadingSeperator(path);
		    ZipEntry fileEntry = new ZipEntry(path);
		    FileInputStream in = new FileInputStream(subfiles[i]);
		    ByteArrayOutputStream baos = new ByteArrayOutputStream();
		    out.putNextEntry(fileEntry);
		    byte[] bRead = new byte[1024];
		    int l = -1;
		    if (baos != null)
		    {
			try
			{
			    while ((l = in.read(bRead)) != -1)
			    {
				baos.write(bRead, 0, l);
			    }
			    out.write(baos.toByteArray());
			}
			finally
			{
			    in.close();
			    baos.close();
			}

		    }
		    out.closeEntry();
		}
	    }
	}
	ICFLogger.d(TAG, "doInternalZip Ended");
    }

    // Remove leading '/' to avoid an issue where Windows Explorer will not see the zipped contents
    private static String removeLeadingSeperator(String path)
    {
	if (path.indexOf('/') == 0)
	{
	    return path.replaceFirst("/", "");
	}
	else
	{
	    return path;
	}
    }

    /**
     *  Remove trailing '/' so it won't be flattened by {@link #flattenFile}
     */
    private static String removeTrailingSeperator(String path)
    {
	if (path != null 
		&& path.length() > 1 
		&& path.charAt(path.length() - 1) == '/')
	    return path.substring(0, path.length() - 1);
	else
	    return path;
	   
    }
    
    /**
     * 
     * @param directoryDelimiter
     *            - what to replace the '/' file separator with
     * @return
     */
    public static String flattenFile(String path, String directoryDelimiter)
    {
	if (path == null)
	{
	    return null;
	}
	else
	{
	    return path.replaceAll("/", directoryDelimiter == null ? "_" : directoryDelimiter);
	}
    }

    private static String getExcludedPath(String realPath, String excludePath)
    {
	String path = realPath;
	if (path.startsWith(excludePath))
	{
	    path = path.substring(excludePath.length());
	}
	return path;
    }

    public static byte[] getFileContent(String path)
    {
	try
	{
	    return readInputStream(new FileInputStream(path));
	}
	catch (Exception e)
	{
	    ICFLogger.e(TAG, path, e);
	}
	return null;
    }

    public static String getRelativePath(String filePath)
    {
	if (!TextUtils.isEmpty(filePath))
	{
	    String rootPath = RebateManager.getInstance().getFormBean().getCustomerInfo().getJobRootPath();
	    // String jobId = RebateManager.getInstance().getFormBean().getCustomerInfo().getJobId();
	    return filePath.replace(rootPath, "");
	}
	return filePath;
    }

    public static String getAbsolutePath(String filePath)
    {
	if (!TextUtils.isEmpty(filePath))
	{
	    String rootPath = RebateManager.getInstance().getFormBean().getCustomerInfo().getJobRootPath();
	    // String jobId = RebateManager.getInstance().getFormBean().getCustomerInfo().getJobId();
	    return rootPath + filePath;
	}
	return filePath;
    }

    public static boolean cp(String from, String to)
    {
	final int BUFFER_SIZE = 256;

	byte[] buffer = new byte[BUFFER_SIZE];
	boolean success = false;
	File fromFile = new File(from);
	File toFile = new File(to);

	try
	{
	    FileInputStream fis = new FileInputStream(fromFile);
	    FileOutputStream fos = new FileOutputStream(toFile);
	    int count = 0;
	    while ((count = fis.read(buffer, 0, BUFFER_SIZE)) != -1)
	    {
		fos.write(buffer, 0, count);
	    }
	    fis.close();
	    fos.close();
	    success = true;
	}
	catch (FileNotFoundException e)
	{
	    Log.e(TAG, "File not found");
	}
	catch (IOException e)
	{
	    Log.e(TAG, "IOException Occured");
	}
	return success;
    }
}
