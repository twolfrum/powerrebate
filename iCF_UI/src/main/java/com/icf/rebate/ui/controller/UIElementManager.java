package com.icf.rebate.ui.controller;

import java.util.ArrayList;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.icf.rebate.networklayer.model.UIElement;
import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.ui.util.UiUtil;
import com.icf.rebate.ui.util.UiViewUtils;

public class UIElementManager
{
    private Context mContext;

    public UIElementManager(Context context)
    {
	mContext = context;
    }

    public View inflate(ArrayList<UIElement> list)
    {
	ViewGroup rootLayout = createLinearLayout(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, -1, LinearLayout.VERTICAL);
	rootLayout.setBackgroundResource(R.color.background);

	// Launch Button
	LinearLayout manifoldLaunch = createLinearLayout(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1, LinearLayout.HORIZONTAL);
	android.widget.LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) manifoldLaunch.getLayoutParams();
	params.topMargin = mContext.getResources().getDimensionPixelOffset(R.dimen.qiv_top_margin);
	manifoldLaunch.setLayoutParams(params);
	manifoldLaunch.setGravity(Gravity.CENTER_HORIZONTAL);

	rootLayout.addView(manifoldLaunch);

	// manifoldLaunch.addView(createButton(mContext.getString(R.string.launchmanifold), 0, LayoutParams.WRAP_CONTENT, 0.8f));

	// Value Layout
	LinearLayout valueLayout = createLinearLayout(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1, LinearLayout.HORIZONTAL);
	params = (android.widget.LinearLayout.LayoutParams) manifoldLaunch.getLayoutParams();
	params.topMargin = mContext.getResources().getDimensionPixelOffset(R.dimen.qiv_top_margin);
	valueLayout.setLayoutParams(params);
	valueLayout.setGravity(Gravity.CENTER_HORIZONTAL);

	rootLayout.addView(valueLayout);

	LinearLayout subValueLayout = createLinearLayout(0, LayoutParams.WRAP_CONTENT, 0.8f, -1, LinearLayout.VERTICAL);
	int padding = mContext.getResources().getDimensionPixelOffset(R.dimen.qiv_text_entry_padding);
	subValueLayout.setPadding(padding, padding, padding, padding);
	subValueLayout.setBackgroundResource(R.drawable.homescreen_items_bg);

	for (UIElement element : list)
	{
	    if (element.getInputType() == null && element.getName() != null && element.getDataType() != null)
	    {
		createLabelAndInput(element, subValueLayout);
	    }
	    else if (element.getInputType() != null && element.getInputType().equalsIgnoreCase("B"))
	    {
		if (element.getName().equalsIgnoreCase("Launch iManifold"))
		{
		    Button button = createButton(element.getName(), 0, LayoutParams.WRAP_CONTENT, 0.8f);
		    button.setTag("manifold");
		    manifoldLaunch.addView(button);
		}
		else
		{
		    subValueLayout.addView(createButton(element.getName(), LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, -1));
		}
	    }
	    else if (element.isPicture())
	    {
		createPictureLayout(element, subValueLayout);
	    }
	    else if (element.getInputType() != null && element.getInputType().equalsIgnoreCase("CO"))
	    {
		createComboBox(element, subValueLayout);
	    }
	}
	View view = rootLayout.findViewWithTag("manifold");
	if (view == null)
	{
	    rootLayout.removeView(manifoldLaunch);
	}
	valueLayout.addView(subValueLayout);
	return rootLayout;
    }

    private RelativeLayout createRelativeLayout(int width, int height, float weight)
    {
	RelativeLayout layout = new RelativeLayout(mContext);
	LayoutParams lp = null;
	if (weight == -1)
	{
	    lp = new RelativeLayout.LayoutParams(width, height);
	}
	else
	{
	    lp = new LinearLayout.LayoutParams(width, height, weight);
	}
	layout.setLayoutParams(lp);
	return layout;
    }

    private LinearLayout createLinearLayout(int width, int height, float weight, int weightSum, int orientation)
    {
	LinearLayout group = new LinearLayout(mContext);
	LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, height);
	if (weight > 0)
	{
	    params.weight = weight;
	}
	if (weightSum > 0)
	{
	    group.setWeightSum(weightSum);
	}
	group.setOrientation(orientation);
	group.setLayoutParams(params);
	return group;
    }

    private LinearLayout createLinearLayout(int width, int height, int weightSum, int orientation)
    {
	return createLinearLayout(width, height, -1, weightSum, orientation);
    }

    private Button createButton(String buttonText, int width, int height, float weight)
    {
	Button button = UiViewUtils.createButtonView(mContext, buttonText, null);
	LayoutParams params = null;
	if (weight > 0)
	{
	    params = new LinearLayout.LayoutParams(width, height, weight);
	}
	else
	{
	    params = new LayoutParams(width, height);
	}
	button.setLayoutParams(params);
	button.setTextColor(mContext.getResources().getColor(R.color.white));
	button.setOnClickListener(new OnClickListener()
	{

	    @Override
	    public void onClick(View v)
	    {
		UiUtil.showNotImplemented(LibUtils.getApplicationContext());
	    }
	});
	return button;
    }

    private void createLabelAndInput(UIElement item, ViewGroup vg)
    {
	TextView label = UiViewUtils.createTextView(mContext, item.getName(), 15);
	label.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
	label.setTextColor(mContext.getResources().getColor(R.color.header_bg));
	vg.addView(label);

	EditText edtText = UiViewUtils.createEditTextView(mContext, null);
	LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) edtText.getLayoutParams();
	params.width = LayoutParams.MATCH_PARENT;
	params.topMargin = mContext.getResources().getDimensionPixelOffset(R.dimen.qiv_text_entry_top_margin);
	edtText.setLayoutParams(params);
	edtText.setBackgroundResource(R.drawable.btn_bg_transparent);
	vg.addView(edtText);
    }

    private void createPictureLayout(UIElement element, LinearLayout subValueLayout)
    {
	LinearLayout layout = createLinearLayout(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 2, LinearLayout.HORIZONTAL);
	android.widget.LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) layout.getLayoutParams();
	params.topMargin = mContext.getResources().getDimensionPixelOffset(R.dimen.qiv_text_entry_top_margin);
	int padding = mContext.getResources().getDimensionPixelOffset(R.dimen.qiv_text_entry_padding);
	layout.setPadding(padding, padding, padding, padding);
	layout.setBackgroundResource(R.drawable.part_bg);
	layout.setOnClickListener(new OnClickListener()
	{

	    @Override
	    public void onClick(View v)
	    {
		UiUtil.showNotImplemented(LibUtils.getApplicationContext());
	    }
	});

	TextView label = UiViewUtils.createTextView(mContext, element.getName(), 15);
	label.setLayoutParams(new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 1.5f));
	label.setSingleLine(false);
	label.setTextColor(mContext.getResources().getColor(R.color.header_bg));
	layout.addView(label);

	ImageView imgView = UiViewUtils.createImageButtonView(mContext, R.drawable.camera_img);
	imgView.setLayoutParams(new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 0.5f));
	layout.addView(imgView);

	subValueLayout.addView(layout);
    }

    private void createComboBox(UIElement element, LinearLayout subValueLayout)
    {

	TextView label = UiViewUtils.createTextView(mContext, element.getName(), 15);
	LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	params.bottomMargin = mContext.getResources().getDimensionPixelOffset(R.dimen.qiv_text_entry_top_margin);
	label.setLayoutParams(params);
	label.setTextColor(mContext.getResources().getColor(R.color.header_bg));
	subValueLayout.addView(label);

	// View view = UiViewUtils.createComboBoxView(mContext, null, null, element.getValueList(), new OnItemSelectedListener()
	// {
	//
	// @Override
	// public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
	// {
	//
	// }
	//
	// @Override
	// public void onNothingSelected(AdapterView<?> parent)
	// {
	//
	// }
	// });
	// ((Spinner) view).setSelection(0);
	//
	// subValueLayout.addView(UiViewUtils.createSpinnerBg(mContext, view));
    }
}
