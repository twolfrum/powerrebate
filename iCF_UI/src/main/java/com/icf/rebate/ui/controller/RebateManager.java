package com.icf.rebate.ui.controller;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.icf.ameren.rebate.ui.BuildConfig;
import com.icf.rebate.app.model.AirSealing;
import com.icf.rebate.app.model.AirSealingItem;
import com.icf.rebate.app.model.DuctSealing;
import com.icf.rebate.app.model.DuctSealingItem;
import com.icf.rebate.networklayer.DataParserImpl;
import com.icf.rebate.networklayer.HttpManagerListener;
import com.icf.rebate.networklayer.ICFHttpManager;
import com.icf.rebate.networklayer.model.AppDetails;
import com.icf.rebate.networklayer.model.ContractorDetails;
import com.icf.rebate.networklayer.model.CustomerInfo;
import com.icf.rebate.networklayer.model.CustomerScreenLayout;
import com.icf.rebate.networklayer.model.FormResponseBean;
import com.icf.rebate.networklayer.model.FormResponseBean.Equipment;
import com.icf.rebate.networklayer.model.FormResponseBean.Insulation;
import com.icf.rebate.networklayer.model.FormResponseBean.InsulationItem;
import com.icf.rebate.networklayer.model.FormResponseBean.Item;
import com.icf.rebate.networklayer.model.FormResponseBean.TuneUp;
import com.icf.rebate.networklayer.model.FormResponseBean.TuneUpItem;
import com.icf.rebate.networklayer.model.FormResponseBean.WDItem;
import com.icf.rebate.networklayer.model.FormResponseBean.WindowAndDoor;
import com.icf.rebate.networklayer.model.PendingJobItem;
import com.icf.rebate.networklayer.model.QIVItem;
import com.icf.rebate.networklayer.model.ResponseBean;
import com.icf.rebate.networklayer.model.ThermostatInstallItem;
import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.rebate.ui.PendingJobsFragment;
import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.ui.RootActivity;
import com.icf.rebate.ui.listeners.RequestCallFlow;
import com.icf.rebate.ui.util.Analytics;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.FileUtil;
import com.icf.rebate.ui.util.ICFLogger;
import com.icf.rebate.ui.util.UiUtil;

public class RebateManager implements HttpManagerListener
{

    private static final String TAG = RebateManager.class.getName();

    private FormResponseBean defaultBean = null;

    private FormResponseBean currentBean = null;

    private static RebateManager _instance;

    private RequestCallFlow callFlowImpl;

    private JSONObject autoSuggestList;

    private ContractorDetails contractorBean;

    // private Hashtable<String, AutoSuggestData> suggestHashtable;

    private RebateManager()
    {
    }

    public static RebateManager getInstance()
    {
	if (_instance == null)
	{
	    _instance = new RebateManager();
	}
	return _instance;
    }

    public void updateAdapter()
    {
	RootActivity rActivity = UiUtil.rootActivity.get();
	if (rActivity != null)
	{
	    Fragment frag = rActivity.getSupportFragmentManager().findFragmentById(R.id.mainLayout);
	    if (frag instanceof PendingJobsFragment)
	    {
		((PendingJobsFragment) frag).refreshAdapter();
	    }
	}
    }

    public boolean doFormValidation(PendingJobItem p)
    {
	int formState = p.getBean().isFormValid();
	if (formState != AppConstants.FORM_ITEM_FILLED_STATE.ITEM_COMPLETELY_SAVED.getState())
	{
	    int jobState = p.getBean().getCustomerInfo().getState();
	    ICFLogger.d(TAG, "Job State :" + jobState);
	    // p.setJobState(CustomerInfo.INCOMPLETED_JOB);
	    // RebateManager.getInstance().updateFormState(p.getBean(), CustomerInfo.INCOMPLETED_JOB);
	    // updateAdapter();

	    Activity activity = UiUtil.rootActivity.get();
	    String msg = null;

	    if (formState == AppConstants.FORM_EDITED_STATE.FORM_UNEDITED.getState())
	    {
		msg = activity.getString(R.string.form_unedited_msg);
		ICFLogger.d(TAG, "Validation error:" + activity.getString(R.string.error_title) + msg);
	    }
	    else if (formState == AppConstants.FORM_ITEM_FILLED_STATE.ITEM_PARTIALLY_SAVED.getState())
	    {
		msg = activity.getString(R.string.form_validation_failed_msg);
		p.getBean().setEditMode(AppConstants.FORM_EDIT_MODE.VALIDATION_MODE.getMode());
		ICFLogger.d(TAG, "Validation error:" + activity.getString(R.string.error_title) + msg);
		RebateManager.getInstance().updateFormState(p.getBean());
	    }

	    if (msg != null)
	    {
		UiUtil.showError(activity, activity.getString(R.string.error_title), msg);
		ICFLogger.d(TAG, "doSubmission error:" + activity.getString(R.string.error_title) + msg);
	    }

	    return false;
	}
	else
	{
	    return true;
	}
    }

    public void doSubmission(final PendingJobItem p, final int errorId)
    {
	// Comment this for no validation
	
	if (!doFormValidation(p))
	{
	    return;
	}
	// --------------------------------
	
	p.setJobState(CustomerInfo.SUBMISSION_IN_PROGRESS);
	ICFLogger.d(TAG, "Job State :" + CustomerInfo.SUBMISSION_IN_PROGRESS);
	RebateManager.getInstance().updateFormState(p.getBean(), CustomerInfo.SUBMISSION_IN_PROGRESS, new Runnable()
	{

	    @Override
	    public void run()
	    {
		try
		{

		    // Timestamp of app submission
		    DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);
		    AppDetails appDetails = p.getBean().getAppDetails();
		    appDetails.setApplicationSubmitDate(df.format(new Date(System.currentTimeMillis())));
		    p.getBean().setAppDetails(appDetails);
		    
		    
		    String destFolder = FileUtil.getCurrentUserDirectory() + "/submit";
		    File folder = new File(destFolder);
		    if (!folder.exists())
		    {
			folder.mkdir();
		    }
		    final String zipFileName = FileUtil.getCurrentUserDirectory() + "/submit/" + p.getCustomerName().replace("\\s", "_") + ".zip";
		    ICFLogger.d(TAG, "zipFileName :" + zipFileName);
		    createFormDataFile(p.getBean());
		    FileUtil.doZip(new String[] { p.getFolderPath() }, p.getFolderPath(), zipFileName, new FilenameFilter()
				{
					@Override
					public boolean accept(File dir, String filename)
					{
						if (filename != null && filename.length() > 0 && filename.equalsIgnoreCase(AppConstants.JOB_DOCUMENT_UI))
						{
						return false;
						}
						return true;
					}
				});
			ICFHttpManager.getInstance().doUploadData(new HttpManagerListener() {
				@Override
				public void networkConnectivityError(Message msg) {
					p.setJobState(CustomerInfo.SUBMISSION_ERROR);
					Analytics.reportSubmissionFailure((String) msg.obj);
					RebateManager.getInstance().updateFormState(p.getBean(), CustomerInfo.SUBMISSION_ERROR);
					updateAdapter();
					final Activity activity = UiUtil.rootActivity.get();
					activity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							UiUtil.showError(activity, activity.getString(R.string.error_title), activity.getString(errorId));
							ICFLogger.d(TAG, "doSubmission Error :" + activity.getString(R.string.error_title) + activity.getString(errorId));
						}
					});
				}

				@Override
				public void dataReceived(Message msg) {
					try {
						ResponseBean bean = (ResponseBean) msg.obj;
						ICFLogger.d(TAG, "Response For Job Submission : " + bean.getStatusMessage());
						if ("S0000".equalsIgnoreCase(bean.getStatusCode())) {
							Analytics.reportSubmissionSuccess();
							p.setJobState(CustomerInfo.SUBMITTED);
							// There was a race condition here, fixed it with the PII deletion update
							// RebateManager.getInstance().updateFormState(p.getBean(), CustomerInfo.SUBMITTED);
							if (BuildConfig.DEBUG) {
								ICFLogger.d(TAG, "[DEBUG MODE] Copying zip to /tmp...");
								File tmp = new File(FileUtil.getCurrentUserDirectory() + File.separator + "tmp");
								boolean tmpDirExists = true;
								if (!tmp.exists()) {
									tmpDirExists = tmp.mkdir();
									ICFLogger.d(TAG, "[DEBUG MODE] Created directory " + tmp.getPath());
								}
								if (tmpDirExists) {
									FileUtil.copyFile(new File(zipFileName), new File(tmp.getPath() + File.separator + System.nanoTime() + "_rebate.zip"));
									ICFLogger.d(TAG, "[DEBUG MODE] Copy successful.");
								}
							}
							FileUtil.deleteFile(zipFileName);
							FileUtil.deleteEntireFolder(p.getFolderPath(), false);
							FileUtil.createSubmitSuccessJson(p.getFolderPath(),
									p.getBean().getCustomerInfo().getJobId(),
									p.getBean().getCustomerInfo().getCustomerAddress());  // Creates a skeleton json that only contains "customerDetails" object
							// We don't want to store any PII in this app after job is completed
							RootActivity activityCallback = UiUtil.rootActivity.get(); // Need to add to and refresh pending jobs adapter
							p.getBean().getCustomerInfo().setState(CustomerInfo.SUBMITTED);
							if (activityCallback != null) {
								// Need to give p the jobId so that we won't have 2 entries added to list view
								p.setId(p.getBean().getCustomerInfo().getJobId());
								activityCallback.addOrUpdatePendingJob(p);
							}
							updateAdapter();
						} else {
							Analytics.reportSubmissionFailure(bean.getStatusMessage());
							p.setJobState(CustomerInfo.SUBMISSION_ERROR);
							RebateManager.getInstance().updateFormState(p.getBean(), CustomerInfo.SUBMISSION_ERROR);
							updateAdapter();
						}
					} catch (Exception e) {
						ICFLogger.e(TAG, e);
						p.setJobState(CustomerInfo.SUBMISSION_ERROR);
						RebateManager.getInstance().updateFormState(p.getBean(), CustomerInfo.SUBMISSION_ERROR);
						updateAdapter();
					}
				}

				@Override
				public void connectionError(Message msg) {
					p.setJobState(CustomerInfo.SUBMISSION_ERROR);
					RebateManager.getInstance().updateFormState(p.getBean(), CustomerInfo.SUBMISSION_ERROR);
					updateAdapter();
				}
			}, p, zipFileName);
		}
		catch (IOException e)
		{
		    e.printStackTrace();
		}
	    }
	});
	updateAdapter();

	// ICFHttpManager.getInstance().doUploadData(null, ((RootActivity) getActivity()).progressListener, p, zipFileName);

    }

	public void checkAndrequestData(RequestCallFlow callFlow) {
		this.callFlowImpl = callFlow;

		if ((currentBean != null && currentBean.getCustomerInfo().getState() == CustomerInfo.NEW_JOB)) {
			if (defaultBean == null) {
				defaultBean = createCachedFormBean();
				if (defaultBean == null) {
					Activity activity = UiUtil.rootActivity.get();
					UiUtil.showError(activity, activity.getString(R.string.error_title), activity.getString(R.string.network_error));
				} else {
					updateUi(CustomerInfo.INCOMPLETED_JOB);
				}
			} else {
				updateUi(CustomerInfo.INCOMPLETED_JOB);
			}
		} else {
			callFlowImpl.updateData(currentBean);
		}
	}

    public ContractorDetails getContractorDetail()
    {
	if (contractorBean == null)
	{
	    contractorBean = new ContractorDetails();
	    if (LibUtils.getLoggedInUserBean() != null)
	    {
		contractorBean.setFirstName(LibUtils.getLoggedInUserBean().getFirstName());
		contractorBean.setLastName(LibUtils.getLoggedInUserBean().getLastName());
		// contractorBean.setAddress(LibUtils.getLoggedInUserBean().getAddress());
		contractorBean.setContractorCompany(LibUtils.getLoggedInUserBean().getContractorCompany());
		contractorBean.setAddress1(LibUtils.getLoggedInUserBean().getAddress1());
		contractorBean.setAddress2(LibUtils.getLoggedInUserBean().getAddress2());
		contractorBean.setCity(LibUtils.getLoggedInUserBean().getCity());
		contractorBean.setState(LibUtils.getLoggedInUserBean().getState());
		contractorBean.setEmail(LibUtils.getLoggedInUserBean().getEmailId());
		contractorBean.setCell(LibUtils.getLoggedInUserBean().getCell());
		contractorBean.setPhone(LibUtils.getLoggedInUserBean().getPhone());
		contractorBean.setZip(LibUtils.getLoggedInUserBean().getZip());
		contractorBean.setContractorAccountNumber(LibUtils.getLoggedInUserBean().getContractorAccountNumber());
		if (LibUtils.getSelectedUtilityCompany() != null)
		{
		    contractorBean.setCompany(LibUtils.getSelectedUtilityCompany().getName());
		}
	    }
	    // if (defaultBean == null)
	    // {
	    // defaultBean = createCachedFormBean();
	    // if (defaultBean == null)
	    // {
	    // Activity activity = UiUtil.rootActivity.get();
	    // UiUtil.showError(activity, activity.getString(R.string.error_title), activity.getString(R.string.network_error));
	    // }
	    // }
	    // contractorBean = defaultBean.getContractorDetail();
	}
	return contractorBean;
    }

    public CustomerScreenLayout getCustomerScreenLayout() {
	if (defaultBean == null) {
	    defaultBean = createCachedFormBean();
	    if (defaultBean == null) {
		final Activity activity = UiUtil.rootActivity.get();
		activity.runOnUiThread(new Runnable()
		{
		    @Override
		    public void run() {
			UiUtil.showError(activity, activity.getString(R.string.error_title), activity.getString(R.string.network_error));
		    }
		});
		return null;
	    }
	}
	return defaultBean.getClonedCustomerScreenLayout();
    }
    
    // TODO: why do we need this? why can't we use bean.getAppDetails
    public AppDetails getAppDetail()
    {
	if (defaultBean == null)
	{
	    defaultBean = createCachedFormBean();
	    if (defaultBean == null)
	    {
		final Activity activity = UiUtil.rootActivity.get();
		activity.runOnUiThread(new Runnable()
		{

		    @Override
		    public void run()
		    {
			UiUtil.showError(activity, activity.getString(R.string.error_title), activity.getString(R.string.network_error));
		    }
		});
		return null;
	    }
	    contractorBean = getContractorDetail();
	}
	return defaultBean.getClonedAppDetails();
    }

    public FormResponseBean getDefaultBean()
    {
	return defaultBean;
    }

    private void updateUi(int state)
    {
	FormResponseBean tempBean = createCachedFormBean();
	if (currentBean != null)
	{
	    tempBean.setCustomerInfo(currentBean.getCustomerInfo());
	    tempBean.setAppDetails(currentBean.getAppDetails());
	    tempBean.setContractorDetail(currentBean.getContractorDetail());
	    tempBean.setThermostatReferralChecked(currentBean.isThermostatReferralChecked());
	}

	currentBean = tempBean;
	RebateManager.getInstance().updateFormState(currentBean, state);
	callFlowImpl.updateData(tempBean);
    }

    @Override
    public void connectionError(Message msg)
    {

    }

    @Override
    public void networkConnectivityError(Message msg)
    {

    }

    @Override
    public void dataReceived(Message msg)
    {

    }

    public FormResponseBean getFormBean()
    {
	return currentBean;
    }

    public FormResponseBean createCachedFormBean()
    {
	FormResponseBean bean = null;
	String fileName = LibUtils.getSelectedUtilityCompany().getId() + "_form_data.json";
	byte[] data = null;
	try
	{
	    data = FileUtil.getResourceBundleFileContent(fileName);
	    if (data != null)
	    {
		bean = (FormResponseBean) DataParserImpl.getInstance().parse(ICFHttpManager.REQ_ID_FORM, data);
	    }
	}
	catch (Exception e)
	{
	    ICFLogger.e(TAG, "Error in createCachedFormBean", e);
	}
	return bean;
    }

    public void setCurrentBean(FormResponseBean bean)
    {
	currentBean = bean;
    }

    /**
     * 
     * @param suggestionListName
     * @return Manufacturer list for the selected utility company to be used in autosuggestion list For now ignoring the parameters since the same list is
     *         returned for all the cases.
     */
    public List<String> getAutoSuggestList(String suggestionListName)
    {
	if (suggestionListName == null)
	{
	    return null;
	}
	try
	{
	    if (autoSuggestList == null)
	    {
		String fileName = LibUtils.getSelectedUtilityCompany().getId() + "_manufacturers.json";
		String data = new String(FileUtil.getResourceBundleFileContent(fileName));
		autoSuggestList = new JSONObject(data).getJSONObject("autoSuggestion");
	    }
	    JSONArray array = autoSuggestList.getJSONArray(suggestionListName);
	    List<String> list = new ObjectMapper().readValue(array.toString(), new TypeReference<List<String>>()
	    {
	    });
	    return list;
	}
	catch (Exception e)
	{
	    ICFLogger.e(TAG, e);
	}
	return null;
    }

    public void utilityCompanyChanged()
    {
	autoSuggestList = null;
    }

    private void notifySubmissionFailure()
    {
	Activity activity = (Activity) UiUtil.rootActivity.get();
	if (activity != null)
	{
	    NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(activity);
	    mBuilder.setSmallIcon(R.drawable.app_icon);
	    mBuilder.setContentTitle(activity.getText(R.string.submission_fails_title));
	    mBuilder.setContentText(activity.getText(R.string.submission_fails));

	    Intent notificationIntent = new Intent(activity.getApplicationContext(), RootActivity.class);
	    notificationIntent.putExtra("notification_submission_failed", true);
	    PendingIntent pendingIntent = PendingIntent.getActivity(activity.getApplicationContext(), 100, notificationIntent, 0);
	    mBuilder.setContentIntent(pendingIntent);

	    int mNotificationId = 1000;
	    NotificationManager mNotifyMgr = (NotificationManager) activity.getSystemService(Context.NOTIFICATION_SERVICE);
	    mNotifyMgr.notify(mNotificationId, mBuilder.build());
	}
    }

    private void createFormDataFile(FormResponseBean bean)
    {
	if (bean == null)
	{
	    bean = currentBean;
	}
	if (bean != null)
	{
	    DataParserImpl.getInstance().writeFormDataFile(bean);
	}
    }

    public void updateFormState(FormResponseBean bean)
    {
	if (bean == null)
	{
	    bean = currentBean;
	}
	if (bean != null)
	{
	    DataParserImpl.getInstance().writeJson(bean, null);
	}
    }

    public void updateFormState(FormResponseBean bean, int state, Runnable runnable)
    {
	if (bean == null)
	{
	    bean = currentBean;
	}
	if (bean != null)
	{
	    if (bean.getCustomerInfo() != null)
	    {
		if ((state <= CustomerInfo.COMPLETED_JOB && state > bean.getCustomerInfo().getState()) || state >= CustomerInfo.COMPLETED_JOB)
		{
		    bean.getCustomerInfo().setState(state);
		    DataParserImpl.getInstance().writeJson(bean, runnable);
		}
	    }
	}
	if (state == CustomerInfo.SUBMISSION_ERROR)
	{
	    notifySubmissionFailure();
	}
    }

    public void updateFormState(FormResponseBean bean, int state)
    {
	if (bean == null)
	{
	    bean = currentBean;
	}
	if (bean != null)
	{
	    if (bean.getCustomerInfo() != null)
	    {
		if (state > bean.getCustomerInfo().getState())
		{
		    bean.getCustomerInfo().setState(state);
		    DataParserImpl.getInstance().writeJson(bean, null);
		}
	    }
	}
	if (state == CustomerInfo.SUBMISSION_ERROR)
	{
	    notifySubmissionFailure();
	}
    }

    public boolean checkAnyFormFilled(String serviceId)
    {
	if (currentBean.getCustomerInfo().getState() == CustomerInfo.NEW_JOB)
	{
	    return true;
	}
	Context context = LibUtils.getApplicationContext();
	if (serviceId.equalsIgnoreCase(context.getResources().getString(R.string.equipmentId)))
	{
	    List<Equipment> equipList = currentBean.getEquipments();
	    if (equipList != null)
	    {
		for (Equipment equipment : equipList)
		{
		    List<Item> itemList = equipment.getItems();
		    for (Item item : itemList)
		    {
			if (item.isItemSaved())
			{
			    if (item.getFormFilledState() == AppConstants.FORM_ITEM_FILLED_STATE.ITEM_PARTIALLY_SAVED.getState())
			    {
				return false;
			    }
			}
		    }
		}
	    }
	    return true;
	} else if (serviceId.equalsIgnoreCase(context.getResources().getString(R.string.insulationId))) {
	    List<Insulation> insulationList = currentBean.getInsulation();
	    if (insulationList != null)
	    {
		for (Insulation insulation : insulationList)
		{
		    List<InsulationItem> itemList = insulation.getItems();
		    if (itemList != null) {
		    for (InsulationItem item : itemList)
		    {
			if (item.isItemSaved())
			{
			    if (item.getFormFilledState() == AppConstants.FORM_ITEM_FILLED_STATE.ITEM_PARTIALLY_SAVED.getState())
			    {
				return false;
			    }
			}
		    }
		    }
		}
	    }
	}
	else if (serviceId.equalsIgnoreCase(context.getResources().getString(R.string.windowDoorId))) {
	    List<WindowAndDoor> windowAndDoorList = currentBean.getWindowAndDoor();
	    if (windowAndDoorList != null)
	    {
		for (WindowAndDoor wd : windowAndDoorList)
		{
		    List<WDItem> itemList = wd.getItems();
		    if (itemList != null) {
		    for (WDItem item : itemList)
		    {
			if (item.isItemSaved())
			{
			    if (item.getFormFilledState() == AppConstants.FORM_ITEM_FILLED_STATE.ITEM_PARTIALLY_SAVED.getState())
			    {
				return false;
			    }
			}
		    }
		    }
		}
	    }
	}
	else if (serviceId.equalsIgnoreCase(context.getResources().getString(R.string.qualityInstallVerificationId)))
	{
	    List<QIVItem> qivList = currentBean.getQivList();
	    if (qivList != null)
	    {
		for (QIVItem qiv : qivList)
		{
		    if (!AppConstants.QUALITY_INSTALL_VERIFICATION.equalsIgnoreCase(qiv.getName()))
		    {
			// Added 11/20/2015 remove red error mark if qiv not required
			if (!qiv.isQivRequired())
			    return true;
			if (qiv.isItemSaved())
			{
			    if (qiv.getFormFilledState() == AppConstants.FORM_ITEM_FILLED_STATE.ITEM_PARTIALLY_SAVED.getState() || qiv.getFormFilledState() == AppConstants.FORM_ITEM_FILLED_STATE.ITEM_UNEDITED.getState())
			    {
				return false;
			    }
			}
			else
			{
			    return false;
			}
		    }
		}
	    }
	    return true;
	}
	else if (serviceId.equalsIgnoreCase(context.getResources().getString(R.string.tuneUpId)))
	{
	    List<TuneUp> tuneUpList = currentBean.getTuneUp();
	    if (tuneUpList != null)
	    {
		for (TuneUp tuneUp : tuneUpList)
		{
		    List<TuneUpItem> itemList = tuneUp.getItems();
		    if (itemList != null) {
		    for (TuneUpItem item : itemList)
		    {
			if (item.isItemSaved())
			{
			    if (item.getFormFilledState() == AppConstants.FORM_ITEM_FILLED_STATE.ITEM_PARTIALLY_SAVED.getState())
			    {
				return false;
			    }
			}
		    }
		}
		}
	    }
	    /*List<TuneUp> tuneUpList = currentBean.getTuneUp();
	    if (tuneUpList != null)
	    {
		for (TuneUp tuneup : tuneUpList)
		{
		    if (tuneup.isItemSaved())
		    {
			if (tuneup.getFormFilledState() == AppConstants.FORM_ITEM_FILLED_STATE.ITEM_PARTIALLY_SAVED.getState())
			{
			    return false;
			}
		    }
		}
	    }
	    return true;*/
	    
	}
	else if (serviceId.equalsIgnoreCase(context.getResources().getString(R.string.ductsealingId)))
	{
		List<DuctSealing> ductSealingList = currentBean.getDuctSealing();
		if (ductSealingList != null) {
			for (DuctSealing ductSealing : ductSealingList) {
				List<DuctSealingItem> itemList = (List<DuctSealingItem>)ductSealing.getItems();
				if (itemList != null) {
					for (DuctSealingItem item : itemList) {
						if (item.isItemSaved()) {
							if (item.getFormFilledState() ==
									AppConstants.FORM_ITEM_FILLED_STATE.ITEM_PARTIALLY_SAVED.getState()) {
								return false;
							}
						}
					}
				}
				return true;
			}
		}
	}
	else if (serviceId.equalsIgnoreCase(context.getResources().getString(R.string.airsealingId))) {
		List<AirSealing> airSealingList = currentBean.getAirSealing();
		if (airSealingList != null) {
			for (AirSealing airSealing : airSealingList) {
				List<AirSealingItem> itemList = (List<AirSealingItem>)airSealing.getItems();
				if (itemList != null) {
					for (AirSealingItem item : itemList) {
						if (item.isItemSaved()) {
							if (item.getFormFilledState() ==
									AppConstants.FORM_ITEM_FILLED_STATE.ITEM_PARTIALLY_SAVED.getState()) {
								return false;
							}
						}
					}
				}
				return true;
			}
		}
	}
	else if (serviceId.equalsIgnoreCase(context.getResources().getString(R.string.thermostatInstallId)))
	{
	    List<ThermostatInstallItem> thermoStatItemList = currentBean.getThermostatInstall();
	    if (thermoStatItemList != null)
	    {
		for (ThermostatInstallItem thermoStatItem : thermoStatItemList)
		{
		    if (thermoStatItem.isItemSaved())
		    {
			if (thermoStatItem.getFormFilledState() == AppConstants.FORM_ITEM_FILLED_STATE.ITEM_PARTIALLY_SAVED.getState())
			{
			    return false;
			}
		    }
		}
	    }
	    return true;
	}
	return true;
    }

	public String getStaticRebateValue (String serviceId) {
		Context context = LibUtils.getApplicationContext();
		if (serviceId.equalsIgnoreCase(context.getResources().getString(R.string.equipmentId)))
		{
			List<Equipment> equipList = currentBean.getEquipments();
			if (equipList != null)
			{
				for (Equipment equipment : equipList)
				{
					List<Item> itemList = equipment.getItems();
					for (Item item : itemList)
					{
                        String srv = item.getStaticRebateValue();
                        if (!TextUtils.isEmpty(srv)) return srv;
                    }
				}
			}
		} else if (serviceId.equalsIgnoreCase(context.getResources().getString(R.string.insulationId))) {
			List<Insulation> insulationList = currentBean.getInsulation();
			if (insulationList != null)
			{
				for (Insulation insulation : insulationList)
				{
					List<InsulationItem> itemList = insulation.getItems();
					if (itemList != null) {
						for (InsulationItem item : itemList)
						{
                            String srv = item.getStaticRebateValue();
                            if (!TextUtils.isEmpty(srv)) return srv;
						}
					}
				}
			}
		}
		else if (serviceId.equalsIgnoreCase(context.getResources().getString(R.string.windowDoorId))) {
			List<WindowAndDoor> windowAndDoorList = currentBean.getWindowAndDoor();
			if (windowAndDoorList != null)
			{
				for (WindowAndDoor wd : windowAndDoorList)
				{
					List<WDItem> itemList = wd.getItems();
					if (itemList != null) {
						for (WDItem item : itemList)
						{
                            String srv = item.getStaticRebateValue();
                            if (!TextUtils.isEmpty(srv)) return srv;
                        }
					}
				}
			}
		}
		else if (serviceId.equalsIgnoreCase(context.getResources().getString(R.string.qualityInstallVerificationId)))
		{
			List<QIVItem> qivList = currentBean.getQivList();
			if (qivList != null)
			{
				for (QIVItem qiv : qivList)
				{
					if (!AppConstants.QUALITY_INSTALL_VERIFICATION.equalsIgnoreCase(qiv.getName()))
					{
                        String srv = qiv.getStaticRebateValue();
                        if (!TextUtils.isEmpty(srv)) return srv;
                    }
				}
			}
		}
		else if (serviceId.equalsIgnoreCase(context.getResources().getString(R.string.tuneUpId)))
		{
			List<TuneUp> tuneUpList = currentBean.getTuneUp();
			if (tuneUpList != null)
			{
				for (TuneUp tuneUp : tuneUpList)
				{
					List<TuneUpItem> itemList = tuneUp.getItems();
					if (itemList != null) {
						for (TuneUpItem item : itemList)
						{
                            String srv = item.getStaticRebateValue();
                            if (!TextUtils.isEmpty(srv)) return srv;
						}
					}
				}
			}
		}
		else if (serviceId.equalsIgnoreCase(context.getResources().getString(R.string.ductsealingId)))
		{
			List<DuctSealing> ductSealingList = currentBean.getDuctSealing();
			if (ductSealingList != null) {
				for (DuctSealing ductSealing : ductSealingList) {
					List<DuctSealingItem> itemList = (List<DuctSealingItem>)ductSealing.getItems();
					if (itemList != null) {
						for (DuctSealingItem item : itemList) {
                            String srv = item.getStaticRebateValue();
                            if (!TextUtils.isEmpty(srv)) return srv;
                        }
					}
				}
			}
		}
		else if (serviceId.equalsIgnoreCase(context.getResources().getString(R.string.airsealingId))) {
			List<AirSealing> airSealingList = currentBean.getAirSealing();
			if (airSealingList != null) {
				for (AirSealing airSealing : airSealingList) {
					List<AirSealingItem> itemList = (List<AirSealingItem>)airSealing.getItems();
					if (itemList != null) {
						for (AirSealingItem item : itemList) {
                            String srv = item.getStaticRebateValue();
                            if (!TextUtils.isEmpty(srv)) return srv;
                        }
					}
				}
			}
		}
		else if (serviceId.equalsIgnoreCase(context.getResources().getString(R.string.thermostatInstallId)))
		{
			List<ThermostatInstallItem> thermoStatItemList = currentBean.getThermostatInstall();
			if (thermoStatItemList != null)
			{
				for (ThermostatInstallItem thermoStatItem : thermoStatItemList)
				{
					String srv = thermoStatItem.getStaticRebateValue();
					if (!TextUtils.isEmpty(srv)) return srv;
				}
			}
		}
		return "";
	}

    public void resetJob()
    {
	contractorBean = null;
    }

    public void clearData()
    {
	currentBean = null;
	defaultBean = null;
	contractorBean = null;
	_instance = null;
    }
}
