package com.icf.rebate.ui.util;

import android.util.Log;


//TODO: update proguard file to remove the log method call for various level. For eg the release version shouldn't have
//Log.d, Log.v etc
public class ICFLogger
{
    public static final String TAG = ICFLogger.class.getName();
    
    public static void d(String Tag, String msg)
    {
	Log.d(Tag, msg);
    }

    public static void e(String tag, Throwable tr)
    {
	Log.e(tag, tr.getMessage(), tr);
    }

    public static void e(String tag, String msg, Throwable tr)
    {
	Log.e(tag, msg, tr);
    }
    
    
//
//    public static void w(String tag, String msg, Exception e)
//    {
//	Log.w(tag, msg, e);
//    }
//
//    public static void w(String tag, Exception e)
//    {
//	String msg = new String("");
//	String temp = new String("");
//	
//	StackTraceElement[] elements = e.getStackTrace();
//	Class<? extends Exception> throoow = e.getClass();
//	temp = throoow.toString();
//	if (!temp.equals(null))
//	    msg += temp + " :  \t";
//	temp = e.getLocalizedMessage();
//	if (temp != null && !temp.equals(null))
//	    msg += temp + "\n";
//	if (elements != null)
//	{
//	    for (StackTraceElement element : elements)
//	    {
//
//		temp = element.getClassName();
//		if (!temp.equals(null))
//		    msg += "\t\t\t at " + temp + "::";
//		temp = element.getMethodName();
//		if (!temp.equals(null))
//		    msg += temp + "(";
//		temp = element.getFileName();
//		if (!temp.equals(null))
//		    msg += temp + ":";
//		temp = Integer.toString(element.getLineNumber());
//		if (!temp.equals(null))
//		    msg += temp + ")\n";
//	    }
//	}
//	Log.w(tag, msg);
//    }
//
//    public static void w(String tag, String msg)
//    {
//	Log.w(tag, msg);
//    }
//
//    public static void v(String tag, String msg, Throwable tr)
//    {
//	Log.v(tag, msg, tr);
//    }

//    public static void v(String tag, String msg)
//    {
//	Log.v(tag, msg);
//    }
//
//    public static void i(String tag, String msg, Throwable tr)
//    {
//	Log.i(tag, msg, tr);
//    }
//
//    public static void i(String tag, String msg)
//    {
//	Log.i(tag, msg);
//    }
//
//    public static void e(String tag, String msg)
//    {
//	Log.e(tag, msg);
//    }
  //TODO: why do we need this? doesn't Log.e does the same?
//    public static void e(String Tag, Exception e)
//    {
//	String msg = new String("");
//	String temp = new String("");
//	
//	StackTraceElement[] elements = e.getStackTrace();
//	Class<? extends Exception> throoow = e.getClass();
//	temp = throoow.toString();
//	if (!temp.equals(null))
//	    msg += temp + " :  \t";
//	temp = e.getLocalizedMessage();
//	if (temp != null && !temp.equals(null))
//	    msg += temp + "\n";
//	if (elements != null)
//	{
//	    for (StackTraceElement element : elements)
//	    {
//
//		temp = element.getClassName();
//		if (!temp.equals(null))
//		    msg += "\t\t\t at " + temp + "::";
//		temp = element.getMethodName();
//		if (!temp.equals(null))
//		    msg += temp + "(";
//		temp = element.getFileName();
//		if (!temp.equals(null))
//		    msg += temp + ":";
//		temp = Integer.toString(element.getLineNumber());
//		if (!temp.equals(null))
//		    msg += temp + ")\n";
//	    }
//	}
//	Log.e(Tag, msg);
//    }
}
