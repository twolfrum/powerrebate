package com.icf.rebate.ui.controller;

import com.icf.rebate.networklayer.model.FormResponseBean.Options;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

public abstract class BarcodeTextWatcher extends CustomTextWatcher
{
    private String prevValue;

    public BarcodeTextWatcher(EditText editText, Options options, Context context)
    {
	super(editText, options, context);
    }
    
    @Override
    public void onFocusChange(View v, boolean hasFocus)
    {
	super.onFocusChange(v, hasFocus);
	if (hasFocus)
	{
	    prevValue = editText.getText().toString();
	}
	else 
	{
	    String newValue = editText.getText().toString();
	    if (TextUtils.isEmpty(prevValue)) prevValue = "";
	    if (TextUtils.isEmpty(newValue)) newValue = "";
	    if (!prevValue.equals(newValue)) {
		options.setBarcodeScanned(false);
	    }
	}
    }
}
