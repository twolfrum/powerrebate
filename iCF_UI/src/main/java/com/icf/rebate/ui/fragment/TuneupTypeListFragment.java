package com.icf.rebate.ui.fragment;

import java.util.ArrayList;
import java.util.List;

import com.crashlytics.android.Crashlytics;
import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.adapter.TuneUpTypeAdapter;
import com.icf.rebate.networklayer.model.FormResponseBean;
import com.icf.rebate.networklayer.model.FormResponseBean.TuneUp;
import com.icf.rebate.ui.RootActivity;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.listeners.FragmentCallFlow;
import com.icf.rebate.ui.listeners.RequestCallFlow;
import com.icf.rebate.ui.util.AppConstants;

import android.support.v4.app.Fragment;
import android.util.Log;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import io.fabric.sdk.android.Fabric;

public class TuneupTypeListFragment extends Fragment implements OnItemClickListener, RequestCallFlow, FragmentCallFlow
{

    public static String TAG = TuneupTypeListFragment.class.getSimpleName();

    private View rootView;
    private ListView mListView;
    private TuneUpTypeAdapter mLVAdapter;
    private String mTitle;
    List<TuneUp> mTuneupTypeList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
	rootView = inflater.inflate(R.layout.fragment_tuneup_type_list, container, false);
	mListView = (ListView) rootView.findViewById(R.id.lv_tuneup_types);
	// Set title
	RootActivity rootActivity = null;
	try
	{
	    rootActivity = (RootActivity) getActivity();
	}
	catch (ClassCastException e)
	{
	    throw new ClassCastException("This fragment must be attached to RootActivity");
	}

	rootActivity.setTitle(mTitle);

	bindViewListeners();
	requestData();
	return rootView;
    }

    private void bindViewListeners()
    {
	mListView.setOnItemClickListener(this);
    }

    private void updateTuneupList(FormResponseBean bean)
    {
	if (bean == null)
	{
	    return;
	}

	mTuneupTypeList = bean.getTuneUp();

	if (mTuneupTypeList == null)
	{
	    mTuneupTypeList = new ArrayList<>();
	}

	mLVAdapter = new TuneUpTypeAdapter(getActivity(), 0, mTuneupTypeList);
	mLVAdapter.setAdapterListener(mLVAdapter);
	mListView.setAdapter(mLVAdapter);
    }

    /*--------------------------------------------Field access----------------------------------------------------*/

    public String getTitle()
    {
	return mTitle;
    }

    public void setTitle(String title)
    {
	this.mTitle = title;
    }

    /*---------------------------------------Implemented interface methods-------------------------------------*/

    @Override
    public void requestData()
    {
	RebateManager.getInstance().checkAndrequestData(this);
    }

    @Override
    public void getUiControls(View root)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void initialize(View root)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void bindCallbacks(View root)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void showProgressDialog()
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void stopProgressDialog()
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void updateData(Object obj)
    {
	updateTuneupList((FormResponseBean) obj);
    }

    @Override
    public void displayError(String title, String message)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
	// Add crashylitcs for equipment type
	if (mTuneupTypeList != null && mTuneupTypeList.size() > position && Fabric.isInitialized())
	{
	    String str = "";
	    Crashlytics.setString("selectedEquipmentName", str = (mTuneupTypeList.get(position) == null ? "null" : mTuneupTypeList.get(position).getName()));
	    Log.d("TuneUp", "Setting crashlytics custom tag selectedEquipmentName=" + str);
	}

	TuneupPagerFragment fragment = new TuneupPagerFragment();
	fragment.setTitle(mTuneupTypeList.get(position).getName());
	Bundle args = new Bundle();
	args.putInt(AppConstants.SUBTYPE_SELECT_POSITION_KEY, position);
	fragment.setArguments(args);
	((RootActivity) getActivity()).beginTransaction().replace(R.id.mainLayout, fragment).addToBackStack(null).commit();
    }

    /*------------------------------------------------------------------------------------------------------------*/
}
