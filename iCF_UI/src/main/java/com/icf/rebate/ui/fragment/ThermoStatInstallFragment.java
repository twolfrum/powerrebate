package com.icf.rebate.ui.fragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.icf.rebate.adapter.CustomSpinnerAdapter;
import com.icf.rebate.networklayer.model.CustomerInfo;
import com.icf.rebate.networklayer.model.DuctItem;
import com.icf.rebate.networklayer.model.FormResponseBean.Options;
import com.icf.rebate.networklayer.model.FormResponseBean.Part;
import com.icf.rebate.networklayer.model.ThermostatInstallItem;
import com.icf.rebate.networklayer.model.UIElement;
import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.ui.controller.FormUIManager;
import com.icf.rebate.ui.controller.FormUIManager.FormViewCreationCallback;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.listeners.RequestCallFlow;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.ICFLogger;
import com.icf.rebate.ui.util.UiUtil;
import com.icf.rebate.ui.util.UiViewUtils;
import com.icf.rebate.ui.util.rebate.ThermostatRebateCalculation;

public class ThermoStatInstallFragment extends Fragment implements RequestCallFlow, FormViewCreationCallback
{
    // private UIElementManager serverLayoutInflater;

    public static final String TAG = ThermoStatInstallPagerFragment.class.getSimpleName();

    private FormUIManager uiManager;

    private LinearLayout scrollViewLayout;

    private ThermostatInstallItem thermoStatItem;

    private ThermostatRebateCalculation thermostatRebateCalculation;

    private EditText modelNum;

	private TextView result;

    private AutoCompleteTextView manufacturerName;

    private String upgradeMeasureName, oldStat;

    private List<String> manufacturer_top_ten = new ArrayList<String>();

    private List<String> manufacturer_wifi = new ArrayList<String>();

    private Spinner measureName, oldStatSpinner;

    private boolean isDeleted = false;

    private String title;

    public ThermoStatInstallFragment()
    {
	super();
    }

    public static ThermoStatInstallFragment newInstance(ThermostatInstallItem thermoStatItem)
    {
	ThermoStatInstallFragment frag = new ThermoStatInstallFragment();
	frag.thermoStatItem = thermoStatItem;
	return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
	View view = inflater.inflate(R.layout.thermo_stat_install_lyt, null, false);
	return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
	super.onViewCreated(view, savedInstanceState);
	getUiControls(view);
	initalize();
	createViews();

	CustomerInfo cInfo = RebateManager.getInstance().getFormBean().getCustomerInfo();
	if (UiUtil.isJobCompleted(cInfo))
	{
	    UiUtil.setEnableView((ViewGroup) view, false, null);
	}
    }

    private void getUiControls(View view)
    {
	scrollViewLayout = (LinearLayout) view.findViewById(R.id.thermostat_scrol_layout);

    }

    protected void initalize()
    {
	uiManager = new FormUIManager(getString(R.string.thermostatInstallId));
	uiManager.setFormViewCreationCallback(this);
	uiManager.setEquipmentName(getActivity().getResources().getString(R.string.thermostatInstallId));
	thermostatRebateCalculation = new ThermostatRebateCalculation();
    }

    private void createViews()
    {
	List<Part> partList = null;
	if (thermoStatItem != null && thermoStatItem.getPartList() != null)
	{
	    partList = thermoStatItem.getPartList();
	}

	if (partList != null)
	{
	    scrollViewLayout.removeAllViews();
	    boolean isFormFilled = thermoStatItem.getFormFilledState() != AppConstants.FORM_ITEM_FILLED_STATE.ITEM_UNEDITED.getState();
	    uiManager.createViews(getParentFragment(), scrollViewLayout, partList, isFormFilled);
	}
	if (thermoStatItem.getManufacturer_top10() != null)
	{
	    manufacturer_top_ten = thermoStatItem.getManufacturer_top10();
	}
	if (thermoStatItem.getManufacturer_wifi() != null)
	{
	    manufacturer_wifi = thermoStatItem.getManufacturer_wifi();
	}
    }

    @Override
    public void showProgressDialog()
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void stopProgressDialog()
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void updateData(Object obj)
    {

    }

    @Override
    public void displayError(String title, String message)
    {

    }

    public ThermostatInstallItem getstatsItemBean()
    {
	return thermoStatItem;
    }

    public void setThermostatInstallItemBean(ThermostatInstallItem item)
    {
	this.thermoStatItem = item;
    }

    @Override
    public void viewCreated(final View view, Part part, final Options option)
    {
	if (measureName == null || manufacturerName == null || oldStatSpinner == null || modelNum == null || result == null)
	{
	    if (part != null)
	    {
		String id = part.getId();
		if (id != null && id.equalsIgnoreCase(AppConstants.THERMOSTAT_INSTALL_UPGRADE_MEASURE_NAME_ID))
		{
		    if (view instanceof Spinner)
		    {
			measureName = (Spinner) view;
			measureName.setOnItemSelectedListener(new OnItemSelectedListener()
			{

			    @Override
			    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
			    {
				if (parent != null && parent.getRootView() != null)
				{
				    parent.getRootView().clearFocus();
				}
				if (measureName.getSelectedItem() != null)
				{
				    upgradeMeasureName = measureName.getSelectedItem().toString();

				    if (!upgradeMeasureName.equalsIgnoreCase("Select Value"))
				    {
					option.setSavedValue(upgradeMeasureName);
					//if (oldStat != null && oldStat.length() > 0 && !oldStat.equalsIgnoreCase("Select Value"))
					//{
					    updateThermoStatResult();
					//}
					updateAutoSuggestList(upgradeMeasureName);
				    }
				    else
				    {
					option.setSavedValue(null);
				    }

				    if (UiViewUtils.isValidationMode(option.isItemSaved()) && option.isMandatory() && !option.isFieldFilled())
				    {
					((View) view.getParent()).setBackground(getActivity().getResources().getDrawable(R.drawable.bg_dialog_mandatory_box));
				    }
				    else
				    {
					((View) view.getParent()).setBackground(getActivity().getResources().getDrawable(R.drawable.combo_box_bg));
				    }
				}
			    }

			    @Override
			    public void onNothingSelected(AdapterView<?> parent)
			    {

			    }
			});
		    }
		}
		else if (id != null && id.equalsIgnoreCase(AppConstants.THERMOSTAT_INSTALL_MANUFACTURER_NAME_ID))
		{
		    if (view instanceof AutoCompleteTextView)
		    {
			manufacturerName = (AutoCompleteTextView) view;
			String selectedValue = manufacturerName.getText().toString();
			if (selectedValue != null && option != null && !selectedValue.equalsIgnoreCase(option.getSavedValue()))
			{
			    option.setSavedValue(selectedValue);
			}
			manufacturerName.setOnFocusChangeListener(new OnFocusChangeListener()
			{

			    @Override
			    public void onFocusChange(View v, boolean hasFocus)
			    {
				if (manufacturerName != null)
				{
				    String selectedValue = manufacturerName.getText().toString();
				    if (selectedValue != null)
				    {
					option.setSavedValue(selectedValue);
				    }
				}
				if (!hasFocus)
				{
				    if (option != null && option.isMandatory() && !option.isFieldFilled() && UiViewUtils.isValidationMode(option.isItemSaved()))
				    {
					v.setBackground(v.getContext().getResources().getDrawable(R.drawable.bg_dialog_mandatory_box));
				    }
				    else
				    {
					v.setBackground(v.getContext().getResources().getDrawable(R.drawable.bg_dialog_box));
				    }
				}
			    }
			});
		    }
		} else if (id != null && id.equalsIgnoreCase(AppConstants.THERMOSTAT_INSTALL_OLD_T_STAT_TYPE_ID)) {
			if (view instanceof Spinner) {
				oldStatSpinner = (Spinner) view;
				oldStatSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
					@Override
					public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
						if (parent != null && parent.getRootView() != null) {
							parent.getRootView().clearFocus();
						}
						if (oldStatSpinner.getSelectedItem() != null) {
							oldStat = oldStatSpinner.getSelectedItem().toString();
							if (!oldStat.equalsIgnoreCase("Select Value")) {
								option.setSavedValue(oldStat);
								if (upgradeMeasureName != null && upgradeMeasureName.length() > 0 && !upgradeMeasureName.equalsIgnoreCase("Select Value")) {
									updateThermoStatResult();
								}
							} else {
								option.setSavedValue(null);
							}

							if (UiViewUtils.isValidationMode(option.isItemSaved()) && option.isMandatory() && !option.isFieldFilled()) {
								((View) view.getParent()).setBackground(getActivity().getResources().getDrawable(R.drawable.bg_dialog_mandatory_box));
							} else {
								((View) view.getParent()).setBackground(getActivity().getResources().getDrawable(R.drawable.combo_box_bg));
							}
						}
					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {

					}
				});
			}
		}
		else if (id != null && id.equalsIgnoreCase(AppConstants.THERMOSTAT_INSTALL_MODEL_NUMBER_ID))
		{
		    if (view instanceof EditText)
		    {
			modelNum = (EditText) view;
			// modelNum.setInputType(InputType.TYPE_CLASS_NUMBER);
			final String modelNumber = modelNum.getText().toString();
			if (modelNumber != null && option != null && !modelNumber.equalsIgnoreCase(option.getSavedValue()))
			{
			    option.setSavedValue(modelNumber);
			}

		    }
		}
		else if (id != null && id.equalsIgnoreCase(AppConstants.THERMOSTAT_INSTALL_RESULT_ID))
		{
            //$$$TBD SHIT
			result = (TextView) view;
            try {
                thermoStatItem.setStaticRebateValue(
                        ((Map<String, String>) result.getTag()).get("staticDisplayedValue"));
            } catch (Exception e) {
            }
        }
	    }
	}
    }

    public void updateThermoStatResult()
    {
        //rebate value may be TBD
        String rebateValue = thermoStatItem.getStaticRebateValue();
        if (result != null) {
            if (TextUtils.isEmpty(rebateValue)) {
                float value = thermostatRebateCalculation.getRebateValue(upgradeMeasureName,
						true, thermoStatItem.getOldTStatValue(), 1);
                thermoStatItem.setRebateValue(value);
                rebateValue = (String.format("%.02f", value));
            }

            result.setText("" + rebateValue);
            result.setBackground(getActivity().getResources().getDrawable(R.drawable.bg_dialog_box));
            ICFLogger.d(TAG, "Result:" + "$" + rebateValue);
        }
    }

    private void updateAutoSuggestList(String upgradeMeasureName)
    {
	List<String> autoSuggList;
	if (manufacturerName != null)
	{
	    if (upgradeMeasureName.equalsIgnoreCase("Programmable"))
	    {
		autoSuggList = manufacturer_top_ten;
	    }
	    else
	    {
		autoSuggList = manufacturer_wifi;
	    }

	    CustomSpinnerAdapter equipmentAdapter = new CustomSpinnerAdapter(getActivity(), 0, autoSuggList);
	    manufacturerName.setAdapter(equipmentAdapter);
	}
    }

    public void updateCaptureImage(int pageIndex)
    {
	UiUtil.updateCaptureImage(uiManager, pageIndex + "", getActivity());
    }

    @Override
    public void onDestroyView()
    {
	Log.v(TAG, "onDestroyView ");
	UiUtil.cleanBitmap(getView());
	saveForm();

	resetFormViews();
	super.onDestroyView();
    }
    
    
    public void saveForm()
    {
	List<ThermostatInstallItem> items = ((ThermoStatInstallPagerFragment) getParentFragment()).getStatsList();

	if (items != null)
	{
	    int index = items.indexOf(thermoStatItem);
	    if (index > -1)
	    {
		Log.v(TAG, "saveForm() " + index);
		if (!isDeleted)
		{
		    Log.v(TAG, "saveForm() !isDeleted " + index);
		    if (thermoStatItem.isItemSaved() && result != null)
		    {
			String formatedValue = (String.format("%.02f", thermoStatItem.getRebateValue()));
			result.setText(formatedValue);
		    }
		    ((ThermoStatInstallPagerFragment) getParentFragment()).saveForm(index, false);
		}
	    }
	}
    }

    public void setFragmentDetroyed(int index)
    {
	Log.v(TAG, "setFragmentDetroyed " + index);
	isDeleted = true;
    }

    private void resetFormViews()
    {
	measureName = null;
	manufacturerName = null;
	oldStatSpinner = null;
	modelNum = null;
	result = null;
    }

    public void setTitleId(String title)
    {
	this.title = title;
    }

    public String getTitleId()
    {
	return this.title;
    }

}
