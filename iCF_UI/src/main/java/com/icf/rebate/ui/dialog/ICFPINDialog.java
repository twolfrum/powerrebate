package com.icf.rebate.ui.dialog;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.rebate.ui.LoginActivity;
import com.icf.rebate.ui.PendingJobsFragment;
import com.icf.ameren.rebate.ui.R;

/*
 * Class to handle all PIN related feature such as 
 * 1. Setting new PIN
 * 2. Change PIN
 * 3. Validate PIN
 */

public class ICFPINDialog extends DialogFragment implements OnClickListener, OnKeyListener
{

    private int layout;

    private EditText pin_1, pin_2, pin_3, pin_4;

    private boolean validatePinAfterIdle;

    private boolean validatePinOfflineUse;

    private String offlineUserName;

    private Handler mHandler = new Handler();

    private Runnable runnable;

    public static DialogFragment validatePINAfterIdleTime(FragmentActivity activity)
    {
	ICFPINDialog dialogFragment = new ICFPINDialog();
	dialogFragment.setCancelable(false);
	dialogFragment.setValidatePINAfterIdle(true);
	dialogFragment.layout = R.layout.validate_pin_popup;
	FragmentManager fm = activity.getSupportFragmentManager();
	dialogFragment.show(fm, "fragment_edit_name");
	return dialogFragment;
    }

    public static DialogFragment validatePINOfflineUse(String userName, FragmentActivity activity, Runnable runnable)
    {
	ICFPINDialog dialogFragment = new ICFPINDialog();
	dialogFragment.setCancelable(false);
	dialogFragment.setOfflineUserData(userName, runnable);
	dialogFragment.layout = R.layout.validate_pin_popup;
	FragmentManager fm = activity.getSupportFragmentManager();
	dialogFragment.show(fm, "fragment_edit_name");
	return dialogFragment;
    }

    private void setOfflineUserData(String userName, Runnable runnable)
    {
	validatePinOfflineUse = true;
	offlineUserName = userName;
	this.runnable = runnable;
    }

    private void setValidatePINAfterIdle(boolean b)
    {
	this.validatePinAfterIdle = b;
    }

    public static DialogFragment validatePINForPendingJobs(final FragmentActivity activity)
    {
	ICFPINDialog dialogFragment = new ICFPINDialog();
	dialogFragment.setCancelable(true);
	dialogFragment.layout = R.layout.validate_pin_popup;
	FragmentManager fm = activity.getSupportFragmentManager();
	dialogFragment.show(fm, "fragment_edit_name");
	return dialogFragment;
    }

    @Override
    public void onCancel(DialogInterface dialog)
    {
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
	super.onCreate(savedInstanceState);
	setStyle(DialogFragment.STYLE_NO_TITLE, R.style.MyTheme_Dialog);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
	View view = inflater.inflate(layout, container);
	handlePinChange(view);
	return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
	super.onViewCreated(view, savedInstanceState);
	getDialog().setCanceledOnTouchOutside(false);
    }

    private void handlePinChange(final View view)
    {
	pin_1 = (EditText) view.findViewById(R.id.pin_1);
	pin_2 = (EditText) view.findViewById(R.id.pin_2);
	pin_3 = (EditText) view.findViewById(R.id.pin_3);
	pin_4 = (EditText) view.findViewById(R.id.pin_4);
	pin_1.setOnKeyListener(this);
	pin_2.setOnKeyListener(this);
	pin_3.setOnKeyListener(this);
	pin_4.setOnKeyListener(this);

	ImageButton pin_done_btn = (ImageButton) view.findViewById(R.id.change_pin_done_image);
	ImageButton pin_cancel_btn = (ImageButton) view.findViewById(R.id.change_pin_cancel_image);

	if (pin_cancel_btn != null)
	{
	    pin_cancel_btn.setOnClickListener(this);
	}
	if (pin_done_btn != null)
	{
	    pin_done_btn.setOnClickListener(this);
	}
	pin_1.addTextChangedListener(new TextWatcher()
	{
	    @Override
	    public void onTextChanged(CharSequence s, int start, int before, int count)
	    {
		if (pin_1.getText().toString().length() == 1)
		{
		    pin_2.post(new Runnable()
		    {
			public void run()
			{
			    pin_2.requestFocus();
			}
		    });
		}
	    }

	    @Override
	    public void beforeTextChanged(CharSequence s, int start, int count, int after)
	    {
	    }

	    @Override
	    public void afterTextChanged(Editable s)
	    {
	    }
	});
	pin_2.addTextChangedListener(new TextWatcher()
	{

	    @Override
	    public void onTextChanged(CharSequence s, int start, int before, int count)
	    {
		pin_3.post(new Runnable()
		{
		    public void run()
		    {
			pin_3.requestFocus();
		    }
		});

	    }

	    @Override
	    public void beforeTextChanged(CharSequence s, int start, int count, int after)
	    {
	    }

	    @Override
	    public void afterTextChanged(Editable s)
	    {
		if (pin_2.getText().toString().length() == 0)
		{
		    pin_1.post(new Runnable()
		    {
			public void run()
			{
			    pin_1.requestFocus();
			}
		    });
		}
	    }
	});
	pin_3.addTextChangedListener(new TextWatcher()
	{

	    @Override
	    public void onTextChanged(CharSequence s, int start, int before, int count)
	    {
		pin_4.post(new Runnable()
		{
		    public void run()
		    {
			pin_4.requestFocus();
		    }
		});
	    }

	    @Override
	    public void beforeTextChanged(CharSequence s, int start, int count, int after)
	    {
	    }

	    @Override
	    public void afterTextChanged(Editable s)
	    {
		if (pin_3.getText().toString().length() == 0)
		{
		    pin_2.post(new Runnable()
		    {
			public void run()
			{
			    pin_2.requestFocus();
			}
		    });
		}
	    }
	});
	pin_4.addTextChangedListener(new TextWatcher()
	{

	    @Override
	    public void onTextChanged(CharSequence s, int start, int before, int count)
	    {

	    }

	    @Override
	    public void beforeTextChanged(CharSequence s, int start, int count, int after)
	    {
		// TODO Auto-generated method stub

	    }

	    @Override
	    public void afterTextChanged(Editable s)
	    {
		if (pin_4.getText().toString().length() == 0)
		{
		    pin_3.post(new Runnable()
		    {
			public void run()
			{
			    pin_3.requestFocus();
			}
		    });
		}

	    }
	});
	if (validatePinAfterIdle)
	{
	    View verticalLine = view.findViewById(R.id.change_popup_vertical_divider);
	    verticalLine.setVisibility(View.GONE);
	    pin_cancel_btn.setVisibility(View.GONE);
	    ViewGroup vg = (ViewGroup) pin_cancel_btn.getParent();
	    if (vg instanceof LinearLayout)
	    {
		((LinearLayout) vg).setWeightSum(0.5f);
		((LinearLayout) vg).invalidate();
	    }
	}

    }

    @Override
    public void onClick(View v)
    {
	if (layout == R.layout.validate_pin_popup)
	{
	    if (v.getId() == R.id.change_pin_done_image)
	    {
		validatePIN();
	    }
	    else
	    {
		this.dismiss();
	    }
	}
	else if (layout == R.layout.login_pin_change)
	{
	    if (v.getId() == R.id.change_pin_done_image)
	    {
		savePINForUser();
	    }
	    else
	    {
		this.dismiss();
	    }
	}
    }

    private void savePINForUser()
    {
	String pin = getPINString();
	if (pin.length() != 4)
	{
	    showInvalidPINError(R.string.error, R.string.pin_length_error);
	    return;
	}
	LoginActivity activity = (LoginActivity) getActivity();
	LibUtils.setDataForUser(LibUtils.getLoggedInUserBean().getUserName(), LibUtils.KEY_USER_INFO_PIN, getPINString());
	// LibUtils.setPINForUser(activity, LibUtils.getLoggedInUserBean().getUserName(), getPINString());
	this.dismiss();
	activity.hideKeyboard();
	// ICFHttpManager.getInstance().doConfigRequest(activity);
	// UiUtil.showProgressDialog(activity);
	activity.displayUtilitySelectionScreen();
    }

    private String getPINString()
    {
	View view = getView();
	final EditText pin_1 = (EditText) view.findViewById(R.id.pin_1);
	final EditText pin_2 = (EditText) view.findViewById(R.id.pin_2);
	final EditText pin_3 = (EditText) view.findViewById(R.id.pin_3);
	final EditText pin_4 = (EditText) view.findViewById(R.id.pin_4);
	return pin_1.getText().toString() + pin_2.getText().toString() + pin_3.getText().toString() + pin_4.getText().toString();
    }

    private void validatePIN()
    {
	String strPin = getPINString();
	String savedPin = null;
	if (validatePinOfflineUse)
	{
	    savedPin = LibUtils.getDataForUser(offlineUserName, LibUtils.KEY_USER_INFO_PIN);
	}
	else
	{
	    savedPin = LibUtils.getDataForUser(LibUtils.getLoggedInUserBean().getUserName(), LibUtils.KEY_USER_INFO_PIN);
	}
	// String savedPin = LibUtils.getPINForUser(getActivity(), LibUtils.getLoggedInUserBean().getUserName());
	if (strPin != null && strPin.length() < 4)
	{
	    showInvalidPINError(R.string.error, R.string.pin_length_error);
	    return;
	}
	if (strPin.equals(savedPin))
	{
	    if (validatePinOfflineUse)
	    {
		if (runnable != null)
		{
		    runnable.run();
		}
		dismiss();
	    }
	    else
	    {
		this.dismiss();
		if (isCancelable())
		{
		    PendingJobsFragment pendingJobsFragment = new PendingJobsFragment();
		    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout, pendingJobsFragment).addToBackStack("pendingjobs").commitAllowingStateLoss();
		}
	    }
	}
	else
	{
	    showInvalidPINError(R.string.invalid_pin_title, R.string.invalid_pin_txt);
	    pin_1.setText("");
	    pin_3.setText("");
	    pin_4.setText("");
	    pin_2.setText("");
	    pin_1.requestFocus();
	}

    }

    private void showInvalidPINError(int titleId, int messageId)
    {
	new AlertDialog.Builder(getActivity()).setTitle(getString(titleId)).setMessage(getString(messageId)).setPositiveButton(R.string.alert_ok, new DialogInterface.OnClickListener()
	{
	    public void onClick(DialogInterface dialog, int which)
	    {
		dialog.dismiss();
	    }
	}).setIcon(R.drawable.ic_dialog_alert_holo_light).show();
    }

    public static DialogFragment showSetPINDialog(FragmentActivity activity)
    {
	// KVB 12/1/2015 -- App is sometimes crashing here added this if statement
	ICFPINDialog dialogFragment = null;
	if (activity.getSupportFragmentManager().findFragmentByTag("fragment_edit_name") == null) {
	    dialogFragment = new ICFPINDialog();
	    dialogFragment.setCancelable(false);
	    dialogFragment.layout = R.layout.login_pin_change;
	    FragmentManager fm = activity.getSupportFragmentManager();
	    dialogFragment.show(fm, "fragment_edit_name");
	    }
	return dialogFragment;
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event)
    {
	if (keyCode == KeyEvent.KEYCODE_DEL)
	{
	    int id = v.getId();
	    if (id == R.id.pin_2)
	    {
		if (pin_2.getText().toString().length() == 0)
		{
		    pin_1.post(new Runnable()
		    {
			public void run()
			{
			    pin_1.requestFocus();
			}
		    });
		}
	    }
	    else if (id == R.id.pin_3)
	    {
		if (pin_3.getText().toString().length() == 0)
		{
		    pin_2.post(new Runnable()
		    {
			public void run()
			{
			    pin_2.requestFocus();
			}
		    });
		}
	    }
	    else if (id == R.id.pin_4)
	    {
		if (pin_4.getText().toString().length() == 0)
		{
		    pin_3.post(new Runnable()
		    {
			public void run()
			{
			    pin_3.requestFocus();
			}
		    });
		}
	    }
	    else
	    {
	    }
	}
	return false;
    }
}
