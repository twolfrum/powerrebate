package com.icf.rebate.ui.util.rebate;

import java.util.ArrayList;
import java.util.List;

import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.rebate.ui.util.ICFLogger;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class EquipmentListRebateTable
{
    // Database table
    public static final String TABLE_NAME = "equipment_rebate";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_UTILITY_COMPANY = "utility_company";
    public static final String COLUMN_CUSTOMER_TYPE = "customer_type";
    public static final String COLUMN_REPLACEMENT_TYPE = "replacement_type";
    public static final String COLUMN_HOUSE_TYPE = "house_type";
    public static final String COLUMN_EQUIPMENT_TYPE = "equipment_type";
    public static final String COLUMN_UPGRADE_REQUIREMENT = "upgrade_requirement";
    public static final String COLUMN_UPGRADE_REQUIREMENT2 = "upgrade_requirement2";
    public static final String COLUMN_OLD_HEATING_SYSTEM = "old_heating_system";
    public static final String COLUMN_MEASURE_TYPE = "measure_type";
    public static final String COLUMN_BASE_REBATE_VALUE = "baseRebateVal";
    public static final String COLUMN_ECM = "ecm";

    // Database creation SQL statement
    //  + ICFSQLiteOpenHelper.COLUMN_INCENTIVE_2015 + " REAL," + ICFSQLiteOpenHelper.COLUMN_INCENTIVE_2016 + " REAL," + ICFSQLiteOpenHelper.COLUMN_INCENTIVE_2017 + " REAL," + ICFSQLiteOpenHelper.COLUMN_INCENTIVE_2018 + " REAL," + ICFSQLiteOpenHelper.COLUMN_INCENTIVE_2019 + " REAL," + ICFSQLiteOpenHelper.COLUMN_INCENTIVE_2020 + " REAL," + ICFSQLiteOpenHelper.COLUMN_INCENTIVE_2021 + " REAL," + ICFSQLiteOpenHelper.COLUMN_INCENTIVE_2022 + " REAL," + ICFSQLiteOpenHelper.COLUMN_INCENTIVE_2023 + " REAL," + ICFSQLiteOpenHelper.COLUMN_INCENTIVE_2024 + " REAL"
    private static final String DATABASE_CREATE = "create table " + TABLE_NAME + "(" +
			COLUMN_ID + " integer primary key autoincrement, " +
			COLUMN_UTILITY_COMPANY + " INTEGER not null," +
			COLUMN_CUSTOMER_TYPE + " text," +
			COLUMN_REPLACEMENT_TYPE + " text not null," +
			COLUMN_HOUSE_TYPE + " text not null," +
			COLUMN_EQUIPMENT_TYPE + " text not null," +
			COLUMN_UPGRADE_REQUIREMENT + " REAL," +
			COLUMN_OLD_HEATING_SYSTEM + " text," +
			COLUMN_MEASURE_TYPE + " text," +
			COLUMN_ECM + " text," +
			COLUMN_BASE_REBATE_VALUE + " text," +
			COLUMN_UPGRADE_REQUIREMENT2 + " text" + ");";

    private static final String TAG = EquipmentListRebateTable.class.getName();
    private static final List<String> mRebateColumns = new ArrayList<String>();

	//The following value gets updated in method addRebateIntervalFields() and is
	//dependent on utility company. It will have the value of 10 or 11 depending
	//on whether or not the utility incorporates a second upgrade requirement value
	//(COLUMN_UPGRADE_REQUIREMENT2) in the rebate lookup. The value of mFirstRebateIntervalField
	//is used in the insert method to determine if COLUMN_UPGRADE_REQUIREMENT2 will have a
	//value specified in rebate.csv or if it should default to NULL.
	private static int mFirstRebateIntervalField;
    
    public static void onCreate(SQLiteDatabase database)
    {
	database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion)
    {
    }

	public static void addRebateIntervalFields(int id, SQLiteDatabase database, String csvLine) {
		String[] csvLineItems = csvLine.split(",");
		int firstRebateIntervalField = 0;
		int i = 1;
		mRebateColumns.clear();
		for (String s : csvLineItems) {
			// Matches 4 digits between 1900 - 2099
			if (s.matches("19\\d\\d|20\\d\\d")) {
				if (firstRebateIntervalField == 0) firstRebateIntervalField = i;
				String dateRange = "col" + id + "_" + "01_01_" + s + "__12_31_" + s;
				final String sqlStatement = "ALTER TABLE "
						+ TABLE_NAME
						+ " ADD COLUMN "
						+ dateRange
						+ " REAL"
						+ ";";
				mRebateColumns.add(dateRange);
				Cursor cursor = database.rawQuery("SELECT * FROM " + TABLE_NAME, null);
				if (cursor.getColumnIndex(dateRange) == -1) {
					database.execSQL(sqlStatement);
				}
			} else if (s.matches("\\d{2}/\\d{2}/\\d{4}\\s{0,}-\\s{0,}\\d{2}/\\d{2}/\\d{4}")) {
				if (firstRebateIntervalField == 0) firstRebateIntervalField = i;
				final String[] date = s.split("-|/");
				final String colName = "col" + id + "_" + date[0] + "_" + date[1] + "_"
						+ date[2].trim() + "__" + date[3].trim() + "_" + date[4] + "_" + date[5];
				final String sqlStatement = "ALTER TABLE "
						+ TABLE_NAME
						+ " ADD COLUMN "
						+ colName
						+ " REAL"
						+ ";";
				mRebateColumns.add(colName);
				Cursor cursor = database.rawQuery("SELECT * FROM " + TABLE_NAME, null);
				if (cursor.getColumnIndex(colName) == -1) {
					database.execSQL(sqlStatement);
				}
			}
			i++;
		}
		//set the static firstRebateIntervalField to be used in method insert()
		mFirstRebateIntervalField = firstRebateIntervalField;
	}

	public static void insert(int id, SQLiteDatabase database, String[] columnData) {
		try {
			ContentValues values = new ContentValues();
			int i = 1;
			values.put(COLUMN_UTILITY_COMPANY, id);
			for (String value : columnData) {
				switch (i) {
					//case 0: will never be satisfied as the utility company id is
					//not included in columnData array. It is included here for clarity -
					//the value of i reflects which column relative to the PK/_id
					//column will receive the value of var 'value'.
					case 0:
						values.put(COLUMN_UTILITY_COMPANY, Integer.parseInt(value));
						break;
					case 1:
						values.put(COLUMN_CUSTOMER_TYPE, value);
						break;
					case 2:
						values.put(COLUMN_REPLACEMENT_TYPE, value);
						break;
					case 3:
						values.put(COLUMN_HOUSE_TYPE, value);
						break;
					case 4:
						values.put(COLUMN_EQUIPMENT_TYPE, value);
						break;
					case 5:
						values.put(COLUMN_UPGRADE_REQUIREMENT, LibUtils.parseFloat(value, 0f) * ICFSQLiteOpenHelper.FLOAT_MULTIPLIER);
						break;
					case 6:
						values.put(COLUMN_OLD_HEATING_SYSTEM, value);
						break;
					case 7:
						values.put(COLUMN_MEASURE_TYPE, value);
						break;
					case 8:
						values.put(COLUMN_ECM, value);
						break;
					case 9:
						values.put(COLUMN_BASE_REBATE_VALUE, value);
						break;
					default:
						//case 10 may/may not be the first rebate interval column.
						//cases > 10 will ALWAYS be a rebate interval
						if (i - mFirstRebateIntervalField >= 0 &&
								i - mFirstRebateIntervalField < mRebateColumns.size()) {
							values.put(mRebateColumns.get(i - mFirstRebateIntervalField),
									LibUtils.parseDouble(value, 0f));
						} else if (i - mFirstRebateIntervalField < 0) {
							values.put(COLUMN_UPGRADE_REQUIREMENT2, value);
						}
						break;
				}
				i++;
			}
			database.insert(TABLE_NAME, null, values);
			ICFLogger.d(TAG, "Insert : " + TABLE_NAME + " " + values);
		} catch (Exception e) {
			ICFLogger.e(TAG, e);
		}
	}
}
