package com.icf.rebate.ui.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.AutoCompleteTextView;

public class CustomAutoCompleteTextView extends AutoCompleteTextView
{

    public CustomAutoCompleteTextView(Context context, AttributeSet attrs, int defStyle)
    {
	super(context, attrs, defStyle);
    }

    public CustomAutoCompleteTextView(Context context, AttributeSet attrs)
    {
	super(context, attrs);
    }

    public CustomAutoCompleteTextView(Context context)
    {
	super(context);
    }

    @Override
    public boolean enoughToFilter()
    {
	return true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
	if (isEnabled() && event.getAction() == MotionEvent.ACTION_UP && getAdapter() != null)
	{
	    performFiltering(getText(), 0);
	    showDropDown();
	}
	return super.onTouchEvent(event);
    }

}
