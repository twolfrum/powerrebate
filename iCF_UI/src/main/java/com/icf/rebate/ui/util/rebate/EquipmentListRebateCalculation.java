package com.icf.rebate.ui.util.rebate;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.util.Log;

import com.icf.rebate.networklayer.model.CustomerInfo;
import com.icf.rebate.networklayer.model.FormResponseBean;
import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.util.ICFLogger;
import com.icf.rebate.ui.util.UiUtil;

public class EquipmentListRebateCalculation {
    private static final String TAG = EquipmentListRebateCalculation.class.getName();
    private ArrayList<String> selectionArgs;
    private ICFSQLiteOpenHelper helper;
    private String selectionString;
    private String[] columns;
    private float capacity;
    private float baseRebateValue;
    private String orderBy;

    public EquipmentListRebateCalculation(String equipmentType, boolean replacementOnFail) {
        int utilityCompanyId = LibUtils.getSelectedUtilityCompany().getId();

        FormResponseBean bean = RebateManager.getInstance().getFormBean();
        CustomerInfo customerInfo = bean.getCustomerInfo();
        boolean singleHouse = customerInfo.getHouseType() == CustomerInfo.SINGLE_FAMILY_TYPE;
        helper = new ICFSQLiteOpenHelper(LibUtils.getApplicationContext());

        String customerType = null;

        if (bean.getAppDetails() != null) {
            customerType = bean.getAppDetails().getCustomerType();
        }

        if (customerType == null || customerType.length() == 0) {
            customerType = "NA";
        }

        selectionArgs = new ArrayList<>();

        selectionArgs.add(String.valueOf(utilityCompanyId));
        selectionArgs.add(equipmentType);
        selectionArgs.add("NA");
        selectionArgs.add(singleHouse ? "Single Family" : "Multifamily");
        selectionArgs.add("NA");
        selectionArgs.add(replacementOnFail ? "Replace on Fail" : "Early Retirement");
        selectionArgs.add("NA");
        selectionArgs.add(customerType);

        selectionString = EquipmentListRebateTable.COLUMN_UTILITY_COMPANY + "= ? AND " +
                          EquipmentListRebateTable.COLUMN_EQUIPMENT_TYPE + " = ? AND " +
                          "(" + EquipmentListRebateTable.COLUMN_HOUSE_TYPE + " = ? OR " +
                                EquipmentListRebateTable.COLUMN_HOUSE_TYPE + " = ?) AND " +
                          "(" + EquipmentListRebateTable.COLUMN_REPLACEMENT_TYPE + " = ? OR " +
                                EquipmentListRebateTable.COLUMN_REPLACEMENT_TYPE + " = ?) AND " +
                          "(" + DuctSealingRebateTable.COLUMN_CUSTOMER_TYPE + " = ? OR " +
                                DuctSealingRebateTable.COLUMN_CUSTOMER_TYPE + " = ?)";

        // columns = new String[] { LibUtils.getYearColumnName(), EquipmentListRebateTable.COLUMN_MEASURE_TYPE };
    }

    public float calculateRebateForOthers(String seerValue, float cap,
                              String oldHeatingType, String rebateCalcDate) {
        boolean seerIsRange = false;
        if (seerValue != null) {
            //try converting seer value to float to determine if it is a range or discreet value
            String testSeer = seerValue;
            if (seerValue.endsWith("+")) {
                testSeer = seerValue.substring(0, seerValue.lastIndexOf('+'));
            }
            try {
                float numericSeer = Float.parseFloat(testSeer);
            } catch (NumberFormatException nfe) {
                //assume seer is a range value
                seerIsRange = true;
            }
        }

        float retValue = 0.0f;
        this.capacity = cap;
        boolean isPlus = false;
        String actualSelectionString = selectionString;
        ArrayList<String> actualSelectionArgs = (ArrayList<String>) selectionArgs.clone();

        if (seerIsRange) {
            selectionString += " AND " + EquipmentListRebateTable.COLUMN_UPGRADE_REQUIREMENT2 + " = ? ";
            selectionArgs.add(seerValue);
        } else if (seerValue != null) {
            if (seerValue.endsWith("+")) {
                //only get here first time thru
                isPlus = true;
                seerValue = seerValue.substring(0, seerValue.lastIndexOf('+'));
                selectionString += " AND " + EquipmentListRebateTable.COLUMN_UPGRADE_REQUIREMENT + " > ? ";
            } else {
                //either first time thru or second time thru with '+' stripped from seerValue
                selectionString += " AND " + EquipmentListRebateTable.COLUMN_UPGRADE_REQUIREMENT + " <= ? ";
                this.orderBy = EquipmentListRebateTable.COLUMN_UPGRADE_REQUIREMENT + " desc";
            }
            selectionArgs.add(String.valueOf(LibUtils.parseFloat(seerValue, 0) *
                    ICFSQLiteOpenHelper.FLOAT_MULTIPLIER));
        }

        if (oldHeatingType == null) {
            oldHeatingType = "NA";
        }

        selectionString += " AND (" + EquipmentListRebateTable.COLUMN_OLD_HEATING_SYSTEM + " = ? OR " +
                                      EquipmentListRebateTable.COLUMN_OLD_HEATING_SYSTEM + " = ?)";
        selectionArgs.add("NA");
        selectionArgs.add(oldHeatingType);

        retValue = computeRebate(rebateCalcDate);

        selectionString = actualSelectionString;
        selectionArgs = actualSelectionArgs;

        if (isPlus && retValue == 0.0f) {
            //execute again without '+' seerValue
            retValue = calculateRebateForOthers(seerValue, cap, oldHeatingType, rebateCalcDate);
        } else {
            retValue = retValue * capacity + baseRebateValue;
        }
        retValue = UiUtil.convertToPostTwoDigtDecimal(retValue);
        return retValue;
    }

    private float computeRebate(String rebateCalcDate) {

        float retValue = 0;
        SQLiteDatabase database = helper.getReadableDatabase();
        Cursor dbCursor = database.query(EquipmentListRebateTable.TABLE_NAME, null, null, null, null, null, null);
        String[] columnNames = dbCursor.getColumnNames();

        // Gets list of all columns that apply to todays date
        List<String> matchingDateRangeCols = LibUtils.getYearColumnName(columnNames, rebateCalcDate);
        for (String col : matchingDateRangeCols) {
            columns = new String[]{col, EquipmentListRebateTable.COLUMN_MEASURE_TYPE,
                                        EquipmentListRebateTable.COLUMN_BASE_REBATE_VALUE};

            //log the query about to be executed
            String data = LibUtils.getSqlQueryString(EquipmentListRebateTable.TABLE_NAME, columns,
                    selectionString, selectionArgs.toArray(new String[selectionArgs.size()]),
                    null, null, orderBy);
            ICFLogger.d(TAG, data);
            //now execute query
            Cursor cursor = database.query(EquipmentListRebateTable.TABLE_NAME, columns,
                    selectionString, selectionArgs.toArray(new String[selectionArgs.size()]),
                    null, null, orderBy);

            while (cursor.moveToNext()) {
                // retValue = cursor.getFloat(0);
                if (!cursor.isNull(0)) {
                    retValue = retValue < cursor.getFloat(0) ? cursor.getFloat(0) : retValue;
                }
                String measureType = cursor.getString(1);
                if (!cursor.isNull(2)) {
                    baseRebateValue = cursor.getFloat(2);
                }
                if (measureType != null && !measureType.equalsIgnoreCase("ton")) {
                    capacity = 1;
                    // If not ton we don't use baseRebateValue
                    baseRebateValue = 0;
                }
                // break;
            }
        }

        database.close();
        return retValue;
    }

    public float calculateRebateForBrushless(boolean isBrushlessMotorChecked, String ecmValue, boolean isRetrofitBrushlessMotorChecked) {
        float retValue = 0.0f;

        if (isBrushlessMotorChecked || isRetrofitBrushlessMotorChecked) {
            ecmValue = ecmValue == null ? "NA" : ecmValue;

            selectionString += " AND " + EquipmentListRebateTable.COLUMN_ECM + " = ?";
            selectionArgs.add(ecmValue);

            retValue = computeRebate(null);
        }
        retValue = UiUtil.convertToPostTwoDigtDecimal(retValue);
        ICFLogger.d(TAG, "RebateForBrushless :" + "isBrushlessMotorChecked:" + isBrushlessMotorChecked + "ecmValue :" + ecmValue + "retValue" + retValue);
        return retValue;
    }

    public float calculateRebateForWaterHeater(String oldHeatingType, String seerValue, String rebateCalcDate) {
        float retValue = 0.0f;

        if (seerValue != null) {
            if (seerValue.endsWith("+")) {
                seerValue = seerValue.substring(0, seerValue.lastIndexOf('+'));
                selectionString += " AND " + EquipmentListRebateTable.COLUMN_UPGRADE_REQUIREMENT + " > ? ";

            } else {
                selectionString += " AND " + EquipmentListRebateTable.COLUMN_UPGRADE_REQUIREMENT + " <= ? ";
                this.orderBy = EquipmentListRebateTable.COLUMN_UPGRADE_REQUIREMENT + " desc";
            }

            float seer = LibUtils.parseFloat(seerValue, 0) * ICFSQLiteOpenHelper.FLOAT_MULTIPLIER;
            selectionArgs.add(seer + "");

        } else {
            seerValue = "0";
            selectionString += " AND " + EquipmentListRebateTable.COLUMN_UPGRADE_REQUIREMENT + " = ? ";
            selectionArgs.add(seerValue);
        }

        if (oldHeatingType != null) {
            selectionString += " AND (" + EquipmentListRebateTable.COLUMN_OLD_HEATING_SYSTEM + " = ? OR " + EquipmentListRebateTable.COLUMN_OLD_HEATING_SYSTEM + " = ?)";
            selectionArgs.add("NA");
            selectionArgs.add(oldHeatingType);
        } else {
            oldHeatingType = "NA";
            selectionString += " AND " + EquipmentListRebateTable.COLUMN_OLD_HEATING_SYSTEM + " = ?";
            selectionArgs.add(oldHeatingType);
        }

        retValue = computeRebate(rebateCalcDate);
        retValue = UiUtil.convertToPostTwoDigtDecimal(retValue);
        ICFLogger.d(TAG, "RebateForWaterHeater :" + "oldHeatingType:" + oldHeatingType + "seerValue :" + seerValue + "retValue" + retValue);
        return retValue;
    }

    public float calculateRebateForBoilerOrFurnace(String seerValue, float cap,
                                       String oldHeatingType, String rebateCalcDate) {
        boolean seerIsRange = false;
//        if (seerValue != null) {
//            if (seerValue.matches("(\\d*\\.{0,1}\\d{1,})\\s{0,}-\\s{0,}(\\d*\\.{0,1}\\d{1,})")) {
//                //seer is a range of values
//                seerIsRange = true;
//            }
//        }
        if (seerValue != null) {
            //try converting seer value to float to determine if it is a range or discreet value
            String testSeer = seerValue;
            if (seerValue.endsWith("+")) {
                testSeer = seerValue.substring(0, seerValue.lastIndexOf('+'));
            }
            try {
                float numericSeer = Float.parseFloat(testSeer);
            } catch (NumberFormatException nfe) {
                //assume seer is a range value
                seerIsRange = true;
            }
        }

        float retValue = 0.0f;
        this.capacity = cap;
        if (oldHeatingType == null) {
            oldHeatingType = "NA";
        }

        selectionString += " AND (" + EquipmentListRebateTable.COLUMN_OLD_HEATING_SYSTEM + " = ? OR " +
                EquipmentListRebateTable.COLUMN_OLD_HEATING_SYSTEM + " = ?)";
        selectionArgs.add("NA");
        selectionArgs.add(oldHeatingType);

        if (seerIsRange) {
            selectionString += " AND " + EquipmentListRebateTable.COLUMN_UPGRADE_REQUIREMENT2 + " = ? ";
            selectionArgs.add(seerValue);
        } else if (seerValue != null) {
            if (seerValue.endsWith("+")) {
                seerValue = seerValue.substring(0, seerValue.lastIndexOf('+'));
                selectionString += " AND " + EquipmentListRebateTable.COLUMN_UPGRADE_REQUIREMENT + " > ? ";

            } else {
                selectionString += " AND " + EquipmentListRebateTable.COLUMN_UPGRADE_REQUIREMENT + " <= ? ";
                this.orderBy = EquipmentListRebateTable.COLUMN_UPGRADE_REQUIREMENT + " desc";
            }
            selectionArgs.add(String.valueOf(LibUtils.parseFloat(seerValue, 0) * ICFSQLiteOpenHelper.FLOAT_MULTIPLIER));
        }

        retValue = computeRebate(rebateCalcDate);
        if (capacity > 0) {
            retValue = retValue * capacity + baseRebateValue;
        }

        retValue = UiUtil.convertToPostTwoDigtDecimal(retValue);
        ICFLogger.d(TAG, "RebateForBoilerOrFurnace :" + "seerValue:" + seerValue + "oldHeatingType :" + oldHeatingType + "retValue" + retValue);
        return retValue;

    }
}
