package com.icf.rebate.ui.dialog;

import android.view.View;

public interface DialogListener
{
    public void onFinishInflate(int layoutId, View view);

    public void onDismiss();
}
