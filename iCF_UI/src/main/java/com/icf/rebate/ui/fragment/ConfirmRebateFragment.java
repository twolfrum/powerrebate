package com.icf.rebate.ui.fragment;

import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.networklayer.model.AppDetails;
import com.icf.rebate.networklayer.model.CustomerInfo;
import com.icf.rebate.networklayer.model.Submission;
import com.icf.rebate.networklayer.model.CustomerInfo.RebateDifferentPerson;
import com.icf.rebate.networklayer.model.CustomerScreenLayout;
import com.icf.rebate.ui.RootActivity;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.controller.ValidationManager;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.UiUtil;

import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;

public class ConfirmRebateFragment extends Fragment implements View.OnClickListener
{

    private View rootView;
    private CheckBox mChkCustomer, mChkContractor, mChkDiffPerson;
    private ViewGroup mLytDiffPerson;
    private EditText firstName, lastName, address1, city, state, zip, phone, email;
    private CustomerScreenLayout customerScreenLayout;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
	super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
	getActivity().setTitle("Confirm Rebate");
	rootView = inflater.inflate(R.layout.fragment_confirm_rebate, container, false);
	bindViewListeners();
	getUIReferences();
	populateViews();
	// addValidationRebateToOtherPerson();
	return rootView;
    }

    private void getUIReferences()
    {
	firstName = (EditText) mLytDiffPerson.findViewById(R.id.first_name_edit);
	lastName = (EditText) mLytDiffPerson.findViewById(R.id.last_name_edit);
	address1 = (EditText) mLytDiffPerson.findViewById(R.id.address_edit1);
	city = (EditText) mLytDiffPerson.findViewById(R.id.city_edit);
	state = (EditText) mLytDiffPerson.findViewById(R.id.state_edit);
	zip = (EditText) mLytDiffPerson.findViewById(R.id.zip_edit);
	phone = (EditText) mLytDiffPerson.findViewById(R.id.phone_edit);
	email = (EditText) mLytDiffPerson.findViewById(R.id.email_edit);
    }

    private void bindViewListeners()
    {

	mChkCustomer = (CheckBox) rootView.findViewById(R.id.rebate_current_bill_checkbox);
	mChkContractor = (CheckBox) rootView.findViewById(R.id.send_rebate_to_uitility_checkbox);
	mChkDiffPerson = (CheckBox) rootView.findViewById(R.id.send_rebate_checkbox);

	mChkCustomer.setOnClickListener(this);
	mChkContractor.setOnClickListener(this);
	mChkDiffPerson.setOnClickListener(this);

	mLytDiffPerson = (ViewGroup) rootView.findViewById(R.id.send_rebate_lyt);

	rootView.findViewById(R.id.btn_continue).setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
	switch (v.getId())
	{
	case (R.id.rebate_current_bill_checkbox):
	    if (!mChkDiffPerson.isChecked() && !mChkContractor.isChecked()) {
		mChkCustomer.setChecked(true);
	    }
	    if (!customerScreenLayout.getSendRebateToMultiple())
	    {
		mChkDiffPerson.setChecked(false);
		mChkContractor.setChecked(false);
		mLytDiffPerson.setVisibility(View.GONE);
	    }
	    break;
	case (R.id.send_rebate_to_uitility_checkbox):
	    if (!mChkDiffPerson.isChecked() && !mChkCustomer.isChecked()) {
		mChkContractor.setChecked(true);
	    }
	    if (!customerScreenLayout.getSendRebateToMultiple())
	    {
		mChkDiffPerson.setChecked(false);
		mChkCustomer.setChecked(false);
		mLytDiffPerson.setVisibility(View.GONE);
	    }
	    break;
	case (R.id.send_rebate_checkbox):
	    if (!mChkCustomer.isChecked() && !mChkContractor.isChecked()) {
		mChkDiffPerson.setChecked(true);
	    }
	    mLytDiffPerson.setVisibility(mChkDiffPerson.isChecked() ? View.VISIBLE : View.GONE);
	    if (!customerScreenLayout.getSendRebateToMultiple())
	    {
		mChkCustomer.setChecked(false);
		mChkContractor.setChecked(false);
	    }
	    break;
	case (R.id.btn_continue):
	    if (mChkDiffPerson.isChecked())
	    {
		/*
		 * if (!ValidationManager.getInstance().doValidation("confirmRebate")) { return; }
		 */
		boolean validationSuccess = true;
		validationSuccess = validate(firstName, "Please enter a valid first name");
		validationSuccess = validate(lastName, "Please enter a valid last name");
		validationSuccess = validate(address1, "Please enter a valid address");
		validationSuccess = validate(city, "Please enter a valid city");
		validationSuccess = validate(state, "Please enter a valid state");
		validationSuccess = validate(zip, "Please enter a valid zip");
		if (!validationSuccess)
		{
		    return;
		}
	    }
	    retrieveViewInfo();

	    // Screen sequence options

	    Submission submission = null;
	    Fragment frag = null;
	    if ((submission = RebateManager.getInstance().getFormBean().getSubmissionObjectById(AppConstants.TERMS_AND_CONDITIONS_ID)) != null)
	    {
		frag = new TermAndConditionFragment();
	    }
	    else
	    {
		frag = new RebateConfirmationFragment();
	    }
	    ((RootActivity) getActivity()).beginTransaction().replace(R.id.mainLayout, frag).addToBackStack(null).commit();
	    break;
	}
    }

    // TODO add patterns for email and phone number
    public boolean validate(EditText edt, String errorMessage)
    {
	boolean isValid = true;
	if (edt == null || errorMessage == null)
	{
	    return false;
	}
	if (edt.getText().toString().trim().equals(""))
	{
	    isValid = false;
	    UiUtil.showError(getActivity(), "Error", errorMessage);
	}
	return isValid;
    }

    private void populateViews()
    {
	CustomerInfo info = RebateManager.getInstance().getFormBean().getCustomerInfo();
	if (info == null)
	{
	    return;
	}
	if (info.getRebateOption() == CustomerInfo.REBATE_UTILITY_COMPANY)
	{
	    mChkContractor.setChecked(true);
	}
	else if (info.getRebateOption() == CustomerInfo.REBATE_DIFFERENT_PERSON)
	{
	    mChkDiffPerson.setChecked(true);

	    // Not working sometimes just going to add validation manually here
	    // addValidationRebateToOtherPerson();

	    mLytDiffPerson.setVisibility(View.VISIBLE);

	    RebateDifferentPerson diffPerson = info.getDiffPerson();
	    if (diffPerson != null)
	    {
		firstName.setText(diffPerson.getFirstName());
		lastName.setText(diffPerson.getLastName());
		address1.setText(diffPerson.getAddress());
		city.setText(diffPerson.getCity());
		state.setText(diffPerson.getState());
		zip.setText(diffPerson.getZipCode());
		phone.setText(diffPerson.getPhone());
		email.setText(diffPerson.getEmail());
	    }
	}
	else if (info.getRebateOption() == CustomerInfo.REBATE_CURRENT_BILL)
	{
	    mChkCustomer.setChecked(true);
	}
	
	customerScreenLayout = RebateManager.getInstance().getFormBean().getCustomerScreenLayout();
	if (customerScreenLayout != null) {
	    //customize "send rebate to" labelling or hide "send rebate to" options as needed
	    if (!TextUtils.isEmpty(customerScreenLayout.getSendRebateToCustomer())) {
		if (mChkCustomer != null) 
		    mChkCustomer.setText(customerScreenLayout.getSendRebateToCustomer());
	    } else if (mChkCustomer != null) {
		mChkCustomer.setVisibility(View.GONE);
		rootView.findViewById(R.id.custom_info_divider1).setVisibility(View.GONE);
	    }
	    
	    if (!TextUtils.isEmpty(customerScreenLayout.getSendRebateToHVACContractor())) {
		if (mChkContractor != null) 
		    mChkContractor.setText(customerScreenLayout.getSendRebateToHVACContractor());
	    } else if (mChkContractor != null) {
		mChkContractor.setVisibility(View.GONE);
		rootView.findViewById(R.id.custom_info_divider2).setVisibility(View.GONE);
	    }
	    
	    if (!TextUtils.isEmpty(customerScreenLayout.getSendRebateToDifferentPerson())) {
		if (mChkDiffPerson != null) 
		    mChkDiffPerson.setText(customerScreenLayout.getSendRebateToDifferentPerson());
	    } else if (mChkDiffPerson != null) {
		mChkDiffPerson.setVisibility(View.GONE);
	    }
	}
    }

    private void addValidationRebateToOtherPerson()
    {
	EditText firstName = (EditText) mLytDiffPerson.findViewById(R.id.first_name_edit);
	EditText lastName = (EditText) mLytDiffPerson.findViewById(R.id.last_name_edit);
	EditText address1 = (EditText) mLytDiffPerson.findViewById(R.id.address_edit1);
	EditText city = (EditText) mLytDiffPerson.findViewById(R.id.city_edit);
	EditText state = (EditText) mLytDiffPerson.findViewById(R.id.state_edit);
	EditText zip = (EditText) mLytDiffPerson.findViewById(R.id.zip_edit);
	EditText phone = (EditText) mLytDiffPerson.findViewById(R.id.phone_edit);
	EditText email = (EditText) mLytDiffPerson.findViewById(R.id.email_edit);

	phone.setOnFocusChangeListener(new CustomerInformationFragment.FormatPhoneOnFocusChange());

	ValidationManager.getInstance().addValidationView(phone, "confirmRebate", ValidationManager.phoneRule, R.string.validation_rebate_phone, true);
	ValidationManager.getInstance().addValidationView(email, "confirmRebate", ValidationManager.emailRule, R.string.validation_rebate_email, true);
	ValidationManager.getInstance().addValidationView(firstName, "confirmRebate", ValidationManager.emptyRule, R.string.validation_rebatefname);
	ValidationManager.getInstance().addValidationView(lastName, "confirmRebate", ValidationManager.emptyRule, R.string.validation_rebatelname);
	ValidationManager.getInstance().addValidationView(firstName, "confirmRebate", ValidationManager.nameRule, R.string.validation_rebatefname);
	ValidationManager.getInstance().addValidationView(lastName, "confirmRebate", ValidationManager.nameRule, R.string.validation_rebatelname);
	ValidationManager.getInstance().addValidationView(address1, "confirmRebate", ValidationManager.emptyRule, R.string.validation_rebate_address1);
	ValidationManager.getInstance().addValidationView(city, "confirmRebate", ValidationManager.emptyRule, R.string.validation_rebate_city);
	ValidationManager.getInstance().addValidationView(state, "confirmRebate", ValidationManager.emptyRule, R.string.validation_rebate_state);
	ValidationManager.getInstance().addValidationView(zip, "confirmRebate", ValidationManager.emptyRule, R.string.validation_rebate_zip);
    }

    public void retrieveViewInfo()
    {
	CustomerInfo info = RebateManager.getInstance().getFormBean().getCustomerInfo();
	AppDetails appDetails = RebateManager.getInstance().getFormBean().getAppDetails();

	if (mChkContractor.isChecked())
	{
	    info.setRebateOption(CustomerInfo.REBATE_UTILITY_COMPANY);
	    info.updateAppDetail(appDetails);

	    info.setDiffPerson(null);
	}
	else if (mChkDiffPerson.isChecked())
	{
	    info.setRebateOption(CustomerInfo.REBATE_DIFFERENT_PERSON);
	    info.updateAppDetail(appDetails);

	    EditText firstName = (EditText) mLytDiffPerson.findViewById(R.id.first_name_edit);
	    EditText lastName = (EditText) mLytDiffPerson.findViewById(R.id.last_name_edit);
	    EditText address1 = (EditText) mLytDiffPerson.findViewById(R.id.address_edit1);
	    EditText city = (EditText) mLytDiffPerson.findViewById(R.id.city_edit);
	    EditText state = (EditText) mLytDiffPerson.findViewById(R.id.state_edit);
	    EditText zip = (EditText) mLytDiffPerson.findViewById(R.id.zip_edit);
	    EditText phone = (EditText) mLytDiffPerson.findViewById(R.id.phone_edit);
	    EditText email = (EditText) mLytDiffPerson.findViewById(R.id.email_edit);
	    RebateDifferentPerson diffPerson = new RebateDifferentPerson();

	    diffPerson.setPhone(phone.getText().toString());
	    diffPerson.setEmail(email.getText().toString());
	    diffPerson.setFirstName(firstName.getText().toString());
	    diffPerson.setLastName(lastName.getText().toString());
	    diffPerson.setAddress(address1.getText().toString());
	    diffPerson.setCity(city.getText().toString());
	    diffPerson.setState(state.getText().toString());
	    diffPerson.setZipCode(zip.getText().toString());

	    info.setDiffPerson(diffPerson);
	}
	else if (mChkCustomer.isChecked())
	{
	    info.setRebateOption(CustomerInfo.REBATE_CURRENT_BILL);
	    info.updateAppDetail(appDetails);

	    info.setDiffPerson(null);
	}
    }
}
