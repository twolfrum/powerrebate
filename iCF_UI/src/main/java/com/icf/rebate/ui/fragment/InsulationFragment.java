/**
 * 
 */
package com.icf.rebate.ui.fragment;

import java.util.ArrayList;
import java.util.List;

import com.crashlytics.android.Crashlytics;
import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.adapter.InsulationAdapter;
import com.icf.rebate.networklayer.model.FormResponseBean;
import com.icf.rebate.networklayer.model.FormResponseBean.Insulation;
import com.icf.rebate.ui.FormFragment;
import com.icf.rebate.ui.RootActivity;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.listeners.FragmentCallFlow;
import com.icf.rebate.ui.listeners.RequestCallFlow;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import io.fabric.sdk.android.Fabric;
import android.widget.ListView;

/**
 * @author Ken Butler Dec 1, 2015 InsulationFragment.java Copyright (c) 2015 ICF International
 */
public class InsulationFragment extends Fragment implements OnItemClickListener,
        RequestCallFlow, FragmentCallFlow
{

    private static final String TAG = "InsulationFragment";
    private String mTitle;
    private ListView mListView;
    private View rootView;
    private InsulationAdapter mInsulationAdapter;

    public String getTitle()
    {
	return mTitle;
    }

    public void setTitle(String title)
    {
	this.mTitle = title;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
	rootView = inflater.inflate(R.layout.fragment_insulation, container, false);
	mListView = (ListView) rootView.findViewById(R.id.listview_insulation);
	// Set title
	RootActivity rootActivity = null;
	try
	{
	    rootActivity = (RootActivity) getActivity();
	}
	catch (ClassCastException e)
	{
	    throw new ClassCastException("This fragment must be attached to RootActivity");
	}
	if (rootActivity != null)
	{
	    rootActivity.setTitle(mTitle);
	}
	bindListeners();
	requestData();
	return rootView;
    }

    private void bindListeners()
    {
	mListView.setOnItemClickListener(this);
    }

    private void updateInsulationList(FormResponseBean bean)
    {
	List<Insulation> insulationList = bean.getInsulation();

	if (insulationList == null)
	{
	    insulationList = new ArrayList<Insulation>();
	}

	mInsulationAdapter = new InsulationAdapter(getActivity(), 0, insulationList);
	mInsulationAdapter.setAdapterListener(mInsulationAdapter);
	mListView.setAdapter(mInsulationAdapter);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
	// Add crashylitcs for equipment type
	if (mInsulationAdapter != null && mInsulationAdapter.getListObjects() != null && mInsulationAdapter.getListObjects().size() > position && Fabric.isInitialized())
	{
	    String str = "";
	    Crashlytics.setString("selectedEquipmentName", str = (mInsulationAdapter.getItem(position) == null ? "null" : mInsulationAdapter.getItem(position).getName()));
	    Log.d("Insulation", "Setting crashlytics custom tag selectedEquipmentName=" + str);
	}

	InsulationFormFragment formFragment = new InsulationFormFragment();
	Bundle bundle = new Bundle();
	bundle.putInt("index", position);
	formFragment.setArguments(bundle);
	((RootActivity) getActivity()).beginTransaction().replace(R.id.mainLayout, formFragment).addToBackStack(null).commitAllowingStateLoss();
    }

    @Override
    public void showProgressDialog()
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void stopProgressDialog()
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void updateData(Object obj)
    {
	updateInsulationList((FormResponseBean) obj);

    }

    @Override
    public void displayError(String title, String message)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void requestData()
    {
	RebateManager.getInstance().checkAndrequestData(this);
    }

    @Override
    public void getUiControls(View root)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void initialize(View root)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void bindCallbacks(View root)
    {
	// TODO Auto-generated method stub

    }

}
