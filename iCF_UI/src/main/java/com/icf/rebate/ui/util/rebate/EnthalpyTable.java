package com.icf.rebate.ui.util.rebate;

import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.rebate.ui.util.ICFLogger;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class EnthalpyTable
{
    public static final String TABLE_NAME = "enthalpy";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_WET_BULB_TEMP = "wet_bulb_temp";
    public static final String COLUMN_TENTHS_DEG_ZERO = "tenths_deg_0";
    public static final String COLUMN_TENTHS_DEG_ONE = "tenths_deg_1";
    public static final String COLUMN_TENTHS_DEG_TWO = "tenths_deg_2";
    public static final String COLUMN_TENTHS_DEG_THREE = "tenths_deg_3";
    public static final String COLUMN_TENTHS_DEG_FOUR = "tenths_deg_4";
    public static final String COLUMN_TENTHS_DEG_FIVE = "tenths_deg_5";
    public static final String COLUMN_TENTHS_DEG_SIX = "tenths_deg_6";
    public static final String COLUMN_TENTHS_DEG_SEVEN = "tenths_deg_7";
    public static final String COLUMN_TENTHS_DEG_EIGHT = "tenths_deg_8";
    public static final String COLUMN_TENTHS_DEG_NINE = "tenths_deg_9";

    // Database creation SQL statement
    private static final String DATABASE_CREATE = "create table " 
        + TABLE_NAME
        + "(" 
        + COLUMN_ID + " integer primary key autoincrement, " 
        + COLUMN_WET_BULB_TEMP + " INTEGER not null,"
        + COLUMN_TENTHS_DEG_ZERO + " REAL not null,"
        + COLUMN_TENTHS_DEG_ONE + " REAL not null,"
        + COLUMN_TENTHS_DEG_TWO + " REAL not null,"
        + COLUMN_TENTHS_DEG_THREE + " REAL not null,"
        + COLUMN_TENTHS_DEG_FOUR + " REAL not null,"
        + COLUMN_TENTHS_DEG_FIVE + " REAL not null,"
        + COLUMN_TENTHS_DEG_SIX + " REAL not null,"
        + COLUMN_TENTHS_DEG_SEVEN + " REAL not null,"
        + COLUMN_TENTHS_DEG_EIGHT + " REAL not null,"
        + COLUMN_TENTHS_DEG_NINE + " REAL not null"
        + ");";

    private static final String TAG = EnthalpyTable.class.getName();

    public static void onCreate(SQLiteDatabase database) {
	//Unlike the rebate tables which are dropped and re-created from the 
	//downloaded resource files on every app startup, this is a static table.
	//Because of the way the startup sequence handles the dropping and 
	//re-creating of the rebate tables, this method gets called twice
	//where the second iteration will throw "table already exists" error. 
	try {
	    database.execSQL(DATABASE_CREATE);
	} catch (Exception e) {
	    ICFLogger.e(TAG, e);
	}
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
        int newVersion) {
	if (newVersion == 2) {
	    //This table was first introduced in V2
	    //Because of the way the startup sequence handles the dropping and 
	    //re-creating of the rebate tables, this method gets called twice
	    //where the second iteration will throw "table already exists" error. 
	    try {
	    	database.execSQL(DATABASE_CREATE);
	    } catch (Exception e) {
		ICFLogger.e(TAG, e);
	    }
	}
    }
    
    public static void insert(SQLiteDatabase database, String[] columnData)
    {
	try
	{
	    //The first line of the enthalpy.csv file used to populate the table contains 
	    //column headers, so the following code will gracefully handle the parse error
	    //and not insert a db record, which is the desired result.
	    ContentValues values = new ContentValues();
	    int i = 0;
	    for (String value : columnData)
	    {
		switch (i)
		{
		case 0:
		    values.put(COLUMN_WET_BULB_TEMP, Integer.parseInt(value));
		    break;
		case 1:
		    values.put(COLUMN_TENTHS_DEG_ZERO, LibUtils.parseFloat(value, 0f));
		    break;
		case 2:
		    values.put(COLUMN_TENTHS_DEG_ONE, LibUtils.parseFloat(value, 0f));
		    break;
		case 3:
		    values.put(COLUMN_TENTHS_DEG_TWO, LibUtils.parseFloat(value, 0f));
		    break;
		case 4:
		    values.put(COLUMN_TENTHS_DEG_THREE, LibUtils.parseFloat(value, 0f));
		    break;
		case 5:
		    values.put(COLUMN_TENTHS_DEG_FOUR, LibUtils.parseFloat(value, 0f));
		    break;
		case 6:
		    values.put(COLUMN_TENTHS_DEG_FIVE, LibUtils.parseFloat(value, 0f));
		    break;
		case 7:
		    values.put(COLUMN_TENTHS_DEG_SIX, LibUtils.parseFloat(value, 0f));
		    break;
		case 8:
		    values.put(COLUMN_TENTHS_DEG_SEVEN, LibUtils.parseFloat(value, 0f));
		    break;
		case 9:
		    values.put(COLUMN_TENTHS_DEG_EIGHT, LibUtils.parseFloat(value, 0f));
		    break;
		case 10:
		    values.put(COLUMN_TENTHS_DEG_NINE, LibUtils.parseFloat(value, 0f));
		    break;
		}
		i++;
	    }
	    database.insert(TABLE_NAME, null, values);
	    ICFLogger.d(TAG, "Insert : " +TABLE_NAME+ " "+values);
	}
	catch (Exception e)
	{
	    ICFLogger.e(TAG, e);
	}
    }
    
    public static Float query (SQLiteDatabase database, String degrees, String tenths) {
	Float enthalpy = null;
	String enthalpyColumn = null;
	
	try {
	    switch (Integer.parseInt(tenths)) {
	    case 0: enthalpyColumn = COLUMN_TENTHS_DEG_ZERO; break;
	    case 1: enthalpyColumn = COLUMN_TENTHS_DEG_ONE; break;
	    case 2: enthalpyColumn = COLUMN_TENTHS_DEG_TWO; break;
	    case 3: enthalpyColumn = COLUMN_TENTHS_DEG_THREE; break;
	    case 4: enthalpyColumn = COLUMN_TENTHS_DEG_FOUR; break;
	    case 5: enthalpyColumn = COLUMN_TENTHS_DEG_FIVE; break;
	    case 6: enthalpyColumn = COLUMN_TENTHS_DEG_SIX; break;
	    case 7: enthalpyColumn = COLUMN_TENTHS_DEG_SEVEN; break;
	    case 8: enthalpyColumn = COLUMN_TENTHS_DEG_EIGHT; break;
	    case 9: enthalpyColumn = COLUMN_TENTHS_DEG_NINE; break;
	    }
	    Cursor c = database.query(TABLE_NAME, new String[]{enthalpyColumn}, COLUMN_WET_BULB_TEMP + "=" + degrees, null, null, null, null);
	    if (c.moveToFirst()) {
		enthalpy = c.getFloat(0);
	    }
	    c.close();
	} catch (Exception e) {
	    ICFLogger.e(TAG, e);
	}
	
	return enthalpy;
    }
    
    public static boolean isPopulated(SQLiteDatabase database) {
	Cursor c = database.query(TABLE_NAME, new String[]{COLUMN_ID}, null, null, null, null, null);
	int r = c.getCount();
	c.close();
	return (r > 0);
    }

}
