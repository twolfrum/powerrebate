package com.icf.rebate.ui;

import java.io.File;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.customviews.SignatureView;
import com.icf.rebate.ui.fragment.RebateConfirmationFragment;
import com.icf.rebate.ui.util.FileUtil;
import com.icf.rebate.ui.util.UiUtil;

public class SignatureFragment extends DialogFragment
{

    private ImageButton btnClear;

    private ImageButton btnSave;

    private SignatureView signatureView;

    private OnClickListener clickListener;

    private RootActivity rootAct;

    private View signatureFooter;

    private Handler mHandler;

    private String jobId;
    
    private DialogInterface.OnDismissListener mOnDismissListener;

    public SignatureFragment()
    {
	mHandler = new Handler();
	setStyle(0, R.style.MyTheme_Dialog);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
	View view = inflater.inflate(R.layout.signaturelayout, null);
	return view;
    }

    public void onViewCreated(View view, Bundle savedInstanceState)
    {
	getDialog().setCanceledOnTouchOutside(false);
	jobId = RebateManager.getInstance().getFormBean().getCustomerInfo().getJobId();
	mHandler.post(new Runnable()
	{
	    public void run()
	    {
		Dialog d = getDialog();
		if (d != null)
		{
		    Window w = d.getWindow();
		    if (w != null)
		    {
			LayoutParams params = w.getAttributes();
			if (params != null)
			{
			    params.height = (int) (UiUtil.getDisplayHeight(getActivity().getApplicationContext()) * 0.75f);
			}
			w.setAttributes(params);
		    }
		}
	    }
	});
	initSignatureContent(view);
	rootAct = (RootActivity) getActivity();
	if (rootAct != null)
	{
	    rootAct.updateTitle(getResources().getString(R.string.signature_txt));
	}
    }

    private String getSignatureFilePath()
    {
	return FileUtil.getJobDirectory(jobId) + "/signature.png";
    }

    private void initializeExistingSignature()
    {
	Bitmap bmp = BitmapFactory.decodeFile(getSignatureFilePath());
	if (bmp != null)
	{
	    signatureView.setSignatureBitmap(bmp);
	    signatureFooter.setVisibility(View.GONE);
	}
    }

    private void initSignatureContent(View view)
    {
	signatureView = (SignatureView) view.findViewById(R.id.signatureView);
	signatureFooter = view.findViewById(R.id.signatureFooter);
	btnClear = (ImageButton) view.findViewById(R.id.btnClear);
	btnSave = (ImageButton) view.findViewById(R.id.btnDone);
	initializeExistingSignature();
	clickListener = new OnClickListener()
	{

	    @Override
	    public void onClick(View v)
	    {
		int id = v.getId();
		if (id == R.id.btnClear)
		{
		    signatureView.clearSignature();
		}
		else if (id == R.id.btnDone)
		{
		    Thread t = new Thread(new Runnable()
		    {
			public void run()
			{
			    if (signatureView.isUserSigned())
			    {
				File file = new File(getSignatureFilePath());
				final String path = file.getAbsolutePath();
				boolean saveState = signatureView.saveSignature(path);
				if (saveState && getActivity() != null)
				{
				    getActivity().runOnUiThread(new Runnable()
				    {

					@Override
					public void run()
					{
					    // getActivity().onBackPressed();
					    Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.mainLayout);
					    if (fragment instanceof RebateConfirmationFragment)
					    {
						// ((RebateConfirmationFragment) fragment).refreshList(false);
					    }
					    dismissAllowingStateLoss();
					    rootAct = (RootActivity) getActivity();
					    if (rootAct != null)
					    {
						rootAct.updateTitle(getResources().getString(R.string.rebate_confirmation_txt));
					    }
					}
				    });
				}
			    }
			    else
			    {
				getActivity().runOnUiThread(new Runnable()
				{
				    public void run()
				    {
					Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.mainLayout);
					if (fragment instanceof RebateConfirmationFragment)
					{
					  //  ((RebateConfirmationFragment) fragment).refreshList(false);
					}
					dismissAllowingStateLoss();
					UiUtil.showError(getActivity(), getActivity().getString(R.string.alert_title), getActivity().getString(R.string.validation_signature_empty));
				    }
				});
			    }
			}
		    });
		    t.start();
		}
	    }
	};

	btnClear.setOnClickListener(clickListener);
	btnSave.setOnClickListener(clickListener);
    }

    public void setOnDismissListener(OnDismissListener onDismissListener)
    {
	mOnDismissListener = onDismissListener;
    }

    @Override
    public void onDismiss(DialogInterface dialog)
    {
	if (mOnDismissListener != null) {
	    mOnDismissListener.onDismiss(dialog);
	}
	super.onDismiss(dialog);
    }
    
    
}
