package com.icf.rebate.ui.controller;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.text.InputFilter;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.widget.EditText;

import com.icf.rebate.networklayer.model.FormResponseBean.Options;

public abstract class CustomInputTextFilter implements InputFilter
{
    EditText editText;
    Options options;

    public CustomInputTextFilter(EditText editText, Options options)
    {
	this.editText = editText;
	this.options = options;
    }

    private CharSequence validateField(EditText editText, String dest, CharSequence character)
    {
	CharSequence retVal = null;
	String inputStr = dest + character;
	if (options.getFormat() != null && !TextUtils.isEmpty(options.getFormat()))
	{
	    Pattern pattern = Pattern.compile(options.getFormat());
	    Matcher m = pattern.matcher(inputStr);
	    if (m.matches())
	    {
		getEditTextView(editText);
		String savedValue = options.getSavedValue() != null ? (options.getSavedValue() + character) : (character + "");
		options.setSavedValue(savedValue);
	    }
	    else
	    {
		retVal = "";
	    }

	}
	return retVal;
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend)
    {
	// String rule = options.getLength();
	// if (rule != null && !TextUtils.isEmpty(rule))
	// {
	// CharSequence temp = source;
	// String text = source.toString().substring(start, end);
	//
	// Log.v("Varun", "text " + text + " textlen " + text.length());
	// Log.v("Varun", "source " + source + " start " + start + " end " + end + " dest " + dest + " dstart " + dstart + " dend " + dend);
	//
	// if (source.length() > 1 && (end - start - 1) < source.length())
	// {
	// temp = source.charAt(end - start - 1) + "";
	// }
	// else if (source.length() == 0 || (source.length() > rule.length()))
	// {
	// temp = "";
	// }
	//
	// return validateField(editText, dest.toString(), temp);
	// }
	// return null;

	// for (int i = start; i < end; i++)
	{
	    // if (Character.isLowerCase(source.charAt(i)))
	    if (options.getFormat() != null && !TextUtils.isEmpty(options.getFormat()))
	    {
		Pattern pattern = Pattern.compile(options.getFormat());
		String inputString = null;
		if (source instanceof SpannableStringBuilder)
		{
		    inputString = source.toString().substring(start, end);
		}
		else
		{
		    inputString = dest.toString() + source;
		}
		Matcher m = pattern.matcher(inputString);
		if (!m.matches())
		{
		    if (source instanceof SpannableStringBuilder)
		    {
			char[] v = new char[end - start];
			TextUtils.getChars(source, start, end, v, 0);
			String s = new String(v);

			if (source instanceof Spanned)
			{
			    SpannableString sp = new SpannableString(s);
			    TextUtils.copySpansFrom((Spanned) source, start, end, null, sp, 0);
			    return sp;
			}
			else
			{
			    return s;
			}
		    }
		    else
		    {
			return "";
		    }

		}
	    }
	}

	return null; // keep original
    }

    abstract void getEditTextView(EditText edit);
}
