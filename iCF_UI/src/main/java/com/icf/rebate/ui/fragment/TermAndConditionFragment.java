package com.icf.rebate.ui.fragment;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Picture;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioGroup;

import com.icf.rebate.app.model.DuctSealing;
import com.icf.rebate.app.model.DuctSealingItem;
import com.icf.rebate.networklayer.model.AppDetails;
import com.icf.rebate.networklayer.model.ContractorDetails;
import com.icf.rebate.networklayer.model.CustomerInfo;
import com.icf.rebate.networklayer.model.CustomerInfo.RebateDifferentPerson;
import com.icf.rebate.networklayer.model.FormResponseBean;
import com.icf.rebate.networklayer.model.FormResponseBean.Equipment;
import com.icf.rebate.networklayer.model.FormResponseBean.Insulation;
import com.icf.rebate.networklayer.model.FormResponseBean.InsulationItem;
import com.icf.rebate.networklayer.model.FormResponseBean.Item;
import com.icf.rebate.networklayer.model.FormResponseBean.TuneUp;
import com.icf.rebate.networklayer.model.FormResponseBean.TuneUpItem;
import com.icf.rebate.networklayer.model.FormResponseBean.WDItem;
import com.icf.rebate.networklayer.model.FormResponseBean.WindowAndDoor;
import com.icf.rebate.networklayer.model.QIVItem;
import com.icf.rebate.networklayer.model.ThermostatInstallItem;
import com.icf.rebate.networklayer.utils.LibUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;

import com.fasterxml.jackson.databind.util.Converter;
import com.google.zxing.common.StringUtils;
import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.ui.RootActivity;
import com.icf.rebate.ui.SignatureFragment;
import com.icf.rebate.ui.controller.FormUIManager;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.FileUtil;
import com.icf.rebate.ui.util.UiUtil;
import com.icf.rebate.ui.customviews.MeasurableWebView;

public class TermAndConditionFragment extends Fragment implements OnClickListener
{

    // private WebView webview;

    protected static final String TAG = TermAndConditionFragment.class.getSimpleName();
    private final String NL = "<br />";

    private Button btnContinue;

    private WebView txtTermsAndCond;

    private ProgressBar loadingBar;

    private RadioGroup radioGroup;

    private AppDetails appDetail;

    private String jobId;

    private int beginSigSection, endSigSection;

    private String contentHtml;

    @SuppressLint("NewApi")
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
	super.onCreate(savedInstanceState);
	if (Build.VERSION.SDK_INT >= 21)
	{
	    WebView.enableSlowWholeDocumentDraw();
	}
	jobId = RebateManager.getInstance().getFormBean().getCustomerInfo().getJobId();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
	View view = inflater.inflate(R.layout.termsandcond, null, false);
	return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
	super.onViewCreated(view, savedInstanceState);
	appDetail = RebateManager.getInstance().getFormBean().getAppDetails();
	RootActivity rootAct = (RootActivity) getActivity();
	if (rootAct != null)
	{
	    rootAct.updateTitle(getResources().getString(R.string.termsandcondition));
	}

	// webview = (WebView) view.findViewById(R.id.webview);
	// Custom WebViewClient
	WebViewClient webViewClient = new WebViewClient()
	{

	    @Override
	    public boolean shouldOverrideUrlLoading(WebView wv, String url)
	    {
		if (url.equals("http://agree.com/"))
		{
		    SignatureFragment signatureFragment = new SignatureFragment();
		    signatureFragment.setOnDismissListener(new DialogInterface.OnDismissListener()
		    {

			@Override
			public void onDismiss(DialogInterface dialog)
			{
			    File sigFile = new File(getSignatureFilePath());
			    if (sigFile.exists())
			    {
				addImgToHtml();
				txtTermsAndCond.loadDataWithBaseURL("", contentHtml, "text/html", "UTF-8", "");
				btnContinue.setVisibility(View.VISIBLE);
			    }
			}

		    });
		    signatureFragment.show(getActivity().getSupportFragmentManager(), "signatureFragment");
		}
		else if (url.equals("http://disagree.com/")) {
		    //getActivity().getSupportFragmentManager().popBackStack();
			UiUtil.showError(getActivity(), getString(R.string.alert_title), getString(R.string.terms_and_conditions_disagree));
		}
		return true;
	    }

	    /*
	     * @Override public void onLoadResource(WebView wv, String url) { if (url.equals("http://agree.com")) { SignatureFragment signatureFragment = new
	     * SignatureFragment(); signatureFragment.show(getActivity().getSupportFragmentManager(), "signatureFragment"); } }
	     */
	};

	txtTermsAndCond = (WebView) view.findViewById(R.id.termsText);
	txtTermsAndCond.setWebViewClient(webViewClient);

	// txtTermsAndCond.setMovementMethod(new ScrollingMovementMethod());
	loadingBar = (ProgressBar) view.findViewById(R.id.loadingBar);
	loadingBar.setVisibility(View.VISIBLE);

	/*
	 * rbYes = (RadioButton) view.findViewById(R.id.yesRadio); rbNo = (RadioButton) view.findViewById(R.id.noRadio); radioGroup = (RadioGroup)
	 * view.findViewById(R.id.radioGroup); radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
	 * 
	 * @Override public void onCheckedChanged(RadioGroup group, int checkedId) { if (rbYes.getId() == checkedId) { rbYes.setChecked(true);
	 * rbNo.setChecked(false); appDetail.setRebateIncentiveDecision(true); } else if (rbNo.getId() == checkedId) { rbYes.setChecked(false);
	 * rbNo.setChecked(true); appDetail.setRebateIncentiveDecision(false); } } });
	 * 
	 * if (appDetail.isRebateIncentiveDecision()) { rbYes.setChecked(true); } else { rbNo.setChecked(true); }
	 */
	btnContinue = (Button) view.findViewById(R.id.btn_continue);
	btnContinue.setOnClickListener(this);
	File sigFile = new File(getSignatureFilePath());
	    if (sigFile.exists())
	    {
		btnContinue.setVisibility(View.VISIBLE);
	    }

	Thread t = new Thread(new Runnable()
	{

	    @Override
	    public void run()
	    {
		byte[] data = FileUtil.getResourceBundleFileContent(LibUtils.getSelectedUtilityCompany().getId() + "_" + AppConstants.TERMS_FILE);
		loadingBar.setVisibility(View.GONE);
		if (data != null)
		{
		    String temp = replaceTokens(new String(data));

		    if (new File(getSignatureFilePath()).exists())
		    {
			temp = getIndiciesOfSigSection(temp);
			contentHtml = temp;
			addImgToHtml();
		    }
		    else
		    {
			
			// Get button graphic bytes
			byte[] agreeBtnBytes = FileUtil.getResourceBundleFileContent(LibUtils.getSelectedUtilityCompany().getId() + "_" + AppConstants.AGREE_BUTTON_FILE_NAME);
			byte[] disagreeBtnBytes = FileUtil.getResourceBundleFileContent(LibUtils.getSelectedUtilityCompany().getId() + "_" + AppConstants.DISAGREE_BUTTON_FILE_NAME);
			
			// Make URI for buttons
			String agreeBtnUri = "data:image/png;base64,"+Base64.encodeToString(agreeBtnBytes, Base64.DEFAULT);
			String disagreeBtnUri = "data:image/png;base64,"+Base64.encodeToString(disagreeBtnBytes, Base64.DEFAULT);
			
			temp = temp.replace(AppConstants.AGREE_BUTTON_FILE_NAME, agreeBtnUri);
			temp = temp.replace(AppConstants.DISAGREE_BUTTON_FILE_NAME, disagreeBtnUri);
			temp = getIndiciesOfSigSection(temp);
			contentHtml = temp;
		    }
		    
		    

		    getActivity().runOnUiThread(new Runnable()
		    {

			@Override
			public void run()
			{
			    txtTermsAndCond.loadDataWithBaseURL("", contentHtml, "text/html", "UTF-8", "");
			    // txtTermsAndCond.setText(Html.fromHtml(content));

			}
		    });
		}
	    }

	    private String getIndiciesOfSigSection(String temp)
	    {
		// Get the positions for the beginning and end of signature section
		String str = "";
		beginSigSection = temp.indexOf(str = getString(R.string.tandc_add_signature_start));
		endSigSection = temp.indexOf(getString(R.string.tandc_add_signature_end)) - str.length();
		
		temp = temp.replace(getString(R.string.tandc_add_signature_start), "");
		temp = temp.replace(getString(R.string.tandc_add_signature_end), "");
		
		return temp;
	    }

	    private String replaceTokens(String string)
	    {
		String[] tokens = getResources().getStringArray(R.array.tandc_items);
		String[] values = getFormValues(tokens.length);
		return UiUtil.replaceEach(string, tokens, values, false, 0);
	    }

	    /**
	     * This gets values related to the tokens that need replacement, order matters i.e. tokens[0] is replaced by values[0]
	     * 
	     * @param size
	     *            - of array
	     * @return String array of values
	     */
	    private String[] getFormValues(int size)
	    {
		String[] values = new String[size];
		FormResponseBean bean = RebateManager.getInstance().getFormBean();
		if (bean != null)
		{

		    ContractorDetails contractorDetails = bean.getContractorDetail();
		    CustomerInfo primaryInfo = bean.getCustomerInfo();
		    AppDetails appDetails = bean.getAppDetails();

		    if (appDetails != null && contractorDetails != null && primaryInfo != null)
		    {

			RebateDifferentPerson premiseInfo = null;
			if (primaryInfo.getDiffPerson() != null)
			{
			    premiseInfo = primaryInfo.getDiffPerson();
			}

			values[0] = contractorDetails.getFirstName();
			values[1] = contractorDetails.getLastName();
			values[2] = contractorDetails.getCompany();
			values[3] = primaryInfo.getCustomerAccNo();
			values[4] = primaryInfo.getCustomerAccNo2();
			values[5] = primaryInfo.getFirstName();
			values[6] = primaryInfo.getLastName();
			if (primaryInfo.getRebateOption() == CustomerInfo.REBATE_CURRENT_BILL)
			{
			    values[7] = primaryInfo.getFirstName();
			    values[8] = primaryInfo.getLastName();
			    values[9] = primaryInfo.getCustomerAddress();
			    values[10] = primaryInfo.getCustomerCity();
			    values[11] = primaryInfo.getCustomerState();
			    values[12] = primaryInfo.getCustomerZipCode();
			    values[13] = primaryInfo.getPhone();
			    values[14] = primaryInfo.getEmailAddress();
			}
			else if(primaryInfo.getRebateOption() == CustomerInfo.REBATE_DIFFERENT_PERSON && premiseInfo != null)
			{
			    values[7] = premiseInfo.getFirstName();
			    values[8] = premiseInfo.getLastName();
			    values[9] = premiseInfo.getAddress();
			    values[10] = premiseInfo.getCity();
			    values[11] = premiseInfo.getState();
			    values[12] = premiseInfo.getZipCode();
			    values[13] = premiseInfo.getPhone();
			    values[14] = premiseInfo.getEmail();
			}
			else if (primaryInfo.getRebateOption() == CustomerInfo.REBATE_UTILITY_COMPANY) {
			    values[7] = contractorDetails.getFirstName();
			    values[8] = contractorDetails.getLastName();
			    values[9] = contractorDetails.getAddress1() + " " + contractorDetails.getAddress2();
			    values[10] = contractorDetails.getCity();
			    values[11] = contractorDetails.getState();
			    values[12] = contractorDetails.getZip();
			    values[13] = contractorDetails.getPhone();
			    values[14] = contractorDetails.getEmail();
			}
			else {
			    //unimpl
			}
			values[15] = getRebateDetails(bean);
			values[16] = "$"+appDetails.getRebateAmountValue();
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			values[17] = sdf.format(System.currentTimeMillis());
		    }
		}
		return values;
	    }

	});
	t.start();

	// webview.setWebViewClient(new WebViewClient()
	// {
	// @Override
	// public void onPageFinished(WebView view, String url)
	// {
	// super.onPageFinished(view, url);
	// loadingBar.setVisibility(View.GONE);
	// }
	//
	// @Override
	// public void onPageStarted(WebView view, String url, Bitmap favicon)
	// {
	// super.onPageStarted(view, url, favicon);
	// loadingBar.setVisibility(View.VISIBLE);
	// }
	// });
	// webview.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
	// // webview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
	// webview.loadUrl("https://dl.dropboxusercontent.com/u/54330564/icf/terms.html");
    }

    /**
     * This method pull info from {@link FormResponseBean} 
     * @param FormResponseBean modeling the details of this this application
     * @return String with all of the items replaced/serviced and itemized rebate amount
     */
    private String getRebateDetails(FormResponseBean bean)
    {
	StringBuilder rebateDetails = new StringBuilder();

	/*---------------------------------------- Equipment ------------------------------------------*/
	
	List<Equipment> equipmentList = bean.getEquipments();
	if (equipmentList != null)
	{
	    for (Iterator<Equipment> itr = equipmentList.iterator(); itr.hasNext();)
	    {
		Equipment equipment = itr.next();
		List<Item> equipmentItems = equipment.getItems();
		if (equipmentItems != null && equipmentItems.size() > 0)
		{
		    for (Item item : equipmentItems)
		    {
			if (item.getRebateValue() > 0)
			{
			    rebateDetails.append("Equipment Replacement - ");
			    rebateDetails.append(equipment.getName());
			    rebateDetails.append(" ");
			    rebateDetails.append(item.getPageNum() + 1); // Start with 1
			    rebateDetails.append(" - $");
			    rebateDetails.append(item.getRebateValue());
			    rebateDetails.append(NL); // \n not working in webview 
			    rebateDetails.append(NL);
			}
		    }
		}
	    }
	}

	/*---------------------------------------- QIV ------------------------------------------*/
	
	
	List<QIVItem> qivList = bean.getQivList();
	if (qivList != null)
	{
	    for (Iterator<QIVItem> itr = qivList.iterator(); itr.hasNext();)
	    {
		QIVItem item = itr.next();
		if (!item.getName().equalsIgnoreCase("Quality Install Verification"))
		{
		    rebateDetails.append("Quality Install Verification - ");
		    rebateDetails.append(item.getName());
		    rebateDetails.append(" - $");
		    rebateDetails.append(item.getRebateValue());
		    rebateDetails.append(NL);
		    rebateDetails.append(NL);
		}
	    }
	}

	/*-------------------------------------- Tune up ----------------------------------------*/
	
	List<TuneUp> tuneUpList = bean.getTuneUp();
	if (tuneUpList != null)
	{
	    for (Iterator<TuneUp> itr = tuneUpList.iterator(); itr.hasNext();)
	    {
		TuneUp tuneUp = itr.next();
		List<TuneUpItem> tuneUpItems = tuneUp.getItems();
		if (tuneUpItems != null && tuneUpItems.size() > 0)
		{
		    for (TuneUpItem item : tuneUpItems)
		    {
			if (item.getRebateValue() > 0)
			{
			    rebateDetails.append("Tune Up - ");
			    rebateDetails.append(tuneUp.getName());
			    rebateDetails.append(" ");
			    rebateDetails.append(item.getPageNum() + 1); // Start with 1
			    rebateDetails.append(" - $");
			    rebateDetails.append(item.getRebateValue());
			    rebateDetails.append(NL); // \n not working in webview 
			    rebateDetails.append(NL);
			}
		    }
		}
	    }
	}
    
    	/*----------------------------------- Window and Door -----------------------------------*/
	
	List<WindowAndDoor> wdList = bean.getWindowAndDoor();
	if (wdList != null)
	{
	    for (Iterator<WindowAndDoor> itr = wdList.iterator(); itr.hasNext();)
	    {
		WindowAndDoor wd = itr.next();
		List<WDItem> WDItems = wd.getItems();
		if (WDItems != null && WDItems.size() > 0)
		{
		    for (WDItem item : WDItems)
		    {
			if (item.getRebateValue() > 0)
			{
			    rebateDetails.append("Window and Door - ");
			    rebateDetails.append(wd.getName());
			    rebateDetails.append(" ");
			    rebateDetails.append(item.getPageNum() + 1); // Start with 1
			    rebateDetails.append(" - $");
			    rebateDetails.append(item.getRebateValue());
			    rebateDetails.append(NL); // \n not working in webview 
			    rebateDetails.append(NL);
			}
		    }
		}
	    }
	}
	
	/*------------------------------------- Insulation --------------------------------------*/
	
	List<Insulation> insulationList = bean.getInsulation();
	if (insulationList != null)
	{
	    for (Iterator<Insulation> itr = insulationList.iterator(); itr.hasNext();)
	    {
		Insulation insulation = itr.next();
		List<InsulationItem> insulationItems = insulation.getItems();
		if (insulationItems != null && insulationItems.size() > 0)
		{
		    for (InsulationItem item : insulationItems)
		    {
			if (item.getRebateValue() > 0)
			{
			    rebateDetails.append("Insulation - ");
			    rebateDetails.append(insulation.getName());
			    rebateDetails.append(" ");
			    rebateDetails.append(item.getPageNum() + 1); // Start with 1
			    rebateDetails.append(" - $");
			    rebateDetails.append(item.getRebateValue());
			    rebateDetails.append(NL); // \n not working in webview 
			    rebateDetails.append(NL);
			}
		    }
		}
	    }
	}
	
	/*------------------------------------- Duct Sealing ------------------------------------*/
	
	List<DuctSealing> ductSealingList = bean.getDuctSealing();
	if (ductSealingList != null) {
		for (DuctSealing ductSealing : ductSealingList) {
			List<DuctSealingItem> itemList = (List<DuctSealingItem>)ductSealing.getItems();
			if (itemList != null) {
				for (DuctSealingItem item : itemList) {
					if (item.getRebateValue() > 0) {
						rebateDetails.append("Duct Sealing - ");
						rebateDetails.append(ductSealing.getName());
						rebateDetails.append(" ");
						rebateDetails.append(item.getPageNum() + 1); // Start with 1
						rebateDetails.append(" - $");
						rebateDetails.append(item.getRebateValue());
						rebateDetails.append(NL); // \n not working in webview
						rebateDetails.append(NL);
					}
				}
			}
	    }
	}
	
	/*--------------------------------- Thermostat Install ----------------------------------*/
	
	List<ThermostatInstallItem> thermostatInstallList = bean.getThermostatInstall();
	if (thermostatInstallList != null) {
	    for (Iterator<ThermostatInstallItem> itr = thermostatInstallList.iterator(); itr.hasNext();) {
		ThermostatInstallItem item = itr.next();
		if (item.getRebateValue() > 0)
		{
		    rebateDetails.append("Thermostat Install - ");
		    rebateDetails.append(item.getName());
		    rebateDetails.append(" ");
		    rebateDetails.append(item.getPageNum() + 1); // Start with 1
		    rebateDetails.append(" - $");
		    rebateDetails.append(item.getRebateValue());
		    rebateDetails.append(NL); // \n not working in webview 
		    rebateDetails.append(NL);
		}
	    }
	}

	return rebateDetails.toString();
    }

    private void addImgToHtml()
    {
	if (contentHtml != null)
	{
	    StringBuilder sb = new StringBuilder();
	    sb.append(contentHtml.substring(0, beginSigSection));
	    // Append signature image
	    sb.append("<img src=\"");
	    sb.append(Uri.fromFile(new File(getSignatureFilePath())).toString());
	    sb.append("\" height=\"200\" width=\"300\"></img>");
	    sb.append(endSigSection < contentHtml.length() ? contentHtml.substring(endSigSection) : "</body></html>");
	    contentHtml = sb.toString();
	}
    }

    @Override
    public void onClick(View v)
    {
	int id = v.getId();
	if (id == R.id.btn_continue)
	{
	    saveWebviewBitmap();
	    RebateConfirmationFragment rebateConfirmationFragment = new RebateConfirmationFragment();
	    ((RootActivity) getActivity()).beginTransaction().replace(R.id.mainLayout, rebateConfirmationFragment).addToBackStack(null).commit();
	}
    }

    private void saveWebviewBitmap()
    {
//	final Bitmap b = Bitmap.createBitmap(((MeasurableWebView) txtTermsAndCond).getContentWidth(),
//										 ((MeasurableWebView) txtTermsAndCond).getContentHeight(),
//			                              Bitmap.Config.ARGB_8888);
//
//	Canvas c = new Canvas(b);
//	txtTermsAndCond.draw(c);

	txtTermsAndCond.measure(View.MeasureSpec.makeMeasureSpec(
			View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED),
			View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
	txtTermsAndCond.layout(0, 0, txtTermsAndCond.getMeasuredWidth(),
			txtTermsAndCond.getMeasuredHeight());
	txtTermsAndCond.setDrawingCacheEnabled(true);
	txtTermsAndCond.buildDrawingCache();
	Bitmap b = Bitmap.createBitmap(txtTermsAndCond.getMeasuredWidth(),
			txtTermsAndCond.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

	Canvas bigcanvas = new Canvas(b);
	Paint paint = new Paint();
	int iHeight = b.getHeight();
	bigcanvas.drawBitmap(b, 0, iHeight, paint);
	txtTermsAndCond.draw(bigcanvas);

//  //Offload this
//	new Thread(new Runnable()
//	{
//
//	    @Override
//	    public void run()
//	    {
		
		FileOutputStream fos = null;
		try
		{
			String target = FileUtil.getJobDirectory(jobId) + "/TermsAndConditions.jpg";
//			txtTermsAndCond.saveWebArchive(target); no good
		    fos = new FileOutputStream(FileUtil.getJobDirectory(jobId) + "/TermsAndConditions.jpg");
			if (fos != null) {
				b.compress(Bitmap.CompressFormat.JPEG, 0, fos); // Max compression
				fos.close();
			}
			Log.i(TAG, "WebView bitmap saved.");
		}
		catch (Exception e)
		{
		    Log.e(TAG, "Error occured while saving WebView bitmap");
		    e.printStackTrace();
		}
//	    }
//	}).start();
    }

    private String getSignatureFilePath()
    {
	return FileUtil.getJobDirectory(jobId) + "/signature.png";
    }
}
