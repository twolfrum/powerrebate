package com.icf.rebate.ui.fragment;

import java.util.ArrayList;
import java.util.List;

import com.crashlytics.android.Crashlytics;
import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.adapter.SealingTypeAdapter;
import com.icf.rebate.app.model.SealingType;
import com.icf.rebate.networklayer.model.FormResponseBean;
import com.icf.rebate.ui.RootActivity;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.listeners.FragmentCallFlow;
import com.icf.rebate.ui.listeners.RequestCallFlow;
import com.icf.rebate.ui.util.AppConstants;

import android.support.v4.app.Fragment;
import android.util.Log;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import io.fabric.sdk.android.Fabric;

/**
 * Created by 19713 on 2/8/2018.
 */

public class SealingTypeListFragment extends Fragment implements OnItemClickListener,
        RequestCallFlow, FragmentCallFlow {


    public static String TAG = SealingTypeListFragment.class.getSimpleName();

    private View rootView;
    private ListView mListView;
    private SealingTypeAdapter mLVAdapter;
    private String mTitle;
    private String mSealingType;
    List<? extends SealingType> mSealingTypeList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        rootView = inflater.inflate(R.layout.fragment_air_duct_sealing_list, container, false);
        mListView = (ListView) rootView.findViewById(R.id.lv_air_duct_types);
        // Set title
        RootActivity rootActivity = null;
        try
        {
            rootActivity = (RootActivity) getActivity();
        }
        catch (ClassCastException e)
        {
            throw new ClassCastException("This fragment must be attached to RootActivity");
        }

        rootActivity.setTitle(mTitle);

        bindViewListeners();
        requestData();
        return rootView;
    }

    private void bindViewListeners()
    {
        mListView.setOnItemClickListener(this);
    }

    private void updateTuneupList(FormResponseBean bean)
    {
        if (bean == null)
        {
            return;
        }

        if (mSealingType.equals(getString(R.string.ductsealingId))) mSealingTypeList = bean.getDuctSealing();
        if (mSealingType.equals(getString(R.string.airsealingId))) mSealingTypeList = bean.getAirSealing();

        if (mSealingTypeList == null)
        {
            mSealingTypeList = new ArrayList<>();
        }

        mLVAdapter = new SealingTypeAdapter(getActivity(), 0, (List<SealingType>)mSealingTypeList);
        mLVAdapter.setAdapterListener(mLVAdapter);
        mListView.setAdapter(mLVAdapter);
    }

    /*--------------------------------------------Field access----------------------------------------------------*/

    public String getTitle()
    {
        return mTitle;
    }

    public void setTitle(String title)
    {
        this.mTitle = title;
    }

    public String getSealingType()
    {
        return mSealingType;
    }

    public void setSealingType(String sealingType)
    {
        this.mSealingType = sealingType;
    }

    /*---------------------------------------Implemented interface methods-------------------------------------*/

    @Override
    public void requestData()
    {
        RebateManager.getInstance().checkAndrequestData(this);
    }

    @Override
    public void getUiControls(View root)
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void initialize(View root)
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void bindCallbacks(View root)
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void showProgressDialog()
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void stopProgressDialog()
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void updateData(Object obj)
    {
        updateTuneupList((FormResponseBean) obj);
    }

    @Override
    public void displayError(String title, String message)
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        // Add crashylitcs for equipment type
        if (mSealingTypeList != null && mSealingTypeList.size() > position && Fabric.isInitialized())
        {
            String str = "";
            Crashlytics.setString("selectedEquipmentName",
                    str = (mSealingTypeList.get(position) == null ?
                        "null" : mSealingTypeList.get(position).getName()));
            Log.d(TAG, "Setting crashlytics custom tag selectedEquipmentName=" + str);
        }

        Fragment fragment = null;
        if (mSealingType.equals(getString(R.string.ductsealingId))) {
            fragment = new DuctSealingPagerFragment();
            ((DuctSealingPagerFragment)fragment).setTitle(mSealingTypeList.get(position).getName());
        }
        if (mSealingType.equals(getString(R.string.airsealingId))) {
            fragment = new AirSealingPagerFragment();
            ((AirSealingPagerFragment)fragment).setTitle(mSealingTypeList.get(position).getName());
        }

        Bundle args = new Bundle();
        args.putInt(AppConstants.SUBTYPE_SELECT_POSITION_KEY, position);
        fragment.setArguments(args);
        ((RootActivity) getActivity()).beginTransaction().replace(R.id.mainLayout, fragment).addToBackStack(null).commit();
    }

    /*------------------------------------------------------------------------------------------------------------*/
}

