package com.icf.rebate.ui.fragment;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.networklayer.model.CustomerInfo;
import com.icf.rebate.networklayer.model.FormResponseBean;
import com.icf.rebate.networklayer.model.FormResponseBean.Insulation;
import com.icf.rebate.networklayer.model.FormResponseBean.InsulationItem;
import com.icf.rebate.networklayer.model.FormResponseBean.Item;
import com.icf.rebate.networklayer.model.FormResponseBean.Options;
import com.icf.rebate.networklayer.model.FormResponseBean.Part;
import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.rebate.ui.FormFragment;
import com.icf.rebate.ui.controller.FormUIManager;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.controller.FormUIManager.FormViewCreationCallback;
import com.icf.rebate.ui.listeners.FragmentCallFlow;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.ICFLogger;
import com.icf.rebate.ui.util.UiUtil;
import com.icf.rebate.ui.util.UiViewUtils;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

/**
 * @author Ken Butler Dec 3, 2015 InsulationPageFragment.java Copyright (c) 2015 ICF International
 */

public class InsulationPageFragment extends Fragment implements OnCheckedChangeListener, FragmentCallFlow, FormViewCreationCallback {

    public final String TAG = InsulationPageFragment.class.getSimpleName();
    private InsulationItem mItem;
    private LinearLayout mScrollViewLayout;
    private FormUIManager mManager;
    private Insulation mInsulation;
    private boolean isDeleted;
    private String title;
    private EditText mResult, mEdtExistingRVal, mEdtFinalRVal, mExistingThickness,
            mInstalledThickness, mEdtSqFootage, mEdtProjectCost, mEdtQuantity;
    private Spinner mSpinExisting, mSpinInstalled;
    private Map<String, String> mExistingInsulOptionsMap, mInstalledInslOptionsMap;
    private int mExistingRVal = -1;
    private int mFinalRVal = -1;
    private int mSqFootage = -1;
    private float mProjectCost = 0f;
    private int mQuantity = 0;

    public InsulationPageFragment() {
        super();
    }

    public static InsulationPageFragment newInstance(InsulationItem item) {
        if (item == null) {
            throw new IllegalArgumentException("item cannot be null");
        }

        InsulationPageFragment fragment = new InsulationPageFragment();
        fragment.setItem(item);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_page_insulation, null, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getUiControls(view);
        initialize(view);
        bindCallbacks(view);
        requestData();

        CustomerInfo cInfo = RebateManager.getInstance().getFormBean().getCustomerInfo();
        if (UiUtil.isJobCompleted(cInfo)) {
            UiUtil.setEnableView((ViewGroup) view, false, null);
        }
    }

    @Override
    public void requestData() {
        // TODO Auto-generated method stub

    }

    @Override
    public void viewCreated(View view, final Part part, final Options option) {

        String name = null;

        if (part != null) {
            name = part.getId();
            if (name != null) {
                if (name.equalsIgnoreCase(AppConstants.INSULATION_RESULT_ID)) {
                    if (view instanceof EditText) {
                        mResult = (EditText) view;
                        try {
                            mItem.setStaticRebateValue(
                                    ((Map<String, String>) mResult.getTag()).get("staticDisplayedValue"));
                        } catch (Exception e) {
                        }

                    }
                } else if (name.equalsIgnoreCase("existingInsulationRValue")) {
                    mEdtExistingRVal = (EditText) view;
                    if (option != null && option.getSavedValue() != null) {
                        mExistingRVal = LibUtils.parseInteger(option.getSavedValue(), -1);
                    }
                } else if (name.equalsIgnoreCase("finalInsulationRValue")) {
                    mEdtFinalRVal = (EditText) view;
                    if (option != null && option.getSavedValue() != null) {
                        mFinalRVal = LibUtils.parseInteger(option.getSavedValue(), -1);
                    }
                } else if (name.equalsIgnoreCase("insulationSquareFootage")) {
                    mEdtSqFootage = (EditText) view;
                    if (option != null && option.getSavedValue() != null) {
                        mSqFootage = LibUtils.parseInteger(option.getSavedValue(), -1);
                    }
                    mEdtSqFootage.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void afterTextChanged(Editable s) {}
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            setSqFootage(LibUtils.parseInteger(s.toString(), -1));
                        }
                    });
                } else if (name.equalsIgnoreCase("existInsulationType") || name.equals("existingInsulationType")) {
                    Log.d(TAG, option.toString());
                    // Map vals to data see json
                    if (option != null) {
                        mExistingInsulOptionsMap = mapValToData(option.getValues(), option.getData());
                        mSpinExisting = (Spinner) view;
                        mSpinExisting.setOnItemSelectedListener(new OnItemSelectedListener() {

                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                if (parent != null && parent.getRootView() != null) {
                                    parent.getRootView().clearFocus();
                                }
                                String seletedValue = null;

                                if (view instanceof TextView) {
                                    seletedValue = ((TextView) view).getText().toString().trim();
                                    if (!seletedValue.equalsIgnoreCase(AppConstants.COMBO_SELECT_VALUE)) {
                                        option.setSavedValue(seletedValue);
                                    } else {
                                        option.setSavedValue(null);
                                    }

                                    if (UiViewUtils.isValidationMode(option.isItemSaved()) && option.isMandatory() && !option.isFieldFilled()) {
                                        ((View) view.getParent()).setBackground(getActivity().getResources().getDrawable(R.drawable.bg_dialog_mandatory_box));
                                    } else {
                                        ((View) view.getParent()).setBackground(getActivity().getResources().getDrawable(R.drawable.combo_box_bg));
                                    }
                                }

                                if (mEdtExistingRVal != null && mExistingThickness != null && !mExistingThickness.getText().toString().equals("")) {
                                    String selectedValue = parent.getSelectedItem().toString();
                                    if (!selectedValue.equalsIgnoreCase(AppConstants.COMBO_SELECT_VALUE)) {
                                        updateExistingRVal(String.format("%.02f", LibUtils.parseDouble(mExistingInsulOptionsMap.get(selectedValue), 0) * LibUtils.parseDouble(mExistingThickness.getText().toString(), 0)));
                                    }
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> arg0) {
                                // Don't care do nothing
                            }
                        });
                        // Style the spinner, setting an ItemClickHandler strips this bg
                        if (UiViewUtils.isValidationMode(option.isItemSaved()) && option.isMandatory() && !option.isFieldFilled()) {
                            view.setBackground(getActivity().getResources().getDrawable(R.drawable.bg_dialog_mandatory_box));
                        } else {
                            view.setBackground(getActivity().getResources().getDrawable(R.drawable.combo_box_bg));
                        }
                    }
                } else if (name.equalsIgnoreCase("installedInsulationType")) {
                    if (option != null) {
                        mInstalledInslOptionsMap = mapValToData(option.getValues(), option.getData());
                        mSpinInstalled = (Spinner) view;
                        mSpinInstalled.setOnItemSelectedListener(new OnItemSelectedListener() {

                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                if (parent != null && parent.getRootView() != null) {
                                    parent.getRootView().clearFocus();
                                }
                                String seletedValue = null;

                                if (view instanceof TextView) {
                                    seletedValue = ((TextView) view).getText().toString().trim();
                                    if (!seletedValue.equalsIgnoreCase(AppConstants.COMBO_SELECT_VALUE)) {
                                        option.setSavedValue(seletedValue);
                                    } else {
                                        option.setSavedValue(null);
                                    }

                                    if (UiViewUtils.isValidationMode(option.isItemSaved()) && option.isMandatory() && !option.isFieldFilled()) {
                                        ((View) view.getParent()).setBackground(getActivity().getResources().getDrawable(R.drawable.bg_dialog_mandatory_box));
                                    } else {
                                        ((View) view.getParent()).setBackground(getActivity().getResources().getDrawable(R.drawable.combo_box_bg));
                                    }
                                }

                                if (mEdtFinalRVal != null && mInstalledThickness != null && !mInstalledThickness.getText().toString().equals("")) {
                                    String selectedValue = parent.getSelectedItem().toString();
                                    if (!selectedValue.equalsIgnoreCase(AppConstants.COMBO_SELECT_VALUE)) {
                                        updateInstalledRVal(String.format("%.02f", LibUtils.parseDouble(mInstalledInslOptionsMap.get(selectedValue), 0) * LibUtils.parseDouble(mInstalledThickness.getText().toString(), 0)));
                                    }
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> arg0) {
                                // Don't care do nothing
                            }
                        });

                        if (UiViewUtils.isValidationMode(option.isItemSaved()) && option.isMandatory() && !option.isFieldFilled()) {
                            view.setBackground(getActivity().getResources().getDrawable(R.drawable.bg_dialog_mandatory_box));
                        } else {
                            view.setBackground(getActivity().getResources().getDrawable(R.drawable.combo_box_bg));
                        }
                    }
                } else if (name.equalsIgnoreCase("existingInsulationThickness")) {
                    mExistingThickness = (EditText) view;
                    mExistingThickness.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {}
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                        @Override
                        public void afterTextChanged(Editable s) {
                            mExistingThickness.getText().toString().trim();
                            if (s.length() > 0 && mSpinExisting != null && !mSpinExisting.getSelectedItem().toString().equals(AppConstants.COMBO_SELECT_VALUE)
                                    && mEdtExistingRVal != null && mExistingInsulOptionsMap != null) {
                                updateExistingRVal(String.format("%.02f", LibUtils.parseDouble(mExistingInsulOptionsMap.get(mSpinExisting.getSelectedItem().toString()), 0) * LibUtils.parseDouble(mExistingThickness.getText().toString(), 0)));
                            } else {
                                updateExistingRVal("");
                            }
                        }
                    });
                } else if (name.equalsIgnoreCase("installedInsulationThickness")) {
                    mInstalledThickness = (EditText) view;
                    mInstalledThickness.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {}
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                        @Override
                        public void afterTextChanged(Editable s) {
                            mInstalledThickness.getText().toString().trim();
                            if (s.length() > 0 && mSpinInstalled != null && !mSpinInstalled.getSelectedItem().toString().equals(AppConstants.COMBO_SELECT_VALUE)
                                    && mEdtFinalRVal != null && mInstalledInslOptionsMap != null) {
                                updateInstalledRVal(String.format("%.02f", LibUtils.parseDouble(mInstalledInslOptionsMap.get(mSpinInstalled.getSelectedItem().toString()), 0) * LibUtils.parseDouble(mInstalledThickness.getText().toString(), 0)));
                            } else {
                                updateInstalledRVal("");
                            }
                        }
                    });
                } else if (name.equalsIgnoreCase(AppConstants.PROJECT_COST)) {
                    mEdtProjectCost = (EditText) view;
                    if (option != null && option.getSavedValue() != null) {
                        mProjectCost = LibUtils.parseFloat(option.getSavedValue(), 0f);
                    }
                    mEdtProjectCost.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void afterTextChanged(Editable s) {}
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            setProjectCost(LibUtils.parseFloat(option.getSavedValue(), 0f));
                        }
                    });
                } else if (name.equalsIgnoreCase(AppConstants.QUANTITY)) {
                    mEdtQuantity = (EditText) view;
                    if (option != null && option.getSavedValue() != null) {
                        mQuantity = LibUtils.parseInteger(option.getSavedValue(), 0);
                    }
                    mEdtQuantity.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void afterTextChanged(Editable s) {}
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            setQuantity(LibUtils.parseInteger(option.getSavedValue(), 0));
                        }
                    });
                }
            }
        }
    }


    private void updateExistingRVal(String val) {
        mEdtExistingRVal.setText(val);
        mExistingRVal = (int) Math.ceil(LibUtils.parseDouble(val, -1));
    }

    private void updateInstalledRVal(String val) {
        mEdtFinalRVal.setText(val);
        mFinalRVal = (int) Math.floor(LibUtils.parseDouble(val, -1));
    }

    private Map<String, String> mapValToData(List<String> values, List<String> data) {
        if (values == null || data == null)
            return null;
        Map<String, String> map = new HashMap<String, String>();
        for (int i = 0; i < values.size(); i++) {
            if (i < data.size()) {
                map.put(values.get(i), data.get(i));
            }
        }
        return map;
    }


    @Override
    public void onDestroyView() {
        UiUtil.cleanBitmap(getView());
        saveForm();
        resetFormViews();

        super.onDestroyView();
    }


    public void saveForm() {
        if (!isDeleted) {
            List<InsulationItem> items = ((InsulationFormFragment) getParentFragment()).getInsulationItems();
            if (items != null) {
                int index = items.indexOf(mItem);
                if (index > -1) {
                    ICFLogger.d(TAG, "onDestroyView " + index);
                    ((InsulationFormFragment) getParentFragment()).saveForm(this, index, false);
                }
            }
        }
    }

    private void resetFormViews() {
        mResult = null;
    }

    @Override
    public void getUiControls(View root) {
        mScrollViewLayout = (LinearLayout) root.findViewById(R.id.scrol_layout);

    }

    @Override
    public void initialize(View root) {
        mManager = new FormUIManager(getString(R.string.insulationId));
        mManager.setFormViewCreationCallback(this);
        mManager.createViews(getParentFragment(), mScrollViewLayout, mItem.getInsulationDetail().getParts(), mItem.isItemSaved());
        mManager.setEquipmentName(mInsulation.getId());
    }

    @Override
    public void bindCallbacks(View root) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
        // TODO Auto-generated method stub

    }

    public EditText getLastFocusedEditField() {
        return mManager.getLastFocusedEditField();
    }

    public void updateCaptureImage(int pageIndex) {
        UiUtil.updateCaptureImage(mManager, pageIndex + "", getActivity());
    }

    public void updateRebate(float rebateValue) {
        if (mResult != null) {
            String formatedValue = mItem.getStaticRebateValue();
            if (TextUtils.isEmpty(formatedValue)) {
                formatedValue = (String.format("%.02f", rebateValue));
            }
            //$$$TBD SHIT
            mResult.setText(formatedValue + "");
            ICFLogger.d(TAG, "Result:" + "$" + formatedValue + "");
        }
    }

    /*
     * public void updateNumericFields() { if (mExistingRVal != null && mFinalRVal != null) { Object existingR = mExistingRVal.getTag(); Object finalR =
     * mExistingRVal.getTag();
     * 
     * if (existingR != null && existingR instanceof Part) { Part part = (Part) existingR; List<Options> options = part.getOptions(); if (options != null &&
     * options.size() > 0) { Options option = options.get(0); // Just get 0 here only one value in JSON list // mExistingRVal.setText(option.get); } } if
     * (finalR != null && finalR instanceof Part) { Part part = (Part) finalR; } } }
     */

    public InsulationItem getItem() {
        return mItem;
    }

    public void setItem(InsulationItem item) {
        this.mItem = item;
    }

    public Insulation getInsulation() {
        return mInsulation;
    }

    public void setInsulation(Insulation mInsulation) {
        this.mInsulation = mInsulation;
    }

    public String getTitleId() {
        return title;
    }

    public void setTitleId(String title) {
        this.title = title;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setFragmentDeleted() {
        this.isDeleted = true;
    }

    public int getExistingRVal() {
        return mExistingRVal;
    }

    public void setmExistingRVal(int mExistingRVal) {
        this.mExistingRVal = mExistingRVal;
    }

    public int getFinalRVal() {
        return mFinalRVal;
    }

    public void setmFinalRVal(int mFinalRVal) {
        this.mFinalRVal = mFinalRVal;
    }

    public int getSqFootage() {
        return mSqFootage;
    }

    public void setSqFootage(int mSqFootage) {
        this.mSqFootage = mSqFootage;
    }

    public float getProjectCost() {
        return mProjectCost;
    }

    public void setProjectCost(float mProjectCost) {
        this.mProjectCost = mProjectCost;
    }

    public int getQuantity() {
        return mQuantity;
    }

    public void setQuantity(int mQuantity) {
        this.mQuantity = mQuantity;
    }
}
