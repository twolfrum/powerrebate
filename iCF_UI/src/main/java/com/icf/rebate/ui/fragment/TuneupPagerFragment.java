package com.icf.rebate.ui.fragment;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.icf.rebate.adapter.CustomSpinnerAdapter;
import com.icf.rebate.adapter.TuneUpAdapter;
import com.icf.rebate.app.model.DashboardItem.ServiceItem;
import com.icf.rebate.networklayer.model.CustomerInfo;
import com.icf.rebate.networklayer.model.FormResponseBean;
import com.icf.rebate.networklayer.model.FormResponseBean.Options;
import com.icf.rebate.networklayer.model.FormResponseBean.TuneUp;
import com.icf.rebate.networklayer.model.FormResponseBean.TuneUpItem;
import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.ui.RootActivity;
import com.icf.rebate.ui.controller.FormUIManager;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.customviews.CustomPageIndicator;
import com.icf.rebate.ui.customviews.CustomViewPager;
import com.icf.rebate.ui.dialog.DialogListener;
import com.icf.rebate.ui.dialog.ICFDialogFragment;
import com.icf.rebate.ui.listeners.FragmentCallFlow;
import com.icf.rebate.ui.listeners.RequestCallFlow;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.FileUtil;
import com.icf.rebate.ui.util.UiUtil;

public class TuneupPagerFragment extends Fragment
	implements FragmentCallFlow, RequestCallFlow, OnItemSelectedListener, OnClickListener, DialogListener, OnPageChangeListener, PersistentForm
{
    // private RootActivity homeScreenactivity;
    private TuneUpAdapter tuneUpAdapter;
    private Spinner tuneUpComboBox;
    // private List<TuneUp> tuneUpList;
    private TuneUp tuneUp;
    private List<TuneUpItem> tuneUpList;
    public CustomViewPager viewPager;
    private FormUIManager uiManager;
    private int pageIndex = 0;
    private ICFDialogFragment commonDialog;
    private String title;
    private CustomSpinnerAdapter equipmentAdapter;
    private List<String> spinnerList = new ArrayList<>();
    private int mSubtypeSelection; /* The index of the tuneup "type" from the previous fragments list view */

    public TuneupPagerFragment()
    {
	super();
    }

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null) {
			mSubtypeSelection = getArguments().getInt(AppConstants.SUBTYPE_SELECT_POSITION_KEY);
		}

	}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
	View view = inflater.inflate(R.layout.form_screen_layout, container, false);
	return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
	super.onViewCreated(view, savedInstanceState);

	getUiControls(view);
	initialize(view);

	bindCallbacks(view);
	requestData();

	/*float currentRebateForType = getRebate();
	if (RebateManager.getInstance().getFormBean() != null)
	{
	    float totalRebate = RebateManager.getInstance().getFormBean().getTuneUpRebate();
	    RebateManager.getInstance().getFormBean().setTuneUpRebate(totalRebate - currentRebateForType);
	}*/

	CustomerInfo cInfo = RebateManager.getInstance().getFormBean().getCustomerInfo();
	if (UiUtil.isJobCompleted(cInfo))
	{
	    ArrayList<View> list = new ArrayList<View>();
	    list.add(viewPager);
	    UiUtil.setEnableView((ViewGroup) view, false, list);
	}
    }

    @Override
    public void requestData()
    {
	RebateManager.getInstance().checkAndrequestData(this);
    }

    @Override
    public void getUiControls(View root)
    {
	tuneUpComboBox = (Spinner) root.findViewById(R.id.equipment_selector);
	viewPager = (CustomViewPager) root.findViewById(R.id.pager);

    }

    @Override
    public void initialize(View root)
    {
	RootActivity homeScreenactivity = (RootActivity) getActivity();
	uiManager = new FormUIManager("TuneupPager");
	if (homeScreenactivity == null)
	{
	    return;
	}

	RootActivity rootAct = (RootActivity) getActivity();
	if (rootAct != null)
	{
	    rootAct.updateTitle(title);
	}

	tuneUpAdapter = new TuneUpAdapter(getChildFragmentManager(), viewPager);
	viewPager.setAdapter(tuneUpAdapter);
    }

    private void updateUI()
    {
	if (tuneUpList != null)
	{
	    updateSpinnerAdapter();
	    tuneUpAdapter.setTuneUpList(tuneUpList);
	    tuneUpAdapter.setTuneUp(tuneUp);
	    tuneUpAdapter.notifyDataSetChanged();
	    viewPager.setOffscreenPageLimit(tuneUpList.size());
	    CustomPageIndicator indicator = ((CustomPageIndicator) getActivity().findViewById(R.id.page_indicator));
	    viewPager.setPageIndicator(indicator);
	    indicator.setPager(viewPager);

	}
    }

    private void updateSpinnerAdapter()
    {
	ListIterator<TuneUpItem> iterator = tuneUpList.listIterator();
	int index = 1;
	spinnerList.clear();
	while (iterator.hasNext())
	{
	    iterator.next();
	    spinnerList.add("Equipment " + index++);
	}
	if (spinnerList.size() == 0)
	{
	    spinnerList.add("Equipment " + index++);
	}

	equipmentAdapter = new CustomSpinnerAdapter(getActivity(), 0, spinnerList);
	tuneUpComboBox.setAdapter(equipmentAdapter);
	tuneUpComboBox.setSelection(pageIndex);
    }

    @Override
    public void bindCallbacks(View root)
    {
	tuneUpComboBox.setOnItemSelectedListener(this);
	root.findViewById(R.id.save_btn).setOnClickListener(this);
	root.findViewById(R.id.close_btn).setOnClickListener(this);
	root.findViewById(R.id.add_btn).setOnClickListener(this);
	viewPager.setOnPageChangeListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
	if (viewPager.getCurrentPage() != position)
	{
	    viewPager.setCurrentItem(position, true);
	}
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {

    }

    @Override
    public void onDestroyView()
    {
	/*float rebateValue = getRebate();
	if (RebateManager.getInstance().getFormBean() != null)
	{
	    float prevAmmount = RebateManager.getInstance().getFormBean().getTuneUpRebate();
	    RebateManager.getInstance().getFormBean().setTuneUpRebate(rebateValue + prevAmmount);
	}*/

 	super.onDestroyView();
    }

    // private float computeRebate()
    // {
    // int count = tuneUpAdapter.getCount();
    // int eligibleCount = 0;
    // for (int i = 0; i < count; i++)
    // {
    // eligibleCount += tuneUpList.get(i).isEligibile() ? 1 : 0;
    // }
    // return (float) (eligibleCount * rebateTable.getIncentive());
    // }

    private float getRebate()
    {
	float rebateVal = 0;
	// int count = tuneUpAdapter.getCount();
	int count = tuneUpList.size();
	for (int i = 0; i < count; i++)
	{
	   TuneUpItem item = tuneUpList.get(i);
	    rebateVal += item.getRebateValue();
	}
	return (float) rebateVal;
    }

    @Override
    public void onClick(View v)
    {
	if (v != null)
	{
	    UiUtil.hideSoftKeyboard(getActivity(), uiManager.getLastFocusedEditField());

	    int id = v.getId();
	    if (id == R.id.save_btn)
	    {
		saveForm(pageIndex, true);
	    }
	    else if (id == R.id.close_btn)
	    {
		if (viewPager.getPageCount() > 0)
		{
		    commonDialog = ICFDialogFragment.newDialogFrag(R.layout.delete_confirmation);
		    commonDialog.setDialogListener(this);
		    commonDialog.show(getActivity().getSupportFragmentManager(), "Delete Confirmation");
		    getActivity().getSupportFragmentManager().executePendingTransactions();
		}
	    }
	    else if (id == R.id.add_btn)
	    {
		if (tuneUpList != null && tuneUpList.size() < tuneUp.getMaxMeasures()) //AppConstants.MAX_PAGE_SUPPORT)
		{
		    TuneUpItem item = (TuneUpItem) tuneUpList.get(0).createObject();
		    item.setPageNum(tuneUpList.get(tuneUpList.size() - 1).getPageNum() + 1);

		    tuneUpList.add(item);
		    tuneUpAdapter.setTuneUpList(tuneUpList);
		    tuneUpAdapter.setTuneUp(tuneUp);
		    tuneUpAdapter.notifyDataSetChanged();
		    viewPager.setOffscreenPageLimit(tuneUpList.size());
		    viewPager.firePageCountChanged();
		    viewPager.setCurrentItem(tuneUpList.size() - 1);
		    updateSpinnerAdapter();
		}
		else if (tuneUpList.size() == tuneUp.getMaxMeasures())
		{
			UiUtil.showError(getActivity(), getResources().getString(R.string.alert_title),
					getResources().getString(R.string.maximum_measures_limit,
							tuneUp.getName(), tuneUp.getMaxMeasures()));
		}
	    }
	}

    }
    
    @Override
    public int getNoScreensToToolList()
    {
	return 2;
    }

    @Override
    public void saveForm(boolean isManualSave)
    {
	saveForm(pageIndex, isManualSave);
    }

    @Override
    public void setValidState()
    {
	Iterator<TuneUpItem> itemsIterator = tuneUpList.listIterator();
	    while (itemsIterator.hasNext())
	    {
		if (tuneUp == null)
		{
		    break;
		}
		TuneUpItem item = (TuneUpItem) itemsIterator.next();
		if (item.isItemSaved())
		{
		    if (item.getFormFilledState() == AppConstants.FORM_ITEM_FILLED_STATE.ITEM_COMPLETELY_SAVED.getState())
		    {
			tuneUp.setMandatoryFieldsComplete(true);
		    }
		    else
		    {
			tuneUp.setMandatoryFieldsComplete(false);
		    }
		}
		else
		{
		    tuneUp.setMandatoryFieldsComplete(true);
		}
	    }
    }

    @Override
    public boolean isValid()
    {
	setValidState();
	return tuneUp.isMandatoryFieldsComplete();
    }

    public synchronized void saveForm(int pageIndex, boolean manualSave)
    {
	if (tuneUpAdapter != null)
	{
	    boolean isItemSaved = tuneUpAdapter.setItemSaved(pageIndex, manualSave);

	    TuneupFragment frag = tuneUpAdapter.getFragment(tuneUpList.get(pageIndex));
	    if (frag != null)
	    {
		frag.computeEfficiency();
	    }

	    RebateManager.getInstance().updateFormState(null);

	    if (!manualSave && !isItemSaved)
	    {
		deleteItemAtIndex(pageIndex);
		return;
	    }

	    Toast.makeText(getActivity(), getString(R.string.saved_string), Toast.LENGTH_LONG).show();
	}
    }

    private boolean deleteItemAtIndex(int pageIndex)
    {
	boolean retVal = false;

	if (tuneUpList != null && tuneUpList.size() > pageIndex && tuneUpList.size() > 1)
	{
	    final int selectedPageNum = tuneUpList.get(pageIndex).getPageNum();
	    retVal = tuneUpAdapter.deleteItem(pageIndex) != null;

	    Thread t = new Thread(new Runnable()
	    {
		public void run()
		{
		    String equipmentName = getActivity().getResources().getString(R.string.tuneUpId);
		    FileUtil.deleteFolderPartImage(equipmentName, selectedPageNum + "");
		}
	    });
	    t.start();
	}
	return retVal;
    }

    @Override
    public void onFinishInflate(int layoutId, View view)
    {
	if (layoutId == R.layout.delete_confirmation && view != null)
	{
	    ImageButton cancel = (ImageButton) view.findViewById(R.id.delete_cancel_btn);
	    ImageButton done = (ImageButton) view.findViewById(R.id.delete_done_btn);
	    cancel.setOnClickListener(new OnClickListener()
	    {

		@Override
		public void onClick(View v)
		{
		    commonDialog.dismiss();
		}
	    });
	    done.setOnClickListener(new OnClickListener()
	    {

		@Override
		public void onClick(View v)
		{

		    deleteItem();
		}

	    });
	}
    }

    @Override
    public void onDismiss()
    {

    }

    public void setTitle(String title)
    {
	this.title = title;
    }

    @Override
    public void stopProgressDialog()
    {
	getActivity().runOnUiThread(new Runnable()
	{
	    public void run()
	    {
		UiUtil.dismissSpinnerDialog();
	    }
	});
    }

    private synchronized void deleteItem()
    {
	TuneUpItem newItem = (TuneUpItem) tuneUpList.get(0).createObject();
	newItem.setPageNum(tuneUpList.get(tuneUpList.size() - 1).getPageNum() + 1);
	TuneupFragment fragment = (TuneupFragment) tuneUpAdapter.getFragment(tuneUpList.get(pageIndex));
	final int selectedPageNum = tuneUpList.get(pageIndex).getPageNum();
	if (fragment != null)
	{
	    fragment.setFragmentDeleted();
	    	// update rebate amount on delete
	    	float rebateValue = tuneUpList.get(pageIndex).getRebateValue();
		if (RebateManager.getInstance().getFormBean() != null)
		{
		    float prevAmmount = RebateManager.getInstance().getFormBean().getTuneUpRebate();
		    RebateManager.getInstance().getFormBean().setTuneUpRebate(prevAmmount - rebateValue);
		}
	}

	tuneUpAdapter.deleteItem(pageIndex);
	Thread t = new Thread(new Runnable()
	{
	    public void run()
	    {
		String equipmentName = getActivity().getResources().getString(R.string.tuneUpId);
		FileUtil.deleteFolderPartImage(equipmentName, selectedPageNum + "");
	    }
	});
	t.start();
	spinnerList.remove(pageIndex);
	if (newItem != null && tuneUpList.size() == 0)
	{
	    newItem.setPageNum(0);
	    tuneUpList.add(newItem);
	}
	tuneUpAdapter.notifyDataSetChanged();
	viewPager.setOffscreenPageLimit(tuneUpList.size());
	viewPager.firePageCountChanged();
	if (pageIndex >= tuneUpList.size())
	{
	    --pageIndex;
	}

	viewPager.setCurrentItem(pageIndex);
	updateSpinnerAdapter();
	commonDialog.dismiss();
	
    }

    @Override
    public void updateData(final Object obj)
    {
	if (obj != null && obj instanceof FormResponseBean)
	{
	    tuneUp = ((FormResponseBean) obj).getTuneUp().get(mSubtypeSelection);
	    tuneUpList = tuneUp.getItems();

	    getActivity().runOnUiThread(new Runnable()
	    {
		public void run()
		{
		    updateUI();
		}
	    });
	}
    }

    @Override
    public void displayError(String title, String message)
    {

    }

    @Override
    public void showProgressDialog()
    {
	getActivity().runOnUiThread(new Runnable()
	{
	    public void run()
	    {
		UiUtil.showProgressDialog(getActivity());
	    }
	});
    }

    @Override
    public void onPageScrollStateChanged(int arg0)
    {

    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2)
    {

    }

    @Override
    public void onPageSelected(int index)
    {
	pageIndex = index;
	tuneUpComboBox.post(new Runnable()
	{

	    @Override
	    public void run()
	    {
		tuneUpComboBox.setSelection(pageIndex);
	    }
	});
    }

    public void serviceItem(ServiceItem serviceItem)
    {
	setTitle(serviceItem.getServiceItemName());
    }

    public void updateSpinnerName(String name, Options option)
    {
	if (spinnerList != null && equipmentAdapter != null && spinnerList.size() >= pageIndex)
	{
	    spinnerList.set(pageIndex, name);
	    equipmentAdapter.notifyDataSetChanged();
	    option.setValues(spinnerList);
	}
    }

    public void updateSpinneritem(Options option)
    {
	int size = option.getValues().size();
	if (size > 0)
	{
	    for (int i = 0; i < size && i < spinnerList.size(); i++)
	    {
		spinnerList.set(i, option.getValues().get(i));
	    }
	    equipmentAdapter.notifyDataSetChanged();
	}
    }

    public List<TuneUpItem> getTuneUpList()
    {
	return tuneUpList;
    }

    public void setTuneUpList(List<TuneUpItem> tuneUpList)
    {
	this.tuneUpList = tuneUpList;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
	super.onActivityResult(requestCode, resultCode, data);

	if (requestCode == AppConstants.CAPTURE_PHOTO_REQUEST_CODE && resultCode == FragmentActivity.RESULT_OK)
	{
	    TuneupFragment frag = tuneUpAdapter.getFragment(tuneUpList.get(pageIndex));
	    if (tuneUpAdapter != null && frag != null)
	    {
		int pageNum = tuneUpList.get(pageIndex).getPageNum();
		frag.updateCaptureImage(pageNum);
	    }
	}
    }

    public String getPageName()
    {
	TuneupFragment fragm = (TuneupFragment) tuneUpAdapter.getItem(pageIndex);
	return fragm.getTuneUpItemBean().getPageNum() + "";
    }

}
