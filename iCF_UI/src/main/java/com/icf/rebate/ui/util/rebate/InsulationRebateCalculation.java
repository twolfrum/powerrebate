/**
 * 
 */
package com.icf.rebate.ui.util.rebate;

import java.util.ArrayList;
import java.util.List;

import com.icf.rebate.networklayer.model.CustomerInfo;
import com.icf.rebate.networklayer.model.FormResponseBean;
import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.util.ICFLogger;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * @author Ken Butler Dec 8, 2015 InsulationRebateCalculation.java Copyright (c) 2015 ICF International
 */
public class InsulationRebateCalculation
{

    private static final String TAG = EquipmentListRebateCalculation.class.getName();
    private ArrayList<String> selectionArgs;
    private ICFSQLiteOpenHelper helper;
    private String selectionString;
    private String[] columns;
    private String orderBy;

    public InsulationRebateCalculation(String insulationType, int existingR, int finalR, int sqFootage)
    {
	int utilityCompanyId = LibUtils.getSelectedUtilityCompany().getId();

	FormResponseBean bean = RebateManager.getInstance().getFormBean();
	CustomerInfo customerInfo = bean.getCustomerInfo();
	boolean singleHouse = customerInfo.getHouseType() == CustomerInfo.SINGLE_FAMILY_TYPE;
	helper = new ICFSQLiteOpenHelper(LibUtils.getApplicationContext());

	/*
	 * String customerType = null;
	 * 
	 * if (bean.getAppDetails() != null) { customerType = bean.getAppDetails().getCustomerType(); }
	 * 
	 * if (customerType == null || customerType.length() == 0) { customerType = "NA"; }
	 */

	selectionArgs = new ArrayList<>();

	selectionArgs.add(String.valueOf(utilityCompanyId));
	selectionArgs.add(insulationType);
	selectionArgs.add("NA");
	selectionArgs.add(singleHouse ? "Single Family" : "Multifamily");
	selectionArgs.add(String.valueOf(existingR));
	selectionArgs.add("NA");
	selectionArgs.add(String.valueOf(finalR));
	selectionArgs.add("NA");
	selectionArgs.add(String.valueOf(sqFootage));
	selectionArgs.add("NA");
	// selectionArgs.add("NA");
	// selectionArgs.add(insulationCategory);
	// selectionArgs.add("NA");
	// selectionArgs.add(customerType);

	selectionString = InsulationTable.COLUMN_UTILITY_COMPANY + " = ? AND " + InsulationTable.COLUMN_INSULATION_TYPE + " = ? AND (" + InsulationTable.COLUMN_HOUSE_TYPE + " = ? OR " + InsulationTable.COLUMN_HOUSE_TYPE + " = ?) AND (" + InsulationTable.COLUMN_EXISTING_RVAL_MAX + " >= ? OR " + InsulationTable.COLUMN_EXISTING_RVAL_MAX + " = ?) AND (" + InsulationTable.COLUMN_FINAL_RVAL_MIN + " <= ? OR " + InsulationTable.COLUMN_FINAL_RVAL_MIN + " = ?) AND " + "(" + InsulationTable.COLUMN_SQ_FOOTAGE_MIN + " <= ? OR " + InsulationTable.COLUMN_SQ_FOOTAGE_MIN + " = ?);" ;
	// Log.d(TAG, "KVB insulation selction string= " + selectionString);
	// Log.d(TAG, "KVB insulation selection args= " + selectionArgs.toString());
	// columns = new String[] { LibUtils.getYearColumnName(), EquipmentListRebateTable.COLUMN_MEASURE_TYPE };
    }

    public float computeRebate()
    {
	float retVal = 0f;

	SQLiteDatabase database = helper.getReadableDatabase();

	Cursor dbCursor = database.query(InsulationTable.TABLE_NAME, null, null, null, null, null, null);
	String[] columnNames = dbCursor.getColumnNames();

	// Gets list of all columns that apply to todays date
	List<String> matchingDateRangeCols = LibUtils.getYearColumnName(columnNames, null);

	for (String col : matchingDateRangeCols)
	{

	    columns = new String[] { col };

	    Cursor cursor = database.query(InsulationTable.TABLE_NAME, columns, selectionString, selectionArgs.toArray(new String[selectionArgs.size()]), null, null, orderBy);
	    String data = LibUtils.getSqlQueryString(InsulationTable.TABLE_NAME, columns, selectionString, selectionArgs.toArray(new String[selectionArgs.size()]), null, null, orderBy);
	    ICFLogger.d(TAG, data);

	    while (cursor.moveToNext())
	    {
		// retValue = cursor.getFloat(0);
		if (!cursor.isNull(0))
		{
		    retVal = retVal < cursor.getFloat(0) ? cursor.getFloat(0) : retVal;
		}
		// break;
	    }
	}

	database.close();
	Log.d(TAG, "KVB insulation rebateVal= "+retVal);
	return retVal;
    }

}
