package com.icf.rebate.ui.util.rebate;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.List;

import com.icf.rebate.networklayer.model.CustomerInfo;
import com.icf.rebate.networklayer.model.FormResponseBean;
import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.util.ICFLogger;
import com.icf.rebate.ui.util.UiUtil;

public class QIVRebateCalculation
{
    private static final String TAG = QIVRebateCalculation.class.getName();

    public float getSystemCapcityThreshold()
    {
	return passPercentage;
    }

    public void fetchValueFromDB()
    {
	try
	{
	    ICFSQLiteOpenHelper helper = new ICFSQLiteOpenHelper(LibUtils.getApplicationContext());
	    SQLiteDatabase database = helper.getReadableDatabase();
	    String selectionString = QIVTuneUpRebateTable.COLUMN_REBATE_TYPE + "= ? AND " + QIVTuneUpRebateTable.COLUMN_UTILITY_COMPANY + "= ? AND (" + QIVTuneUpRebateTable.COLUMN_CUSTOMER_TYPE + " = ? OR " + EquipmentListRebateTable.COLUMN_CUSTOMER_TYPE + "=  ?) AND (" + EquipmentListRebateTable.COLUMN_HOUSE_TYPE + " = ? OR " + EquipmentListRebateTable.COLUMN_HOUSE_TYPE + " = ?)";

	    String[] selectionArgs = null;

	    String customerType = null;
	    FormResponseBean bean = RebateManager.getInstance().getFormBean();
	    if (bean.getAppDetails() != null)
	    {
		customerType = bean.getAppDetails().getCustomerType();
	    }

	    if (customerType == null || customerType.length() == 0)
	    {
		customerType = "NA";
	    }

	    CustomerInfo customerInfo = bean.getCustomerInfo();
	    boolean singleHouse = customerInfo.getHouseType() == CustomerInfo.SINGLE_FAMILY_TYPE;

	    selectionArgs = new String[] { "qiv", String.valueOf(LibUtils.getSelectedUtilityCompany().getId()), customerType, "NA", singleHouse ? "Single Family" : "Multifamily", "NA" };

	    // Need to figure out which column the date range is referring to
	    Cursor dbCursor = database.query(QIVTuneUpRebateTable.TABLE_NAME, null, null, null, null, null, null);
	    String[] columnNames = dbCursor.getColumnNames();
	    
	    // Gets list of all columns that apply to todays date
	    List<String> matchingDateRangeCols = LibUtils.getYearColumnName(columnNames, null);
	    
	    for (String col : matchingDateRangeCols) {
	    
	    String[] columns = new String[] { QIVTuneUpRebateTable.COLUMN_PASS_PERCENTAGE, col, QIVTuneUpRebateTable.COLUMN_MEASURE_UNIT };

	    Cursor cursor = database.query(QIVTuneUpRebateTable.TABLE_NAME, columns, selectionString, selectionArgs, null, null, null);
	    String data = LibUtils.getSqlQueryString(QIVTuneUpRebateTable.TABLE_NAME, columns, selectionString, selectionArgs, null, null, null);
	    ICFLogger.d(TAG, data);
	    while (cursor.moveToNext())
	    {
		passPercentage = cursor.getInt(0);
		if (!cursor.isNull(1)) {
		    incentive = incentive < cursor.getFloat(1) ? cursor.getFloat(1) : incentive;
		}
		measureUnit = cursor.getString(2);

		// break;
	    }
	    database.close();
	    }
	}
	catch (Exception e)
	{
	    ICFLogger.e(TAG, e);
	}
	
    }

    public float getIncentive(float tonnage)
    {
	float ret = incentive;
	if (measureUnit != null && measureUnit.equalsIgnoreCase(ICFSQLiteOpenHelper.KEY_TONNAGE))
	{
	    ret = tonnage * incentive;
	}
	ret = UiUtil.convertToPostTwoDigtDecimal(ret);
	return ret;
    }

    private int passPercentage;

    private String measureUnit = null;

    public int getPassPercentage()
    {
	return passPercentage;
    }

    private float incentive;
}
