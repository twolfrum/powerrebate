package com.icf.rebate.app.model;

import com.icf.rebate.networklayer.model.FormResponseBean;
import com.icf.rebate.networklayer.model.IList;

/**
 * Created by 19713 on 2/8/2018.
 */

public interface SealingDetail {
    public IList<FormResponseBean.Part> getParts();
}
