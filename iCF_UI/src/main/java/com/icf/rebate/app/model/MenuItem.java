package com.icf.rebate.app.model;

import java.util.ArrayList;

import android.os.Bundle;

public class MenuItem
{
    private int menuItemId;

    private boolean enabled;

    private boolean containChildItems;

    private int menuLayoutId;

    private int menuTitle;

    private int menuImageId;

    private ArrayList<MenuItem> subMenuList;

    private int subMenuViewTypeCount;

    private Bundle saveUIState;

    public int getMenuItemId()
    {
	return menuItemId;
    }

    public void setMenuItemId(int menuItemId)
    {
	this.menuItemId = menuItemId;
    }

    public boolean isContainChildItems()
    {
	return containChildItems;
    }

    public void setContainChildItems(boolean containChildItems)
    {
	this.containChildItems = containChildItems;
    }

    public boolean isEnabled()
    {
	return enabled;
    }

    public void setEnabled(boolean enabled)
    {
	this.enabled = enabled;
    }

    public int getMenuLayoutId()
    {
	return menuLayoutId;
    }

    public void setMenuLayoutId(int menuLayoutId)
    {
	this.menuLayoutId = menuLayoutId;
    }

    public int getMenuTitle()
    {
	return menuTitle;
    }

    public void setMenuTitle(int menuTitle)
    {
	this.menuTitle = menuTitle;
    }

    public int getMenuImageId()
    {
	return menuImageId;
    }

    public void setMenuImageId(int menuImageId)
    {
	this.menuImageId = menuImageId;
    }

    public Bundle getSaveUIState()
    {
	return saveUIState;
    }

    public void setSaveUIState(Bundle saveUIState)
    {
	this.saveUIState = saveUIState;
    }

    public void setSubmenuList(ArrayList<MenuItem> submenu)
    {
	this.subMenuList = submenu;
    }

    public ArrayList<MenuItem> getSubmenuList()
    {
	return this.subMenuList;
    }

    public int getSubMenuViewTypeCount()
    {
	return subMenuViewTypeCount;
    }

    public void setSubMenuViewTypeCount(int subMenuViewTypeCount)
    {
	this.subMenuViewTypeCount = subMenuViewTypeCount;
    }

}