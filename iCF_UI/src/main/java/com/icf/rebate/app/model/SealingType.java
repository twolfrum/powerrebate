package com.icf.rebate.app.model;

import java.util.List;

/**
 * Created by 19713 on 2/8/2018.
 */

public abstract class SealingType {
    public abstract String getName();
    public abstract List<? extends SealingItem> getItems();
    public abstract boolean isMandatoryFieldsComplete();
}
