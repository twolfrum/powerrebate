package com.icf.rebate.app.model;

public class DropdownInfo
{
    private int filterId;

    private String filterName;

    private Object obj;

    public DropdownInfo(int filterId, String filterName)
    {
	super();
	this.filterId = filterId;
	this.filterName = filterName;
    }

    public DropdownInfo(int filterId, String filterName, Object tnumber)
    {
	super();
	this.filterId = filterId;
	this.filterName = filterName;
	this.obj = tnumber;
    }

    public int getFilterId()
    {
	return filterId;
    }

    public void setFilterId(int filterId)
    {
	this.filterId = filterId;
    }

    public String getFilterName()
    {
	return filterName;
    }

    public void setFilterName(String filterName)
    {
	this.filterName = filterName;
    }

    public Object getTnumber()
    {
	return obj;
    }

    public void setTnumber(Object tnumber)
    {
	this.obj = tnumber;
    }

}
