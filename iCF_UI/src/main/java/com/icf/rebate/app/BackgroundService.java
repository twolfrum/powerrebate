package com.icf.rebate.app;

import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.ui.RootActivity;


import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class BackgroundService extends Service
{

    public static final String START_SERVICE = "com.icf.rebate.app.startservice";

    public static final String STOP_SERVICE = "com.icf.rebate.app.stopservice";

    private static final int NOTIFICAITON_ID = 121;

    public BackgroundService()
    {

    }

    private void showNotification()
    {
    Intent notificationIntent = new Intent(getApplicationContext(), RootActivity.class);
    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
	Notification notification = new Notification.Builder(this)
                .setContentText(getText(R.string.app_name))
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.app_icon)
                .setWhen(System.currentTimeMillis())
                .build();
	startForeground(NOTIFICAITON_ID, notification);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
	if (intent != null)
	{
	    String action = intent.getAction();
	    if (START_SERVICE.equalsIgnoreCase(action))
	    {
		showNotification();
	    }
	    else if (STOP_SERVICE.equalsIgnoreCase(action))
	    {
		clearNotification();
	    }
	}

	return super.onStartCommand(intent, flags, startId);
    }

    private void clearNotification()
    {
	stopForeground(true);
    }

    @Override
    public IBinder onBind(Intent intent)
    {
	return null;
    }
}
