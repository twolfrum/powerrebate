package com.icf.rebate.app.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.icf.rebate.networklayer.model.FormResponseBean;

import java.util.List;

/**
 * Created by 19713 on 2/2/2018.
 */

public class AirSealing extends SealingType
        implements FormResponseBean.CalculatableRebateItem {
    @JsonProperty("name")
    private String name;

    @JsonProperty("id")
    private String id;

    @JsonProperty("qivRequired")
    private boolean qivRequired;

    @JsonProperty("ECMIsInAHRIDirectory")
    private String ECMIsInAHRIDirectory;

    @JsonProperty("items")
    private List<AirSealingItem> items;

    @JsonProperty("mandatoryFieldsComplete")
    boolean isMandatoryFieldsComplete = true;

    @JsonProperty("rebateCalcType")
    private String rebateCalcType;

    @JsonProperty("rebateType")
    private String rebateType;

    @JsonProperty("rebateValue")
    private float rebateValue;

    @JsonProperty("maxMeasures")
    private int maxMeasures;

    public float getRebateValue()
    {
        return rebateValue;
    }

    public void setRebateValue(float rebateValue)
    {
        this.rebateValue = rebateValue;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public boolean isQivRequired()
    {
        return qivRequired;
    }

    public void setQivRequired(boolean qivRequired)
    {
        this.qivRequired = qivRequired;
    }

    public String getECMIsInAHRIDirectory()
    {
        return ECMIsInAHRIDirectory;
    }

    public void setECMIsInAHRIDirectory(String eCMIsInAHRIDirectory)
    {
        ECMIsInAHRIDirectory = eCMIsInAHRIDirectory;
    }

    @Override
    public String getRebateCalcType() {
        return rebateCalcType;
    }

    @Override
    public void setRebateCalcType(String rebateCalcType) {
        this.rebateCalcType = rebateCalcType;
    }

    @Override
    public String getRebateType() {
        return rebateType;
    }

    @Override
    public void setRebateType(String rebateType) {
        this.rebateType = rebateType;
    }

    public int getMaxMeasures() {
        return maxMeasures == 0 ? 999 : maxMeasures;
    }

    public void setMaxMeasures(int maxMeasures) {
        this.maxMeasures = maxMeasures;
    }

    public List<? extends SealingItem> getItems()
    {
        return (List<? extends SealingItem>)items;
    }
    public void setItems(List<AirSealingItem> items)
    {
        this.items = items;
    }

    public boolean isMandatoryFieldsComplete()
    {
        return isMandatoryFieldsComplete;
    }

    public void setMandatoryFieldsComplete(boolean isMandatoryFieldsComplete)
    {
        this.isMandatoryFieldsComplete = isMandatoryFieldsComplete;
    }

    public AirSealingItem createNewItem() throws CloneNotSupportedException
    {
        AirSealingItem item = null;
        if (items != null && items.size() > 0)
        {
            item = (AirSealingItem) items.get(items.size() - 1).clone();
        }
        return item;
    }

    public void addNewItem(AirSealingItem item) {
        if (items != null) {
            items.add(item);
        }
    }
}

