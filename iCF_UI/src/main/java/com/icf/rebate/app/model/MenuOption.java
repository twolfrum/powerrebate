package com.icf.rebate.app.model;

import android.graphics.Bitmap;

public class MenuOption
{
	private int imageId;

	private int titleId;

	private int actionId;

	private String strTitle;

	private Bitmap bmpImage;

	private Object item;

	private boolean selected;
	
	private boolean enabled;

	public MenuOption(int titleId, int imageId, int actionId)
	{
		this.imageId = imageId;
		this.titleId = titleId;
		this.actionId = actionId;
	}

	public MenuOption(String title, Bitmap bitmap, int actionId)
	{
		this.strTitle = title;
		this.bmpImage = bitmap;
		this.actionId = actionId;
	}

	public MenuOption()
	{
	}

	public int getImageId()
	{
		return imageId;
	}

	public void setImageId(int imageId)
	{
		this.imageId = imageId;
	}

	public int getTitleId()
	{
		return titleId;
	}

	public void setTitleId(int titleId)
	{
		this.titleId = titleId;
	}

	public int getActionId()
	{
		return actionId;
	}

	public void setActionId(int actionId)
	{
		this.actionId = actionId;
	}

	public String getTitle()
	{
		return strTitle;
	}

	public void setTitle(String strTitle)
	{
		this.strTitle = strTitle;
	}

	public Bitmap getImage()
	{
		return bmpImage;
	}

	public void setImage(Bitmap bmpImage)
	{
		this.bmpImage = bmpImage;
	}

	public Object getItem()
	{
		return item;
	}

	public void setItem(Object item)
	{
		this.item = item;
	}

	public void setSelected(boolean selected)
	{
		this.selected = selected;
	}

	public boolean getSelected()
	{
		return this.selected;
	}
	
	public void setEnabled(boolean enabled)
	{
		this.enabled = enabled;
	}

	public boolean getEnabled()
	{
		return this.enabled;
	}
}
