package com.icf.rebate.app.model;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DashboardItem
{
    @JsonProperty("id")
    private String id;

    @JsonProperty("name")
    private String itemName;

    @JsonProperty("icon")
    private String itemIcon;

    @JsonProperty("options")
    private ArrayList<ServiceItem> serviceList;

    public String getId()
    {
	return id;
    }

    public void setId(String id)
    {
	this.id = id;
    }

    public String getItemName()
    {
	return itemName;
    }

    public void setItemName(String itemName)
    {
	this.itemName = itemName;
    }

    public String getItemIcon()
    {
	return itemIcon;
    }

    public void setItemIcon(String itemIcon)
    {
	this.itemIcon = itemIcon;
    }

    public ArrayList<ServiceItem> getServiceList()
    {
	return serviceList;
    }

    public void setServiceList(ArrayList<ServiceItem> serviceList)
    {
	this.serviceList = serviceList;
    }

    public static class ServiceItem
    {
	@JsonProperty("id")
	private String serviceId;

	@JsonProperty("name")
	private String serviceItemName;

	@JsonProperty("image")
	private String serviceItemImage;

	@JsonProperty("value")
	private String serviceItemValue;

	public ServiceItem()
	{

	}

	public ServiceItem(String id)
	{
	    serviceId = id;
	}

	public String getServiceItemValue()
	{
	    return serviceItemValue;
	}

	public void setServiceItemValue(String serviceItemValue)
	{
	    this.serviceItemValue = serviceItemValue;
	}

	public String getServiceId()
	{
	    return serviceId;
	}

	public void setServiceId(String serviceId)
	{
	    this.serviceId = serviceId;
	}

	public String getServiceItemName()
	{
	    return serviceItemName;
	}

	public void setServiceItemName(String serviceItemName)
	{
	    this.serviceItemName = serviceItemName;
	}

	public String getServiceItemImage()
	{
	    return serviceItemImage;
	}

	public void setServiceItemImage(String serviceItemImage)
	{
	    this.serviceItemImage = serviceItemImage;
	}

	@Override
	public boolean equals(Object o)
	{
	    if (serviceId != null && o != null && ((ServiceItem) o).getServiceId() != null && serviceId.equalsIgnoreCase(((ServiceItem) o).getServiceId()))
	    {
		return true;
	    }
	    return false;
	}
    }
}
