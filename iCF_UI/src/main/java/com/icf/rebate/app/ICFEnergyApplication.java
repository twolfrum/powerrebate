package com.icf.rebate.app;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Application;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import com.crittercism.app.Crittercism;
import com.icf.rebate.networklayer.ICFHttpManager;
import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.ui.util.Analytics;
//import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
//import com.nostra13.universalimageloader.core.ImageLoader;
//import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.icf.rebate.ui.util.PointToServer;
import com.icf.rebate.ui.util.PointToServer.PointingServers;
import com.icf.rebate.ui.util.UiUtil;

public class ICFEnergyApplication extends Application
{

    // private static WeakReference<ICFEnergyApplication> application;

    private PointToServer pointingServerObj;

    @Override
    public void onCreate()
    {
	super.onCreate();
	// application = new WeakReference<ICFEnergyApplication>(this);

	pointingServerObj = new PointToServer(getApplicationContext(), PointingServers.DEV_LOCAL, false);
	ICFHttpManager.init(getApplicationContext(), pointingServerObj.getServerUrl());
	if (!UiUtil.DEBUG_BUILD)
	{
	    Crittercism.initialize(getApplicationContext(), getString(R.string.crittercism_id));
	}
	LibUtils.setApplicationContext(getApplicationContext());
	String storedKey = LibUtils.getAppInfo(LibUtils.KEY_ANALYTICS, null);
	if (storedKey != null)
	{
	    Analytics.init(this, storedKey);
	}
	// Analytics.init(this);

	LibUtils.init(pointingServerObj.getServerUrl());
	String customVersion = pointingServerObj.getDisplayText() + "_" + getString(R.string.app_version) + "_" + getString(R.string.build_version);

	JSONObject json = new JSONObject();
	UiUtil.SCREEN_INCH_7 = is7inchTablet();
	try
	{
	    json.put("customVersionName", customVersion);
	    // json.put("shouldCollectLogcat", true); // necessary for
	    // collecting logcat data on Android Jelly Bean devices.
	}
	catch (JSONException je)
	{
	    // do something...
	}
	// Crittercism.init(getApplicationContext(), CRITTERSSISM, json);

	// initImageLoader(getApplicationContext());
	// handleApplicationCrash();
    }

    private boolean is7inchTablet()
    {
	DisplayMetrics dm = new DisplayMetrics();
	WindowManager wmManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
	wmManager.getDefaultDisplay().getMetrics(dm);
	double x = Math.pow(dm.widthPixels / dm.xdpi, 2);
	double y = Math.pow(dm.heightPixels / dm.ydpi, 2);
	double screenInches = Math.sqrt(x + y);
	DecimalFormat df = new DecimalFormat("#.##", new DecimalFormatSymbols(Locale.US));
	String temp = df.format(screenInches);
	float screenSize = Float.parseFloat(temp);
	if (screenSize >= 6.0f && screenSize <= 7.7f)
	{
	    return true;
	}
	return false;
    }

    // private void handleApplicationCrash() {
    // Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler());
    // }
    //
    // static class ExceptionHandler implements UncaughtExceptionHandler {
    // private UncaughtExceptionHandler defaultUEH = Thread
    // .getDefaultUncaughtExceptionHandler();
    //
    // @Override
    // public void uncaughtException(Thread paramThread,
    // Throwable paramThrowable) {
    // ICFEnergyApplication instance = null;
    // if (application != null && (instance = application.get()) != null) {
    // Log.e("SBConnectApplication",
    // "Unexpected application crash, relaunching app.... ");
    // // Log.e("SBConnectApplication", "Reason : ");
    // // paramThrowable.printStackTrace();
    // PendingIntent intent = PendingIntent.getActivity(instance
    // .getBaseContext(), 0,
    // new Intent(instance.getBaseContext(),
    // LoginActivity.class), 0);
    // AlarmManager mgr = (AlarmManager) instance
    // .getSystemService(Context.ALARM_SERVICE);
    // mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 2000,
    // intent);
    // defaultUEH.uncaughtException(paramThread, paramThrowable);
    // System.exit(2);
    // }
    //
    // }
    // }

    // public void initImageLoader(Context context)
    // {
    // This configuration tuning is custom. You can tune every option, you
    // may tune some of them,
    // or you can create default configuration by
    // ImageLoaderConfiguration.createDefault(this);
    // method.
    // ImageLoaderConfiguration config = new
    // ImageLoaderConfiguration.Builder(context).threadPoolSize(5).threadPriority(Thread.MIN_PRIORITY
    // + 3).discCacheFileNameGenerator(new Md5FileNameGenerator()).build();

    // Initialize ImageLoader with configuration.
    // ImageLoader.getInstance().init(config);
    // }

    // public ImageLoader getImageLoader()
    // {
    // return ImageLoader.getInstance();
    // }

    // public static ICFEnergyApplication getApplication() {
    // return application.get();
    // }

}
