package com.icf.rebate.app.model;

import java.util.ListIterator;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.icf.rebate.networklayer.model.FormResponseBean;
import com.icf.rebate.networklayer.model.FormResponseBean.Part;
import com.icf.rebate.networklayer.model.IList;
import com.icf.rebate.ui.util.AppConstants;

public class DuctSealingItem extends SealingItem implements Cloneable
{
    @JsonProperty("ductSealingDetail")
    private DuctSealingDetail ductSealingDetail;

    @JsonProperty("itemSaved")
    private boolean itemSaved;

    @JsonProperty("pageNum")
    private int pageNum;

    @JsonProperty("staticRebateValue")
    private String staticRebateValue;

    @JsonProperty("formFilledState")
    public int formFilledState = AppConstants.FORM_ITEM_FILLED_STATE.ITEM_UNEDITED.getState();

    private float rebateValue;

    public int getFormFilledState()
    {
	return formFilledState;
    }

    public void setMandatoryFiledComplete(int state)
    {
	this.formFilledState = state;
    }

    public SealingDetail getSealingDetail()
    {
	return ductSealingDetail;
    }

    public boolean isItemSaved()
    {
	return itemSaved;
    }

    public void setItemSaved(boolean itemSaved)
    {
	this.itemSaved = itemSaved;
    }

    public float getRebateValue()
    {
	return rebateValue;
    }

    public void setRebateValue(float rebateValue)
    {
	this.rebateValue = rebateValue;
    }

    public String getStaticRebateValue() {
        return staticRebateValue;
    }

    public void setStaticRebateValue(String staticRebateValue) {
        this.staticRebateValue = staticRebateValue;
    }

    @Override
    public Object clone()
    {
        DuctSealingItem item = new DuctSealingItem();
        if (ductSealingDetail != null && ductSealingDetail.getParts() != null)
        {
            item.ductSealingDetail = (DuctSealingDetail) ductSealingDetail.createObject();
        }
        return item;
    }

    public int getPageNum()
    {
	return pageNum;
    }

    public void setPageNum(int pageNum)
    {
	this.pageNum = pageNum;
    }

    public class DuctSealingDetail implements FormResponseBean.FormObjectBuilder, SealingDetail {
        @JsonProperty("parts")
        IList<Part> parts;

        public IList<FormResponseBean.Part> getParts()
        {
            return parts;
        }

        public void setParts(IList<FormResponseBean.Part> parts)
        {
            this.parts = parts;
        }

        @Override
        public Object createObject()
        {
            DuctSealingDetail newDetail = new DuctSealingDetail();
            IList<FormResponseBean.Part> partList = new IList<FormResponseBean.Part>();
            ListIterator<FormResponseBean.Part> iterator = parts.listIterator();
            FormResponseBean.Part oldPart = null;

            while (iterator.hasNext())
            {
                oldPart = (FormResponseBean.Part) iterator.next();
                if (oldPart.getDuplicatePartId() == -1)
                {
                    FormResponseBean.Part part = (FormResponseBean.Part) (oldPart).createObject();
                    partList.add(part);
                }
            }

            newDetail.parts = partList;
            return newDetail;
        }
    }
}
