package com.icf.rebate.app.model;

import com.icf.rebate.networklayer.model.FormResponseBean.Item;

public class QIVModel
{
    String equipmentName;
    Item item;
    
    public String getEquipmentName()
    {
        return equipmentName;
    }
    public void setEquipmentName(String equipmentName)
    {
        this.equipmentName = equipmentName;
    }
    
    public Item getItem()
    {
        return item;
    }
    public void setItem(Item item)
    {
        this.item = item;
    }
    
    
}
