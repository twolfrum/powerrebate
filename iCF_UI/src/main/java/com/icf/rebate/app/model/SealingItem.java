package com.icf.rebate.app.model;

/**
 * Created by 19713 on 2/8/2018.
 */

public abstract class SealingItem {
    public abstract void setMandatoryFiledComplete(int state);
    public abstract int getFormFilledState();
    public abstract boolean isItemSaved();
    public abstract void setItemSaved(boolean itemSaved);
    public abstract SealingDetail getSealingDetail();
}
