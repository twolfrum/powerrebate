package com.icf.rebate.app.model;

public class ConfirmationMode
{
    private int imageResId;

    private String modeName;

    private boolean selected;

    public int getImageResId()
    {
	return imageResId;
    }

    public void setImageResId(int imageResId)
    {
	this.imageResId = imageResId;
    }

    public String getModeName()
    {
	return modeName;
    }

    public void setModeName(String modeName)
    {
	this.modeName = modeName;
    }

    public void setSelected(boolean selected)
    {
	this.selected = selected;
    }

    public boolean isSelected()
    {
	return this.selected;
    }

}
