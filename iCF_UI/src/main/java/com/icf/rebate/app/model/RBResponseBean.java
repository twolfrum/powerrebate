package com.icf.rebate.app.model;

import com.icf.rebate.networklayer.model.ResponseBean;

public class RBResponseBean extends ResponseBean {
	private byte[] data;

	public RBResponseBean(byte[] data) {
		this.data = data;
	}

	public byte[] getRBData() {
		return data;
	}
	
	
}
