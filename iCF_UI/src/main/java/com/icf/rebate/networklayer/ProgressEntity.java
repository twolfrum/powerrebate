package com.icf.rebate.networklayer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.http.entity.FileEntity;

import android.util.Log;

public class ProgressEntity extends FileEntity
{
    private long totalWrittenByte;

    private int percentageCompleted;

    private UploadProgressListener listener;

    private Object object;

    public ProgressEntity(File file, String contentType)
    {
	super(file, contentType);
    }

    @Override
    public void writeTo(OutputStream outstream) throws IOException
    {
	if (outstream == null)
	{
	    return;
	}
	long totalSize = this.file.length();
	long onePercentage = totalSize / 100;
	InputStream stream = new FileInputStream(this.file);
	try
	{
	    byte[] tmp = new byte[4096];
	    int l = -1;
	    totalWrittenByte = 0;
	    while ((l = stream.read(tmp)) != -1)
	    {
		outstream.write(tmp, 0, l);
		totalWrittenByte += l;
		percentageCompleted = (int) (totalWrittenByte / onePercentage);
		updateListener(percentageCompleted);
	    }
	    percentageCompleted = 100;
	    updateListener(percentageCompleted);
	    outstream.flush();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	    updateListener(-100);
	}
	finally
	{
	    if (stream != null)
	    {
		stream.close();
	    }

	}

    }

    private void updateListener(int percentageCompleted2)
    {
	Log.d("ddddddddddd", "fffff" + percentageCompleted2);
	listener.updateProgress(percentageCompleted2, object);
    }

    public void setProgressListener(UploadProgressListener listener)
    {
	this.listener = listener;
    }

    public void setRequestObj(Object obj)
    {
	this.object = obj;
    }
}
