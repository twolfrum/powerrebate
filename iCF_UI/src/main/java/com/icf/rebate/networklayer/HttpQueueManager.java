package com.icf.rebate.networklayer;

import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import android.content.Context;

import com.icf.rebate.ui.util.ICFLogger;

public class HttpQueueManager
{
    private static final String TAG = HttpQueueManager.class.getSimpleName();
    // private static Context context;
    // private static DataParser dataParser;
    private static HttpQueueManager instance;
    // private Protocol protocol;
    private int reqNumber = 0;
    // private HttpManagerListener listener;
    private ThreadPoolExecutor executor;
    private static final int MAX_NO_OF_THREADS = 3;
    private ArrayList<RequestObject> requestQueue;

    // private boolean isConnected = false;
    // private TaskRunnable task = null;
    // private int requestId;

    // public static void init(Context context) {
    // // HttpManager.context = context;
    // // HttpManager.dataParser = dataParser;
    // instance = new HttpManager();
    // }

    private HttpQueueManager()
    {
	// isConnected = true;
	// this.protocol = protocol;
	reqNumber = 0;
	requestQueue = new ArrayList<RequestObject>();
	executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(MAX_NO_OF_THREADS);
	// setBasicAuthRequired(protocol.getUserName(), protocol.getPassword());
    }

    public static HttpQueueManager getInstance()
    {
	if (instance == null)
	{
	    instance = new HttpQueueManager();
	}
	return instance;
    }

    // public void processGetRequest(int reqID, String url, String urlGetArgs, HttpManagerListener listener) {
    // try {
    // if (context != null) {
    // if (!ConnectionManager
    // .isOnline(context.getApplicationContext())) {
    // if (listener != null) {
    // listener.networkConnectivityError();
    // }
    // return;
    // }
    // int reqIdentifier = reqNumber++;
    // TaskInterface task = getData(reqID, reqIdentifier, url, urlGetArgs);
    // RequestObject reqObj = new RequestObject(reqIdentifier);
    // reqObj.setReqID(reqID);
    // reqObj.setUrlGetParams(urlGetArgs);
    // reqObj.setTask(task);
    // reqObj.setHttpManagerListener(listener);
    // requestQueue.add(reqObj);
    // }
    //
    // } catch (Exception ex) {
    // ex.printStackTrace();
    // }
    //
    // }

    // public void processPostRequest(int reqID, String url, String urlGetArgs, HttpManagerListener listener) {
    // try {
    // if (context != null) {
    // if (!ConnectionManager
    // .isOnline(context.getApplicationContext())) {
    // if (listener != null) {
    // listener.networkConnectivityError();
    // }
    // return;
    // }
    // int reqIdentifier = reqNumber++;
    // TaskRunnable task = postData(reqID, reqIdentifier, url, urlGetArgs);
    // // task.addNameValuePair(nameValuePair);
    // RequestObject reqObj = new RequestObject(reqIdentifier);
    // reqObj.setReqID(reqID);
    // reqObj.setHttpManagerListener(listener);
    // // reqObj.setUrlGetParams(urlGetArgs);
    // reqObj.setTask(task);
    // requestQueue.add(reqObj);
    // }
    //
    // } catch (Exception ex) {
    //
    // }
    //
    // }

    // private TaskRunnable getData(int reqID, int reqNumber, String url, String urlGetArgs) {
    // requestId = reqID;
    // // String url = protocol.getReqForId(reqID);
    // if (urlGetArgs != null && urlGetArgs.length() > 0) {
    // url += '?' + urlGetArgs;
    // }
    // url = url.trim().replaceAll(" ", "%20");
    // // TaskRunnable task = new TaskRunnable(reqID, url, reqNumber, this);
    // task.connect();
    // executor.execute(task);
    // return task;
    // }
    //
    // private TaskRunnable postData(int reqID, int reqNumber, String url, String urlGetArgs) {
    // // String url = protocol.getReqForId(reqID);
    // requestId = reqID;
    // if (urlGetArgs != null && urlGetArgs.length() > 0) {
    // url += '?' + urlGetArgs;
    // }
    // // TaskRunnable task = new TaskRunnable(reqID, url, reqNumber, this, true);
    // // task.addNameValuePair(nameValuePair);
    // task.connect();
    // executor.execute(task);
    // return task;
    // }

    // private TaskInterface postData(int reqID, int reqNumber, String url, byte[] postData) {
    // requestId = reqID;
    // // TaskRunnable task = new TaskRunnable(reqID, url, reqNumber, this, true);
    // task.setPostData(postData);
    // task.connect();
    // executor.execute(task);
    // return task;
    // }

    public void removeReq(int packetNum)
    {

	int index = requestQueue.indexOf(new RequestObject(packetNum));
	try
	{
	    if (index > -1 && index < requestQueue.size())
	    {
		RequestObject obj = requestQueue.get(index);
		TaskInterface task = obj.getTask();
		if (task != null)
		{
		    task.disconnect();
		}
		requestQueue.remove(index);
		this.executor.remove((TaskRunnable) task);
	    }
	}
	catch (Exception e)
	{
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }

    public void cancelAll()
    {

	// isConnected = false;
	for (RequestObject p : this.requestQueue)
	{
	    TaskInterface task = p.getTask();
	    if (task == null)
	    {
		continue;
	    }
	    p.getTask().disconnect();
	    this.executor.remove((TaskRunnable) task);
	    ICFLogger.d(TAG, "Disconnect all the requests.");
	}
	this.executor.purge();
	this.executor.shutdownNow();
	this.requestQueue.clear();
	// this.listener = null;
    }

    public void addToQueue(RequestObject reqObj, TaskRunnable task) throws Exception
    {
	task.connect();
	requestQueue.add(reqObj);
//	if (requestQueue.size() > MAX_NO_OF_THREADS)
//	{
//	    throw new Exception("Max Number of Threads reached. Check if old requests are removed properly");
//	}
	executor.execute(task);
    }

    public int getRequestIdentifier()
    {
	return reqNumber++;
    }
}
