package com.icf.rebate.networklayer.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.icf.rebate.networklayer.utils.LibUtils;

public class LoginResponseBean extends ResponseBean
{

    private static final String STATUS_CHANGE_PASSWORD = "S1000";

    @JsonProperty("userName")
    private String userName;
    @JsonProperty("password")
    private String password;
    @JsonProperty("firstName")
    private String firstName;
    @JsonProperty("middleName")
    private String middleName;
    @JsonProperty("lastName")
    private String lastName;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("emailId")
    private String emailId;

    @JsonProperty("fullName")
    private String fullName;

    @JsonProperty("resetFlag")
    private String resetFlag;

    @JsonProperty("userStatus")
    private int userStatus;

    @JsonProperty("contractorAccountNumber")
    private String contractorAccountNumber;
    
    @JsonProperty("contractorCompany")
    private String contractorCompany;
    
    @JsonProperty("address1")
    private String address1;
    
    @JsonProperty("address2")
    private String address2;

    @JsonProperty("city")
    private String city;

    @JsonProperty("state")
    private String state;

    @JsonProperty("zip")
    private String zip;

    @JsonProperty("cell")
    private String cell;
    
    @JsonProperty("userId")
    private String userId;
    
    public String getUserName()
    {
	return userName;
    }

    public void setUserName(String userName)
    {
	this.userName = userName;
    }

    public String getPassword()
    {
	return password;
    }

    public void setPassword(String password)
    {
	this.password = LibUtils.md5Hash(password);
	// this.password = password;
    }

    public String getFirstName()
    {
	return firstName;
    }

    public void setFirstName(String firstName)
    {
	this.firstName = firstName;
    }

    public String getMiddleName()
    {
	return middleName;
    }

    public void setMiddleName(String middleName)
    {
	this.middleName = middleName;
    }

    public String getLastName()
    {
	return lastName;
    }

    public void setLastName(String lastName)
    {
	this.lastName = lastName;
    }

    public boolean shouldChangePassword()
    {
	return STATUS_CHANGE_PASSWORD.equalsIgnoreCase(statusCode);
    }

    public String getPhone()
    {
	return phone;
    }

    public void setPhone(String phone)
    {
	this.phone = phone;
    }

    public String getEmailId()
    {
	return emailId;
    }

    public void setEmailId(String emailId)
    {
	this.emailId = emailId;
    }

    public String getFullName()
    {
	return fullName;
    }

    public void setFullName(String fullName)
    {
	this.fullName = fullName;
    }

    public String getResetFlag()
    {
	return resetFlag;
    }

    public void setResetFlag(String resetFlag)
    {
	this.resetFlag = resetFlag;
    }

    public int getUserStatus()
    {
	return userStatus;
    }

    public void setUserStatus(int userStatus)
    {
	this.userStatus = userStatus;
    }

    public String getCity()
    {
	return city;
    }

    public void setCity(String city)
    {
	this.city = city;
    }

    public String getState()
    {
	return state;
    }

    public void setState(String state)
    {
	this.state = state;
    }

    public String getZip()
    {
	return zip;
    }

    public void setZip(String zip)
    {
	this.zip = zip;
    }

    public String getCell()
    {
	return cell;
    }

    public void setCell(String cell)
    {
	this.cell = cell;
    }

    public String getAddress1()
    {
        return address1;
    }

    public void setAddress1(String address1)
    {
        this.address1 = address1;
    }

    public String getAddress2()
    {
        return address2;
    }

    public void setAddress2(String address2)
    {
        this.address2 = address2;
    }

    public String getContractorCompany()
    {
        return contractorCompany;
    }

    public void setContractorCompany(String contractorCompany)
    {
        this.contractorCompany = contractorCompany;
    }

    public String getContractorAccountNumber()
    {
        return contractorAccountNumber;
    }

    public void setContractorAccountNumber(String contractorAccountNumber)
    {
        this.contractorAccountNumber = contractorAccountNumber;
    }

    
    
    /**
     * @return the userId
     */
    public String getUserId()
    {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
	return "LoginResponseBean [userName=" + userName + ", password=" + password + ", firstName=" + firstName + ", middleName=" + middleName + ", lastName=" + lastName + ", phone=" + phone + ", emailId=" + emailId + ", fullName=" + fullName + ", resetFlag=" + resetFlag + ", userStatus=" + userStatus + ", contractorAccountNumber=" + contractorAccountNumber + ", contractorCompany=" + contractorCompany + ", address1=" + address1 + ", address2=" + address2 + ", city=" + city + ", state=" + state + ", zip=" + zip + ", cell=" + cell + ", userId=" + userId + "]";
    }

 

    
    
}
