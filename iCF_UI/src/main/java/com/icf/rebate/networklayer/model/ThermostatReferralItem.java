package com.icf.rebate.networklayer.model;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.icf.rebate.networklayer.model.FormResponseBean.Options;
import com.icf.rebate.networklayer.model.FormResponseBean.Part;
import com.icf.rebate.ui.util.AppConstants;

public class ThermostatReferralItem
{
    @JsonProperty("thermostatId")
    public long id = new Date().getTime();

    @JsonProperty("name")
    private String name;

    @JsonProperty("parts")
    List<Part> parts;
    
    public List<Part> getParts()
    {
	return parts;
    }
    
    public void setParts(List<Part> parts) {
	this.parts = parts;
    }

//    public void setOptions(List<Options> options)
//    {
//	this.parts.get(0).setO = options;
//    }

    public String getName()
    {
	return name;
    }

    public void setName(String name)
    {
	this.name = name;
    }
}
