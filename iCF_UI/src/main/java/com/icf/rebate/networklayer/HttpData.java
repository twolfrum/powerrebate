package com.icf.rebate.networklayer;

import org.apache.http.StatusLine;

public class HttpData
{
    private byte[] data;

    private StatusLine status;

    public HttpData(StatusLine status, byte[] data)
    {
	this.status = status;
	this.data = data;
    }

    public byte[] getData()
    {
	return data;
    }

    public void setData(byte[] data)
    {
	this.data = data;
    }

    public StatusLine getStatus()
    {
	return status;
    }

    public void setStatus(StatusLine status)
    {
	this.status = status;
    }

}
