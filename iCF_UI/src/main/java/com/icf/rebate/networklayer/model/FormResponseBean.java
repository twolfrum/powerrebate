package com.icf.rebate.networklayer.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;

import android.text.TextUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.icf.rebate.app.model.AirSealingItem;
import com.icf.rebate.app.model.AirSealing;
import com.icf.rebate.app.model.DashboardItem.ServiceItem;
import com.icf.rebate.app.model.DuctSealingItem;
import com.icf.rebate.app.model.DuctSealing;
import com.icf.rebate.app.model.SealingItem;
import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.ICFLogger;
import com.icf.rebate.ui.util.UiUtil;
import com.icf.rebate.ui.util.rebate.EquipmentListRebateCalculation;
import com.icf.rebate.ui.util.rebate.InsulationRebateCalculation;
import com.icf.rebate.ui.util.rebate.ProjectCostRebateCalculation;
import com.icf.rebate.ui.util.rebate.WDRebateCalculation;

public class FormResponseBean extends ResponseBean
{
    private static final String TAG = FormResponseBean.class.getName();

    @JsonProperty("customerDetails")
    CustomerInfo customerInfo;
    
    @JsonProperty("customerScreenLayout")
    CustomerScreenLayout customerScreenLayout;
    
    @JsonProperty("rebateConfirmationDetails")
    RebateConfirmationDetails rebateConfirmationDetails;

    @JsonProperty("appDetails")
    AppDetails appDetails;

    @JsonProperty("contractorDetails")
    ContractorDetails contractorDetail;
    
    @JsonProperty("submission")
    List<Submission> submissionItems;

    @JsonProperty("equipments")
    List<Equipment> equipments;
    
    @JsonProperty("insulation")
    List<Insulation> insulation;
    
    @JsonProperty("windowAndDoor")
    List<WindowAndDoor> windowAndDoor;

    @JsonProperty("ductImprovements")
    List<DuctItem> ductImprovements;

    @JsonProperty("ductSealing")
    List<DuctSealing> ductSealing;

    @JsonProperty("airSealing")
    List<AirSealing>airSealing;

    @JsonProperty("qualityInstallVerification")
    List<QIVItem> qivList;

    @JsonProperty("thermostatReferral")
    List<ThermostatReferralItem> thermostatReferralList;

    @JsonProperty("thermostatInstall")
    List<ThermostatInstallItem> thermostatInstall;
    
    @JsonProperty("tuneUp")
    List<TuneUp> tuneUp;

    public List<Long> mandatoryFields = new ArrayList<Long>();

    @JsonIgnore
    private boolean isThermostatReferralChecked;

    int editMode = AppConstants.FORM_EDIT_MODE.EDIT_MODE.getMode();

    private List<Equipment> filteredEquipmentList;
	private List<TuneUp> filteredTuneUpList;
	private List<AirSealing> filteredAirSealingList;
	private List<DuctSealing> filteredDuctSealingList;

	@JsonIgnore
    private boolean serializationMode;

    @JsonIgnore
    private ArrayList<ServiceItem> serviceList;

    public RebateConfirmationDetails getRebateConfirmationDetails()
    {
        return rebateConfirmationDetails;
    }

    public void setRebateConfirmationDetails(RebateConfirmationDetails rebateConfirmationDetails)
    {
        this.rebateConfirmationDetails = rebateConfirmationDetails;
    }
    
    public ArrayList<ServiceItem> getServiceList()
    {
	return serviceList;
    }

    public void setServiceList(ArrayList<ServiceItem> serviceList)
    {
	this.serviceList = serviceList;
    }

    public boolean isThermostatReferralChecked()
    {
	return isThermostatReferralChecked;
    }

    public void setThermostatReferralChecked(boolean isThermostatReferralChecked)
    {
	this.isThermostatReferralChecked = isThermostatReferralChecked;
    }

    public int getEditMode()
    {
	return editMode;
    }

    public void setEditMode(int editMode)
    {
	this.editMode = editMode;
    }

    public ContractorDetails getContractorDetail()
    {
	return contractorDetail;
    }

    public void setContractorDetail(ContractorDetails contractorDetail)
    {
	this.contractorDetail = contractorDetail;
    }

    public int isFormValid() {
		boolean isFormEdited = false;
		int completelySavedForms = 0;

		if (windowAndDoor != null) {
			Iterator<WindowAndDoor> wdIterator = windowAndDoor.listIterator();
			while (wdIterator.hasNext()) {
				FormResponseBean.WindowAndDoor wd = (FormResponseBean.WindowAndDoor) wdIterator.next();
				if (wd != null && wd.getItems() != null) {
					Iterator<WDItem> itemsIterator = wd.getItems().listIterator();
					while (itemsIterator.hasNext()) {
						FormResponseBean.WDItem item = (FormResponseBean.WDItem) itemsIterator.next();

						if (item.isItemSaved()) {
							isFormEdited = true;
							++completelySavedForms;
						}

						if (item.getFormFilledState() == AppConstants.FORM_ITEM_FILLED_STATE.ITEM_PARTIALLY_SAVED.getState()) {
							return item.getFormFilledState();
						} else if (item.getFormFilledState() == AppConstants.FORM_ITEM_FILLED_STATE.ITEM_COMPLETELY_SAVED.getState()) {
							--completelySavedForms;
						}
					}
				}
			}
		}

		if (insulation != null) {
			Iterator<Insulation> insulationIterator = insulation.listIterator();
			while (insulationIterator.hasNext()) {
				FormResponseBean.Insulation insulation = (FormResponseBean.Insulation) insulationIterator.next();
				if (insulation != null && insulation.getItems() != null) {
					Iterator<InsulationItem> itemsIterator = insulation.getItems().listIterator();
					while (itemsIterator.hasNext()) {
						FormResponseBean.InsulationItem item = (FormResponseBean.InsulationItem) itemsIterator.next();

						if (item.isItemSaved()) {
							isFormEdited = true;
							++completelySavedForms;
						}

						if (item.getFormFilledState() == AppConstants.FORM_ITEM_FILLED_STATE.ITEM_PARTIALLY_SAVED.getState()) {
							return item.getFormFilledState();
						} else if (item.getFormFilledState() == AppConstants.FORM_ITEM_FILLED_STATE.ITEM_COMPLETELY_SAVED.getState()) {
							--completelySavedForms;
						}
					}
				}
			}
		}

		if (equipments != null) {
			Iterator<Equipment> equipmentIterator = equipments.listIterator();
			while (equipmentIterator.hasNext()) {
				FormResponseBean.Equipment equipment = (FormResponseBean.Equipment) equipmentIterator.next();
				if (equipment != null && equipment.getItems() != null) {
					Iterator<Item> itemsIterator = equipment.getItems().listIterator();
					while (itemsIterator.hasNext()) {
						FormResponseBean.Item item = (FormResponseBean.Item) itemsIterator.next();

						if (item.isItemSaved()) {
							isFormEdited = true;
							++completelySavedForms;
						}

						if (item.getFormFilledState() == AppConstants.FORM_ITEM_FILLED_STATE.ITEM_PARTIALLY_SAVED.getState()) {
							return item.getFormFilledState();
						} else if (item.getFormFilledState() == AppConstants.FORM_ITEM_FILLED_STATE.ITEM_COMPLETELY_SAVED.getState()) {
							--completelySavedForms;
						}
					}
				}
			}
		}

		if (ductSealing != null) {
			Iterator<DuctSealing> iterator = ductSealing.listIterator();
			while (iterator.hasNext()) {
				DuctSealing type = (DuctSealing) iterator.next();
				if (type.getItems() != null) {
					Iterator<? extends SealingItem> itemIterator = type.getItems().iterator();
					while (itemIterator.hasNext()) {
						DuctSealingItem item = (DuctSealingItem)itemIterator.next();
						if (item.isItemSaved()) {
							isFormEdited = true;
							++completelySavedForms;
						}
						if (item.getFormFilledState() == AppConstants.FORM_ITEM_FILLED_STATE.ITEM_PARTIALLY_SAVED.getState()) {
							return item.getFormFilledState();
						} else if (item.getFormFilledState() == AppConstants.FORM_ITEM_FILLED_STATE.ITEM_COMPLETELY_SAVED.getState()) {
							--completelySavedForms;
						}
					}
				}
			}
		}

	if (airSealing != null)
	{
	    Iterator<AirSealing> iterator = airSealing.listIterator();
		while (iterator.hasNext()) {
			AirSealing type = (AirSealing) iterator.next();
			if (type.getItems() != null) {
				Iterator<? extends SealingItem> itemIterator = type.getItems().iterator();
				while (itemIterator.hasNext()) {
					AirSealingItem item = (AirSealingItem)itemIterator.next();
					if (item.isItemSaved()) {
						isFormEdited = true;
						++completelySavedForms;
					}
					if (item.getFormFilledState() == AppConstants.FORM_ITEM_FILLED_STATE.ITEM_PARTIALLY_SAVED.getState()) {
						return item.getFormFilledState();
					} else if (item.getFormFilledState() == AppConstants.FORM_ITEM_FILLED_STATE.ITEM_COMPLETELY_SAVED.getState()) {
						--completelySavedForms;
					}
				}
			}
		}
	}
	
	if (qivList != null)
	{
	    Iterator<QIVItem> iterator = qivList.listIterator();
	    while (iterator.hasNext())
	    {
		QIVItem item = (QIVItem) iterator.next();
		if (item.isQivRequired())
		{
		    if (item.isItemSaved())
		    {
			isFormEdited = true;
			++completelySavedForms;
		    }
		    if (item.getFormFilledState() == AppConstants.FORM_ITEM_FILLED_STATE.ITEM_PARTIALLY_SAVED.getState())
		    {
			return item.getFormFilledState();
		    }
		    else if (item.getFormFilledState() == AppConstants.FORM_ITEM_FILLED_STATE.ITEM_COMPLETELY_SAVED.getState())
		    {
			--completelySavedForms;
		    }
		    else if (item.getFormFilledState() == AppConstants.FORM_ITEM_FILLED_STATE.ITEM_UNEDITED.getState())
		    {
			return AppConstants.FORM_ITEM_FILLED_STATE.ITEM_PARTIALLY_SAVED.getState();
		    }
		}
	    }
	}

	if (thermostatInstall != null)
	{
	    Iterator<ThermostatInstallItem> iterator = thermostatInstall.listIterator();
	    while (iterator.hasNext())
	    {
		ThermostatInstallItem item = (ThermostatInstallItem) iterator.next();
		if (item.isItemSaved())
		{
		    isFormEdited = true;
		    ++completelySavedForms;
		}
		if (item.getFormFilledState() == AppConstants.FORM_ITEM_FILLED_STATE.ITEM_PARTIALLY_SAVED.getState())
		{
		    return item.getFormFilledState();
		}
		else if (item.getFormFilledState() == AppConstants.FORM_ITEM_FILLED_STATE.ITEM_COMPLETELY_SAVED.getState())
		{
		    --completelySavedForms;
		}
	    }
	}

	if (tuneUp != null)
	{
	    Iterator<TuneUp> iterator = tuneUp.listIterator();
	    while (iterator.hasNext()) {
			TuneUp type = (TuneUp) iterator.next();
			if (type.getItems() != null) {
				Iterator<TuneUpItem> itemIterator = type.getItems().iterator();
				while (itemIterator.hasNext()) {
					TuneUpItem item = itemIterator.next();
					if (item.isItemSaved()) {
						isFormEdited = true;
						++completelySavedForms;
					}
					if (item.getFormFilledState() == AppConstants.FORM_ITEM_FILLED_STATE.ITEM_PARTIALLY_SAVED.getState()) {
						return item.getFormFilledState();
					} else if (item.getFormFilledState() == AppConstants.FORM_ITEM_FILLED_STATE.ITEM_COMPLETELY_SAVED.getState()) {
						--completelySavedForms;

					}
				}
			}
		}
	}

	int retState = -1;
	if (isFormEdited && completelySavedForms == 0)
	{
	    retState = AppConstants.FORM_ITEM_FILLED_STATE.ITEM_COMPLETELY_SAVED.getState();
	}
	else if (!isFormEdited)
	{
	    retState = AppConstants.FORM_EDITED_STATE.FORM_UNEDITED.getState();
	}
	else if (isFormEdited && completelySavedForms > 0)
	{
	    retState = AppConstants.FORM_ITEM_FILLED_STATE.ITEM_PARTIALLY_SAVED.getState();
	}

	return retState;
    }

    @JsonIgnore
    public float getThermostatInstallRebate()
    {
	if (appDetails != null)
	{
	    return appDetails.getThermostatInstallRebate();
	}
	return 0.0f;
    }

    public void setThermostatInstallRebate(float thermostatInstallRebate)
    {
	if (appDetails != null)
	{
	    appDetails.setThermostatInstallRebate(thermostatInstallRebate);
	}
    }

    @JsonIgnore
    public float getInsulationListRebate(){
	float rebate = 0f;
	if (appDetails != null)  {
	    rebate = appDetails.getInsulationListRebate();
	}
	return rebate;
    }
    
    public void setInsulationListRebate(float rebate) {
	if (appDetails != null) {
	    appDetails.setInsulationListRebate(rebate);
	}
    }
    
    @JsonIgnore
    public float getWindowDoorListRebate(){
	float rebate = 0f;
	if (appDetails != null)  {
	    rebate = appDetails.getWindowDoorListRebate();
	}
	return rebate;
    }
    
    public void setWindowDoorListRebate(float rebate) {
	if (appDetails != null) {
	    appDetails.setWindowDoorListRebate(rebate);
	}
    }
    
    @JsonIgnore
    public float getEquipmentListRebate()
    {
	if (appDetails != null)
	{
	    return appDetails.getEquipmentListRebate();
	}
	return 0.0f;
    }

    public void setEquipmentListRebate(float rebate)
    {
	if (appDetails != null)
	{
	    appDetails.setEquipmentListRebate(rebate);
	}
    }

    // TODO: implement this
    public float getRebateValue(String serviceId)
    {
	return getEquipmentListRebate();
    }

    @JsonIgnore
    public float getQIVRebate()
    {
	if (appDetails != null)
	{
	    return appDetails.getQivrebate();
	}
	return 0.0f;
    }

    public void setQIVRebate(float rebate)
    {
	// qivRebate = rebate;
	if (appDetails != null)
	{
	    appDetails.setQivrebate(rebate);
	}
    }

    public void setTuneUpRebate(float rebate)
    {
	// tuneUpRebate = rebate;
	if (appDetails != null)
	{
	    appDetails.setTuneUpRebate(rebate);
	}
    }

    @JsonIgnore
    public float getTuneUpRebate()
    {
	if (appDetails != null)
	{
	    return appDetails.getTuneUpRebate();
	}
	return 0.0f;
    }

	@JsonIgnore
	public float getDuctSealingRebate() {
		if (appDetails != null) {
			return appDetails.getDuctImprovementRebate();
		}
		return 0.0f;
	}

	public void setDuctSealingRebate(float rebateValue) {
		// ductSealingRebate = rebateValue;
		if (appDetails != null) {
			appDetails.setDuctImprovementRebate(rebateValue);
		}
	}

    @JsonIgnore
    public float getAirSealingRebate()
    {
        if (appDetails != null) {
            return appDetails.getAirSealingRebate();
        }
        return 0.0f;
    }

    public void setAirSealingRebate(float rebateValue)
    {
		if (appDetails != null) {
			appDetails.setAirSealingRebate(rebateValue);
		}
	}

    public List<ThermostatInstallItem> getThermostatInstall()
    {

	ArrayList<ThermostatInstallItem> list = null;
	if (serializationMode)
	{
	    if (thermostatInstall != null)
	    {
		for (ThermostatInstallItem item : thermostatInstall)
		{
		    if (item.isItemSaved())
		    {
			if (list == null)
			{
			    list = new ArrayList<ThermostatInstallItem>();
			}
			list.add(item);
		    }
		}
	    }
	    return list;
	}
	return thermostatInstall;
    }

    public void setThermostatInstall(List<ThermostatInstallItem> thermostatInstall)
    {
	this.thermostatInstall = thermostatInstall;
    }

    @JsonIgnore
    public float getTotalRebate()
    {
	if (appDetails != null)
	{
	    return Float.parseFloat(appDetails.getRebateAmountValue());
	}
	return 0.0f;
    }

	public void setTotalRebate(float totalRebate) {
		if (appDetails != null) {
			appDetails.setRebateAmountValue(String.valueOf(totalRebate));
		}
	}

	public interface FormObjectBuilder
	{
		Object createObject();
	}

	public interface CalculatableRebateItem {
		public String getRebateCalcType();
		public void setRebateCalcType(String rebateCalcType);
		public String getRebateType();
		public void setRebateType(String rebateType);
	}

	public static class WindowAndDoor {
	@JsonProperty("name")
	private String name;
	
	@JsonProperty("id")
	private String id;
	
	@JsonProperty("qivRequired")
	private boolean qivRequired;
	
	@JsonProperty("ECMIsInAHRIDirectory")
	private String ECMIsInAHRIDirectory;
	
	@JsonProperty("items")
	private List<WDItem> items;
	
	@JsonProperty("rebateValue")
	private double rebateValue;
	
	@JsonProperty("mandatoryFieldsComplete")
	boolean isMandatoryFieldsComplete = true;

    @JsonProperty("maxMeasures")
    private int maxMeasures;

    public double getRebateValue()
	{
	    return rebateValue;
	}

	public void setRebateValue(float rebateValue)
	{
	    this.rebateValue = rebateValue;
	}
	
	public String getName()
	{
	    return name;
	}

	public void setName(String name)
	{
	    this.name = name;
	}

	public String getId()
	{
	    return id;
	}

	public void setId(String id)
	{
	    this.id = id;
	}

	public boolean isQivRequired()
	{
	    return qivRequired;
	}

	public void setQivRequired(boolean qivRequired)
	{
	    this.qivRequired = qivRequired;
	}

	public String getECMIsInAHRIDirectory()
	{
	    return ECMIsInAHRIDirectory;
	}

	public void setECMIsInAHRIDirectory(String eCMIsInAHRIDirectory)
	{
	    ECMIsInAHRIDirectory = eCMIsInAHRIDirectory;
	}

    public int getMaxMeasures() {
		return maxMeasures == 0 ? 999 : maxMeasures;
    }

    public void setMaxMeasures(int maxMeasures) {
        this.maxMeasures = maxMeasures;
    }

    public List<WDItem> getItems()
	{
	    return items;
	}

	public void setItems(List<WDItem> items)
	{
	    this.items = items;
	}

	public boolean isMandatoryFieldsComplete()
	{
	    return isMandatoryFieldsComplete;
	}

	public void setMandatoryFieldsComplete(boolean isMandatoryFieldsComplete)
	{
	    this.isMandatoryFieldsComplete = isMandatoryFieldsComplete;
	}
	

	public WDItem createNewItem() throws CloneNotSupportedException
	{
	    WDItem item = null;
	    if (items != null && items.size() > 0)
	    {
		item = (WDItem) items.get(items.size() - 1).clone();
	    }
	    return item;
	}
	
	public void addNewItem(WDItem item)
	{
	    if (items != null)
	    {
		items.add(item);
	    }
	}
	
	public double computeRebate(WDItem item, double uVal, double shgcVal, int numInstalled)
	{
	    
	    double oldValue = item.getRebateValue(); 
	    // Retrieve existingR and finalR vals
	    double newValue = item.computeRebate(id, uVal, shgcVal, numInstalled);
	    // to avoid iterating all items to calculate new rebate;
	    rebateValue -= oldValue;
	    rebateValue += newValue;
	    return newValue;
	}
    }
    
    
    public static class WDItem implements Cloneable {

	@JsonProperty("itemSaved")
	private boolean itemSaved;
	
	@JsonProperty("formFilledState")
	public int formFilledState = AppConstants.FORM_ITEM_FILLED_STATE.ITEM_UNEDITED.getState();
	
	@JsonProperty("pageNum")
	private int pageNum;
	
	@JsonProperty("rebateValue")
	private double rebateValue;

    @JsonProperty("staticRebateValue")
    private String staticRebateValue;

	@JsonProperty("windowDoorDetail")
	private WDDetail wDDetail;
	
	public int getFormFilledState()
	{
	    return formFilledState;
	}

	public void setMandatoryFiledComplete(int state)
	{
	    this.formFilledState = state;
	}

	public boolean isItemSaved()
	{
	    return itemSaved;
	}

	public void setItemSaved(boolean itemSaved)
	{
	    this.itemSaved = itemSaved;
	}

	public int getPageNum()
	{
	    return pageNum;
	}

	public void setPageNum(int pageNum)
	{
	    this.pageNum = pageNum;
	}

	public double getRebateValue()
	{
	    return rebateValue;
	}

	public void setRebateValue(float rebateValue)
	{
	    this.rebateValue = rebateValue;
	}

    public String getStaticRebateValue() {
        return staticRebateValue;
    }

    public void setStaticRebateValue(String staticRebateValue) {
        this.staticRebateValue = staticRebateValue;
    }

    public WDDetail getwDDetail()
{
    return wDDetail;
}

	public void setwDDetail(WDDetail wDDetail)
	{
	    this.wDDetail = wDDetail;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException
	{
	    WDItem item = new WDItem();
	   
	    if (wDDetail != null && wDDetail.getParts() != null)
	    {
		WDDetail newWDDetail = new WDDetail();
		newWDDetail = (WDDetail) wDDetail.createObject();
		item.wDDetail = newWDDetail;
	    }
	    
	    return item;
	}
	
	public double computeRebate(String id, double uVal, double shgcVal, int numInstalled) {
	    WDRebateCalculation rebateCalculator = new WDRebateCalculation(id, uVal, shgcVal, numInstalled);
	    rebateValue = rebateCalculator.computeRebate();
	    return rebateValue;
	}
    }
    
    public static class WDDetail implements FormObjectBuilder {
	
	@JsonProperty("parts")
	List<Part> parts;

	public List<Part> getParts()
	{
	    return parts;
	}

	public void setParts(List<Part> parts)
	{
	    this.parts = parts;
	}

		@Override
		public Object createObject() {
			WDDetail newDetail = new WDDetail();
			IList<Part> partList = new IList<Part>();
			ListIterator<Part> iterator = parts.listIterator();

			Part oldPart = null;

			while (iterator.hasNext()) {
				oldPart = (Part) iterator.next();
				if (oldPart.getDuplicatePartId() == -1) {
					Part part = (Part) (oldPart).createObject();
					partList.add(part);
				}
			}

			newDetail.parts = partList;

			return newDetail;

		}

	}

    public static class TuneUp implements FormObjectBuilder
    {
        @JsonProperty("tuneupId")
        public long id = new Date().getTime();

        @JsonProperty("name")
        private String name;

        @JsonProperty("id")
        private String jsonId;

        @JsonProperty("items")
        List<TuneUpItem> items;

        @JsonProperty("rebateValue")
        float rebateValue;

        @JsonProperty("pageNum")
        int pageNum;

        @JsonProperty("mandatoryFieldsComplete")
        boolean isMandatoryFieldsComplete = true;

        @JsonProperty("maxMeasures")
        private int maxMeasures;

        public boolean isMandatoryFieldsComplete()
        {
    	return isMandatoryFieldsComplete;
        }

		public void setMandatoryFieldsComplete(boolean isMandatoryFieldsComplete) {
			this.isMandatoryFieldsComplete = isMandatoryFieldsComplete;
		}

        public float getRebateValue()
        {
    	return rebateValue;
        }

        public void setRebateValue(float rebateValue)
        {
    	this.rebateValue = rebateValue;
        }

        public int getMaxMeasures() {
			return maxMeasures == 0 ? 999 : maxMeasures;
        }

        public void setMaxMeasures(int maxMeasures) {
            this.maxMeasures = maxMeasures;
        }

        private boolean packageSelected = true;

        @JsonProperty("itemSaved")
        private boolean itemSaved;

        public Category getSplit()
        {
    	if (isPackageSelected())
    	{
    	    if (UiUtil.serializationMode)
    	    {
    		return null;
    	    }
    	}
    	return items != null && items.size() > 0 ? items.get(0).getSplit() : null;
        }

        public void setSplit(Category split)
        {
    	if (items != null && items.size() > 0)
    	{
    	    items.get(0).setSplit(split);
    	}
        }

        public Category getPackages()
        {
    	if (!isPackageSelected())
    	{
    	    if (UiUtil.serializationMode)
    	    {
    		return null;
    	    }
    	}
    	return items != null && items.size() > 0 ? items.get(0).getPackages() : null;
        }

        public void setPackages(Category packages)
        {
    	if (items != null && items.size() > 0)
    	{
    	    items.get(0).setPackages(packages);
    	}
        }

        public boolean isItemSaved()
        {
    	return itemSaved;
        }

        public void setItemSaved(boolean itemSaved)
        {
    	this.itemSaved = itemSaved;
        }

        public boolean isPackageSelected()
        {
    	return packageSelected;
        }

        public void setPackageSelected(boolean state)
        {
    	this.packageSelected = state;
        }

        @Override
        public Object createObject()
        {
    	TuneUp newTuneUp = new TuneUp();
    	newTuneUp.itemSaved = false;
    	newTuneUp.packageSelected = true;
    	Category packages = null, split = null;
    	newTuneUp.items = new ArrayList<>();
    	TuneUpItem newTuneUpItem = new TuneUpItem();

    	if (items != null && items.size() > 0)
    	{
    	    packages = items.get(0).getPackages();
    	    split = items.get(0).getSplit();
    	}
    	else
    	{
    	    return null;
    	}
    	if (packages != null && packages.getParts() != null && packages.getParts().size() > 0)
    	{
    	    newTuneUpItem.setPackages((Category) packages.createObject());
    	}
    	if (split != null && split.getParts() != null && split.getParts().size() > 0)
    	{
    	    newTuneUpItem.setSplit((Category) split.createObject());
    	}
    	newTuneUp.getItems().add(newTuneUpItem);
    	return newTuneUp;
        }

        public int getPageNum()
        {
    	return pageNum;
        }

        public void setPageNum(int pageNum)
        {
    	this.pageNum = pageNum;
        }

        public String getName()
        {
    	return name;
        }

        public void setName(String name)
        {
    	this.name = name;
        }

        public String getJsonId()
        {
    	return jsonId;
        }

        public void setJsonId(String jsonId)
        {
    	this.jsonId = jsonId;
        }

        public List<TuneUpItem> getItems()
        {
    	return items;
        }

        public void setItems(List<TuneUpItem> items)
        {
    	this.items = items;
        }

    }
    
    public static class TuneUpItem implements FormObjectBuilder
    {
	@JsonProperty("split")
	private Category split;

	@JsonProperty("packages")
	private Category packages;

	@JsonProperty("itemSaved")
	private boolean itemSaved;

	@JsonProperty("mandatoryFieldsComplete")
	boolean isMandatoryFieldsComplete = true;

	@JsonProperty("formFilledState")
	public int formFilledState = AppConstants.FORM_ITEM_FILLED_STATE.ITEM_UNEDITED.getState();

	@JsonProperty("rebateValue")
	float rebateValue;

    @JsonProperty("staticRebateValue")
    private String staticRebateValue;

	@JsonProperty("pageNum")
	int pageNum;

	private boolean packageSelected = true;

	public Category getSplit()
	{
	    return split;
	}

	public void setSplit(Category split)
	{
	    this.split = split;
	}

	public Category getPackages()
	{
	    return packages;
	}

	public void setPackages(Category packages)
	{
	    this.packages = packages;
	}

	public boolean isItemSaved()
	{
	    return itemSaved;
	}

	public void setItemSaved(boolean itemSaved)
	{
	    this.itemSaved = itemSaved;
	}

	public boolean isPackageSelected()
	{
	    return packageSelected;
	}

	public void setPackageSelected(boolean state)
	{
	    this.packageSelected = state;
	}

	public int getFormFilledState()
	{
	    return formFilledState;
	}

	public void setMandatoryFiledComplete(int state)
	{
	    this.formFilledState = state;
	}

	public boolean isMandatoryFieldsComplete()
	{
	    return isMandatoryFieldsComplete;
	}

	public void setMandatoryFieldsComplete(boolean isMandatoryFieldsComplete)
	{
	    this.isMandatoryFieldsComplete = isMandatoryFieldsComplete;
	}

	public float getRebateValue()
	{
	    return rebateValue;
	}

	public void setRebateValue(float rebateValue)
	{
	    this.rebateValue = rebateValue;
	}

    public String getStaticRebateValue() {
        return staticRebateValue;
    }

    public void setStaticRebateValue(String staticRebateValue) {
        this.staticRebateValue = staticRebateValue;
    }

    public int getPageNum()
	{
	    return pageNum;
	}

	public void setPageNum(int pageNum)
	{
	    this.pageNum = pageNum;
	}

	@Override
	public Object createObject()
	{
	    TuneUpItem newTuneUp = new TuneUpItem();
	    newTuneUp.itemSaved = false;
	    newTuneUp.packageSelected = true;
	    Category packages = null, split = null;

	    packages = this.packages;
	    split = this.split;

	    if (packages != null && packages.getParts() != null && packages.getParts().size() > 0)
	    {
		newTuneUp.setPackages((Category) packages.createObject());
	    }
	    if (split != null && split.getParts() != null && split.getParts().size() > 0)
	    {
		newTuneUp.setSplit((Category) split.createObject());
	    }
	    return newTuneUp;
	}

    }

    // KVB 12/1/2015 -- Adding insulation support
    
    public static class Insulation implements CalculatableRebateItem {
	@JsonProperty("name")
	private String name;
	
	@JsonProperty("id")
	private String id;
	
	@JsonProperty("qivRequired")
	private boolean qivRequired;
	
	@JsonProperty("ECMIsInAHRIDirectory")
	private String ECMIsInAHRIDirectory;
	
	@JsonProperty("items")
	private List<InsulationItem> items;
	
	@JsonProperty("mandatoryFieldsComplete")
	boolean isMandatoryFieldsComplete = true;
	
	@JsonProperty("rebateValue")
	private float rebateValue;

    @JsonProperty("rebateCalcType")
    private String rebateCalcType;

    @JsonProperty("rebateType")
    private String rebateType;

    @JsonProperty("maxMeasures")
    private int maxMeasures;

    public float getRebateValue()
	{
	    return rebateValue;
	}

	public void setRebateValue(float rebateValue)
	{
	    this.rebateValue = rebateValue;
	}

	public String getName()
	{
	    return name;
	}

	public void setName(String name)
	{
	    this.name = name;
	}

	public String getId()
	{
	    return id;
	}

	public void setId(String id)
	{
	    this.id = id;
	}

	public boolean isQivRequired()
	{
	    return qivRequired;
	}

	public void setQivRequired(boolean qivRequired)
	{
	    this.qivRequired = qivRequired;
	}

	public String getECMIsInAHRIDirectory()
	{
	    return ECMIsInAHRIDirectory;
	}

	public void setECMIsInAHRIDirectory(String eCMIsInAHRIDirectory)
	{
	    ECMIsInAHRIDirectory = eCMIsInAHRIDirectory;
	}

    @Override
    public String getRebateCalcType() {
        return rebateCalcType;
    }

    @Override
    public void setRebateCalcType(String rebateCalcType) {
        this.rebateCalcType = rebateCalcType;
    }

    @Override
    public String getRebateType() {
        return rebateType;
    }

    @Override
    public void setRebateType(String rebateType) {
        this.rebateType = rebateType;
    }

    public int getMaxMeasures() {
        return maxMeasures == 0 ? 999 : maxMeasures;
    }

    public void setMaxMeasures(int maxMeasures) {
        this.maxMeasures = maxMeasures;
    }

    public List<InsulationItem> getItems()
{
    return items;
}

	public void setItems(List<InsulationItem> items)
	{
	    this.items = items;
	}

	public boolean isMandatoryFieldsComplete()
	{
	    return isMandatoryFieldsComplete;
	}

	public void setMandatoryFieldsComplete(boolean isMandatoryFieldsComplete)
	{
	    this.isMandatoryFieldsComplete = isMandatoryFieldsComplete;
	}
	
	public InsulationItem createNewItem() throws CloneNotSupportedException
	{
	    InsulationItem item = null;
	    if (items != null && items.size() > 0)
	    {
		item = (InsulationItem) items.get(items.size() - 1).clone();
	    }
	    return item;
	}
	
	/**
	 * 
	 * @param item
	 * @return new rebate value for the item.
	 */
	public float computeRebate(InsulationItem item, int existingRVal, int finalRVal, int sqFootage,
                                  float projectCost, int quantity)
	{
        float oldValue = item.getRebateValue();
        float newValue = 0f;

        if (!TextUtils.isEmpty(rebateCalcType) &&
                rebateCalcType.equals(ProjectCostRebateCalculation.CALC_TYPE)) {
            newValue = item.computeRebate(rebateType, projectCost, quantity);
        } else {
            if (existingRVal == -1 || finalRVal == -1) return 0f;
            newValue = item.computeRebate(id, existingRVal, finalRVal, sqFootage);
        }
        // to avoid iterating all items to calculate new rebate;
        rebateValue -= oldValue;
        rebateValue += newValue;
        return newValue;
	}

	public void addNewItem(InsulationItem item) {
			if (items != null) {
				items.add(item);
			}
		}
	}

	public static class InsulationItem implements Cloneable {

	@JsonProperty("insulationDetail")
	private InsulationDetail insulationDetail;

	@JsonProperty("itemSaved")
	private boolean itemSaved;

	@JsonProperty("formFilledState")
	public int formFilledState = AppConstants.FORM_ITEM_FILLED_STATE.ITEM_UNEDITED.getState();

	@JsonProperty("pageNum")
	private int pageNum;

	@JsonProperty("rebateValue")
	private float rebateValue;

    @JsonProperty("staticRebateValue")
    private String staticRebateValue;

    public float getRebateValue()
{
    return rebateValue;
}

	public void setRebateValue(float rebateValue)
	{
	    this.rebateValue = rebateValue;
	}

	public int getPageNum()
	{
	    return pageNum;
	}

	public void setPageNum(int pageNum)
	{
	    this.pageNum = pageNum;
	}

	public int getFormFilledState()
	{
	    return formFilledState;
	}

	public void setMandatoryFiledComplete(int state)
	{
	    this.formFilledState = state;
	}

	public InsulationDetail getInsulationDetail()
	{
	    return insulationDetail;
	}

	public void setInsulationDetail(InsulationDetail insulationDetail)
	{
	    this.insulationDetail = insulationDetail;
	}

	public boolean isItemSaved()
	{
	    return itemSaved;
	}


	public void setItemSaved(boolean itemSaved)
	{
	    this.itemSaved = itemSaved;
	}

    public String getStaticRebateValue() {
        return staticRebateValue;
    }

    public void setStaticRebateValue(String staticRebateValue) {
        this.staticRebateValue = staticRebateValue;
    }

	@Override
	protected Object clone() throws CloneNotSupportedException
	{
	    InsulationItem item = new InsulationItem();
	   
	    if (insulationDetail != null && insulationDetail.getParts() != null)
	    {
		InsulationDetail newInsulationDetail = new InsulationDetail();
		newInsulationDetail = (InsulationDetail) insulationDetail.createObject();
		item.insulationDetail = newInsulationDetail;
	    }
	    
	    return item;
	}

    public float computeRebate(String id, int existingRVal, int finalRVal, int sqFootage) {
		InsulationRebateCalculation rebateCalculator =
                new InsulationRebateCalculation(id, existingRVal, finalRVal, sqFootage);
            rebateValue = rebateCalculator.computeRebate();
            return rebateValue;

    }

    public float computeRebate(String rebateType, float projectCost, int quantity) {
        ProjectCostRebateCalculation rebateCalculator =
                new ProjectCostRebateCalculation();
        rebateValue = rebateCalculator.getRebateValue(rebateType, projectCost, quantity);
        return rebateValue;
    }
}

public static class InsulationDetail implements FormObjectBuilder {
	
	@JsonProperty("parts")
	IList<Part> parts;

	public IList<Part> getParts()
	{
	    return parts;
	}

	public void setParts(IList<Part> parts)
	{
	    this.parts = parts;
	}
	
	@Override
	public Object createObject()
	{
	    InsulationDetail newDetail = new InsulationDetail();
	    IList<Part> partList = new IList<Part>();
	    ListIterator<Part> iterator = parts.listIterator();

	    Part oldPart = null;

	    while (iterator.hasNext())
	    {
		oldPart = (Part) iterator.next();
		if (oldPart.getDuplicatePartId() == -1)
		{
		    Part part = (Part) (oldPart).createObject();
		    partList.add(part);
		}
	    }

	    newDetail.parts = partList;

	    return newDetail;
	    
	    
	}
    }
    
    public static class Equipment implements CalculatableRebateItem
    {
	@JsonProperty("name")
	String name;

	@JsonProperty("qivRequired")
	String qivRequired;
	
	@JsonProperty("qivHidden")
	boolean qivHidden = false;

	@JsonProperty("items")
	List<Item> items;

	@JsonProperty("rebateValue")
	private float rebateValue;

	@JsonProperty("id")
	private String equipmentId;

    @JsonProperty("rebateCalcType")
	private String rebateCalcType;

    @JsonProperty("rebateCalcDateId")
	private String rebateCalcDateId;

	@JsonProperty("rebateType")
	private String rebateType;

	@JsonProperty("mandatoryFieldsComplete")
	boolean isMandatoryFieldsComplete = true;

    @JsonProperty("maxMeasures")
	private int maxMeasures;

	public Equipment() {}

	public Equipment(String equipmentId)
	{
	    this.equipmentId = equipmentId;
	}

	public boolean isMandatoryFieldsComplete()
	{
	    return isMandatoryFieldsComplete;
	}

	public void setMandatoryFieldsComplete(boolean isMandatoryFieldsComplete)
	{
	    this.isMandatoryFieldsComplete = isMandatoryFieldsComplete;
	}

	public String getRebateCalcType() {
		return rebateCalcType;
	}

	public void setRebateCalcType(String rebateCalcType) {
		this.rebateCalcType = rebateCalcType;
	}

	public String getRebateCalcDateId() {
		return rebateCalcDateId;
	}

	public void setRebateCalcDateId(String rebateCalcDateId) {
		this.rebateCalcDateId = rebateCalcDateId;
	}

	public String getRebateType() {
		return rebateType;
	}

	public void setRebateType(String rebateType) {
		this.rebateType = rebateType;
	}

	@Override
	public boolean equals(Object obj) {
		try {
			return equipmentId.equalsIgnoreCase(((Equipment) obj).equipmentId);
		} catch (Exception e) {

		}
		return false;
	}

	public Boolean isQivRequired()
	{
	    return Boolean.parseBoolean(qivRequired);
	}

	public void setQivRequired(String qivRequired)
	{
	    this.qivRequired = qivRequired;
	}

	public String getEquipmentId()
	{
	    return equipmentId;
	}

	public void setEquipmentId(String equipmentId)
	{
	    this.equipmentId = equipmentId;
	}

	public float getRebateValue()
	{
	    return rebateValue;
	}

	public int getMaxMeasures() {
		return maxMeasures == 0 ? 999 : maxMeasures;
	}

	public void setMaxMeasures(int maxMeasures) {
		this.maxMeasures = maxMeasures;
	}

	public Item createNewItem() throws CloneNotSupportedException
	{
	    Item item = null;
	    if (items != null && items.size() > 0)
	    {
		item = (Item) items.get(items.size() - 1).clone();
	    }
	    return item;
	}

	public List<Item> getItems()
	{
	    return items;
	}

	public void setItems(List<Item> items)
	{
	    this.items = items;
	}

	public void addNewItem(Item item)
	{
	    if (items != null)
	    {
		items.add(item);
	    }
	}

	public Item removeItemAtPos(int index)
	{
	    if (items != null && items.size() > index && items.size() > 0)
	    {
		return items.remove(index);
	    }
	    return null;
	}

	public String getName()
	{
	    return name;
	}

	public void setName(String name)
	{
	    this.name = name;
	}

	/**
	 * 
	 * @param item
	 * @return new rebate value for the item.
	 */
	public float computeRebate(Item item)
	{
	    float oldValue = item.getRebateValue();    
	    float newValue = 0f;
        if (!TextUtils.isEmpty(rebateCalcType) &&
                rebateCalcType.equals(ProjectCostRebateCalculation.CALC_TYPE)) {
            newValue = item.computeProjectCostRebate(rebateType);
        } else {
            newValue = item.computeRebate(equipmentId, rebateCalcDateId);
        }

        // to avoid iterating all items to calculate new rebate;
	    rebateValue -= oldValue;
	    rebateValue += newValue;
	    return newValue;
	}

	public void setRebate(float rebate)
	{
	    this.rebateValue = rebate;
	}

	public boolean canCreateQIVFor()
	{
	    if (equipmentId != null && (equipmentId.equalsIgnoreCase(AppConstants.BRUSHLESSPERMANENTMAGNETMOTOR_ID) || equipmentId.equalsIgnoreCase(AppConstants.WATERHEATER_ID) || equipmentId.equalsIgnoreCase(AppConstants.BOILER_ID) || equipmentId.equalsIgnoreCase(AppConstants.FURNACE_ID) || equipmentId.equalsIgnoreCase(AppConstants.MINISPLITS_ID) || equipmentId.equalsIgnoreCase(AppConstants.GEOTHERMALHEATPUMPS_ID)))
	    {
		return false;
	    }

	    return true;
	}

	public boolean isQivHidden()
	{
	    return qivHidden;
	}

	public void setQivHidden(boolean qivHidden)
	{
	    this.qivHidden = qivHidden;
	}
	
	

    }

    public static class Category implements FormObjectBuilder
    {
	@JsonProperty("parts")
	IList<Part> parts;

	public IList<Part> getParts()
	{
	    return parts;
	}

	public void setParts(IList<Part> parts)
	{
	    this.parts = parts;
	}

	@Override
	public Object createObject()
	{
	    Category newCat = new Category();
	    IList<Part> partList = new IList<Part>();
	    ListIterator<Part> iterator = parts.listIterator();

	    Part oldPart = null;

	    while (iterator.hasNext())
	    {
		oldPart = (Part) iterator.next();
		if (oldPart.getDuplicatePartId() == -1)
		{
		    Part part = (Part) (oldPart).createObject();
		    partList.add(part);
		}
	    }

	    newCat.parts = partList;

	    return newCat;
	}

	@JsonIgnore
	Part getPartById(String id)
	{
	    if (parts != null)
	    {
		int index = parts.indexOf(new Part(id));
		if (index != -1)
		{
		    return parts.get(index);
		}
	    }
	    return null;
	}

	Part getPartByIdAndSelectType(String id, String systemType)
	{
	    if (parts != null)
	    {
		ListIterator<Part> iterator = parts.listIterator();

		while (iterator.hasNext())
		{
		    FormResponseBean.Part part = (FormResponseBean.Part) iterator.next();

		    if (part != null && part.getSystemType() != null && part.getId() != null && part.getId().equalsIgnoreCase(id) && part.getSystemType().equalsIgnoreCase(systemType))
		    {
			return part;
		    }
		}
	    }
	    return null;
	}
    }

    public static class Item implements Cloneable
    {
	@JsonProperty("replaceOnFail")
	Category replaceOnFail;

	@JsonProperty("earlyRetirement")
	Category earlyRetirement;

	@JsonProperty("storeReplaceOnFailSelected")
	public boolean replaceOnFailSelected = true;

	@JsonProperty("itemSaved")
	private boolean itemSaved;

	@JsonProperty("rebateValue")
	private float rebateValue;

	@JsonProperty("id")
	private long id = new Date().getTime();

	@JsonProperty("formFilledState")
	public int formFilledState = AppConstants.FORM_ITEM_FILLED_STATE.ITEM_UNEDITED.getState();

	@JsonProperty("pageNum")
	private int pageNum;

    @JsonProperty("staticRebateValue")
    private String staticRebateValue;

	public int getFormFilledState()
	{
	    return formFilledState;
	}

	public void setMandatoryFiledComplete(int state)
	{
	    this.formFilledState = state;
	}

	public float getRebateValue()
	{
	    return rebateValue;
	}

	public void setPageNum(int pageNum)
	{
	    this.pageNum = pageNum;
	}

	public int getPageNum()
	{
	    return this.pageNum;
	}

    public String getStaticRebateValue() {
        return staticRebateValue;
    }

    public void setStaticRebateValue(String staticRebateValue) {
        this.staticRebateValue = staticRebateValue;
    }

     /**
	 * Based on the Equipment ID the logic for computing the rebate will vary
	 * 
	 * @return
	 */
	public float computeRebate(String equipmentId, String rebateCalcDateId)
	{
 	    EquipmentListRebateCalculation rebateCalculator = new EquipmentListRebateCalculation(equipmentId, replaceOnFailSelected);

	    if (equipmentId != null)
	    {
		if (equipmentId.contains(AppConstants.BRUSHLESSPERMANENTMAGNETMOTOR_ID))
		{
		    if (isRetrofitBrushlessMotorChecked())
		    {
			equipmentId = AppConstants.RETROFIT_BRUSHLESSPERMANENTMAGNETMOTOR_ID;
			rebateCalculator = new EquipmentListRebateCalculation(equipmentId, replaceOnFailSelected);
			rebateValue = rebateCalculator.calculateRebateForBrushless(isBrushlessMotorChecked(), "NA", isRetrofitBrushlessMotorChecked());
		    }
		    else
		    {
			rebateValue = rebateCalculator.calculateRebateForBrushless(isBrushlessMotorChecked(), getECMAHRIDirChecked(), isRetrofitBrushlessMotorChecked());
		    }
		}
		else if (equipmentId.contains(AppConstants.WATERHEATER_ID))
		{
		    // rebateValue = rebateCalculator.calculateRebateForWaterHeater(getWaterHeaterType(), getWaterHeaterEfficiency());
		    rebateValue = rebateCalculator.calculateRebateForWaterHeater(getOldHeatingSystemType(),
					getWaterHeaterEfficiency(), getRebateCalcDate(rebateCalcDateId));
		}
		else if (equipmentId.contains(AppConstants.BOILER_ID))
		{
			String oldHeatingType = null;
			String effOrSeer = null;

			//Rebate calculations for boilers may be based on either boiler efficiency
			//or seer value. First assume boiler efficiency
			effOrSeer = getBoilerFurnaceEfficiency();
			if (effOrSeer != null) {
				oldHeatingType = getBoilerOldHeatingSystemType();
				if (oldHeatingType == null) oldHeatingType = getOldHeatingSystemType();
				if (oldHeatingType != null) {
					if (!isBoilerEligibleForOldHeatingSystemType(oldHeatingType)) {
						if (oldHeatingType.indexOf(AppConstants.AC_FURNACE_SUFFIX) != -1 || oldHeatingType.equalsIgnoreCase("Dual Fuel Heating System")) {
							effOrSeer = null;
						}
					} else {
						oldHeatingType = "acfurnace";
					}
				}
			} else {
				//boiler efficiency not in play, look for seer value
				effOrSeer = getSEERValue();
			}
			rebateValue = rebateCalculator.calculateRebateForBoilerOrFurnace(effOrSeer,
					getTonnage(), oldHeatingType, getRebateCalcDate(rebateCalcDateId));
		}
		// Ignore furnace for now... it seems to be working okay
		else if (equipmentId.equalsIgnoreCase(AppConstants.FURNACE_ID))
		{

		    String furnaceEff = getFurnaceEfficiency();

		    String oldHeatingType = getFurnanceOldHeatingSystemType();
		    if (oldHeatingType == null)
			oldHeatingType = getOldHeatingSystemType();
		    if (oldHeatingType != null)
		    {
			if (!isFurnaceEligibleForOldHeatingSystemType(oldHeatingType))
			{
			    if (oldHeatingType.indexOf(AppConstants.AC_FURNACE_SUFFIX) != -1 || oldHeatingType.equalsIgnoreCase("Dual Fuel Heating System"))
			    {
				furnaceEff = null;
			    }
			}
			else
			{
			    oldHeatingType = "acfurnace";
			}
		    }
		    rebateValue = rebateCalculator.calculateRebateForBoilerOrFurnace(furnaceEff, getTonnage(),
					null, getRebateCalcDate(rebateCalcDateId));
		}
		else if (equipmentId.contains(AppConstants.GEOTHERMALHEATPUMPS_ID))
		{
		   // rebateValue = rebateCalculator.calculateRebateForOthers(getSEERValueForGeothermal(), getTonnageForGeoThermal(), null);
		   // rebateValue = rebateCalculator.calculateRebateForOthers(getSEERValueForGeothermal(), getTonnageForGeoThermal(), getOldHeatingSystemType());
		   // KVB - should be sending the new equipment tonnage not the old equipment tonnage
		    rebateValue = rebateCalculator.calculateRebateForOthers(getSEERValueForGeothermal(),
                    getNewTonnageForGeothermal(), getOldHeatingSystemType(), getRebateCalcDate(rebateCalcDateId));
		}
		else if (equipmentId.contains(AppConstants.MINISPLITS_ID))
		{
		    String oldHeatingType = getOldHeatingSystemTypeForWaterHeaterType();
		    if (oldHeatingType == null) {
			oldHeatingType = getOldHeatingSystemType();
		    }
		    rebateValue = rebateCalculator.calculateRebateForOthers(getSEERValue(),
                    ((float) getTonnage() / AppConstants.ONE_TON_TO_BTUH),
                    oldHeatingType, getRebateCalcDate(rebateCalcDateId));
		}
		else if(equipmentId.contains(AppConstants.HEATPUMP_ID)) {
		    rebateValue = rebateCalculator.calculateRebateForOthers(getSEERValue(),
                    getTonnage(), getOldHeatingSystemType(), getRebateCalcDate(rebateCalcDateId));
		}
		else
		{
		    rebateValue = rebateCalculator.calculateRebateForOthers(getSEERValue(),
                    getTonnage(), getOldHeatingSystemType(), getRebateCalcDate(rebateCalcDateId));
		}
	    }

	    return rebateValue;
	}

    public float computeProjectCostRebate(String rebateType) {
        ProjectCostRebateCalculation rebateCalculator = new ProjectCostRebateCalculation();
        return rebateCalculator.getRebateValue(rebateType, getProjectCost(), getQuantity());
    }

    @Override
	protected Object clone() throws CloneNotSupportedException
	{
	    Item item = new Item();

	    if (replaceOnFail != null && replaceOnFail.getParts() != null)
	    {
		Category newReplaceOnFail = new Category();
		newReplaceOnFail = (Category) replaceOnFail.createObject();
		item.replaceOnFail = newReplaceOnFail;
	    }
	    if (earlyRetirement != null && earlyRetirement.getParts() != null)
	    {
		Category newEarlyRetirement = new Category();
		newEarlyRetirement = (Category) earlyRetirement.createObject();
		item.earlyRetirement = newEarlyRetirement;
	    }

	    item.setReplaceOnFailSelected(true);
	    // long lDateTime = new Date().getTime();
	    // Log.v("Varun", "clone " + lDateTime);
	    // setId(lDateTime);
	    return item;
	}

	@JsonIgnore
	public float getTonnage()
	{
	    try
	    {
		Category select = replaceOnFail;
		Part part = select.getPartById("tonnage");
		return LibUtils.parseFloat(part.getSavedValue(), 0);
	    }
	    catch (Exception e)
	    {
		ICFLogger.e(TAG, e);
	    }
	    return 0.0f;
	}

	@JsonIgnore
	public float getTonnageForGeoThermal()
	{
	    try
	    {
		Category select = earlyRetirement;
		Part part = select.getPartById("tonnage");
		return LibUtils.parseFloat(part.getSavedValue(), 0);
	    }
	    catch (Exception e)
	    {
		ICFLogger.e(TAG, e);
	    }
	    return 0.0f;
	}
	
	@JsonIgnore
	public float getNewTonnageForGeothermal() {
	    Category select = replaceOnFail;
	    Part part = select.getPartById("tonnage");
	    return LibUtils.parseFloat(part.getSavedValue(), 0);
	}

	@JsonIgnore
	public String getSEERValue()
	{
	    Category select = replaceOnFail;
	    Part part = select.getPartById(AppConstants.SEER_ID);
	    if (part != null)
	    {
		return part.getSavedValue();
	    }
	    return null;
	}

    @JsonIgnore
    public String getRebateCalcDate(String rebateCalcDateId)
    {
        Category select = replaceOnFail;
        Part part = select.getPartById(rebateCalcDateId);
        if (part != null)
        {
            return part.getSavedValue();
        }
        return null;
    }

    @JsonIgnore
	public String getSEERValueForGeothermal()
	{
	    String selectedType = getGeothermalSystemType();

	    if (selectedType != null)
	    {
		Category select = replaceOnFail;
		Part part = select.getPartByIdAndSelectType(AppConstants.SEER_ID, selectedType);
		if (part != null)
		{
		    return part.getSavedValue();
		}
	    }
	    return null;
	}

	@JsonIgnore
	public String getGeothermalSystemType()
	{
	    Category select = replaceOnFail;
	    Part part = select.getPartById(AppConstants.SYSTEM_TYPE);
	    if (part != null)
	    {
		return part.getSavedValue();
	    }
	    return null;
	}

	@JsonIgnore
	public boolean isBrushlessMotorChecked()
	{
	    if (replaceOnFail != null)
	    {
		Part part = replaceOnFail.getPartById(AppConstants.BRUSHLESS_PERMANENT_MAGNET_MOTOR_CHECKBOX_ID);
		if (part != null)
		{
		    return Boolean.parseBoolean(part.getSavedValue());
		}
	    }
	    return false;
	}

	@JsonIgnore
	public boolean isRetrofitBrushlessMotorChecked()
	{
	    if (replaceOnFail != null)
	    {
		Part part = replaceOnFail.getPartById(AppConstants.OLD_BLOWER_MOTOR_CHECKBOX_ID);
		if (part != null)
		{
		    return Boolean.parseBoolean(part.getSavedValue());
		}
	    }
	    return false;
	}

	@JsonIgnore
	public String getECMAHRIDirChecked()
	{
	    String retVal = null;

	    if (replaceOnFail != null)
	    {
		Part part = replaceOnFail.getPartById(AppConstants.BRUSHLESS_PERMANENT_MAGNET_MOTOR_ECM_AHRI_ID);
		if (part != null)
		{
		    if (part.getSavedValue() != null && part.getSavedValue().equalsIgnoreCase("true"))
		    {
			retVal = "Yes";
		    }
		    else
		    {
			retVal = "No";
		    }
		}
	    }
	    return retVal;
	}

	@JsonIgnore
	public String getWaterHeaterType()
	{
	    Part part = replaceOnFail.getPartById(AppConstants.NEW_WATER_HEATER_ID);
	    if (part != null)
	    {
		return part.getSavedValue();
	    }
	    return null;
	}

	@JsonIgnore
	public String getOldHeatingSystemTypeForWaterHeaterType()
	{
	    Category select = replaceOnFail;
	    Part part = select.getPartById(AppConstants.MINI_SPLITS_OLD_HEATING_SYSTEM_TYPE_ID);
	    if (part != null)
	    {
		return part.getSavedValue();
	    }
	    return null;
	}

	@JsonIgnore
	public String getWaterHeaterEfficiency()
	{
	    String eff = null;
	    Part part = replaceOnFail.getPartById(AppConstants.WATER_HEATER_EFFICIENCY_ID);
	    if (part != null && part.getSavedValue() != null)
	    {
		try
		{
		    eff = Float.parseFloat(part.getSavedValue()) + "";
		}
		catch (Exception e)
		{

		}

	    }
	    return eff;
	}

	@JsonIgnore
	public String getFurnaceEfficiency()
	{
	    String efficiency = null;

	    Part part = replaceOnFail.getPartById(AppConstants.FURNACE_EFFICIENCY_ID);
	    if (part != null && part.getSavedValue() != null)
	    {
		try
		{
		    efficiency = Float.parseFloat(part.getSavedValue()) + "";
		}
		catch (Exception e)
		{

		}
	    }
	    return efficiency;
	}

	@JsonIgnore
	public String getBoilerFurnaceEfficiency()
	{
	    String efficiency = null;
	    Part part = replaceOnFail.getPartById(AppConstants.BOILER_EFFICIENCY_ID);
	    if (part != null && part.getSavedValue() != null)
	    {
		try
		{
		    efficiency = Float.parseFloat(part.getSavedValue()) + "";
		}
		catch (Exception e)
		{

		}
	    }

	    return efficiency;
	}

	
	@JsonIgnore
	public String getBoilerOldHeatingSystemType()
	{
	    Category select = replaceOnFail;
	    String type = null;
	    // if (!replaceOnFailSelected)
	    {
		Part part = select.getPartById(AppConstants.BOILER_OLD_HEATING_SYSTEM_TYPE_ID);
		if (part != null)
		{
		    type = part.getSavedValue();
		}
	    }

	    return type;
	}
	
	@JsonIgnore
	public String getOldHeatingSystemType() {
	    Category select = replaceOnFail;
	    String type = null;
	    Part part = select.getPartById(AppConstants.OLD_HEATING_SYSTEM_TYPE_ID);
	    if (part != null) {
		type = part.getSavedValue();
	    }
	    return type;
	}

	@JsonIgnore
	public String getFurnanceOldHeatingSystemType()
	{
	    Category select = replaceOnFail;
	    String type = null;
	    // if (!replaceOnFailSelected)
	    {
		Part part = select.getPartById(AppConstants.FURNACE_OLD_HEATING_SYSTEM_TYPE_ID);
		if (part != null)
		{
		    type = part.getSavedValue();
		}
	    }

	    return type;
	}

	@JsonIgnore
	public boolean isBoilerEligibleForOldHeatingSystemType(String type)
	{
	    boolean retVal = true;

	    if (type != null && type.indexOf(AppConstants.AC_FURNACE_SUFFIX) != -1)
	    {
		Category select = replaceOnFail;
		Part part = select.getPartById(AppConstants.BOILER_OLD_SYSTEM_EFFICIENCY_ID);

		if (part != null && !TextUtils.isEmpty(part.getSavedValue()) && (!part.isHidden() && Float.parseFloat(part.getSavedValue()) > AppConstants.BOILER_OLD_SYSTEM_EFFICIENCY_THERSHOLD_VALUE))
		{
		    retVal = false;
		}
	    }

	    return retVal;
	}

	@JsonIgnore
	public boolean isFurnaceEligibleForOldHeatingSystemType(String type)
	{
	    boolean retVal = true;

	    if (type != null && (type.indexOf(AppConstants.AC_FURNACE_SUFFIX) != -1 || type.equalsIgnoreCase("Dual Fuel Heating System")))
	    {
		Category select = replaceOnFail;
		Part part = select.getPartById(AppConstants.FURNACE_OLD_SYSTEM_EFFICIENCY_ID);

		if (part != null && !TextUtils.isEmpty(part.getSavedValue()) && (Float.parseFloat(part.getSavedValue()) > AppConstants.BOILER_OLD_SYSTEM_EFFICIENCY_THERSHOLD_VALUE))
		{
		    retVal = false;
		}
	    }

	    return retVal;
	}

    @JsonIgnore
    public float getProjectCost()
    {
        try
        {
            Category select = replaceOnFail;
            Part part = select.getPartById(AppConstants.PROJECT_COST);
            return LibUtils.parseFloat(part.getSavedValue(), 0f);
        }
        catch (Exception e)
        {
            ICFLogger.e(TAG, e);
        }
        return 0.0f;
    }

    @JsonIgnore
    public int getQuantity()
    {
        try
        {
            Category select = replaceOnFail;
            Part part = select.getPartById(AppConstants.QUANTITY);
            return LibUtils.parseInteger(part.getSavedValue(), 0);
        }
        catch (Exception e)
        {
            ICFLogger.e(TAG, e);
        }
        return 0;
    }

    public Category getReplaceOnFail()
{
    return replaceOnFail;
}

	public void setReplaceOnFail(Category replaceOnFail)
	{
	    this.replaceOnFail = replaceOnFail;
	}

	public Category getEarlyRetirement()
	{
	    if (replaceOnFailSelected)
	    {
		if (UiUtil.serializationMode)
		{
		    return null;
		}
	    }
	    return earlyRetirement;
	}

	public void setEarlyRetirement(Category earlyRetirement)
	{
	    this.earlyRetirement = earlyRetirement;
	}

	public long getId()
	{
	    return id;
	}

	public void setId(long id)
	{
	    this.id = id;
	}

	public boolean isReplaceOnFailSelected()
	{
	    return replaceOnFailSelected;
	}

	public void setReplaceOnFailSelected(boolean replaceOnFailSelected)
	{
	    this.replaceOnFailSelected = replaceOnFailSelected;
	}

	public boolean isItemSaved()
	{
	    return itemSaved;
	}

	public void setItemSaved(boolean itemSaved)
	{
	    this.itemSaved = itemSaved;
	}

    }

    public static class Part implements Serializable, FormObjectBuilder
    {
	private static final long serialVersionUID = 3708512095169311695L;

	@JsonProperty("id")
	String id;
	@JsonProperty("name")
	String name;
	@JsonProperty("picture")
	String picture;
	@JsonProperty("options")
	List<Options> options;
	@JsonProperty("mandatory")
	String mandatory;
	@JsonProperty("hidden")
	String hidden;
	@JsonProperty("storeImagePath")
	String imagePath;
	@JsonProperty("xmlName")
	String xmlName;
	@JsonProperty("systemType")
	String systemType;

	long duplicatePartId = -1;

	public Part()
	{

	}

	boolean isItemSaved = false;

	public boolean isItemSaved()
	{
	    return isItemSaved;
	}

	public void setItemSaved(boolean state)
	{
	    isItemSaved = state;
	}

	@JsonIgnore
	String getSavedValue()
	{
	    return options != null && options.size() > 0 ? options.get(0).savedValue : null;
	}

	public Part(String id)
	{
	    this.id = id;
	}

	@Override
	public boolean equals(Object obj)
	{
	    Part p = (Part) obj;
	    if (p == null || p.id == null || this.id == null)
	    {
		return false;
	    }
	    return p.id.equals(this.id) && ((this.duplicatePartId > -1) ? p.duplicatePartId == this.duplicatePartId : true);
	}

	public String getXmlName()
	{
	    return xmlName;
	}

	public void setXmlName(String xmlName)
	{
	    this.xmlName = xmlName;
	}

	public String getId()
	{
	    return id;
	}

	public void setId(String id)
	{
	    this.id = id;
	}

	public boolean isHidden()
	{
	    return Boolean.parseBoolean(hidden);
	}

	public void setHidden(String hidden)
	{
	    this.hidden = hidden;
	}

	public String getName()
	{
	    return name;
	}

	public void setName(String name)
	{
	    this.name = name;
	}

	public boolean hasPicture()
	{
	    return Boolean.parseBoolean(picture);
	}

	public void setPicture(String picture)
	{
	    this.picture = picture;
	}

	public List<Options> getOptions()
	{
	    return options;
	}

	public void setOptions(List<Options> options)
	{
	    this.options = options;
	}

	@JsonIgnore
	public void addOptions(Options option)
	{
	    if (this.options == null)
	    {
		this.options = new ArrayList<FormResponseBean.Options>();
	    }
	    this.options.add(option);
	}

	@JsonIgnore
	public void removeOptions(Options option)
	{
	    if (this.options != null)
	    {
		this.options.remove(option);
	    }
	}

	public boolean isMandatory()
	{
	    return mandatory == null ? true : Boolean.parseBoolean(mandatory);
	}

	public void setMandatory(String mandatory)
	{
	    this.mandatory = mandatory;
	}

	public String getImagePath()
	{
	    return imagePath;
	}

	public void setImagePath(String imagePath)
	{
	    this.imagePath = imagePath;
	}

	public String getSystemType()
	{
	    return systemType;
	}

	public void setSystemType(String systemType)
	{
	    this.systemType = systemType;
	}

	public long getDuplicatePartId()
	{
	    return duplicatePartId;
	}

	public void setDuplicatePartId()
	{
	    this.duplicatePartId = new Random().nextInt(10000);
	}

	@Override
	public Object createObject()
	{
	    Part newPart = new Part();
	    // String id = new Random().nextInt(10000) + "";
	    newPart.id = id;
	    newPart.name = name;
	    newPart.picture = picture;
	    newPart.mandatory = mandatory;
	    newPart.hidden = hidden;

	    List<Options> optionList = new ArrayList<Options>();
	    if (options != null)
	    {
		ListIterator<Options> iterator = options.listIterator();
		while (iterator.hasNext())
		{
			Options seedOption = (Options) iterator.next();
		    Options option = (Options) seedOption.createObject();
			//Hidden parts are used to define default values required
			//for rebate calculation but are unknown or irrelevant to the
			//user input. Copy the default value to the new part instance
			if (Boolean.parseBoolean(hidden)) {
				option.setSavedValue(seedOption.getSavedValue());
			}
		    optionList.add(option);
		}
		newPart.options = optionList;
	    }

	    newPart.imagePath = null;
	    newPart.xmlName = xmlName;
	    newPart.systemType = systemType;
	    newPart.duplicatePartId = duplicatePartId;

	    return newPart;
	}

	public Part createDuplicatePart()
	{
	    Part newPart = (Part) createObject();
	    newPart.setDuplicatePartId();
	    return newPart;

	}

    }

    public static class Options implements Serializable, FormObjectBuilder
    {
	/**
	     * 
	     */
	private static final long serialVersionUID = 2098050497949503041L;
	@JsonProperty("id")
	String id;
	@JsonProperty("name")
	String name;
	@JsonProperty("dataType")
	String dataType;
	@JsonProperty("barcode")
	String barcode;
	@JsonProperty("reference")
	Reference reference;
	@JsonProperty("values")
	List<String> values;
	// KVB 12/3/2015 -- Adding support for data mapping to values 
	@JsonProperty("data")
	List<String> data;
	//-----------------------------------------------------------
	@JsonProperty("barcodeScanned")
	boolean barcodeScanned;
	@JsonProperty("mandatory")
	String mandatory;
	@JsonProperty("inputType")
	String inputType;
	@JsonProperty("numberOfLines")
	String numberOfLines;
	@JsonProperty("hidden")
	String hidden;

	@JsonProperty("storeSavedValue")
	String savedValue;

	@JsonProperty("xmlName")
	String xmlName;

	@JsonProperty("editable")
	String editable;

	@JsonProperty("defaultSystemType")
	String defaultSystemType;

	@JsonProperty("format")
	String format;

	@JsonProperty("minLength")
	String minLength;

	@JsonProperty("externalReferId")
	String externalReferId;

	@JsonProperty("packageName")
	String packageName;

	@JsonProperty("launchUri")
	String launchUri;

	@JsonProperty("suggestion")
	String suggestion;

	@JsonProperty("digitGrouping")
	String digitGrouping;
	
	@JsonProperty("minValue")
	String minValue;
	
	@JsonProperty("maxValue")
	String maxValue;

	@JsonProperty("staticDisplayedValue")
	String staticDisplayedValue;

	public List<String> getData()
	{
	    return data;
	}

	public void setData(List<String> data)
	{
	    this.data = data;
	}

	public String getDigitGrouping()
	{
	    return digitGrouping;
	}

	public void setDigitGrouping(String digitGrouping)
	{
	    this.digitGrouping = digitGrouping;
	}

	public String getSuggestion()
	{
	    return suggestion;
	}

	public void setSuggestion(String suggestion)
	{
	    this.suggestion = suggestion;
	}

	boolean isItemSaved = false;

	public boolean isItemSaved()
	{
	    return isItemSaved;
	}

	public void setItemSaved(boolean state)
	{
	    isItemSaved = state;
	}

	public String getFormat()
	{
	    return format;
	}

	public void setFormat(String format)
	{
	    this.format = format;
	}

	public String getMinLength()
	{
	    return minLength;
	}

	public void setMinLength(String minLength)
	{
	    this.minLength = minLength;
	}

	public String getXmlName()
	{
	    return xmlName;
	}

	public void setXmlName(String xmlName)
	{
	    this.xmlName = xmlName;
	}

	public String getId()
	{
	    return id;
	}

	public void setId(String id)
	{
	    this.id = id;
	}

	public boolean isHidden()
	{
	    return Boolean.parseBoolean(hidden);
	}

	public void setHidden(String hidden)
	{
	    this.hidden = hidden;
	}

	public String getName()
	{
	    return name;
	}

	public void setName(String name)
	{
	    this.name = name;
	}

	public String getDataType()
	{
	    return dataType;
	}

	public void setDataType(String dataType)
	{
	    this.dataType = dataType;
	}

	public boolean hasBarcode()
	{
	    return Boolean.parseBoolean(barcode);
	}

	public void setBarcode(String barcode)
	{
	    this.barcode = barcode;
	}

	public List<String> getValues()
	{
	    return values;
	}

	public void setValues(List<String> values)
	{
	    this.values = values;
	}

	public boolean isMandatory()
	{
	    return mandatory == null ? true : Boolean.parseBoolean(mandatory);
	}

	public void setMandatory(String mandatory)
	{
	    this.mandatory = mandatory;
	}

	public boolean isFieldFilled()
	{
	    return savedValue != null && !TextUtils.isEmpty(savedValue);
	}

	public String getInputType()
	{
	    return inputType;
	}

	public void setInputType(String inputType)
	{
	    this.inputType = inputType;
	}

	public String getSavedValue()
	{
	    return savedValue;
	}

	public void setSavedValue(String savedValue)
	{
	    this.savedValue = savedValue;
	}

	public Reference getReference()
	{
	    return reference;
	}

	public void setReference(Reference reference)
	{
	    this.reference = reference;
	}

	public boolean isEditable()
	{
	    return editable != null ? Boolean.parseBoolean(editable) : true;
	}

	public void setEditable(String editable)
	{
	    this.editable = editable;
	}

	public String getDefaultSystemType()
	{
	    return defaultSystemType;
	}

	public void setDefaultSystemType(String defaultSystemType)
	{
	    this.defaultSystemType = defaultSystemType;
	}

	public void setExternalReferId(String externalReferId)
	{
	    this.externalReferId = externalReferId;
	}

	public String getExternalReferId()
	{
	    return this.externalReferId;
	}

	public String getPackageName()
	{
	    return packageName;
	}

	public void setPackageName(String packageName)
	{
	    this.packageName = packageName;
	}

	public String getLaunchUri()
	{
	    return launchUri;
	}

	public void setLaunchUri(String launchUri)
	{
	    this.launchUri = launchUri;
	}

	public String getNumberOfLines()
	{
	    return numberOfLines;
	}

	public void setNumberOfLines(String numberOfLines)
	{
	    this.numberOfLines = numberOfLines;
	}

	public String getMinValue()
	{
	    return minValue;
	}

	public void setMinValue(String minValue)
	{
	    this.minValue = minValue;
	}

	public String getMaxValue()
	{
	    return maxValue;
	}

	public void setMaxValue(String maxValue)
	{
	    this.maxValue = maxValue;
	}
	
	public boolean getBarcodeScanned()
	{
	    return barcodeScanned;
	}

	public void setBarcodeScanned(boolean isBarcodeScanned)
	{
	    this.barcodeScanned = isBarcodeScanned;
	}

	public String getStaticDisplayedValue()
	{
		return staticDisplayedValue;
	}

	public void setStaticDisplayed(String staticDisplayedValue)
	{
		this.staticDisplayedValue = staticDisplayedValue;
	}

	public static class Reference
	{
	    @JsonProperty("name")
	    String name;
	    @JsonProperty("referId")
	    String referId;

	    @JsonProperty("storeIsChecked")
	    boolean isChecked;

	    public String getName()
	    {
		return name;
	    }

	    public void setName(String name)
	    {
		this.name = name;
	    }

	    public String getReferId()
	    {
		return referId;
	    }

	    public void setReferId(String referId)
	    {
		this.referId = referId;
	    }

	    public boolean isChecked()
	    {
		return isChecked;
	    }

	    public void setChecked(boolean isChecked)
	    {
		this.isChecked = isChecked;
	    }

	}

	@Override
	public Object createObject()
	{
	    Options newOptn = new Options();
	    // String id = new Random().nextInt(10000) + "";
	    newOptn.id = id;
	    newOptn.name = name;
	    newOptn.dataType = dataType;
	    newOptn.barcode = barcode;

	    if (reference != null)
	    {
            newOptn.reference = new Reference();
            newOptn.reference.isChecked = false;
            newOptn.reference.referId = reference.referId;
            newOptn.reference.name = reference.name;
	    }

	    newOptn.values = values;
	    newOptn.data = data;
	    newOptn.mandatory = mandatory;
	    newOptn.inputType = inputType;
		newOptn.numberOfLines = numberOfLines;
		newOptn.hidden = hidden;
		newOptn.xmlName = xmlName;
		newOptn.editable = editable;
	    newOptn.defaultSystemType = defaultSystemType;
	    newOptn.format = format;
	    newOptn.minLength = minLength;
	    newOptn.externalReferId = externalReferId;
	    newOptn.packageName = packageName;
	    newOptn.launchUri = launchUri;
		newOptn.suggestion = suggestion;
	    newOptn.digitGrouping = digitGrouping;
	    newOptn.suggestion = suggestion;
        newOptn.minValue = minValue;
        newOptn.maxValue = maxValue;

	    newOptn.savedValue = null;
        newOptn.barcodeScanned = false;

	    return newOptn;
	}
    }
    

    public List<Equipment> getEquipments()
    {
	if (filteredEquipmentList == null)
	{
	    filteredEquipmentList = filterEquipmentsByCustomerType((ArrayList<Equipment>) equipments);
	}
	ArrayList<Equipment> list = null;
	if (serializationMode)
	{
	    if (filteredEquipmentList != null)
	    {
		for (Equipment item : filteredEquipmentList)
		{
		    ArrayList<Item> itemList = (ArrayList<Item>) item.getItems();
		    for (int i = 0; i < itemList.size(); i++)
		    {
			if (list == null)
			{
			    list = new ArrayList<Equipment>();
			}
			if (itemList.get(i).isItemSaved())
			{
			    list.add(item);
			    break;
			}
		    }
		}
	    }
	    return list;
	}
	return filteredEquipmentList;
    }

    public void setEquipments(List<Equipment> equipments)
    {
	this.equipments = equipments;
    }

    public void filterEquipmentList()
    {
	filteredEquipmentList = filterEquipmentsByCustomerType((ArrayList<Equipment>) equipments);
    }

    private List<Equipment> filterEquipmentsByCustomerType(ArrayList<Equipment> equipments)
    {
	if (equipments == null)
	{
	    return null;
	}
	try
	{
	    List<Equipment> filteredEquipmentList = (List<Equipment>) equipments.clone();
	    String customerType = appDetails.getCustomerType();
	    List<String> filterList = null;
	    if (customerType == null || customerType.equalsIgnoreCase("NA"))
	    {
		return filteredEquipmentList;
	    }
	    else
	    {
		filterList = this.appDetails.getEquipmentListFilter().get(customerType);
	    }

	    if (filterList == null || filterList.isEmpty())
	    {
		return filteredEquipmentList;
	    }

        filteredEquipmentList.clear();
	    for (String equipmentId : filterList)
	    {
		    Equipment equipment = new Equipment(equipmentId);
            int i = ((List<Equipment>) equipments).indexOf(equipment);
            if (i > -1 ) {
                filteredEquipmentList.add(equipments.get(i));
            }
        }
	    return filteredEquipmentList;
	}
	catch (Exception e)
	{
	    ICFLogger.e(TAG, e);
	}
	return equipments;
    }

    public List<DuctItem> getDuctImprovements()
    {
	return ductImprovements;
    }

    public void setDuctImprovements(List<DuctItem> ductImprovements)
    {
	this.ductImprovements = ductImprovements;
    }

    public List<QIVItem> getQivList()
    {
	ArrayList<QIVItem> list = null;
	if (serializationMode)
	{
	    if (qivList != null)
	    {
		for (QIVItem item : qivList)
		{
		    if (item.isItemSaved() && item.getFormFilledState() == AppConstants.FORM_ITEM_FILLED_STATE.ITEM_COMPLETELY_SAVED.getState())
		    {
			if (list == null)
			{
			    list = new ArrayList<QIVItem>();
			}
			list.add(item);
		    }
		}
	    }
	    return list;
	}
	return qivList;
    }

    public void setQivList(List<QIVItem> qivList)
    {
	this.qivList = qivList;
    }

	public List<ThermostatReferralItem> getThermostatReferralList() {
    if (thermostatReferralList == null) thermostatReferralList = new ArrayList<ThermostatReferralItem>();
	if (serializationMode) {
		ListIterator<ThermostatReferralItem> iterator = thermostatReferralList.listIterator();
		while (iterator.hasNext()) {
			ThermostatReferralItem thermostatReferralItem = (ThermostatReferralItem) iterator.next();
			if (serviceList != null && serviceList.indexOf(new ServiceItem(AppConstants.THERMOSTATREFERRAL_ID)) > -1) {

				List<Part> parts = null;
				List<Options> options = null;
				Options option = null;
				Part part = null;

				parts = thermostatReferralItem.getParts();
				if (parts == null) {
					parts = new ArrayList<FormResponseBean.Part>();
					thermostatReferralItem.setParts(parts);
				}
				if (parts.size() == 0) {
					part = new Part();
					part.setXmlName(AppConstants.THERMOSTATREFERRAL_XML_NAME);
					part.setId(AppConstants.THERMOSTATREFERRAL_XML_NAME);
					options = new ArrayList<FormResponseBean.Options>();
					option = new Options();
					options.add(option);
					part.setOptions(options);
					parts.add(part);
				} else {
					parts = thermostatReferralItem.getParts();
					part = parts.get(0);
					options = part.getOptions();
					if (options.size() == 0) {
						option = new Options();
						options.add(option);
					} else {
						option = options.get(0);
					}
				}
				option.setSavedValue(isThermostatReferralChecked ? "true" : "false");
			} else {
				thermostatReferralItem.setParts(null);
			}

			break;
		}

	}
	return thermostatReferralList;
}

    public void setThermostatReferralList(List<ThermostatReferralItem> thermostatReferralList)
    {
	this.thermostatReferralList = thermostatReferralList;
    }

    public CustomerInfo getCustomerInfo()
    {
	return customerInfo;
    }

    public void setCustomerInfo(CustomerInfo customerInfo)
    {
	this.customerInfo = customerInfo;
    }

    public List<TuneUp> getTuneUp() {
		if (filteredTuneUpList == null) {
			String customerType = appDetails.getCustomerType();
			if (TextUtils.isEmpty(customerType) || customerType.equals("combo")) {
				filteredTuneUpList = tuneUp;
			} else {
				filteredTuneUpList = new ArrayList<TuneUp>();
				List<String> custTypeEquips =
			        appDetails.getEquipmentListFilter().get(customerType);
				for (TuneUp _tuneUp : tuneUp) {
					if (custTypeEquips.contains(_tuneUp.getJsonId())) {
						filteredTuneUpList.add(_tuneUp);
					}
				}
			}
		}
		return filteredTuneUpList;
    }
    
    public void setTuneUp(List<TuneUp> tuneUps) {
	this.tuneUp = tuneUps;
    }

	public List<DuctSealing> getDuctSealing() {
		if (filteredDuctSealingList == null) {
			String houseType = appDetails.getHouseType();
			if (TextUtils.isEmpty(houseType)) {
				filteredDuctSealingList = ductSealing;
			} else {
				filteredDuctSealingList = new ArrayList<DuctSealing>();
                if (appDetails.getAirDuctSealingHouseTypeFilter() != null) {
                    List<String> houseTypeEquips =
                            appDetails.getAirDuctSealingHouseTypeFilter().get(houseType);
                    for (DuctSealing _ductSealing : ductSealing) {
                        if (houseTypeEquips.contains(_ductSealing.getId())) {
                            filteredDuctSealingList.add(_ductSealing);
                        }
                    }
                }
			}
		}
		return filteredDuctSealingList;
	}

	public void setDuctSealing(List<DuctSealing> ductSealing) {
		this.ductSealing = ductSealing;
	}

    public List<AirSealing> getAirSealing(){
		if (filteredAirSealingList == null) {
			String houseType = appDetails.getHouseType();
			if (TextUtils.isEmpty(houseType)) {
				filteredAirSealingList = airSealing;
			} else {
				filteredAirSealingList = new ArrayList<AirSealing>();
				List<String> houseTypeEquips =
						appDetails.getAirDuctSealingHouseTypeFilter().get(houseType);
				for (AirSealing _airSealing : airSealing) {
					if (houseTypeEquips.contains(_airSealing.getId())) {
						filteredAirSealingList.add(_airSealing);
					}
				}
			}
		}
		return filteredAirSealingList;
	}

    public void setAirSealing(List<AirSealing> airSealing)
    {
	this.airSealing = airSealing;
    }

    public AppDetails getAppDetails()
    {
	return appDetails;
    }

    @JsonIgnore
    public AppDetails getClonedAppDetails()
    {
	try
	{
	    return appDetails.clone();
	}
	catch (CloneNotSupportedException e)
	{
	    e.printStackTrace();
	    return appDetails;
	}
    }
    
    @JsonIgnore
    public CustomerScreenLayout getClonedCustomerScreenLayout() {
	// This bean may or may not exist given the contents of form_data.json
	if (customerScreenLayout == null) {
	    return null;
	}
	try {
	    return customerScreenLayout.clone();
	} catch(CloneNotSupportedException e) {
	    e.printStackTrace();
	    return customerScreenLayout;
	}
    }

    public void setAppDetails(AppDetails appDetails)
    {
	this.appDetails = appDetails;
    }

    @JsonIgnore
    public void initSerializationMode()
    {
	if (UiUtil.isJobCompleted(getCustomerInfo()) || UiUtil.isJobSubmitted(getCustomerInfo()))
	{
	    serializationMode = true;
	    UiUtil.serializationMode = true;
	}
	else
	{
	    serializationMode = false;
	    UiUtil.serializationMode = false;
	}
    }

    @JsonIgnore
    public void resetSerializationMode()
    {
	this.serializationMode = false;
	UiUtil.serializationMode = false;
    }

    public List<Insulation> getInsulation()
    {
        return insulation;
    }

    public void setInsulation(List<Insulation> insulation)
    {
        this.insulation = insulation;
    }

    public List<WindowAndDoor> getWindowAndDoor()
    {
        return windowAndDoor;
    }

    public void setWindowAndDoor(List<WindowAndDoor> windowAndDoor)
    {
        this.windowAndDoor = windowAndDoor;
    }

    public List<Submission> getSubmission()
    {
        return submissionItems;
    }

    public void setSubmission(List<Submission> submissionItems)
    {
        this.submissionItems = submissionItems;
    }
    
    

    @JsonIgnore
    public Submission getSubmissionObjectById(String id) {
	Submission obj = null;
	if (submissionItems != null && submissionItems.size() > 0) {
	    int index = submissionItems.indexOf(new Submission(id));
	    if (index != -1) {
		obj = submissionItems.get(index);
	    }
	}
	return obj;
    }

    public CustomerScreenLayout getCustomerScreenLayout()
    {
        return customerScreenLayout;
    }

    public void setCustomerScreenLayout(CustomerScreenLayout customerScreenLayout)
    {
        this.customerScreenLayout = customerScreenLayout;
    }
}
