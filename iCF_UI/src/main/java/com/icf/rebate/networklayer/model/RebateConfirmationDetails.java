package com.icf.rebate.networklayer.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RebateConfirmationDetails
{
    @JsonProperty("items")
    List<RebateConfirmationItem> items;

    public List<RebateConfirmationItem> getItems()
    {
        return items;
    }

    public void setItems(List<RebateConfirmationItem> items)
    {
        this.items = items;
    }
}
