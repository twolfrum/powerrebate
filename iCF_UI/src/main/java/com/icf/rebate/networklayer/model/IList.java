package com.icf.rebate.networklayer.model;

import java.util.ArrayList;
import java.util.Collection;

public class IList<T> extends ArrayList<T>
{
    /**
     * 
     */
    private static final long serialVersionUID = -2867174765138848405L;

    @Override
    public boolean add(T object)
    {
	return super.add(object);
    }

    @Override
    public void add(int index, T object)
    {
	super.add(index, object);
    }

    @Override
    public boolean addAll(Collection<? extends T> collection)
    {
	return super.addAll(collection);
    }
}
