package com.icf.rebate.networklayer.model.serializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.icf.rebate.networklayer.model.FormResponseBean;

public class FormResponseBeanSerializer extends JsonSerializer<FormResponseBean>
{
    @Override
    public void serialize(FormResponseBean bean, JsonGenerator jsonGenerator, SerializerProvider arg2) throws IOException, JsonProcessingException
    {
	jsonGenerator.writeStartObject();

	if (bean.getCustomerInfo() != null)
	{
	    jsonGenerator.writeObjectField("customerDetails", bean.getCustomerInfo());
	}

	if (bean.getClonedAppDetails() != null)
	{
	    jsonGenerator.writeObjectField("appDetails", bean.getAppDetails());
	}

	if (bean.getContractorDetail() != null)
	{
	    jsonGenerator.writeObjectField("contractorDetail", bean.getContractorDetail());
	}

	jsonGenerator.writeEndObject();
    }
}
