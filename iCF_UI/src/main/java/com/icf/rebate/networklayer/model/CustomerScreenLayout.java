
package com.icf.rebate.networklayer.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Ken Butler Jan 20, 2016 
 * 
 * CustomerScreen.java - allows ui elements on {@link:CustomerInformationFragment} 
 * to be adjusted based on customerScreenLayout in form_data.json 
 * 
 * Copyright (c) 2016 ICF International
 */
public class CustomerScreenLayout implements Cloneable
{
    @JsonProperty("sendRebateToDifferentPerson")
    private String sendRebateToDifferentPerson;
    @JsonProperty("phone")
    private CustomerScreenItem phone;
    @JsonProperty("state")
    private String state;
    @JsonProperty("singleFamily")
    private String singleFamily;
    @JsonProperty("differentEmail")
    private String differentEmail;
    @JsonProperty("approximateYearHouseBuilt") //free text entry
    private CustomerScreenItem approximateYearHouseBuilt;
    @JsonProperty("yearBuilt") //drop down list of year ranges
    private CustomerScreenItem yearBuilt;
    @JsonProperty("city")
    private String city;
    @JsonProperty("accountNumber")
    private CustomerScreenItem accountNumber;
    @JsonProperty("differentPhone")
    private String differentPhone;
    @JsonProperty("differentFirstName")
    private String differentFirstName;
    @JsonProperty("differentCity")
    private String differentCity;
    @JsonProperty("firstName")
    private String firstName;
    @JsonProperty("zip")
    private String zip;
    @JsonProperty("lastName")
    private String lastName;
    @JsonProperty("accountNumber2")
    private CustomerScreenItem accountNumber2;
    @JsonProperty("heatingCoolingSystem")
    private CustomerScreenItem heatingCoolingSystem;
    @JsonProperty("differentZip")
    private String differentZip;
    @JsonProperty("differentLastName")
    private String differentLastName;
    @JsonProperty("differentState")
    private String differentState;
    @JsonProperty("approximateSizeofHouse")
    private CustomerScreenItem approximateSizeofHouse;
    @JsonProperty("sendRebateToHVACContractor")
    private String sendRebateToHVACContractor;
    @JsonProperty("differentAddress")
    private String differentAddress;
    @JsonProperty("email")
    private CustomerScreenItem email;
    @JsonProperty("address")
    private String address;
    @JsonProperty("basementFoundation")
    private String basementFoundation;
    @JsonProperty("multiFamily")
    private String multiFamily;
    @JsonProperty("sendRebateToCustomer")
    private String sendRebateToCustomer;
    @JsonProperty("sendRebateToMultiple")
    private boolean sendRebateToMultiple;
    @JsonProperty("permitNumber")
    private CustomerScreenItem permitNumber;
    @JsonProperty("distributor")
    private CustomerScreenItem distributor;
    @JsonProperty("newConstruction")
    private String newConstruction;
    @JsonProperty("buildingType")
    private CustomerScreenItem buildingType;
    @JsonProperty("waterHeatingFuelType")
    private CustomerScreenItem waterHeatingFuelType;
    @JsonProperty("centralAC")
    private CustomerScreenItem centralAC;
    @JsonProperty("auditorCompanyNameText")
    private CustomerScreenItem auditorCompanyNameText;
    @JsonProperty("heatingCoolingFuelType")
    private CustomerScreenItem heatingCoolingFuelType;
    @JsonProperty("customerType")
    private CustomerScreenItem customerType;
    @JsonProperty("projectTypes")
    private CustomerScreenItem projectTypes;
    @JsonProperty("technicianName")
    private CustomerScreenItem technicianName;
    @JsonProperty("technicianId")
    private CustomerScreenItem technicianId;
    @JsonProperty("technicianPhoneNumber")
    private CustomerScreenItem technicianPhone;
    @JsonProperty("projectIncentive")
    private CustomerScreenItem projectIncentive;
    @JsonProperty("dateInstalled")
    private CustomerScreenItem dateInstalled;

    public String getSendRebateToDifferentPerson()
    {
	return sendRebateToDifferentPerson;
    }

    public void setSendRebateToDifferentPerson(String sendRebateToDifferentPerson)
    {
	this.sendRebateToDifferentPerson = sendRebateToDifferentPerson;
    }

    public CustomerScreenItem getPhone()
    {
	return phone;
    }

    public void setPhone(CustomerScreenItem phone)
    {
	this.phone = phone;
    }

    public String getState()
    {
	return state;
    }

    public void setState(String state)
    {
	this.state = state;
    }

    public String getSingleFamily()
    {
	return singleFamily;
    }

    public void setSingleFamily(String singleFamily)
    {
	this.singleFamily = singleFamily;
    }

    public String getDifferentEmail()
    {
	return differentEmail;
    }

    public void setDifferentEmail(String differentEmail)
    {
	this.differentEmail = differentEmail;
    }

    public String getCity()
    {
	return city;
    }

    public void setCity(String city)
    {
	this.city = city;
    }

    public CustomerScreenItem getAccountNumber()
    {
	return accountNumber;
    }

    public void setAccountNumber(CustomerScreenItem accountNumber)
    {
	this.accountNumber = accountNumber;
    }

    public String getDifferentPhone()
    {
	return differentPhone;
    }

    public void setDifferentPhone(String differentPhone)
    {
	this.differentPhone = differentPhone;
    }

    public String getDifferentFirstName()
    {
	return differentFirstName;
    }

    public void setDifferentFirstName(String differentFirstName)
    {
	this.differentFirstName = differentFirstName;
    }

    public String getDifferentCity()
    {
	return differentCity;
    }

    public void setDifferentCity(String differentCity)
    {
	this.differentCity = differentCity;
    }

    public String getFirstName()
    {
	return firstName;
    }

    public void setFirstName(String firstName)
    {
	this.firstName = firstName;
    }

    public String getZip()
    {
	return zip;
    }

    public void setZip(String zip)
    {
	this.zip = zip;
    }

    public String getLastName()
    {
	return lastName;
    }

    public void setLastName(String lastName)
    {
	this.lastName = lastName;
    }

    public CustomerScreenItem getAccountNumber2()
    {
	return accountNumber2;
    }

    public void setAccountNumber2(CustomerScreenItem accountNumber2)
    {
	this.accountNumber2 = accountNumber2;
    }

    public CustomerScreenItem getHeatingCoolingSystem()
    {
	return heatingCoolingSystem;
    }

    public void setHeatingCoolingSystem(CustomerScreenItem heatingCoolingSystem)
    {
	this.heatingCoolingSystem = heatingCoolingSystem;
    }

    public String getDifferentZip()
    {
	return differentZip;
    }

    public void setDifferentZip(String differentZip)
    {
	this.differentZip = differentZip;
    }

    public String getDifferentLastName()
    {
	return differentLastName;
    }

    public void setDifferentLastName(String differentLastName)
    {
	this.differentLastName = differentLastName;
    }

    public String getDifferentState()
    {
	return differentState;
    }

    public void setDifferentState(String differentState)
    {
	this.differentState = differentState;
    }

    public CustomerScreenItem getApproximateSizeofHouse()
    {
	    return approximateSizeofHouse;
    }

    public void setApproximateSizeofHouse(CustomerScreenItem approximateSizeofHouse)
    {
	this.approximateSizeofHouse = approximateSizeofHouse;
    }

    public CustomerScreenItem getApproximateYearHouseBuilt ()
    {
        return approximateYearHouseBuilt;
    }

    public void setApproximateYearHouseBuilt(CustomerScreenItem approximateYearHouseBuilt)
    {
        this.approximateYearHouseBuilt = approximateYearHouseBuilt;
    }

    public CustomerScreenItem getYearBuilt()
    {
        return yearBuilt;
    }

    public void setYearBuilt(CustomerScreenItem yearBuilt)
    {
        this.yearBuilt = yearBuilt;
    }

    public CustomerScreenItem getDistributor()
    {
        return distributor;
    }

    public void setDistributor(CustomerScreenItem distributor)
    {
        this.distributor = distributor;
    }

    public String getSendRebateToHVACContractor()
    {
	return sendRebateToHVACContractor;
    }

    public void setSendRebateToHVACContractor(String sendRebateToHVACContractor)
    {
	this.sendRebateToHVACContractor = sendRebateToHVACContractor;
    }

    public String getDifferentAddress()
    {
	return differentAddress;
    }

    public void setDifferentAddress(String differentAddress)
    {
	this.differentAddress = differentAddress;
    }

    public boolean getSendRebateToMultiple()
    {
        return sendRebateToMultiple;
    }

    public void setSendRebateToMultiple(boolean sendRebateToMultiple)
    {
        this.sendRebateToMultiple = sendRebateToMultiple;
    }

    public CustomerScreenItem getEmail()
    {
	return email;
    }

    public void setEmail(CustomerScreenItem email)
    {
	this.email = email;
    }

    public String getAddress()
    {
	return address;
    }

    public void setAddress(String address)
    {
	this.address = address;
    }

    public String getBasementFoundation()
    {
	return basementFoundation;
    }

    public void setBasementFoundation(String basementFoundation)
    {
	this.basementFoundation = basementFoundation;
    }

    public String getMultiFamily()
    {
	return multiFamily;
    }

    public void setMultiFamily(String multiFamily)
    {
	this.multiFamily = multiFamily;
    }

    public String getSendRebateToCustomer()
    {
	return sendRebateToCustomer;
    }

    public void setSendRebateToCustomer(String sendRebateToCustomer)
    {
	this.sendRebateToCustomer = sendRebateToCustomer;
    }

    public CustomerScreenItem getPermitNumber()
    {
	return permitNumber;
    }

    public void setPermitNumber(CustomerScreenItem permitNumber)
    {
	this.permitNumber = permitNumber;
    }

    public String getNewConstruction()
    {
	return newConstruction;
    }

    public void setNewConstruction(String newConstruction)
    {
	this.newConstruction = newConstruction;
    }

    public CustomerScreenItem getBuildingType() {
        return buildingType;
    }

    public void setBuildingType(CustomerScreenItem buildingType) {
        this.buildingType = buildingType;
    }

    public CustomerScreenItem getWaterHeatingFuelType()
    {
	return waterHeatingFuelType;
    }

    public void setWaterHeatingFuelType(CustomerScreenItem waterHeatingFuelType)
    {
	this.waterHeatingFuelType = waterHeatingFuelType;
    }

    public CustomerScreenItem getCentralAC()
    {
	return centralAC;
    }

    public void setCentralAC(CustomerScreenItem centralAC)
    {
	this.centralAC = centralAC;
    }

    public CustomerScreenItem getAuditorCompanyNameText()
    {
        return auditorCompanyNameText;
    }

    public void setAuditorCompanyNameText(CustomerScreenItem auditorCompanyNameText)
    {
        this.auditorCompanyNameText = auditorCompanyNameText;
    }

    public CustomerScreenItem getHeatingCoolingFuelType()
    {
	return heatingCoolingFuelType;
    }

    public void setHeatingCoolingFuelType(CustomerScreenItem heatingCoolingFuelType)
    {
	this.heatingCoolingFuelType = heatingCoolingFuelType;
    }

    public CustomerScreenItem getCustomerType() {
        return customerType;
    }

    public void setCustomerType(CustomerScreenItem customerType) {
        this.customerType = customerType;
    }

    public CustomerScreenItem getProjectTypes() {
        return projectTypes;
    }

    public void setProjectTypes(CustomerScreenItem projectTypes) {
        this.projectTypes = projectTypes;
    }

    public CustomerScreenItem getTechnicianName() {
        return technicianName;
    }

    public void setTechnicianName(CustomerScreenItem technicianName) {
        this.technicianName = technicianName;
    }

    public CustomerScreenItem getTechnicianId() {
        return technicianId;
    }

    public void setTechnicianId(CustomerScreenItem technicianId) {
        this.technicianId = technicianId;
    }

    public CustomerScreenItem getTechnicianPhone() {
        return technicianPhone;
    }

    public void setTechnicianPhone(CustomerScreenItem technicianPhone) {
        this.technicianPhone = technicianPhone;
    }

    public CustomerScreenItem getProjectIncentive() {
        return projectIncentive;
    }

    public void setProjectIncentive(CustomerScreenItem projectIncentive) {
        this.projectIncentive = projectIncentive;
    }

    public CustomerScreenItem getDateInstalled() {
        return dateInstalled;
    }

    public void setDateInstalled(CustomerScreenItem dateInstalled) {
        this.dateInstalled = dateInstalled;
    }

    @Override
    protected CustomerScreenLayout clone() throws CloneNotSupportedException
    {
        // TODO Auto-generated method stub
        return (CustomerScreenLayout) super.clone();
    }
}
