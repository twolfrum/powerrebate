package com.icf.rebate.networklayer;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.util.Log;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.icf.rebate.app.model.RBResponseBean;
import com.icf.rebate.networklayer.model.ConfigurationDetail;
import com.icf.rebate.networklayer.model.FormResponseBean;
import com.icf.rebate.networklayer.model.LoginResponseBean;
import com.icf.rebate.networklayer.model.PendingJobItem;
import com.icf.rebate.networklayer.model.ResponseBean;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.DateFormatter;
import com.icf.rebate.ui.util.FileUtil;
import com.icf.rebate.ui.util.ICFLogger;
import com.icf.rebate.ui.util.UiUtil;

public class DataParserImpl implements DataParser
{

    private static final String TAG = DataParserImpl.class.getName();
    private static ObjectMapper objectMapper;
    private static DataParserImpl parser;

    private DataParserImpl()
    {
	objectMapper = new ObjectMapper();
	objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
    }

    public static DataParserImpl getInstance()
    {
	if (parser == null)
	{
	    parser = new DataParserImpl();
	}
	return parser;
    }

    @Override
	public ResponseBean parse(int requestId, byte[] data) {
		//$$$PROCESS_JSON
		ResponseBean bean = null;

		Class<?> className = null;

		{
			switch (requestId) {
				case ICFHttpManager.REQ_ID_LOGIN:
				case ICFHttpManager.REQ_ID_CHANGE_PASSWORD:
					className = LoginResponseBean.class;
					break;
				case ICFHttpManager.REQ_ID_CONFIG:
					className = ConfigurationDetail.class;
					break;
				case ICFHttpManager.REQ_ID_FORM:
					className = FormResponseBean.class;
					break;
				case ICFHttpManager.REQ_ID_RESOURCE_BUNDLE:
					ICFLogger.d(TAG, "Resource Bundle Response length : " + data.length);
					RBResponseBean obj = new RBResponseBean(data);
					if (data != null && data.length > 0) {
						obj.setStatusCode("S0000");
					}
					return obj;
				case ICFHttpManager.REQ_ID_LOGOUT:
					break;
				case ICFHttpManager.REQ_ID_SUBMIT:
					className = ResponseBean.class;
					break;
				case ICFHttpManager.REQ_ID_RESET_PASSWORD:
					className = ResponseBean.class;
					break;

				default:
					break;
			}
			ICFLogger.d(TAG, new String(data));
			if (className != null) {
				if (objectMapper != null) {
					try {
						bean = (ResponseBean) objectMapper.readValue(new ByteArrayInputStream(data), className);
					} catch (JsonParseException e) {
						e.printStackTrace();
					} catch (JsonMappingException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			}
		}
		return bean;
	}

    private boolean isRunning;

    private static int hitUpdateCount;

    private FormResponseBean queueFormBean;

    public void writeJson(final FormResponseBean bean, final Runnable runnable)
    {
	if (isRunning)
	{
	    hitUpdateCount++;
	    queueFormBean = bean;
	    return;
	}
	Thread t = new Thread(new Runnable()
	{
	    public void run()
	    {
		isRunning = true;
		writeToFile(bean);
		isRunning = false;
		if (hitUpdateCount > 0)
		{
		    writeToFile(queueFormBean);
		}
		if (runnable != null)
		{
		    runnable.run();
		}
		if (hitUpdateCount > 0)
		{
		    hitUpdateCount = 0;
		}
	    }
	});
	t.start();

    }

    public void writeFormDataFile(final FormResponseBean bean)
    {

	try
	{
	    bean.initSerializationMode();
	    String data = objectMapper.writeValueAsString(bean);
	    // Uncomment to get a copy of the json file for debug
	   /* Log.d(TAG, "current user directory= " + FileUtil.getCurrentUserDirectory());
	    try
	    {
		File tmp = new File(FileUtil.getCurrentUserDirectory()+"/jsontest.json");
		FileOutputStream fos = new FileOutputStream(tmp);
		fos.write(data.getBytes());
		fos.close();

	    }
	    catch (Exception e1)
	    {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	    }*/
	    bean.resetSerializationMode();
	    String jobId = bean.getCustomerInfo().getJobId();
	    String filePath = FileUtil.getJobDirectory(jobId) + "/" + AppConstants.JOB_DOCUMENT;
	    File file = new File(filePath);
	    FileOutputStream out = null;
	    try
	    {
		out = new FileOutputStream(file);
		out.write(data.getBytes());
		out.flush();
		// ICFLogger.d(TAG, "gopal:writeJson written");
		ICFLogger.d(TAG, "Data:" + data);
	    }
	    catch (Exception e)
	    {
		ICFLogger.d(TAG, e.toString());
	    }
	    finally
	    {
		if (out != null)
		{
		    try
		    {
			out.close();
		    }
		    catch (IOException e)
		    {
			e.printStackTrace();
		    }
		}
	    }
	}
	catch (JsonProcessingException e)
	{
	    e.printStackTrace();
	}
	finally
	{

	}
    }

    private void writeToFile(FormResponseBean bean)
    {
	try
	{
	    hitUpdateCount = 0;
	    // bean.initSerializationMode();
	    String data = objectMapper.writeValueAsString(bean);
	    // bean.resetSerializationMode();
	    String jobId = bean.getCustomerInfo().getJobId();
	    String filePath = FileUtil.getJobDirectory(jobId) + "/" + AppConstants.JOB_DOCUMENT_UI;
	    File file = new File(filePath);
	    FileOutputStream out = null;
	    try
	    {
		out = new FileOutputStream(file);
		out.write(data.getBytes());
		out.flush();
		// ICFLogger.d(TAG, "gopal:writeJson written");
		ICFLogger.d(TAG, "Data:" + data);
	    }
	    catch (Exception e)
	    {
		ICFLogger.d(TAG, e.toString());
	    }
	    finally
	    {
		if (out != null)
		{
		    try
		    {
			out.close();
		    }
		    catch (IOException e)
		    {
			e.printStackTrace();
		    }
		}
		if (UiUtil.rootActivity.get() != null)
		{
		    File tempFile = new File(filePath);
		    long fileTime = System.currentTimeMillis();
		    if (tempFile.exists())
		    {
			fileTime = tempFile.lastModified();
			String folderPath = tempFile.getParent();

			PendingJobItem item = new PendingJobItem();
			item.setId(bean.getCustomerInfo().getJobId());
			item.setBean(bean);
			item.setDate(DateFormatter.getFormattedDate(fileTime));
			item.setCustomerName(bean.getCustomerInfo().getFirstName() + " " + bean.getCustomerInfo().getLastName());
			item.setCustomerAddress(bean.getCustomerInfo().getCustomerAddress());
			item.setJobState(bean.getCustomerInfo().getState());
			item.setLastModified(fileTime);
			item.setFolderPath(folderPath);

			UiUtil.rootActivity.get().addOrUpdatePendingJob(item);
			RebateManager.getInstance().updateAdapter();
		    }

		}
	    }
	}
	catch (JsonProcessingException e)
	{
	    e.printStackTrace();
	}
	finally
	{

	}
    }
}
