package com.icf.rebate.networklayer;

/**
 * Request Object to keep track of the download packet.
 * 
 * @author varun.t
 * 
 */
public class RequestObject
{

    private int reqNumber;
    private int reqID;
    private String urlGetParams;
    // If needed private int retries;
    private TaskInterface task;
    private HttpManagerListener listener;

    public RequestObject(int reqNumber)
    {
	this.reqNumber = reqNumber;
    }

    public int getReqNumber()
    {
	return reqNumber;
    }

    public int getReqID()
    {
	return reqID;
    }

    public void setReqID(int reqID)
    {
	this.reqID = reqID;
    }

    public String getUrlGetParams()
    {
	return urlGetParams;
    }

    public void setUrlGetParams(String url)
    {
	this.urlGetParams = url;
    }

    public TaskInterface getTask()
    {
	return task;
    }

    public void setTask(TaskInterface task)
    {
	this.task = task;
    }

    @Override
    public boolean equals(Object another)
    {
	try
	{
	    RequestObject reqObj = (RequestObject) another;
	    return reqNumber == reqObj.getReqNumber();
	}
	catch (Exception e)
	{

	}
	return false;
    }

    public void setHttpManagerListener(HttpManagerListener listener)
    {
	this.listener = listener;
    }
}
