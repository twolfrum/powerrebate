package com.icf.rebate.networklayer.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import android.util.Log;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.icf.rebate.networklayer.model.FormResponseBean.FormObjectBuilder;
import com.icf.rebate.networklayer.model.FormResponseBean.Options;
import com.icf.rebate.networklayer.model.FormResponseBean.Part;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.UiUtil;

public class QIVItem implements FormObjectBuilder
{
    @JsonProperty("qivId")
    public long qivId = new Date().getTime();

    @JsonProperty("id")
    long id = -1;

    @JsonProperty("name")
    String name;

    @JsonIgnore
    String equipmentId;

    @JsonIgnore
    int pageIndex;

    public String getEquipmentId()
    {
	return equipmentId;
    }

    public void setEquipmentId(String equipmentId)
    {
	this.equipmentId = equipmentId;
    }

    public int getPageIndex()
    {
	return pageIndex;
    }

    public void setPageIndex(int pageIndex)
    {
	this.pageIndex = pageIndex;
    }

    @JsonProperty("parts")
    private List<Part> partList;

    @JsonIgnore
    int index = -1;

    @JsonProperty("itemSaved")
    boolean savedItem;

    @JsonProperty("staticRebateValue")
    private String staticRebateValue;

    @JsonProperty("qivRequired")
    boolean qivRequired;

    @JsonProperty("formFilledState")
    public int formFilledState = AppConstants.FORM_ITEM_FILLED_STATE.ITEM_UNEDITED.getState();

    float rebateValue;

    public int getFormFilledState()
    {
	return formFilledState;
    }

    public void setMandatoryFiledComplete(int state)
    {
	this.formFilledState = state;
    }

    public float getRebateValue()
    {
	return rebateValue;
    }

    public void setRebateValue(float rebateValue)
    {
	this.rebateValue = rebateValue;
    }

    public String getStaticRebateValue() {
        return staticRebateValue;
    }

    public void setStaticRebateValue(String staticRebateValue) {
        this.staticRebateValue = staticRebateValue;
    }

    public String getName()
    {
	return name;
    }

    public void setName(String name)
    {
	this.name = name;
    }

    public long getId()
    {
	return id;
    }

    public void setId(long id)
    {
	this.id = id;
    }

    public List<Part> getPartList()
    {
	return partList;
    }

    public void setPartList(List<Part> partList)
    {
	this.partList = partList;
    }

    @Override
    public Object createObject()
    {
	QIVItem item = new QIVItem();
	item.name = name;
	item.savedItem = true;
	if (partList != null)
	{
	    ListIterator<Part> iterator = partList.listIterator();
	    List<Part> partList = new ArrayList<Part>();

	    Part oldPart = null;
	    while (iterator.hasNext())
	    {
		oldPart = (Part) iterator.next();
		if (oldPart.getDuplicatePartId() == -1)
		{
		    Part part = (Part) oldPart.createObject();
		    partList.add(part);
		}
	    }

	    item.partList = partList;
	}
	return item;
    }

    public boolean isItemSaved()
    {
	return savedItem;
    }

    public void setSavedItem(boolean savedItem)
    {
	this.savedItem = savedItem;
    }

    public int getIndex()
    {
	return index;
    }

    public void setIndex(int index)
    {
	this.index = index;
    }

    public Boolean isQivRequired()
    {
	return qivRequired;
    }

    public void setQivRequired(boolean qivRequired)
    {
	this.qivRequired = qivRequired;
    }
}
