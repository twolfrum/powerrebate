package com.icf.rebate.networklayer;

public interface UploadProgressListener
{
    public void updateProgress(int progress, Object obj);

    public void onSuccess(Object obj);

    public void onFailure(Object obj);
}
