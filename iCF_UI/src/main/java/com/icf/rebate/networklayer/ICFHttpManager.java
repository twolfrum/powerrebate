package com.icf.rebate.networklayer;

import java.io.File;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.icf.rebate.networklayer.model.LoginResponseBean;
import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.rebate.ui.util.FileUtil;
import com.icf.rebate.ui.util.ICFLogger;

public class ICFHttpManager
{
    private static ICFHttpManager instance;

    private static DataParserImpl dataParser;

    private HttpQueueManager httpManager;

    private String baseURL;

    public static final int REQ_ID_LOGIN = 1;
    public static final int REQ_ID_CHANGE_PASSWORD = 2;
    public static final int REQ_ID_CONFIG = 3;
    public static final int REQ_ID_RESOURCE_BUNDLE = 4;
    public static final int REQ_ID_LOGOUT = 5;
    public static final int REQ_ID_RESET_LOGIN = 6;
    public static final int REQ_ID_FORM = 7;
    public static final int REQ_ID_SUBMIT = 8;
    public static final int REQ_ID_RESET_PASSWORD = 9;

    public static final String LOGIN_API = "/rebate/rest/user/login";
    public static final String LOGOUT_API = "/rebate/rest/user/logout";
    public static final String GET_CONFIGURATION_API = "/rebate/rest/app/config/";
    public static final String RESOURCE_BUNDLE_API = "/rebate/rest/app/rb/get";
    public static final String RESET_PASSWORD_API = "/rebate/rest/user/resetpassword";
    public static final String CHANGE_ACCOUNT_PASSWORD = "/rebate/rest/user/changepwd";
    public static final String RESET_LOGIN_API = "/rebate/rest/user/loginreset"; // testing purpose
    public static final String FORM_API = "/rebate/rest/module/fields/get?_companyname=default";
    // public static final String FORM_SUBMIT_API = "/rebate/rest/module/fields/save";
    public static final String FORM_SUBMIT_API = "/rebate/rest/module/fields/uploadZip";

    private static final String TAG = ICFHttpManager.class.getName();

    public static void init(Context context, String baseURL)
    {
	dataParser = DataParserImpl.getInstance();
	instance = new ICFHttpManager();
	instance.baseURL = baseURL;
    }

	public static void setServerUrl (String serverUrl) {
		instance.baseURL = serverUrl;
	}

	private ICFHttpManager()
    {
	httpManager = HttpQueueManager.getInstance();
    }

    public static ICFHttpManager getInstance()
    {
	return instance;
    }

    public boolean checkLoginConnectivity(String userName)
    {
	if (!ConnectionManager.isOnline())
	{
	    String path = FileUtil.getHttpResponseDirectory(userName, HttpRequest.getResponseCachedFileName(REQ_ID_LOGIN));
	    File file = new File(path);
	    if (file.exists())
	    {
		return false;
	    }
	}
	return true;
    }

    public HttpRequest resetPassword(HttpManagerListener listener, String username) {
	HttpRequest postRequest = null;
	try {
	    JSONObject postData = new JSONObject();
	    postData.put("userName", username);
	    postRequest = new HttpRequest(listener, dataParser);
	    postRequest.processPostRequest(REQ_ID_RESET_PASSWORD, baseURL + RESET_PASSWORD_API, postData.toString().getBytes(), listener);
	} catch (JSONException e) {
	    ICFLogger.e(TAG, e);
	}
	return postRequest;
    }
    
    public HttpRequest login(HttpManagerListener listener, String username, String password)
    {
	try
	{
	    JSONObject postObj = new JSONObject();
	    postObj.put("userName", username);
	    postObj.put("password", LibUtils.md5Hash(password));
	    HttpRequest httpRequest = new HttpRequest(listener, dataParser);

	    httpRequest.setSaveData(FileUtil.getHttpResponseDirectory(username, httpRequest.getResponseCachedFileName(REQ_ID_LOGIN)));
	    // setCommonHeaders(httpRequest);
	    httpRequest.processPostRequest(REQ_ID_LOGIN, baseURL + LOGIN_API, postObj.toString().getBytes(), listener);
	    return httpRequest;
	}
	catch (JSONException je)
	{
	    ICFLogger.e(TAG, je);
	}
	return null;
    }

    public void cancelAll()
    {
	if (httpManager != null)
	{
	    httpManager.cancelAll();
	}
    }

    public HttpRequest doUploadData(HttpManagerListener listener, Object obj, String path)
    {

	try
	{
	    final HttpRequest httpRequest = new HttpRequest(listener, dataParser);
	    httpRequest.setRequestObject(obj);
	    httpRequest.processPostRequest(REQ_ID_SUBMIT, baseURL + FORM_SUBMIT_API, path.getBytes(), listener);
	    return httpRequest;
	}
	catch (Exception je)
	{
	    ICFLogger.e(TAG, je);
	}
	return null;
    }

    public HttpRequest doResourceBundleRequest(HttpManagerListener listener)
    {
	try
	{
	    JSONObject postObj = new JSONObject();
	    LoginResponseBean bean = LibUtils.getLoggedInUserBean();
	    postObj.put("userName", bean.getUserName());
	    postObj.put("password", bean.getPassword());
	    postObj.put("type", LibUtils.getDeviceType());
	    postObj.put("density", LibUtils.getDeviceDensity());

	    // httPost.setEntity(new UrlEncodedFormEntity(nvps, Consts.UTF_8));
	    // postObj.put("userName", bean.getUserName());
	    // postObj.put("password", bean.getPassword());
	    // //TODO: need to remove this after discussion with US team.
	    // postObj.put("_companyname", "default");
	    // postObj.put("_type", LibUtils.getDeviceType());
	    // postObj.put("_density", LibUtils.getDeviceDensity());

	    HttpRequest httpRequest = new HttpRequest(listener, dataParser);
	    httpRequest.setSaveData(FileUtil.getHttpResponseDirectory(bean.getUserName(), httpRequest.getResponseCachedFileName(REQ_ID_RESOURCE_BUNDLE)));
	    // setCommonHeaders(httpRequest);
	    httpRequest.processPostRequest(REQ_ID_RESOURCE_BUNDLE, baseURL + RESOURCE_BUNDLE_API, postObj.toString().getBytes(), listener);
	    return httpRequest;
	}
	catch (Exception je)
	{
	    ICFLogger.e(TAG, je);
	}
	return null;
    }

    public HttpRequest doConfigRequest(HttpManagerListener listener)
    {
	try
	{
	    JSONObject postObj = new JSONObject();
	    LoginResponseBean bean = LibUtils.getLoggedInUserBean();
	    postObj.put("userName", bean.getUserName());
	    postObj.put("password", bean.getPassword());
	    HttpRequest httpRequest = new HttpRequest(listener, dataParser);
	    httpRequest.setSaveData(FileUtil.getHttpResponseDirectory(bean.getUserName(), httpRequest.getResponseCachedFileName(REQ_ID_CONFIG)));
	    // setCommonHeaders(httpRequest);
	    httpRequest.processPostRequest(REQ_ID_CONFIG, baseURL + GET_CONFIGURATION_API, postObj.toString().getBytes(), listener);
	    return httpRequest;
	}
	catch (JSONException je)
	{
	    ICFLogger.e(TAG, je);
	}
	return null;
    }

    public HttpRequest changePassword(HttpManagerListener listener, String userName, String oldPassword, String newPassword)
    {
	try
	{
	    JSONObject postObj = new JSONObject();
	    postObj.put("userName", userName);
	    postObj.put("oldPassword", LibUtils.md5Hash(oldPassword));
	    postObj.put("newPassword", newPassword);
	    postObj.put("verifyPassword", newPassword);
	    HttpRequest httpRequest = new HttpRequest(listener, dataParser);
	    httpRequest.setSaveData(FileUtil.getHttpResponseDirectory(userName, httpRequest.getResponseCachedFileName(REQ_ID_LOGIN)));
	    httpRequest.processPostRequest(REQ_ID_CHANGE_PASSWORD, baseURL + CHANGE_ACCOUNT_PASSWORD, postObj.toString().getBytes(), listener);
	    return httpRequest;
	}
	catch (JSONException je)
	{
	    ICFLogger.e(TAG, je);
	}
	return null;
    }

    public void doReset(String userName, String password)
    {
	try
	{
	    JSONObject postObj = new JSONObject();
	    postObj.put("userName", userName);
	    postObj.put("password", LibUtils.md5Hash(password));
	    HttpRequest httpRequest = new HttpRequest(null, dataParser);
	    httpRequest.processPostRequest(REQ_ID_RESET_LOGIN, baseURL + RESET_LOGIN_API, postObj.toString().getBytes(), null);
	}
	catch (JSONException je)
	{
	    ICFLogger.e(TAG, je);
	}
    }

    public void doLogout(HttpManagerListener listener, String userName, String password, String deviceId)
    {
	try
	{
	    JSONObject postObj = new JSONObject();
	    postObj.put("userName", userName);
	    postObj.put("password", password);
	    postObj.put("deviceId", deviceId);
	    HttpRequest httpRequest = new HttpRequest(listener, dataParser);
	    httpRequest.processPostRequest(REQ_ID_LOGOUT, baseURL + LOGOUT_API, postObj.toString().getBytes(), listener);
	}
	catch (JSONException je)
	{
	    ICFLogger.e(TAG, je);
	}
    }

}
