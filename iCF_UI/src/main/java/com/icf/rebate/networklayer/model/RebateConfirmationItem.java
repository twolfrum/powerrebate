package com.icf.rebate.networklayer.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RebateConfirmationItem implements Serializable
{
    public RebateConfirmationItem() {
	super();
    }
    
    // JSON Options
    public RebateConfirmationItem(String id, String label, String filename, String type) {
	super();
	this.id = id;
	this.label = label;
	this.filename = filename;
	this.type = type;
    }
    
    public static final long serialVersionUID = 1L;
    @JsonProperty("id")
    private String id;
    @JsonProperty("label")
    private String label;
    @JsonProperty("filename")
    private String filename;
    @JsonProperty("mandatory")
    private boolean mandatory;
    @JsonProperty("type")
    private String type;
    @JsonProperty("numberOfImages")
    private int numberOfImages;
    @JsonIgnore
    private int currentThumbPreviewPosition = 0;
    public String getId()
    {
        return id;
    }
    public void setId(String id)
    {
        this.id = id;
    }
    public String getLabel()
    {
        return label;
    }
    public void setLabel(String label)
    {
        this.label = label;
    }
    public String getFilename()
    {
        return filename;
    }
    public void setFilename(String filename)
    {
        this.filename = filename;
    }
    public String getType()
    {
        return type;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    public boolean isMandatory() {
        return mandatory;
    }

    public void setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
    }

    public int getNumberOfImages()
    {
        return numberOfImages;
    }
    public void setNumberOfImages(int numberOfImages)
    {
        this.numberOfImages = numberOfImages;
    }
    @JsonIgnore
    public int getCurrentThumbPreviewPosition()
    {
        return currentThumbPreviewPosition;
    }
    @JsonIgnore
    public void setCurrentThumbPreviewPosition(int currentThumbPreviewPosition)
    {
        this.currentThumbPreviewPosition = currentThumbPreviewPosition;
    }
    
}
