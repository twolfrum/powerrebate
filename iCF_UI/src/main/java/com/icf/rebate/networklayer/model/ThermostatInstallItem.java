package com.icf.rebate.networklayer.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.icf.rebate.networklayer.model.FormResponseBean.FormObjectBuilder;
import com.icf.rebate.networklayer.model.FormResponseBean.Part;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.UiUtil;

public class ThermostatInstallItem implements FormObjectBuilder
{
    @JsonProperty("thermostatId")
    public long id = new Date().getTime();

    @JsonProperty("name")
    private String name;

    @JsonProperty("manufacturer_top10")
    private List<String> manufacturer_top10;

    @JsonProperty("manufacturer_wifi")
    private List<String> manufacturer_wifi;

    @JsonProperty("parts")
    private ArrayList<Part> partList;

    @JsonProperty("itemSaved")
    private boolean itemSaved;

    @JsonProperty("pageNum")
    private int pageNum;

    @JsonProperty("formFilledState")
    public int formFilledState = AppConstants.FORM_ITEM_FILLED_STATE.ITEM_UNEDITED.getState();

    @JsonProperty("staticRebateValue")
    private String staticRebateValue;

    @JsonProperty("maxMeasures")
    private int maxMeasures;

    private float rebateValue;

    public int getFormFilledState()
    {
	return formFilledState;
    }

    public void setMandatoryFiledComplete(int state)
    {
	this.formFilledState = state;
    }

    public String getName()
    {
	return name;
    }

    public void setName(String name)
    {
	this.name = name;
    }

    public List<String> getManufacturer_top10()
    {
	return manufacturer_top10;
    }

    public void setManufacturer_top10(List<String> manufacturer_top10)
    {
	this.manufacturer_top10 = manufacturer_top10;
    }

    public List<String> getManufacturer_wifi()
    {
	return manufacturer_wifi;
    }

    public void setManufacturer_wifi(List<String> manufacturer_wifi)
    {
	this.manufacturer_wifi = manufacturer_wifi;
    }

    public int getMaxMeasures() {
        return maxMeasures == 0 ? 999 : maxMeasures;
    }

    public void setMaxMeasures(int maxMeasures) {
        this.maxMeasures = maxMeasures;
    }

    public ArrayList<Part> getPartList()
    {
	return partList;
    }

    public void setPartList(ArrayList<Part> partList)
    {
	this.partList = partList;
    }

    public boolean isItemSaved()
    {
	return itemSaved;
    }

    public void setItemSaved(boolean itemSaved)
    {
	this.itemSaved = itemSaved;
    }

    public float getRebateValue()
    {
	return rebateValue;
    }

    public void setRebateValue(float rebateValue)
    {
	this.rebateValue = rebateValue;
    }

    public String getStaticRebateValue() {
        return staticRebateValue;
    }

    public void setStaticRebateValue(String staticRebateValue) {
        this.staticRebateValue = staticRebateValue;
    }

    @Override
    public Object createObject()
    {
	ThermostatInstallItem item = new ThermostatInstallItem();
	item.itemSaved = false;
	if (partList != null)
	{
	    ListIterator<Part> iterator = partList.listIterator();
	    List<Part> partList = new ArrayList<Part>();

	    Part oldPart = null;
	    while (iterator.hasNext())
	    {
		oldPart = (Part) iterator.next();
		if (oldPart.getDuplicatePartId() == -1)
		{
		    Part part = (Part) oldPart.createObject();
		    partList.add(part);
		}
	    }

	    item.partList = (ArrayList<Part>) partList;
	    if (manufacturer_top10 != null)
	    {
		item.manufacturer_top10 = new ArrayList<String>(manufacturer_top10);
	    }
	    if (manufacturer_wifi != null)
	    {
		item.manufacturer_wifi = new ArrayList<String>(manufacturer_wifi);
	    }
	    item.setMaxMeasures(maxMeasures);
	}
	return item;
    }

    public void setPageNum(int pageNum)
    {
	this.pageNum = pageNum;
    }

    public int getPageNum()
    {
	return this.pageNum;
    }

    @JsonIgnore
    public String getOldTStatValue()
    {
        Part part = getPartById(AppConstants.THERMOSTAT_INSTALL_OLD_T_STAT_TYPE_ID);
        if (part != null)
        {
            return part.getSavedValue();
        }
        return null;
    }

    @JsonIgnore
    private Part getPartById(String id)
    {
        if (partList != null)
        {
            int index = partList.indexOf(new Part(id));
            if (index != -1)
            {
                return partList.get(index);
            }
        }
        return null;
    }
}
