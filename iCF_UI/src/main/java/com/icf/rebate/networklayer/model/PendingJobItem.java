package com.icf.rebate.networklayer.model;

public class PendingJobItem
{

    private String id;

    private String customerName;

    private String customerAddress;

    private String date;

    private long lastModified;

    private FormResponseBean bean;

    private int jobState;

    private String folderPath;

    public String getCustomerName()
    {
	return customerName;
    }

    public void setCustomerName(String customerName)
    {
	this.customerName = customerName;
    }

    public String getDate()
    {
	return date;
    }

    public void setDate(String date)
    {
	this.date = date;
    }

    public void setJobState(int state)
    {
	this.jobState = state;
    }

    public FormResponseBean getBean()
    {
	return bean;
    }

    public void setBean(FormResponseBean bean)
    {
	this.bean = bean;
    }

    public void setLastModified(long lastModified)
    {
	this.lastModified = lastModified;
    }

    public long getLastModified()
    {
	return this.lastModified;
    }

    public boolean isJobCompleted()
    {
	return (jobState >= CustomerInfo.COMPLETED_JOB && jobState <= CustomerInfo.SUBMISSION_ERROR);
    }

    public boolean isSubmissionError()
    {
	return (jobState == CustomerInfo.SUBMISSION_ERROR);
    }

    public boolean isSubmitted()
    {
	return (jobState == CustomerInfo.SUBMITTED);
    }

    public boolean isInComplete()
    {
	return (jobState == CustomerInfo.INCOMPLETED_JOB || jobState == CustomerInfo.NEW_JOB);
    }

    public boolean isSubmissionInProgress()
    {
	return (jobState == CustomerInfo.SUBMISSION_IN_PROGRESS);
    }

    public void setFolderPath(String absolutePath)
    {
	this.folderPath = absolutePath;
    }

    public String getFolderPath()
    {
	return this.folderPath;
    }

    public String getId()
    {
	return id;
    }

    public void setId(String id)
    {
	this.id = id;
    }

    public void setCustomerAddress(String customerAddress)
    {
	this.customerAddress = customerAddress;
    }

    public String getCustomerAddress()
    {
	return this.customerAddress;
    }

    @Override
    public boolean equals(Object o)
    {
	PendingJobItem pji = (PendingJobItem) o;
	if (pji != null && pji.getId() != null && id != null)
	{
	    if (pji.getId().equalsIgnoreCase(id))
	    {
		return true;
	    }
	}
	return false;
    }
}
