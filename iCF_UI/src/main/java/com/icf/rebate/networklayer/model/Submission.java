package com.icf.rebate.networklayer.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Submission {

    @JsonProperty("name")
    private String name;
    
    @JsonProperty("id")
    private String id;
    
    @JsonProperty("items")
    List<SubmissionItem> items;

    
    public Submission(String id) {
	this.id = id;
    }
    
    public Submission() {
	super();
    }
    
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public List<SubmissionItem> getItems()
    {
        return items;
    }

    public void setItems(List<SubmissionItem> items)
    {
        this.items = items;
    }
    
    @JsonIgnore
    public SubmissionItem getItemById(String id) {
	SubmissionItem item = null;
	if (items != null && items.size() > 0) {
	    int index = items.indexOf(new SubmissionItem(id));
	    if (index > -1) {
		item = items.get(index);
	    }
	}
	return item;
    }

    @Override
    public boolean equals(Object o)
    {
	boolean equal = false;
	if (o instanceof Submission) {
	    Submission objToCompare = ((Submission)o);
	    if (this.getId().equals(objToCompare.getId())) {
		equal = true;
	    }
	}
	return equal;
    }
    
    
}
