package com.icf.rebate.networklayer.model;

public class CustomerScreenItem
{
    private String title;
    private boolean mandatory;
    private String minValue;
    private String maxValue;
    private String minLength;
    private String maxLength;


    public CustomerScreenItem() {}

    public CustomerScreenItem(String title) {
        this.title = title;
    }
    
    public CustomerScreenItem(String title, boolean mandatory)
    {
	    this.title = title;
	    this.mandatory = mandatory;
    }

    public CustomerScreenItem(String title, boolean mandatory, String minValue, String maxValue)
    {
        this.title = title;
        this.mandatory = mandatory;
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public boolean isMandatory()
    {
        return mandatory;
    }

    public void setMandatory(boolean mandatory)
    {
        this.mandatory = mandatory;
    }

    public String getMinValue() {
        return minValue;
    }

    public void setMinValue(String minValue) {
        this.minValue = minValue;
    }

    public String getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(String maxValue) {
        this.maxValue = maxValue;
    }

    public String getMinLength() {
        return minLength;
    }

    public void setMinLength(String minLength) {
        this.minLength = minLength;
    }

    public String getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(String maxLength) {
        this.maxLength = maxLength;
    }
}
