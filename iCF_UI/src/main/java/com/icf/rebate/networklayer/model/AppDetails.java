package com.icf.rebate.networklayer.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AppDetails implements Cloneable
{
    @JsonProperty("deviceType")
    private String deviceType;

    @JsonProperty("mobileAppVersion")
    private String mobileAppVersion;

    @JsonProperty("androidBuildVersion")
    private String androidBuildVersion;

    @JsonProperty("androidBuildUpgradeMessage")
    private String androidBuildUpgradeMessage;

    @JsonProperty("startDate")
    private String applicationCreateDate;

    @JsonProperty("submissionDate")
    private String applicationSubmitDate;

    @JsonProperty("customerType")
    private String customerType;

    @JsonProperty("houseType")
    private String houseType;

    @JsonProperty("rebateToCurrentBill")
    private boolean rebateToCurrentBill;

    @JsonProperty("rebateToUtility")
    private boolean rebateToUtility;

    @JsonProperty("rebateToDifferentPerson")
    private boolean rebateToDifferentPerson;

    @JsonProperty("rebateAmountValue")
    private String rebateAmountValue;

    @JsonProperty("rebateIncentiveDecision")
    private boolean rebateIncentiveDecision;

    @JsonProperty("approximateSizeofHouse")
    private String approximateSizeofHouse;

    @JsonProperty("approximateYearHouseBuilt") //free-form text entry
    private String approximateYearHouseBuilt;

    @JsonProperty("basement")
    private boolean basementFoundation;

    @JsonProperty("tuneUpRebate")
    private float tuneUpRebate;

    @JsonProperty("ductImprovementRebate")
    private float ductImprovementRebate;

    @JsonProperty("equipmentListRebate")
    private float equipmentListRebate;

    @JsonProperty("insulationListRebate")
    private float insulationListRebate;

    @JsonProperty("windowDoorListRebate")
    private float windowDoorListRebate;

    @JsonProperty("qivrebate")
    private float qivrebate;

    @JsonProperty("thermostatInstallRebate")
    private float thermostatInstallRebate;

    @JsonProperty("airSealingRebate")
    private float airSealingRebate;

    @JsonProperty("equipmentListFilter")
    private Map<String, List<String>> equipmentListFilter;

    @JsonProperty("airDuctSealingHouseTypeFilter")
    private Map<String, List<String>> airDuctSealingHouseTypeFilter;

    @JsonProperty("comments")
    private String comments;

    @JsonProperty("documentLater")
    private boolean documentLater;

    @JsonProperty("orderNumber")
    private String orderNumber;

    // KVB - (12/15/15) Adding primary heating/cooling system support
    @JsonProperty("heatingCoolingSystem")
    private List<String> heatingCoolingSystem;

    @JsonProperty("heatingCoolingSelection")
    private String heatingCoolingSelection;

    @JsonProperty("buildingType")
    private List<String> buildingTypes;

    @JsonProperty("buildingTypeSelection")
    private String buildingTypeSelection;

    @JsonProperty("waterHeatingFuelTypeSelection")
    private String waterHeatingFuelTypeSelection;

    @JsonProperty("permitNumber")
    private String permitNumber;

    @JsonProperty("newConstruction")
    private boolean newConstruction;

    @JsonProperty("waterHeatingFuelType")
    private List<String> waterHeatingFuelType;
    
    @JsonProperty("centralAC")
    private List<String> centralAC;
    
    @JsonProperty("centralACSelection")
    private String centralACSelection;

    @JsonProperty("auditorCompanyNameSelection")
    private String auditorCompanyNameSelection;

    @JsonProperty("heatingCoolingFuelType")
    private List<String> heatingCoolingFuelType;
    
    @JsonProperty("heatingCoolingFuelTypeSelection")
    private String heatingCoolingFuelTypeSelection;

    @JsonProperty("yearBuilt") //drop down list of year ranges
    private List<String> yearBuilt;

    @JsonProperty("yearBuiltSelection")
    private String yearBuiltSelection;

    @JsonProperty("distributor")
    private List<String> distributor;

    @JsonProperty("distributorSelection")
    private String distributorSelection;

    @JsonProperty("projectTypes")
    private List<String> projectTypes;

    @JsonProperty("projectTypeSelection")
    private String projectTypeSelection;

    @JsonProperty("dateInstalled")
    private String dateInstalled;

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getMobileAppVersion() {
        return mobileAppVersion;
    }

    public void setMobileAppVersion(String mobileAppVersion) {
        this.mobileAppVersion = mobileAppVersion;
    }

    public float getInsulationListRebate()
    {
	return insulationListRebate;
    }

    public void setInsulationListRebate(float insulationListRebate)
    {
	this.insulationListRebate = insulationListRebate;
    }

    public float getWindowDoorListRebate()
    {
	return windowDoorListRebate;
    }

    public void setWindowDoorListRebate(float windowDoorListRebate)
    {
	this.windowDoorListRebate = windowDoorListRebate;
    }

    public Map<String, List<String>> getEquipmentListFilter()
    {
	return equipmentListFilter;
    }

    public void setEquipmentListFilter(Map<String, List<String>> equipmentListFilter)
    {
	this.equipmentListFilter = equipmentListFilter;
    }

    public Map<String, List<String>> getAirDuctSealingHouseTypeFilter()
    {
        return airDuctSealingHouseTypeFilter;
    }

    public void setAirDuctSealingHouseTypeFilter(Map<String, List<String>> airDuctSealingHouseTypeFilter)
    {
        this.airDuctSealingHouseTypeFilter = airDuctSealingHouseTypeFilter;
    }

    public String getCustomerType()
    {
	return customerType;
    }

    public void setCustomerType(String customerType)
    {
	this.customerType = customerType;
    }

    public String getHouseType()
    {
	return houseType;
    }

    public void setHouseType(String houseType)
    {
	this.houseType = houseType;
    }

    public boolean isRebateToCurrentBill()
    {
	return rebateToCurrentBill;
    }

    public void setRebateToCurrentBill(boolean rebateToCurrentBill)
    {
	this.rebateToCurrentBill = rebateToCurrentBill;
    }

    public boolean isRebateToUtility()
    {
	return rebateToUtility;
    }

    public void setRebateToUtility(boolean rebateToUtility)
    {
	this.rebateToUtility = rebateToUtility;
    }

    public boolean isRebateToDifferentPerson()
    {
	return rebateToDifferentPerson;
    }

    public void setRebateToDifferentPerson(boolean rebateToDifferentPerson)
    {
	this.rebateToDifferentPerson = rebateToDifferentPerson;
    }

    public String getRebateAmountValue() {

        rebateAmountValue = String.valueOf(getEquipmentListRebate() +
                getQivrebate() + getTuneUpRebate() + getDuctImprovementRebate() +
                getThermostatInstallRebate() + getInsulationListRebate() +
                getWindowDoorListRebate() + getAirSealingRebate());

        return rebateAmountValue;
    }

    public void setRebateAmountValue(String rebateAmountValue) {
        this.rebateAmountValue = rebateAmountValue;
    }

    public boolean isRebateIncentiveDecision()
    {
	return rebateIncentiveDecision;
    }

    public void setRebateIncentiveDecision(boolean rebateIncentiveDecision)
    {
	this.rebateIncentiveDecision = rebateIncentiveDecision;
    }

    public String getApproximateSizeofHouse()
    {
	return approximateSizeofHouse;
    }

    public void setApproximateSizeofHouse(String approximateSizeofHouse)
    {
	this.approximateSizeofHouse = approximateSizeofHouse;
    }

    public String getApproximateYearHouseBuilt()
    {
	return approximateYearHouseBuilt;
    }

    public void setApproximateYearHouseBuilt(String approximateYearHouseBuilt)
    {
	this.approximateYearHouseBuilt = approximateYearHouseBuilt;
    }

    public boolean isBasementFoundation()
    {
	return basementFoundation;
    }

    public void setBasementFoundation(boolean basementFoundation)
    {
	this.basementFoundation = basementFoundation;
    }

    public float getTuneUpRebate()
    {
	return tuneUpRebate;
    }

    public void setTuneUpRebate(float tuneUpRebate)
    {
	this.tuneUpRebate = tuneUpRebate;
    }

    public float getDuctImprovementRebate()
    {
	return ductImprovementRebate;
    }

    public void setDuctImprovementRebate(float ductImprovementRebate)
    {
	this.ductImprovementRebate = ductImprovementRebate;
    }

    public float getAirSealingRebate()
    {
        return airSealingRebate;
    }

    public void setAirSealingRebate(float airSealingRebate)
    {
        this.airSealingRebate = airSealingRebate;
    }

    public float getEquipmentListRebate()
    {
	return equipmentListRebate;
    }

    public void setEquipmentListRebate(float equipmentListRebate)
    {
	this.equipmentListRebate = equipmentListRebate;
    }

    public float getQivrebate()
    {
	return qivrebate;
    }

    public void setQivrebate(float qivrebate)
    {
	this.qivrebate = qivrebate;
    }

    public float getThermostatInstallRebate()
    {
	return thermostatInstallRebate;
    }

    public void setThermostatInstallRebate(float thermostatInstallRebate)
    {
	this.thermostatInstallRebate = thermostatInstallRebate;
    }

    @Override
    protected AppDetails clone() throws CloneNotSupportedException
    {
	return (AppDetails) super.clone();
    }

    public String getComments()
    {
	return comments;
    }

    public void setComments(String comments)
    {
	this.comments = comments;
    }
    
    public boolean isDocumentLater()
    {
	return documentLater;
    }

    public void setDocumentLater(boolean documentLater)
    {
	this.documentLater = documentLater;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getApplicationCreateDate()
    {
	return applicationCreateDate;
    }

    public void setApplicationCreateDate(String applicationCreateDate)
    {
	this.applicationCreateDate = applicationCreateDate;
    }

    public String getApplicationSubmitDate()
    {
	return applicationSubmitDate;
    }

    public void setApplicationSubmitDate(String applicationSubmitDate)
    {
	this.applicationSubmitDate = applicationSubmitDate;
    }

    public List<String> getHeatingCoolingSystem()
    {
	return heatingCoolingSystem;
    }

    public void setHeatingCoolingSystem(List<String> heatingCoolingSystem)
    {
	this.heatingCoolingSystem = heatingCoolingSystem;
    }

    public String getHeatingCoolingSelection()
    {
	return heatingCoolingSelection;
    }

    public void setHeatingCoolingSelection(String heatingCoolingSelection)
    {
	this.heatingCoolingSelection = heatingCoolingSelection;
    }

    public String getAndroidBuildVersion()
    {
	return androidBuildVersion;
    }

    public void setAndroidBuildVersion(String androidBuildVersion)
    {
	this.androidBuildVersion = androidBuildVersion;
    }

    public String getAndroidBuildUpgradeMessage()
    {
	return androidBuildUpgradeMessage;
    }

    public void setAndroidBuildUpgradeMessage(String androidBuildUpgradeMessage)
    {
	this.androidBuildUpgradeMessage = androidBuildUpgradeMessage;
    }

    public List<String> getBuildingTypes()
    {
	return buildingTypes;
    }

    public void setBuildingTypes(List<String> buildingType)
    {
	this.buildingTypes = buildingType;
    }

    public String getBuildingTypeSelection()
    {
	return buildingTypeSelection;
    }

    public void setBuildingTypeSelection(String buildingTypeSelection)
    {
	this.buildingTypeSelection = buildingTypeSelection;
    }

    public String getPermitNumber()
    {
	return permitNumber;
    }

    public void setPermitNumber(String permitNumber)
    {
	this.permitNumber = permitNumber;
    }

    public boolean isNewConstruction()
    {
	return newConstruction;
    }

    public void setNewConstruction(boolean newConstruction)
    {
	this.newConstruction = newConstruction;
    }

    public List<String> getWaterHeatingFuelType()
    {
	return waterHeatingFuelType;
    }

    public void setWaterHeatingFuelType(List<String> waterHeatingFuelType)
    {
	this.waterHeatingFuelType = waterHeatingFuelType;
    }

    public String getWaterHeatingFuelTypeSelection()
    {
	return waterHeatingFuelTypeSelection;
    }

    public void setWaterHeatingFuelTypeSelection(String waterHeatingFuelTypeSelection)
    {
	this.waterHeatingFuelTypeSelection = waterHeatingFuelTypeSelection;
    }

    public List<String> getCentralAC()
    {
        return centralAC;
    }

    public void setCentralAC(List<String> centralAC)
    {
        this.centralAC = centralAC;
    }

    public String getCentralACSelection()
    {
        return centralACSelection;
    }

    public void setCentralACSelection(String centralACSelection)
    {
        this.centralACSelection = centralACSelection;
    }

    public List<String> getHeatingCoolingFuelType()
    {
        return heatingCoolingFuelType;
    }

    public void setHeatingCoolingFuelType(List<String> heatingCoolingFuelType)
    {
        this.heatingCoolingFuelType = heatingCoolingFuelType;
    }

    public String getHeatingCoolingFuelTypeSelection()
    {
        return heatingCoolingFuelTypeSelection;
    }

    public void setHeatingCoolingFuelTypeSelection(String heatingCoolingFuelTypeSelection)
    {
        this.heatingCoolingFuelTypeSelection = heatingCoolingFuelTypeSelection;
    }

    public String getAuditorCompanyNameSelection()
    {
        return auditorCompanyNameSelection;
    }

    public void setAuditorCompanyNameSelection(String auditorCompanyNameSelection)
    {
        this.auditorCompanyNameSelection = auditorCompanyNameSelection;
    }

    public List<String> getYearBuilt()
    {
        return yearBuilt;
    }

    public void setYearBuilt(List<String> yearBuilt)
    {
        this.yearBuilt = yearBuilt;
    }

    public String getYearBuiltSelection()
    {
        return yearBuiltSelection;
    }

    public void setYearBuiltSelection(String yearBuiltSelection)
    {
        this.yearBuiltSelection = yearBuiltSelection;
    }

    public List<String> getDistributor()
    {
        return distributor;
    }

    public void setDistributor(List<String> distributor)
    {
        this.distributor = distributor;
    }

    public String getDistributorSelection()
    {
        return distributorSelection;
    }

    public void setDistributorSelection(String distributorSelection)
    {
        this.distributorSelection = distributorSelection;
    }

    public List<String> getProjectTypes() {
        return projectTypes;
    }

    public void setProjectTypes(List<String> projectTypes) {
        this.projectTypes = projectTypes;
    }

    public String getProjectTypeSelection() {
        return projectTypeSelection;
    }

    public void setProjectTypeSelection(String projectTypeSelection) {
        this.projectTypeSelection = projectTypeSelection;
    }

    public List<String> getSelectableCustomerTypes() {
        if (equipmentListFilter == null) return new ArrayList<String>();
        return new ArrayList<String>(equipmentListFilter.keySet());
    }

    public String getDateInstalled() {
        return dateInstalled;
    }

    public void setDateInstalled(String dateInstalled) {
        this.dateInstalled = dateInstalled;
    }
}
