package com.icf.rebate.networklayer.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseBean
{

    @JsonIgnore
    protected String statusCode;

    @JsonIgnore
    private String statusMessage;

    @JsonIgnore
    public String getStatusCode()
    {
	return statusCode;
    }

    @JsonProperty("statusCode")
    public void setStatusCode(String statusCode)
    {
	this.statusCode = statusCode;
    }

    @JsonIgnore
    public boolean isSuccessResponse()
    {
	return statusCode.charAt(0) == 'S';
    }

    @JsonIgnore
    public String getStatusMessage()
    {
	return statusMessage;
    }

    @JsonProperty("statusMsg")
    public void setStatusMessage(String statusMessage)
    {
	this.statusMessage = statusMessage;
    }
}
