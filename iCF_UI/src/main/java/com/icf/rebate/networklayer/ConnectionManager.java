package com.icf.rebate.networklayer;

import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.FileEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicStatusLine;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.rebate.ui.util.FileUtil;
import com.icf.rebate.ui.util.ICFLogger;

/**
 * 
 * @author varun.t
 * 
 */
public class ConnectionManager
{

    private static final String TAG = ConnectionManager.class.getSimpleName();

    private String uri;
    private static HttpClient httpClient;
    private HttpGet httpGet;
    private HttpPost httpPost;
    private static final int READ_TIMEOUT_MILLLIS = 60000;
    private static final int HTTP_PORT = 80;
    private static final int HTTPS_PORT = 443;

    public ConnectionManager(String uri)
    {
	this(uri, null, null);
    }

    public ConnectionManager(String uri, String username, String password)
    {

	this.uri = uri;
	if (httpClient == null)
	{
	    SchemeRegistry schemeRegistry = new SchemeRegistry();
	    schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), HTTP_PORT));
	    try
	    {
		schemeRegistry.register(new Scheme("https", new CustomSSLSocketFactory(), HTTPS_PORT));
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }

	    HttpParams params = new BasicHttpParams();
	    HttpConnectionParams.setConnectionTimeout(params, READ_TIMEOUT_MILLLIS / 2);
	    HttpConnectionParams.setSoTimeout(params, READ_TIMEOUT_MILLLIS);
	    ThreadSafeClientConnManager connManager = new ThreadSafeClientConnManager(params, schemeRegistry);
	    httpClient = new DefaultHttpClient(connManager, params);
	    if (username != null && password != null)
	    {
		setHttpClientCredentials(username, password);
	    }
	}
    }

    public class CustomSSLSocketFactory extends org.apache.http.conn.ssl.SSLSocketFactory
    {
	private SSLSocketFactory FACTORY = HttpsURLConnection.getDefaultSSLSocketFactory();

	public CustomSSLSocketFactory() throws KeyManagementException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException
	{
	    super(null);
	    try
	    {
		SSLContext context = SSLContext.getInstance("TLS");
		TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		tmf.init((KeyStore) null); // Using null here initializes the TMF with the default trust store
		
		// Get the default trust manager
		X509TrustManager x509Tm = null;
		for (TrustManager tm : tmf.getTrustManagers()) {
		    if (tm instanceof X509TrustManager) {
			x509Tm = (X509TrustManager) tm;
			break;
		    }
		}
		
		TrustManager[] tm = new TrustManager[] { x509Tm };
		context.init(null, tm, null);
		FACTORY = context.getSocketFactory();
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}

	public Socket createSocket() throws IOException
	{
	    return FACTORY.createSocket();
	}
    }

    // This was creating a vulnerability it was accepting any cert
    /*public class FullX509TrustManager implements X509TrustManager
    {
	@Override
	public void checkClientTrusted(X509Certificate[] argChain, String argAuthType) throws CertificateException
	{
	}

	@Override
	public void checkServerTrusted(X509Certificate[] argChain, String argAuthType) throws CertificateException
	{
	}

	@Override
	public X509Certificate[] getAcceptedIssuers()
	{
	    return null;
	}
    }*/

    public void setHttpClientCredentials(String username, String password)
    {

	if (username != null && password != null)
	{
	    ((DefaultHttpClient) httpClient).getCredentialsProvider().setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(username, password));
	}

    }

    public void abortReq()
    {

	if (httpGet != null)
	{
	    httpGet.abort();
	}

	if (httpPost != null)
	{
	    httpPost.abort();
	}
    }

    private HttpData createHttpData(StatusLine status, byte[] data)
    {
	HttpData httpData = new HttpData(status, data);
	return httpData;
    }

    public HttpData getBytesDataByGet() throws ClientProtocolException, IOException
    {
	byte[] retVal = null;
	StatusLine status = null;
	if (httpClient != null)
	{
	    httpGet = new HttpGet(uri);
	    ICFLogger.d(TAG, "API URI  : " + uri);
	    final HttpResponse response = httpClient.execute(httpGet);
	    status = response.getStatusLine();
	    if (status.getStatusCode() == HttpStatus.SC_OK)
	    {
		ICFLogger.d(TAG, "API Response Code for Request  : " + uri + " " + response.getStatusLine().getStatusCode());
		HttpEntity entity = response.getEntity();
		retVal = EntityUtils.toByteArray(entity);
		ICFLogger.d(TAG, "API Status Msg " + response.getStatusLine().getReasonPhrase());
	    }

	}

	return createHttpData(status, retVal);
    }

    public HttpData getBytesDataByPost(List<NameValuePair> nameValuePairs) throws ClientProtocolException, IOException
    {
	byte[] retVal = null;
	StatusLine status = null;
	if (httpClient != null)
	{
	    httpPost = new HttpPost(uri);
	    setCommonHeaders(httpPost);
	    if (nameValuePairs != null)
	    {
		httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	    }
	    ICFLogger.d(TAG, "Media API  : " + uri);
	    final HttpResponse response = httpClient.execute(httpPost);
	    ICFLogger.d(TAG, "Media API Response Code for Request  : " + uri + " " + response.getStatusLine().getStatusCode());
	    long time = System.currentTimeMillis();
	    status = response.getStatusLine();
	    if (status.getStatusCode() == HttpStatus.SC_OK)
	    {
		HttpEntity entity = response.getEntity();
		retVal = EntityUtils.toByteArray(entity);
		ICFLogger.d(TAG, "Reading Response content took " + (System.currentTimeMillis() - time));
	    }
	}
	return createHttpData(status, retVal);

    }

    private void setCommonHeaders(HttpPost httpPost)
    {
	httpPost.setHeader("Content-Type", "application/json");
    }

    public HttpData getBytesDataByPost(Header[] headers, File file) throws ClientProtocolException, IOException
    {
	byte[] retVal = null;
	StatusLine status = null;
	try
	{
	    if (httpClient != null)
	    {
		httpPost = new HttpPost(uri);
		if(headers != null) {
		    httpPost.setHeaders(headers);
		} else {
		    setCommonHeaders(httpPost);
		}
		FileEntity fileEntity = new FileEntity(file, "application/octet-stream");
		httpPost.setEntity(fileEntity);
		ICFLogger.d(TAG, "API  : " + uri);
//		ICFLogger.d(TAG, "API Post Data : " + new String(postData));

		for (Header header : httpPost.getAllHeaders())
		{
		    ICFLogger.d(TAG, "Header name : " + header.getName() + " value : " + header.getValue());
		}

		final HttpResponse response = httpClient.execute(httpPost);
		ICFLogger.d(TAG, "API Response Code for Request  : " + uri + " " + response.getStatusLine().getStatusCode());
		status = response.getStatusLine();
		if (status.getStatusCode() == HttpStatus.SC_OK)
		{
		    HttpEntity entity = response.getEntity();
		    // retVal = EntityUtils.toByteArray(entity);
		    long time = System.currentTimeMillis();
		    retVal = FileUtil.readInputStream(entity.getContent());
		    ICFLogger.d(TAG, "Time taken to write the content to server : " + (System.currentTimeMillis() - time));
		}
	    }
	}
	catch (Exception e)
	{
	    ICFLogger.e(TAG, e);
	    status = new BasicStatusLine(HttpVersion.HTTP_1_0, 9000, "client error");
	}
	return createHttpData(status, retVal);

    }
    
    public HttpData getBytesDataByPost(Header[] headers, byte[] postData) throws ClientProtocolException, IOException
    {
	byte[] retVal = null;
	StatusLine status = null;
	try
	{
	    if (httpClient != null)
	    {
		httpPost = new HttpPost(uri);
		if(headers != null) {
		    httpPost.setHeaders(headers);
		} else {
		    setCommonHeaders(httpPost);
		}
		if (postData != null)
		{
		    HttpEntity entity = new ByteArrayEntity(postData);
		    httpPost.setEntity(entity);
		}
		ICFLogger.d(TAG, "API  : " + uri);
		ICFLogger.d(TAG, "API Post Data : " + new String(postData));

		for (Header header : httpPost.getAllHeaders())
		{
		    ICFLogger.d(TAG, "Header name : " + header.getName() + " value : " + header.getValue());
		}
                ICFLogger.d(TAG, "httpPost params= " + httpPost.getParams().toString());
		final HttpResponse response = httpClient.execute(httpPost);
		ICFLogger.d(TAG, "API Response Code for Request  : " + uri + " " + response.getStatusLine().getStatusCode());
		status = response.getStatusLine();
		if (status.getStatusCode() == HttpStatus.SC_OK)
		{
		    HttpEntity entity = response.getEntity();
		    // retVal = EntityUtils.toByteArray(entity);
		    long time = System.currentTimeMillis();
		    retVal = FileUtil.readInputStream(entity.getContent());
		    ICFLogger.d(TAG, "Time taken to write the content to server : " + (System.currentTimeMillis() - time));		    
		}
	    }
	}
	catch (Exception e)
	{
	    ICFLogger.e(TAG, e);
	    status = new BasicStatusLine(HttpVersion.HTTP_1_0, 9000, "client error");
	}
	return createHttpData(status, retVal);

    }
    
    public HttpData getBytesDataByPostParseHeadersToJson(Header[] headers, byte[] postData) throws ClientProtocolException, IOException
    {
	byte[] retVal = null;
	StatusLine status = null;
	try
	{
	    if (httpClient != null)
	    {
		httpPost = new HttpPost(uri);
		if(headers != null) {
		    httpPost.setHeaders(headers);
		} else {
		    setCommonHeaders(httpPost);
		}
		if (postData != null)
		{
		    HttpEntity entity = new ByteArrayEntity(postData);
		    httpPost.setEntity(entity);
		}
		ICFLogger.d(TAG, "API  : " + uri);
		ICFLogger.d(TAG, "API Post Data : " + new String(postData));

                ICFLogger.d(TAG, "httpPost params= " + httpPost.getParams().toString());
		final HttpResponse response = httpClient.execute(httpPost);
		ICFLogger.d(TAG, "API Response Code for Request  : " + uri + " " + response.getStatusLine().getStatusCode());
		 StringBuilder msgAndCode = new StringBuilder();
		 msgAndCode.append("{\"");
		 int headersAddedCount = 0;
		 for (Header header : response.getAllHeaders())
		 {
		    ICFLogger.d(TAG, "Header name : " + header.getName() + " value : " + header.getValue());
		    if (header.getName().equalsIgnoreCase("statusMsg") || header.getName().equalsIgnoreCase("statusCode")) {
			msgAndCode.append(header.getName());
			msgAndCode.append("\":\"");
			msgAndCode.append(header.getValue());
			msgAndCode.append("\"");
			if (++headersAddedCount < 2) {
			    msgAndCode.append(",\"");
			} else {
			    msgAndCode.append("}");
			}
		    }
		}
		retVal = msgAndCode.toString().getBytes();
		status = response.getStatusLine();
		if (status.getStatusCode() == HttpStatus.SC_OK)
		{
		    HttpEntity entity = response.getEntity();
		    // retVal = EntityUtils.toByteArray(entity);
		    long time = System.currentTimeMillis();
		    
		    // retVal = FileUtil.readInputStream(entity.getContent());
		    ICFLogger.d(TAG, "Time taken to write the content to server : " + (System.currentTimeMillis() - time));		    
		}
	    }
	}
	catch (Exception e)
	{
	    ICFLogger.e(TAG, e);
	    status = new BasicStatusLine(HttpVersion.HTTP_1_0, 9000, "client error");
	}
	return createHttpData(status, retVal);

    }

    /*
    public HttpData getBytesDataByPost(UploadProgressListener listener, String fileName, Object obj) throws ClientProtocolException, IOException
    {
	byte[] retVal = null;
	StatusLine status = null;
	try
	{
	    if (httpClient != null)
	    {
		httpPost = new HttpPost(uri);
		// httpPost.setHeader("Content-Type", "multipart/form-data");
		if (fileName != null)
		{
		    MultipartEntity mpeb = new MultipartEntity();
		    File file = new File(fileName);
		    ProgressFileBody pfb = new ProgressFileBody(file);
		    pfb.setProgressListener(listener);
		    pfb.setRequestObj(obj);

		    mpeb.addPart("companyName", new StringBody(LibUtils.getSelectedUtilityCompany().getName()));
		    mpeb.addPart("userName", new StringBody(LibUtils.getLoggedInUserBean().getUserName()));
		    mpeb.addPart("modulename", new StringBody("Rebate tool"));
		    mpeb.addPart("file", pfb);

		    // ProgressEntity entity = new ProgressEntity(new File(fileName), "application/octet-stream");
		    httpPost.setEntity(mpeb);
		}
		ICFLogger.d(TAG, "API  : " + uri);
		ICFLogger.d(TAG, "API Post Data : " + fileName);

		for (Header header : httpPost.getAllHeaders())
		{
		    ICFLogger.d(TAG, "Header name : " + header.getName() + " value : " + header.getValue());
		}

		final HttpResponse response = httpClient.execute(httpPost);
		ICFLogger.d(TAG, "API Response Code for Request  : " + uri + " " + response.getStatusLine().getStatusCode());
		status = response.getStatusLine();
		if (status.getStatusCode() == HttpStatus.SC_OK)
		{
		    if (listener != null)
		    {
			listener.onSuccess(obj);
		    }
		    HttpEntity entity = response.getEntity();
		    // retVal = EntityUtils.toByteArray(entity);
		    long time = System.currentTimeMillis();
		    retVal = FileUtil.readInputStream(entity.getContent());
		}
		else
		{
		    if (listener != null)
		    {
			listener.onFailure(obj);
		    }
		}
	    }
	}
	catch (Exception e)
	{
	    if (listener != null)
	    {
		listener.onFailure(obj);
	    }
	    status = new BasicStatusLine(HttpVersion.HTTP_1_0, 9000, "client error");
	    ICFLogger.e(TAG, e);
	}
	return createHttpData(status, retVal);

    }
    */

    public static boolean isOnline()
    {
	ConnectivityManager manager = (ConnectivityManager) LibUtils.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
	NetworkInfo info = manager.getActiveNetworkInfo();
	return (info != null) && info.isConnectedOrConnecting();
    }

    public void setHeader(String key, String value)
    {

    }

}
