package com.icf.rebate.networklayer.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SubmissionItem {

    public static final String SURVEY_ID = "survey";
    public static final String TERMS_ID = "termsAndConditions";
    public static final String CONFIRMATION_ID = "rebateConfirmation";
    
    @JsonProperty("name")
    private String name;
    
    @JsonProperty("id")
    private String id;
    
    @JsonProperty("surveyDetail")
    private PartListItem surveyDetail;
    
    @JsonProperty("itemSaved")
    private boolean itemSaved;

    public SubmissionItem(String id) {
 	this.id = id;
     }
    
    public SubmissionItem() {
	super();
    }
   
    
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public boolean isItemSaved()
    {
        return itemSaved;
    }

    public void setItemSaved(boolean itemSaved)
    {
        this.itemSaved = itemSaved;
    }

    public PartListItem getSurveyDetail()
    {
        return surveyDetail;
    }

    public void setSurveyDetail(PartListItem surveyDetail)
    {
        this.surveyDetail = surveyDetail;
    }

    @Override
    public boolean equals(Object o)
    {
	boolean objectsAreEqual = false;
	if (o instanceof SubmissionItem) {
	    SubmissionItem itemToCompare = (SubmissionItem) o;
	    if (this.getId().equalsIgnoreCase(itemToCompare.getId())) {
		objectsAreEqual = true;
	    }
	}
	return objectsAreEqual;
    }
 
}
