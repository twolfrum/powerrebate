package com.icf.rebate.networklayer;

import java.io.File;
import java.util.List;

import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.json.JSONObject;

import android.os.Bundle;
import android.os.Handler.Callback;
import android.os.Message;
import android.text.TextUtils;

import com.icf.rebate.networklayer.model.ResponseBean;
import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.FileUtil;

public class HttpRequest implements Callback
{

    private int requestId;
    private TaskRunnable task;
    // private Context context;
    private HttpManagerListener listener;
    private HttpQueueManager httpManager;
    private DataParser dataParser;
    private String savePath;
    private Object requestObj;

    public HttpRequest(HttpManagerListener listener, DataParser dataParser)
    {
	this.listener = listener;
	this.dataParser = dataParser;
	task = new TaskRunnable();
	httpManager = HttpQueueManager.getInstance();
    }

    public void processGetRequest(int reqID, String url, HttpManagerListener listener)
    {
	try
	{
	    if (checkConnectivity(reqID, null))
	    {

		int reqIdentifier = httpManager.getRequestIdentifier();
		task.init(reqID, url, reqIdentifier, this);
		RequestObject reqObj = new RequestObject(reqIdentifier);
		reqObj.setReqID(reqID);
		reqObj.setHttpManagerListener(listener);
		reqObj.setTask(task);
		requestId = reqID;

		httpManager.addToQueue(reqObj, task);
	    }
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
    }

    static String getResponseCachedFileName(int reqID)
    {
	switch (reqID)
	{
	case ICFHttpManager.REQ_ID_LOGIN:
	    return "login.json";
	case ICFHttpManager.REQ_ID_CONFIG:
	    return "config.json";
	case ICFHttpManager.REQ_ID_RESOURCE_BUNDLE:
	    return "rb.zip";
	default:
	    return null;
	}
    }

    private boolean handleOfflineMode(int reqID, byte[] postData)
    {
	try
	{
	    if (!TextUtils.isEmpty(savePath))
	    {
		JSONObject jobj = new JSONObject(new String(postData));
		String path = FileUtil.getHttpResponseDirectory(jobj.getString("userName"), getResponseCachedFileName(reqID));
		File file = new File(path);
		if (file.exists())
		{

		    byte[] data = FileUtil.getFileContent(path);
		    if (data != null && data.length > 0)
		    {
			Message message = new Message();
			message.obj = dataParser.parse(reqID, data);
			message.arg1 = reqID;
			Bundle bundle = new Bundle();
			bundle.putBoolean("offlineData", true);
			message.setData(bundle);
			listener.dataReceived(message);
			return true;
		    }
		}
	    }
	}
	catch (Exception e)
	{

	}
	return false;
    }

    private boolean checkConnectivity(int reqID, byte[] postData)
    {
	if (!ConnectionManager.isOnline())
	{
	    try
	    {
		if (handleOfflineMode(reqID, postData))
		{
		    return false;
		}
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	    if (listener != null)
	    {
		Message msg = new Message();
		msg.arg1 = reqID;
		msg.obj = LibUtils.getApplicationContext().getResources().getString(R.string.no_network_connection);
		listener.networkConnectivityError(msg);
	    }
	    return false;
	}
	return true;

    }

    public void processPostRequest(int reqID, String url, byte[] postData, HttpManagerListener listener)
    {

	try
	{
	    // if (context != null)]
	    if (checkConnectivity(reqID, postData))
	{
		int reqIdentifier = httpManager.getRequestIdentifier();
		task.init(reqID, url, reqIdentifier, this, true);
		RequestObject reqObj = new RequestObject(reqIdentifier);
		reqObj.setReqID(reqID);
		reqObj.setHttpManagerListener(listener);
		reqObj.setTask(task);
		requestId = reqID;
		task.setPostData(postData);
		httpManager.addToQueue(reqObj, task);
	}
	}
	catch (Exception ex)
	{
	    ex.printStackTrace();
	}
    }

    //
    // public void processPostRequest(int reqID, String url, byte[] postData, HttpManagerListener listener)
    // {
    // processPostRequest(reqID, url, postData, listener, null);
    // }

    public void processGetRequest(int reqID, String url, List<NameValuePair> requestParams, HttpManagerListener listener)
    {
	processGetRequest(reqID, url, URLEncodedUtils.format(requestParams, null), listener);
    }

    public void processGetRequest(int reqID, String url, String requestParams, HttpManagerListener listener)
    {

	try
	{
	    // if (context != null)
	    {
		if (checkConnectivity(reqID, null))
		{
		    int reqIdentifier = httpManager.getRequestIdentifier();
		    task.init(reqID, url + '?' + requestParams, reqIdentifier, this, false);
		    RequestObject reqObj = new RequestObject(reqIdentifier);
		    reqObj.setReqID(reqID);
		    reqObj.setHttpManagerListener(listener);
		    reqObj.setTask(task);
		    requestId = reqID;
		    // task.setPostData(postData);
		    httpManager.addToQueue(reqObj, task);
		}
	    }
	}
	catch (Exception ex)
	{
	    ex.printStackTrace();
	}

    }

    public void cancel()
    {
	// TODO
    }

    @Override
    public boolean handleMessage(Message msg)
    {
	if (listener != null)
	{
	    if (msg != null)
	    {
		if (msg.obj != null)
		{
		    HttpData httpData = (HttpData) msg.obj;
		    if (httpData != null)
		    {
			if (httpData.getStatus().getStatusCode() == HttpStatus.SC_OK)
			{
			    ResponseBean bean = dataParser.parse(requestId, httpData.getData());
			    msg.obj = bean;
			    if (bean != null && !bean.isSuccessResponse() && !(AppConstants.HTTP_AUTHENTICATION_ERROR.equalsIgnoreCase(bean.getStatusCode())))
			    {
				if (handleOfflineMode(task.reqId, task.postData))
				{
				    httpManager.removeReq(msg.arg2);
				    return true;
				}
			    }
			    if (bean != null && bean.isSuccessResponse())
			    {
				if (savePath != null)
				{
				    try
				    {
					FileUtil.writeFile(httpData.getData(), new File(savePath));
				    }
				    catch (Exception e)
				    {
					e.printStackTrace();
				    }
				}
			    }
			    listener.dataReceived(msg);
			    httpManager.removeReq(msg.arg2);
			    return true;
			}
			else
			{
			    if (handleOfflineMode(task.reqId, task.postData))
			    {
				httpManager.removeReq(msg.arg2);
				return true;
			    }
			}
		    }
		}
	    }
	}
	if (listener != null)
	{
	    listener.connectionError(msg);
	}
	httpManager.removeReq(msg.arg2);
	return true;
    }

    public void setHeader(String key, String value)
    {
	task.setHeader(key, value);
    }

    public void setSaveData(String path)
    {
	this.savePath = path;
    }

    public void setRequestObject(Object obj)
    {
	this.requestObj = obj;
    }

    public Object getRequestObj()
    {
	return this.requestObj;
    }

}
