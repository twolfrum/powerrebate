package com.icf.rebate.networklayer;

import com.icf.rebate.networklayer.model.ResponseBean;

public interface DataParser {
	public ResponseBean parse(int requestId, byte[] data);
}
