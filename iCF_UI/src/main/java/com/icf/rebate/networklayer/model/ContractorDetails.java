package com.icf.rebate.networklayer.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ContractorDetails
{
    @JsonProperty("firstName")
    private String firstName;

    @JsonProperty("lastName")
    private String lastName;
    
    @JsonProperty("contractorAccountNumber")
    private String contractorAccountNumber;
    
    @JsonProperty("contractorCompany")
    private String contractorCompany;
    
    @JsonProperty("address1")
    private String address1;
    
    @JsonProperty("address2")
    private String address2;

    @JsonProperty("city")
    private String city;

    @JsonProperty("state")
    private String state;

    @JsonProperty("zip")
    private String zip;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("cell")
    private String cell;

    @JsonProperty("email")
    private String email;

    @JsonProperty("company")
    private String company;

    public String getFirstName()
    {
	return firstName;
    }

    public void setFirstName(String firstName)
    {
	this.firstName = firstName;
    }

    public String getLastName()
    {
	return lastName;
    }

    public void setLastName(String lastName)
    {
	this.lastName = lastName;
    }

    public String getCity()
    {
	return city;
    }

    public void setCity(String city)
    {
	this.city = city;
    }

    public String getState()
    {
	return state;
    }

    public void setState(String state)
    {
	this.state = state;
    }

    public String getZip()
    {
	return zip;
    }

    public void setZip(String zip)
    {
	this.zip = zip;
    }

    public String getPhone()
    {
	return phone;
    }

    public void setPhone(String phone)
    {
	this.phone = phone;
    }

    public String getCell()
    {
	return cell;
    }

    public void setCell(String cell)
    {
	this.cell = cell;
    }

    public String getEmail()
    {
	return email;
    }

    public void setEmail(String email)
    {
	this.email = email;
    }

    public String getCompany()
    {
	return company;
    }

    public void setCompany(String company)
    {
	this.company = company;
    }

    public String getAddress1()
    {
        return address1;
    }

    public void setAddress1(String address1)
    {
        this.address1 = address1;
    }

    public String getAddress2()
    {
        return address2;
    }

    public void setAddress2(String address2)
    {
        this.address2 = address2;
    }

    public String getContractorCompany()
    {
        return contractorCompany;
    }

    public void setContractorCompany(String contractorCompany)
    {
        this.contractorCompany = contractorCompany;
    }

    public String getContractorAccountNumber()
    {
        return contractorAccountNumber;
    }

    public void setContractorAccountNumber(String contractorAccountNumber)
    {
        this.contractorAccountNumber = contractorAccountNumber;
    }

    
}
