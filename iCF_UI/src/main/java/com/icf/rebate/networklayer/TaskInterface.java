package com.icf.rebate.networklayer;

/**
 * 
 * @author varun.t
 * 
 */
public interface TaskInterface
{

    void connect();

    void disconnect();
}
