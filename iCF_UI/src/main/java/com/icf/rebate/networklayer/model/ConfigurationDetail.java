package com.icf.rebate.networklayer.model;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.icf.rebate.app.model.DashboardItem;

public class ConfigurationDetail extends ResponseBean
{

    @JsonProperty("sessionExpTime")
    private int sessionExpiryTime;

    @JsonProperty("sessionExpTimeLogout")
    private int sessionExpTimeLogout;
    
    @JsonProperty("utilityCompanies")
    private ArrayList<UtilityCompany> utilityList;

    @JsonProperty("termsAndCond")
    private String termsAndCond;

    @JsonProperty("analyticsKey")
    private String analyticsKey;

    @JsonProperty("reportIssue")
    private boolean reportIssue;

    @JsonProperty("emailTo")
    private String emailTo;

    @JsonProperty("noOfSubmittedJobs")
    private String noOfSubmittedJobs;

    public ArrayList<UtilityCompany> getUtilityList()
    {
	return utilityList;
    }

    public void setUtilityList(ArrayList<UtilityCompany> utilityList)
    {
	this.utilityList = utilityList;
    }

    public String getNoOfSubmittedJobs()
    {
	return noOfSubmittedJobs;
    }

    public void setNoOfSubmittedJobs(String noOfSubmittedJobs)
    {
	this.noOfSubmittedJobs = noOfSubmittedJobs;
    }

    public int getSessionExpiryTime()
    {
	return sessionExpiryTime;
    }

    public void setSessionExpiryTime(int sessionExpiryTime)
    {
	this.sessionExpiryTime = sessionExpiryTime;
    }

    public int getSessionExpTimeLogout()
    {
        return sessionExpTimeLogout;
    }

    public void setSessionExpTimeLogout(int sessionExpTimeLogout)
    {
        this.sessionExpTimeLogout = sessionExpTimeLogout;
    }

    public String getTermsAndCond()
    {
	return termsAndCond;
    }

    public void setTermsAndCond(String termsAndCond)
    {
	this.termsAndCond = termsAndCond;
    }

    public String getAnalyticsKey()
    {
	return analyticsKey;
    }

    public void setAnalyticsKey(String analyticsKey)
    {
	this.analyticsKey = analyticsKey;
    }

    public boolean isReportIssue()
    {
	return reportIssue;
    }

    public void setReportIssue(boolean reportIssue)
    {
	this.reportIssue = reportIssue;
    }

    public String getEmailTo()
    {
	return emailTo;
    }

    public void setEmailTo(String emailTo)
    {
	this.emailTo = emailTo;
    }

    public static class UtilityCompany
    {
	@JsonProperty("companyId")
	private int id;

	@JsonProperty("companyName")
	private String name;

	@JsonProperty("icon")
	private String icon;

	@JsonProperty("dashboardItems")
	private ArrayList<DashboardItem> dashboardItemList;

	public int getId()
	{
	    return id;
	}

	public void setId(int id)
	{
	    this.id = id;
	}

	public String getName()
	{
	    return name;
	}

	public void setName(String name)
	{
	    this.name = name;
	}

	public String getIcon()
	{
	    return icon;
	}

	public void setIcon(String icon)
	{
	    this.icon = icon;
	}

	public ArrayList<DashboardItem> getDashboardItemList()
	{
	    return dashboardItemList;
	}

	public void setDashboardItemList(ArrayList<DashboardItem> dashboardItemList)
	{
	    this.dashboardItemList = dashboardItemList;
	}
	
	

	@Override
	public String toString()
	{
	    return name;
	}
    }

}
