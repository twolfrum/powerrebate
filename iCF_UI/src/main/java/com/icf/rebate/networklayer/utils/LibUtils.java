package com.icf.rebate.networklayer.utils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.media.ExifInterface;
import android.os.Build;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;

import io.fabric.sdk.android.Fabric;

import com.icf.rebate.networklayer.model.ConfigurationDetail;
import com.icf.rebate.networklayer.model.ConfigurationDetail.UtilityCompany;
import com.icf.rebate.networklayer.model.CustomerInfo;
import com.icf.rebate.networklayer.model.LoginResponseBean;
import com.crashlytics.android.Crashlytics;
import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.util.FileUtil;
import com.icf.rebate.ui.util.GPS;
import com.icf.rebate.ui.util.ICFLogger;
import com.icf.rebate.ui.util.rebate.DuctSealingRebateTable;
import com.icf.rebate.ui.util.rebate.EnthalpyTable;
import com.icf.rebate.ui.util.rebate.EquipmentListRebateTable;
import com.icf.rebate.ui.util.rebate.ICFSQLiteOpenHelper;
import com.icf.rebate.ui.util.rebate.InsulationTable;
import com.icf.rebate.ui.util.rebate.ProjectCostRebateTable;
import com.icf.rebate.ui.util.rebate.QIVTuneUpRebateTable;
import com.icf.rebate.ui.util.rebate.ThermostatRebateTable;
import com.icf.rebate.ui.util.rebate.WindowDoorTable;
import com.icf.ameren.rebate.ui.BuildConfig;

public class LibUtils {
    private static String SERVER_URL = null;

    public static final String FILES_RB = "RB";

    // TODO: remove this later
    public static final boolean FAKE_DROPBOX_URL = true;

    private static final String DEFAULT_SHARED_PREFERENCE = "DEFAULT_SHARED_PREFERENCE";

    public static final String KEY_APP_INFO_USERNAME = "USERNAME";

    public static final String KEY_USER_INFO_PIN = "pin";

    public static final String KEY_USER_INFO_UTILITY_COMPANY = "company";

    public static final String KEY_ANALYTICS = "analytics_key";

    private static final String TAG = LibUtils.class.getName();

    private static Context appContext;

    public static boolean UNTRUSTED_SUPPORTED = true;

    private static int[] successCodeList = new int[]{0, 9105};

    private static SharedPreferences sharedPreference;

    private static UtilityCompany selectedUtilityCompany = null;


    public static UtilityCompany getSelectedUtilityCompany() {
        return selectedUtilityCompany;
    }

    public static void setSelectedUtilityCompany(UtilityCompany selectedUtilityCompany) {
        // Crashlytics custom tag for utility company
        if (selectedUtilityCompany != null && Fabric.isInitialized()) {
            Crashlytics.setString("utilityCompanyId", String.valueOf(selectedUtilityCompany.getId()));
            Log.d(TAG, "Setting crashlytics custom tag utilityComanyId=" + selectedUtilityCompany.getId());
        }
        RebateManager.getInstance().utilityCompanyChanged();
        LibUtils.selectedUtilityCompany = selectedUtilityCompany;
        if (selectedUtilityCompany != null) {
            setDataForUser(LibUtils.getLoggedInUserBean().getUserName(), KEY_USER_INFO_UTILITY_COMPANY, String.valueOf(selectedUtilityCompany.getId()));
        }
    }

    public static void setApplicationContext(Context context) {
        appContext = context;
        sharedPreference = context.getSharedPreferences(DEFAULT_SHARED_PREFERENCE, Context.MODE_PRIVATE);
    }

    public static Context getApplicationContext() {
        return appContext;
    }

    public static String getServerURL() {
        return LibUtils.SERVER_URL;
    }

    public static String getURLEncodedString(String str) {
        if (str != null) {
            try {
                return URLEncoder.encode(str, "UTF-8");
            } catch (Exception e) {
                Log.d("URlencoded", "Issue in URL encoder");
            }
        }
        return "";
    }

    public static String getURLDecodedString(String str) {
        if (str != null) {
            try {
                return URLDecoder.decode(str, "UTF-8");
            } catch (Exception e) {
                Log.d("URLDecoded", "Issue in URL Decoder");
            }
        }
        return "";
    }

    public static boolean isSuccessResponse(int errorCode) {
        if (successCodeList != null) {
            for (int a : successCodeList) {
                if (errorCode == a) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isSuccessResponse(String errorCode) {
        if (errorCode != null) {
            errorCode = errorCode.trim();
        }
        int err = -1;
        try {
            err = Integer.parseInt(errorCode);
        } catch (Exception e) {

        }
        return isSuccessResponse(err);
    }

    public static String buildVersion = null;

    public static enum RELEASETYPE {
        TESTING, RELEASE;
    }

    public static RELEASETYPE BUILDTYPE;

    private static LoginResponseBean loginResponseBean;

    private static ConfigurationDetail configurationDetail;

    public static boolean RESET_PENDING_JOBS;

    public static boolean isTesting() {
    /*
	 * if (LibUtils.BUILDTYPE.equals(LibUtils.RELEASETYPE.TESTING)) { return true; }
	 */
        return false;
    }

    public static int parseInteger(String argString, int defaultValue) {
        try {
            return Integer.parseInt(argString.trim());
        } catch (Exception e) {

        }
        return defaultValue;
    }

    public static float parseFloat(String argString, float defaultValue) {
        if (argString != null) {
            argString = argString.replaceAll("[^\\w\\s!.]", "");
            try {
                return Float.parseFloat(argString.trim());
            } catch (Exception e) {

            }
        }
        return defaultValue;
    }

    public static double parseDouble(String argString, double defaultValue) {
        try {
            return Double.parseDouble(argString.trim());
        } catch (Exception e) {

        }
        return defaultValue;
    }

    public static String formatNumber(String format, Object number) {
        DecimalFormat formatter = new DecimalFormat(format);
        try {
            return formatter.format(number);
        } catch (Exception e) {

        }
        return null;
    }

    public static void getDisplayInches(StringBuffer buf) {
        DisplayMetrics dm = new DisplayMetrics();
        WindowManager wmManager = (WindowManager) appContext.getSystemService(Context.WINDOW_SERVICE);
        wmManager.getDefaultDisplay().getMetrics(dm);
        double x = Math.pow(dm.widthPixels / dm.xdpi, 2);
        double y = Math.pow(dm.heightPixels / dm.ydpi, 2);
        double screenInches = Math.sqrt(x + y);
        DecimalFormat df = new DecimalFormat("0.00");
        String temp = df.format(screenInches);
        Log.d("debug", "Screen inches : " + temp);
        buf.append(temp);
        buf.append("_dpi-").append(dm.densityDpi);
    }

    public static String getUserAgent() {
        StringBuffer buf = new StringBuffer();
        buf.append(Build.MANUFACTURER).append("_");
        getDisplayInches(buf);
        buf.append("_");
        buf.append(Build.MODEL).append("_");
        buf.append("Android-");
        buf.append(Build.VERSION.RELEASE).append("_");
        buf.append("AppId-");
        buf.append("SBConnect").append("_");
        try {
            PackageInfo info = appContext.getPackageManager().getPackageInfo(appContext.getPackageName(), 0);
            if (info != null) {
                buf.append(info.versionName);
            }
        } catch (NameNotFoundException e) {
            buf.append("n/a");
        }
        Log.d("", "UserAgent" + buf.toString());
        return buf.toString();
    }

    public static void init(String serverURL) {
        LibUtils.SERVER_URL = serverURL;
    }

    public static void setServerUrl(String serverURL) {
        LibUtils.SERVER_URL = serverURL;
    }

    public static String getCurrentLocale() {
        return Locale.getDefault().getLanguage();
    }

    // private static LoginResponseBean userDetail;
    //
    // public static void setUserDetail(LoginResponseBean userDetail) {
    // LibUtils.userDetail = userDetail;
    // }
    //
    // public static LoginResponseBean getUserDetail() {
    // return LibUtils.userDetail;
    // }

    public static LoginResponseBean getLoggedInUserBean() {
        return loginResponseBean;
    }

    public static void setLoggedInUserBean(LoginResponseBean bean) {
        LibUtils.loginResponseBean = bean;
    }

    public static void setConfigurationDetail(ConfigurationDetail detail) {
        configurationDetail = detail;
    }

    public static ConfigurationDetail getConfigurationDetail() {
        return configurationDetail;
    }

    public static void setAppInfo(String key, String value) {
        sharedPreference.edit().putString(key, value).commit();
    }

    public static String getAppInfo(String key, String defaultValue) {
        return sharedPreference.getString(key, defaultValue);
    }

    public static void removeAppInfo(String key) {
        sharedPreference.edit().remove(key);
    }

    public static String getDataForUser(String userName, String key) {
        return appContext.getSharedPreferences("pref_" + userName, Context.MODE_PRIVATE).getString(key, null);
    }

    public static void setDataForUser(String userName, String key, String value) {
        appContext.getSharedPreferences("pref_" + userName, Context.MODE_PRIVATE).edit().putString(key, value).commit();
    }

    public static String getCurrentLogFile() {
        String retVal = null;
        if (sharedPreference != null && appContext != null) {
            Resources mR = appContext.getResources();
            retVal = sharedPreference.getString(mR.getString(R.string.current_log_file_pref), null);

        }

        return retVal;
    }

    public static void setCurrentLogFile(String name) {
        if (sharedPreference != null && appContext != null) {
            Resources mR = appContext.getResources();
            Editor editor = sharedPreference.edit();
            editor.putString(mR.getString(R.string.current_log_file_pref), name);
            editor.apply();

        }

    }

    // public static String getPINForUser(String userName)
    // {
    // SharedPreferences pref = appContext.getSharedPreferences("pref_" + userName, Context.MODE_PRIVATE);
    // return pref.getString("pin", null);
    // }
    //
    // public static void setPINForUser(String userName, String pin)
    // {
    // SharedPreferences pref = appContext.getSharedPreferences("pref_" + userName, Context.MODE_PRIVATE);
    // Editor ed = pref.edit();
    // ed.putString("pin", pin);
    // ed.commit();
    // }

    public static String getDeviceType() {
        return appContext.getResources().getBoolean(R.bool.isTablet) ? "tablet" : "phone";
    }

    // TODO should return the actual density.
    public static String getDeviceDensity() {
        return "xxhdpi";
    }

    public static void insertRebateRules(int utilityCompanyId, SQLiteDatabase database, byte[] data, String utilityCompanyName) {
        Log.d(TAG, "---------------------------------------------------------------");
        Log.d(TAG, "| Inserting rebate information for " + utilityCompanyName);
        Log.d(TAG, "---------------------------------------------------------------");

        String[] rebateTypes = new String[] {
            "qiv-tuneup", //idx=0
            "duct sealing", //idx=1
            "project cost", //idx=2
            "thermostat", //idx=3
            "window and door", //idx=4
            "insulation", //idx=5
            "equipment", //idx=6
        };
        String[] rebateTypeKeys = new String[] {
            "house type", //idx=0: qiv-tuneup
            "house type", //idx=1: duct sealing
            "projectCostMultiplier", //idx=2: project cost
            "upgrade measure name", //idx=3: thermostat
            "doorOrWindowTypeId", //idx=4: window and door
            "insulationTypeID", //idx=5: insulation
            "house type", //idx=6: equipment
        };
        int rebateTypeIdx = 0;
        String hdrSplit[];
        Class clz = null;
        Method addIntervalMethod = null;
        Method insertMethod = null;
        boolean inserting = false;

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(data)));
            String line = null;
            database.beginTransaction();

            while ((line = reader.readLine()) != null) {
                //don't process empty lines
                if (line.startsWith(",,,,")) {
                    continue;
                }

                try {
                    hdrSplit = line.split(",");
                } catch (Exception e) {
                    continue;
                }

                if (line.toLowerCase().startsWith("rebatetype") ||
                        line.toLowerCase().startsWith("customertype")) {
                    inserting = false;
                    Class insertMethodValuesClass = null;
                    while (true) {
                        if (hdrSplit[2].equalsIgnoreCase(rebateTypeKeys[rebateTypeIdx])) {
                            switch (rebateTypeIdx) {
                                case 0: //qiv-tuneup
                                    clz = QIVTuneUpRebateTable.class;
                                    insertMethodValuesClass = String.class;
                                    break;
                                case 1: //duct sealing
                                    clz = DuctSealingRebateTable.class;
                                    insertMethodValuesClass = String.class;
                                    break;
                                case 2: //project cost
                                    clz = ProjectCostRebateTable.class;
                                    insertMethodValuesClass = String.class;
                                    break;
                                case 3: //thermostat
                                    clz = ThermostatRebateTable.class;
                                    insertMethodValuesClass = String.class;
                                    break;
                                case 4: //window and door
                                    clz = WindowDoorTable.class;
                                    insertMethodValuesClass = String[].class;
                                    break;
                                case 5: //insulation
                                    clz = InsulationTable.class;
                                    insertMethodValuesClass = String[].class;
                                    break;
                                case 6: //equipment
                                    clz = EquipmentListRebateTable.class;
                                    insertMethodValuesClass = String[].class;
                            }
                            try {
                                addIntervalMethod = clz.getMethod("addRebateIntervalFields",
                                        int.class, SQLiteDatabase.class, String.class);
                                insertMethod = clz.getMethod("insert",
                                        int.class, SQLiteDatabase.class, insertMethodValuesClass);
                                addIntervalMethod.invoke(null, utilityCompanyId, database, line);
                                inserting = true;
                                break;
                            } catch (Exception e) {
                                Log.e(TAG, "Injecting " + rebateTypes[rebateTypeIdx] +
                                    "rebate information failed for utility " + utilityCompanyName);
                                ICFLogger.e(TAG, e);
                            } finally {
                                rebateTypeIdx++;
                            }
                            break;
                        }
                        rebateTypeIdx++;
                        if (rebateTypeIdx == rebateTypeKeys.length) break;
                    }
                } else if (inserting) {
                    if (insertMethod.getParameterTypes()[2] == String.class)
                        insertMethod.invoke(null, utilityCompanyId, database, line);
                    else if (insertMethod.getParameterTypes()[2] == String[].class)
                        insertMethod.invoke(null, utilityCompanyId, database, line.split(","));
                }
            }
            database.setTransactionSuccessful();
        } catch (Exception e) {
            Log.e(TAG, "Injecting rebate information failed for utility " + utilityCompanyName);
            ICFLogger.e(TAG, e);
        } finally {
            database.endTransaction();
            database.close();
        }
    }

    public static void processRebateFiles() {
        try {
            ICFSQLiteOpenHelper helper = new ICFSQLiteOpenHelper(getApplicationContext());
            helper.truncateRebateTable();
            List<UtilityCompany> utilityList = getConfigurationDetail().getUtilityList();
            for (UtilityCompany utilityCompany : utilityList) {
                try {
                    String fileName = utilityCompany.getId() + "_rebate.csv";
                    byte[] data = FileUtil.getResourceBundleFileContent(fileName);
                    insertRebateRules(utilityCompany.getId(), helper.getWritableDatabase(), data, utilityCompany.getName());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            //populate enthalpy table if necessary
            populateEnthalpyTable(helper.getWritableDatabase());
        } catch (Exception e) {
            ICFLogger.e(TAG, e);
        }

    }

    public static void populateEnthalpyTable(SQLiteDatabase database) {

        if (EnthalpyTable.isPopulated(database)) return;

        try {
            byte[] data = FileUtil.readInputStream(appContext.getAssets().open("enthalpy.csv"));
            BufferedReader reader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(data)));
            String line = null;
            database.beginTransaction();

            while ((line = reader.readLine()) != null) {
                try {
                    String[] columnData = line.split(",");
                    EnthalpyTable.insert(database, columnData);
                } catch (Exception e) {
                    ICFLogger.e(TAG, e);
                }
            }
            reader.close();

            database.setTransactionSuccessful();
            database.endTransaction();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            database.close();
        }

    }

    public static void updateExifData(String filePath) {
        try {
            ExifInterface exifInterface = new ExifInterface(filePath);

            CustomerInfo info = RebateManager.getInstance().getFormBean().getCustomerInfo();
            double latitude = info.getGeoLat();
            double longitude = info.getGeoLong();
            exifInterface.setAttribute(ExifInterface.TAG_GPS_LATITUDE, GPS.convert(latitude));
            exifInterface.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, GPS.latitudeRef(latitude));
            exifInterface.setAttribute(ExifInterface.TAG_GPS_LONGITUDE, GPS.convert(longitude));
            exifInterface.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, GPS.longitudeRef(longitude));
            exifInterface.saveAttributes();
        } catch (Exception e) {
            ICFLogger.e(TAG, "Error in updateExifData", e);
        }
    }

    public static List<String> getYearColumnName(String[] columnNames, String rebateCalcDate) {
	/*int year = Calendar.getInstance().get(Calendar.YEAR);
	return "incentive_" + year;*/

        // Finds a list of rebate intervals that match the current date
        List<String> hits = new ArrayList<String>();
        for (int i = columnNames.length - 1; i >= 0; i--) {
            String col;
            if ((col = columnNames[i]).matches(".*_\\d{2}_\\d{2}_\\d{4}__\\d{2}_\\d{2}_\\d{4}")) {
                int dateToken = col.indexOf("__");
                String[] start = col.substring(col.indexOf("_") + 1, dateToken).split("_");
                String[] end = col.substring(dateToken + 2).split("_");

                Calendar beginInterval = new GregorianCalendar(Integer.parseInt(start[2]), Integer.parseInt(start[0]) - 1, Integer.parseInt(start[1]));
                Calendar endInterval = new GregorianCalendar(Integer.parseInt(end[2]), Integer.parseInt(end[0]) - 1, Integer.parseInt(end[1]));
                Calendar matchDate = new GregorianCalendar();

                if (rebateCalcDate != null) {
                    String[] dateTokens = rebateCalcDate.split("/");
                    matchDate.set(Integer.parseInt(dateTokens[2]),
                            Integer.parseInt(dateTokens[0]) - 1,
                            Integer.parseInt(dateTokens[1]));
                }
                SimpleDateFormat fmt = new SimpleDateFormat("MM-dd-yyyy");
                String dbg = fmt.format(matchDate.getTime());
                if (matchDate.after(beginInterval) && matchDate.before(endInterval)) {
                    hits.add(col);
                }
            }
        }
        return hits;
    }

    public static String md5Hash(String input) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(input.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString().toUpperCase();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }

    public static String getSqlQueryString(String tableName, String[] columns, String selectionString, String[] selectionArgs, String groupBy, String having, String orderBy) {
        StringBuffer buf = new StringBuffer("select ");
        if (columns != null && columns.length > 0) {
            for (String column : columns) {
                buf.append(column + ",");
            }
            buf.deleteCharAt(buf.length() - 1);
        }

        buf.append(" from ");
        buf.append(tableName + " ");

        if (selectionString != null && selectionArgs != null) {
            buf.append("where ");
            Pattern p = Pattern.compile("\\?");
            Matcher m = p.matcher(selectionString);
            int i = 0;

            while (m.find()) {
                m.appendReplacement(buf, selectionArgs[i]);
                i++;
            }
            m.appendTail(buf);
        }
        if (groupBy != null) {
            buf.append("GROUP BY " + groupBy);
        }

        if (having != null) {
            buf.append("HAVING " + having);
        }

        if (orderBy != null) {
            buf.append("ORDER BY " + orderBy);
        }
        return buf.toString();
    }

}
