package com.icf.rebate.networklayer.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomerInfo
{
    @JsonProperty("firstName")
    private String firstName;

    @JsonProperty("lastName")
    private String lastName;

    @JsonProperty("jobId")
    private String jobId;

    @JsonProperty("accountNumber")
    private String customerAccNo;
    
    @JsonProperty("accountNumber2")
    private String customerAccNo2;

    @JsonProperty("gpsAddress")
    private String geoAddress;

    @JsonProperty("address")
    private String customerAddress;

    @JsonProperty("email")
    private String emailAddress;
    
    @JsonProperty("phone")
    private String phone;

    @JsonProperty("houseType")
    private int houseType;

    @JsonProperty("rebateOption")
    private int rebateOption;

    @JsonProperty("geoLat")
    private double geoLat;

    @JsonProperty("geoLong")
    private double geoLong;

    @JsonProperty("customerAddress")
    private RebateDifferentPerson diffPerson;

    @JsonProperty("jobState")
    private int state;

    @JsonProperty("city")
    private String customercity;

    @JsonProperty("state")
    private String customerstate;

    @JsonProperty("country")
    private String customerCountry;

    @JsonProperty("zip")
    private String customerzipCode;

    @JsonProperty("isAddressEdited")
    private boolean isAddressEdited = false;

    @JsonProperty("canEditHouseOrCustomerType")
    private boolean canEditHouseOrCustomerType = true;

    @JsonProperty("technicianName")
    private String technicianName;

    @JsonProperty("technicianId")
    private String technicianId;

    @JsonProperty("technicianPhone")
    private String technicianPhone;

    @JsonProperty("customerIncentive")
    private String customerIncentive;

    @JsonProperty("jobRootPath")
    private String jobRootPath;
    
    // @JsonProperty("customerType")
    // private String customerType;

    public boolean canEditHouseOrCustomerType()
    {
	return canEditHouseOrCustomerType;
    }

    public void setCanEditHouseOrCustomerType(boolean canEdit)
    {
	this.canEditHouseOrCustomerType = canEdit;
    }

    public boolean isAddressEdited()
    {
	return isAddressEdited;
    }

    public void setAddressEdited(boolean isAddressEdited)
    {
	this.isAddressEdited = isAddressEdited;
    }

    public static final int NEW_JOB = 0;

    public static final int INCOMPLETED_JOB = 1;

    public static final int COMPLETED_JOB = 2;

    public static final int SUBMISSION_IN_PROGRESS = 3;

    public static final int SUBMISSION_ERROR = 4;

    public static final int SUBMITTED = 5;

    /** ---- **/

    public static final int REBATE_CURRENT_BILL = 1;

    public static final int REBATE_UTILITY_COMPANY = 2;

    public static final int REBATE_DIFFERENT_PERSON = 3;

    /** ----- **/

    public static final int SINGLE_FAMILY_TYPE = 1;

    public static final int MULTIPLE_FAMILY_TYPE = 2;

    public String getFirstName()
    {
	return (firstName == null ? "" : firstName);
    }

    public void setFirstName(String firstName)
    {
	this.firstName = firstName;
    }

    public String getLastName()
    {
	return (lastName == null ? "" : lastName);
    }

    public void setLastName(String lastName)
    {
	this.lastName = lastName;
    }

    public String getJobId()
    {
	return jobId;
    }

    public void setJobId(String jobId)
    {
	this.jobId = jobId;
    }

    public String getCustomerAccNo()
    {
	return customerAccNo;
    }

    public void setCustomerAccNo(String customerAccNo)
    {
	this.customerAccNo = customerAccNo;
    }

    public String getGeoAddress()
    {
	return geoAddress;
    }

    public void setGeoAddress(String geoAddress)
    {
	this.geoAddress = geoAddress;
    }

    public String getCustomerAddress()
    {
	return customerAddress;
    }

    public void setCustomerAddress(String customerAddress)
    {
	this.customerAddress = customerAddress;
    }

    public String getEmailAddress()
    {
	return emailAddress;
    }

    public void setEmailAddress(String emailAddress)
    {
	this.emailAddress = emailAddress;
    }

    public String getCustomerCity()
    {
	return customercity;
    }

    public void setCustomercity(String customercity)
    {
	this.customercity = customercity;
    }

    public String getCustomerState()
    {
	return customerstate;
    }

    public void setCustomerstate(String customerstate)
    {
	this.customerstate = customerstate;
    }

    public String getCustomerCountry()
    {
	return customerCountry;
    }

    public void setCustomerCountry(String customerCountry)
    {
	this.customerCountry = customerCountry;
    }

    public String getCustomerZipCode()
    {
	return customerzipCode;
    }

    public void setCustomerzipCode(String customerzipCode)
    {
	this.customerzipCode = customerzipCode;
    }

    public int getHouseType()
    {
	return houseType;
    }

    public void setHouseType(int houseType)
    {
	this.houseType = houseType;
    }

    public int getRebateOption()
    {
	return rebateOption;
    }

    public void setRebateOption(int rebateOption)
    {
	this.rebateOption = rebateOption;
    }

    public double getGeoLat()
    {
	return geoLat;
    }

    public void setGeoLat(double geoLat)
    {
	this.geoLat = geoLat;
    }

    public double getGeoLong()
    {
	return geoLong;
    }

    public void setGeoLong(double geoLong)
    {
	this.geoLong = geoLong;
    }

    public RebateDifferentPerson getDiffPerson()
    {
	return diffPerson;
    }

    public void setDiffPerson(RebateDifferentPerson diffPerson)
    {
	this.diffPerson = diffPerson;
    }

    public int getState()
    {
	return state;
    }

    public void setState(int submitState)
    {
	this.state = submitState;
    }

    public String getCustomerAccNo2()
    {
        return customerAccNo2;
    }

    public void setCustomerAccNo2(String customerAccNo2)
    {
        this.customerAccNo2 = customerAccNo2;
    }

    public String getPhone()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getTechnicianName() {
        return technicianName;
    }

    public void setTechnicianName(String technicianName) {
        this.technicianName = technicianName;
    }

    public String getTechnicianId() {
        return technicianId;
    }

    public void setTechnicianId(String technicianId) {
        this.technicianId = technicianId;
    }

    public String getTechnicianPhone() {
        return technicianPhone;
    }

    public void setTechnicianPhone(String technicianPhone) {
        this.technicianPhone = technicianPhone;
    }

    public String getCustomerIncentive() {
        return customerIncentive;
    }

    public void setCustomerIncentive(String customerIncentive) {
        this.customerIncentive = customerIncentive;
    }

    public String getJobRootPath()
    {
	return jobRootPath;
    }

    public void setJobRootPath(String jobRootPath)
    {
	this.jobRootPath = jobRootPath;
    }

    public static class RebateDifferentPerson
    {
	@JsonProperty("firstName")
	private String firstName;

	@JsonProperty("lastName")
	private String lastName;

	@JsonProperty("address")
	private String address;

	@JsonProperty("city")
	private String city;

	@JsonProperty("state")
	private String state;

	@JsonProperty("zip")
	private String zipCode;
	
	@JsonProperty("phone")
	private String phone;
	
	@JsonProperty("email")
	private String email;

	public String getFirstName()
	{
	    return firstName;
	}

	public void setFirstName(String firstName)
	{
	    this.firstName = firstName;
	}

	public String getLastName()
	{
	    return lastName;
	}

	public void setLastName(String lastName)
	{
	    this.lastName = lastName;
	}

	public String getAddress()
	{
	    return address;
	}

	public void setAddress(String address)
	{
	    this.address = address;
	}

	public String getCity()
	{
	    return city;
	}

	public void setCity(String city)
	{
	    this.city = city;
	}

	public String getState()
	{
	    return state;
	}

	public void setState(String state)
	{
	    this.state = state;
	}

	public String getZipCode()
	{
	    return zipCode;
	}

	public void setZipCode(String zipCode)
	{
	    this.zipCode = zipCode;
	}

	public String getPhone()
	{
	    return phone;
	}

	public void setPhone(String phone)
	{
	    this.phone = phone;
	}

	public String getEmail()
	{
	    return email;
	}

	public void setEmail(String email)
	{
	    this.email = email;
	}
	
	
	
	
    }

    public void updateAppDetail(AppDetails appDetails)
    {
	if (appDetails != null)
	{
	    appDetails.setRebateToDifferentPerson(false);
	    appDetails.setRebateToUtility(false);
	    appDetails.setRebateToCurrentBill(false);
	    if (rebateOption == CustomerInfo.REBATE_CURRENT_BILL)
	    {
		appDetails.setRebateToCurrentBill(true);
	    }
	    else if (rebateOption == CustomerInfo.REBATE_DIFFERENT_PERSON)
	    {
		appDetails.setRebateToDifferentPerson(true);
	    }
	    else if (rebateOption == CustomerInfo.REBATE_UTILITY_COMPANY)
	    {
		appDetails.setRebateToUtility(true);
	    }
	}
    }

    
}
