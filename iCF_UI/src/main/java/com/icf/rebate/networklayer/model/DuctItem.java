package com.icf.rebate.networklayer.model;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DuctItem
{
    @JsonProperty("name")
    private String name;

    @JsonProperty("options")
    private ArrayList<UIElement> uiList;

    public String getName()
    {
	return name;
    }

    public void setName(String name)
    {
	this.name = name;
    }

    public ArrayList<UIElement> getUiList()
    {
	return uiList;
    }

    public void setUiList(ArrayList<UIElement> uiList)
    {
	this.uiList = uiList;
    }

}
