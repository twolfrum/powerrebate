package com.icf.rebate.networklayer;

import java.io.File;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicHeader;

import android.os.Handler.Callback;
import android.os.Message;

import com.icf.rebate.networklayer.utils.LibUtils;

/**
 * Task to be executed by HttpManger.
 * 
 * @author varun.t
 * 
 */
public class TaskRunnable implements Runnable, TaskInterface
{

    Callback reponseListener;
    String url;
    int reqId;
    boolean isPostReq = false;
    List<NameValuePair> nameValuePair;
    ConnectionManager conManager;
    int reqNumber;
    boolean isBasicAuthNeeded = false;
    String user;
    String pass;
    byte[] postData;

    public void setBasicAuthRequired(String username, String password)
    {

	if (username != null && password != null)
	{
	    isBasicAuthNeeded = true;
	    this.user = username;
	    this.pass = password;
	}
	else
	{
	    isBasicAuthNeeded = false;
	}
    }

    public TaskRunnable()
    {

    }

    public void init(int reqId, String url, int reqNumber, Callback reponseListener)
    {
	init(reqId, url, reqNumber, reponseListener, false);
    }

    public void init(int reqId, String url, int reqNumber, Callback reponseListener, boolean isPostReq)
    {

	this.reponseListener = reponseListener;
	this.url = url;
	this.reqId = reqId;
	this.reqNumber = reqNumber;
	this.isPostReq = isPostReq;
	this.isBasicAuthNeeded = false;
    }

    public void addNameValuePair(List<NameValuePair> nameValuePair)
    {
	if (nameValuePair != null)
	{
	    this.isPostReq = true;
	    this.nameValuePair = nameValuePair;
	}
	else
	{
	    this.isPostReq = false;
	}

    }

    private void sendData(HttpData data)
    {

	Message message = new Message();
	message.obj = data;
	message.arg1 = reqId;
	message.arg2 = reqNumber;
	if (reponseListener != null)
	{
	    reponseListener.handleMessage(message);
	}
    }

    @Override
    public void connect()
    {
    }

    @Override
    public void disconnect()
    {
	reponseListener = null;
	if (conManager != null)
	{
	    conManager.abortReq();
	    conManager = null;
	}
    }

    @Override
    public void run()
    {

	if (isBasicAuthNeeded)
	{
	    conManager = new ConnectionManager(url, user, pass);
	}
	else
	{
	    conManager = new ConnectionManager(url);
	}

	HttpData data = null;
	try
	{

	    if (!isPostReq)
	    {
		data = conManager.getBytesDataByGet();
	    }
	    else
	    {
//		if (reqId != ICFHttpManager.REQ_ID_SUBMIT)
		{
		    if (postData == null)
		    {
			data = conManager.getBytesDataByPost(nameValuePair);
		    }
		    else
		    {
			Header[] headers = null;
			//TODO: need to find a generic way to do this. TaskRunnable shouldn't be using any application specific request ids.
			if (reqId == ICFHttpManager.REQ_ID_SUBMIT) {
			    headers = new Header[4];
			    headers[0] = new BasicHeader("username", LibUtils.getLoggedInUserBean().getUserName());
			    headers[1] = new BasicHeader("password", LibUtils.getLoggedInUserBean().getPassword());
			    headers[2] = new BasicHeader("companyName", LibUtils.getSelectedUtilityCompany().getName());
			    headers[3] = new BasicHeader("Content-Type", "application/octet-stream");
			    data = conManager.getBytesDataByPost(headers, new File(new String(postData)));
			} 
			else if (reqId == ICFHttpManager.REQ_ID_RESET_PASSWORD) { //TODO change the response from the server so it includes statusCode and statusMessage in json format in the body, that is how this module is set up to take the data, making a special case for now
			    data = conManager.getBytesDataByPostParseHeadersToJson(headers, postData);
			}
			else {
			    data = conManager.getBytesDataByPost(headers, postData);
			}
		    }
		}
//		else
//		{
//		    data = conManager.getBytesDataByPost(progressListener, new String(postData), requestObj);
//		}
	    }

	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}

	// if (isConnected)
	{
	    sendData(data);
	}
    }

    public void setPostData(byte[] postData)
    {
	this.postData = postData;
    }

    public void setHeader(String key, String value)
    {
	conManager.setHeader(key, value);
    }
}
