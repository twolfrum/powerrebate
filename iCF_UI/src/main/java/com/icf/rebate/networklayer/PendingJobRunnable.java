package com.icf.rebate.networklayer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.os.Handler;
import android.text.TextUtils;

import com.icf.rebate.networklayer.model.ConfigurationDetail;
import com.icf.rebate.networklayer.model.CustomerInfo;
import com.icf.rebate.networklayer.model.FormResponseBean;
import com.icf.rebate.networklayer.model.PendingJobItem;
import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.rebate.ui.listeners.PendingJobListener;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.DateFormatter;
import com.icf.rebate.ui.util.FileUtil;

public class PendingJobRunnable implements Runnable
{
    private ArrayList<PendingJobItem> pendingJobList;

    private boolean running;

    private int maxJobListSize;

    private boolean initializedFully;

    private Handler mHandler;

    private PendingJobListener mListener;

    private File[] getDirectory(String path)
    {
	File dir = new File(path);
	File[] listFiles = dir.listFiles(new FilenameFilter()
	{

	    @Override
	    public boolean accept(File dir, String filename)
	    {
		File file = new File(dir, filename);
		if (file.isDirectory())
		{
		    return true;
		}
		return false;
	    }
	});
	return listFiles;
    }

    private ArrayList<PendingJobItem> getPendingJobList(File[] listFiles)
    {
	if (listFiles == null)
	{
	    return null;
	}
	ArrayList<PendingJobItem> pendingJobList = new ArrayList<PendingJobItem>();
	for (File file : listFiles)
	{
	    try
	    {
		File tempfile = new File(file, AppConstants.JOB_DOCUMENT_UI);
		if (tempfile.exists())
		{
		    FileInputStream fis = new FileInputStream(tempfile);
		    byte[] data = new byte[fis.available()];
		    fis.read(data);
		    fis.close();

		    FormResponseBean bean = (FormResponseBean) DataParserImpl.getInstance().parse(ICFHttpManager.REQ_ID_FORM, data);
		    if (bean != null)
		    {
			if (bean.getCustomerInfo() != null)
			{
			    long fileTime = tempfile.lastModified();
			    PendingJobItem item = new PendingJobItem();
			    item.setId(bean.getCustomerInfo().getJobId());
			    // item.setBean(bean);
			    item.setDate(DateFormatter.getFormattedDate(fileTime));
			    item.setCustomerName(bean.getCustomerInfo().getFirstName() + " " + bean.getCustomerInfo().getLastName());
			    item.setCustomerAddress(bean.getCustomerInfo().getCustomerAddress());
			    item.setJobState(bean.getCustomerInfo().getState());
			    item.setLastModified(fileTime);
			    item.setFolderPath(file.getAbsolutePath());

			    pendingJobList.add(item);
			}
		    }
		}
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}
	return pendingJobList;
    }

    @Override
    public void run()
    {
	running = true;
	String path = FileUtil.getUserUtilityCompanyDirectory();
	// FileUtil.getCurrentUserDirectory();
	// FileUtil.getUserUtilityCompanyDirectory();
	File[] listFiles = getDirectory(path);

	if (listFiles != null)
	{
	    ArrayList<PendingJobItem> tempJobList = new ArrayList<PendingJobItem>();
	    // for (File companyDir : listFiles)
	    // {
	    // File[] customerDir = getDirectory(companyDir.getAbsolutePath());
	    // ArrayList<PendingJobItem> list = getPendingJobList(customerDir);
	    // if (list != null)
	    // {
	    // tempJobList.addAll(list);
	    // }
	    // }
	    ArrayList<PendingJobItem> list = getPendingJobList(listFiles);
	    if (list != null)
	    {
		tempJobList.addAll(list);
	    }

	    if (tempJobList.size() > 0)
	    {
		tempJobList = sortPendingJobList(tempJobList);
	    }
	    if (pendingJobList != null)
	    {
		pendingJobList.clear();
	    }

	    if (LibUtils.RESET_PENDING_JOBS)
	    {
		for (PendingJobItem item : tempJobList)
		{
		    if (item.getBean() != null && item.getBean().getCustomerInfo() != null && item.getBean().getCustomerInfo().getState() == CustomerInfo.SUBMISSION_IN_PROGRESS)
		    {
			item.getBean().getCustomerInfo().setState(CustomerInfo.SUBMISSION_ERROR);
			item.setJobState(CustomerInfo.SUBMISSION_ERROR);
		    }
		}
		LibUtils.RESET_PENDING_JOBS = false;
	    }
	    pendingJobList = tempJobList;
	}
	initializedFully = true;
	if (mListener != null)
	{
	    mListener.intializedCompleted();
	}
	running = false;
    }

    private ArrayList<PendingJobItem> sortPendingJobList(ArrayList<PendingJobItem> list)
    {
	Collections.sort(list, new Comparator<PendingJobItem>()
	{

	    @Override
	    public int compare(PendingJobItem lhs, PendingJobItem rhs)
	    {
		if (lhs.getLastModified() < rhs.getLastModified())
		{
		    return 1;
		}
		else if (lhs.getLastModified() > rhs.getLastModified())
		{
		    return -1;
		}
		return 0;
	    }
	});
	return validateSubmittedJobs(list);
    }

    public void addOrUpdatePendingJob(PendingJobItem item)
    {
	if (pendingJobList != null)
	{
	    int index = pendingJobList.indexOf(item);
	    if (index != -1)
	    {
		pendingJobList.set(index, item);
	    }
	    else
	    {
		pendingJobList.add(item);
	    }
	    pendingJobList = sortPendingJobList(pendingJobList);
	}
	else
	{
	    pendingJobList = new ArrayList<PendingJobItem>();
	    pendingJobList.add(item);
	}
    }

    public ArrayList<PendingJobItem> getPendingJobItems()
    {
	return pendingJobList;
    }

    public boolean isRunning()
    {
	return running;
    }

    private ArrayList<PendingJobItem> validateSubmittedJobs(ArrayList<PendingJobItem> jobList)
    {
	ArrayList<PendingJobItem> tempList = jobList;
	ArrayList<PendingJobItem> deleteJobList = null;
	ConfigurationDetail configurationDetail = LibUtils.getConfigurationDetail();
	if (configurationDetail != null)
	{
	    maxJobListSize = Integer.valueOf(configurationDetail.getNoOfSubmittedJobs());
	    if (TextUtils.isEmpty(configurationDetail.getNoOfSubmittedJobs()))
	    {
		maxJobListSize = AppConstants.MAX_SUBMITTED_ENTRIES;
	    }
	    ArrayList<PendingJobItem> submittedJobList = new ArrayList<PendingJobItem>();
	    deleteJobList = new ArrayList<PendingJobItem>();
	    int submittedCount = 0;
	    for (int i = 0; i < jobList.size(); i++)
	    {
		if (jobList.get(i).isSubmitted())
		{
		    if (submittedCount < maxJobListSize)
		    {
			submittedJobList.add(jobList.get(i));
		    }
		    else
		    {
			deleteJobList.add(jobList.get(i));
		    }
		    submittedCount++;
		    continue;
		}
		submittedJobList.add(jobList.get(i));
	    }
	    tempList = submittedJobList;
	}
	deleteJobs(deleteJobList);
	return tempList;
    }

    private void deleteJobs(ArrayList<PendingJobItem> deleteJobList)
    {
	if (deleteJobList != null && deleteJobList.size() > 0)
	{
	    for (PendingJobItem jobItem : deleteJobList)
	    {
		if (jobItem != null)
		{
		    FileUtil.deleteEntireFolder(jobItem.getFolderPath(), true);
		}
	    }
	}
    }

    public boolean isInitializedFully(PendingJobListener listener)
    {
	this.mListener = listener;
	return initializedFully;
    }
}
