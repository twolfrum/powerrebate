package com.icf.rebate.networklayer;

import android.os.Message;

/**
 * To be handled by UI modules who make request data calls.
 * 
 * (NOTE : The callback executes on non UI Thread, so delegate your UI processing to main thread.)
 * 
 * @author varun.t
 * 
 */
public interface HttpManagerListener
{

    void connectionError(Message msg);

    void networkConnectivityError(Message msg);

    void dataReceived(Message msg);

}
