/**
 * 
 */
package com.icf.rebate.networklayer.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.icf.rebate.networklayer.model.FormResponseBean.Part;

/**
 * @author Ken Butler Jan 12, 2016
 * PartListItem.java - this is meant to be a base class for beans with a list of {@link Part}
 * Copyright (c) 2016 ICF International
 */
public class PartListItem {

    @JsonProperty("parts")
    private List<Part> parts;

    public List<Part> getParts()
    {
        return parts;
    }

    public void setParts(List<Part> parts)
    {
        this.parts = parts;
    } 
    
}
