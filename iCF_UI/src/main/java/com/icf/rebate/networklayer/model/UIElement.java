package com.icf.rebate.networklayer.model;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UIElement
{

    @JsonProperty("name")
    private String name;

    @JsonProperty("dataType")
    private String dataType;

    @JsonProperty("picture")
    private boolean picture;

    @JsonProperty("inputType")
    private String inputType;

    @JsonProperty("values")
    private ArrayList<String> valueList;

    public String getName()
    {
	return name;
    }

    public void setName(String name)
    {
	this.name = name;
    }

    public String getDataType()
    {
	return dataType;
    }

    public void setDataType(String dataType)
    {
	this.dataType = dataType;
    }

    public boolean isPicture()
    {
	return picture;
    }

    public void setPicture(boolean picture)
    {
	this.picture = picture;
    }

    public String getInputType()
    {
	return inputType;
    }

    public void setInputType(String inputType)
    {
	this.inputType = inputType;
    }

    public ArrayList<String> getValueList()
    {
	return valueList;
    }

    public void setValueList(ArrayList<String> valueList)
    {
	this.valueList = valueList;
    }

}
