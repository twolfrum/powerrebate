package com.icf.rebate.adapter;

import java.util.List;
import java.util.ListIterator;

import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.adapter.EquipmentAdapter.EquipmentHolder;
import com.icf.rebate.networklayer.model.FormResponseBean;
import com.icf.rebate.networklayer.model.FormResponseBean.Category;
import com.icf.rebate.networklayer.model.FormResponseBean.Equipment;
import com.icf.rebate.networklayer.model.FormResponseBean.Insulation;
import com.icf.rebate.networklayer.model.FormResponseBean.InsulationItem;
import com.icf.rebate.networklayer.model.FormResponseBean.Item;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class InsulationAdapter extends CommonAdapter<Insulation> implements AdapterListener<Insulation> {

    private LayoutInflater inflater;
    
    public InsulationAdapter(Context context, int textViewResourceId, List<Insulation> objects)
    {
	super(context, textViewResourceId, objects);
	inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public static class InsulationHolder
    {
	ImageView serviceImage;

	TextView insulationName;

	TextView insulationCount;

	ImageView exclamationImage;
    }
    

    @Override
    public View createAndFillView(Insulation t, int position)
    {
	InsulationHolder holder = null;

	View view = inflater.inflate(R.layout.equipment_list_item, null, false);
	holder = new InsulationHolder();
	holder.insulationName = (TextView) view.findViewById(R.id.equipmentData);
	holder.insulationCount = (TextView) view.findViewById(R.id.equipmentCount);
	holder.exclamationImage = (ImageView) view.findViewById(R.id.exclamation_icon);
	view.setTag(holder);
	fillViewData(view, t, position);
	return view;
    }

    @Override
    public View fillViewData(View view, Insulation t, int position)
    {
		final InsulationHolder holder = (InsulationHolder) view.getTag();
		if (t != null && holder != null)
		{
		    holder.insulationName.setText(t.getName());
		    List<InsulationItem> items = t.getItems();
		    int count = 0;

		    if (items != null)
		    {
			ListIterator<InsulationItem> iterator = items.listIterator();

			while (iterator.hasNext())
			{
			    FormResponseBean.InsulationItem item = (FormResponseBean.InsulationItem) iterator.next();
			    if (item.isItemSaved())
			    {
				++count;
			    }
			}
		    }

		    holder.insulationCount.setText("" + count);
		    holder.insulationCount.setVisibility(View.VISIBLE);
		    if (!t.isMandatoryFieldsComplete())
		    {
			holder.exclamationImage.setVisibility(View.VISIBLE);
		    }
		    else
		    {
			holder.exclamationImage.setVisibility(View.GONE);
		    }

		}
		return view;
    }

    @Override
    public int getItemViewType(Insulation t, int position)
    {
	// TODO Auto-generated method stub
	return 0;
    }

    @Override
    public void updateViewData(CommonAdapter<Insulation> adapter)
    {
	// TODO Auto-generated method stub
	
    }
}
