package com.icf.rebate.adapter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import android.content.Context;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.icf.rebate.networklayer.model.PendingJobItem;
import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.ui.util.UiUtil;

public class PendingJobAdapter extends CommonAdapter<PendingJobItem> implements AdapterListener<PendingJobItem>
{
    private LayoutInflater inflater;

    private OnClickListener listener;
    
    private Context mContext;

    public PendingJobAdapter(Context context, int textViewResourceId, List<PendingJobItem> objects)
    {
	super(context, textViewResourceId, objects);
	mContext = context;
	inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public static class PendingJobHolder
    {
	TextView customerName;

	TextView date;

	TextView customerAddress;

	Button btnSubmit;

	ImageView imViewdone;

	View submissionRoot;

	ProgressBar progressBar;

	TextView txtPercentage;

	TextView errorNotify;

	ImageView trashIcon;

	ImageView arrowIcon;
    }

    @Override
    public View createAndFillView(PendingJobItem t, int position)
    {
	PendingJobHolder holder = null;

	View view = inflater.inflate(R.layout.pendingjob_listitem, null, false);
	holder = new PendingJobHolder();
	holder.errorNotify = (TextView) view.findViewById(R.id.errorSubmitNotify);
	holder.customerName = (TextView) view.findViewById(R.id.customer_name);
	holder.customerAddress = (TextView) view.findViewById(R.id.customer_address);
	holder.date = (TextView) view.findViewById(R.id.update_date);
	holder.btnSubmit = (Button) view.findViewById(R.id.submitBtn);
	holder.imViewdone = (ImageView) view.findViewById(R.id.done);
	holder.submissionRoot = view.findViewById(R.id.submissionProgressRoot);
	holder.progressBar = (ProgressBar) view.findViewById(R.id.progress);
	holder.trashIcon = (ImageView) view.findViewById(R.id.trash_icon);
	holder.arrowIcon = (ImageView) view.findViewById(R.id.arrow);
	view.setTag(holder);
	fillViewData(view, t, position);
	return view;
    }

    @Override
    public View fillViewData(View view, PendingJobItem t, int position)
    {
	final PendingJobHolder holder = (PendingJobHolder) view.getTag();
	if (t != null && holder != null)
	{
	    // Special case if job has state of submitted
	    if (!t.isSubmitted()) {
		holder.customerName.setText(t.getCustomerName());
		holder.customerAddress.setText(t.getCustomerAddress());
		holder.date.setText(t.getDate());
	    } 
	    else
	    {
		holder.customerName.setText(getContext().getString(R.string.job_submitted));
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(t.getLastModified());
		holder.customerAddress.setText(t.getCustomerAddress());
		holder.date.setText(t.getDate());
	    }

	    holder.btnSubmit.setTag(t);
	    holder.trashIcon.setTag(t);
	    holder.arrowIcon.setVisibility(View.VISIBLE);
	    holder.btnSubmit.setVisibility(View.INVISIBLE);
	    holder.imViewdone.setVisibility(View.INVISIBLE);
	    holder.submissionRoot.setVisibility(View.INVISIBLE);
	    holder.errorNotify.setVisibility(View.INVISIBLE);
	    holder.trashIcon.setVisibility(View.INVISIBLE);
	    if (t.isInComplete())
	    {
		holder.btnSubmit.setVisibility(View.INVISIBLE);
		holder.imViewdone.setVisibility(View.GONE);
		holder.trashIcon.setVisibility(View.VISIBLE);
	    }
	    else if (t.isJobCompleted())

	    // (!t.isSubmissionInProgress() || (t.isSubmissionInProgress() && progress == -1)))
	    {
		holder.btnSubmit.setVisibility(View.VISIBLE);
		holder.imViewdone.setVisibility(View.GONE);
	    }
	    else if (t.isSubmitted())
	    {
		holder.arrowIcon.setVisibility(View.GONE);
		holder.btnSubmit.setVisibility(View.GONE);
		holder.trashIcon.setVisibility(View.VISIBLE);
		holder.imViewdone.setVisibility(View.VISIBLE);
	    }
	    if (t.isSubmissionError())
	    {
		holder.errorNotify.setVisibility(View.VISIBLE);
	    }
	    if (t.isSubmissionInProgress())
	    {
		holder.btnSubmit.setVisibility(View.INVISIBLE);
		holder.imViewdone.setVisibility(View.GONE);
		holder.submissionRoot.setVisibility(View.VISIBLE);
	    }

	    holder.btnSubmit.setOnClickListener(listener);
	    holder.trashIcon.setOnClickListener(listener);
	}
	return view;
    }

    @Override
    public int getItemViewType(PendingJobItem t, int position)
    {
	return 0;
    }

    @Override
    public void updateViewData(CommonAdapter<PendingJobItem> adapter)
    {

    }

    public void setOnClickListener(OnClickListener adapterOnClickListener)
    {
	listener = adapterOnClickListener;
    }

}
