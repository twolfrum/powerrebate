package com.icf.rebate.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.ViewGroup;

import com.icf.rebate.networklayer.model.FormResponseBean.Part;
import com.icf.rebate.networklayer.model.FormResponseBean.TuneUp;
import com.icf.rebate.networklayer.model.FormResponseBean.TuneUpItem;
import com.icf.rebate.ui.customviews.CustomViewPager;
import com.icf.rebate.ui.fragment.TuneupFragment;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.UiUtil;

public class TuneUpAdapter extends FragmentPagerAdapter implements ViewPager.OnPageChangeListener
{
    TuneUp tuneUp;
    List<TuneUpItem> tuneUpList;
    int index;
    CustomViewPager viewPager;
    HashMap<TuneUpItem, TuneupFragment> fragMap;

    ArrayList<String> titleList = new ArrayList<String>();

    public TuneUpAdapter(FragmentManager fm, CustomViewPager viewPager)
    {
	super(fm);

	this.viewPager = viewPager;
	fragMap = new HashMap<TuneUpItem, TuneupFragment>();
	viewPager.setOnPageChangeListener(this);
	notifyDataSetChanged();

    }

    @Override
    public Parcelable saveState()
    {
	return null;
    }

    public void setTuneUpList(List<TuneUpItem> tuneUpList)
    {
	this.tuneUpList = tuneUpList;
    }
    
    public void setTuneUp(TuneUp tuneUp) {
	this.tuneUp = tuneUp;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position)
    {
	if (titleList.size() == position)
	{
	    String title = "" + System.nanoTime();
	    titleList.add(title);
	}
	return super.instantiateItem(container, position);
    }

    @Override
    public long getItemId(int argPosition)
    {
	String te = titleList.get(argPosition);
	long id = Long.parseLong(te);
	return id;
    }

    @Override
    public Fragment getItem(int index)
    {
	Log.v(TuneupFragment.TAG, "adapter getItem " + index);
	TuneupFragment frag = null;
	if (this.tuneUpList != null && this.tuneUpList.size() > index)
	{
	    TuneUpItem item = tuneUpList.get(index);

	    if (fragMap != null && fragMap.size() > index && fragMap.containsKey(item))
	    {
		Log.v(TuneupFragment.TAG, "adapter  getItem contains " + index);
		return fragMap.get(item);
	    }

	    frag = TuneupFragment.newInstance(item, tuneUp, index);
	    String title = null;
	    if (index >= titleList.size())
	    {
		title = "" + System.nanoTime();
		titleList.add(title);
	    }
	    else
	    {
		title = titleList.get(index);
	    }
	    frag.setTitleId(title);
	    fragMap.put(item, frag);
	    Log.v(TuneupFragment.TAG, "adapter getItem createNew " + index + " " + frag);
	}
	return frag;
    }

    public TuneUpItem deleteItem(int index)
    {
	TuneUpItem item = null;

	Log.v(TuneupFragment.TAG, "adapter deleteItem " + index);

	if (tuneUpList != null && tuneUpList.size() > index)
	{
	    TuneupFragment fragment = null;
	    item = tuneUpList.remove(index);
	    if (fragMap.containsKey(item))
	    {
		fragment = fragMap.get(item);
		Log.v(TuneupFragment.TAG, "adapter deleteItem contains " + index);
		fragMap.remove(item);

		destroyItem1(viewPager, index, fragment);
	    }
	}
	return item;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object)
    {

    }

    public void destroyItem1(ViewGroup container, int position, Object object)
    {
	if (position <= getCount())
	{
	    FragmentManager manager = ((Fragment) object).getFragmentManager();
	    Log.v(TuneupFragment.TAG, "destroyItem " + ((Fragment) object));
	    FragmentTransaction trans = manager.beginTransaction();
	    trans.remove((Fragment) object);
	    trans.commitAllowingStateLoss();

	    titleList.remove(position);
	}

    }

    @Override
    public int getCount()
    {
	int count = 0;
	if (tuneUpList != null)
	{
	    count = tuneUpList.size();
	}
	return count;
    }

    @Override
    public int getItemPosition(Object object)
    {
	TuneupFragment frag = (TuneupFragment) object;
	int idx = titleList.indexOf(frag.getTitleId());
	if (idx >= 0)
	{
	    return idx;
	}
	// int pos = tuneUpList.indexOf(frag.getTuneUpItemBean());
	//
	// if (pos < 0)
	// {
	// pos = POSITION_NONE;
	// }

	// Log.v(TuneupFragment.TAG, "adapter getItemPosition  " + pos);

	return POSITION_NONE;
    }

    public boolean setItemSaved(int index, boolean manualSave)
    {
	boolean retVal = false;

	if (tuneUpList != null && tuneUpList.size() > index)
	{
	    TuneUpItem item = tuneUpList.get(index);

	    if (item != null)
	    {
		List<Part> parts = null;
		if (item.isPackageSelected() && item.getPackages() != null)
		{
		    parts = item.getPackages().getParts();
		}
		else if (item.getSplit() != null)
		{
		    parts = item.getSplit().getParts();
		}

		int formState = UiUtil.validateForm(parts);
		item.setMandatoryFiledComplete(formState);
		retVal = formState != AppConstants.FORM_ITEM_FILLED_STATE.ITEM_UNEDITED.getState();

		if (!item.isItemSaved())
		{
		    item.setItemSaved(manualSave ? true : retVal);
		}

		return item.isItemSaved();
	    }
	}

	return retVal;
    }

    @Override
    public void onPageScrollStateChanged(int arg0)
    {
	if (viewPager != null)
	{
	    OnPageChangeListener listener = viewPager.getPageChangeListener();
	    if (listener != null)
	    {
		listener.onPageScrollStateChanged(arg0);
	    }
	}

    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2)
    {
	if (viewPager != null)
	{
	    OnPageChangeListener listener = viewPager.getPageChangeListener();
	    if (listener != null)
	    {
		listener.onPageScrolled(arg0, arg1, arg2);
	    }
	}

    }

    @Override
    public void onPageSelected(int arg0)
    {
	viewPager.onPageChanged();
	if (viewPager != null)
	{
	    OnPageChangeListener listener = viewPager.getPageChangeListener();
	    if (listener != null)
	    {
		listener.onPageSelected(arg0);
	    }
	}

    }

    public TuneupFragment getFragment(TuneUpItem item)
    {
	return fragMap.get(item);
    }

}
