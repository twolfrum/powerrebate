package com.icf.rebate.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.ViewGroup;

import com.icf.rebate.networklayer.model.FormResponseBean;
import com.icf.rebate.networklayer.model.FormResponseBean.Equipment;
import com.icf.rebate.networklayer.model.FormResponseBean.Item;
import com.icf.rebate.networklayer.model.FormResponseBean.Part;
import com.icf.rebate.ui.FormFragment;
import com.icf.rebate.ui.customviews.CustomViewPager;
import com.icf.rebate.ui.fragment.EquipmentPageFragment;
import com.icf.rebate.ui.fragment.ThermoStatInstallFragment;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.UiUtil;

public class EquipementPageAdapter extends FragmentPagerAdapter implements ViewPager.OnPageChangeListener
{
    Equipment equipment;
    int index;
    CustomViewPager viewPager;
    HashMap<Item, EquipmentPageFragment> fragMap;

    ArrayList<String> titleList = new ArrayList<String>();

    public EquipementPageAdapter(FragmentManager fm, CustomViewPager viewPager)
    {
	super(fm);
	this.viewPager = viewPager;
	viewPager.setOnPageChangeListener(this);
	fragMap = new HashMap<Item, EquipmentPageFragment>();
	notifyDataSetChanged();
    }

    @Override
    public Parcelable saveState()
    {
	// Log.v(FormFragment.TAG, "fragMap size " + fragMap.size() + " " + fragMap.get(equipment.getItems().get(0)));
	return null;
    }

    public void destroyItem1(ViewGroup container, int position, Object object)
    {
	if (position <= getCount())
	{
	    FragmentManager manager = ((Fragment) object).getFragmentManager();
	    Log.v(FormFragment.TAG, "destroyItem " + ((Fragment) object));
	    FragmentTransaction trans = manager.beginTransaction();
	    trans.remove((Fragment) object);
	    trans.commitAllowingStateLoss();

	    titleList.remove(position);
	}
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object)
    {
	// super.destroyItem(container, position, object);

    }

    public void setEquipment(Equipment equipment)
    {
	this.equipment = equipment;

    }

    @Override
    public Object instantiateItem(ViewGroup container, int position)
    {
	if (titleList.size() == position)
	{
	    String title = "" + System.nanoTime();
	    titleList.add(title);
	}
	return super.instantiateItem(container, position);
    }

    @Override
    public long getItemId(int argPosition)
    {
	String te = titleList.get(argPosition);
	long id = Long.parseLong(te);
	return id;
    }

    @Override
    public Fragment getItem(int index)
    {
	Log.v(FormFragment.TAG, "adapter getItem " + index);
	EquipmentPageFragment frag = null;
	if (equipment.getItems() != null && equipment.getItems().size() > index)
	{
	    Item item = equipment.getItems().get(index);

	    if (fragMap != null && fragMap.size() > index && fragMap.containsKey(item))
	    {
		Log.v(FormFragment.TAG, "adapter  getItem contains " + index);
		return fragMap.get(item);
	    }

	    frag = EquipmentPageFragment.newInstance(item);
	    String title = null;
	    if (index >= titleList.size())
	    {
		title = "" + System.nanoTime();
		titleList.add(title);
	    }
	    else
	    {
		title = titleList.get(index);
	    }
	    frag.setTitleId(title);
	    frag.setEquipment(equipment);
	    fragMap.put(item, frag);
	    this.index = index;
	    Log.v(ThermoStatInstallFragment.TAG, "adapter getItem createNew " + index + " " + frag);
	}
	return frag;
    }

    @Override
    public int getCount()
    {
	int count = 0;
	if (equipment != null && equipment.getItems() != null)
	{
	    count = equipment.getItems().size();
	}
	return count;
    }

    @Override
    public int getItemPosition(Object object)
    {
	EquipmentPageFragment frag = (EquipmentPageFragment) object;

	int idx = titleList.indexOf(frag.getTitleId());
	if (idx >= 0)
	{
	    return idx;
	}
	// int pos = equipment.getItems().indexOf(frag.getItem());
	//
	// if (pos < 0)
	// {
	// pos = POSITION_NONE;
	// }

	// Log.v(FormFragment.TAG, "adapter getItemPosition  " + pos);

	return POSITION_NONE;
    }

    public Item deleteItem(int index)
    {
	Item item = null;

	Log.v(FormFragment.TAG, "adapter deleteItem " + index);

	if (equipment.getItems() != null && equipment.getItems().size() > index)
	{
	    EquipmentPageFragment fragment = null;
	    item = equipment.getItems().remove(index);
	    if (fragMap.containsKey(item))
	    {
		fragment = fragMap.get(item);
		Log.v(FormFragment.TAG, "adapter deleteItem contains " + index);
		fragMap.remove(item);

		destroyItem1(viewPager, index, fragment);
	    }
	}

	return item;
    }

    public boolean setItemSaved(int index, boolean manualSave)
    {
	boolean retVal = false;

	if (equipment.getItems() != null && equipment.getItems().size() > index)
	{
	    Item item = equipment.getItems().get(index);

	    if (item != null)
	    {
		List<Part> parts = new ArrayList<FormResponseBean.Part>();
		if (item.isReplaceOnFailSelected() && item.getReplaceOnFail() != null)
		{
		    parts = item.getReplaceOnFail().getParts();
		}
		else if (item.getEarlyRetirement() != null)
		{
		    if (item.getReplaceOnFail() != null)
		    {
			parts.addAll(item.getReplaceOnFail().getParts());
			parts.remove(new Part(AppConstants.EQUIPMENTS_RESULT_ID));
		    }
		    parts.addAll(item.getEarlyRetirement().getParts());
		}

		int formState = UiUtil.validateForm(parts);
		item.setMandatoryFiledComplete(formState);

		retVal = formState != AppConstants.FORM_ITEM_FILLED_STATE.ITEM_UNEDITED.getState();

		if (!item.isItemSaved())
		{
		    item.setItemSaved(manualSave ? true : retVal);
		}

		return item.isItemSaved();
	    }
	}

	return retVal;
    }

    @Override
    public void onPageScrollStateChanged(int arg0)
    {
	if (viewPager != null)
	{
	    OnPageChangeListener listener = viewPager.getPageChangeListener();
	    if (listener != null)
	    {
		listener.onPageScrollStateChanged(arg0);
	    }
	}

    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2)
    {
	if (viewPager != null)
	{
	    OnPageChangeListener listener = viewPager.getPageChangeListener();
	    if (listener != null)
	    {
		listener.onPageScrolled(arg0, arg1, arg2);
	    }
	}

    }

    @Override
    public void onPageSelected(int arg0)
    {
	viewPager.onPageChanged();
	if (viewPager != null)
	{
	    OnPageChangeListener listener = viewPager.getPageChangeListener();
	    if (listener != null)
	    {
		listener.onPageSelected(arg0);
	    }
	}
    }

    public EquipmentPageFragment getCurrentFragment(Item item)
    {
	return fragMap.get(item);
    }

}
