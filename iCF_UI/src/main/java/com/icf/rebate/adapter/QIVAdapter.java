package com.icf.rebate.adapter;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.icf.rebate.networklayer.model.QIVItem;
import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.util.AppConstants;

public class QIVAdapter extends CommonAdapter<QIVItem> implements AdapterListener<QIVItem>
{
    private LayoutInflater inflater;

    public QIVAdapter(Context context, int textViewResourceId, List<QIVItem> objects)
    {
	super(context, textViewResourceId, objects);
	inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public static class EquipmentHolder
    {
	TextView equipmentName;
	ImageView exclamationImage;
    }

    @Override
    public View createAndFillView(QIVItem t, int position)
    {
	EquipmentHolder holder = null;

	View view = inflater.inflate(R.layout.equipment_list_item, null, false);
	holder = new EquipmentHolder();
	holder.equipmentName = (TextView) view.findViewById(R.id.equipmentData);
	holder.exclamationImage = (ImageView) view.findViewById(R.id.exclamation_icon);
	view.setTag(holder);
	fillViewData(view, t, position);
	return view;
    }

    private void setColor(View view, int position)
    {
	View root = view.findViewById(R.id.equipmentListRoot);
	Drawable d = getContext().getResources().getDrawable(R.drawable.equipment_lst_bg);

	int bluecolor = getContext().getResources().getColor(R.color.start_color);
	int endcolor = getContext().getResources().getColor(R.color.end_color);
	int diffBlue = Color.blue(endcolor) - Color.blue(bluecolor);
	int diffRed = Color.red(endcolor) - Color.red(bluecolor);
	int diffGreen = Color.green(endcolor) - Color.green(bluecolor);

	float mydiffBlue = (float) diffBlue / (float) getCount();
	float mydiffRed = (float) diffRed / (float) getCount();
	float mydiffGreen = (float) diffGreen / (float) getCount();
	mydiffBlue *= position;
	mydiffRed *= position;
	mydiffGreen *= position;
	mydiffBlue += Color.blue(bluecolor);
	mydiffRed += Color.red(bluecolor);
	mydiffGreen += Color.green(bluecolor);

	int color = Color.argb(Color.alpha(bluecolor), (int) mydiffRed, (int) mydiffGreen, (int) mydiffBlue);
	((GradientDrawable) d).setColor(color);
	root.setBackground(d);

    }

    @Override
    public View fillViewData(View view, QIVItem t, int position)
    {
	// setColor(view,position);
	final EquipmentHolder holder = (EquipmentHolder) view.getTag();
	if (t != null && holder != null)
	{
	    holder.equipmentName.setText(t.getName());
	    holder.exclamationImage.setVisibility(View.GONE);
	    if (t.isItemSaved())
	    {
		if (t.isQivRequired() && (t.getFormFilledState() == AppConstants.FORM_ITEM_FILLED_STATE.ITEM_PARTIALLY_SAVED.getState() || t.getFormFilledState() == AppConstants.FORM_ITEM_FILLED_STATE.ITEM_UNEDITED.getState()))
		{
		    holder.exclamationImage.setVisibility(View.VISIBLE);
		}
		else
		{
		    holder.exclamationImage.setVisibility(View.GONE);
		}
	    }
	    else
	    {
		holder.exclamationImage.setVisibility(View.VISIBLE);
	    }

	}

	return view;
    }

    public void updateData(List<QIVItem> objects)
    {

    }

    @Override
    public int getItemViewType(QIVItem t, int position)
    {
	return 0;
    }

    @Override
    public void updateViewData(CommonAdapter<QIVItem> adapter)
    {

    }

}
