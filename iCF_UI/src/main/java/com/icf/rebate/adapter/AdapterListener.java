package com.icf.rebate.adapter;

import android.view.View;

public interface AdapterListener<T>
{
    public View createAndFillView(T t, int position);

    public View fillViewData(View view, T t, int position);

    public int getItemViewType(T t, int position);

    public void updateViewData(CommonAdapter<T> adapter);
}
