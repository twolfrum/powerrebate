package com.icf.rebate.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.ViewGroup;

import com.icf.rebate.networklayer.model.ThermostatInstallItem;
import com.icf.rebate.ui.customviews.CustomViewPager;
import com.icf.rebate.ui.fragment.ThermoStatInstallFragment;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.UiUtil;

public class ThermoStatInstallAdapter extends FragmentPagerAdapter implements ViewPager.OnPageChangeListener
{
    List<ThermostatInstallItem> statList;
    CustomViewPager viewPager;
    HashMap<ThermostatInstallItem, ThermoStatInstallFragment> fragMap;

    ArrayList<String> titleList = new ArrayList<String>();

    public ThermoStatInstallAdapter(FragmentManager fm, CustomViewPager viewPager)
    {
	super(fm);

	this.viewPager = viewPager;
	fragMap = new HashMap<ThermostatInstallItem, ThermoStatInstallFragment>();
	viewPager.setOnPageChangeListener(this);
	notifyDataSetChanged();

    }

    @Override
    public Parcelable saveState()
    {
	// Log.v(ThermoStatInstallFragment.TAG, "fragMap size " + fragMap.size() + " " + fragMap.get(statList.get(0)));
	return null;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object)
    {

    }

    public void destroyItem1(ViewGroup container, int position, Object object)
    {
	// super.destroyItem(container, position, object);
	if (position <= getCount())
	{
	    FragmentManager manager = ((Fragment) object).getFragmentManager();
	    Log.v(ThermoStatInstallFragment.TAG, "destroyItem " + ((Fragment) object));
	    FragmentTransaction trans = manager.beginTransaction();
	    trans.remove((Fragment) object);
	    trans.commitAllowingStateLoss();

	    titleList.remove(position);
	}

    }

    public void setInstallList(List<ThermostatInstallItem> statList)
    {
	this.statList = statList;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position)
    {
	if (titleList.size() == position)
	{
	    String title = "" + System.nanoTime();
	    titleList.add(title);
	}
	return super.instantiateItem(container, position);
    }

    @Override
    public long getItemId(int argPosition)
    {
	String te = titleList.get(argPosition);
	long id = Long.parseLong(te);
	return id;
    }

    @Override
    public Fragment getItem(int index)
    {
	Log.v(ThermoStatInstallFragment.TAG, "adapter getItem " + index);
	ThermoStatInstallFragment thermoStatInstallFragment = null;
	if (this.statList != null && this.statList.size() > index)
	{
	    ThermostatInstallItem item = statList.get(index);

	    if (fragMap != null && fragMap.size() > index && fragMap.containsKey(item))
	    {
		Log.v(ThermoStatInstallFragment.TAG, "adapter  getItem contains " + index);
		return fragMap.get(item);
	    }

	    thermoStatInstallFragment = ThermoStatInstallFragment.newInstance(item);
	    String title = null;
	    if (index >= titleList.size())
	    {
		title = "" + System.nanoTime();
		titleList.add(title);
	    }
	    else
	    {
		title = titleList.get(index);
	    }
	    thermoStatInstallFragment.setTitleId(title);
	    fragMap.put(item, thermoStatInstallFragment);
	    Log.v(ThermoStatInstallFragment.TAG, "adapter getItem createNew " + index + " " + thermoStatInstallFragment);
	}
	return thermoStatInstallFragment;
    }

    public ThermostatInstallItem deleteItem(int index)
    {
	ThermostatInstallItem item = null;

	Log.v(ThermoStatInstallFragment.TAG, "adapter deleteItem " + index);

	if (statList != null && statList.size() > index)
	{
	    ThermoStatInstallFragment fragment = null;
	    item = statList.remove(index);
	    if (fragMap.containsKey(item))
	    {
		fragment = fragMap.get(item);
		Log.v(ThermoStatInstallFragment.TAG, "adapter deleteItem contains " + index);
		fragMap.remove(item);

		destroyItem1(viewPager, index, fragment);
	    }
	}

	return item;
    }

    @Override
    public int getCount()
    {
	int count = 0;
	if (statList != null)
	{
	    count = statList.size();
	}
	return count;
    }

    @Override
    public int getItemPosition(Object object)
    {
	ThermoStatInstallFragment frag = (ThermoStatInstallFragment) object;

	int idx = titleList.indexOf(frag.getTitleId());
	if (idx >= 0)
	{
	    return idx;
	}
	// int pos = statList.indexOf(frag.getstatsItemBean());
	//
	// if (pos < 0)
	// {
	// pos = POSITION_NONE;
	//
	// }
	//
	// Log.v(ThermoStatInstallFragment.TAG, "adapter getItemPosition  " + pos);

	return POSITION_NONE;
    }

    public boolean setItemSaved(int index, boolean manualSave)
    {
	boolean retVal = false;

	if (statList != null && statList.size() > index)
	{
	    ThermostatInstallItem item = statList.get(index);

	    if (item != null)
	    {
		int formState = UiUtil.validateForm(item.getPartList());
		item.setMandatoryFiledComplete(formState);
		retVal = formState != AppConstants.FORM_ITEM_FILLED_STATE.ITEM_UNEDITED.getState();

		if (!item.isItemSaved())
		{
		    item.setItemSaved(manualSave ? true : retVal);
		}

		return item.isItemSaved();
	    }
	}

	return retVal;

    }

    @Override
    public void onPageScrollStateChanged(int arg0)
    {
	if (viewPager != null)
	{
	    OnPageChangeListener listener = viewPager.getPageChangeListener();
	    if (listener != null)
	    {
		listener.onPageScrollStateChanged(arg0);
	    }
	}

    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2)
    {
	if (viewPager != null)
	{
	    OnPageChangeListener listener = viewPager.getPageChangeListener();
	    if (listener != null)
	    {
		listener.onPageScrolled(arg0, arg1, arg2);
	    }
	}

    }

    @Override
    public void onPageSelected(int arg0)
    {
	viewPager.onPageChanged();
	if (viewPager != null)
	{
	    OnPageChangeListener listener = viewPager.getPageChangeListener();
	    if (listener != null)
	    {
		listener.onPageSelected(arg0);
	    }
	}

    }

    public ThermoStatInstallFragment getThermoStatFragment(ThermostatInstallItem item)
    {
	return fragMap.get(item);
    }

}
