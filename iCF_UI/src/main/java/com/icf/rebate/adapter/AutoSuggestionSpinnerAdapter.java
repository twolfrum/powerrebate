package com.icf.rebate.adapter;

import org.json.JSONArray;

import com.icf.ameren.rebate.ui.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class AutoSuggestionSpinnerAdapter extends CustomSpinnerAdapter
{
    private JSONArray values;

    public AutoSuggestionSpinnerAdapter(Context context, int textViewResourceId, JSONArray objects)
    {
	super(context, textViewResourceId);
	this.values = objects;
    }
    

    @Override
    public int getCount()
    {
	if (values != null)
	{
	    return values.length();
	}
	return 0;
    }

    @Override
    public String getItem(int position)
    {
	try
	{
	    if (values != null)
	    {
		return values.getString(position);
	    }
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	return null;
    }

    @Override
    public long getItemId(int position)
    {
	return position;
    }    
}
