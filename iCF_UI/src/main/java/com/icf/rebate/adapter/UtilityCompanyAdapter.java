package com.icf.rebate.adapter;

import java.io.InputStream;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.icf.rebate.networklayer.model.ConfigurationDetail.UtilityCompany;
import com.icf.rebate.networklayer.utils.LibUtils;
import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.ui.util.ImageLoader;
//import com.nostra13.universalimageloader.core.ImageLoader;
import com.icf.rebate.ui.util.UiUtil;

public class UtilityCompanyAdapter extends CommonAdapter<UtilityCompany> implements AdapterListener<UtilityCompany>
{
    private LayoutInflater inflater;

    // private ImageLoader imgLoader;

    private int mode = 0;
    private ImageLoader imageLoader;

    private Context context;

    public UtilityCompanyAdapter(Context context, int textViewResourceId, List<UtilityCompany> objects)
    {
	super(context, textViewResourceId, objects);
	inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	defaultImage = context.getResources().getDrawable(R.drawable.default_utility_company_logo);
	imageLoader = new ImageLoader(context);
	this.context = context;
    }

    @Override
    public void onDestroy()
    {
	imageLoader = null;
	inflater = null;
    }

    public static class RebateModeHolder
    {
	ImageView image;

	TextView name;

	CheckBox chkbox;
    }

    public void setMode(int mode)
    {
	this.mode = mode;
    }

    @Override
    public View createAndFillView(UtilityCompany t, int position)
    {
	RebateModeHolder holder = null;

	View view = inflater.inflate(mode == 0 ? R.layout.rebate_confirmation_list_item : R.layout.change_company_listitem, null, false);
	holder = new RebateModeHolder();
	holder.image = (ImageView) view.findViewById(R.id.list_image);
	holder.name = (TextView) view.findViewById(R.id.list_txt);
	holder.chkbox = (CheckBox) view.findViewById(R.id.list_check_box);
	view.setTag(holder);
	fillViewData(view, t, position);
	return view;
    }

    private OnCheckedChangeListener checkListener = new OnCheckedChangeListener()
    {

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
	{
	    Object obj = buttonView.getTag();
	    if (obj instanceof UtilityCompany)
	    {
		notifyDataSetChanged();
	    }
	}
    };

    private Drawable defaultImage;

    @Override
    public View fillViewData(View view, UtilityCompany t, int position)
    {
	final RebateModeHolder holder = (RebateModeHolder) view.getTag();
	if (t != null && holder != null)
	{
	    loadUtilityCompanyLogo(t.getIcon(), holder.image);
	    holder.name.setText(t.getName());
	    holder.chkbox.setTag(t);
	    holder.chkbox.setOnCheckedChangeListener(checkListener);
	    UtilityCompany company = LibUtils.getSelectedUtilityCompany();
	    if (company != null && t.getId() == company.getId())
	    {
		if (mode == 1)
		{
		    holder.name.setTextColor(context.getResources().getColor(R.color.white));
		}
		holder.chkbox.setChecked(true);
	    }
	    else
	    {
		if (mode == 1)
		{
		    holder.name.setTextColor(context.getResources().getColor(R.color.gray_setting));
		}
		holder.chkbox.setChecked(false);
	    }

	}
	return view;
    }

    // private Drawable loadUtilityCompanyLogo(String icon) {
    // if(TextUtils.isEmpty(icon)) {
    // return defaultImage;
    // } else {
    // try {
    // InputStream istr = getContext().getAssets().open(icon);
    // Drawable drawable = Drawable.createFromStream(istr, null);
    // istr.close();
    // return drawable;
    // } catch(Exception e) {
    //
    // }
    // }
    // return defaultImage;
    // }

    private void loadUtilityCompanyLogo(String imageName, ImageView imageView)
    {
	imageLoader.loadBitmap(imageName, imageView, defaultImage, this);
    }

    @Override
    public int getItemViewType(UtilityCompany t, int position)
    {
	return 0;
    }

    @Override
    public void updateViewData(CommonAdapter<UtilityCompany> adapter)
    {

    }

}
