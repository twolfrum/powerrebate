package com.icf.rebate.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.icf.rebate.app.model.DashboardItem;
import com.icf.ameren.rebate.ui.R;
//import com.nostra13.universalimageloader.core.ImageLoader;
import com.icf.rebate.ui.util.ImageLoader;

public class DashboardAdapter extends CommonAdapter<DashboardItem> implements AdapterListener<DashboardItem>
{
    private LayoutInflater inflater;

    private ImageLoader imageLoader;

    public DashboardAdapter(Context context, int textViewResourceId, List<DashboardItem> objects)
    {
	super(context, textViewResourceId, objects);
	inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	imageLoader = new ImageLoader(context);
//	imgLoader = ((ICFEnergyApplication) context.getApplicationContext()).getImageLoader();
    }

    public static class DashboardHolder
    {
	ImageView serviceImage;

	TextView serviceName;
    }

    @Override
    public View createAndFillView(DashboardItem t, int position)
    {
	DashboardHolder holder = null;

	View view = inflater.inflate(R.layout.dashboard_list_item, null, false);
	holder = new DashboardHolder();
	holder.serviceImage = (ImageView) view.findViewById(R.id.service_image);
	holder.serviceName = (TextView) view.findViewById(R.id.service_txt);
	view.setTag(holder);
	fillViewData(view, t, position);
	return view;
    }

    @Override
    public View fillViewData(View view, DashboardItem t, int position)
    {
	final DashboardHolder holder = (DashboardHolder) view.getTag();
	if (t != null && holder != null)
	{
	    String name = t.getItemName();
	    if(!name.equals(holder.serviceName.getText())) {
		imageLoader.loadBitmap(t.getItemIcon(), holder.serviceImage, -1);
		holder.serviceName.setText(t.getItemName());
	    }
	}
	return view;
    }

    @Override
    public int getItemViewType(DashboardItem t, int position)
    {
	return 0;
    }

    @Override
    public void updateViewData(CommonAdapter<DashboardItem> adapter)
    {

    }
    
    @Override
    public void onDestroy() {
	imageLoader = null;
	inflater = null;
    }

}
