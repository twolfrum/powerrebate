package com.icf.rebate.adapter;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.provider.Settings.Secure;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.icf.rebate.app.model.MenuOption;
import com.icf.rebate.networklayer.ConnectionManager;
import com.icf.rebate.networklayer.HttpManagerListener;
import com.icf.rebate.networklayer.ICFHttpManager;
import com.icf.rebate.networklayer.model.ConfigurationDetail;
import com.icf.rebate.networklayer.model.ConfigurationDetail.UtilityCompany;
import com.icf.rebate.networklayer.model.LoginResponseBean;
import com.icf.rebate.networklayer.utils.LibUtils;
import com.google.android.gms.wearable.DataApi.GetFdForAssetResult;
import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.ui.LoginActivity;
import com.icf.rebate.ui.RootActivity;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.fragment.DashboardFragment;
import com.icf.rebate.ui.util.FileUtil;
import com.icf.rebate.ui.util.UiUtil;

public class SettingsExpandableListAdapter extends BaseExpandableListAdapter
{
    private Context context;
    private ArrayList<MenuOption> settingList;
    private LayoutInflater inflater;
    private Object child;
    private ConfigurationDetail configDetail;
    private Activity activity;

    private MenuOption option;
    private int totalCount;

    public SettingsExpandableListAdapter(Activity activity, ArrayList<MenuOption> settingList, ExpandableListView listView, Context context)
    {
	this.activity = activity;
	this.settingList = settingList;
	this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	this.context = context;
	totalCount = settingList.size();
	option = settingList.get(1);
    }

    @Override
    public int getGroupCount()
    {
	return settingList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition)
    {
	if (child != null)
	    return 1;
	else
	    return 0;
    }

    @Override
    public Object getGroup(int groupPosition)
    {
	return settingList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition)
    {
	return child;
    }

    @Override
    public long getGroupId(int groupPosition)
    {
	return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition)
    {
	return childPosition;
    }

    @Override
    public boolean hasStableIds()
    {
	return true;
    }

    @SuppressWarnings("unused")
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent)
    {
	MenuOption menu = (MenuOption) getGroup(groupPosition);
	child = menu.getItem();
	if (convertView == null)
	{
	    if (groupPosition == 0)
	    {
		convertView = inflater.inflate(R.layout.settings_home_list_item, null);
	    }
	    else
	    {
		convertView = inflater.inflate(R.layout.settings_main_list_item, null);
	    }
	}

	TextView title = (TextView) convertView.findViewById(R.id.setting_list_title);
	ImageView expand_collapse_bg = (ImageView) convertView.findViewById(R.id.expand_collapse_bg);
	ImageView logo = (ImageView) convertView.findViewById(R.id.setting_logo);
	TextView companyName = null;
	TextView emailId = null;
	title.setText(menu.getTitleId());
	if (groupPosition == 0)
	{
	    companyName = (TextView) convertView.findViewById(R.id.companyName);
	    emailId = (TextView) convertView.findViewById(R.id.emailId);
	}
	// expand_collapse_bg.setBackgroundResource(R.drawable.collapse_btn);
	// TODO: shouldn't the if condition be the other way around?
	if ((context.getResources().getString(menu.getTitleId())).equals(context.getResources().getString(R.string.about)) ||
			(context.getResources().getString(menu.getTitleId())).equals(context.getResources().getString(R.string.reportIssue)) ||
			(context.getResources().getString(menu.getTitleId())).equals(context.getResources().getString(R.string.logout)) ||
			(context.getResources().getString(menu.getTitleId())).equals(context.getResources().getString(R.string.home)) ||
	    	(context.getResources().getString(menu.getTitleId())).equals(context.getResources().getString(R.string.faq)))
	{
	    if (expand_collapse_bg != null)
	    {
		expand_collapse_bg.setVisibility(View.GONE);
	    }
	}
	else
	{
	    if (expand_collapse_bg != null)
	    {
		expand_collapse_bg.setVisibility(View.VISIBLE);
	    }
	}
	if ((context.getResources().getString(menu.getTitleId())).equals(context.getResources().getString(R.string.home)))
	{
	    logo.setVisibility(View.VISIBLE);
	    // String firstName = pref.getString("firstname", null);
	    // String lastName = pref.getString("lastname", null);

	    String firstName = LibUtils.getLoggedInUserBean().getFirstName();
	    String lastName = LibUtils.getLoggedInUserBean().getLastName();

	    if (firstName != null && lastName != null)
	    {
		title.setText(firstName + " " + lastName);
	    }

	    UtilityCompany company = LibUtils.getSelectedUtilityCompany();
	    companyName.setText(LibUtils.getSelectedUtilityCompany().getName());
	    emailId.setText(LibUtils.getLoggedInUserBean().getEmailId());

	    Bitmap bitmap = null;
	    int newHeight = 0, newWidth = 0;
	    try
	    {
		InputStream istr = new FileInputStream(FileUtil.getResourceBundleFile(company.getIcon()));
		BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
		bitmap = BitmapFactory.decodeStream(istr, null, bitmapOptions);
		// Need to scale the bitmap
		DisplayMetrics dm = activity.getResources().getDisplayMetrics();
		newWidth = (int) (dm.density * 65);
		newHeight = newWidth * bitmapOptions.outHeight / bitmapOptions.outWidth;
		bitmap = UiUtil.scaleBitmap(bitmap, newWidth, newHeight);
		istr.close();
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	    if (bitmap != null)
	    {
		RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(newWidth, newHeight);
		logo.setLayoutParams(lp);
		logo.setImageBitmap(bitmap);
	    }
	    else
	    {
		logo.setImageResource(R.drawable.default_utility_company_logo);
	    }
	}
	else
	{
	    logo.setVisibility(View.GONE);
	}
	if ((context.getResources().getString(menu.getTitleId())).equals(context.getResources().getString(R.string.faq))) {

	}

	if (isExpanded)
	{
	    expand_collapse_bg.setImageResource(R.drawable.expand_btn);
	}
	else
	{
	    expand_collapse_bg.setImageResource(R.drawable.collapse_btn);
	}

	return convertView;

    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent)
    {
	MenuOption vo = (MenuOption) getGroup(groupPosition);
	if ((context.getResources().getString(vo.getTitleId())).equals(context.getResources().getString(R.string.my_company_txt)))
	{
	    convertView = inflater.inflate(R.layout.change_company, null);
	    ListView comList = (ListView) convertView.findViewById(R.id.companyList);

	    // if (controllerContext != null)
	    {
		configDetail = LibUtils.getConfigurationDetail();
		LayoutParams list = (LayoutParams) comList.getLayoutParams();
		int height = inflater.getContext().getResources().getDimensionPixelSize(R.dimen.setting_company_list_item_height_with_padding);
		list.height = (configDetail.getUtilityList().size() * (height + comList.getDividerHeight()));
		comList.setLayoutParams(list);
		UtilityCompanyAdapter adapter = new UtilityCompanyAdapter(context, 0, configDetail.getUtilityList());
		adapter.setMode(1);
		adapter.setAdapterListener(adapter);
		comList.setAdapter(adapter);
		comList.setOnItemClickListener(new OnItemClickListener()
		{
		    @Override
		    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
		    {
			ArrayList<UtilityCompany> uti = configDetail.getUtilityList();
			if (uti != null)
			{
			    UtilityCompany company = uti.get(position);
			    LibUtils.setSelectedUtilityCompany(company);
			}
			UtilityCompanyAdapter adapter = (UtilityCompanyAdapter) parent.getAdapter();
			adapter.notifyDataSetChanged();
			notifyDataSetChanged();
			Fragment f = ((RootActivity) activity).getSupportFragmentManager().findFragmentById(R.id.mainLayout);
			if (f instanceof DashboardFragment)
			{
			    ((DashboardFragment) f).initialize();
			}
			RebateManager.getInstance().clearData();
			((RootActivity) activity).clearPendingJobs();
			((RootActivity) activity).retreivePendingJobs();
			((RootActivity) activity).updateCompanyDetails();
			// KVB adding support for an app update message after utility company is switched
			// ((RootActivity) activity).checkForUpdate();
		    }
		});
	    }
	}
	else if ((context.getResources().getString(vo.getTitleId())).equals(context.getResources().getString(R.string.changePassword)))
	{
	    convertView = inflater.inflate(R.layout.change_password, null);
	    final EditText current_password = (EditText) convertView.findViewById(R.id.current_password_field);
	    final EditText new_password = (EditText) convertView.findViewById(R.id.new_password_field);
	    final EditText confirm_password = (EditText) convertView.findViewById(R.id.confirm_password_field);
	    current_password.requestFocus();
	    Button btn_change = (Button) convertView.findViewById(R.id.change_password_btn);
	    Button btn_clear = (Button) convertView.findViewById(R.id.clear_password_btn);
	    btn_change.setOnClickListener(new OnClickListener()
	    {
		@Override
		public void onClick(View v)
		{
		    String currentPassword = current_password.getText().toString().trim();
		    String newPassword = new_password.getText().toString().trim();
		    String confirmPassword = confirm_password.getText().toString().trim();
		    LoginResponseBean bean = LibUtils.getLoggedInUserBean();

		    if (currentPassword.length() <= 0)
		    {
			UiUtil.showError(activity, context.getResources().getString(R.string.error_title), context.getResources().getString(R.string.please_enter_current_password));
			return;
		    }
		    else if (newPassword.length() <= 0)
		    {
			UiUtil.showError(activity, context.getResources().getString(R.string.error_title), context.getResources().getString(R.string.please_enter_new_password));
			return;
		    }
		    else if (confirmPassword.length() <= 0)
		    {
			UiUtil.showError(activity, context.getResources().getString(R.string.error_title), context.getResources().getString(R.string.please_enter_confirm_password));
			return;
		    }
		    else if (bean != null)
		    {
			String username = bean.getUserName();

			if (newPassword.equals(confirmPassword))
			{
			    changePasswordRequest(username, currentPassword, newPassword);
			    ((RootActivity) activity).passwordChange = newPassword;
			}
			else
			{
			    UiUtil.showError(activity, context.getResources().getString(R.string.alert_title), context.getResources().getString(R.string.password_mismatch));
			    new_password.setText("");
			    confirm_password.setText("");
			    current_password.setText("");
			    current_password.requestFocus();
			}
		    }
		}

	    });
	    btn_clear.setOnClickListener(new OnClickListener()
	    {

		@Override
		public void onClick(View v)
		{
		    new_password.setText("");
		    confirm_password.setText("");
		    current_password.setText("");

		}
	    });

	}
	else if ((context.getResources().getString(vo.getTitleId())).equals(context.getResources().getString(R.string.changePin)))
	{
	    convertView = inflater.inflate(R.layout.change_pin, null);
	    final EditText current_pin = (EditText) convertView.findViewById(R.id.current_pin_field);
	    final EditText new_pin = (EditText) convertView.findViewById(R.id.new_pin_field);
	    final EditText confirm_pin = (EditText) convertView.findViewById(R.id.confirm_pin_field);
	    Button btn_change_pin = (Button) convertView.findViewById(R.id.change_pin_btn);
	    Button btn_clear_pin = (Button) convertView.findViewById(R.id.clear_pin_btn);
	    Button btnResetPin = (Button) convertView.findViewById(R.id.reset_pin);
	    current_pin.requestFocus();
	    btnResetPin.setOnClickListener(new OnClickListener()
	    {

		@Override
		public void onClick(View v)
		{
		    // Reset pin
		    LibUtils.setDataForUser(LibUtils.getLoggedInUserBean().getUserName(), LibUtils.KEY_USER_INFO_PIN, "");
		    // Logout
		    try
		    {
			if (activity != null)
			{
			    UiUtil.showProgressDialog(activity);
			    LoginResponseBean bean = LibUtils.getLoggedInUserBean();
			    String username = null;
			    String password = null;
			    String deviceId = null;
			    if (bean != null)
			    {
				username = bean.getUserName();
				password = bean.getPassword();
				deviceId = Secure.getString(activity.getContentResolver(), Secure.ANDROID_ID);
			    }

			    if (!ConnectionManager.isOnline())
			    {
				UiUtil.dismissSpinnerDialog();
				Intent intent = new Intent(context, LoginActivity.class);
				context.startActivity(intent);
				activity.finish();
			    }
			    
			    ICFHttpManager.getInstance().doLogout((HttpManagerListener) activity, username, password, deviceId);
			}
		    }
		    catch (Exception e)
		    {
			e.printStackTrace();
		    }
		}
	    });
	    btn_change_pin.setOnClickListener(new OnClickListener()
	    {

		@Override
		public void onClick(View v)
		{
		    String currentPin = current_pin.getText().toString().trim();
		    String newPin = new_pin.getText().toString().trim();
		    String confin = confirm_pin.getText().toString().trim();
		    LoginResponseBean bean = LibUtils.getLoggedInUserBean();
		    /*
		     * if (currentPin.length() > 0 && newPin.length() > 0 && confin.length() > 0 && bean != null) { if (new_pin.length() < 4 ||
		     * confirm_pin.length() < 4) { UiUtil.showError(activity, context.getResources().getString(R.string.invalid_pin_title),
		     * context.getResources().getString(R.string.pin_length_error)); return; } String username = bean.getUserName(); if (newPin.equals(confin))
		     * { LibUtils.setDataForUser(username, LibUtils.KEY_USER_INFO_PIN, newPin); UiUtil.showError(activity,
		     * context.getResources().getString(R.string.successful_title), context.getResources().getString(R.string.update_pin_successfully)); } else
		     * { UiUtil.showError(activity, context.getResources().getString(R.string.alert_title),
		     * context.getResources().getString(R.string.pin_mismatch)); } } else { UiUtil.showError(activity,
		     * context.getResources().getString(R.string.alert_title), context.getResources().getString(R.string.all_fields_mandatory)); }
		     */

		    if (currentPin.length() <= 0)
		    {
			UiUtil.showError(activity, context.getResources().getString(R.string.error_title), context.getResources().getString(R.string.please_enter_current_pin));
			return;
		    }
		    else if (newPin.length() <= 0)
		    {
			UiUtil.showError(activity, context.getResources().getString(R.string.error_title), context.getResources().getString(R.string.please_enter_new_pin));
			return;
		    }
		    else if (confin.length() <= 0)
		    {
			UiUtil.showError(activity, context.getResources().getString(R.string.error_title), context.getResources().getString(R.string.please_enter_confirm_pin));
			return;
		    }
		    else
		    {
			String savedPin = LibUtils.getDataForUser(LibUtils.getLoggedInUserBean().getUserName(), LibUtils.KEY_USER_INFO_PIN);
			if (current_pin.getText().toString().equalsIgnoreCase(savedPin))
			{
			    if (new_pin.length() < 4 || confirm_pin.length() < 4)
			    {
				UiUtil.showError(activity, context.getResources().getString(R.string.invalid_pin_title), context.getResources().getString(R.string.pin_length_error));
				return;
			    }
			    String username = bean.getUserName();
			    if (newPin.equals(confin))
			    {
				LibUtils.setDataForUser(username, LibUtils.KEY_USER_INFO_PIN, newPin);
				UiUtil.showError(activity, context.getResources().getString(R.string.successful_title), context.getResources().getString(R.string.update_pin_successfully));
			    }
			    else
			    {
				UiUtil.showError(activity, context.getResources().getString(R.string.alert_title), context.getResources().getString(R.string.pin_mismatch));
				new_pin.setText("");
				confirm_pin.setText("");
				current_pin.setText("");
				current_pin.requestFocus();
			    }

			}
			else
			{
			    UiUtil.showError(activity, context.getResources().getString(R.string.error_title), context.getResources().getString(R.string.wrong_pin_txt));
			    new_pin.setText("");
			    confirm_pin.setText("");
			    current_pin.setText("");
			    current_pin.requestFocus();
			    return;
			}

		    }

		}
	    });
	    btn_clear_pin.setOnClickListener(new OnClickListener()
	    {

		@Override
		public void onClick(View v)
		{
		    new_pin.setText("");
		    confirm_pin.setText("");
		    current_pin.setText("");

		}
	    });
	}
	return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition)
    {
	return true;
    }

    private void changePasswordRequest(String userName, String oldPassword, String newPassword)
    {
	if (!ConnectionManager.isOnline())
	{
	    UiUtil.showError(activity, activity.getString(R.string.error), activity.getString(R.string.no_network_connection));
	    return;
	}
	UiUtil.showProgressDialog(activity);
	ICFHttpManager.getInstance().changePassword(((RootActivity) activity), userName, oldPassword, newPassword);

    }

    public void update(boolean b)
    {
	if (!b)
	{
	    if (totalCount == settingList.size())
	    {
		option = settingList.get(1);
		settingList.remove(1);
		notifyDataSetChanged();
	    }
	}
	else
	{
	    if (totalCount != settingList.size())
	    {
		if (option != null)
		{
		    settingList.add(1, option);
		    notifyDataSetChanged();
		}
	    }
	}
    }

}
