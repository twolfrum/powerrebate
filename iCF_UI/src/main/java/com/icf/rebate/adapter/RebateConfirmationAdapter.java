package com.icf.rebate.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.icf.rebate.app.model.ConfirmationMode;
import com.icf.ameren.rebate.ui.R;

public class RebateConfirmationAdapter extends CommonAdapter<ConfirmationMode> implements AdapterListener<ConfirmationMode>
{
    private LayoutInflater inflater;
    private OnClickListener listener;

    public RebateConfirmationAdapter(Context context, int textViewResourceId, List<ConfirmationMode> objects)
    {
	super(context, textViewResourceId, objects);
	inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public static class RebateModeHolder
    {
	ImageView image;

	TextView name;

	CheckBox chkbox;

	ImageView trash;
    }

    @Override
    public View createAndFillView(ConfirmationMode t, int position)
    {
	RebateModeHolder holder = null;

	View view = inflater.inflate(R.layout.rebate_confirmation_list_item, null, false);
	holder = new RebateModeHolder();
	holder.image = (ImageView) view.findViewById(R.id.list_image);
	holder.name = (TextView) view.findViewById(R.id.list_txt);
	holder.chkbox = (CheckBox) view.findViewById(R.id.list_check_box);
	holder.trash = (ImageView) view.findViewById(R.id.trash_icon);
	view.setTag(holder);
	fillViewData(view, t, position);
	return view;
    }

    @Override
    public View fillViewData(View view, ConfirmationMode t, int position)
    {
	final RebateModeHolder holder = (RebateModeHolder) view.getTag();
	if (t != null && holder != null)
	{
	    holder.image.setImageResource(t.getImageResId());
	    holder.name.setText(t.getModeName());
	    holder.chkbox.setChecked(t.isSelected());
	    holder.trash.setTag(position);
	    holder.trash.setVisibility(t.isSelected() ? View.VISIBLE : View.GONE);
	    holder.trash.setOnClickListener(listener);
	    if (!checkConfirmationEnabled())
	    {
		holder.name.setEnabled(true);
	    }
	    else
	    {
		holder.name.setEnabled(t.isSelected());
	    }
	}
	return view;
    }

    private boolean checkConfirmationEnabled()
    {
	int count = getCount();
	boolean selected = false;
	for (int i = 0; i < count; i++)
	{
	    ConfirmationMode mode = getItem(i);
	    if (mode.isSelected())
	    {
		selected = true;
	    }
	}
	return selected;
    }

    @Override
    public int getItemViewType(ConfirmationMode t, int position)
    {
	return 0;
    }

    @Override
    public void updateViewData(CommonAdapter<ConfirmationMode> adapter)
    {

    }

    public void setOnClickListener(OnClickListener adapterOnClickListener)
    {
	listener = adapterOnClickListener;
    }

}
