package com.icf.rebate.adapter;

import java.util.List;

import org.json.JSONArray;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.icf.ameren.rebate.ui.R;

public class CustomSpinnerAdapter extends ArrayAdapter<String>
{

    private LayoutInflater inflater;

    public CustomSpinnerAdapter(Context context, int textViewResourceId, List<String> objects)
    {
	super(context, textViewResourceId, objects);
	this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public CustomSpinnerAdapter(Context context, int textViewResourceId)
    {
	super(context, textViewResourceId);
	this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    
    
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent)
    {
	return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
	return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent)
    {
	if (convertView == null)
	{
	    convertView = inflater.inflate(R.layout.list_item_text_view, parent, false);
	}
	if (position < getCount())
	{
	    String name = (String) getItem(position);

	    if (name != null)
	    {
		TextView txView = (TextView) convertView;
		if (txView != null)
		{
		    txView.setText(name);
		}
	    }
	}

	return convertView;
    }
}
