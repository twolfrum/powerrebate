package com.icf.rebate.adapter;

import java.util.ArrayList;
import java.util.Hashtable;

import com.icf.rebate.app.model.MenuItem;
import com.icf.rebate.ui.listeners.MenuItemListener;
import com.icf.ameren.rebate.ui.R;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

public class SlideMenuAdapter extends BaseExpandableListAdapter
{

    private Context context;

    private ArrayList<MenuItem> menuList;

    private Hashtable<Integer, Integer> mapViewType;

    private int groupTypeCount;

    private ExpandableListView eLV;

    private MenuItemListener menuItemListener;

    private int slideType;

    private int childTypeCount;

    public SlideMenuAdapter(Context context, ArrayList<MenuItem> groupList, int groupTypeCount, int childTypeCount)
    {
	super();
	this.context = context;
	this.menuList = groupList;
	this.groupTypeCount = groupTypeCount;
	this.childTypeCount = childTypeCount;
	mapViewType = new Hashtable<Integer, Integer>();
    }

    @Override
    public int getGroupType(int groupPosition)
    {
	MenuItem item = menuList.get(groupPosition);
	Integer val = mapViewType.get(item.getMenuLayoutId());
	if (val == null)
	{
	    val = mapViewType.size();
	    mapViewType.put(item.getMenuLayoutId(), val);
	    return val;
	}
	else
	{
	    return val;
	}
    }

    @Override
    public int getGroupTypeCount()
    {
	return groupTypeCount;
    }

    @Override
    public int getChildType(int groupPosition, int childPosition)
    {
	int position = 0;
	for (int i = 0; i < groupPosition; i++)
	{
	    MenuItem item = menuList.get(i);
	    if (item.getSubmenuList() != null)
	    {
		position += item.getSubmenuList().size();
	    }
	}
	int pos = position + childPosition;
	return pos;
    }

    @Override
    public int getChildTypeCount()
    {
	return childTypeCount;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition)
    {
	MenuItem item = menuList.get(groupPosition);
	if (item != null)
	{
	    ArrayList<MenuItem> submenuList = item.getSubmenuList();
	    if (submenuList != null)
	    {

		return submenuList.get(childPosition);
	    }
	}
	return null;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition)
    {
	return childPosition;
    }

    @Override
    public View getChildView(final int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent)
    {

	MenuItem menuItem = (MenuItem) getChild(groupPosition, childPosition);
	if (menuItem != null)
	{
	    if (convertView == null)
	    {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = inflater.inflate(menuItem.getMenuLayoutId(), null);
	    }
	    convertView.setBackgroundColor(R.color.sliding_right_menu_child_backgound);

	    if (menuItemListener != null)
	    {
		menuItemListener.onFinishInflate(menuItem.getMenuLayoutId(), convertView, menuItem);
	    }
	}
	return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition)
    {
	MenuItem item = menuList.get(groupPosition);

	if (item.getSubmenuList() == null || item.getSubmenuList().size() == 0)
	{
	    return 0;
	}
	return item.getSubmenuList().size();
    }

    @Override
    public Object getGroup(int groupPosition)
    {
	return menuList.get(groupPosition);
    }

    @Override
    public int getGroupCount()
    {
	return menuList.size();
    }

    @Override
    public long getGroupId(int groupPosition)
    {
	return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent)
    {
	MenuItem menuItem = (MenuItem) getGroup(groupPosition);
	eLV = (ExpandableListView) parent;

	if (convertView == null)
	{
	    LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    convertView = infalInflater.inflate(menuItem.getMenuLayoutId(), null);
	}
	if (menuItemListener != null)
	{
	    menuItemListener.onFinishInflate(menuItem.getMenuLayoutId(), convertView, menuItem);
	}
	if (menuItem.getSubmenuList() != null && menuItem.getSubmenuList().size() > 0)
	{

	    ImageView img = (ImageView) convertView.findViewById(R.id.expand_collapse);
	    if (img != null)
	    {
		img.setImageResource(isExpanded ? R.drawable.expand_btn : R.drawable.collapse_btn);
	    }
	}

	return convertView;
    }

    @Override
    public boolean hasStableIds()
    {
	return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition)
    {
	MenuItem menuItem = (MenuItem) getGroup(groupPosition);
	// if (menuItem.getMenuItemId() == context.getResources().getInteger(R.integer.menu_help_id))
	// {
	// return true;
	// }
	return false;
    }

    @Override
    public void onGroupExpanded(int groupPosition)
    {
	super.onGroupExpanded(groupPosition);
	collapseAllOtherGroups(groupPosition);
	// selectedGroupPosition = groupPosition;

    }

    public void setMenuItemListener(MenuItemListener listener)
    {
	this.menuItemListener = listener;
    }

    public void setType(int type)
    {
	this.slideType = type;
    }

    public int getType()
    {
	return this.slideType;
    }

    public ArrayList<MenuItem> getMenuList()
    {
	return menuList;
    }

    private void collapseAllOtherGroups(int groupPosition)
    {
	int len = getGroupCount();

	for (int i = 0; i < len; i++)
	{
	    if (i != groupPosition)
	    {
		if (eLV != null)
		{
		    eLV.collapseGroup(i);
		    eLV.removeAllViewsInLayout();
		}
	    }
	}
    }

    public void collapseAll()
    {
	int len = getGroupCount();
	for (int i = 0; i < len; i++)
	{
	    if (eLV != null)
	    {
		eLV.collapseGroup(i);
	    }
	}

    }

}
