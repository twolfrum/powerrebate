package com.icf.rebate.adapter;

import java.util.List;
import java.util.ListIterator;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.icf.rebate.networklayer.model.FormResponseBean;
import com.icf.rebate.networklayer.model.FormResponseBean.Equipment;
import com.icf.rebate.networklayer.model.FormResponseBean.Item;
import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.util.AppConstants;

public class EquipmentAdapter extends CommonAdapter<Equipment> implements AdapterListener<Equipment>
{
    private LayoutInflater inflater;

    public EquipmentAdapter(Context context, int textViewResourceId, List<Equipment> objects)
    {
	super(context, textViewResourceId, objects);
	inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public static class EquipmentHolder
    {
	ImageView serviceImage;

	TextView equipmentName;

	TextView equipmentCount;

	ImageView exclamationImage;
    }

    private void setColor(View view, int position)
    {
	View root = view.findViewById(R.id.equipmentListRoot);
	Drawable d = getContext().getResources().getDrawable(R.drawable.equipment_lst_bg);

	int bluecolor = getContext().getResources().getColor(R.color.start_color);
	int endcolor = getContext().getResources().getColor(R.color.end_color);
	int diffBlue = Color.blue(endcolor) - Color.blue(bluecolor);
	int diffRed = Color.red(endcolor) - Color.red(bluecolor);
	int diffGreen = Color.green(endcolor) - Color.green(bluecolor);

	float mydiffBlue = (float) diffBlue / (float) getCount();
	float mydiffRed = (float) diffRed / (float) getCount();
	float mydiffGreen = (float) diffGreen / (float) getCount();
	mydiffBlue *= position;
	mydiffRed *= position;
	mydiffGreen *= position;
	mydiffBlue += Color.blue(bluecolor);
	mydiffRed += Color.red(bluecolor);
	mydiffGreen += Color.green(bluecolor);

	int color = Color.argb(Color.alpha(bluecolor), (int) mydiffRed, (int) mydiffGreen, (int) mydiffBlue);
	((GradientDrawable) d).setColor(color);
	root.setBackground(d);

    }

    @Override
    public View createAndFillView(Equipment t, int position)
    {
	EquipmentHolder holder = null;

	View view = inflater.inflate(R.layout.equipment_list_item, null, false);
	holder = new EquipmentHolder();
	holder.equipmentName = (TextView) view.findViewById(R.id.equipmentData);
	holder.equipmentCount = (TextView) view.findViewById(R.id.equipmentCount);
	holder.exclamationImage = (ImageView) view.findViewById(R.id.exclamation_icon);
	view.setTag(holder);
	fillViewData(view, t, position);
	return view;
    }

    @Override
    public View fillViewData(View view, Equipment t, int position)
    {
	// setColor(view, position);
	final EquipmentHolder holder = (EquipmentHolder) view.getTag();
	if (t != null && holder != null)
	{
	    holder.equipmentName.setText(t.getName());
	    List<Item> items = t.getItems();
	    int count = 0;

	    if (items != null)
	    {
		ListIterator<Item> iterator = items.listIterator();

		while (iterator.hasNext())
		{
		    FormResponseBean.Item item = (FormResponseBean.Item) iterator.next();
		    if (item.isItemSaved())
		    {
			++count;
		    }
		}
	    }

	    holder.equipmentCount.setText("" + count);
	    holder.equipmentCount.setVisibility(View.VISIBLE);
	    if (!t.isMandatoryFieldsComplete())
	    {
		holder.exclamationImage.setVisibility(View.VISIBLE);
	    }
	    else
	    {
		holder.exclamationImage.setVisibility(View.GONE);
	    }

	}
	return view;
    }

    @Override
    public int getItemViewType(Equipment t, int position)
    {
	return 0;
    }

    @Override
    public void updateViewData(CommonAdapter<Equipment> adapter)
    {

    }

}
