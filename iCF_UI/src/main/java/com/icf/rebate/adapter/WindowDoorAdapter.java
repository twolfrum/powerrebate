/**
 * 
 */
package com.icf.rebate.adapter;

import java.util.List;
import java.util.ListIterator;

import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.networklayer.model.FormResponseBean;
import com.icf.rebate.networklayer.model.FormResponseBean.Insulation;
import com.icf.rebate.networklayer.model.FormResponseBean.Part;
import com.icf.rebate.networklayer.model.FormResponseBean.WDItem;
import com.icf.rebate.networklayer.model.FormResponseBean.WindowAndDoor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @author Ken Butler Dec 17, 2015
 * WindowDoorAdapter.java
 * Copyright (c) 2015 ICF International
 */
public class WindowDoorAdapter extends CommonAdapter<WindowAndDoor> implements AdapterListener<WindowAndDoor> {

    private LayoutInflater inflater;
    
    public WindowDoorAdapter(Context context, int textViewResourceId, List<WindowAndDoor> objects)
    {
	super(context, textViewResourceId, objects);
	inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public static class Holder
    {
	ImageView serviceImage;

	TextView name;

	TextView count;

	ImageView exclamationImage;
    }
    

    @Override
    public View createAndFillView(WindowAndDoor t, int position)
    {
	Holder holder = null;

	View view = inflater.inflate(R.layout.equipment_list_item, null, false);
	holder = new Holder();
	holder.name = (TextView) view.findViewById(R.id.equipmentData);
	holder.count = (TextView) view.findViewById(R.id.equipmentCount);
	holder.exclamationImage = (ImageView) view.findViewById(R.id.exclamation_icon);
	view.setTag(holder);
	fillViewData(view, t, position);
	return view;
    }

    @Override
    public View fillViewData(View view, WindowAndDoor t, int position)
    {
		final Holder holder = (Holder) view.getTag();
		if (t != null && holder != null)
		{
		    holder.name.setText(t.getName());
		    List<WDItem> items = t.getItems();
		    int count = 0;

		    if (items != null)
		    {
			ListIterator<WDItem> iterator = items.listIterator();

			while (iterator.hasNext())
			{
			    WDItem item = (WDItem) iterator.next();
			    if (item.isItemSaved())
			    {
				++count;
			    }
			}
		    }

		    holder.count.setText("" + count);
		    holder.count.setVisibility(View.VISIBLE);
		    if (!t.isMandatoryFieldsComplete())
		    {
			holder.exclamationImage.setVisibility(View.VISIBLE);
		    }
		    else
		    {
			holder.exclamationImage.setVisibility(View.GONE);
		    }

		}
		return view;
    }

    @Override
    public int getItemViewType(WindowAndDoor t, int position)
    {
	// TODO Auto-generated method stub
	return 0;
    }

    @Override
    public void updateViewData(CommonAdapter<WindowAndDoor> adapter)
    {
	// TODO Auto-generated method stub
	
    }
    
}
