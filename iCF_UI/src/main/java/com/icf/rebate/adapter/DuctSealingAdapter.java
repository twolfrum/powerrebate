package com.icf.rebate.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.icf.rebate.app.model.DuctSealing;
import com.icf.rebate.app.model.DuctSealingItem;
import com.icf.rebate.ui.controller.FormUIManager;
import com.icf.rebate.ui.customviews.CustomViewPager;
import com.icf.rebate.ui.fragment.DuctSealingFragment;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.UiUtil;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.ViewGroup;

public class DuctSealingAdapter extends FragmentPagerAdapter implements ViewPager.OnPageChangeListener
{
	public static final String TAG = DuctSealingAdapter.class.getSimpleName();

    DuctSealing ductSealing;
    List<DuctSealingItem> ductList;
    CustomViewPager viewPager;
    HashMap<DuctSealingItem, DuctSealingFragment> fragMap;
    ArrayList<String> titleList = new ArrayList<String>();


	public DuctSealingAdapter(FragmentManager fm, CustomViewPager viewPager, FormUIManager manager) {
		super(fm);

		this.viewPager = viewPager;
		fragMap = new HashMap<DuctSealingItem, DuctSealingFragment>();
		viewPager.setOnPageChangeListener(this);
		notifyDataSetChanged();
	}

	@Override
	public Parcelable saveState() {
		return null;
	}

	public void destroyItem1(ViewGroup container, int position, Object object) {
		if (position <= getCount()) {
			FragmentManager manager = ((Fragment) object).getFragmentManager();
			Log.v(DuctSealingFragment.TAG, "destroyItem " + ((Fragment) object));
			FragmentTransaction trans = manager.beginTransaction();
			trans.remove((Fragment) object);
			trans.commitAllowingStateLoss();

			titleList.remove(position);
		}
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		if (titleList.size() == position) {
			String title = "" + System.nanoTime();
			titleList.add(title);
		}
		return super.instantiateItem(container, position);
	}

	@Override
	public long getItemId(int argPosition) {
		String te = titleList.get(argPosition);
		long id = Long.parseLong(te);
		return id;
	}

    public void setDuctList(List<DuctSealingItem> ductList) {
        this.ductList = ductList;
    }

    public void setDuctSealing(DuctSealing ductSealing) {
        this.ductSealing = ductSealing;
    }

    @Override
    public Fragment getItem(int index)
    {
	Log.v(TAG, "adapter getItem " + index);
	DuctSealingFragment ductSealingFragment = null;
	if (this.ductList != null && this.ductList.size() > index)
	{
	    DuctSealingItem item = ductList.get(index);

	    if (fragMap != null && fragMap.size() > index && fragMap.containsKey(item))
	    {
		Log.v(TAG, "adapter  getItem contains " + index);
		return fragMap.get(item);
	    }

	    ductSealingFragment = DuctSealingFragment.newInstance(item,
                ductSealing.getRebateType(), ductSealing.getRebateCalcType());
	    String title = null;
	    if (index >= titleList.size())
	    {
		title = "" + System.nanoTime();
		titleList.add(title);
	    }
	    else
	    {
		title = titleList.get(index);
	    }
	    ductSealingFragment.setTitleId(title);

	    fragMap.put(item, ductSealingFragment);
	    Log.v(TAG, "adapter getItem createNew " + index + " " + ductSealingFragment);
	}
	return ductSealingFragment;
    }

    public DuctSealingItem deleteItem(int index)
    {
	DuctSealingItem item = null;

	Log.v(TAG, "adapter deleteItem " + index);

	if (ductList != null && ductList.size() > index)
	{
	    item = ductList.remove(index);
	    DuctSealingFragment fragment = null;
	    if (fragMap.containsKey(item))
	    {
		fragment = fragMap.get(item);
		Log.v(TAG, "adapter deleteItem contains " + index);
		fragMap.remove(item);

		destroyItem1(viewPager, index, fragment);
	    }
	}

	return item;
    }

    @Override
    public int getCount()
    {
	int count = 0;
	if (ductList != null)
	{
	    count = ductList.size();
	}
	return count;
    }

	@Override
	public int getItemPosition(Object object) {
		DuctSealingFragment frag = (DuctSealingFragment) object;

		int idx = titleList.indexOf(frag.getTitleId());
		if (idx >= 0) {
			return idx;
		}


		return POSITION_NONE;
	}

	public boolean setItemSaved(int index, boolean manualSave) {
		boolean retVal = false;

		if (ductList != null && ductList.size() > index) {
			DuctSealingItem item = ductList.get(index);

			if (item != null) {
				int formState = UiUtil.validateForm(item.getSealingDetail().getParts());
				item.setMandatoryFiledComplete(formState);
				retVal = formState != AppConstants.FORM_ITEM_FILLED_STATE.ITEM_UNEDITED.getState();

				if (!item.isItemSaved()) {
					item.setItemSaved(manualSave ? true : retVal);
				}

				return item.isItemSaved();
			}
		}

		return retVal;
	}

    @Override
    public void onPageScrollStateChanged(int arg0)
    {
	if (viewPager != null)
	{
	    OnPageChangeListener listener = viewPager.getPageChangeListener();
	    if (listener != null)
	    {
		listener.onPageScrollStateChanged(arg0);
	    }
	}

    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2)
    {
	if (viewPager != null)
	{
	    OnPageChangeListener listener = viewPager.getPageChangeListener();
	    if (listener != null)
	    {
		listener.onPageScrolled(arg0, arg1, arg2);
	    }
	}

    }

    @Override
    public void onPageSelected(int arg0)
    {
	viewPager.onPageChanged();
	if (viewPager != null)
	{
	    OnPageChangeListener listener = viewPager.getPageChangeListener();
	    if (listener != null)
	    {
		listener.onPageSelected(arg0);
	    }
	}

    }

    public DuctSealingFragment getDuctSealingFragment(DuctSealingItem item)
    {
	return fragMap.get(item);
    }

}
