package com.icf.rebate.adapter;

import java.util.List;
import java.util.ListIterator;

import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.networklayer.model.FormResponseBean.TuneUp;
import com.icf.rebate.networklayer.model.FormResponseBean.TuneUpItem;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class TuneUpTypeAdapter extends CommonAdapter<TuneUp> implements AdapterListener<TuneUp>
{
    private LayoutInflater mInflater;

	public TuneUpTypeAdapter(Context context, int textViewResourceId, List<TuneUp> objects) {
		super(context, textViewResourceId, objects);
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public class Holder {
		ImageView serviceImage;
		TextView name;
		TextView count;
		ImageView exclamationImage;
	}

    @Override
	public View createAndFillView(TuneUp t, int position) {
		Holder holder = null;

		View view = mInflater.inflate(R.layout.equipment_list_item, null, false);
		holder = new Holder();
		holder.name = (TextView) view.findViewById(R.id.equipmentData);
		holder.count = (TextView) view.findViewById(R.id.equipmentCount);
		holder.exclamationImage = (ImageView) view.findViewById(R.id.exclamation_icon);
		view.setTag(holder);
		fillViewData(view, t, position);
		return view;
	}

    @Override
    public View fillViewData(View view, TuneUp t, int position)
    {
	final Holder holder = (Holder) view.getTag();
	if (t != null && holder != null)
	{
	    holder.name.setText(t.getName());
	    List<TuneUpItem> items = t.getItems();
	    int count = 0;

	    if (items != null)
	    {
		ListIterator<TuneUpItem> iterator = items.listIterator();

		while (iterator.hasNext())
		{
		    TuneUpItem item = iterator.next();
		    if (item.isItemSaved())
		    {
			++count;
		    }
		}
	    }

	    holder.count.setText("" + count);
	    holder.count.setVisibility(View.VISIBLE);
	    if (!t.isMandatoryFieldsComplete())
	    {
		holder.exclamationImage.setVisibility(View.VISIBLE);
	    }
	    else
	    {
		holder.exclamationImage.setVisibility(View.GONE);
	    }

	}
	return view;
    }

    @Override
    public int getItemViewType(TuneUp t, int position)
    {
	return 0;
    }

    @Override
    public void updateViewData(CommonAdapter<TuneUp> adapter)
    {
	// Unimpl
    }

}
