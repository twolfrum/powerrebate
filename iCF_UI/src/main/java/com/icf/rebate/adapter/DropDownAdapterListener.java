package com.icf.rebate.adapter;

import android.view.View;

public interface DropDownAdapterListener<T>
{
    public View createDropDownAndFillView(T t, int position);

    public View fillDropDownViewData(View view, T t, int position);
}
