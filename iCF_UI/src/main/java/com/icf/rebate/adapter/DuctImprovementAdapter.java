package com.icf.rebate.adapter;

import java.util.List;

import com.icf.rebate.networklayer.model.DuctItem;
import com.icf.ameren.rebate.ui.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

public class DuctImprovementAdapter extends CommonAdapter<DuctItem> implements AdapterListener<DuctItem>
{

    private LayoutInflater inflater;

    public DuctImprovementAdapter(Context context, int textViewResourceId, List<DuctItem> objects)
    {
	super(context, textViewResourceId, objects);
	inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View createAndFillView(DuctItem t, int position)
    {
	View view = inflater.inflate(R.layout.duct_improvement_list_item, null, false);
	view = fillViewData(view, t, position);
	return view;
    }

    @Override
    public View fillViewData(View view, DuctItem t, int position)
    {
	TextView textview = (TextView) view.findViewById(R.id.improvementListText);
	textview.setText(t.getName());
	return view;
    }

    @Override
    public int getItemViewType(DuctItem t, int position)
    {
	return 0;
    }

    @Override
    public void updateViewData(CommonAdapter<DuctItem> adapter)
    {

    }

}
