/**
 * 
 */
package com.icf.rebate.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.icf.rebate.networklayer.model.FormResponseBean.Insulation;
import com.icf.rebate.networklayer.model.FormResponseBean.InsulationItem;
import com.icf.rebate.networklayer.model.FormResponseBean.Part;
import com.icf.rebate.networklayer.model.FormResponseBean.WDItem;
import com.icf.rebate.networklayer.model.FormResponseBean.WindowAndDoor;
import com.icf.rebate.ui.FormFragment;
import com.icf.rebate.ui.customviews.CustomViewPager;
import com.icf.rebate.ui.fragment.InsulationPageFragment;
import com.icf.rebate.ui.fragment.ThermoStatInstallFragment;
import com.icf.rebate.ui.fragment.WDPageFragment;
import com.icf.rebate.ui.util.AppConstants;
import com.icf.rebate.ui.util.UiUtil;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.ViewGroup;

/**
 * @author Ken Butler Dec 18, 2015
 * WDPageAdapter.java
 * Copyright (c) 2015 ICF International
 */
public class WDPageAdapter extends FragmentPagerAdapter implements ViewPager.OnPageChangeListener {

    private static final String TAG = "WDPageAdapter";
    private WindowAndDoor mWindowAndDoor;
    private int mIndex;
    private CustomViewPager mViewPager;
    private HashMap<WDItem, WDPageFragment> mFragMap;
    private ArrayList<String> mTitleList = new ArrayList<String>();
 
    
    public WDPageAdapter(FragmentManager fm, CustomViewPager viewPager)
    {
	super(fm);
	this.mViewPager = viewPager;
	viewPager.setOnPageChangeListener(this);
	mFragMap = new HashMap<WDItem, WDPageFragment>();
	notifyDataSetChanged();
    }
    
    public void destroyItem1(ViewGroup container, int position, Object object)
    {
	if (position <= getCount())
	{
	    FragmentManager manager = ((Fragment) object).getFragmentManager();
	    Log.v(FormFragment.TAG, "destroyItem " + ((Fragment) object));
	    FragmentTransaction trans = manager.beginTransaction();
	    trans.remove((Fragment) object);
	    trans.commitAllowingStateLoss();

	    mTitleList.remove(position);
	}
    }
    
    public void setWindowAndDoor(WindowAndDoor wd)
    {
	this.mWindowAndDoor = wd;
    }
    
    @Override
    public Object instantiateItem(ViewGroup container, int position)
    {
	if (mTitleList.size() == position)
	{
	    String title = "" + System.nanoTime();
	    mTitleList.add(title);
	}
	return super.instantiateItem(container, position);
    }
    
    @Override
    public long getItemId(int argPosition)
    {
	String te = mTitleList.get(argPosition);
	long id = Long.parseLong(te);
	return id;
    }
    
    @Override
    public Fragment getItem(int index)
    {
	Log.v(FormFragment.TAG, "adapter getItem " + index);
	WDPageFragment frag = null;
	if (mWindowAndDoor.getItems() != null && mWindowAndDoor.getItems().size() > index)
	{
	    WDItem item = mWindowAndDoor.getItems().get(index);
	    Log.d(TAG, "item= "+item);
	    if (mFragMap != null && mFragMap.size() > index && mFragMap.containsKey(item))
	    {
		Log.v(FormFragment.TAG, "adapter  getItem contains " + index);
		return mFragMap.get(item);
	    }

	    frag = WDPageFragment.newInstance(item);
	    String title = null;
	    if (index >= mTitleList.size())
	    {
		title = "" + System.nanoTime();
		mTitleList.add(title);
	    }
	    else
	    {
		title = mTitleList.get(index);
	    }
	    frag.setTitleId(title);
	    frag.setWindowAndDoor(mWindowAndDoor);
	    mFragMap.put(item, frag);
	    this.mIndex = index;
	    Log.v(ThermoStatInstallFragment.TAG, "adapter getItem createNew " + index + " " + frag);
	}
	return frag;
    }
    
    @Override
    public int getCount()
    {
	int count = 0;
	if (mWindowAndDoor != null && mWindowAndDoor.getItems() != null)
	{
	    count = mWindowAndDoor.getItems().size();
	}
	return count;
    }
    
    @Override
    public int getItemPosition(Object object)
    {
	WDPageFragment frag = (WDPageFragment) object;

	int idx = mTitleList.indexOf(frag.getTitleId());
	if (idx >= 0)
	{
	    return idx;
	}
	return POSITION_NONE;
    }
    
    public WDItem deleteItem(int index)
    {
	WDItem item = null;

	Log.v(FormFragment.TAG, "adapter deleteItem " + index);

	if (mWindowAndDoor.getItems() != null && mWindowAndDoor.getItems().size() > index)
	{
	    WDPageFragment fragment = null;
	    item = mWindowAndDoor.getItems().remove(index);
	    if (mFragMap.containsKey(item))
	    {
		fragment = mFragMap.get(item);
		Log.v(FormFragment.TAG, "adapter deleteItem contains " + index);
		mFragMap.remove(item);

		destroyItem1(mViewPager, index, fragment);
	    }
	}

	return item;
    }
    
    public boolean setItemSaved(int index, boolean manualSave)
    {
	boolean retVal = false;

	if (mWindowAndDoor.getItems() != null && mWindowAndDoor.getItems().size() > index)
	{
	    WDItem item = mWindowAndDoor.getItems().get(index);

	    if (item != null)
	    {
		// List<Part> parts = new ArrayList<FormResponseBean.Part>();
		 List<Part> parts = null;
		if (item.getwDDetail() != null && item.getwDDetail().getParts() != null) {
		    parts = item.getwDDetail().getParts();
		}
		/*if (item.isReplaceOnFailSelected() && item.getReplaceOnFail() != null)
		{
		    parts = item.getReplaceOnFail().getParts();
		}
		else if (item.getEarlyRetirement() != null)
		{
		    if (item.getReplaceOnFail() != null)
		    {
			parts.addAll(item.getReplaceOnFail().getParts());
			parts.remove(new Part(AppConstants.EQUIPMENTS_RESULT_ID));
		    }
		    parts.addAll(item.getEarlyRetirement().getParts());
		}*/

		int formState = UiUtil.validateForm(parts);
		item.setMandatoryFiledComplete(formState);

		retVal = formState != AppConstants.FORM_ITEM_FILLED_STATE.ITEM_UNEDITED.getState();

		if (!item.isItemSaved())
		{
		    item.setItemSaved(manualSave ? true : retVal);
		}

		return item.isItemSaved();
	    }
	}

	return retVal;
    }
    
    @Override
    public void onPageScrollStateChanged(int arg0)
    {
	if (mViewPager != null)
	{
	    OnPageChangeListener listener = mViewPager.getPageChangeListener();
	    if (listener != null)
	    {
		listener.onPageScrollStateChanged(arg0);
	    }
	}
	
    }
    
    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2)
    {
	if (mViewPager != null)
	{
	    OnPageChangeListener listener = mViewPager.getPageChangeListener();
	    if (listener != null)
	    {
		listener.onPageScrolled(arg0, arg1, arg2);
	    }
	}
	
    }
    
    @Override
    public void onPageSelected(int arg0)
    {
	mViewPager.onPageChanged();
	if (mViewPager != null)
	{
	    OnPageChangeListener listener = mViewPager.getPageChangeListener();
	    if (listener != null)
	    {
		listener.onPageSelected(arg0);
	    }
	}
	
    }
    
    public WDPageFragment getCurrentFragment(WDItem item)
    {
	return mFragMap.get(item);
    }
    
}
