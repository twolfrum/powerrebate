package com.icf.rebate.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.app.model.SealingType;
import com.icf.rebate.app.model.SealingItem;

import java.util.List;
import java.util.ListIterator;

/**
 * Created by 19713 on 2/8/2018.
 */

public class SealingTypeAdapter extends CommonAdapter<SealingType> implements AdapterListener<SealingType> {

    private LayoutInflater mInflater;

    public SealingTypeAdapter(Context context, int textViewResourceId, List<SealingType> objects) {
        super(context, textViewResourceId, objects);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public class Holder {
        ImageView serviceImage;
        TextView name;
        TextView count;
        ImageView exclamationImage;
    }

    @Override
    public View createAndFillView(SealingType t, int position) {
        Holder holder = null;

        View view = mInflater.inflate(R.layout.equipment_list_item, null, false);
        holder = new Holder();
        holder.name = (TextView) view.findViewById(R.id.equipmentData);
        holder.count = (TextView) view.findViewById(R.id.equipmentCount);
        holder.exclamationImage = (ImageView) view.findViewById(R.id.exclamation_icon);
        view.setTag(holder);
        fillViewData(view, t, position);
        return view;
    }

    @Override
    public View fillViewData(View view, SealingType t, int position)
    {
        final Holder holder = (Holder) view.getTag();
        if (t != null && holder != null)
        {
            holder.name.setText(t.getName());
            List<? extends SealingItem> items = t.getItems();
            int count = 0;

            if (items != null)
            {
                ListIterator<? extends SealingItem> iterator = items.listIterator();

                while (iterator.hasNext())
                {
                    SealingItem item = iterator.next();
                    if (item.isItemSaved())
                    {
                        ++count;
                    }
                }
            }

            holder.count.setText("" + count);
            holder.count.setVisibility(View.VISIBLE);
            if (!t.isMandatoryFieldsComplete())
            {
                holder.exclamationImage.setVisibility(View.VISIBLE);
            }
            else
            {
                holder.exclamationImage.setVisibility(View.GONE);
            }

        }
        return view;
    }

    @Override
    public int getItemViewType(SealingType t, int position)
    {
        return 0;
    }

    @Override
    public void updateViewData(CommonAdapter<SealingType> adapter)
    {
        // Unimpl
    }
}
