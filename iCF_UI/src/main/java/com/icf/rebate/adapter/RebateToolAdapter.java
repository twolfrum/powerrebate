package com.icf.rebate.adapter;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.icf.rebate.app.model.DashboardItem.ServiceItem;
import com.icf.rebate.networklayer.model.CustomerInfo;
import com.icf.rebate.networklayer.model.FormResponseBean;
import com.icf.ameren.rebate.ui.R;
import com.icf.rebate.ui.controller.RebateManager;
import com.icf.rebate.ui.util.AppConstants;

public class RebateToolAdapter extends CommonAdapter<ServiceItem> implements AdapterListener<ServiceItem>
{
    private LayoutInflater inflater;
    private Context context;
    private FormResponseBean bean;

    public RebateToolAdapter(Context context, int textViewResourceId, List<ServiceItem> objects)
    {
	super(context, textViewResourceId, objects);
	this.context = context;
	inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	bean = RebateManager.getInstance().getFormBean();
    }

    @Override
    public View createAndFillView(ServiceItem t, int position)
    {
	RebateToolHolder holder = null;
	final View view = inflater.inflate(R.layout.rebate_list_main_item, null, false);
	holder = new RebateToolHolder();
	holder.btnName = (TextView) view.findViewById(R.id.rebate_tool_list_btn);
	holder.rebateValue = (TextView) view.findViewById(R.id.rebate_equi_list_btn);
	holder.listImage = (ImageView) view.findViewById(R.id.rebate_multi_record_icon);
	holder.exclamationImage = (ImageView) view.findViewById(R.id.exclamation_icon);
	holder.thermoStatCheckbox = (CheckBox) view.findViewById(R.id.thermo_stat_checkbox);
	final CustomerInfo info = bean.getCustomerInfo();
	if (info != null)
	{
	    if (info.getEmailAddress() == null || (info.getEmailAddress() != null && TextUtils.isEmpty(info.getEmailAddress().toString())))
	    {
		holder.thermoStatCheckbox.setChecked(false);
	    }
	}
	view.setTag(holder);
	fillViewData(view, t, position);
	return view;
    }

   

    private void setColor(View view, int position)
    {
	View root = view;
	Drawable d = getContext().getResources().getDrawable(R.drawable.equipment_lst_bg);

	int bluecolor = getContext().getResources().getColor(R.color.start_color);
	int endcolor = getContext().getResources().getColor(R.color.end_color);
	int diffBlue = Color.blue(endcolor) - Color.blue(bluecolor);
	int diffRed = Color.red(endcolor) - Color.red(bluecolor);
	int diffGreen = Color.green(endcolor) - Color.green(bluecolor);

	float mydiffBlue = (float) diffBlue / (float) getCount();
	float mydiffRed = (float) diffRed / (float) getCount();
	float mydiffGreen = (float) diffGreen / (float) getCount();
	mydiffBlue *= position;
	mydiffRed *= position;
	mydiffGreen *= position;
	mydiffBlue += Color.blue(bluecolor);
	mydiffRed += Color.red(bluecolor);
	mydiffGreen += Color.green(bluecolor);

	int color = Color.argb(Color.alpha(bluecolor), (int) mydiffRed, (int) mydiffGreen, (int) mydiffBlue);
	((GradientDrawable) d).setColor(color);
	root.setBackground(d);
    }

    @Override
    public View fillViewData(View view, ServiceItem t, int position)
    {
//	setColor(view, position);
	final RebateToolHolder holder = (RebateToolHolder) view.getTag();
	if (t != null && holder != null)
	{
	    if (t.getServiceItemName().equalsIgnoreCase(context.getResources().getString(R.string.equipment_replacement)))
	    {
		holder.listImage.setVisibility(View.VISIBLE);
	    }
	    else
	    {
		holder.listImage.setVisibility(View.GONE);
	    }
	    if (t.getServiceId().equalsIgnoreCase(context.getResources().getString(R.string.thermostatReferralId)))
	    {
		holder.thermoStatCheckbox.setVisibility(View.VISIBLE);
	    }
	    else
	    {
		holder.thermoStatCheckbox.setVisibility(View.GONE);

	    }

	    if (!RebateManager.getInstance().checkAnyFormFilled(t.getServiceId()))
	    {
		holder.exclamationImage.setVisibility(View.VISIBLE);
	    }
	    else
	    {
		holder.exclamationImage.setVisibility(View.GONE);
	    }

	    if (t.getServiceId().equalsIgnoreCase(context.getResources().getString(R.string.thermostatReferralId)))
	    {
		holder.rebateValue.setVisibility(View.INVISIBLE);
	    }

	    if (holder.thermoStatCheckbox.getVisibility() == View.VISIBLE)
	    {
		CustomerInfo info = bean.getCustomerInfo();

		if (info != null && bean.isThermostatReferralChecked())
		{
		    if (info.getEmailAddress() == null || (info.getEmailAddress() != null && TextUtils.isEmpty(info.getEmailAddress().toString())))
		    {
			holder.thermoStatCheckbox.setChecked(false);
		    }
		    else
		    {
			holder.thermoStatCheckbox.setChecked(true);
		    }

		}
	    }

	    holder.btnName.setText(t.getServiceItemName());
	    holder.rebateValue.setText(getRebateValue(t.getServiceId()));
	}
	return view;
    }

    private String getRebateValue(String id) {

		float value = (float) 0.00;
		String staticValue = "";
		if (context.getString(R.string.equipmentId).equalsIgnoreCase(id)) {
            staticValue = RebateManager.getInstance().getStaticRebateValue(id);
			value = bean.getEquipmentListRebate();
		} else if (context.getString(R.string.qualityInstallVerificationId).equalsIgnoreCase(id)) {
            staticValue = RebateManager.getInstance().getStaticRebateValue(id);
			value = bean.getQIVRebate();
		} else if (context.getString(R.string.tuneUpId).equalsIgnoreCase(id)) {
            staticValue = RebateManager.getInstance().getStaticRebateValue(id);
			value = bean.getTuneUpRebate();
		} else if (context.getString(R.string.ductsealingId).equalsIgnoreCase(id)) {
            staticValue = RebateManager.getInstance().getStaticRebateValue(id);
			value = bean.getDuctSealingRebate();
		} else if (context.getString(R.string.airsealingId).equalsIgnoreCase(id)) {
            staticValue = RebateManager.getInstance().getStaticRebateValue(id);
			value = bean.getAirSealingRebate();
		} else if (context.getString(R.string.windowDoorId).equalsIgnoreCase(id)) {
            staticValue = RebateManager.getInstance().getStaticRebateValue(id);
			value = bean.getWindowDoorListRebate();
		} else if (context.getString(R.string.thermostatInstallId).equalsIgnoreCase(id)) {
			staticValue = RebateManager.getInstance().getStaticRebateValue(id);
			value = bean.getThermostatInstallRebate();
		} else if (context.getString(R.string.insulationId).equalsIgnoreCase(id)) {
            staticValue = RebateManager.getInstance().getStaticRebateValue(id);
			value = bean.getInsulationListRebate();
		}

		if (!TextUtils.isEmpty(staticValue)) return staticValue;

		String formatedValue = (String.format("%.02f", value));
		return "$" + formatedValue;
	}

    @Override
    public int getItemViewType(ServiceItem t, int position)
    {
	return 0;
    }

    @Override
    public void updateViewData(CommonAdapter<ServiceItem> adapter)
    {

    }

    public static class RebateToolHolder
    {
	TextView btnName, rebateValue;

	ImageView listImage, exclamationImage;

	CheckBox thermoStatCheckbox;
    }

}
