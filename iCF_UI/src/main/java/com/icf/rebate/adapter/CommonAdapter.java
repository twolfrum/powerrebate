package com.icf.rebate.adapter;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.icf.rebate.ui.util.ImageLoader;

import android.content.Context;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;

public class CommonAdapter<T> extends ArrayAdapter<T>
{

    private AdapterListener<T> listener;

    private int viewTypeCount = 1;

    private int selectedItem = -1;

    private List<T> objects;

    private MODE mode;

    private int maxCount;

    private boolean updateViewData;

    private Object tag;

    private Filter filter;

    private DropDownAdapterListener<T> dropDownListener;

    private int adapterType = 1;

    public static final int NORMAL_TYPE = 1;

    public static final int SEARCH_TYPE = 2;

    public static enum MODE
    {
	normal, edit
    };

    public CommonAdapter(Context context, int textViewResourceId, List<T> objects)
    {
	super(context, textViewResourceId, objects);
	this.objects = objects;
	this.viewTypeCount = 1;
    }

    public CommonAdapter(Context context, int textViewResourceId, List<T> objects, int viewTypeCount)
    {
	super(context, textViewResourceId, objects);
	this.viewTypeCount = viewTypeCount;
	this.objects = objects;
    }
    
    public void setAdapterType(int type)
    {
	this.adapterType = type;
    }

    public int getAdapterType()
    {
	return this.adapterType;
    }

    public void setAdapterListener(AdapterListener<T> listener)
    {
	this.listener = listener;
    }

    public void setDropDownAdapterListener(DropDownAdapterListener<T> dropDownListener)
    {
	this.dropDownListener = dropDownListener;
    }

    public AdapterListener<T> getAdapterListener()
    {
	return this.listener;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
	T t = getItem(position);
	View view = null;
	if (convertView == null)
	{
	    view = listener.createAndFillView(t, position);
	}
	else
	{
	    view = listener.fillViewData(convertView, t, position);
	}
	return view;
    }

    @Override
    public View getDropDownView(int argPosition, View argConvertView, ViewGroup argParent)
    {
	if (dropDownListener != null)
	{
	    T t = getItem(argPosition);
	    View view = null;
	    if (argConvertView == null)
	    {
		view = dropDownListener.createDropDownAndFillView(t, argPosition);
	    }
	    else
	    {
		view = dropDownListener.fillDropDownViewData(argConvertView, t, argPosition);
	    }
	    return view;
	}
	return super.getDropDownView(argPosition, argConvertView, argParent);
    }

    @Override
    public int getItemViewType(int position)
    {
	if (viewTypeCount == 1)
	{
	    super.getItemViewType(position);
	}
	else
	{
	    T t = getItem(position);
	    return listener.getItemViewType(t, position);
	}
	return 1;
    }

    @Override
    public int getViewTypeCount()
    {
	return viewTypeCount;
    }

    public void setSelectedItem(int position)
    {
	this.selectedItem = position;
    }

    public int getSelectedItem()
    {
	return selectedItem;
    }

    public List<T> getListObjects()
    {
	return objects;
    }

    public void setMode(MODE mode)
    {
	this.mode = mode;
    }

    public MODE getMode()
    {
	return this.mode;
    }

    public void setMaxCount(int count)
    {
	this.maxCount = count;
    }

    @Override
    public int getCount()
    {
	if (maxCount > 0)
	{
	    int count = super.getCount();
	    if (count < maxCount)
	    {
		return count;
	    }
	    return maxCount;
	}
	return super.getCount();
    }

    @Override
    public void add(T argObject)
    {
	updateViewData = true;
	super.add(argObject);
    }

    public void addAll(Collection<? extends T> argCollection)
    {
	updateViewData = true;
	if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
	{
	    super.addAll(argCollection);
	}
	else
	{
	    if (argCollection != null)
	    {
		Iterator<? extends T> t = argCollection.iterator();
		while (t.hasNext())
		{
		    add(t.next());
		}
	    }
	}

    }

    public void addAll(T... argItems)
    {
	updateViewData = true;
	if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
	{
	    super.addAll(argItems);
	}
	else
	{
	    if (argItems != null)
	    {
		for (T item : argItems)
		{
		    add(item);
		}
	    }
	}
    }

    @Override
    public void remove(T argObject)
    {
	updateViewData = true;
	super.remove(argObject);
    }

    @Override
    public void clear()
    {
	updateViewData = true;
	super.clear();
    }

    @Override
    public void insert(T argObject, int argIndex)
    {
	updateViewData = true;
	super.insert(argObject, argIndex);
    }

    @Override
    public void notifyDataSetChanged()
    {
	super.notifyDataSetChanged();
	if (updateViewData)
	{
	    if (listener != null)
	    {
		listener.updateViewData(this);
	    }
	    updateViewData = false;
	}
    }

    public void setTag(Object obj)
    {
	this.tag = obj;
    }

    public Object getTag()
    {
	return this.tag;
    }

    public void setFilter(Filter argFilter)
    {
	filter = argFilter;
    }

    @Override
    public Filter getFilter()
    {
	if (filter != null)
	{
	    return filter;
	}
	return super.getFilter();
    }

    public void add(List<?> temp)
    {
	addAll((Collection<? extends T>) temp);
    }

    public void onDestroy() {
    }
}
